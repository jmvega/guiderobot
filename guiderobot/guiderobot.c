/*
 *  Copyright (C) 2008 José María Cañas Plaza 
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Authors : Julio Vega Pérez (julio [dot] vega [at] urjc [dot] es)
 *  Authors : José María Cañas (jmplaza [at] gsyc [dot] es
 *  Authors : Darío Rodríguez de Diego (drd [dot] sqki [at] gmail [dot] com)
 *  Authors : Víctor Hidalgo 
 *
 *  This schema was programed for GuideRobot Project http://jde.gsyc.es/index.php/GuideRobot
 *
 */

#include "jde.h"
#include "graphics_gtk.h"
#include "guiderobot.h"
#include <string.h>
#include <stdlib.h>

#include <glade/glade.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gtk/gtkgl.h>

#define MAXNAME 50
#define MAXID 50
#define MINROWS 1
#define MAXCOLS 6
#define MINPERSONS 1
#define MAXPHOTO 200
#define MAXPATH 200
#define MINLEFTATTACH 0
#define MINRIGHTATTACH 1
#define MINTOPATTACH 0
#define MINBOTTOMATTACH 1

int guiderobot_id=0; 
int guiderobot_brothers[MAX_SCHEMAS];
arbitration guiderobot_callforarbitration;
int guiderobot_cycle=100; /* ms */

enum guiderobot_states {init,t1,r1,t2,r2,t3,r3,t4,end};
static int guiderobot_state;

/*TYPES*/

typedef struct{
	float x;
	float y;
}Tvoxel;
/*Type with information of a person*/
typedef struct
{
	char name[MAXNAME];
	char surename[MAXNAME];
	char photo[MAXPHOTO];
	Tvoxel location;
	char widgetid[MAXID];
	GtkWidget *image;
	GtkWidget *eventbox;
}TPerson;

/*Type of a list node*/
typedef struct TNode
{
	TPerson person;
	struct TNode *next;	
}TList;

/*Type of table size*/
typedef struct
{
	guint rows;
	guint cols;
}TSizeTable;

/*Imported variables*/
registerdisplay myregister_displaycallback;
deletedisplay mydelete_displaycallback;

/*interaction with locaNavigation schema*/
runFn Navresume;
stopFn Navsuspend;

/*GTK variables*/

GladeXML *xml=NULL; /*Fichero xml*/
GtkWidget *winpeople=NULL;
GtkWidget *table=NULL;


/*Global variables*/
FILE *f;
TList *persons=NULL;/*List with info of people*/
TSizeTable sizetable;/*Size of the table*/
Tvoxel *target;/*Variable who import a new target*/
int *validTarget;/*Variable who import a flag that means valid target*/

char filepath[MAXPATH];

/*Some control variable*/

int createlist = 0;/*createlist = 0 if the list of persons is empty; createlist = 1 if list is full*/
int loadedgui=0;

/*CALLBACKS*/

/*Callback of window closed*/
void on_delete_window (GtkWidget *widget,GdkEvent *event,gpointer user_data)
{
   gdk_threads_leave();
   guiderobot_hide();
   gdk_threads_enter();
}
/*Callback of image clicked*/
void on_image_clicked(GtkWidget *widget, gpointer user_data)
{
	TList *aux;
	char *widgetname;
	int match;

	if (persons != NULL)
	{
		aux = persons;
		match = 0;
		widgetname = gtk_widget_get_name(widget);
		match = strcmp(widgetname,aux->person.widgetid);
		while ((aux->next != NULL) && (match != 0))
		{
			aux = aux->next;
			match = strcmp(widgetname,aux->person.widgetid);
		}
		printf("X = %f Y = %f\n",aux->person.location.x,aux->person.location.y);
		*target = aux->person.location;
		*validTarget = TRUE;
	}
}

/*Creates new node for list*/
void insertNode(TList *newnode)
{
	TList *aux;
	if (persons == NULL)
	{
		persons = newnode;
	}	
	else
	{
		aux = persons;
		while (aux->next != NULL)	
		{
			aux = aux->next;
		}
		aux->next = newnode;
	}
}

/*Creates new list of persons*/
void createList(TList **l)
{
	TList *new;
	int endfile;
	int i;	

	f=fopen(filepath,"r");
	if (f == NULL)
	{
		printf("ERROR: Can't open file\n");
	}
	else
	{
		i = 0;
		endfile = feof(f);
		while(endfile == 0)
		{
			new = (TList *)malloc(sizeof(TList));
			/*Next node = NULL*/
			new->next = NULL;
			fscanf(f,"%s",new->person.name);
			fscanf(f,"%s",new->person.surename);
			fscanf(f,"%s",new->person.photo);		
			fscanf(f,"%f",&new->person.location.x);
			fscanf(f,"%f\n",&new->person.location.y);
			i++;
			endfile = feof(f);
			/*Create the widgets*/
			new->person.image=gtk_image_new_from_file(new->person.photo);
			new->person.eventbox=gtk_event_box_new();
			gtk_container_add (GTK_CONTAINER (new->person.eventbox), new->person.image);
			sprintf(new->person.widgetid,"%d",i);
			gtk_widget_set_name(new->person.eventbox,new->person.widgetid);
			gtk_widget_show(new->person.image);
			gtk_widget_show(new->person.eventbox);
			/*Widget callback*/
			g_signal_connect(G_OBJECT(new->person.eventbox), "button_press_event", G_CALLBACK(on_image_clicked), NULL);
			/*Insert on list*/
			insertNode(new);
		}
	}
	fclose(f);
}

int listLength(TList *pers)
{
	TList *aux;
	int i;
	if (pers == NULL)
	{
		return 0;
	}	
	else
	{
		i=1;
		aux = pers;
		while (aux->next != NULL)
		{
			aux = aux->next;
			i++;
		}
		return i;
	}
}

/*Funcion que calcula la dimension de la tabla*/
void sizeTable(TList *pers,TSizeTable *size)
{
	int numcells;
	numcells = listLength(pers);
	if (numcells < MAXCOLS)
	{
		size->rows = MINROWS;
		if (numcells == 0)
		{
			size->cols = MINPERSONS;
		}
		else
		{
			size->cols = numcells;
		}
	}
	else
	{
		if ((numcells % MAXCOLS) != 0)
		{
			size->rows = (numcells / MAXCOLS)+1 ;
		}
		else
		{
			size->rows = (numcells / MAXCOLS) ;
		}
			size->cols = MAXCOLS;
	}
}

void fillTable(GtkWidget *table,TList *pers)
{
	guint left_attach,right_attach,top_attach,bottom_attach;
	TList *aux;
	int newrow;/*if newrow == 1 then we need a new row to insert the persons*/

	/*Initializing values*/
	left_attach = MINLEFTATTACH;
	right_attach = MINRIGHTATTACH;
	top_attach = MINTOPATTACH;
	bottom_attach = MINBOTTOMATTACH;	

	if (pers == NULL)
	{
		printf("ERROR: Can't fill table because there isn't personal info\n");
	}
	else
	{
		aux = pers;
		gtk_table_attach_defaults (table,aux->person.eventbox,left_attach,right_attach,top_attach,bottom_attach);
		while (aux->next != NULL)
		{
			aux = aux->next;
			left_attach++;
			right_attach++;
			newrow = (left_attach % MAXCOLS == 0);
			if (newrow)
			{
				left_attach = 0;
				right_attach = 1;
				top_attach++;
				bottom_attach++;
			}
		gtk_table_attach_defaults (table,aux->person.eventbox,left_attach,right_attach,top_attach,bottom_attach);
		}
	}
}
void guiderobot_iteration()
{  
 
}

/*Importar símbolos*/
void guiderobot_imports()
{
	Navresume=(runFn)myimport("globalNavigation","run");
	Navsuspend=(stopFn)myimport("globalNavigation","stop");
	target=(Tvoxel *)myimport("globalNavigation","target");
	validTarget=(int *)myimport("globalNavigation","destinationChoosen");
}

/*Exportar símbolos*/
void guiderobot_exports()
{
  myexport("localNavigation","id",&guiderobot_id);
  myexport("localNavigation","run",(void *) &guiderobot_run);
  myexport("localNavigation","stop",(void *) &guiderobot_stop);
}

void guiderobot_stop()
{
  /* printf("guiderobot: cojo-suspend\n");*/
  pthread_mutex_lock(&(all[guiderobot_id].mymutex));
  put_state(guiderobot_id,slept);
	Navsuspend();
  printf("guiderobot: off\n");
  pthread_mutex_unlock(&(all[guiderobot_id].mymutex));
  /*  printf("guiderobot: suelto-suspend\n");*/
}


void guiderobot_run(int father, int *brothers, arbitration fn)
{
  int i;

  /* update the father incorporating this schema as one of its children */
  if (father!=GUIHUMAN && father!=SHELLHUMAN)
    {
      pthread_mutex_lock(&(all[father].mymutex));
      all[father].children[guiderobot_id]=TRUE;
      pthread_mutex_unlock(&(all[father].mymutex));
    }

  pthread_mutex_lock(&(all[guiderobot_id].mymutex));
  /* this schema resumes its execution with no children at all */
  for(i=0;i<MAX_SCHEMAS;i++) all[guiderobot_id].children[i]=FALSE;
  all[guiderobot_id].father=father;
  if (brothers!=NULL)
    {
      for(i=0;i<MAX_SCHEMAS;i++) guiderobot_brothers[i]=-1;
      i=0;
      while(brothers[i]!=-1) {guiderobot_brothers[i]=brothers[i];i++;}
    }
  guiderobot_callforarbitration=fn;
  put_state(guiderobot_id,notready);
  printf("guiderobot: on\n");
  pthread_cond_signal(&(all[guiderobot_id].condition));
  pthread_mutex_unlock(&(all[guiderobot_id].mymutex));
  guiderobot_imports();
	guiderobot_show();
}

void *guiderobot_thread(void *not_used)
{
   struct timeval a,b;
   long n=0; /* iteration */
   long next,bb,aa;

   for(;;)
   {
      pthread_mutex_lock(&(all[guiderobot_id].mymutex));

      if (all[guiderobot_id].state==slept)
      {
	 guiderobot_state=init;
	 pthread_cond_wait(&(all[guiderobot_id].condition),&(all[guiderobot_id].mymutex));
	 pthread_mutex_unlock(&(all[guiderobot_id].mymutex));
      }
      else
      {
	 /* check preconditions. For now, preconditions are always satisfied*/
	 if (all[guiderobot_id].state==notready)
	    put_state(guiderobot_id,ready);
	 /* check brothers and arbitrate. For now this is the only winner */
	 if (all[guiderobot_id].state==ready)
	 {
		put_state(guiderobot_id,winner);
		all[guiderobot_id].children[(*(int *)myimport("localNavigation","id"))]=TRUE;
		Navresume(guiderobot_id,NULL,NULL);
	 	gettimeofday(&a,NULL);
	 	aa=a.tv_sec*1000000+a.tv_usec;
	 	n=0;
	 }

	 if (all[guiderobot_id].state==winner)
	    /* I'm the winner and must execute my iteration */
	 {
	    pthread_mutex_unlock(&(all[guiderobot_id].mymutex));
	    /*      gettimeofday(&a,NULL);*/
	    n++;
	    guiderobot_iteration();
	    gettimeofday(&b,NULL);
	    bb=b.tv_sec*1000000+b.tv_usec;
	    next=aa+(n+1)*(long)guiderobot_cycle*1000-bb;

	    if (next>5000)
	    {
	       usleep(next-5000);
	       /* discounts 5ms taken by calling usleep itself, on average */
	    }
	    else  ;
	 }
	 else
	    /* just let this iteration go away. overhead time negligible */
	 {
	    pthread_mutex_unlock(&(all[guiderobot_id].mymutex));
	    usleep(guiderobot_cycle*1000);
	 }
      }
   }
}
void guiderobot_terminate()
{

}

void guiderobot_init(char *configfile)
{
  pthread_mutex_lock(&(all[guiderobot_id].mymutex));
  printf("guiderobot schema started up\n");
  guiderobot_exports();
  put_state(guiderobot_id,slept);
  pthread_create(&(all[guiderobot_id].mythread),NULL,guiderobot_thread,NULL);
  pthread_mutex_unlock(&(all[guiderobot_id].mymutex));
  if (myregister_displaycallback==NULL){
      if ((myregister_displaycallback=
           (registerdisplay)myimport ("graphics_gtk",
            "register_displaycallback"))==NULL)
      {
         printf ("I can't fetch register_displaycallback from graphics_gtk\n");
         jdeshutdown(1);
      }
      if ((mydelete_displaycallback=
           (deletedisplay)myimport ("graphics_gtk", "delete_displaycallback"))
           ==NULL)
      {
         printf ("I can't fetch delete_displaycallback from graphics_gtk\n");
         jdeshutdown(1);
      }
   }
	/*Leemos el archivo y creamos la lista, para luergo calcular el tamaño de la tabla*/
	strcpy(filepath,configfile);
}

void guiderobot_guidisplay()
{
   gdk_threads_enter();
	 gtk_widget_show(winpeople);
   gtk_widget_queue_draw(winpeople);
   gdk_threads_leave();
}


void guiderobot_hide()
{
  mydelete_displaycallback(guiderobot_guidisplay);
  if (winpeople!=NULL)
	{
      gdk_threads_enter();
      gtk_widget_hide(winpeople);
      gdk_threads_leave();
   }
	loadedgui=0;
	createlist = 0;
	persons=NULL;
	all[guiderobot_id].guistate=off;
}


void guiderobot_show()
{
   static pthread_mutex_t guiderobot_gui_mutex;

   pthread_mutex_lock(&guiderobot_gui_mutex);
   if (!loadedgui){
      loadglade ld_fn;
      loadedgui=1;
      pthread_mutex_unlock(&guiderobot_gui_mutex);

      /*Load the window from the .glade xml file*/
      gdk_threads_enter();  
      if ((ld_fn=(loadglade)myimport("graphics_gtk","load_glade"))==NULL){
         fprintf (stderr,"I can't fetch 'load_glade' from 'graphics_gtk'.\n");
         jdeshutdown(1);
      }
      xml = ld_fn ("guiderobot.glade");
      if (xml==NULL){
         fprintf(stderr, "Error loading graphical guiderobot on xml\n");
         jdeshutdown(1);
      }
      winpeople = glade_xml_get_widget(xml, "people");
			table = glade_xml_get_widget(xml, "table");
			if ((winpeople==NULL) || (table == NULL))
			{
         fprintf(stderr, "Error loading graphical guiderobot loading GtkWidgets\n");
         jdeshutdown(1);
      }
      else
			{
					g_signal_connect(G_OBJECT(winpeople), "delete-event", G_CALLBACK(on_delete_window), NULL);
					/*Fill table with info of persons*/
					if (createlist == 0)
					{
						createList(&persons);
						sizeTable(persons,&sizetable);
						createlist = 1;
					}
					gtk_table_resize(table,sizetable.rows,sizetable.cols);
					fillTable(table,persons);
			}
      gdk_threads_leave();
   }
   myregister_displaycallback(guiderobot_guidisplay);
   all[guiderobot_id].guistate=on;
}
