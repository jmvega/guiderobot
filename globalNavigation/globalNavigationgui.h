/** Header file generated with fdesign on Wed Apr 23 18:04:48 2008.**/

#ifndef FD_globalNavigationgui_h_
#define FD_globalNavigationgui_h_

/** Callbacks, globals and object handlers **/
extern int freeobj_ventanaA_handle(FL_OBJECT *, int, FL_Coord, FL_Coord,
			int, void *);


/**** Forms and Objects ****/
typedef struct {
	FL_FORM *globalNavigationgui;
	void *vdata;
	char *cdata;
	long  ldata;
	FL_OBJECT *canvasGL;
	FL_OBJECT *exit;
	FL_OBJECT *sala123;
	FL_OBJECT *sala122;
	FL_OBJECT *sala120;
	FL_OBJECT *sala119;
	FL_OBJECT *sala118;
	FL_OBJECT *sala109;
	FL_OBJECT *sala110;
	FL_OBJECT *sala111;
	FL_OBJECT *sala112;
	FL_OBJECT *sala113;
	FL_OBJECT *sala114;
	FL_OBJECT *sala121;
	FL_OBJECT *sala117;
	FL_OBJECT *sala115;
	FL_OBJECT *sala116;
	FL_OBJECT *stop;
	FL_OBJECT *ventanaA;
	FL_OBJECT *sala136;
	FL_OBJECT *sala135;
	FL_OBJECT *sala133;
	FL_OBJECT *sala132;
	FL_OBJECT *sala131;
	FL_OBJECT *salaBanios;
	FL_OBJECT *sala124;
	FL_OBJECT *sala125;
	FL_OBJECT *sala126;
	FL_OBJECT *sala127;
	FL_OBJECT *sala128;
	FL_OBJECT *sala134;
	FL_OBJECT *sala130;
	FL_OBJECT *sala129;
} FD_globalNavigationgui;

extern FD_globalNavigationgui * create_form_globalNavigationgui(void);

#endif /* FD_globalNavigationgui_h_ */
