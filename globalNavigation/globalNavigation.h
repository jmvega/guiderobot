#include "gridslib.h"
#include "fuzzylib.h"

#include <GL/gl.h>              
#include <GL/glx.h>
#include <GL/glu.h>
#include <GL/freeglut_std.h>
#include <forms.h>

#include "jde.h"
#include <glcanvas.h>
#include "graphics_xforms.h"
#include "globalNavigationgui.h"
#include "pioneer.h"
#include "progeo.h"

#include "pioneeropengl.h"

#define v3f glVertex3f
#define VFF_SIGMA_THETA 30
#define VFF_SIGMA_RADIO_MAX 5000
#define VFF_SIGMA_RADIO_MIN 1000
#define MOUSELEFT 1
#define MOUSEMIDDLE 2
#define MOUSERIGHT 3
#define MOUSEWHEELUP 4
#define MOUSEWHEELDOWN 5
#define LIMITE_LLEGADA_OBJETIVO 40

extern void globalNavigation_init();
extern void globalNavigation_stop();
extern void globalNavigation_run(int father, int *brothers, arbitration fn);
extern void globalNavigation_show();
extern void globalNavigation_hide();
extern int globalNavigation_cycle;

typedef struct SoRtype{
  struct SoRtype *father;
  float posx;
  float posy;
  float posz;
  float foax;
  float foay;
  float foaz;
  float roll;
}SofReference;
