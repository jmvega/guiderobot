#include "globalNavigation.h"

/* Gui callbacks */
registerbuttons myregister_buttonscallback;
registerdisplay myregister_displaycallback;
deletebuttons mydelete_buttonscallback;
deletedisplay mydelete_displaycallback;

Display *display;
int *myscreen;
Window globalNavigationgui_win;

int globalNavigation_id=0; 
int globalNavigation_brothers[MAX_SCHEMAS];
arbitration globalNavigation_callforarbitration;
int globalNavigation_cycle=200; /* ms */

int *mylaser = NULL;
runFn laserresume;
stopFn lasersuspend;

float *myencoders = NULL;
runFn encodersresume;
stopFn encoderssuspend;

float *myv = NULL;
float *myw = NULL;
runFn motorsresume;
stopFn motorssuspend;

FD_globalNavigationgui *fd_globalNavigationgui;
GC globalNavigationgui_gc;
Window  globalNavigation_canvas_win;
unsigned long globalNavigation_display_state;
int globalNavigation_visual_refresh=FALSE;
int globalNavigation_iteracion_display=0;
int globalNavigation_canvas_mouse_button_pressed=0;
int globalNavigation_mouse_button=0;
int globalNavigation_robot_mouse_motion=0;
FL_Coord globalNavigation_x_canvas,globalNavigation_y_canvas,old_globalNavigation_x_canvas,old_globalNavigation_y_canvas;
float globalNavigation_mouse_x, globalNavigation_mouse_y;
int globalNavigation_mouse_new=0;

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* PARÁMETROS GRAFICOS */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
#define VFF_COLOR 1
#define SECWINDOW_COLOR 0
#define PUSHED 1
#define RELEASED 0 
#define FORCED_REFRESH 999999999.99 /* ms */
static int forcedGradientRefresh = FALSE;
static int forcedOccupancyRefresh = FALSE; // podremos forzar el refresco según nuestras necesidades
static int gradient_planningmax = 1;
int mirojo4=33, mirojo3=34, mirojo2=35, mirojo1=36, migris=37, miverde1=38, miverde2=39, miverde3=40, miverde4=41;
int rango_cell1=42,rango_cell2=43,rango_cell3=44,rango_cell4=45,rango_cell5=46,rango_cell6=47,rango_cell7=48,rango_cell8=49,rango_cell9=50,rango_cell10=51,rango_cell11=52,rango_cell12=53,rango_cell13=54,rango_cell14=55,rango_cell15=56,rango_cell16=57,rango_cell17=58,rango_cell18=59,rango_cell19=60,rango_cell20=61,rango_cell21=62,rango_cell22=63,rango_cell23=64,rango_cell24=65,rango_cell25=66,rango_cell26=67,rango_cell27=68,rango_cell28=69,rango_cell29=70,rango_cell30=71,rango_cell31=72,rango_cell32=73;
int caminito=74;
float glmirojo4[3]; 
/*Every forced_refresh the display is drawn from scratch. If it is too small it will cause flickering with grid display. No merece la pena una hebra de "display_lento" solo para repintar completamente la pantalla. */

float globalNavigation_escala, globalNavigation_width, globalNavigation_height;
float globalNavigation_odometrico[5];
#define RANGO_MAX 50000. /* en mm */
#define RANGO_MIN 500. /* en mm */ 
#define RANGO_INICIAL 30000. /* en mm */
float globalNavigation_rango=(float)RANGO_INICIAL; /* Rango de visualizacion en milimetros */

#define EGOMAX NUM_SONARS+5
XPoint globalNavigation_ego[EGOMAX];
int numglobalNavigation_ego=0;
int visual_delete_globalNavigation_ego=FALSE;

XPoint globalNavigation_laser_dpy[NUM_LASER];
int visual_delete_globalNavigation_laser=FALSE;

XPoint globalNavigation_us_dpy[NUM_SONARS*2];
int visual_delete_globalNavigation_us=FALSE;

#define joystick_maxRotVel 30 /* deg/sec */
#define joystick_maxTranVel 500 /* mm/sec */

/**RECURSOS OPEN GL**/
HPoint2D mouse_on_canvas;
TPinHoleCamera virtualcam;
int flashImage = TRUE;
int foa_mode, cam_mode;
float radius = 500;
float t = 0.5; // lambda
#define PI 3.141592654
#define MAX_RADIUS_VALUE 10000000
#define MIN_RADIUS_VALUE 0
#define WHEEL_DELTA 1000

static SofReference mypioneer;

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* CONSTANTES DE VISUALIZACION DE DISTINTOS RECURSOS GRAFICOS */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
unsigned long DISPLAY_ROBOT = 0x01UL;
unsigned long DISPLAY_SONARS = 0x04UL; // no se muestran, ya que no hacemos uso de ellos
unsigned long DISPLAY_OCCUPANCYGRID = 0x26UL;
unsigned long DISPLAY_GRADIENTGRID = 0x20UL;
static int DEBUG = FALSE;

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* PARÁMETROS DE ALGORITMO GPP -EXTRA AÑADIDO PARA NAVEGACIÓN GLOBAL-*/
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
#define MAX_SEGMENTOS 1250
typedef struct {
  int x_inicio;
  int y_inicio;
  int x_final;
  int y_final; 
} Tsegmento;

typedef struct {
  int ancho;
  int alto;
  int dimension;
  float resolucion;
  int x_max;
  int y_max;
  int x_min;
  int y_min;
  int n_segmentos;
  float x_robot;
  float y_robot;
  float theta_robot;
  Tsegmento segmentos[MAX_SEGMENTOS];
} Tmundo;

Tmundo mi_mundo;
char *fich_imagen;
int drawnOccupancy = FALSE;
Tvoxel actualPoint;

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* PARÁMETROS DE ALGORITMO GPP */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
// Posibles estados de las celdas de la rejilla
#define OCCUPIED -1
#define EMPTY -2
#define UNKNOWN -5

#define MAX_CASILLAS 12000
#define MAX_SEGMENTOS 1250
#define alcanceGPP 700
#define ENCAPILLA -3
#define OPTIMIZED -4
#define DISTANCIA_OBSTACULOS 14 /* distancia maxima a la que propaga el campo de los obstaculos */
#define PESO 10 /* peso inicial con el que propagan los obstaculos */
#define CORRECCION 2 /* este factor se aplica al frente de los obstaculos, 
		       de tal manera que "invierte" el campo haciendolo mayor 
		       cuanto mas cerca se está de uno */
#define ventanaGPPX 500.00 /* mm de longitud lado X */
#define ventanaGPPY 500.00 /* mm de longitud lado Y */
#define numPuntosX 25 /* 25 puntos en ventanaGPPX para dibujar gráficamente */
#define numPuntosY 25 /* 25 puntos en ventanaGPPY para dibujar gráficamente */
#define totalPuntos (4*numPuntosX + 4*numPuntosY) /* 4 porque tenemos 4 laterales en una ventana*/
#define MAX_TIME_ON_GRID 99999 * 1000000 /* tiempo máximo de permanencia de un punto en la rejilla (dado en microseg.) */

typedef struct {
  int fila;
  int columna;
  float estado;
} Tcasilla;

typedef struct nodo {
  float distancia;
  Tcasilla* casillas; /* cuidado con el tamaño, nos podemos quedar sin memoria */
  int casillasmax;
  struct nodo *siguiente;
} Tnodo;

typedef struct map {
	Tvoxel *point;
	int *state;
	unsigned long int *updatingTime;
} Tmap;

static int refreshCounter = 0;
int REFRESH_TIMER = 5; /* a cada REFRESH_TIMER iteraciones refrescaremos gráficamente las Grids */
#define INCREMENT 100 /* mm a los que vamos saltando en el recorrido de rayos para relleno de occupancyGrid */
#define FOLLOWGPP_COLOR 31
#define MAX_CHANGES_ON_GRID 999999 /* maximos cambios permitidos en el occupancyGrid para regenerar el gradiente */

pthread_mutex_t GPPdisplayMutex; // evitar conflictos freeGradientMemory vs. displayGradientGPP

Tnodo *lista; /* frente de onda*/

int kfrente = 0;
float menor = 1;
int destino_alcanzado = 0;
float dist_max = 0;
int destino_elegido = 0;
int origen_elegido = 1;
int obstaculos_propagados = 0;
int obstaculos_normalizados = 1;
int primera_vez = 0;			
int buscar = 0;
int ctrl = 0;
int fin_espera = 0;
int fin_plan = 0;
int en_objetivo = 0;

int isInitGrid = FALSE;
int destinationChoosen = FALSE;

float origenpoint_x, origenpoint_y;
Tvoxel myGlobalDestination;
XPoint GraphicGlobalDestination[9];
float q_llevo;
int theta_robot;
int x_rob;
int y_rob;
int fil, col;

int myColor;

Tvoxel centrogrid;

float *aux; /*vector auxiliar en el k marcaremos las casillas que han sido alcanzadas por el frente de los obstaculos*/

int coste_optimo;
int coste_real;

Tgrid *gradientGrid = NULL; // grid que contiene información del gradiente
Tgrid *occupancyGrid = NULL; // grid que contiene información recibida por los lásers

char *configFile;

int follow_grid_cycle = 100; /* ms */
int follow_grid_controller; 
float dif_angle;
float v_actual,w_actual;
int estado;
float lento;
float anticipo;

int mensajeEsperaDestino = FALSE, /* booleanas para eventos de mensajes de información */
		mensajeEsperaOrigen = FALSE, 
		mensajeDestinoElegido = FALSE,
		mensajeNuevoGradiente = FALSE;
		showMemory = FALSE;
int visual_delete_globalNavigation_grid = FALSE;
XRectangle mapaGrafico [160000]; // 6561 = 81 * 81 --> Representa al grid en nuestro canvas
XRectangle gradienteGrafico [160000]; // 6561 = 81 * 81 --> Representa al grid en nuestro canvas
int pendienteDeReubicacionGrafica = FALSE; // evento activado cuando se produce una reubicación del grid, para que gráficamentet se produzca su actualización correctamente
int movedInformation = FALSE; // evento activado cuando se produce una reubicación del grid, ya que hay que mover la información de estados del occupancyGrid -> gradientGrid


#define tamMemoria 800
Tvoxel ventanaGPP [totalPuntos];
int visual_delete_globalNavigation_ventanaGPP = FALSE;
XPoint coord_graf_ventanaGPP[totalPuntos];

typedef struct {
	Tvoxel pos;
	int color;
} TEstelaRecorrido;

TEstelaRecorrido estelaRecorrido[tamMemoria/2];

XPoint coordGrafEstela[tamMemoria/2];
int posEstela = 0;
int numEstelas;

void rellenoEstela (int color) {
	Tvoxel posActualRobot;
	
	posActualRobot.x = myencoders[0];
	posActualRobot.y = myencoders[1];

	if (posEstela >= (tamMemoria/2)) posEstela = 0; /* reinicio */

	estelaRecorrido [posEstela].pos = posActualRobot;
	estelaRecorrido [posEstela].color = color;

	posEstela ++;
}

/**********************************************************************************/
/**********************************************************************************/
/****************************COMPORTAMIENTO GPP************************************/
/**********************************************************************************/   
unsigned long int dameTiempo() {
	struct timeval t; 

	gettimeofday(&t,NULL);
	return t.tv_sec*1000000 + t.tv_usec;	
}

float localizar_angulo(int col,int fil) {
	float salida;

	if((col==0)&&(fil==0))
		salida=0.;
	else if(((col==1)&&(fil==0))||((col==2)&&(fil==0)))
		salida=0.;
	else if((col==2)&&(fil==1))
		salida=22.5;
	else if (((col==1)&&(fil==1))||((col==2)&&(fil==2)))
		salida=45.;
	else if((col==1)&&(fil==2))
		salida=67.5;
	else if(((col==0)&&(fil==1))||((col==0)&&(fil==2)))
		salida=90.;
	else if((col==-1)&&(fil==2))
		salida=112.5;
	else if(((col==-1)&&(fil==1))||((col==-2)&&(fil==2)))
		salida=135.;
	else if((col==-2)&&(fil==1))
		salida=157.5;
	else if(((col==-1)&&(fil==0))||((col==-2)&&(fil==0)))
		salida=179.;
	else if((col==-2)&&(fil==-1))
		salida=-157.5;
	else if(((col==-1)&&(fil==-1))||((col==-2)&&(fil==-2)))
		salida=-135.;
	else if((col==-1)&&(fil==-2))
		salida=-112.5;
	else if(((col==0)&&(fil==-1))||((col==0)&&(fil==-2)))
		salida=-90.;
	else if((col==1)&&(fil==-2))
		salida=-67.5;
	else if(((col==1)&&(fil==-1))||((col==2)&&(fil==-2)))
		salida=-45.;
	else if((col==2)&&(fil==-1))
		salida=-22.5;

	return salida;
}

void followGradient () {
	int col,fil;
	float follow_grid_v,follow_grid_w,auxi;
	int i,j,col_ant, fil_ant;
	int ok1=0, ok2=0;
	static int previousVzero = FALSE;

	if (((myencoders[0] < myGlobalDestination.x + 200) && (myencoders[0] > myGlobalDestination.x - 200)) && ((myencoders[1] < myGlobalDestination.y + 200) && (myencoders[1] > myGlobalDestination.y - 200))) {
			printf("THE END: ARRIVAL TO DESTINATION!\n"); // llegada a objetivo global (FIN)
			*myv=0;
			*myw=0;
			en_objetivo=1;
	} else { // vamos siguiendo el gradiente
		col=((int)(myencoders[0]-gradientGrid->lo.x-gradientGrid->resolucion/2)/gradientGrid->resolucion);
		fil=((int)(myencoders[1]-gradientGrid->lo.y-gradientGrid->resolucion/2)/gradientGrid->resolucion);

		estado=gradientGrid->map[(fil*gradientGrid->size)+col].estado;

		for (i=2;i>=-2;i--) {
			for(j=2;j>=-2;j--) {
				if ((fil+i>=0)&&(fil+i<gradientGrid->size)&&
					(col+j>=0)&&(col+j<gradientGrid->size)&&
					(gradientGrid->map[(fil+i)*gradientGrid->size+(col+j)].estado<=estado)&&
					(gradientGrid->map[(fil+i)*gradientGrid->size+(col+j)].estado>0))	{
					estado=gradientGrid->map[(fil+i)*gradientGrid->size+(col+j)].estado;
					auxi=localizar_angulo(j,i);

					if (auxi<0)
						auxi=auxi+360;
				}
			}
		}

		dif_angle=auxi-q_llevo;

		if (dif_angle > 180)
			dif_angle=dif_angle-360;

		if (dif_angle < -180)
			dif_angle=360+dif_angle;

		if (estado!=0) {
			if (estado < 20) lento = 2;
			else if (estado<5) lento = 1;
			else lento=0;

			for (i=0;i<NUM_LASER;i++)	{
				if ((i>=67.5)&&(i<=112.5)) {
					if ((mylaser[i]<alcanceGPP+v_actual)&&(mylaser[i]!=0)) anticipo=1;
					else anticipo=0;
				}
			}

			anticipo=0;

			if ((ok1=fc_output(follow_grid_controller,"velocidad_angular",&follow_grid_w))<0) {
				follow_grid_w=0.0;
			}
			if ((ok2=fc_output(follow_grid_controller,"velocidad_traccion",&follow_grid_v))<0) {
				if (previousVzero == TRUE) { // para no quedar estancados en ciertos puntos
					follow_grid_v=120.0;
					previousVzero = FALSE;
				} else {
					follow_grid_v=0.0;
					previousVzero = TRUE; // para la siguiente vuelta sabemos que la previa a sido un 0
				}
			}	// si para alguno de los controladores no hay reglas aplicables fc_output devuelve -1 y se aplica la velocidad_angular o de traccion nula. Si hay alguna regla aplicable se manda el valor que ella recomiende:

			fc_output(follow_grid_controller,"velocidad_angular",&follow_grid_w);
			fc_output(follow_grid_controller,"velocidad_traccion",&follow_grid_v);

			if ((follow_grid_v>v_actual)&&(follow_grid_v>0)) //si aceleramos
				if (follow_grid_v-v_actual>follow_grid_cycle*1)
					follow_grid_v=v_actual+follow_grid_cycle*1;

			*myv=follow_grid_v;
			*myw=follow_grid_w;
			v_actual=follow_grid_v;
			w_actual=follow_grid_w;

			q_llevo=myencoders[2]*RADTODEG;

			if ((myencoders[2]*RADTODEG)>180)
				q_llevo=(myencoders[2]*RADTODEG)-360;
			else if ((int)myencoders[2]*RADTODEG==360)
				q_llevo=0;
			else q_llevo=myencoders[2]*RADTODEG;

			if (aux[(fil)*gradientGrid->size+col]!=2)
				coste_real=coste_real+gradientGrid->map[(fil)*gradientGrid->size+col].estado;

			aux[(fil*gradientGrid->size)+col]=2;
			fil_ant=fil;
			col_ant=col;

			rellenoEstela (FOLLOWGPP_COLOR);
		} // fin if (estado!=0)
	} // fin "vamos siguiendo el gradiente"
}

void ingresar_celdilla_en_frente(int columna,int fila, float estado, float dist) {
	// por construccion jamas se va a insertar un nodo antes del primero, lo cual simplifica los algoritmos
	Tnodo *nuevo,*p,*q;
	int cont=0; // parametro meramente informativo en los printfs

	kfrente++; // parametro meramente informativo en los printfs
	p = lista;
	q = p;

	while((p->siguiente!=NULL)&&((p->distancia<dist))) {
		q = p;
		p = p->siguiente; 
		cont++;
	} // p siempre apuntará al último y q al penúltimo

	if (p->distancia<dist) { // insertamos el nuevo nodo el último, ya que su distancia es mayor
		nuevo=(Tnodo*) malloc (sizeof(Tnodo));
		nuevo->casillas = (Tcasilla*) malloc (MAX_CASILLAS*sizeof (Tcasilla));
		nuevo->distancia=dist;
		nuevo->casillasmax=1;
		nuevo->casillas[0].fila=fila;
		nuevo->casillas[0].columna=columna;
		nuevo->casillas[0].estado=estado;

		nuevo->siguiente=p->siguiente;
		p->siguiente=nuevo;
	}	else if (p->distancia>dist)	{ // insertamos el nuevo nodo antes del último y tras el penúltimo
		nuevo=(Tnodo*) malloc (sizeof(Tnodo));
		nuevo->casillas = (Tcasilla*) malloc (MAX_CASILLAS*sizeof (Tcasilla));
		nuevo->distancia=dist;
		nuevo->casillasmax=1;
		nuevo->casillas[0].fila=fila;
		nuevo->casillas[0].columna=columna;
		nuevo->casillas[0].estado=estado;

		nuevo->siguiente=p;
		q->siguiente=nuevo;
	}	else { // nueva casilla a insertar en el último nodo (el de máxima distancia)
		p->casillas[p->casillasmax].fila=fila;
		p->casillas[p->casillasmax].columna=columna;
		p->casillas[p->casillasmax].estado=estado;
		p->casillasmax++;
	}
}

void optimizeWay(int c, int f) {
	int i,j;
	Tcasilla caminito;
	// busca desde una celdilla del grid su vecina de menor estado, que no haya sido visitada antes. A ésta la marca con el estado de OPTIMIZED

	if ((c > 0) && (f > 0)) { // para no causar conflicto con estas coordenadas
		if (gradientGrid->map[f*gradientGrid->size+c].estado>=0)
			menor=gradientGrid->map[f*gradientGrid->size+c].estado;

		if (gradientGrid->map[(f*gradientGrid->size)+c].estado!=0) { // si no estamos en el objetivo
			for (i=1;i>=-1;i--)
				for(j=1;j>=-1;j--)
					if ((f+i>=0)&&(f+i<gradientGrid->size)&&
					(c+j>=0)&&(c+j<gradientGrid->size)&&
					(menor>=gradientGrid->map[((f+i)*gradientGrid->size)+(c+j)].estado)&&
					(aux[((f+i)*gradientGrid->size)+(c+j)]!=OPTIMIZED)&&
					(gradientGrid->map[((f+i)*gradientGrid->size)+(c+j)].estado!=OCCUPIED)&&
					(gradientGrid->map[((f+i)*gradientGrid->size)+(c+j)].estado!=EMPTY)) {
						menor=gradientGrid->map[((f+i)*gradientGrid->size)+(c+j)].estado;
						caminito.fila=f+i;
						caminito.columna=c+j;
						//vemos cual es la de menor estado de los vecinitos y nos quedamos con ella
					}

			if ((caminito.fila>=0)&&(caminito.fila<gradientGrid->size)&&
					(caminito.columna>=0)&&(caminito.columna<gradientGrid->size)&&
					(gradientGrid->map[(caminito.fila*gradientGrid->size)+caminito.columna].estado!=0)) {
				aux[(caminito.fila*gradientGrid->size)+caminito.columna]=OPTIMIZED; //la marcamos para no volver atras
				coste_optimo=coste_optimo+gradientGrid->map[(caminito.fila*gradientGrid->size)+caminito.columna].estado;
				optimizeWay(caminito.columna, caminito.fila); // RECURSIVO !!
			}
		}
	} // fin if (!((c == 0) && (f == 0)))
}

void propagar (Tnodo *frente, int obstaculo) {
	int i,j,k,cont=0, co,fo;
	float d;

	// Expandir todas las casillas de una determinada distancia en el frente de onda. Cada distancia tiene un nodo. En el frente de onda los nodos estan ordenados por distancia, de menor a mayor, y siempre se recorre en ese orden
	if (frente!=NULL)	{
		for(k=0;k<frente->casillasmax;k++) {
			co=frente->casillas[k].columna;
			fo=frente->casillas[k].fila;

			if (frente->casillas[k].estado!=OCCUPIED) { // si es obstaculo no se modifica, sino actualizaremos su estado
				if ((fo>=0)&&(fo<gradientGrid->size) && (co>=0)&&(co<gradientGrid->size)) {
					gradientGrid->map[fo*gradientGrid->size+co].estado=frente->distancia;

					if ((fo==(int)((origenpoint_y-gradientGrid->lo.y-gradientGrid->resolucion/2)/gradientGrid->resolucion))&&
					(co==(int)((origenpoint_x-gradientGrid->lo.x-gradientGrid->resolucion/2)/gradientGrid->resolucion))&&
					(obstaculo==1)) {
						destino_alcanzado=1;
					}
					if (obstaculo==0) // obstaculos aun no propagados del todo, asi que...
						aux[fo*gradientGrid->size+co] = 1; // ...la introducimos en el vector auxiliar
				}
			}

			for (i=1;i>=-1;i--) { // para cada vecinito de la casilla actual
				for(j=1;j>=-1;j--) { // incorporo los vecinos no recorridos previamente al frente de onda de la siguiente iteracion
					if ((fo+i>=0)&&(fo+i<gradientGrid->size)&&
						(co+j>=0)&&(co+j<gradientGrid->size)&&
						(gradientGrid->map[(fo+i)*gradientGrid->size+(co+j)].estado==EMPTY)) { // si el vecino está vacío y está en la rejilla
						if ((i+j)%2!=0) d = frente->distancia +1.; // los vecinos transversales se les impone distancia 1
						else d = frente->distancia +1.414213562; // los vecinos diagonales se les impone más distancia
						/* Distribucion de distancias en vecinos:
													pi 1 pi
													1  0  1
													pi 1 pi                  */

						cont++; // parametro meramente informativo en los printfs
						ingresar_celdilla_en_frente(co+j,fo+i,gradientGrid->map[(fo+i)*gradientGrid->size+(co+j)].estado,d);
						// para evitar la insercion multiple de la misma celdilla
						gradientGrid->map[(fo+i)*gradientGrid->size+(co+j)].estado=ENCAPILLA;
					}//introducimos en el frente las celdillas propagadas por los obstaculos, con su valor
					else if ((fo+i>=0)&&(fo+i<gradientGrid->size)&&
						(co+j>=0)&&(co+j<gradientGrid->size)&&
						(gradientGrid->map[(fo+i)*gradientGrid->size+(co+j)].estado<=dist_max)&&
						(aux[(fo+i)*gradientGrid->size+(co+j)]==1)&&
						(gradientGrid->map[(fo+i)*gradientGrid->size+(co+j)].estado>0)) {
						if ((i+j)%2!=0)
							d=frente->distancia+gradientGrid->map[(fo+i)*gradientGrid->size+(co+j)].estado+1.;
						else 
							d=frente->distancia+gradientGrid->map[(fo+i)*gradientGrid->size+(co+j)].estado+1.414213562;

						cont++; // parametro meramente informativo en los printfs
						ingresar_celdilla_en_frente(co+j,fo+i,gradientGrid->map[(fo+i)*gradientGrid->size+(co+j)].estado,d); 
						// para evitar la insercion multiple de la misma celdilla
						gradientGrid->map[(fo+i)*gradientGrid->size+(co+j)].estado=ENCAPILLA;
					}
				}
			}
			kfrente--; // parametro meramente informativo en los printfs
		} // fin for(k=0;k<frente->casillasmax;k++)

		lista=frente->siguiente;
		free(frente);	// liberamos la memoria del nodo que acabamos de propagar
	} else // (fin if (frente!=NULL))
		destino_alcanzado=1;
}

void generateGradient () {
	Tnodo *inicio;

	int i,f,c,valor;

	// 1ª FASE: expandimos el frente de los obstaculos
	if (obstaculos_propagados==0)	{
		if (DEBUG) printf("-->Loading gradient. 1st level: \"Obstacle gradient expansion\"...\n");
		if (ctrl==0) { //nos indica si hemos terminado de ingresar todos los obstaculos en el frente
			// ingreso en el frente de onda todos las celdillas que son obstaculo
			for (f=0;f<gradientGrid->size;f++) {
				for (c=0;c<gradientGrid->size;c++) {
					if (gradientGrid->map[f*gradientGrid->size+c].estado==OCCUPIED) {
						if (DEBUG) printf("Detectado obstaculo en %i, %i\n", f, c);
						if (primera_vez==0)	{ // creamos el 1er nodo de la lista, esto se hace en el pcpo de los tiempos
							//printf ("START: myTime is %u\n", dameTiempo ()); // para toma de tiempos con obstaculos
							inicio = (Tnodo*)malloc(sizeof(Tnodo));
							inicio->casillas = (Tcasilla*) malloc (MAX_CASILLAS*sizeof (Tcasilla));
							inicio->distancia = PESO;
							inicio->casillas[0].fila = f;
							inicio->casillas[0].columna = c;
							inicio->casillas[0].estado = OCCUPIED;
							inicio->casillasmax = 1;
							inicio->siguiente = NULL;
							lista = inicio;
						}	else // el resto de las celdas se introducen en la lista
							ingresar_celdilla_en_frente(c,f,OCCUPIED,PESO);

						primera_vez ++;
					}
				}
			}
		}	else { // hay obstaculos en la rejilla
			if (lista->distancia<DISTANCIA_OBSTACULOS) { // cuando todas las celdas obstaculo se han ingresado, propagamos el frente, hasta una distancia limite
				propagar(lista,obstaculos_propagados);
			} else { //acaba esta fase y liberamos la lista
				lista=NULL;
				obstaculos_propagados=1;
				obstaculos_normalizados=0;
			}
		} 
		ctrl ++; // fin de ingreso de obstaculos
	}

	//2ª FASE: limpiamos las celdas del frente que stén marcadas ENCAPILLA y las que sean mayores que la distancia limite
	if (obstaculos_normalizados==0) {
		//printf ("START: myTime is %u\n", dameTiempo ()); // para toma de tiempos cuando no hay obstaculos
		if (DEBUG) printf("-->Loading gradient. 2nd level: \"Improving obstacle gradient\"...\n");
		for (i=0;i<gradientGrid->size*gradientGrid->size;i++) { //limpiamos las celdillas con estado ENCAPILLA para que no nos hagan de frontera
			if (gradientGrid->map[i].estado==ENCAPILLA) {
				gradientGrid->map[i].estado=EMPTY;
				aux[i]=0;//las kitamos del vector auxiliar creado pa llevar cuenta de las celdas propagadas
			}
			//invertimos el campo, para que las celdas + próximas a los obstáculos tengan mayor peso
			else if ((gradientGrid->map[i].estado!=EMPTY)&&(gradientGrid->map[i].estado!=OCCUPIED)) {
				valor=gradientGrid->map[i].estado-PESO;
				gradientGrid->map[i].estado=(gradientGrid->map[i].estado-(valor*CORRECCION))+6;

				if (gradientGrid->map[i].estado>dist_max)
					dist_max=gradientGrid->map[i].estado; //con esto controlamos hasta donde propagan los obstaculos
			}
			if ((gradientGrid->map[i].estado<=0) && (gradientGrid->map[i].estado!=OCCUPIED))	{//comprobamos que no hemos "invertido demasiado"
				gradientGrid->map[i].estado=EMPTY;
				aux[i]=0;//las kitamos del vector auxiliar creado pa llevar cuenta de las celdas propagadas
			}
			obstaculos_normalizados=1;
		} // fin for (i=0;i<gradientGrid->size*gradientGrid->size;i++)
	}

	//3ª FASE: elegimos el destino a donde viajará el robot
	if ((obstaculos_normalizados==1)&&(obstaculos_propagados==1)&&(destino_elegido==0)) {
		if (DEBUG) printf("-->Loading gradient. 3rd level: \"Setting destination (%f, %f)\"...\n", myGlobalDestination.x, myGlobalDestination.y);
		inicio = (Tnodo*)malloc(sizeof(Tnodo));
		inicio->casillas = (Tcasilla*) malloc (MAX_CASILLAS*sizeof (Tcasilla));
		inicio->distancia=0.; // la casilla de destino tiene distancia 0
		inicio->casillas[0].fila=(int)(myGlobalDestination.y-gradientGrid->lo.y-gradientGrid->resolucion/2)/gradientGrid->resolucion;
		inicio->casillas[0].columna=(int)(myGlobalDestination.x-gradientGrid->lo.x-gradientGrid->resolucion/2)/gradientGrid->resolucion;
		inicio->casillasmax=1;
		inicio->siguiente=NULL;
		lista=inicio;

	  destino_elegido=1;
	  origen_elegido=0;
	}

	//4ª FASE: elegimos el origen desde donde viajará el robot
	if (origen_elegido==0) {

		/*PONEMOS EL ORIGEN QUE APARECE EN EL STAGE PORQUE DEL MUNDO LO PILLA MAL??*/

		origenpoint_x=myencoders [0];
		origenpoint_y=myencoders [1];
		if (DEBUG) printf("-->Loading gradient. 4th level: \"Setting origin (%f, %f)\"...\n", origenpoint_x, origenpoint_y);
		origen_elegido=1;
	}

	//5ª FASE: propagamos el gradiente de distancias
	if ((obstaculos_propagados==1)&&(obstaculos_normalizados==1)&&(buscar==0)&&(destino_elegido==1)&&(origen_elegido==1))	{
		if (DEBUG) printf("-->Loading gradient. 5th level: \"Distance gradient expansion\"...\n");
		if ((lista!=NULL)||(destino_alcanzado==0)) {
			for(i=0;i<60;i++) propagar(lista,obstaculos_propagados);
		}

		if (destino_alcanzado==1) {
			for(i=0;i<500;i++) propagar(lista,2);
			buscar=1;
		}
		if (lista==NULL) buscar=1;
	}

	//6ª FASE: buscamos la ruta mínima al objetivo
	if ((obstaculos_propagados==1)&&(obstaculos_normalizados==1)&&(buscar==1)&&(destino_elegido==1)&&(origen_elegido==1)) {
		if (DEBUG) printf("-->Loading gradient. 6th level: \"Searching minimal cost way\"...\n");
		if (menor!=0) {
			forcedGradientRefresh = TRUE; // forzamos el refresco del nuevo gradiente a seguir
			optimizeWay((int)(origenpoint_x-gradientGrid->lo.x-gradientGrid->resolucion/2)/gradientGrid->resolucion,(int)(origenpoint_y-gradientGrid->lo.y-gradientGrid->resolucion/2)/gradientGrid->resolucion);

			if (DEBUG) printf("Way to destination was found!\n");
			//printf ("END: myTime is %u\n", dameTiempo ()); // para toma de tiempos
			fin_plan=1;
		}
	}
}

void waitingForDestination () {
	if (!destinationChoosen) {
		if (!mensajeEsperaDestino) {
			if (DEBUG) printf("Waiting for destination...\n"); // esperamos a que nos pinchen el destino
			mensajeEsperaDestino = TRUE;
		}
	}	else {
		if (DEBUG) printf("Chosen destination on mouse position = [%f, %f]\n", myGlobalDestination.x, myGlobalDestination.y);
		fin_espera = 1;
	}
}

void capturarMundo () { 
	FILE *fich_mundo;
	FILE *salida;
	char *fich="./worlds/dep2puertas.wld";
	int i,j,leidos,linea;
	char buf[MAX_SEGMENTOS], aux[MAX_SEGMENTOS], aux1[MAX_SEGMENTOS],x0[MAX_SEGMENTOS],y0[MAX_SEGMENTOS],x1[MAX_SEGMENTOS],y1[MAX_SEGMENTOS];

	mi_mundo.ancho=0;
	mi_mundo.alto=0;
	mi_mundo.dimension=0;
	mi_mundo.x_max=0;
	mi_mundo.y_max=0;
	mi_mundo.x_min=0;
	mi_mundo.y_min=0;
	mi_mundo.x_robot=0;
	mi_mundo.y_robot=0;
	mi_mundo.theta_robot=0;
	mi_mundo.n_segmentos=0;

	fich_mundo= fopen(fich,"r");
	if (fich_mundo == NULL) {
		fprintf(stderr,"Parser: I can't found file %s\n", fich); exit(-1);
	}

	printf("Reading world file %s...\n", fich);

	leidos=1;
	j=0;
	while (leidos!=0) {
		i=0; 
		buf[0]=' ';

		while ((leidos==1)&&(buf[i]!='\n'))
			leidos=fread(&buf[++i],1,1,fich_mundo);

		linea=i-1;
		i=0;
		while (buf[i]==' ') i++;

		if (!((buf[i]=='\n')||(i>=linea))) {
			sscanf(&buf[i],"%s %s %s %s %s %s",aux,aux1,x0,y0,x1,y1);
			if (strcmp(aux,"width")==0) {
				// printf("%s\n",aux1);
				mi_mundo.ancho=(int) atoi(aux1);
				// printf("ancho:%d\n",mi_mundo.ancho);
			} else if (strcmp(aux,"height")==0) {
				// printf("%s\n",aux1);
				mi_mundo.alto=(int) atoi(aux1);
				// printf("alto:%d\n",mi_mundo.alto);
			} else if (strcmp(aux,"position")==0) {
				// printf("%s\n",aux1);
				mi_mundo.x_robot=(int) atoi(aux1);
				mi_mundo.y_robot=(int) atoi(x0);
				mi_mundo.theta_robot=(int) atoi(y0);

				// printf("position_X:%d position_Y:%d theta:%d\n",mi_mundo.x_robot,mi_mundo.y_robot,mi_mundo.theta_robot);
			}	else if (strcmp(aux,"Start")==0) {
				mi_mundo.segmentos[j].x_inicio=(int) atoi(x0);
				mi_mundo.segmentos[j].y_inicio=(int) atoi(y0);
				mi_mundo.segmentos[j].x_final=(int) atoi(x1);
				mi_mundo.segmentos[j].y_final=(int) atoi(y1);

				// printf("Segmento %d\n x0:%d y0:%d x1:%d y1:%d\n",j,mi_mundo.segmentos[j].x_inicio, mi_mundo.segmentos[j].y_inicio, mi_mundo.segmentos[j].x_final, mi_mundo.segmentos[j].y_final);

				if (mi_mundo.x_max<mi_mundo.segmentos[j].x_inicio)
					mi_mundo.x_max=mi_mundo.segmentos[j].x_inicio;
				else if (mi_mundo.x_max<mi_mundo.segmentos[j].x_final)
					mi_mundo.x_max=mi_mundo.segmentos[j].x_final;
				if (mi_mundo.y_max<mi_mundo.segmentos[j].y_inicio)
					mi_mundo.y_max=mi_mundo.segmentos[j].y_inicio;
				else if (mi_mundo.y_max<mi_mundo.segmentos[j].y_final)
					mi_mundo.y_max=mi_mundo.segmentos[j].y_final;

				if (mi_mundo.x_min>mi_mundo.segmentos[j].x_inicio)
					mi_mundo.x_min=mi_mundo.segmentos[j].x_inicio;
				else if (mi_mundo.x_min>mi_mundo.segmentos[j].x_final)
					mi_mundo.x_min=mi_mundo.segmentos[j].x_final;
				if (mi_mundo.y_min>mi_mundo.segmentos[j].y_inicio)
					mi_mundo.y_min=mi_mundo.segmentos[j].y_inicio;
				else if (mi_mundo.y_min>mi_mundo.segmentos[j].y_final)
					mi_mundo.y_min=mi_mundo.segmentos[j].y_final;

				// printf("Xmax:%d Ymax:%d, Xmin:%d, Ymin:%d\n",mi_mundo.x_max, mi_mundo.y_max, mi_mundo.x_min, mi_mundo.y_min);
				j++;
				mi_mundo.n_segmentos=j;
			}
		} // fin if (!((buf[i]=='\n')||(i>=linea)))
	} // fin while (leidos!=0)

	mi_mundo.ancho=mi_mundo.x_max-mi_mundo.x_min;
	mi_mundo.alto=mi_mundo.y_max-mi_mundo.y_min;

	if (mi_mundo.ancho>=mi_mundo.alto)
		mi_mundo.dimension = mi_mundo.ancho;
	else
		mi_mundo.dimension = mi_mundo.alto;

	fclose(fich_mundo);
	printf("Read %d segments. Closing world file %s...\n", mi_mundo.n_segmentos, fich);
}

Tmundo capturarMundoStage () {
	FILE *world;
	FILE *imagen;
	char *temp;
	char *path="/home/jmvega/worlds-stage/";
	char *fich_mundo="/home/jmvega/worlds-stage/stageDep2.world";
	int i,j,k,leidos,linea,cont;
	char buf[MAX_SEGMENTOS], aux[MAX_SEGMENTOS], val[MAX_SEGMENTOS], x0[MAX_SEGMENTOS], y0[MAX_SEGMENTOS], x1[MAX_SEGMENTOS], y1[MAX_SEGMENTOS];
	char buf_image[80*1024];

	world= fopen(fich_mundo,"r");
	if (fich_mundo == NULL) {
		fprintf(stderr,"Parser: I can't found file %s\n", fich_mundo); exit(-1);
	}

	printf("Reading world file %s\n", fich_mundo);

	leidos=1;
	j=0;
	while (leidos!=0)	{
		i=0; 
		buf[0]=' ';
		while ((leidos==1)&&(buf[i]!='\n'))
			leidos=fread(&buf[++i],1,1,world);

		linea=i-1;
		i=0;
		while (buf[i]==' ') i++;

		if (!((buf[i]=='\n')||(i>=linea))) {
			sscanf(&buf[i],"%s %s %s %s %s %s",aux,val,x0,y0,x1,y1);

			if (strcmp(aux,"bitmap")==0) { // hemos encontrado la línea de especificación del fichero 
				cont=0;
				temp=(char*)malloc((strlen(val)-2)*sizeof(char));
				temp[0] = "\0";
				fich_imagen=(char*)malloc((strlen(temp)+strlen(path))*sizeof(char));
				fich_imagen[0] = "\0";

				for (k=1;k<strlen(val)-1;k++)
				temp[cont++]=val[k];

				sprintf(fich_imagen,"%s%s",path,temp); // obtenemos la ruta completa del fichero imagen a abrir
				strcpy (fich_imagen, "/home/jmvega/worlds-stage/bitmaps/mydep2.pgm");
				printf("Image file to use: %s\n",fich_imagen);
			}

			if (strcmp(aux,"resolution")==0) { // línea de especificación de la resolución
				mi_mundo.resolucion=atof(val)*1000;
				printf("Stage resolution: %f\n",mi_mundo.resolucion);
			} else if (strcmp(aux,"pose")==0) { // línea de posición del robot en nuestro mundo stage
				mi_mundo.x_robot=atof(x0)*1000;
				mi_mundo.y_robot=atof(y0)*1000;
				mi_mundo.theta_robot=atof(x1);
				printf("Robot pose: [%f, %f] Theta: %f\n", mi_mundo.x_robot, mi_mundo.y_robot, mi_mundo.theta_robot);
			}
		} // fin if (!((buf[i]=='\n')||(i>=linea)))
	} // fin while (leidos!=0)

	fclose(world);
	printf("World file %s closed.\n", fich_mundo);

	imagen = fopen (fich_imagen,"r"); // fichero imagen

	if (imagen == NULL) {
		fprintf(stderr,"Parser: I can't found file %s\n", fich_imagen); exit(-1);
	}

	printf("Reading image file %s\n", fich_imagen);

	cont=0;
	leidos=1;
	j=0;
	while (leidos!=0)	{
		i=0; 
		buf_image[0]=' ';
		while ((leidos==1)&&(buf_image[i]!='\n'))
			leidos=fread(&buf_image[++i],1,1,imagen);

		linea=i-1;
		i=0;
		while (buf_image[i]==' ') i++;

		if (!((buf_image[i]=='\n')||(i>=linea))) {
			if (cont<3) {
				sscanf(&buf_image[i],"%s %s",aux,val);	
				cont++;
				if (cont==3) {
					mi_mundo.alto=(int)atoi(val);
					mi_mundo.ancho=(int)atoi(aux);
					printf("Image has %d rows & %d columns, using resolution %f\n",mi_mundo.alto, mi_mundo.ancho, mi_mundo.resolucion);
				}
			}
		} // fin if (!((buf_image[i]=='\n')||(i>=linea)))
	} // fin while (leidos!=0)

	fclose(imagen);
	printf("File %s closed.\n",fich_imagen);

	if (mi_mundo.alto>=mi_mundo.ancho)
		mi_mundo.dimension=(int)(mi_mundo.alto*mi_mundo.resolucion);
	else
		mi_mundo.dimension=(int)(mi_mundo.ancho*mi_mundo.resolucion);
}

void createConfigFile () {
	FILE *salida;
	salida = fopen (configFile,"w");

	if (salida == NULL) {
		fprintf(stderr,"Parser: I can't found file %s\n", configFile); exit(-1);
	}

	printf("Building config file %s\n", configFile);
  fprintf(salida,"dimension = %d\n", mi_mundo.dimension);
	fprintf(salida,"resolucion = 115.\n");
	fprintf(salida,"tipo = ec_diferencial\n");
	fprintf(salida,"ec_diferencial_speed = 1\n");
	fprintf(salida,"paso_tiempo = nulo\n");
	fprintf(salida,"cell_angles = 0\n");
	fprintf(salida,"mayoria_saturacion = 30\n");
	fprintf(salida,"mayoria_ruido = 10\n");
	fprintf(salida,"long_historia = 0\n");
	fprintf(salida,"\n");
	fprintf(salida,"sonar_filtra = independientes\n");
	fprintf(salida,"sonar_geometria = cono_denso\n");
	fprintf(salida,"sonar_apertura = 20.\n");
	fprintf(salida,"sonar_noobstacle = 3000.\n");
	fprintf(salida,"sonar_radialerror = 10.\n");
	fprintf(salida,"sonar_fdistancia = lineal\n");
	fprintf(salida,"sonar_residuo = 0.05\n");
	fprintf(salida,"sonar_o = 0.4\n");
	fprintf(salida,"sonar_e = -0.5\n");
	fprintf(salida,"sonar_mind = 700\n");
	fprintf(salida,"sonar_maxd = 1100.\n");
	fprintf(salida,"\n");
	fprintf(salida,"robot_geometria = cilindro\n");
	fprintf(salida,"robot_radio = 248.\n");
	fprintf(salida,"robot_e = -0.8\n");
	fprintf(salida,"\n");
	fprintf(salida,"laser_geometria = cono_denso\n");
	fprintf(salida,"laser_apertura = 0.5\n");
	fprintf(salida,"laser_muestras = 90\n");
	fprintf(salida,"laser_noobstacle = 8000.\n");
	fprintf(salida,"laser_o = 1\n");
	fprintf(salida,"laser_e = -0.7\n");

	fclose(salida);
}

void initGradientGrid () {
	int i;

	coste_real = 0; // inicialización de parámetros...
	coste_optimo = 0;
	kfrente = 0;
	menor = 1;
	destino_alcanzado = 0;
	dist_max = 0;
	destino_elegido = 0;
	origen_elegido = 1;
	obstaculos_propagados = 0;
	obstaculos_normalizados = 1;
	primera_vez = 0;			
	buscar = 0;
	ctrl = 0;
	fin_espera = 0;
	fin_plan = 0;
	en_objetivo = 0;
	mensajeNuevoGradiente = 0; // mostraremos nueva información del nuevo gradiente

	centrogrid.y = centrogrid.y;

	gradientGrid = inicia_grid (centrogrid, configFile); // gradiente

	aux = (float*)malloc((gradientGrid->size*gradientGrid->size)*sizeof(float));

	for(i=0;i< ((gradientGrid->size)*(gradientGrid->size)) ;i++) {
		gradientGrid->map[i].estado = occupancyGrid->map[i].estado;
	}

	for(i=0;i< ((gradientGrid->size)*(gradientGrid->size)) ;i++)
		aux[i]=0;

	if ((myencoders[2]*RADTODEG)>180)
		q_llevo=(myencoders[2]*RADTODEG)-360;
	else if ((int)myencoders[2]*RADTODEG==360)
		q_llevo=0;
	else
		q_llevo=myencoders[2]*RADTODEG;     

	if (gradientGrid!=NULL) { // si no hubo ningún problema en la inicialización
		if (DEBUG) printf("Gradient Grid initialized correctly--> Size %.0f mm, each cell %.0f mm\n",gradientGrid->size*gradientGrid->resolucion,gradientGrid->resolucion);
	} else {
		if (DEBUG) printf("Not enough memory for gradient grid\n");
		exit (-1);
	}
}

void initOccupancyGrid () {
	int i;

	occupancyGrid = inicia_grid (centrogrid, configFile); // memoria

	for(i=0;i< ((occupancyGrid->size)*(occupancyGrid->size)) ;i++) {
		//printf ("posicion de celda %i del mapa = [%f, %f]\n", i, occupancyGrid->map[i].centro.x, occupancyGrid->map[i].centro.y);
		occupancyGrid->map[i].estado = EMPTY;
		occupancyGrid->map[i].ultima_actualizacion = dameTiempo ();
	}

	if (occupancyGrid!=NULL) { // si no hubo ningún problema en la inicialización
		if (DEBUG) printf("Occupancy Grid initialized correctly--> Size %.0f mm, each cell %.0f mm\n",occupancyGrid->size*occupancyGrid->resolucion,occupancyGrid->resolucion);
	} else {
		if (DEBUG) printf("Not enough memory for occupancy grid\n");
		exit (-1);
	}
} 

int celdaRejilla2xy (Tgrid *myGrid, int celda, int *j, int *i) {
	if ((celda >= (myGrid->size * myGrid->size)) || (celda < 0)) // celda no perteneciente
		return (-1);
	else {
		*i = celda/myGrid->size;
		*j = celda%myGrid->size;
		return (0); // celda OK
	}
}

int xy2celda(int x, int y, int dim) {
	int celda;
	celda=(y*dim)+x;
	//printf("XY2CELDA=> celda:%d=y:%d*dim:%d+x:%d\n",celda,y,dim,x);
	return celda;
}

int xy2celdaRejilla(Tgrid *myGrid, Tvoxel mi_punto) {
  int celda=-1;
  int i,j;
  
  int resolucion=myGrid->resolucion;
  
  j=(mi_punto.x-myGrid->lo.x-resolucion/2)/resolucion;
  i=(mi_punto.y-myGrid->lo.y-resolucion/2)/resolucion;

	if ((i>=0) && (i<myGrid->size) && (j>=0) && (j<myGrid->size)) { // punto perteneciente a rejilla
	  if ((i>=0) && (j>=0)) celda=i*myGrid->size+j;
	}
  
  return celda;
}

void drawRejillaGPP () {
	int i=0, R, G, B;
  for (i = 0 ; i < ((occupancyGrid->size)*(occupancyGrid->size)) ; i++) {
		if (occupancyGrid->map[i].estado == OCCUPIED) { // si está ocupada la celdilla
			R=(int)255;
			G=(int)0;
			B=(int)0;
			fl_mapcolor(myColor,R,G,B);
			fl_set_foreground(globalNavigationgui_gc,myColor);

			xy2globalNavigationcanvas(occupancyGrid->map[i].centro,&(mapaGrafico[i]));

			XDrawRectangle(display,globalNavigation_canvas_win,globalNavigationgui_gc,mapaGrafico[i].x,mapaGrafico[i].y,1,1);
			XFillRectangle(display,globalNavigation_canvas_win,globalNavigationgui_gc,mapaGrafico[i].x,mapaGrafico[i].y,1,1);
		}
	}
}

void drawRejillaGradienteGPP () {
	int i, R, G, B;
	int valor;
	float c;

	pthread_mutex_lock (&GPPdisplayMutex);

	if (lista != NULL) { // no hay gradiente a dibujar
	  for (i = 0 ; i < ((gradientGrid->size)*(gradientGrid->size)) ; i++) {
			if (gradient_planningmax<gradientGrid->map[i].estado)
				gradient_planningmax=gradientGrid->map[i].estado;

			c = (float)(gradientGrid->map[i].estado - 0)*100./(float)(gradient_planningmax-0);

			if(gradientGrid->map[i].estado==-2)
				fl_set_foreground(globalNavigationgui_gc,FL_WHITE);

			else if (c<3.3)  fl_set_foreground(globalNavigationgui_gc,rango_cell31);
			else if (c<6.6)  fl_set_foreground(globalNavigationgui_gc,rango_cell30);
			else if (c<9.9)  fl_set_foreground(globalNavigationgui_gc,rango_cell29);
			else if (c<13.2) fl_set_foreground(globalNavigationgui_gc,rango_cell28);
			else if (c<16.5) fl_set_foreground(globalNavigationgui_gc,rango_cell27);
			else if (c<19.7) fl_set_foreground(globalNavigationgui_gc,rango_cell26);
			else if (c<23)   fl_set_foreground(globalNavigationgui_gc,rango_cell25);
			else if (c<26.3) fl_set_foreground(globalNavigationgui_gc,rango_cell24);
			else if (c<29.6) fl_set_foreground(globalNavigationgui_gc,rango_cell23);	     
			else if (c<32.9) fl_set_foreground(globalNavigationgui_gc,rango_cell22);
			else if (c<36.2) fl_set_foreground(globalNavigationgui_gc,rango_cell21);
			else if (c<39.5) fl_set_foreground(globalNavigationgui_gc,rango_cell20);
			else if (c<42.8) fl_set_foreground(globalNavigationgui_gc,rango_cell19);
			else if (c<46.1) fl_set_foreground(globalNavigationgui_gc,rango_cell18);
			else if (c<49.4) fl_set_foreground(globalNavigationgui_gc,rango_cell17);
			else if (c<52.7) fl_set_foreground(globalNavigationgui_gc,rango_cell16);
			else if (c<56)   fl_set_foreground(globalNavigationgui_gc,rango_cell15);
			else if (c<59.3) fl_set_foreground(globalNavigationgui_gc,rango_cell14);	     
			else if (c<62.6) fl_set_foreground(globalNavigationgui_gc,rango_cell13);
			else if (c<65.9) fl_set_foreground(globalNavigationgui_gc,rango_cell12);
			else if (c<69.2) fl_set_foreground(globalNavigationgui_gc,rango_cell11); 
			else if (c<72.5) fl_set_foreground(globalNavigationgui_gc,rango_cell10); 
			else if (c<75.8) fl_set_foreground(globalNavigationgui_gc,rango_cell9); 
			else if (c<79.1) fl_set_foreground(globalNavigationgui_gc,rango_cell8); 
			else if (c<82.4) fl_set_foreground(globalNavigationgui_gc,rango_cell7); 
			else if (c<85.7) fl_set_foreground(globalNavigationgui_gc,rango_cell6); 
			else if (c<89)   fl_set_foreground(globalNavigationgui_gc,rango_cell5); 
			else if (c<92.3) fl_set_foreground(globalNavigationgui_gc,rango_cell4); 
			else if (c<95.6) fl_set_foreground(globalNavigationgui_gc,rango_cell3); 
			else if (c<98.9) fl_set_foreground(globalNavigationgui_gc,rango_cell2); 
			else if (c<100)  fl_set_foreground(globalNavigationgui_gc,rango_cell1); 
			else fl_set_foreground(globalNavigationgui_gc,rango_cell32);

			xy2globalNavigationcanvas (gradientGrid->map[i].centro,&(gradienteGrafico[i]));

			XDrawRectangle(display,globalNavigation_canvas_win,globalNavigationgui_gc,gradienteGrafico[i].x,gradienteGrafico[i].y,5,5);
			XFillRectangle(display,globalNavigation_canvas_win,globalNavigationgui_gc,gradienteGrafico[i].x,gradienteGrafico[i].y,5,5);
		}
	} else {
		if (DEBUG) printf ("Gradient grid not generated\n");
	}

	pthread_mutex_unlock (&GPPdisplayMutex);
}

void drawPioneerRobot () {
  float r,lati,longi,dx,dy,dz;

	glColor3f(0., 0., 1.);
	glPointSize (15.0);
	glBegin(GL_POINTS);
		v3f(myencoders[0], myencoders[1], 5.);
	glEnd();

/*
  glLoadIdentity();
  if (myencoders!=NULL){
     mypioneer.posx=myencoders[0]/100.;
     mypioneer.posy=myencoders[1]/100.;
     mypioneer.posz=0.;
     mypioneer.foaz=myencoders[0]/100.;
     mypioneer.foay=myencoders[1]/100.;
     mypioneer.foaz=10.;
     mypioneer.roll=myencoders[2]*RADTODEG;
  }
  else{
     mypioneer.posx=0.;
     mypioneer.posy=0.;
     mypioneer.posz=0.;
     mypioneer.foax=0.;
     mypioneer.foay=0.;
     mypioneer.foaz=10.;
     mypioneer.roll=0.;
  }
  glTranslatef(mypioneer.posx,mypioneer.posy,mypioneer.posz);
  dx=(mypioneer.foax-mypioneer.posx);
  dy=(mypioneer.foay-mypioneer.posy);
  dz=(mypioneer.foaz-mypioneer.posz);
  longi=(float)atan2(dy,dx)*360./(2.*PI);
  glRotatef(longi,0.,0.,1.);
  r=sqrt(dx*dx+dy*dy+dz*dz);
  if (r<0.00001) lati=0.;
  else lati=acos(dz/r)*360./(2.*PI);
  glRotatef(lati,0.,1.,0.);
  glRotatef(mypioneer.roll,0.,0.,1.);

  glPushMatrix();
  glTranslatef(1.,0.,0.);
  loadModel();
  glPopMatrix();*/
}

void drawGradient () {
	int i, R, G, B;
	int valor;
	float c;

	glColor3f(1., 0., 1.);
	glPointSize (10.0);
	glBegin(GL_POINTS);
		v3f(myGlobalDestination.x, myGlobalDestination.y, 5.);
	glEnd();

	glPointSize (5.0);

	if (lista != NULL) { // no hay gradiente a dibujar
	  for (i = 0 ; i < ((gradientGrid->size)*(gradientGrid->size)) ; i++) {
			if (gradient_planningmax<gradientGrid->map[i].estado)
				gradient_planningmax=gradientGrid->map[i].estado;

			c = (float)(gradientGrid->map[i].estado - 0)*100./(float)(gradient_planningmax-0);

			if (gradientGrid->map[i].estado!=-2) {
				if(aux[i]==-4)
					glColor3f (1, 0.96, 0.25);

				if(c<3.33) glColor3f(1,1,1);
				else if(c<6.67) glColor3f(0.966666666666667,0.966666666666667,0.966666666666667);
				else if(c<10) glColor3f(0.933333333333333,0.933333333333333,0.933333333333333);
				else if(c<13.33) glColor3f(0.9,0.9,0.9);
				else if(c<16.6666666666667) glColor3f(0.866666666666667,0.866666666666667,0.866666666666667);
				else if(c<20) glColor3f(0.833333333333333,0.833333333333333,0.833333333333333);
				else if(c<23.3333333333333) glColor3f(0.8,0.8,0.8);
				else if(c<26.6666666666667) glColor3f(0.766666666666667,0.766666666666667,0.766666666666667);
				else if(c<30) glColor3f(0.733333333333333,0.733333333333333,0.733333333333333);
				else if(c<33.3333333333333) glColor3f(0.7,0.7,0.7);
				else if(c<36.6666666666667) glColor3f(0.666666666666667,0.666666666666667,0.666666666666667);
				else if(c<40) glColor3f(0.633333333333333,0.633333333333333,0.633333333333333);
				else if(c<43.3333333333333) glColor3f(0.6,0.6,0.6);
				else if(c<46.6666666666667) glColor3f(0.566666666666667,0.566666666666667,0.566666666666667);
				else if(c<50) glColor3f(0.533333333333333,0.533333333333333,0.533333333333333);
				else if(c<53.3333333333333) glColor3f(0.5,0.5,0.5);
				else if(c<56.6666666666667) glColor3f(0.466666666666667,0.466666666666667,0.466666666666667);
				else if(c<60) glColor3f(0.433333333333333,0.433333333333333,0.433333333333333);
				else if(c<63.3333333333333) glColor3f(0.4,0.4,0.4);
				else if(c<66.6666666666667) glColor3f(0.366666666666667,0.366666666666667,0.366666666666667);
				else if(c<70) glColor3f(0.333333333333333,0.333333333333333,0.333333333333333);
				else if(c<73.3333333333333) glColor3f(0.3,0.3,0.3);
				else if(c<76.6666666666667) glColor3f(0.266666666666667,0.266666666666667,0.266666666666667);
				else if(c<80) glColor3f(0.233333333333333,0.233333333333333,0.233333333333333);
				else if(c<83.3333333333333) glColor3f(0.2,0.2,0.2);
				else if(c<86.6666666666667) glColor3f(0.166666666666667,0.166666666666667,0.166666666666667);
				else if(c<90) glColor3f(0.133333333333333,0.133333333333333,0.133333333333333);
				else if(c<93.3333333333333) glColor3f(0.1,0.1,0.1);
				else if(c<96.6666666666667) glColor3f(0.0666666666666667,0.0666666666666667,0.0666666666666667);
				else if(c<100) glColor3f(0.0333333333333333,0.0333333333333333,0.0333333333333333);
				else glColor3f(0.6,0.8,1.0);

				if (aux[i]==2)
					glColor3f(0.74,1.,0.74);

				glBegin(GL_POINTS);
					v3f(gradientGrid->map[i].centro.x, gradientGrid->map[i].centro.y, 0.);
				glEnd();
			}
		}
	} else {
		if (DEBUG) printf ("Gradient grid not generated\n");
	}
}

void drawDepartamental () {
	int i=0, R, G, B;
	R=(int)255;
	G=(int)0;
	B=(int)0;

	glColor3f(R, G, B);
	glPointSize (5.0);
  for (i = 0 ; i < ((occupancyGrid->size)*(occupancyGrid->size)) ; i++) {
		if (occupancyGrid->map[i].estado == OCCUPIED) { // si está ocupada la celdilla
			glBegin(GL_POINTS);
				v3f(occupancyGrid->map[i].centro.x, occupancyGrid->map[i].centro.y, 0.);
			glEnd();
		}
	}
}

void initFollower () {
	// Obtención de reglas para la actuación según el gradiente que se genere
	follow_grid_controller = fc_open("/home/jmvega/jdeconf/follow_grid.fzz"); // abrimos nuestro fichero de reglas borrosas

	if (follow_grid_controller!=-1) { // si hemos abierto correctamente el fichero de reglas borrosas
		fc_link(follow_grid_controller,"angulo",&dif_angle);
		fc_link(follow_grid_controller,"v_actual",&v_actual);
		fc_link(follow_grid_controller,"w_actual",&w_actual);
		fc_link(follow_grid_controller,"cerca",&lento);
		fc_link(follow_grid_controller,"anticipo",&anticipo);
	}
}

void freeGradientMemory () {
	Tnodo* listAux;

	if (DEBUG) printf("Releasing gradient memory...\n");
	termina_grid (gradientGrid);

	free (aux);
	aux = NULL;

	listAux = lista;
	while (listAux != NULL) {
		free (listAux->casillas);
		listAux = listAux->siguiente;
	}

	free (lista);
	lista = NULL;
}

void comportamientoGPP () {
	if (fin_espera == 0) { // hay que pinchar con el ratón al destino para poder generar el gradiente y empezar!
		waitingForDestination ();
	}	else if ((fin_plan == 0) || (!isInitGrid)) { // sólo nos falta ir siguiendo el gradiente generado; sólo se generará cuando haya reubicación
		if (!isInitGrid) { // FASE INICIAL: INICIALIZACION DE ESTRUCTURAS Y DEMÁS... sólo se hará una vez
			initGradientGrid ();
			initFollower ();
			isInitGrid = TRUE;
		}	else if (fin_plan == 0) {
			generateGradient ();
		}
	} else if ((en_objetivo) == 0) {
		followGradient ();
		if (!mensajeNuevoGradiente) {
			REFRESH_TIMER = 99999999;
			if (DEBUG) printf("\nFollowing new optimized way according to the actual gradient\n");
			celdaRejilla2xy (gradientGrid, xy2celdaRejilla (gradientGrid, myGlobalDestination), &col, &fil);
			if (DEBUG) printf("--->Actual destination = [%f, %f]. Celdilla = [%i, %i]\n", myGlobalDestination.x, myGlobalDestination.y, fil, col);
			Tvoxel temporalPoint;
			temporalPoint.x = origenpoint_x;
			temporalPoint.y = origenpoint_y;
			celdaRejilla2xy (gradientGrid, xy2celdaRejilla (gradientGrid, temporalPoint), &col, &fil);
			if (DEBUG) printf("--->Actual origin = [%f, %f]. Celdilla = [%i, %i]\n", temporalPoint.x, temporalPoint.y, fil, col);
			printf("--->Actual origin = [%f, %f]. Celdilla = [%i, %i]\n", temporalPoint.x, temporalPoint.y, fil, col);
			mensajeNuevoGradiente = 1;
		}
	}
}
/**********************************************************************************/
/**********************************************************************************/
/**********************************F I N*******************************************/
/**********************************************************************************/

const char *globalNavigation_range(FL_OBJECT *ob, double value, int prec) {
	static char buf[32];

	sprintf(buf,"%.1f",value/1000.);
	return buf;
}

int xy2globalNavigationcanvas(Tvoxel point, XPoint* grafico) {
     /* return -1 if point falls outside the canvas */
	float xi, yi;

	xi = (point.x * globalNavigation_odometrico[3] - point.y * globalNavigation_odometrico[4] + globalNavigation_odometrico[0])*globalNavigation_escala;
	yi = (point.x * globalNavigation_odometrico[4] + point.y * globalNavigation_odometrico[3] + globalNavigation_odometrico[1])*globalNavigation_escala;
	/* Con esto cambiamos al sistema de referencia de visualizacion, centrado en algun punto xy y con alguna orientacion definidos por odometrico. Ahora cambiamos de ese sistema al del display, donde siempre hay un desplazamiento a la esquina sup. izda. y que las y se cuentan para abajo. */

	grafico->x = xi + globalNavigation_width/2;
	grafico->y = -yi + globalNavigation_height/2;

	if ((grafico->x <0)||(grafico->x>globalNavigation_width)) return -1; 
	if ((grafico->y <0)||(grafico->y>globalNavigation_height)) return -1; 
	return 0;
}

void buildOccupancyGrid () {
	FILE *imagen;
	int cont, leidos, i, j, k, linea;
	char *buff_imagen;
	char *casillas;
	int kk, x_world, y_world,celda,fil,col, previous_x = -9;

	imagen = fopen(fich_imagen,"r");
	if (imagen == NULL) {
		fprintf(stderr,"Parser: I can't found file %s\n", fich_imagen); exit(-1);
	}

	printf("Image file %s opened. Building image buffer...\n", fich_imagen);

	buff_imagen = (char*)malloc((mi_mundo.alto*mi_mundo.ancho*8)*sizeof(char));
	casillas = (char*)malloc((mi_mundo.alto*mi_mundo.ancho)*sizeof (char));

	kk=0;
	cont=0;
	leidos=1;
	j=0;

	while (leidos!=0) {
		i=0; 
		buff_imagen[0]=' ';
		while ((leidos==1)&&(buff_imagen[i]!='\n')) {
			leidos=fread(&buff_imagen[++i],1,1,imagen);
			kk++;
		}
		linea=i-1;
		i=0;
		while (buff_imagen[i]==' ') i++;

		if (!((buff_imagen[i]=='\n')||(i>=linea))) {
			if (cont==4) { // una vez leidas las 5 primeras lineas, procedemos a cargar las casillas
				for (k=0;k<kk-1;k++)
				casillas[k]=buff_imagen[i+k];
			}

			cont++;
			kk=0;
		}
	} // fin while (leidos != 0)

	fclose (imagen);
	printf("Image file %s closed.\n",fich_imagen);

	printf("Building occupancy grid according to image buffer...\n", fich_imagen);

	// recorremos las celdillas, viendo en k pixel caen y actualizando cuando debamos
	k = 0;
	while (k < occupancyGrid->size*occupancyGrid->size) {
		y_world=((k/occupancyGrid->size)*occupancyGrid->resolucion)/mi_mundo.resolucion;
		x_world=((k%occupancyGrid->size)*occupancyGrid->resolucion)/mi_mundo.resolucion;

		if ((x_world<=mi_mundo.ancho)&&(y_world<=mi_mundo.alto)) {
			celda=xy2celda(x_world,mi_mundo.alto-y_world,mi_mundo.ancho);

			if (((int) casillas[celda]==89) || ((int) casillas[celda]==0))
			occupancyGrid->map[k].estado=OCCUPIED;
		}
		k ++;
	}
}

void globalNavigation_iteration() {
	speedcounter(globalNavigation_id);
	comportamientoGPP ();
}

void globalNavigation_stop() {
  pthread_mutex_lock(&(all[globalNavigation_id].mymutex));
  put_state(globalNavigation_id,slept);
  all[globalNavigation_id].children[(*(int *)myimport("laser","id"))]=FALSE;
  all[globalNavigation_id].children[(*(int *)myimport("motors","id"))]=FALSE;
  all[globalNavigation_id].children[(*(int *)myimport("encoders","id"))]=FALSE;
  lasersuspend();
  encoderssuspend();
  motorssuspend();
  printf("globalNavigation: off\n");
  pthread_mutex_unlock(&(all[globalNavigation_id].mymutex));
}

void globalNavigation_run(int father, int *brothers, arbitration fn) {
  int i;
 
  pthread_mutex_lock(&(all[globalNavigation_id].mymutex));
  /* this schema resumes its execution with no children at all */
  for(i=0;i<MAX_SCHEMAS;i++) all[globalNavigation_id].children[i]=FALSE;
 
  all[globalNavigation_id].father=father;
  if (brothers!=NULL)
    {
      for(i=0;i<MAX_SCHEMAS;i++) globalNavigation_brothers[i]=-1;
      i=0;
      while(brothers[i]!=-1) {globalNavigation_brothers[i]=brothers[i];i++;}
    }
  globalNavigation_callforarbitration=fn;
  put_state(globalNavigation_id,notready);

  mylaser=(int *)myimport("laser","laser");
  laserresume=(runFn)myimport("laser","run");
  lasersuspend=(stopFn *)myimport("laser","stop");

  myencoders=(float *)myimport("encoders","jde_robot");
  encodersresume=(runFn)myimport("encoders","run");
  encoderssuspend=(stopFn)myimport("encoders","stop");

  myv=(float *)myimport("motors","v");
  myw=(float *)myimport("motors","w");
  motorsresume=(runFn)myimport("motors","run");
  motorssuspend=(stopFn)myimport("motors","stop");

  printf("globalNavigation: on\n");
  pthread_cond_signal(&(all[globalNavigation_id].condition));
  pthread_mutex_unlock(&(all[globalNavigation_id].mymutex));
}

void *globalNavigation_thread(void *not_used) 
{
  struct timeval a,b;
  long diff, next;

  for(;;)
    {
      pthread_mutex_lock(&(all[globalNavigation_id].mymutex));

      if (all[globalNavigation_id].state==slept) 
	{
	  pthread_cond_wait(&(all[globalNavigation_id].condition),&(all[globalNavigation_id].mymutex));
	  pthread_mutex_unlock(&(all[globalNavigation_id].mymutex));
	}
      else 
	{
	  /* check preconditions. For now, preconditions are always satisfied*/
	  if (all[globalNavigation_id].state==notready) put_state(globalNavigation_id,ready);
	  else if (all[globalNavigation_id].state==ready)	  /* check brothers and arbitrate. For now this is the only winner */
	    { 
	      put_state(globalNavigation_id,winner);
	      *myv=0; *myw=0; 
	      /* start the winner state from controlled motor values */ 
	      all[globalNavigation_id].children[(*(int *)myimport("laser","id"))]=TRUE;
	      all[globalNavigation_id].children[(*(int *)myimport("motors","id"))]=TRUE;
	      all[globalNavigation_id].children[(*(int *)myimport("encoders","id"))]=TRUE;
	      laserresume(globalNavigation_id,NULL,NULL);
	      encodersresume(globalNavigation_id,NULL,NULL);
	      motorsresume(globalNavigation_id,NULL,NULL);
	    }	  
	  else if (all[globalNavigation_id].state==winner);

	  if (all[globalNavigation_id].state==winner)
	    /* I'm the winner and must execute my iteration */
	    {
	      pthread_mutex_unlock(&(all[globalNavigation_id].mymutex));
	      gettimeofday(&a,NULL);
	      globalNavigation_iteration();
	      gettimeofday(&b,NULL);  

	      diff = (b.tv_sec-a.tv_sec)*1000000+b.tv_usec-a.tv_usec;
	      next = globalNavigation_cycle*1000-diff-10000; 
	      if (next>0) 
		/* discounts 10ms taken by calling usleep itself */
		usleep(globalNavigation_cycle*1000-diff);
	      else 
		/* just let this iteration go away. overhead time negligible */
		{
		usleep(globalNavigation_cycle*1000);
		}
	    }
	  else 
	    {
	      pthread_mutex_unlock(&(all[globalNavigation_id].mymutex));
	      usleep(globalNavigation_cycle*1000);
	    }
	}
    }
}

int freeobj_ventanaA_handle(FL_OBJECT *obj, int event, FL_Coord mx, FL_Coord my,int key, void *xev){
  return 0;
}

void motion_event(FL_OBJECT *ob, Window win, int win_width, 
		  int win_height, XKeyEvent *xev, void *user_data){
  float longi, lati,r ;

	xev = (XKeyPressedEvent*)xev;

	/* las siguiente ecuaciones sirven mapear el movimiento:

	Eje X :: (0,w) -> (-180,180)
	Eje Y :: (0,h) -> (-90,90)

	donde (h,w) es el larcho,ancho del gl_canvas
	Si el button pulsado es 2 (centro del raton) entonces no actualizamos la posicion

	*/

	if (2 == xev->keycode) {
		flashImage = TRUE;
	  return;
	}

	mouse_on_canvas.x = ((xev->x*360/fd_globalNavigationgui->canvasGL->w)-180);
	mouse_on_canvas.y = ((xev->y*-180/fd_globalNavigationgui->canvasGL->h)+90);
	
}

void button_press_event (FL_OBJECT *ob, Window win, int win_width, 
			 int win_height, XKeyEvent *xev, void *user_data){
  
  xev = (XKeyPressedEvent*)xev;
  float longi, lati,r;

	/* Los dos modos son excluyentes */
	if (2 == xev->keycode) {
		flashImage = TRUE;
	  return;
	}else {
		flashImage = FALSE;
		if (1 == xev->keycode){
		  cam_mode = 1;
		  foa_mode = 0;  
		}else if (3 == xev->keycode){
		  foa_mode = 1;
		  cam_mode = 0;
		}else if (5 == xev->keycode){
		  if (radius < MAX_RADIUS_VALUE)
		    radius+=WHEEL_DELTA;
		}else if (4 == xev->keycode){
		  if (radius>MIN_RADIUS_VALUE)
		    radius-=WHEEL_DELTA;
		}
	}
}

void loadMyColors () {
  /* colors for grids display */
  fl_mapcolor(mirojo4,205,40,40);
  fl_mapcolor(mirojo3,215,80,80);
  fl_mapcolor(mirojo2,237,131,131);
  fl_mapcolor(mirojo1,255,189,189); 
  fl_mapcolor(migris,230,230,230);
  fl_mapcolor(miverde1,189,255,189); 
  fl_mapcolor(miverde2,131,237,131);
  fl_mapcolor(miverde3,80,215,80);
  fl_mapcolor(miverde4,40,205,40);

  fl_mapcolor(caminito,255,245,63);

  fl_mapcolor(rango_cell1,0,0,0);
  fl_mapcolor(rango_cell2,8,8,8);
  fl_mapcolor(rango_cell3,16,16,16);
  fl_mapcolor(rango_cell4,24,24,24);
  fl_mapcolor(rango_cell5,32,32,32);
  fl_mapcolor(rango_cell6,40,40,40);
  fl_mapcolor(rango_cell7,48,48,48);
  fl_mapcolor(rango_cell8,56,56,56);
  fl_mapcolor(rango_cell9,64,64,64);
  fl_mapcolor(rango_cell10,72,72,72);
  fl_mapcolor(rango_cell11,80,80,80);
  fl_mapcolor(rango_cell12,88,88,88);
  fl_mapcolor(rango_cell13,96,96,96);
  fl_mapcolor(rango_cell14,104,104,104);
  fl_mapcolor(rango_cell15,112,112,112);
  fl_mapcolor(rango_cell16,120,120,120);
  fl_mapcolor(rango_cell17,128,128,128);
  fl_mapcolor(rango_cell18,136,136,136);
  fl_mapcolor(rango_cell19,148,148,148);
  fl_mapcolor(rango_cell20,156,156,156);
  fl_mapcolor(rango_cell21,164,164,164);
  fl_mapcolor(rango_cell22,172,172,172);
  fl_mapcolor(rango_cell23,180,180,180);
  fl_mapcolor(rango_cell24,188,188,188);
  fl_mapcolor(rango_cell25,196,196,196);
  fl_mapcolor(rango_cell26,204,204,204);
  fl_mapcolor(rango_cell27,212,212,212);
  fl_mapcolor(rango_cell28,220,220,220);
  fl_mapcolor(rango_cell29,228,228,228);
  fl_mapcolor(rango_cell30,236,236,236);
  fl_mapcolor(rango_cell31,244,244,244);
  fl_mapcolor(rango_cell32,252,252,252);
}

void globalNavigation_init(char *conf) {
  pthread_mutex_lock(&(all[globalNavigation_id].mymutex));
  myexport("globalNavigation","id",&globalNavigation_id);
  myexport("globalNavigation","run",(void *) &globalNavigation_run);
  myexport("globalNavigation","stop",(void *) &globalNavigation_stop);
  printf("globalNavigation schema started up\n");
  put_state(globalNavigation_id,slept);
  pthread_create(&(all[globalNavigation_id].mythread),NULL,globalNavigation_thread,NULL);

	init_pioneer ();

	if (myregister_buttonscallback==NULL){
    if ((myregister_buttonscallback=(registerbuttons)myimport ("graphics_xforms", "register_buttonscallback"))==NULL){
      if (DEBUG) printf("I can't fetch register_buttonscallback from graphics_xforms\n");
      jdeshutdown(1);
    }
    if ((mydelete_buttonscallback=(deletebuttons)myimport ("graphics_xforms", "delete_buttonscallback"))==NULL){
      if (DEBUG) printf("I can't fetch delete_buttonscallback from graphics_xforms\n");
      jdeshutdown(1);
    }
    if ((myregister_displaycallback=(registerdisplay)myimport ("graphics_xforms", "register_displaycallback"))==NULL){
      if (DEBUG) printf("I can't fetch register_displaycallback from graphics_xforms\n");
      jdeshutdown(1);
    }
    if ((mydelete_displaycallback=(deletedisplay)myimport ("graphics_xforms", "delete_displaycallback"))==NULL){
      jdeshutdown(1);
      if (DEBUG) printf("I can't fetch delete_displaycallback from graphics_xforms\n");
    }
  }

  if ((myscreen=(int *)myimport("graphics_xforms", "screen"))==NULL){
    fprintf(stderr, "globalNavigation: I can't fetch screen from graphics_xforms\n");
    jdeshutdown(1);
  }
  if ((display=(Display *)myimport("graphics_xforms", "display"))==NULL){
    fprintf(stderr, "globalNavigation: I can't fetch display from graphics_xforms\n");
    jdeshutdown(1);
  }
  pthread_mutex_unlock(&(all[globalNavigation_id].mymutex));

	globalNavigation_escala = 20000; // escala del mundillo

	numEstelas = 0; // contador para la estela (rastro) del robot

	/* COMPORTAMIENTO GPP */
	pthread_mutex_init (&GPPdisplayMutex, NULL);
	// Esta inicializacion es equivalente a: pthread_mutex_t GPPdisplayMutex = PTHREAD_MUTEX_INITIALIZER

	configFile = "./gppGlobalPlanning.conf";
	capturarMundoStage ();
	centrogrid.x = 4180;
	centrogrid.y = 1800;
	theta_robot = mi_mundo.theta_robot;
	x_rob = mi_mundo.x_robot;
	y_rob = mi_mundo.y_robot;

	createConfigFile (); // creamos el fichero de configuración gppGlobalPlanning.conf

	initOccupancyGrid ();

	buildOccupancyGrid ();

	// Valores para la cámara virtual de la esquina izquierda del laboratorio (myColorD)
  virtualcam.position.X=50.000000;
  virtualcam.position.Y=100.000000;
  virtualcam.position.Z=2955.000000;
  virtualcam.position.H=1.;
  virtualcam.foa.X=2160.000000;
  virtualcam.foa.Y=1151.000000;
  virtualcam.foa.Z=1550.000000;
  virtualcam.foa.H=1.;
  virtualcam.fdistx=405.399994;
  virtualcam.fdisty=405.399994;
  virtualcam.u0=142.600006;
  virtualcam.v0=150.399994;
  virtualcam.roll=2.956911;

	update_camera_matrix (&virtualcam);

  int argc=1;
  char **argv;
  char *aa;
  char a[]="myjde";
 
  aa=a;
  argv=&aa;

	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	myGlobalDestination.x = 9000.;
	myGlobalDestination.y = -12500.;
}

void globalNavigation_guibuttons(void *obj1)
{
  FL_OBJECT *obj=(FL_OBJECT *)obj1;

	if (obj == fd_globalNavigationgui->exit) globalNavigation_hide();
	else if (obj == fd_globalNavigationgui->stop) {
		globalNavigation_stop ();
	} else if (obj == fd_globalNavigationgui->sala123) {
		if (fl_get_button(obj)==PUSHED) {
			myGlobalDestination.x = 9000.;
			myGlobalDestination.y = 11500.;
			fl_set_button(fd_globalNavigationgui->sala122, 0);
			fl_set_button(fd_globalNavigationgui->sala121, 0);
			fl_set_button(fd_globalNavigationgui->sala120, 0);
			fl_set_button(fd_globalNavigationgui->sala119, 0);
			fl_set_button(fd_globalNavigationgui->sala118, 0);
			fl_set_button(fd_globalNavigationgui->sala117, 0);
			fl_set_button(fd_globalNavigationgui->sala109, 0);
			fl_set_button(fd_globalNavigationgui->sala110, 0);
			fl_set_button(fd_globalNavigationgui->sala111, 0);
			fl_set_button(fd_globalNavigationgui->sala112, 0);
			fl_set_button(fd_globalNavigationgui->sala113, 0);
			fl_set_button(fd_globalNavigationgui->sala114, 0);
			fl_set_button(fd_globalNavigationgui->sala115, 0);
			fl_set_button(fd_globalNavigationgui->sala116, 0);

			fl_set_button(fd_globalNavigationgui->sala136, 0);
			fl_set_button(fd_globalNavigationgui->sala135, 0);
			fl_set_button(fd_globalNavigationgui->sala134, 0);
			fl_set_button(fd_globalNavigationgui->sala133, 0);
			fl_set_button(fd_globalNavigationgui->sala132, 0);
			fl_set_button(fd_globalNavigationgui->sala131, 0);
			fl_set_button(fd_globalNavigationgui->sala130, 0);
			fl_set_button(fd_globalNavigationgui->salaBanios, 0);
			fl_set_button(fd_globalNavigationgui->sala124, 0);
			fl_set_button(fd_globalNavigationgui->sala125, 0);
			fl_set_button(fd_globalNavigationgui->sala126, 0);
			fl_set_button(fd_globalNavigationgui->sala127, 0);
			fl_set_button(fd_globalNavigationgui->sala128, 0);
			fl_set_button(fd_globalNavigationgui->sala129, 0);

			destinationChoosen = TRUE;
		}
	} else if (obj == fd_globalNavigationgui->sala122) {
		if (fl_get_button(obj)==PUSHED) {
			myGlobalDestination.x = 9000.;
			myGlobalDestination.y = 7500.;
			fl_set_button(fd_globalNavigationgui->sala123, 0);
			fl_set_button(fd_globalNavigationgui->sala121, 0);
			fl_set_button(fd_globalNavigationgui->sala120, 0);
			fl_set_button(fd_globalNavigationgui->sala119, 0);
			fl_set_button(fd_globalNavigationgui->sala118, 0);
			fl_set_button(fd_globalNavigationgui->sala117, 0);
			fl_set_button(fd_globalNavigationgui->sala109, 0);
			fl_set_button(fd_globalNavigationgui->sala110, 0);
			fl_set_button(fd_globalNavigationgui->sala111, 0);
			fl_set_button(fd_globalNavigationgui->sala112, 0);
			fl_set_button(fd_globalNavigationgui->sala113, 0);
			fl_set_button(fd_globalNavigationgui->sala114, 0);
			fl_set_button(fd_globalNavigationgui->sala115, 0);
			fl_set_button(fd_globalNavigationgui->sala116, 0);

			fl_set_button(fd_globalNavigationgui->sala136, 0);
			fl_set_button(fd_globalNavigationgui->sala135, 0);
			fl_set_button(fd_globalNavigationgui->sala134, 0);
			fl_set_button(fd_globalNavigationgui->sala133, 0);
			fl_set_button(fd_globalNavigationgui->sala132, 0);
			fl_set_button(fd_globalNavigationgui->sala131, 0);
			fl_set_button(fd_globalNavigationgui->sala130, 0);
			fl_set_button(fd_globalNavigationgui->salaBanios, 0);
			fl_set_button(fd_globalNavigationgui->sala124, 0);
			fl_set_button(fd_globalNavigationgui->sala125, 0);
			fl_set_button(fd_globalNavigationgui->sala126, 0);
			fl_set_button(fd_globalNavigationgui->sala127, 0);
			fl_set_button(fd_globalNavigationgui->sala128, 0);
			fl_set_button(fd_globalNavigationgui->sala129, 0);

			destinationChoosen = TRUE;
		}
	} else if (obj == fd_globalNavigationgui->sala121) {
		if (fl_get_button(obj)==PUSHED) {
			myGlobalDestination.x = 9000.;
			myGlobalDestination.y = 4500.;
			fl_set_button(fd_globalNavigationgui->sala122, 0);
			fl_set_button(fd_globalNavigationgui->sala123, 0);
			fl_set_button(fd_globalNavigationgui->sala120, 0);
			fl_set_button(fd_globalNavigationgui->sala119, 0);
			fl_set_button(fd_globalNavigationgui->sala118, 0);
			fl_set_button(fd_globalNavigationgui->sala117, 0);
			fl_set_button(fd_globalNavigationgui->sala109, 0);
			fl_set_button(fd_globalNavigationgui->sala110, 0);
			fl_set_button(fd_globalNavigationgui->sala111, 0);
			fl_set_button(fd_globalNavigationgui->sala112, 0);
			fl_set_button(fd_globalNavigationgui->sala113, 0);
			fl_set_button(fd_globalNavigationgui->sala114, 0);
			fl_set_button(fd_globalNavigationgui->sala115, 0);
			fl_set_button(fd_globalNavigationgui->sala116, 0);

			fl_set_button(fd_globalNavigationgui->sala136, 0);
			fl_set_button(fd_globalNavigationgui->sala135, 0);
			fl_set_button(fd_globalNavigationgui->sala134, 0);
			fl_set_button(fd_globalNavigationgui->sala133, 0);
			fl_set_button(fd_globalNavigationgui->sala132, 0);
			fl_set_button(fd_globalNavigationgui->sala131, 0);
			fl_set_button(fd_globalNavigationgui->sala130, 0);
			fl_set_button(fd_globalNavigationgui->salaBanios, 0);
			fl_set_button(fd_globalNavigationgui->sala124, 0);
			fl_set_button(fd_globalNavigationgui->sala125, 0);
			fl_set_button(fd_globalNavigationgui->sala126, 0);
			fl_set_button(fd_globalNavigationgui->sala127, 0);
			fl_set_button(fd_globalNavigationgui->sala128, 0);
			fl_set_button(fd_globalNavigationgui->sala129, 0);

			destinationChoosen = TRUE;
		}
	} else if (obj == fd_globalNavigationgui->sala120) {
		if (fl_get_button(obj)==PUSHED) {
			myGlobalDestination.x = 9000.;
			myGlobalDestination.y = 1000.;
			fl_set_button(fd_globalNavigationgui->sala122, 0);
			fl_set_button(fd_globalNavigationgui->sala121, 0);
			fl_set_button(fd_globalNavigationgui->sala123, 0);
			fl_set_button(fd_globalNavigationgui->sala119, 0);
			fl_set_button(fd_globalNavigationgui->sala118, 0);
			fl_set_button(fd_globalNavigationgui->sala117, 0);
			fl_set_button(fd_globalNavigationgui->sala109, 0);
			fl_set_button(fd_globalNavigationgui->sala110, 0);
			fl_set_button(fd_globalNavigationgui->sala111, 0);
			fl_set_button(fd_globalNavigationgui->sala112, 0);
			fl_set_button(fd_globalNavigationgui->sala113, 0);
			fl_set_button(fd_globalNavigationgui->sala114, 0);
			fl_set_button(fd_globalNavigationgui->sala115, 0);
			fl_set_button(fd_globalNavigationgui->sala116, 0);

			fl_set_button(fd_globalNavigationgui->sala136, 0);
			fl_set_button(fd_globalNavigationgui->sala135, 0);
			fl_set_button(fd_globalNavigationgui->sala134, 0);
			fl_set_button(fd_globalNavigationgui->sala133, 0);
			fl_set_button(fd_globalNavigationgui->sala132, 0);
			fl_set_button(fd_globalNavigationgui->sala131, 0);
			fl_set_button(fd_globalNavigationgui->sala130, 0);
			fl_set_button(fd_globalNavigationgui->salaBanios, 0);
			fl_set_button(fd_globalNavigationgui->sala124, 0);
			fl_set_button(fd_globalNavigationgui->sala125, 0);
			fl_set_button(fd_globalNavigationgui->sala126, 0);
			fl_set_button(fd_globalNavigationgui->sala127, 0);
			fl_set_button(fd_globalNavigationgui->sala128, 0);
			fl_set_button(fd_globalNavigationgui->sala129, 0);

			destinationChoosen = TRUE;
		}
	} else if (obj == fd_globalNavigationgui->sala119) {
		if (fl_get_button(obj)==PUSHED) {
			myGlobalDestination.x = 9000.;
			myGlobalDestination.y = -2000.;
			fl_set_button(fd_globalNavigationgui->sala122, 0);
			fl_set_button(fd_globalNavigationgui->sala121, 0);
			fl_set_button(fd_globalNavigationgui->sala120, 0);
			fl_set_button(fd_globalNavigationgui->sala123, 0);
			fl_set_button(fd_globalNavigationgui->sala118, 0);
			fl_set_button(fd_globalNavigationgui->sala117, 0);
			fl_set_button(fd_globalNavigationgui->sala109, 0);
			fl_set_button(fd_globalNavigationgui->sala110, 0);
			fl_set_button(fd_globalNavigationgui->sala111, 0);
			fl_set_button(fd_globalNavigationgui->sala112, 0);
			fl_set_button(fd_globalNavigationgui->sala113, 0);
			fl_set_button(fd_globalNavigationgui->sala114, 0);
			fl_set_button(fd_globalNavigationgui->sala115, 0);
			fl_set_button(fd_globalNavigationgui->sala116, 0);

			fl_set_button(fd_globalNavigationgui->sala136, 0);
			fl_set_button(fd_globalNavigationgui->sala135, 0);
			fl_set_button(fd_globalNavigationgui->sala134, 0);
			fl_set_button(fd_globalNavigationgui->sala133, 0);
			fl_set_button(fd_globalNavigationgui->sala132, 0);
			fl_set_button(fd_globalNavigationgui->sala131, 0);
			fl_set_button(fd_globalNavigationgui->sala130, 0);
			fl_set_button(fd_globalNavigationgui->salaBanios, 0);
			fl_set_button(fd_globalNavigationgui->sala124, 0);
			fl_set_button(fd_globalNavigationgui->sala125, 0);
			fl_set_button(fd_globalNavigationgui->sala126, 0);
			fl_set_button(fd_globalNavigationgui->sala127, 0);
			fl_set_button(fd_globalNavigationgui->sala128, 0);
			fl_set_button(fd_globalNavigationgui->sala129, 0);

			destinationChoosen = TRUE;
		}
	} else if (obj == fd_globalNavigationgui->sala118) {
		if (fl_get_button(obj)==PUSHED) {
			myGlobalDestination.x = 9000.;
			myGlobalDestination.y = -5500.;
			fl_set_button(fd_globalNavigationgui->sala122, 0);
			fl_set_button(fd_globalNavigationgui->sala121, 0);
			fl_set_button(fd_globalNavigationgui->sala120, 0);
			fl_set_button(fd_globalNavigationgui->sala119, 0);
			fl_set_button(fd_globalNavigationgui->sala123, 0);
			fl_set_button(fd_globalNavigationgui->sala117, 0);
			fl_set_button(fd_globalNavigationgui->sala109, 0);
			fl_set_button(fd_globalNavigationgui->sala110, 0);
			fl_set_button(fd_globalNavigationgui->sala111, 0);
			fl_set_button(fd_globalNavigationgui->sala112, 0);
			fl_set_button(fd_globalNavigationgui->sala113, 0);
			fl_set_button(fd_globalNavigationgui->sala114, 0);
			fl_set_button(fd_globalNavigationgui->sala115, 0);
			fl_set_button(fd_globalNavigationgui->sala116, 0);

			fl_set_button(fd_globalNavigationgui->sala136, 0);
			fl_set_button(fd_globalNavigationgui->sala135, 0);
			fl_set_button(fd_globalNavigationgui->sala134, 0);
			fl_set_button(fd_globalNavigationgui->sala133, 0);
			fl_set_button(fd_globalNavigationgui->sala132, 0);
			fl_set_button(fd_globalNavigationgui->sala131, 0);
			fl_set_button(fd_globalNavigationgui->sala130, 0);
			fl_set_button(fd_globalNavigationgui->salaBanios, 0);
			fl_set_button(fd_globalNavigationgui->sala124, 0);
			fl_set_button(fd_globalNavigationgui->sala125, 0);
			fl_set_button(fd_globalNavigationgui->sala126, 0);
			fl_set_button(fd_globalNavigationgui->sala127, 0);
			fl_set_button(fd_globalNavigationgui->sala128, 0);
			fl_set_button(fd_globalNavigationgui->sala129, 0);

			destinationChoosen = TRUE;
		}
	} else if (obj == fd_globalNavigationgui->sala117) {
		if (fl_get_button(obj)==PUSHED) {
			myGlobalDestination.x = 9000.;
			myGlobalDestination.y = -12500.;
			fl_set_button(fd_globalNavigationgui->sala122, 0);
			fl_set_button(fd_globalNavigationgui->sala121, 0);
			fl_set_button(fd_globalNavigationgui->sala120, 0);
			fl_set_button(fd_globalNavigationgui->sala119, 0);
			fl_set_button(fd_globalNavigationgui->sala118, 0);
			fl_set_button(fd_globalNavigationgui->sala123, 0);
			fl_set_button(fd_globalNavigationgui->sala109, 0);
			fl_set_button(fd_globalNavigationgui->sala110, 0);
			fl_set_button(fd_globalNavigationgui->sala111, 0);
			fl_set_button(fd_globalNavigationgui->sala112, 0);
			fl_set_button(fd_globalNavigationgui->sala113, 0);
			fl_set_button(fd_globalNavigationgui->sala114, 0);
			fl_set_button(fd_globalNavigationgui->sala115, 0);
			fl_set_button(fd_globalNavigationgui->sala116, 0);

			fl_set_button(fd_globalNavigationgui->sala136, 0);
			fl_set_button(fd_globalNavigationgui->sala135, 0);
			fl_set_button(fd_globalNavigationgui->sala134, 0);
			fl_set_button(fd_globalNavigationgui->sala133, 0);
			fl_set_button(fd_globalNavigationgui->sala132, 0);
			fl_set_button(fd_globalNavigationgui->sala131, 0);
			fl_set_button(fd_globalNavigationgui->sala130, 0);
			fl_set_button(fd_globalNavigationgui->salaBanios, 0);
			fl_set_button(fd_globalNavigationgui->sala124, 0);
			fl_set_button(fd_globalNavigationgui->sala125, 0);
			fl_set_button(fd_globalNavigationgui->sala126, 0);
			fl_set_button(fd_globalNavigationgui->sala127, 0);
			fl_set_button(fd_globalNavigationgui->sala128, 0);
			fl_set_button(fd_globalNavigationgui->sala129, 0);

			destinationChoosen = TRUE;
		}
	} else if (obj == fd_globalNavigationgui->sala109) {
		if (fl_get_button(obj)==PUSHED) {
			myGlobalDestination.x = 9000.;
			myGlobalDestination.y = 14000.;
			fl_set_button(fd_globalNavigationgui->sala122, 0);
			fl_set_button(fd_globalNavigationgui->sala121, 0);
			fl_set_button(fd_globalNavigationgui->sala120, 0);
			fl_set_button(fd_globalNavigationgui->sala119, 0);
			fl_set_button(fd_globalNavigationgui->sala118, 0);
			fl_set_button(fd_globalNavigationgui->sala117, 0);
			fl_set_button(fd_globalNavigationgui->sala123, 0);
			fl_set_button(fd_globalNavigationgui->sala110, 0);
			fl_set_button(fd_globalNavigationgui->sala111, 0);
			fl_set_button(fd_globalNavigationgui->sala112, 0);
			fl_set_button(fd_globalNavigationgui->sala113, 0);
			fl_set_button(fd_globalNavigationgui->sala114, 0);
			fl_set_button(fd_globalNavigationgui->sala115, 0);
			fl_set_button(fd_globalNavigationgui->sala116, 0);

			fl_set_button(fd_globalNavigationgui->sala136, 0);
			fl_set_button(fd_globalNavigationgui->sala135, 0);
			fl_set_button(fd_globalNavigationgui->sala134, 0);
			fl_set_button(fd_globalNavigationgui->sala133, 0);
			fl_set_button(fd_globalNavigationgui->sala132, 0);
			fl_set_button(fd_globalNavigationgui->sala131, 0);
			fl_set_button(fd_globalNavigationgui->sala130, 0);
			fl_set_button(fd_globalNavigationgui->salaBanios, 0);
			fl_set_button(fd_globalNavigationgui->sala124, 0);
			fl_set_button(fd_globalNavigationgui->sala125, 0);
			fl_set_button(fd_globalNavigationgui->sala126, 0);
			fl_set_button(fd_globalNavigationgui->sala127, 0);
			fl_set_button(fd_globalNavigationgui->sala128, 0);
			fl_set_button(fd_globalNavigationgui->sala129, 0);

			destinationChoosen = TRUE;
		}
	} else if (obj == fd_globalNavigationgui->sala110) {
		if (fl_get_button(obj)==PUSHED) {
			myGlobalDestination.x = 9000.;
			myGlobalDestination.y = 10500.;
			fl_set_button(fd_globalNavigationgui->sala122, 0);
			fl_set_button(fd_globalNavigationgui->sala121, 0);
			fl_set_button(fd_globalNavigationgui->sala120, 0);
			fl_set_button(fd_globalNavigationgui->sala119, 0);
			fl_set_button(fd_globalNavigationgui->sala118, 0);
			fl_set_button(fd_globalNavigationgui->sala117, 0);
			fl_set_button(fd_globalNavigationgui->sala109, 0);
			fl_set_button(fd_globalNavigationgui->sala123, 0);
			fl_set_button(fd_globalNavigationgui->sala111, 0);
			fl_set_button(fd_globalNavigationgui->sala112, 0);
			fl_set_button(fd_globalNavigationgui->sala113, 0);
			fl_set_button(fd_globalNavigationgui->sala114, 0);
			fl_set_button(fd_globalNavigationgui->sala115, 0);
			fl_set_button(fd_globalNavigationgui->sala116, 0);

			fl_set_button(fd_globalNavigationgui->sala136, 0);
			fl_set_button(fd_globalNavigationgui->sala135, 0);
			fl_set_button(fd_globalNavigationgui->sala134, 0);
			fl_set_button(fd_globalNavigationgui->sala133, 0);
			fl_set_button(fd_globalNavigationgui->sala132, 0);
			fl_set_button(fd_globalNavigationgui->sala131, 0);
			fl_set_button(fd_globalNavigationgui->sala130, 0);
			fl_set_button(fd_globalNavigationgui->salaBanios, 0);
			fl_set_button(fd_globalNavigationgui->sala124, 0);
			fl_set_button(fd_globalNavigationgui->sala125, 0);
			fl_set_button(fd_globalNavigationgui->sala126, 0);
			fl_set_button(fd_globalNavigationgui->sala127, 0);
			fl_set_button(fd_globalNavigationgui->sala128, 0);
			fl_set_button(fd_globalNavigationgui->sala129, 0);

			destinationChoosen = TRUE;
		}
	} else if (obj == fd_globalNavigationgui->sala111) {
		if (fl_get_button(obj)==PUSHED) {
			myGlobalDestination.x = 9000.;
			myGlobalDestination.y = 7500.;
			fl_set_button(fd_globalNavigationgui->sala122, 0);
			fl_set_button(fd_globalNavigationgui->sala121, 0);
			fl_set_button(fd_globalNavigationgui->sala120, 0);
			fl_set_button(fd_globalNavigationgui->sala119, 0);
			fl_set_button(fd_globalNavigationgui->sala118, 0);
			fl_set_button(fd_globalNavigationgui->sala117, 0);
			fl_set_button(fd_globalNavigationgui->sala109, 0);
			fl_set_button(fd_globalNavigationgui->sala110, 0);
			fl_set_button(fd_globalNavigationgui->sala123, 0);
			fl_set_button(fd_globalNavigationgui->sala112, 0);
			fl_set_button(fd_globalNavigationgui->sala113, 0);
			fl_set_button(fd_globalNavigationgui->sala114, 0);
			fl_set_button(fd_globalNavigationgui->sala115, 0);
			fl_set_button(fd_globalNavigationgui->sala116, 0);

			fl_set_button(fd_globalNavigationgui->sala136, 0);
			fl_set_button(fd_globalNavigationgui->sala135, 0);
			fl_set_button(fd_globalNavigationgui->sala134, 0);
			fl_set_button(fd_globalNavigationgui->sala133, 0);
			fl_set_button(fd_globalNavigationgui->sala132, 0);
			fl_set_button(fd_globalNavigationgui->sala131, 0);
			fl_set_button(fd_globalNavigationgui->sala130, 0);
			fl_set_button(fd_globalNavigationgui->salaBanios, 0);
			fl_set_button(fd_globalNavigationgui->sala124, 0);
			fl_set_button(fd_globalNavigationgui->sala125, 0);
			fl_set_button(fd_globalNavigationgui->sala126, 0);
			fl_set_button(fd_globalNavigationgui->sala127, 0);
			fl_set_button(fd_globalNavigationgui->sala128, 0);
			fl_set_button(fd_globalNavigationgui->sala129, 0);

			destinationChoosen = TRUE;
		}
	} else if (obj == fd_globalNavigationgui->sala112) {
		if (fl_get_button(obj)==PUSHED) {
			myGlobalDestination.x = 9000.;
			myGlobalDestination.y = 4500.;
			fl_set_button(fd_globalNavigationgui->sala122, 0);
			fl_set_button(fd_globalNavigationgui->sala121, 0);
			fl_set_button(fd_globalNavigationgui->sala120, 0);
			fl_set_button(fd_globalNavigationgui->sala119, 0);
			fl_set_button(fd_globalNavigationgui->sala118, 0);
			fl_set_button(fd_globalNavigationgui->sala117, 0);
			fl_set_button(fd_globalNavigationgui->sala109, 0);
			fl_set_button(fd_globalNavigationgui->sala110, 0);
			fl_set_button(fd_globalNavigationgui->sala111, 0);
			fl_set_button(fd_globalNavigationgui->sala123, 0);
			fl_set_button(fd_globalNavigationgui->sala113, 0);
			fl_set_button(fd_globalNavigationgui->sala114, 0);
			fl_set_button(fd_globalNavigationgui->sala115, 0);
			fl_set_button(fd_globalNavigationgui->sala116, 0);

			fl_set_button(fd_globalNavigationgui->sala136, 0);
			fl_set_button(fd_globalNavigationgui->sala135, 0);
			fl_set_button(fd_globalNavigationgui->sala134, 0);
			fl_set_button(fd_globalNavigationgui->sala133, 0);
			fl_set_button(fd_globalNavigationgui->sala132, 0);
			fl_set_button(fd_globalNavigationgui->sala131, 0);
			fl_set_button(fd_globalNavigationgui->sala130, 0);
			fl_set_button(fd_globalNavigationgui->salaBanios, 0);
			fl_set_button(fd_globalNavigationgui->sala124, 0);
			fl_set_button(fd_globalNavigationgui->sala125, 0);
			fl_set_button(fd_globalNavigationgui->sala126, 0);
			fl_set_button(fd_globalNavigationgui->sala127, 0);
			fl_set_button(fd_globalNavigationgui->sala128, 0);
			fl_set_button(fd_globalNavigationgui->sala129, 0);

			destinationChoosen = TRUE;
		}
	} else if (obj == fd_globalNavigationgui->sala113) {
		if (fl_get_button(obj)==PUSHED) {
			myGlobalDestination.x = 9000.;
			myGlobalDestination.y = 1000.;
			fl_set_button(fd_globalNavigationgui->sala122, 0);
			fl_set_button(fd_globalNavigationgui->sala121, 0);
			fl_set_button(fd_globalNavigationgui->sala120, 0);
			fl_set_button(fd_globalNavigationgui->sala119, 0);
			fl_set_button(fd_globalNavigationgui->sala118, 0);
			fl_set_button(fd_globalNavigationgui->sala117, 0);
			fl_set_button(fd_globalNavigationgui->sala109, 0);
			fl_set_button(fd_globalNavigationgui->sala110, 0);
			fl_set_button(fd_globalNavigationgui->sala111, 0);
			fl_set_button(fd_globalNavigationgui->sala112, 0);
			fl_set_button(fd_globalNavigationgui->sala123, 0);
			fl_set_button(fd_globalNavigationgui->sala114, 0);
			fl_set_button(fd_globalNavigationgui->sala115, 0);
			fl_set_button(fd_globalNavigationgui->sala116, 0);

			fl_set_button(fd_globalNavigationgui->sala136, 0);
			fl_set_button(fd_globalNavigationgui->sala135, 0);
			fl_set_button(fd_globalNavigationgui->sala134, 0);
			fl_set_button(fd_globalNavigationgui->sala133, 0);
			fl_set_button(fd_globalNavigationgui->sala132, 0);
			fl_set_button(fd_globalNavigationgui->sala131, 0);
			fl_set_button(fd_globalNavigationgui->sala130, 0);
			fl_set_button(fd_globalNavigationgui->salaBanios, 0);
			fl_set_button(fd_globalNavigationgui->sala124, 0);
			fl_set_button(fd_globalNavigationgui->sala125, 0);
			fl_set_button(fd_globalNavigationgui->sala126, 0);
			fl_set_button(fd_globalNavigationgui->sala127, 0);
			fl_set_button(fd_globalNavigationgui->sala128, 0);
			fl_set_button(fd_globalNavigationgui->sala129, 0);

			destinationChoosen = TRUE;
		}
	} else if (obj == fd_globalNavigationgui->sala114) {
		if (fl_get_button(obj)==PUSHED) {
			myGlobalDestination.x = 9000.;
			myGlobalDestination.y = -2000.;
			fl_set_button(fd_globalNavigationgui->sala122, 0);
			fl_set_button(fd_globalNavigationgui->sala121, 0);
			fl_set_button(fd_globalNavigationgui->sala120, 0);
			fl_set_button(fd_globalNavigationgui->sala119, 0);
			fl_set_button(fd_globalNavigationgui->sala118, 0);
			fl_set_button(fd_globalNavigationgui->sala117, 0);
			fl_set_button(fd_globalNavigationgui->sala109, 0);
			fl_set_button(fd_globalNavigationgui->sala110, 0);
			fl_set_button(fd_globalNavigationgui->sala111, 0);
			fl_set_button(fd_globalNavigationgui->sala112, 0);
			fl_set_button(fd_globalNavigationgui->sala113, 0);
			fl_set_button(fd_globalNavigationgui->sala123, 0);
			fl_set_button(fd_globalNavigationgui->sala115, 0);
			fl_set_button(fd_globalNavigationgui->sala116, 0);

			fl_set_button(fd_globalNavigationgui->sala136, 0);
			fl_set_button(fd_globalNavigationgui->sala135, 0);
			fl_set_button(fd_globalNavigationgui->sala134, 0);
			fl_set_button(fd_globalNavigationgui->sala133, 0);
			fl_set_button(fd_globalNavigationgui->sala132, 0);
			fl_set_button(fd_globalNavigationgui->sala131, 0);
			fl_set_button(fd_globalNavigationgui->sala130, 0);
			fl_set_button(fd_globalNavigationgui->salaBanios, 0);
			fl_set_button(fd_globalNavigationgui->sala124, 0);
			fl_set_button(fd_globalNavigationgui->sala125, 0);
			fl_set_button(fd_globalNavigationgui->sala126, 0);
			fl_set_button(fd_globalNavigationgui->sala127, 0);
			fl_set_button(fd_globalNavigationgui->sala128, 0);
			fl_set_button(fd_globalNavigationgui->sala129, 0);

			destinationChoosen = TRUE;
		}
	} else if (obj == fd_globalNavigationgui->sala115) {
		if (fl_get_button(obj)==PUSHED) {
			myGlobalDestination.x = 9000.;
			myGlobalDestination.y = -7000.;
			fl_set_button(fd_globalNavigationgui->sala122, 0);
			fl_set_button(fd_globalNavigationgui->sala121, 0);
			fl_set_button(fd_globalNavigationgui->sala120, 0);
			fl_set_button(fd_globalNavigationgui->sala119, 0);
			fl_set_button(fd_globalNavigationgui->sala118, 0);
			fl_set_button(fd_globalNavigationgui->sala117, 0);
			fl_set_button(fd_globalNavigationgui->sala109, 0);
			fl_set_button(fd_globalNavigationgui->sala110, 0);
			fl_set_button(fd_globalNavigationgui->sala111, 0);
			fl_set_button(fd_globalNavigationgui->sala112, 0);
			fl_set_button(fd_globalNavigationgui->sala113, 0);
			fl_set_button(fd_globalNavigationgui->sala114, 0);
			fl_set_button(fd_globalNavigationgui->sala123, 0);
			fl_set_button(fd_globalNavigationgui->sala116, 0);

			fl_set_button(fd_globalNavigationgui->sala136, 0);
			fl_set_button(fd_globalNavigationgui->sala135, 0);
			fl_set_button(fd_globalNavigationgui->sala134, 0);
			fl_set_button(fd_globalNavigationgui->sala133, 0);
			fl_set_button(fd_globalNavigationgui->sala132, 0);
			fl_set_button(fd_globalNavigationgui->sala131, 0);
			fl_set_button(fd_globalNavigationgui->sala130, 0);
			fl_set_button(fd_globalNavigationgui->salaBanios, 0);
			fl_set_button(fd_globalNavigationgui->sala124, 0);
			fl_set_button(fd_globalNavigationgui->sala125, 0);
			fl_set_button(fd_globalNavigationgui->sala126, 0);
			fl_set_button(fd_globalNavigationgui->sala127, 0);
			fl_set_button(fd_globalNavigationgui->sala128, 0);
			fl_set_button(fd_globalNavigationgui->sala129, 0);

			destinationChoosen = TRUE;
		}
	} else if (obj == fd_globalNavigationgui->sala116) {
		if (fl_get_button(obj)==PUSHED) {
			myGlobalDestination.x = 9000.;
			myGlobalDestination.y = -11000.;
			fl_set_button(fd_globalNavigationgui->sala122, 0);
			fl_set_button(fd_globalNavigationgui->sala121, 0);
			fl_set_button(fd_globalNavigationgui->sala120, 0);
			fl_set_button(fd_globalNavigationgui->sala119, 0);
			fl_set_button(fd_globalNavigationgui->sala118, 0);
			fl_set_button(fd_globalNavigationgui->sala117, 0);
			fl_set_button(fd_globalNavigationgui->sala109, 0);
			fl_set_button(fd_globalNavigationgui->sala110, 0);
			fl_set_button(fd_globalNavigationgui->sala111, 0);
			fl_set_button(fd_globalNavigationgui->sala112, 0);
			fl_set_button(fd_globalNavigationgui->sala113, 0);
			fl_set_button(fd_globalNavigationgui->sala114, 0);
			fl_set_button(fd_globalNavigationgui->sala115, 0);
			fl_set_button(fd_globalNavigationgui->sala123, 0);

			fl_set_button(fd_globalNavigationgui->sala136, 0);
			fl_set_button(fd_globalNavigationgui->sala135, 0);
			fl_set_button(fd_globalNavigationgui->sala134, 0);
			fl_set_button(fd_globalNavigationgui->sala133, 0);
			fl_set_button(fd_globalNavigationgui->sala132, 0);
			fl_set_button(fd_globalNavigationgui->sala131, 0);
			fl_set_button(fd_globalNavigationgui->sala130, 0);
			fl_set_button(fd_globalNavigationgui->salaBanios, 0);
			fl_set_button(fd_globalNavigationgui->sala124, 0);
			fl_set_button(fd_globalNavigationgui->sala125, 0);
			fl_set_button(fd_globalNavigationgui->sala126, 0);
			fl_set_button(fd_globalNavigationgui->sala127, 0);
			fl_set_button(fd_globalNavigationgui->sala128, 0);
			fl_set_button(fd_globalNavigationgui->sala129, 0);

			destinationChoosen = TRUE;
		}
	} else if (obj == fd_globalNavigationgui->sala136) {/*** ala izquierda ***/
		if (fl_get_button(obj)==PUSHED) {
			myGlobalDestination.x = 9000.;
			myGlobalDestination.y = 11500.;
			fl_set_button(fd_globalNavigationgui->sala122, 0);
			fl_set_button(fd_globalNavigationgui->sala121, 0);
			fl_set_button(fd_globalNavigationgui->sala120, 0);
			fl_set_button(fd_globalNavigationgui->sala119, 0);
			fl_set_button(fd_globalNavigationgui->sala118, 0);
			fl_set_button(fd_globalNavigationgui->sala117, 0);
			fl_set_button(fd_globalNavigationgui->sala109, 0);
			fl_set_button(fd_globalNavigationgui->sala110, 0);
			fl_set_button(fd_globalNavigationgui->sala111, 0);
			fl_set_button(fd_globalNavigationgui->sala112, 0);
			fl_set_button(fd_globalNavigationgui->sala113, 0);
			fl_set_button(fd_globalNavigationgui->sala114, 0);
			fl_set_button(fd_globalNavigationgui->sala115, 0);
			fl_set_button(fd_globalNavigationgui->sala116, 0);

			fl_set_button(fd_globalNavigationgui->sala123, 0);
			fl_set_button(fd_globalNavigationgui->sala135, 0);
			fl_set_button(fd_globalNavigationgui->sala134, 0);
			fl_set_button(fd_globalNavigationgui->sala133, 0);
			fl_set_button(fd_globalNavigationgui->sala132, 0);
			fl_set_button(fd_globalNavigationgui->sala131, 0);
			fl_set_button(fd_globalNavigationgui->sala130, 0);
			fl_set_button(fd_globalNavigationgui->salaBanios, 0);
			fl_set_button(fd_globalNavigationgui->sala124, 0);
			fl_set_button(fd_globalNavigationgui->sala125, 0);
			fl_set_button(fd_globalNavigationgui->sala126, 0);
			fl_set_button(fd_globalNavigationgui->sala127, 0);
			fl_set_button(fd_globalNavigationgui->sala128, 0);
			fl_set_button(fd_globalNavigationgui->sala129, 0);

			destinationChoosen = TRUE;
		}
	} else if (obj == fd_globalNavigationgui->sala135) {
		if (fl_get_button(obj)==PUSHED) {
			myGlobalDestination.x = 9000.;
			myGlobalDestination.y = 7500.;
			fl_set_button(fd_globalNavigationgui->sala123, 0);
			fl_set_button(fd_globalNavigationgui->sala121, 0);
			fl_set_button(fd_globalNavigationgui->sala120, 0);
			fl_set_button(fd_globalNavigationgui->sala119, 0);
			fl_set_button(fd_globalNavigationgui->sala118, 0);
			fl_set_button(fd_globalNavigationgui->sala117, 0);
			fl_set_button(fd_globalNavigationgui->sala109, 0);
			fl_set_button(fd_globalNavigationgui->sala110, 0);
			fl_set_button(fd_globalNavigationgui->sala111, 0);
			fl_set_button(fd_globalNavigationgui->sala112, 0);
			fl_set_button(fd_globalNavigationgui->sala113, 0);
			fl_set_button(fd_globalNavigationgui->sala114, 0);
			fl_set_button(fd_globalNavigationgui->sala115, 0);
			fl_set_button(fd_globalNavigationgui->sala116, 0);

			fl_set_button(fd_globalNavigationgui->sala136, 0);
			fl_set_button(fd_globalNavigationgui->sala122, 0);
			fl_set_button(fd_globalNavigationgui->sala134, 0);
			fl_set_button(fd_globalNavigationgui->sala133, 0);
			fl_set_button(fd_globalNavigationgui->sala132, 0);
			fl_set_button(fd_globalNavigationgui->sala131, 0);
			fl_set_button(fd_globalNavigationgui->sala130, 0);
			fl_set_button(fd_globalNavigationgui->salaBanios, 0);
			fl_set_button(fd_globalNavigationgui->sala124, 0);
			fl_set_button(fd_globalNavigationgui->sala125, 0);
			fl_set_button(fd_globalNavigationgui->sala126, 0);
			fl_set_button(fd_globalNavigationgui->sala127, 0);
			fl_set_button(fd_globalNavigationgui->sala128, 0);
			fl_set_button(fd_globalNavigationgui->sala129, 0);

			destinationChoosen = TRUE;
		}
	} else if (obj == fd_globalNavigationgui->sala134) {
		if (fl_get_button(obj)==PUSHED) {
			myGlobalDestination.x = 9000.;
			myGlobalDestination.y = 4500.;
			fl_set_button(fd_globalNavigationgui->sala122, 0);
			fl_set_button(fd_globalNavigationgui->sala123, 0);
			fl_set_button(fd_globalNavigationgui->sala120, 0);
			fl_set_button(fd_globalNavigationgui->sala119, 0);
			fl_set_button(fd_globalNavigationgui->sala118, 0);
			fl_set_button(fd_globalNavigationgui->sala117, 0);
			fl_set_button(fd_globalNavigationgui->sala109, 0);
			fl_set_button(fd_globalNavigationgui->sala110, 0);
			fl_set_button(fd_globalNavigationgui->sala111, 0);
			fl_set_button(fd_globalNavigationgui->sala112, 0);
			fl_set_button(fd_globalNavigationgui->sala113, 0);
			fl_set_button(fd_globalNavigationgui->sala114, 0);
			fl_set_button(fd_globalNavigationgui->sala115, 0);
			fl_set_button(fd_globalNavigationgui->sala116, 0);

			fl_set_button(fd_globalNavigationgui->sala136, 0);
			fl_set_button(fd_globalNavigationgui->sala135, 0);
			fl_set_button(fd_globalNavigationgui->sala121, 0);
			fl_set_button(fd_globalNavigationgui->sala133, 0);
			fl_set_button(fd_globalNavigationgui->sala132, 0);
			fl_set_button(fd_globalNavigationgui->sala131, 0);
			fl_set_button(fd_globalNavigationgui->sala130, 0);
			fl_set_button(fd_globalNavigationgui->salaBanios, 0);
			fl_set_button(fd_globalNavigationgui->sala124, 0);
			fl_set_button(fd_globalNavigationgui->sala125, 0);
			fl_set_button(fd_globalNavigationgui->sala126, 0);
			fl_set_button(fd_globalNavigationgui->sala127, 0);
			fl_set_button(fd_globalNavigationgui->sala128, 0);
			fl_set_button(fd_globalNavigationgui->sala129, 0);

			destinationChoosen = TRUE;
		}
	} else if (obj == fd_globalNavigationgui->sala133) {
		if (fl_get_button(obj)==PUSHED) {
			myGlobalDestination.x = 9000.;
			myGlobalDestination.y = 1000.;
			fl_set_button(fd_globalNavigationgui->sala122, 0);
			fl_set_button(fd_globalNavigationgui->sala121, 0);
			fl_set_button(fd_globalNavigationgui->sala123, 0);
			fl_set_button(fd_globalNavigationgui->sala119, 0);
			fl_set_button(fd_globalNavigationgui->sala118, 0);
			fl_set_button(fd_globalNavigationgui->sala117, 0);
			fl_set_button(fd_globalNavigationgui->sala109, 0);
			fl_set_button(fd_globalNavigationgui->sala110, 0);
			fl_set_button(fd_globalNavigationgui->sala111, 0);
			fl_set_button(fd_globalNavigationgui->sala112, 0);
			fl_set_button(fd_globalNavigationgui->sala113, 0);
			fl_set_button(fd_globalNavigationgui->sala114, 0);
			fl_set_button(fd_globalNavigationgui->sala115, 0);
			fl_set_button(fd_globalNavigationgui->sala116, 0);

			fl_set_button(fd_globalNavigationgui->sala136, 0);
			fl_set_button(fd_globalNavigationgui->sala135, 0);
			fl_set_button(fd_globalNavigationgui->sala134, 0);
			fl_set_button(fd_globalNavigationgui->sala120, 0);
			fl_set_button(fd_globalNavigationgui->sala132, 0);
			fl_set_button(fd_globalNavigationgui->sala131, 0);
			fl_set_button(fd_globalNavigationgui->sala130, 0);
			fl_set_button(fd_globalNavigationgui->salaBanios, 0);
			fl_set_button(fd_globalNavigationgui->sala124, 0);
			fl_set_button(fd_globalNavigationgui->sala125, 0);
			fl_set_button(fd_globalNavigationgui->sala126, 0);
			fl_set_button(fd_globalNavigationgui->sala127, 0);
			fl_set_button(fd_globalNavigationgui->sala128, 0);
			fl_set_button(fd_globalNavigationgui->sala129, 0);

			destinationChoosen = TRUE;
		}
	} else if (obj == fd_globalNavigationgui->sala132) {
		if (fl_get_button(obj)==PUSHED) {
			myGlobalDestination.x = 9000.;
			myGlobalDestination.y = -2000.;
			fl_set_button(fd_globalNavigationgui->sala122, 0);
			fl_set_button(fd_globalNavigationgui->sala121, 0);
			fl_set_button(fd_globalNavigationgui->sala120, 0);
			fl_set_button(fd_globalNavigationgui->sala123, 0);
			fl_set_button(fd_globalNavigationgui->sala118, 0);
			fl_set_button(fd_globalNavigationgui->sala117, 0);
			fl_set_button(fd_globalNavigationgui->sala109, 0);
			fl_set_button(fd_globalNavigationgui->sala110, 0);
			fl_set_button(fd_globalNavigationgui->sala111, 0);
			fl_set_button(fd_globalNavigationgui->sala112, 0);
			fl_set_button(fd_globalNavigationgui->sala113, 0);
			fl_set_button(fd_globalNavigationgui->sala114, 0);
			fl_set_button(fd_globalNavigationgui->sala115, 0);
			fl_set_button(fd_globalNavigationgui->sala116, 0);

			fl_set_button(fd_globalNavigationgui->sala136, 0);
			fl_set_button(fd_globalNavigationgui->sala135, 0);
			fl_set_button(fd_globalNavigationgui->sala134, 0);
			fl_set_button(fd_globalNavigationgui->sala133, 0);
			fl_set_button(fd_globalNavigationgui->sala119, 0);
			fl_set_button(fd_globalNavigationgui->sala131, 0);
			fl_set_button(fd_globalNavigationgui->sala130, 0);
			fl_set_button(fd_globalNavigationgui->salaBanios, 0);
			fl_set_button(fd_globalNavigationgui->sala124, 0);
			fl_set_button(fd_globalNavigationgui->sala125, 0);
			fl_set_button(fd_globalNavigationgui->sala126, 0);
			fl_set_button(fd_globalNavigationgui->sala127, 0);
			fl_set_button(fd_globalNavigationgui->sala128, 0);
			fl_set_button(fd_globalNavigationgui->sala129, 0);

			destinationChoosen = TRUE;
		}
	} else if (obj == fd_globalNavigationgui->sala131) {
		if (fl_get_button(obj)==PUSHED) {
			myGlobalDestination.x = 9000.;
			myGlobalDestination.y = -5500.;
			fl_set_button(fd_globalNavigationgui->sala122, 0);
			fl_set_button(fd_globalNavigationgui->sala121, 0);
			fl_set_button(fd_globalNavigationgui->sala120, 0);
			fl_set_button(fd_globalNavigationgui->sala119, 0);
			fl_set_button(fd_globalNavigationgui->sala123, 0);
			fl_set_button(fd_globalNavigationgui->sala117, 0);
			fl_set_button(fd_globalNavigationgui->sala109, 0);
			fl_set_button(fd_globalNavigationgui->sala110, 0);
			fl_set_button(fd_globalNavigationgui->sala111, 0);
			fl_set_button(fd_globalNavigationgui->sala112, 0);
			fl_set_button(fd_globalNavigationgui->sala113, 0);
			fl_set_button(fd_globalNavigationgui->sala114, 0);
			fl_set_button(fd_globalNavigationgui->sala115, 0);
			fl_set_button(fd_globalNavigationgui->sala116, 0);

			fl_set_button(fd_globalNavigationgui->sala136, 0);
			fl_set_button(fd_globalNavigationgui->sala135, 0);
			fl_set_button(fd_globalNavigationgui->sala134, 0);
			fl_set_button(fd_globalNavigationgui->sala133, 0);
			fl_set_button(fd_globalNavigationgui->sala132, 0);
			fl_set_button(fd_globalNavigationgui->sala118, 0);
			fl_set_button(fd_globalNavigationgui->sala130, 0);
			fl_set_button(fd_globalNavigationgui->salaBanios, 0);
			fl_set_button(fd_globalNavigationgui->sala124, 0);
			fl_set_button(fd_globalNavigationgui->sala125, 0);
			fl_set_button(fd_globalNavigationgui->sala126, 0);
			fl_set_button(fd_globalNavigationgui->sala127, 0);
			fl_set_button(fd_globalNavigationgui->sala128, 0);
			fl_set_button(fd_globalNavigationgui->sala129, 0);

			destinationChoosen = TRUE;
		}
	} else if (obj == fd_globalNavigationgui->sala130) {
		if (fl_get_button(obj)==PUSHED) {
			myGlobalDestination.x = 9000.;
			myGlobalDestination.y = -12500.;
			fl_set_button(fd_globalNavigationgui->sala122, 0);
			fl_set_button(fd_globalNavigationgui->sala121, 0);
			fl_set_button(fd_globalNavigationgui->sala120, 0);
			fl_set_button(fd_globalNavigationgui->sala119, 0);
			fl_set_button(fd_globalNavigationgui->sala118, 0);
			fl_set_button(fd_globalNavigationgui->sala123, 0);
			fl_set_button(fd_globalNavigationgui->sala109, 0);
			fl_set_button(fd_globalNavigationgui->sala110, 0);
			fl_set_button(fd_globalNavigationgui->sala111, 0);
			fl_set_button(fd_globalNavigationgui->sala112, 0);
			fl_set_button(fd_globalNavigationgui->sala113, 0);
			fl_set_button(fd_globalNavigationgui->sala114, 0);
			fl_set_button(fd_globalNavigationgui->sala115, 0);
			fl_set_button(fd_globalNavigationgui->sala116, 0);

			fl_set_button(fd_globalNavigationgui->sala136, 0);
			fl_set_button(fd_globalNavigationgui->sala135, 0);
			fl_set_button(fd_globalNavigationgui->sala134, 0);
			fl_set_button(fd_globalNavigationgui->sala133, 0);
			fl_set_button(fd_globalNavigationgui->sala132, 0);
			fl_set_button(fd_globalNavigationgui->sala131, 0);
			fl_set_button(fd_globalNavigationgui->sala117, 0);
			fl_set_button(fd_globalNavigationgui->salaBanios, 0);
			fl_set_button(fd_globalNavigationgui->sala124, 0);
			fl_set_button(fd_globalNavigationgui->sala125, 0);
			fl_set_button(fd_globalNavigationgui->sala126, 0);
			fl_set_button(fd_globalNavigationgui->sala127, 0);
			fl_set_button(fd_globalNavigationgui->sala128, 0);
			fl_set_button(fd_globalNavigationgui->sala129, 0);

			destinationChoosen = TRUE;
		}
	} else if (obj == fd_globalNavigationgui->salaBanios) {
		if (fl_get_button(obj)==PUSHED) {
			myGlobalDestination.x = 9000.;
			myGlobalDestination.y = 14000.;
			fl_set_button(fd_globalNavigationgui->sala122, 0);
			fl_set_button(fd_globalNavigationgui->sala121, 0);
			fl_set_button(fd_globalNavigationgui->sala120, 0);
			fl_set_button(fd_globalNavigationgui->sala119, 0);
			fl_set_button(fd_globalNavigationgui->sala118, 0);
			fl_set_button(fd_globalNavigationgui->sala117, 0);
			fl_set_button(fd_globalNavigationgui->sala123, 0);
			fl_set_button(fd_globalNavigationgui->sala110, 0);
			fl_set_button(fd_globalNavigationgui->sala111, 0);
			fl_set_button(fd_globalNavigationgui->sala112, 0);
			fl_set_button(fd_globalNavigationgui->sala113, 0);
			fl_set_button(fd_globalNavigationgui->sala114, 0);
			fl_set_button(fd_globalNavigationgui->sala115, 0);
			fl_set_button(fd_globalNavigationgui->sala116, 0);

			fl_set_button(fd_globalNavigationgui->sala136, 0);
			fl_set_button(fd_globalNavigationgui->sala135, 0);
			fl_set_button(fd_globalNavigationgui->sala134, 0);
			fl_set_button(fd_globalNavigationgui->sala133, 0);
			fl_set_button(fd_globalNavigationgui->sala132, 0);
			fl_set_button(fd_globalNavigationgui->sala131, 0);
			fl_set_button(fd_globalNavigationgui->sala130, 0);
			fl_set_button(fd_globalNavigationgui->sala109, 0);
			fl_set_button(fd_globalNavigationgui->sala124, 0);
			fl_set_button(fd_globalNavigationgui->sala125, 0);
			fl_set_button(fd_globalNavigationgui->sala126, 0);
			fl_set_button(fd_globalNavigationgui->sala127, 0);
			fl_set_button(fd_globalNavigationgui->sala128, 0);
			fl_set_button(fd_globalNavigationgui->sala129, 0);

			destinationChoosen = TRUE;
		}
	} else if (obj == fd_globalNavigationgui->sala124) {
		if (fl_get_button(obj)==PUSHED) {
			myGlobalDestination.x = 9000.;
			myGlobalDestination.y = 10500.;
			fl_set_button(fd_globalNavigationgui->sala122, 0);
			fl_set_button(fd_globalNavigationgui->sala121, 0);
			fl_set_button(fd_globalNavigationgui->sala120, 0);
			fl_set_button(fd_globalNavigationgui->sala119, 0);
			fl_set_button(fd_globalNavigationgui->sala118, 0);
			fl_set_button(fd_globalNavigationgui->sala117, 0);
			fl_set_button(fd_globalNavigationgui->sala109, 0);
			fl_set_button(fd_globalNavigationgui->sala123, 0);
			fl_set_button(fd_globalNavigationgui->sala111, 0);
			fl_set_button(fd_globalNavigationgui->sala112, 0);
			fl_set_button(fd_globalNavigationgui->sala113, 0);
			fl_set_button(fd_globalNavigationgui->sala114, 0);
			fl_set_button(fd_globalNavigationgui->sala115, 0);
			fl_set_button(fd_globalNavigationgui->sala116, 0);

			fl_set_button(fd_globalNavigationgui->sala136, 0);
			fl_set_button(fd_globalNavigationgui->sala135, 0);
			fl_set_button(fd_globalNavigationgui->sala134, 0);
			fl_set_button(fd_globalNavigationgui->sala133, 0);
			fl_set_button(fd_globalNavigationgui->sala132, 0);
			fl_set_button(fd_globalNavigationgui->sala131, 0);
			fl_set_button(fd_globalNavigationgui->sala130, 0);
			fl_set_button(fd_globalNavigationgui->salaBanios, 0);
			fl_set_button(fd_globalNavigationgui->sala110, 0);
			fl_set_button(fd_globalNavigationgui->sala125, 0);
			fl_set_button(fd_globalNavigationgui->sala126, 0);
			fl_set_button(fd_globalNavigationgui->sala127, 0);
			fl_set_button(fd_globalNavigationgui->sala128, 0);
			fl_set_button(fd_globalNavigationgui->sala129, 0);

			destinationChoosen = TRUE;
		}
	} else if (obj == fd_globalNavigationgui->sala125) {
		if (fl_get_button(obj)==PUSHED) {
			myGlobalDestination.x = 9000.;
			myGlobalDestination.y = 7500.;
			fl_set_button(fd_globalNavigationgui->sala122, 0);
			fl_set_button(fd_globalNavigationgui->sala121, 0);
			fl_set_button(fd_globalNavigationgui->sala120, 0);
			fl_set_button(fd_globalNavigationgui->sala119, 0);
			fl_set_button(fd_globalNavigationgui->sala118, 0);
			fl_set_button(fd_globalNavigationgui->sala117, 0);
			fl_set_button(fd_globalNavigationgui->sala109, 0);
			fl_set_button(fd_globalNavigationgui->sala110, 0);
			fl_set_button(fd_globalNavigationgui->sala123, 0);
			fl_set_button(fd_globalNavigationgui->sala112, 0);
			fl_set_button(fd_globalNavigationgui->sala113, 0);
			fl_set_button(fd_globalNavigationgui->sala114, 0);
			fl_set_button(fd_globalNavigationgui->sala115, 0);
			fl_set_button(fd_globalNavigationgui->sala116, 0);

			fl_set_button(fd_globalNavigationgui->sala136, 0);
			fl_set_button(fd_globalNavigationgui->sala135, 0);
			fl_set_button(fd_globalNavigationgui->sala134, 0);
			fl_set_button(fd_globalNavigationgui->sala133, 0);
			fl_set_button(fd_globalNavigationgui->sala132, 0);
			fl_set_button(fd_globalNavigationgui->sala131, 0);
			fl_set_button(fd_globalNavigationgui->sala130, 0);
			fl_set_button(fd_globalNavigationgui->salaBanios, 0);
			fl_set_button(fd_globalNavigationgui->sala124, 0);
			fl_set_button(fd_globalNavigationgui->sala111, 0);
			fl_set_button(fd_globalNavigationgui->sala126, 0);
			fl_set_button(fd_globalNavigationgui->sala127, 0);
			fl_set_button(fd_globalNavigationgui->sala128, 0);
			fl_set_button(fd_globalNavigationgui->sala129, 0);

			destinationChoosen = TRUE;
		}
	} else if (obj == fd_globalNavigationgui->sala126) {
		if (fl_get_button(obj)==PUSHED) {
			myGlobalDestination.x = 9000.;
			myGlobalDestination.y = 4500.;
			fl_set_button(fd_globalNavigationgui->sala122, 0);
			fl_set_button(fd_globalNavigationgui->sala121, 0);
			fl_set_button(fd_globalNavigationgui->sala120, 0);
			fl_set_button(fd_globalNavigationgui->sala119, 0);
			fl_set_button(fd_globalNavigationgui->sala118, 0);
			fl_set_button(fd_globalNavigationgui->sala117, 0);
			fl_set_button(fd_globalNavigationgui->sala109, 0);
			fl_set_button(fd_globalNavigationgui->sala110, 0);
			fl_set_button(fd_globalNavigationgui->sala111, 0);
			fl_set_button(fd_globalNavigationgui->sala123, 0);
			fl_set_button(fd_globalNavigationgui->sala113, 0);
			fl_set_button(fd_globalNavigationgui->sala114, 0);
			fl_set_button(fd_globalNavigationgui->sala115, 0);
			fl_set_button(fd_globalNavigationgui->sala116, 0);

			fl_set_button(fd_globalNavigationgui->sala136, 0);
			fl_set_button(fd_globalNavigationgui->sala135, 0);
			fl_set_button(fd_globalNavigationgui->sala134, 0);
			fl_set_button(fd_globalNavigationgui->sala133, 0);
			fl_set_button(fd_globalNavigationgui->sala132, 0);
			fl_set_button(fd_globalNavigationgui->sala131, 0);
			fl_set_button(fd_globalNavigationgui->sala130, 0);
			fl_set_button(fd_globalNavigationgui->salaBanios, 0);
			fl_set_button(fd_globalNavigationgui->sala124, 0);
			fl_set_button(fd_globalNavigationgui->sala125, 0);
			fl_set_button(fd_globalNavigationgui->sala112, 0);
			fl_set_button(fd_globalNavigationgui->sala127, 0);
			fl_set_button(fd_globalNavigationgui->sala128, 0);
			fl_set_button(fd_globalNavigationgui->sala129, 0);

			destinationChoosen = TRUE;
		}
	} else if (obj == fd_globalNavigationgui->sala127) {
		if (fl_get_button(obj)==PUSHED) {
			myGlobalDestination.x = 9000.;
			myGlobalDestination.y = 1000.;
			fl_set_button(fd_globalNavigationgui->sala122, 0);
			fl_set_button(fd_globalNavigationgui->sala121, 0);
			fl_set_button(fd_globalNavigationgui->sala120, 0);
			fl_set_button(fd_globalNavigationgui->sala119, 0);
			fl_set_button(fd_globalNavigationgui->sala118, 0);
			fl_set_button(fd_globalNavigationgui->sala117, 0);
			fl_set_button(fd_globalNavigationgui->sala109, 0);
			fl_set_button(fd_globalNavigationgui->sala110, 0);
			fl_set_button(fd_globalNavigationgui->sala111, 0);
			fl_set_button(fd_globalNavigationgui->sala112, 0);
			fl_set_button(fd_globalNavigationgui->sala123, 0);
			fl_set_button(fd_globalNavigationgui->sala114, 0);
			fl_set_button(fd_globalNavigationgui->sala115, 0);
			fl_set_button(fd_globalNavigationgui->sala116, 0);

			fl_set_button(fd_globalNavigationgui->sala136, 0);
			fl_set_button(fd_globalNavigationgui->sala135, 0);
			fl_set_button(fd_globalNavigationgui->sala134, 0);
			fl_set_button(fd_globalNavigationgui->sala133, 0);
			fl_set_button(fd_globalNavigationgui->sala132, 0);
			fl_set_button(fd_globalNavigationgui->sala131, 0);
			fl_set_button(fd_globalNavigationgui->sala130, 0);
			fl_set_button(fd_globalNavigationgui->salaBanios, 0);
			fl_set_button(fd_globalNavigationgui->sala124, 0);
			fl_set_button(fd_globalNavigationgui->sala125, 0);
			fl_set_button(fd_globalNavigationgui->sala126, 0);
			fl_set_button(fd_globalNavigationgui->sala113, 0);
			fl_set_button(fd_globalNavigationgui->sala128, 0);
			fl_set_button(fd_globalNavigationgui->sala129, 0);

			destinationChoosen = TRUE;
		}
	} else if (obj == fd_globalNavigationgui->sala128) {
		if (fl_get_button(obj)==PUSHED) {
			myGlobalDestination.x = 9000.;
			myGlobalDestination.y = -2000.;
			fl_set_button(fd_globalNavigationgui->sala122, 0);
			fl_set_button(fd_globalNavigationgui->sala121, 0);
			fl_set_button(fd_globalNavigationgui->sala120, 0);
			fl_set_button(fd_globalNavigationgui->sala119, 0);
			fl_set_button(fd_globalNavigationgui->sala118, 0);
			fl_set_button(fd_globalNavigationgui->sala117, 0);
			fl_set_button(fd_globalNavigationgui->sala109, 0);
			fl_set_button(fd_globalNavigationgui->sala110, 0);
			fl_set_button(fd_globalNavigationgui->sala111, 0);
			fl_set_button(fd_globalNavigationgui->sala112, 0);
			fl_set_button(fd_globalNavigationgui->sala113, 0);
			fl_set_button(fd_globalNavigationgui->sala123, 0);
			fl_set_button(fd_globalNavigationgui->sala115, 0);
			fl_set_button(fd_globalNavigationgui->sala116, 0);

			fl_set_button(fd_globalNavigationgui->sala136, 0);
			fl_set_button(fd_globalNavigationgui->sala135, 0);
			fl_set_button(fd_globalNavigationgui->sala134, 0);
			fl_set_button(fd_globalNavigationgui->sala133, 0);
			fl_set_button(fd_globalNavigationgui->sala132, 0);
			fl_set_button(fd_globalNavigationgui->sala131, 0);
			fl_set_button(fd_globalNavigationgui->sala130, 0);
			fl_set_button(fd_globalNavigationgui->salaBanios, 0);
			fl_set_button(fd_globalNavigationgui->sala124, 0);
			fl_set_button(fd_globalNavigationgui->sala125, 0);
			fl_set_button(fd_globalNavigationgui->sala126, 0);
			fl_set_button(fd_globalNavigationgui->sala127, 0);
			fl_set_button(fd_globalNavigationgui->sala114, 0);
			fl_set_button(fd_globalNavigationgui->sala129, 0);

			destinationChoosen = TRUE;
		}
	} else if (obj == fd_globalNavigationgui->sala129) {
		if (fl_get_button(obj)==PUSHED) {
			myGlobalDestination.x = 9000.;
			myGlobalDestination.y = -7000.;
			fl_set_button(fd_globalNavigationgui->sala122, 0);
			fl_set_button(fd_globalNavigationgui->sala121, 0);
			fl_set_button(fd_globalNavigationgui->sala120, 0);
			fl_set_button(fd_globalNavigationgui->sala119, 0);
			fl_set_button(fd_globalNavigationgui->sala118, 0);
			fl_set_button(fd_globalNavigationgui->sala117, 0);
			fl_set_button(fd_globalNavigationgui->sala109, 0);
			fl_set_button(fd_globalNavigationgui->sala110, 0);
			fl_set_button(fd_globalNavigationgui->sala111, 0);
			fl_set_button(fd_globalNavigationgui->sala112, 0);
			fl_set_button(fd_globalNavigationgui->sala113, 0);
			fl_set_button(fd_globalNavigationgui->sala114, 0);
			fl_set_button(fd_globalNavigationgui->sala123, 0);
			fl_set_button(fd_globalNavigationgui->sala116, 0);

			fl_set_button(fd_globalNavigationgui->sala136, 0);
			fl_set_button(fd_globalNavigationgui->sala135, 0);
			fl_set_button(fd_globalNavigationgui->sala134, 0);
			fl_set_button(fd_globalNavigationgui->sala133, 0);
			fl_set_button(fd_globalNavigationgui->sala132, 0);
			fl_set_button(fd_globalNavigationgui->sala131, 0);
			fl_set_button(fd_globalNavigationgui->sala130, 0);
			fl_set_button(fd_globalNavigationgui->salaBanios, 0);
			fl_set_button(fd_globalNavigationgui->sala124, 0);
			fl_set_button(fd_globalNavigationgui->sala125, 0);
			fl_set_button(fd_globalNavigationgui->sala126, 0);
			fl_set_button(fd_globalNavigationgui->sala127, 0);
			fl_set_button(fd_globalNavigationgui->sala128, 0);
			fl_set_button(fd_globalNavigationgui->sala115, 0);

			destinationChoosen = TRUE;
		}
	}
}

static int InitOGL(FL_OBJECT *ob, Window win,int w,int h, XEvent *xev, void *ud)
 /* Inicializa OpenGL con los parametros que diran como se visualiza. */
{
	glViewport(0,0,(GLint)w,(GLint)h);  
	glDrawBuffer(GL_BACK);
	glClearColor(0.6f, 0.8f, 1.0f, 0.0f);  
	glClearDepth(1.0);                           /* Enables Clearing Of The Depth Buffer */
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
	glEnable(GL_TEXTURE_2D);     /* Enable Texture Mapping */
	glEnable (GL_AUTO_NORMAL);
	glEnable (GL_NORMALIZE);  
	glEnable(GL_DEPTH_TEST);     /* Enables Depth Testing */
	glDepthFunc(GL_LESS);  
	glShadeModel(GL_SMOOTH);     /* Enables Smooth Color Shading */
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	return 0;
}

void initConfiguration () {
  float r,lati,longi;

	if (!flashImage) {
	  if (foa_mode){
	    longi=2*PI*mouse_on_canvas.x/360.;
	    lati=2*PI*mouse_on_canvas.y/360.;
	    r=5000;
	    virtualcam.foa.X=r*cos(lati)*cos(longi);
	    virtualcam.foa.Y=r*cos(lati)*sin(longi);
	    virtualcam.foa.Z=r*sin(lati);
	  }
	  
	  if (cam_mode) {
	    longi=2*PI*mouse_on_canvas.x/360.;
	    lati=2*PI*mouse_on_canvas.y/360.;
	    
	    virtualcam.position.X=radius*cos(lati)*cos(longi);
	    virtualcam.position.Y=radius*cos(lati)*sin(longi);
	    virtualcam.position.Z=radius*sin(lati);
	    
	    virtualcam.foa.X=0;
	    virtualcam.foa.Y=0;
	    virtualcam.foa.Z=0;
	  }
	}
  
  fl_activate_glcanvas(fd_globalNavigationgui->canvasGL);
  /* Set the OpenGL state machine to the right context for this display */
  /* reset of the depth and color buffers */
  InitOGL(fd_globalNavigationgui->canvasGL, FL_ObjWin(fd_globalNavigationgui->canvasGL),fd_globalNavigationgui->canvasGL->w,fd_globalNavigationgui->canvasGL->h,NULL,NULL);
    
  /* Virtual camera */
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity(); 

  /* perspective projection. intrinsic parameters + frustrum */
  gluPerspective(45.,(GLfloat)640/(GLfloat)480,1.0,100000.0);
  /* extrinsic parameters */
  gluLookAt(virtualcam.position.X,virtualcam.position.Y,virtualcam.position.Z,virtualcam.foa.X,virtualcam.foa.Y,virtualcam.foa.Z,0.,0.,1.);

	glEnable (GL_CULL_FACE); // solo veremos una cara de los planos, para el "efecto transparencia"
	glCullFace (GL_FRONT);
}

void globalNavigation_guidisplay() {
	char text[80]="";
	static float k=0;
	int i;
	Tvoxel aa,bb;
	static XPoint targetgraf;
	XPoint a,b;

	/******************* Dibujamos en OpenGL *********************/
	initConfiguration ();

	drawDepartamental ();
	drawGradient ();
	drawPioneerRobot ();

	glXSwapBuffers(fl_display, fl_get_canvas_id(fd_globalNavigationgui->canvasGL));


	/************************Fin de OpenGL************************

	// slow refresh of the complete globalNavigation gui, needed because incremental refresh misses window occlusions **
	if (globalNavigation_iteracion_display*globalNavigation_cycle>FORCED_REFRESH) {
		globalNavigation_iteracion_display=0;
		globalNavigation_visual_refresh=TRUE;
	}
	else globalNavigation_iteracion_display++;

	k=k+1.;
	sprintf(text,"%.1f",k);

	fl_winset(globalNavigation_canvas_win); 

	if (globalNavigation_visual_refresh==TRUE)	{
		fl_rectbound(0,0,globalNavigation_width,globalNavigation_height,FL_WHITE);   
		XFlush(display);
	}

	// Pinto los nuevos puntos de la REJILLA GRADIENTE GPP LOCAL
	if ((((globalNavigation_display_state&DISPLAY_GRADIENTGRID)!=0)&&(globalNavigation_visual_refresh==FALSE)&&(refreshCounter%REFRESH_TIMER==0)) || (visual_delete_globalNavigation_grid==TRUE)) {
		forcedGradientRefresh = FALSE;
		drawRejillaGradienteGPP ();
	}

	// Pinto los nuevos puntos de la REJILLA DE MEMORIA GPP LOCAL
	if ((((globalNavigation_display_state&DISPLAY_OCCUPANCYGRID)!=0)&&(globalNavigation_visual_refresh==FALSE)&&(refreshCounter%REFRESH_TIMER==0)) || (visual_delete_globalNavigation_grid==TRUE)) {
		drawRejillaGPP (); // compararemos y sólo dibujaremos los "novedosos"
	}

	// VISUALIZACION DE LA ESTELA DEL ROBOT **
	numEstelas=0;
	for	(i=0;i<tamMemoria/2;i++) {
			xy2globalNavigationcanvas(estelaRecorrido[i].pos,&coordGrafEstela[numEstelas]);
			numEstelas++;
			if (estelaRecorrido[i].color == VFF_COLOR)
				fl_set_foreground(globalNavigationgui_gc,FL_CYAN);
			else
				fl_set_foreground(globalNavigationgui_gc,FL_MAGENTA);
			XDrawPoint(display,globalNavigation_canvas_win,globalNavigationgui_gc,coordGrafEstela[numEstelas].x,coordGrafEstela[numEstelas].y);
	}

	// VISUALIZACION: PINTAR O BORRAR DE EL PROPIO ROBOT.
	if ((((globalNavigation_display_state&DISPLAY_ROBOT)!=0) &&(globalNavigation_visual_refresh==FALSE))
	|| (visual_delete_globalNavigation_ego==TRUE))	{  
		fl_set_foreground(globalNavigationgui_gc,FL_WHITE);

		for(i=0;i<numglobalNavigation_ego;i++) 
			XDrawLine(display,globalNavigation_canvas_win,globalNavigationgui_gc,globalNavigation_ego[i].x,globalNavigation_ego[i].y,globalNavigation_ego[i+1].x,globalNavigation_ego[i+1].y);
	}

	if ((globalNavigation_display_state&DISPLAY_ROBOT)!=0) {
		fl_set_foreground(globalNavigationgui_gc,FL_MAGENTA);
		us2xy(15,0.,0.,&aa,&myencoders[0]);
		xy2globalNavigationcanvas(aa,&globalNavigation_ego[0]);
		us2xy(3,0.,0.,&aa,&myencoders[0]);
		xy2globalNavigationcanvas(aa,&globalNavigation_ego[1]);
		us2xy(4,0.,0.,&aa,&myencoders[0]);
		xy2globalNavigationcanvas(aa,&globalNavigation_ego[2]);
		us2xy(8,0.,0.,&aa,&myencoders[0]);
		xy2globalNavigationcanvas(aa,&globalNavigation_ego[3]);
		us2xy(15,0.,0.,&aa,&myencoders[0]);
		xy2globalNavigationcanvas(aa,&globalNavigation_ego[EGOMAX-1]);
		for(i=0;i<NUM_SONARS;i++) {
			us2xy((15+i)%NUM_SONARS,0.,0.,&aa,&myencoders[0]);
			xy2globalNavigationcanvas(aa,&globalNavigation_ego[i+4]);       
		}

		// pinto los nuevos **
		numglobalNavigation_ego=EGOMAX-1;
		for (i=0;i<numglobalNavigation_ego;i++) 
			XDrawLine(display,globalNavigation_canvas_win,globalNavigationgui_gc,globalNavigation_ego[i].x,globalNavigation_ego[i].y,globalNavigation_ego[i+1].x,globalNavigation_ego[i+1].y);

		fl_set_foreground(globalNavigationgui_gc,FL_BLUE);
		XDrawPoints(display,globalNavigation_canvas_win,globalNavigationgui_gc,GraphicGlobalDestination,9,CoordModeOrigin);		
	}

	// clear all flags. If they were set at the beginning, they have been already used in this iteration **
	globalNavigation_visual_refresh=FALSE;
	visual_delete_globalNavigation_us=FALSE; 
	visual_delete_globalNavigation_laser=FALSE; 
	visual_delete_globalNavigation_ego=FALSE;
	visual_delete_globalNavigation_grid = FALSE;

	refreshCounter ++;*/
}

void globalNavigation_guisuspend_aux(void)
{
  /* to make a safety stop when the robot is being teleoperated from GUI */
  mydelete_buttonscallback(globalNavigation_guibuttons);
  mydelete_displaycallback(globalNavigation_guidisplay);
  fl_hide_form(fd_globalNavigationgui->globalNavigationgui);
}

void globalNavigation_hide(){
  static callback fn=NULL;
  if (fn==NULL){
    if ((fn=(callback)myimport ("graphics_xforms", "suspend_callback"))!=NULL){
      fn ((gui_function)globalNavigation_guisuspend_aux);
    }
  }
  else{
    fn ((gui_function)globalNavigation_guisuspend_aux);
  }
}

static int image_displaysetup () {
	/* Inicializa las ventanas, la paleta de colores y memoria compartida para visualizacion*/ 
	XGCValues gc_values;
	XWindowAttributes win_attributes;
	XColor nuevocolor;
	int pixelNum, numCols;
	int allocated_colors=0, non_allocated_colors=0;

	globalNavigationgui_win= FL_ObjWin(fd_globalNavigationgui->ventanaA);
	XGetWindowAttributes(display, globalNavigationgui_win, &win_attributes);
	XMapWindow(display, globalNavigationgui_win);

	gc_values.graphics_exposures = False;
	globalNavigationgui_gc = XCreateGC(display, globalNavigationgui_win, GCGraphicsExposures, &gc_values);  

	return win_attributes.depth;
}

void globalNavigation_guiresume_aux(void)
{
  static int k=0;
  XGCValues gc_values;

  if (k==0) /* not initialized */
    {
      k++;
      /* Coord del sistema odometrico respecto del visual */
      globalNavigation_odometrico[0]=0.;
      globalNavigation_odometrico[1]=0.;
      globalNavigation_odometrico[2]=0.;
      globalNavigation_odometrico[3]= cos(0.);
      globalNavigation_odometrico[4]= sin(0.);

      globalNavigation_display_state = globalNavigation_display_state | DISPLAY_ROBOT;
			globalNavigation_display_state = globalNavigation_display_state | DISPLAY_GRADIENTGRID;
			globalNavigation_display_state = globalNavigation_display_state | DISPLAY_OCCUPANCYGRID;

      fd_globalNavigationgui = create_form_globalNavigationgui();
      fl_set_form_position(fd_globalNavigationgui->globalNavigationgui,400,50);
      fl_show_form(fd_globalNavigationgui->globalNavigationgui,FL_PLACE_POSITION,FL_FULLBORDER,"GUIA MUSEO. AONDE LE LLEVO?");
      
      /* canvas handlers */
			fl_add_canvas_handler(fd_globalNavigationgui->canvasGL,Expose,InitOGL,0);
			fl_add_canvas_handler(fd_globalNavigationgui->canvasGL,MotionNotify,motion_event,0);
			fl_add_canvas_handler(fd_globalNavigationgui->canvasGL,ButtonPress,button_press_event,0);

			fl_set_button(fd_globalNavigationgui->sala123, 0);
			fl_set_button(fd_globalNavigationgui->sala122, 0);
			fl_set_button(fd_globalNavigationgui->sala121, 0);
			fl_set_button(fd_globalNavigationgui->sala120, 0);
			fl_set_button(fd_globalNavigationgui->sala119, 0);
			fl_set_button(fd_globalNavigationgui->sala118, 0);
			fl_set_button(fd_globalNavigationgui->sala117, 0);
			fl_set_button(fd_globalNavigationgui->sala109, 0);
			fl_set_button(fd_globalNavigationgui->sala110, 0);
			fl_set_button(fd_globalNavigationgui->sala111, 0);
			fl_set_button(fd_globalNavigationgui->sala112, 0);
			fl_set_button(fd_globalNavigationgui->sala113, 0);
			fl_set_button(fd_globalNavigationgui->sala114, 0);
			fl_set_button(fd_globalNavigationgui->sala115, 0);
			fl_set_button(fd_globalNavigationgui->sala116, 0);
    }

	fl_show_form(fd_globalNavigationgui->globalNavigationgui,FL_PLACE_POSITION,FL_FULLBORDER,"GUIA MUSEO. AONDE LE LLEVO?");

  /* Empiezo con el canvas en blanco */
  myregister_buttonscallback(globalNavigation_guibuttons);
  myregister_displaycallback(globalNavigation_guidisplay);
	globalNavigationgui_win = FL_ObjWin(fd_globalNavigationgui->ventanaA);
	image_displaysetup ();
}

void globalNavigation_show(){
   static callback fn=NULL;
   if (fn==NULL){
      if ((fn=(callback)myimport ("graphics_xforms", "resume_callback"))!=NULL){
         fn ((gui_function)globalNavigation_guiresume_aux);
      }
   }
   else{
      fn ((gui_function)globalNavigation_guiresume_aux);
   }
}

