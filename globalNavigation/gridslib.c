/* Copyright 2001,2002 Jos� Maria Ca�as (jm) */
/*
    This file is part of gridslib.

    Gridslib is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

/* 
  2.0 August 2002, jm
    (1) visualjambs reading
    (2) sonar_noobstacle parameter.
  1.4, 26 Sep 2001, jm, posibilidad de robots rectangulares, incorpora_robot.
  1.3c, 24 Sep 2001, Lia incluye codigo para incorporar lecturas laser.
  1.2 JoseMaria   
 */

#include <math.h>
#include "gridslib.h"
#include <stdlib.h>  /* peticion-liberacion de memoria, extraccion de numeros aleatorios */ 
#include <stdio.h>
#include <string.h>

#ifndef PI
#define PI           3.14159265
#endif

#ifndef DEGTORAD
#define DEGTORAD     (PI / 180.0)
#endif


#define max(a,b) ((a>b)?a:b)
#define min(a,b) ((a<b)?a:b)
/* OJO esos parentesis son importantisimos. Al definirse como macros implementadas como sentencias, no como funciones, cuidado si hacemos las cosas sin parentesis. Sin ellos ymax=max(y1,max(y2,y3)) no vale el maximo de y1,y2,y3.*/



unsigned long num_azar=0;
#define PSEUDOALEATORIO 157
float azar[PSEUDOALEATORIO]; 
/* Array con valores aleatorios para la asignacion de puntos de vacio dentro del cono ultrasonico. Valores "precalculados" entre 0 y 1, ambos incluidos */

#define MAXFILTRO 100
#define F_DELTA 5.
#define F_THETA 5.
/* "MAXFILTRO" es el tamanyo de la memoria de observaciones que sirve para filtrar lecturas independientes. "F_DELTA" es la holgura que se permite para considerar una coordenada x o y similar a otra. "F_THETA" es la holgura en el angulo */


float no_radial_compensation=0.9;
/* "no_radial_compensation" es un factor por el que se multiplica el radio de la medida para distribuir los puntos vacios */

float densidad=0.8;
/* Para modelos muestreados: "llenosencono" y "vaciosencono" se calculan en cada lectura para mantener una densidad bidimensional de puntos constante en cada medida, con independencia del radio medido. "densidad" es un factor que determina el numero de puntos de vacio por cono. 1 == la densidad espacial de puntos corresponde a 1 punto por celdilla (aunque luego se distribuyan aleatoriamente).*/


#define SATURACION 0.0000001


void grid_reset(Tgrid *gg)
{
int i,k;

 gg->numabbgrid=0;
 for(i=0;i< ((gg->size)*(gg->size)) ;i++)
   {gg->map[i].estado=0.;
   gg->map[i].estado_o=0.;
   gg->map[i].estado_e=0.;
   gg->map[i].abreviado=-1;
   gg->map[i].nueva=0;
   gg->map[i].ultima_actualizacion=0;
   gg->map[i].in=0;

   for(k=0; k<gg->longhistoria;k++)
     { 
       if (gg->map[i].historia!=NULL)
	 {
	   gg->map[i].historia[k].o=0.;
	   gg->map[i].historia[k].e=0.;
	   gg->map[i].historia[k].angle=0.;
	   gg->map[i].historia[k].clock=0;
	 }
     }

   for(k=0; k<gg->cell_angles;k++)
     {
       if ((gg->map[i].angles_o!=NULL)&&(gg->map[i].angles_e!=NULL))
	 {
	   if (gg->regla==probabilistico)
	     {gg->map[i].angles_o[k]=1.-(float)pow(0.5,1./(double)gg->cell_angles);
	     }
	   else 
	     {
	       gg->map[i].angles_o[k]=0.;
	       gg->map[i].angles_e[k]=0.;
	     }
	 }
     }
   }
 
 if (gg->filtro!=NULL)
   {
     for(i=0;i<MAXFILTRO;i++)
       gg->filtro[i].clock=-1.;
     gg->num_sonarfiltro=0;
   }
 
 if (gg->visualjamb_filtro!=NULL)
   {
     for(i=0;i<MAXFILTRO;i++)
       gg->visualjamb_filtro[i].clock=-1;
     gg->num_visualjambfiltro=0;
   }
}


Tgrid *inicia_grid(Tvoxel centro, char *fichero)
/* devuelve un puntero a una estructura grid convenientemente inicializada. Si hay algun problema devuelve NULL. Primero abre el fichero para extraer los parametros que alli haya. OJO no es robusto a omisiones o valores incoherentes. */
{
  Tgrid *nuevogrid;
  float nuevoeje;
  FILE *f;
  char buf[MAX_LINE];
  char parametro[MAX_LINE];
  char valor[MAX_LINE];
  int leidos,i,linea,j,count=0;
  
  /*nuevogrid=malloc(sizeof(Tgrid));*/
  nuevogrid=calloc(1,sizeof(Tgrid)); /* for windows, memory is set to 0 */

  if (nuevogrid==NULL) {fprintf(stderr,"gridslib.c: No hay suficiente memoria: inicia_grid\n"); exit(-1);}
  nuevogrid->longhistoria = 0;
  nuevogrid->sonar_residuo = 0.;
  nuevogrid->ec_diferencial_speed = 1.;
  nuevogrid->mayoria_ruido = 0.;
  nuevogrid->mayoria_saturacion = 50.;
  nuevogrid->sonar_radialerror = 100.;
  nuevogrid->sonar_lobe_ticks = 200;
  nuevogrid->sonar_lobe_datanumber = 0;
  nuevogrid->sonar_lobe_data = NULL;
  nuevogrid->sonar_noobstacle = 200;
  nuevogrid->laser_noobstacle = 8000;
  nuevogrid->cell_angles=0;
  /* Les damos unos valores por defecto que no molesten */
 
  /* lee configuracion grid del fichero*/
  f= fopen(fichero,"r");
  if (f == NULL) {fprintf(stderr,"gridslib.c: No encuentro el fichero %s\n",fichero); exit(-1);}
  leidos=1;
  while (leidos!=0)
    {
    i=0; buf[0]=' ';
    while ((leidos==1)&&(buf[i]!='\n'))
      { leidos=fread(&buf[++i],1,1,f);
      }

    buf[++i]='\0';
    linea=i-1;i=0;
    while (buf[i]==' ') i++;

    if ((buf[i]=='\n')||(i>=linea)); /* linea en blanco */
    else if (buf[i]=='#'); /* linea comentario */ 
    else 
      {sscanf(&buf[i],"%s = %s",parametro,valor);
      if (strcmp(parametro,"resolucion")==0)
	nuevogrid->resolucion = (float) atof(valor);
      else if (strcmp(parametro,"dimension")==0)
	nuevogrid->eje = (float) atof(valor);
      else if (strcmp(parametro,"tipo")==0)
	{
	  if (strcmp(valor,"probabilistico")==0)
	    nuevogrid->regla=probabilistico;
	  else if (strcmp(valor,"mayoria")==0)
	    nuevogrid->regla=mayoria;
	  else if (strcmp(valor,"ec_diferencial")==0)
	    nuevogrid->regla=ec_diferencial;
	  else if (strcmp(valor,"histogramico")==0)
	    nuevogrid->regla=histogramico;
	  else if (strcmp(valor,"borroso")==0)
	    nuevogrid->regla=borroso;
	  else if (strcmp(valor,"teoria_evidencia")==0)
	    nuevogrid->regla=teoria_evidencia;
	}
      else if (strcmp(parametro,"long_historia")==0)
	nuevogrid->longhistoria = (int) atoi(valor);
      else if (strcmp(parametro,"cell_angles")==0)
	nuevogrid->cell_angles = (int) atoi(valor);
      else if (strcmp(parametro,"ec_diferencial_speed")==0)
	nuevogrid->ec_diferencial_speed = (float) atof(valor);
      else if (strcmp(parametro,"mayoria_saturacion")==0)
	nuevogrid->mayoria_saturacion = (float) atof(valor);
      else if (strcmp(parametro,"mayoria_ruido")==0)
	nuevogrid->mayoria_ruido = (float) atof(valor);
      else if (strcmp(parametro,"factor_envejecimiento")==0)
	nuevogrid->factor_envejecimiento = (float) atof(valor);
      else if (strcmp(parametro,"operador_union")==0)
	{
	  if (strcmp(valor,"suma_acotada")==0)
	    nuevogrid->operador_union=suma_acotada;
	  else if (strcmp(valor,"producto_algebraico")==0)
	    nuevogrid->operador_union=producto_algebraico;
	}
      /* Parametros de evolucion temporal */
      else if (strcmp(parametro,"paso_tiempo")==0)
	{       if (strcmp(valor,"nulo")==0)
	            nuevogrid->paso_tiempo=nulo;
	        else if (strcmp(valor,"multiplicar")==0)
	            nuevogrid->paso_tiempo=multiplicar;
	        else if (strcmp(valor,"observacion_neutra")==0)
	            nuevogrid->paso_tiempo=observacion_neutra;
	}
  
      
      /* Parametros del modelo sonar */
      else if (strcmp(parametro,"sonar_filtra")==0)
	{
	  if (strcmp(valor,"independientes")==0)
	    nuevogrid->sonar_filtra=independientes;
	  else if (strcmp(valor,"todas")==0)
	    nuevogrid->sonar_filtra=todas;
	}
      else if (strcmp(parametro,"sonar_geometria")==0)
	{   
	  if (strcmp(valor,"cono_denso")==0)
	    nuevogrid->sonar_geometria=cono_denso;
	  else if (strcmp(valor,"cono_muestreado")==0)
	    nuevogrid->sonar_geometria=cono_muestreado;
	  else if (strcmp(valor,"lobulo_denso")==0)
	    nuevogrid->sonar_geometria=lobulo_denso;
	  else if (strcmp(valor,"eje_denso")==0)
	    nuevogrid->sonar_geometria=eje_denso;
	}
      else if (strcmp(parametro,"sonar_apertura")==0)
	nuevogrid->sonar_apertura = (float) atof(valor);
      else if (strcmp(parametro,"sonar_fdistancia")==0)
	{ 
	  if (strcmp(valor,"constante")==0)
	    nuevogrid->sonar_fdistancia=constante;
	  else if (strcmp(valor,"lineal")==0)
	    nuevogrid->sonar_fdistancia=lineal;
	}
      else if (strcmp(parametro,"sonar_residuo")==0)
	nuevogrid->sonar_residuo = (float) atof(valor);
      else if (strcmp(parametro,"sonar_maxd")==0)
	nuevogrid->sonar_maxd = (float) atof(valor);
      else if (strcmp(parametro,"sonar_mind")==0)
	nuevogrid->sonar_mind = (float) atof(valor);
      else if (strcmp(parametro,"sonar_e")==0)
	nuevogrid->sonar_e = (float) atof(valor);
      else if (strcmp(parametro,"sonar_o")==0)
	nuevogrid->sonar_o = (float) atof(valor);
      else if (strcmp(parametro,"sonar_noobstacle")==0)
	nuevogrid->sonar_noobstacle = (float) atof(valor);
      else if (strcmp(parametro,"sonar_radialerror")==0)
	nuevogrid->sonar_radialerror = (float) atof(valor);
      else if (strcmp(parametro,"sonar_lobe_ticks")==0)
	nuevogrid->sonar_lobe_ticks = (float) atof(valor);
      else if (strcmp(parametro,"sonar_lobe_data")==0)
	{
	  /* one pass to guess how many data are */
	  nuevogrid->sonar_lobe_datanumber=1;
	  j=i;
	  while((buf[j]!='\n')&&(buf[j]!=' ')&&(buf[j]!='\0')) j++;
	  j++;
	  while((buf[j]!='\n')&&(buf[j]!=' ')&&(buf[j]!='\0')) j++;
	  j++;
	  while((buf[j]!='\n')&&(buf[j]!=' ')&&(buf[j]!='\0')) j++;

	  while (sscanf(&buf[j],"%s",parametro)==1) 
	    {while((buf[j]!='\n')&&(buf[j]!=' ')&&(buf[j]!='\0')) j++;
	    j++; /* space between successive data */
	    nuevogrid->sonar_lobe_datanumber++;};
	  
	  nuevogrid->sonar_lobe_data=malloc(nuevogrid->sonar_lobe_datanumber*sizeof(float));
	  if (nuevogrid->sonar_lobe_data==NULL) 
	    {printf("gridslib: no dynamic memory for sonar lobe data\n");
	    exit(-1);
	    }
	  
	  /* second pass to already fill the structure */
	  count=0;
	  nuevogrid->sonar_lobe_data[count++]=(float)atof(valor);
	  j=i;
	  while((buf[j]!='\n')&&(buf[j]!=' ')&&(buf[j]!='\0')) j++;
	  j++;
	  while((buf[j]!='\n')&&(buf[j]!=' ')&&(buf[j]!='\0')) j++;
	  j++;
	  while((buf[j]!='\n')&&(buf[j]!=' ')&&(buf[j]!='\0')) j++;
	  
	  while (sscanf(&buf[j]," %s",valor)==1) 
	    {while((buf[j]!='\n')&&(buf[j]!=' ')&&(buf[j]!='\0')) j++;
	    j++; /* space between successive data */
	    nuevogrid->sonar_lobe_data[count++]=(float)atof(valor);
	    }
	}
      
    
      /* Parametros del modelo laser */
      else if (strcmp(parametro,"laser_geometria")==0)
	{
	  if (strcmp(valor,"cono_denso")==0)
	    nuevogrid->laser_geometria=cono_denso;
	  else if (strcmp(valor,"eje_denso")==0)
	    nuevogrid->laser_geometria=eje_denso;
	  else if (strcmp(valor,"lobulo_denso")==0)
	    nuevogrid->laser_geometria=lobulo_denso;
	}
      else if (strcmp(parametro,"laser_apertura")==0)
	nuevogrid->laser_apertura = (float) atof(valor);
      else if (strcmp(parametro,"laser_muestras")==0)
	nuevogrid->laser_muestras = (int) atoi(valor);
      else if (strcmp(parametro,"laser_noobstacle")==0)
	nuevogrid->laser_noobstacle = (float) atof(valor);
      else if (strcmp(parametro,"laser_e")==0)
	nuevogrid->laser_e = (float) atof(valor);
      else if (strcmp(parametro,"laser_o")==0)
	nuevogrid->laser_o = (float) atof(valor);


      /* Parametros del modelo robot */
      else if (strcmp(parametro,"robot_geometria")==0)
	{ 
	  if (strcmp(valor,"cilindro")==0)
	    nuevogrid->robot_geometria=cilindro;
	  else if (strcmp(valor,"rectangulo")==0)
	    nuevogrid->robot_geometria=rectangulo;
	}
      else if (strcmp(parametro,"robot_radio")==0)
	nuevogrid->robot_radio = (float) atof(valor);
      else if (strcmp(parametro,"robot_ancho")==0)
	nuevogrid->robot_ancho = (float) atof(valor);
      else if (strcmp(parametro,"robot_largo")==0)
	nuevogrid->robot_largo = (float) atof(valor);
      else if (strcmp(parametro,"robot_e")==0)
	nuevogrid->robot_e = (float) atof(valor);   

      /* Parametros del modelo jamba visual */
      else if (strcmp(parametro,"visualjamb_rmax")==0)
	nuevogrid->visualjamb_rmax = (float) atof(valor);
      else if (strcmp(parametro,"visualjamb_aperture")==0)
	nuevogrid->visualjamb_aperture = (float) atof(valor);
      else if (strcmp(parametro,"visualjamb_aperture_pixels")==0)
	nuevogrid->visualjamb_aperture_pixels = (float) atoi(valor);
      else if (strcmp(parametro,"visualjamb_positive")==0)
	nuevogrid->visualjamb_positive = (float) atof(valor);
      else if (strcmp(parametro,"visualjamb_negative")==0)
	nuevogrid->visualjamb_negative = (float) atof(valor);
      else if (strcmp(parametro,"visualjamb_filtra")==0)
	{
	  if (strcmp(valor,"independientes")==0)
	    nuevogrid->visualjamb_filtra=independientes;
	  else if (strcmp(valor,"todas")==0)
	    nuevogrid->visualjamb_filtra=todas;
	}
      }
    }
  fclose(f);

  if (nuevogrid->resolucion <= 0) return NULL;
  else  
    {
      /* Debido al desplazamiento artificial del centro del grid, como maximo
      media celdilla. Se amplia en nuevo eje para cubrir lo que cubria en
      antiguo eje en el antiguo centro (que no esta permitido). No solo para
      este desplazamiento inicial, sino para los realizados con reubica_grid. */
      nuevoeje = nuevogrid->eje + nuevogrid->resolucion/2.; 
      nuevogrid->eje = nuevoeje;
    
      /* Calculo de numero de celdillas, siempre impar */
      nuevogrid->size=(int)(nuevoeje/(2.*nuevogrid->resolucion)+0.5)*2+1;
      //printf("Numero celdillas %d\n",nuevogrid->size);
     /* Reserva de memoria para las celdillas, historia y el grid abreviado*/
      nuevogrid->map1=malloc(sizeof(Tceldilla)*nuevogrid->size*nuevogrid->size);
      nuevogrid->map2=malloc(sizeof(Tceldilla)*nuevogrid->size*nuevogrid->size);
      nuevogrid->map=nuevogrid->map1;
      if ((nuevogrid->map1==NULL)||(nuevogrid->map2==NULL))
      {fprintf(stderr,"gridslib.c: No hay suficiente memoria celdillas: inicia_grid\n"); exit(-1);}

      for(i=0;i< ((nuevogrid->size)*(nuevogrid->size)) ;i++)
	{
	  if (nuevogrid->cell_angles>0)
	    {
	      nuevogrid->map1[i].angles_o=malloc(sizeof(float)*nuevogrid->cell_angles);
	      nuevogrid->map1[i].angles_e=malloc(sizeof(float)*nuevogrid->cell_angles);
	      nuevogrid->map2[i].angles_o=malloc(sizeof(float)*nuevogrid->cell_angles);
	      nuevogrid->map2[i].angles_e=malloc(sizeof(float)*nuevogrid->cell_angles);
	      if ((nuevogrid->map1[i].angles_o==NULL)||
		  (nuevogrid->map1[i].angles_e==NULL)||
		  (nuevogrid->map2[i].angles_o==NULL)||
		  (nuevogrid->map2[i].angles_e==NULL))
		{fprintf(stderr,"gridslib.c: no memory for cells angles: inicia_grid\n"); exit(-1);}
	    }
	  else 
	    {
	      nuevogrid->map1[i].angles_o=NULL; 
	      nuevogrid->map1[i].angles_e=NULL; 
	      nuevogrid->map2[i].angles_o=NULL;
	      nuevogrid->map2[i].angles_e=NULL;
	    }

	  if (nuevogrid->longhistoria>0)
	    {
	      nuevogrid->map1[i].historia=malloc(sizeof(Tevidencia)*nuevogrid->longhistoria);
	      nuevogrid->map2[i].historia=malloc(sizeof(Tevidencia)*nuevogrid->longhistoria);
	      if ((nuevogrid->map1[i].historia==NULL)||
		  (nuevogrid->map2[i].historia==NULL))
		{fprintf(stderr,"gridslib.c: no memory for cells history: inicia_grid\n"); exit(-1);}
	    }
	  else {
	    nuevogrid->map1[i].historia=NULL; 
	    nuevogrid->map2[i].historia=NULL;
	  }
	}


      nuevogrid->thresabbgrid=0.;
      nuevogrid->numabbgrid=0;
      nuevogrid->abbgrid1=malloc(sizeof(int)*nuevogrid->size*nuevogrid->size);
      nuevogrid->abbgrid2=malloc(sizeof(int)*nuevogrid->size*nuevogrid->size);
      nuevogrid->abbgrid=nuevogrid->abbgrid1;
      if ((nuevogrid->abbgrid1==NULL)||(nuevogrid->abbgrid2==NULL))
      {fprintf(stderr,"gridslib.c: No hay suficiente memoria grid_abreviado: inicia_grid\n"); exit(-1);}
 
      if (nuevogrid->sonar_filtra == independientes)
	{nuevogrid->filtro=malloc(sizeof(Tlectura_sonar)*MAXFILTRO);
	if (nuevogrid->filtro==NULL)
	  {fprintf(stderr,"gridslib.c: No hay suficiente memoria para el filtro sonar: inicia_grid\n"); exit(-1);}
	}
      else nuevogrid->filtro=NULL;
      

      if (nuevogrid->visualjamb_filtra == independientes)
	{nuevogrid->visualjamb_filtro=malloc(sizeof(Tjamb_reading)*MAXFILTRO);
	if (nuevogrid->visualjamb_filtro==NULL)
	  {fprintf(stderr,"gridslib.c: No hay suficiente memoria para el filtro visualjamb: inicia_grid\n"); exit(-1);}
	}
      else nuevogrid->visualjamb_filtro=NULL;
      
      grid_reset(nuevogrid);

      /* Localizacion del grid, esquinas, centro y de todas sus celdillas */
      reubica_grid(nuevogrid,centro);

  
      for(i=0;i<PSEUDOALEATORIO;i++)
	azar[i]= (float) rand() / (float) RAND_MAX;
      /*inicializa tabla de numeros pseudoaleatorios para distribuir los puntos de vacio dentro del cono ultrasonico. Repetidas llamadas a esta funcion renuevan el contenido de esta tabla */


      return nuevogrid;
    }
  
}

void envejece_grid(Tgrid *gg)
     /* OJO cuando filtramos las lecturas independientes el filtro tambien debe envejecer */
{
  int i,j;
  float oldestado;
  Tevidencia e;

  if (gg->paso_tiempo==multiplicar)
 {
   for(i=0;i< ((gg->size)*(gg->size)) ;i++) 
     {oldestado=gg->map[i].estado;
     gg->map[i].estado*=gg->factor_envejecimiento;
     if (gg->map[i].estado!=oldestado) gg->map[i].nueva=1; /*flag visual*/
     }
 }
  else if (gg->paso_tiempo==observacion_neutra)
    {
      e.o=0;
      e.e=0;
      e.clock=0;

      if (gg->longhistoria>0){
	for(i=0;i< ((gg->size)*(gg->size)) ;i++) 
	  {
	    /* introduce la sensacion nula en la historia de esa celdilla */
	    oldestado=gg->map[i].estado;	 
	    actualiza_celdilla(gg,e);
	    if (gg->map[i].estado!=oldestado) gg->map[i].nueva=1;
	  }
      }
    }
  else if (gg->paso_tiempo==nulo);


   /* El paso del tiempo tambien va haciendo que se olviden lecturas de
      la memoria del filtro. Insertamos tantas observaciones nulas 
      (clock<0) como ritmo de olvido tengamos.  */
  if ((gg->sonar_filtra==independientes)&&(gg->paso_tiempo==multiplicar))
    {
      for (j=0; j< (int)(MAXFILTRO*(1.-gg->factor_envejecimiento)); j++)
	gg->filtro[(gg->num_sonarfiltro)+j%MAXFILTRO].clock=-1;
      gg->num_sonarfiltro=((gg->num_sonarfiltro)+j)%MAXFILTRO;
   
    }

 if ((gg->visualjamb_filtra==independientes)&&(gg->paso_tiempo==multiplicar))
    {
      for (j=0; j< (int)(MAXFILTRO*(1.-gg->factor_envejecimiento)); j++)
	gg->visualjamb_filtro[(gg->num_visualjambfiltro)+j%MAXFILTRO].clock=-1;
      gg->num_visualjambfiltro=((gg->num_visualjambfiltro)+j)%MAXFILTRO;
   
    }
}


float peso_distancia(Tgrid *grid,float distancia)
{
 
  if (grid->sonar_fdistancia==lineal)
    {
      if (distancia<grid->sonar_mind) return 1.;
      else if (distancia<grid->sonar_maxd) return (1.-(distancia-grid->sonar_mind)/(grid->sonar_maxd-grid->sonar_mind)*(1-grid->sonar_residuo));
      else return grid->sonar_residuo;
    }   
  else if (grid->sonar_fdistancia==constante)
    {
      if (distancia<grid->sonar_mind) return grid->sonar_residuo;
      else if (distancia<grid->sonar_maxd) return 1;
      else return grid->sonar_residuo;
    }
  else return 0.;
}


int actualiza_celdilla(Tgrid *gg, Tevidencia e)
{int deltax, deltay;
 float oldestado,maxincremento,estadotmp;
 /* Ojo al cambio de orden en la matriz[fila][columna], las filas son las ordenadas y las columnas las abscisas: [deltay][deltax]. Necesita que la esquina del grid este bien calculada para el instante actual. Actualiza convenientemente el estado de ocupacion de la celdilla. */
 int i, j, delmismotipo, c,ii,jj;
 float oldprob, oldratio, ratio, prob;
 float oldcv, cv, gro;
 float grado_o;
 float factor_secuencia,acumulado;
 int cxmin,cxmax,cymin,cymax;
 float mefmap,mefsensor,k;
 int zz;
 float max;

 deltax=(int)((e.punto.x-gg->lo.x+gg->resolucion/2.)/gg->resolucion);
 deltay=(int)((e.punto.y-gg->lo.y+gg->resolucion/2.)/gg->resolucion);
 c=deltay*gg->size+deltax;

 if ((deltax>=gg->size) || (deltay>=gg->size) || (deltax<0) || (deltay<0)) return -1; /* cae fuera del grid */
 else if (gg->map[c].ultima_actualizacion==e.clock) return -2;
 /* procede de la misma sensacion que otro punto que ya actualizo el valor de la celdilla */ 
 else {
   oldestado=gg->map[c].estado;
    if (gg->longhistoria>0){
	 gg->map[c].historia[gg->map[c].in].o=e.o;
	 gg->map[c].historia[gg->map[c].in].e=e.e;
	 gg->map[c].historia[gg->map[c].in].angle=e.angle;
	 gg->map[c].historia[gg->map[c].in].clock=e.clock;
	 gg->map[c].in=(++gg->map[c].in)%gg->longhistoria;
    }


   if (gg->regla==ec_diferencial)
     {
       /*  Al introducir ec_diferencial_speed se introduce una dinamica con cierta resistencia a cambiar de opinion. Ahora son necesarias varias sensaciones para llevar del estado desocupado al ocupado. */

       if (gg->longhistoria>0) 
	 {
	   if (gg->cell_angles>0)
	     {
	     for(ii=0; ii<gg->cell_angles; ii++)
	       gg->map[c].angles_o[ii]=0.;
	     for(j=0; j< gg->longhistoria; j++)
	       { 
		 i=(gg->map[c].in+j)%gg->longhistoria;
		 if (gg->map[c].historia[i].clock>0)
		   {
		     zz=((int)(floor(((e.angle+2.*PI)/2.*PI)*gg->cell_angles)))%gg->cell_angles; 
		     estadotmp=gg->map[c].angles_o[zz];
		     if (gg->map[c].historia[i].o>0.) maxincremento=MAX_ESTADO-estadotmp;
		     else if (gg->map[c].historia[i].o<0.) maxincremento=-(-MAX_ESTADO-estadotmp);
		     else maxincremento=0.;
		     gg->map[c].angles_o[zz]+= gg->ec_diferencial_speed*maxincremento*gg->map[c].historia[i].o;
		   }
	       }
	     /* combination of different angles */
	     max=gg->map[c].angles_o[0];
	     for(ii=0; ii<gg->cell_angles; ii++)
	       {if (((gg->map[c].angles_o[ii]>MAX_ESTADO/20)||
		    (gg->map[c].angles_o[ii]<-MAX_ESTADO/20)) &&
		   (gg->map[c].angles_o[ii]>max))
		   max=gg->map[c].angles_o[ii];
	       }
	     gg->map[c].estado=max;
	     }
	   
	   else
	     {
	       gg->map[c].estado=0;
	       for(j=0; j< gg->longhistoria; j++)
		 {
		   i=(gg->map[c].in+j)%gg->longhistoria;
		   if (gg->map[c].historia[i].clock>0)
		     /* non null evidence */
		     {
		       estadotmp=gg->map[c].estado;
		       if (gg->map[c].historia[i].o>0.) maxincremento=MAX_ESTADO-estadotmp;
		       else if (gg->map[c].historia[i].o<0.) maxincremento=-(-MAX_ESTADO-estadotmp);
		       else maxincremento=0.;
		       
		       gg->map[c].estado+= gg->ec_diferencial_speed*maxincremento*gg->map[c].historia[i].o; 
		     }
		 }
	     }
	 }
       else
	 {
	   if (gg->cell_angles>0)
	     { 
	       zz=((int)(floor(((e.angle+2.*PI)/2.*PI)*gg->cell_angles)))%gg->cell_angles; 
	       estadotmp=gg->map[c].angles_o[zz];
	       if (e.o>0.) maxincremento=MAX_ESTADO-estadotmp;
	       else if (e.o<0.) maxincremento=-(-MAX_ESTADO-estadotmp);
	       else maxincremento=0.;
	       gg->map[c].angles_o[zz]+= gg->ec_diferencial_speed*maxincremento*e.o;
	       /* combination of different angles */
	       max=gg->map[c].angles_o[0];
	       for(ii=0; ii<gg->cell_angles; ii++)
		 {
		 if  (((gg->map[c].angles_o[ii]>MAX_ESTADO/20)||
		      (gg->map[c].angles_o[ii]<-MAX_ESTADO/20)) &&
		     (gg->map[c].angles_o[ii]>max))
		   max=gg->map[c].angles_o[ii];
		 }
	       gg->map[c].estado=max;
	     }
	   else 
	     {
	       /* Max incremento */
	       if (e.o>0.) maxincremento=MAX_ESTADO-gg->map[c].estado;
	       else if (e.o<0.) maxincremento=-(-MAX_ESTADO-gg->map[c].estado);
	       else maxincremento=0.;
	       gg->map[c].estado+= gg->ec_diferencial_speed*maxincremento*e.o; 
	     }
	 }

      /* Factor secuencia: la historia de la celdilla se necesita para el factor secuencia. maxincremento ya lleva el signo.
	  for(i=0; i<gg->longhistoria; i++)
	  {
	  if (gg->map[c].historia[i].o*e.o > 0.) delmismotipo++; 
	  }
	  factor_secuencia= (float)(delmismotipo)/(float)(gg->longhistoria);
       */
     }

 else if (gg->regla==mayoria)
   { 
     acumulado=0.;
     for(j=0; j< gg->longhistoria; j++)
       {acumulado+=gg->map[c].historia[j].o;
       }
     acumulado = acumulado*100./(float) gg->longhistoria;

     if (acumulado>= gg->mayoria_saturacion) 
       gg->map[c].estado=MAX_ESTADO;
     else if ((acumulado >= gg->mayoria_ruido) &&
	      (gg->mayoria_ruido!=gg->mayoria_saturacion))
       gg->map[c].estado=MAX_ESTADO*(acumulado - gg->mayoria_ruido)/(gg->mayoria_saturacion - gg->mayoria_ruido);
     else if (acumulado >= -gg->mayoria_ruido) gg->map[c].estado=0.; 
     else if ((acumulado >= -gg->mayoria_saturacion) &&
	      (gg->mayoria_ruido!=gg->mayoria_saturacion))
       gg->map[c].estado=-MAX_ESTADO*(-acumulado - gg->mayoria_ruido)/(gg->mayoria_saturacion - gg->mayoria_ruido);
     else gg->map[c].estado=-MAX_ESTADO; 
   }

 else if (gg->regla==probabilistico)
   {
     if (gg->longhistoria>0) 
       {
	 gg->map[c].estado=0;
	  for(j=0; j< gg->longhistoria; j++)
	     {
	       i=(gg->map[c].in+j)%gg->longhistoria;
	       if (gg->map[c].historia[i].clock>0)
		 /* non null evidence */
		 {
		   oldprob=(gg->map[c].estado / MAX_ESTADO + 1.)/2.;
		   oldratio=oldprob/(1-oldprob);
		   ratio=oldratio*gg->map[c].historia[i].o/(1-gg->map[c].historia[i].o);
		   prob= ratio / (1.+ratio);
		   prob=min(prob,1-SATURACION);
		   prob=max(prob,SATURACION);     
		   gg->map[c].estado = (2.*prob-1.)*MAX_ESTADO;
		 }
	     }
       }
     else 
       {
	 if (gg->cell_angles>0)
	   {
	     zz=((int)(floor(((e.angle+2.*PI)/2.*PI)*gg->cell_angles)))%gg->cell_angles; 
	     /*oldprob=(gg->map[c].angles_o[zz] / MAX_ESTADO + 1.)/2.;*/
	     oldprob = gg->map[c].angles_o[zz];
	     oldratio=oldprob/(1.-oldprob);
	     ratio=oldratio*e.o/(1-e.o);
	     prob=ratio / (1.+ratio);
	     prob=min(prob,1-SATURACION);
	     prob=max(prob,SATURACION);     
	     gg->map[c].angles_o[zz]=prob;
	     /*gg->map[c].angles_o[zz]=(2.*prob-1.)*MAX_ESTADO;*/
	         
	     prob=1.;
	     for(ii=0; ii<gg->cell_angles; ii++)
	      { prob=prob*(1.-gg->map[c].angles_o[ii]);
	      }
	     prob=1.-prob;    
	     gg->map[c].estado = (2.*prob-1.)*MAX_ESTADO;
	     /*gg->map[c].estado = (2.*gg->map[c].angles_o[0]-1.)*MAX_ESTADO;*/
	   }
	 else 
	   {
	     oldprob=(gg->map[c].estado / MAX_ESTADO + 1.)/2.;
	     oldratio=oldprob/(1.-oldprob);
	     ratio=oldratio*e.o/(1-e.o);
	     prob= ratio / (1.+ratio);
	     prob=min(prob,1-SATURACION);
	     prob=max(prob,SATURACION);     
	     gg->map[c].estado = (2.*prob-1.)*MAX_ESTADO;
	   }
       }
   }

 else if (gg->regla==histogramico)
   {
     if (gg->longhistoria>0) 
       {
	 gg->map[c].estado=0;
	  for(j=0; j< gg->longhistoria; j++)
	     {
	       i=(gg->map[c].in+j)%gg->longhistoria;
	       if (gg->map[c].historia[i].clock>0)
		 /* non null evidence */
		 {
		   oldcv=(gg->map[c].estado+MAX_ESTADO)*15./(2.*MAX_ESTADO);

		   /*
		   gro=0.;
		   cxmin=max(deltax-1,0);
		   cxmax=min(deltax+1,gg->size);
		   cymin=max(deltay-1,0);
		   cymax=min(deltay+1,gg->size);
		   for(ii=cymin;ii<=cymax;ii++)
		     for(jj=cxmin;jj<=cxmax;jj++) 
		       {
			 if ((jj==deltax)&&(ii==deltay));
			 else gro+=0.5*(gg->map[ii*gg->size+jj].estado+MAX_ESTADO)*15./(2.*MAX_ESTADO);
		       }
		   cv=oldcv+gg->map[c].historia[i].o+gro; 
		   */

		   cv=oldcv+gg->map[c].historia[i].o; 
		   cv=min(cv,15.);
		   cv=max(cv,0.);
		   gg->map[c].estado= 2.*MAX_ESTADO*cv/15. - MAX_ESTADO;
		 }
	     }
       }
     else
       {
	 oldcv=(gg->map[c].estado+MAX_ESTADO)*15./(2.*MAX_ESTADO);

	 /*
	 gro=0.;
	 cxmin=max(deltax-1,0);
	 cxmax=min(deltax+1,gg->size);
	 cymin=max(deltay-1,0);
	 cymax=min(deltay+1,gg->size);
	 for(ii=cymin;ii<=cymax;ii++)
	   for(jj=cxmin;jj<=cxmax;jj++) 
	     {
	       if ((jj==deltax)&&(ii==deltay));
	       else gro+=0.5*(gg->map[ii*gg->size+jj].estado+MAX_ESTADO)*15./(2.*MAX_ESTADO);
	     }
	 
	 cv=oldcv+e.o+gro;
	 */

	 cv=oldcv+e.o; 
	 cv=min(cv,15.);
	 cv=max(cv,0.);
	 gg->map[c].estado= 2.*MAX_ESTADO*cv/15. - MAX_ESTADO;
       }
   }

   else if (gg->regla==borroso)
     {
       if (gg->longhistoria>0) 
	 {
	   gg->map[c].estado_o=0;
	   gg->map[c].estado_e=0;
	   for(j=0; j< gg->longhistoria; j++)
	     {
	       i=(gg->map[c].in+j)%gg->longhistoria;
	       if (gg->map[c].historia[i].clock>0)
		 /* non null evidence */
		 {
		   if (gg->operador_union==suma_acotada) 
		     {
		       gg->map[c].estado_o=min(1,gg->map[c].estado_o+gg->map[c].historia[i].o);
		       gg->map[c].estado_e=min(1,gg->map[c].estado_e+gg->map[c].historia[i].e);
		       grado_o=max(0,gg->map[c].estado_o-gg->map[c].estado_e);
		     }
		   else if (gg->operador_union==producto_algebraico)
		     {
		       gg->map[c].estado_o=
			 gg->map[c].estado_o+gg->map[c].historia[i].o-(gg->map[c].estado_o*gg->map[c].historia[i].o);
		       gg->map[c].estado_e=
			 gg->map[c].estado_e+gg->map[c].historia[i].e-(gg->map[c].estado_e*gg->map[c].historia[i].e);
		       grado_o=gg->map[c].estado_o*(1-gg->map[c].estado_e);
		     }
		   gg->map[c].estado = (2.*grado_o-1.)*MAX_ESTADO;
		 }
	     }
	 }
       
       else 
	 {
	   if (gg->operador_union==suma_acotada) 
	     {
	       gg->map[c].estado_o=min(1,gg->map[c].estado_o+e.o);
	       gg->map[c].estado_e=min(1,gg->map[c].estado_e+e.e);
	       grado_o=max(0,gg->map[c].estado_o-gg->map[c].estado_e);
	     }
	   else if (gg->operador_union==producto_algebraico)
	     {
	       gg->map[c].estado_o=
		 gg->map[c].estado_o+e.o-(gg->map[c].estado_o*e.o);
	       gg->map[c].estado_e=
		 gg->map[c].estado_e+e.e-(gg->map[c].estado_e*e.e);
	       grado_o=gg->map[c].estado_o*(1-gg->map[c].estado_e);
	     }
	   gg->map[c].estado = (2.*grado_o-1.)*MAX_ESTADO;
	 }
     }

   else if (gg->regla==teoria_evidencia)
     {
       if (gg->longhistoria>0) 
	 {
	   gg->map[c].estado_o=0;
	   gg->map[c].estado_e=0;
	   for(j=0; j< gg->longhistoria; j++)
	     {
	       i=(gg->map[c].in+j)%gg->longhistoria;
	       if (gg->map[c].historia[i].clock>0)
		 /* non null evidence */
		 {
		   k=1.-gg->map[c].estado_e*gg->map[c].historia[i].o-gg->map[c].estado_o*gg->map[c].historia[i].e;
		   mefmap=1.-gg->map[c].estado_e-gg->map[c].estado_o;
		   mefsensor=1.-gg->map[c].historia[i].o-gg->map[c].historia[i].e;
		   
		   gg->map[c].estado_e=
		     (gg->map[c].estado_e*gg->map[c].historia[i].e+gg->map[c].estado_e*mefsensor+mefmap*gg->map[c].historia[i].e)/k;
		   gg->map[c].estado_o=
		     (gg->map[c].estado_o*gg->map[c].historia[i].o+gg->map[c].estado_o*mefsensor+mefmap*gg->map[c].historia[i].o)/k;
		   
		   gg->map[c].estado_e=min(gg->map[c].estado_e,1-SATURACION);
		   gg->map[c].estado_e=max(gg->map[c].estado_e,SATURACION);
		   gg->map[c].estado_o=min(gg->map[c].estado_o,1-SATURACION);
		   gg->map[c].estado_o=max(gg->map[c].estado_o,SATURACION);
		   
		   grado_o=gg->map[c].estado_o-gg->map[c].estado_e;
		   gg->map[c].estado = (grado_o)*MAX_ESTADO;
		 }
	     }
	 }
       else 
	 {
	   k=1.-gg->map[c].estado_e*e.o-gg->map[c].estado_o*e.e;
	   mefmap=1.-gg->map[c].estado_e-gg->map[c].estado_o;
	   mefsensor=1.-e.o-e.e;
	   
	   gg->map[c].estado_e=
	     (gg->map[c].estado_e*e.e+gg->map[c].estado_e*mefsensor+mefmap*e.e)/k;
	   gg->map[c].estado_o=
	     (gg->map[c].estado_o*e.o+gg->map[c].estado_o*mefsensor+mefmap*e.o)/k;
	   
	   gg->map[c].estado_e=min(gg->map[c].estado_e,1-SATURACION);
	   gg->map[c].estado_e=max(gg->map[c].estado_e,SATURACION);
	   gg->map[c].estado_o=min(gg->map[c].estado_o,1-SATURACION);
	   gg->map[c].estado_o=max(gg->map[c].estado_o,SATURACION);
	   
	   grado_o=gg->map[c].estado_o-gg->map[c].estado_e;
	   gg->map[c].estado = (grado_o)*MAX_ESTADO;
	 }
     }

     /* Ingreso si procede en el grid abreviado de celdillas con estado no nulo
	( si el estado es no nulo y que no estuviera anteriormente en el grid
	abreviado */
   if ((gg->map[c].estado>gg->thresabbgrid)&&(gg->map[c].abreviado==-1))
     { gg->map[c].abreviado=gg->numabbgrid;
     gg->abbgrid[gg->numabbgrid]=c;
     gg->numabbgrid++;
     }
   
   /* Borrado si procede del grid abreviado con celdillas de estado no nulo (
      si estado ahora es nulo y estaba en el grid abreviado. Lo que hace es poner
      el ultimo del grid abreviado en la posicion de este que se da de baja */
   if ((gg->map[c].estado<=gg->thresabbgrid)&&(gg->map[c].abreviado!=-1))
     { 
       if (gg->numabbgrid==1) {gg->map[c].abreviado=-1;gg->numabbgrid=0;} /* El limite es un caso especial */
       else {
	 gg->map[gg->abbgrid[gg->numabbgrid-1]].abreviado=gg->map[c].abreviado;
	 gg->abbgrid[gg->map[c].abreviado]=gg->abbgrid[gg->numabbgrid-1];
	 gg->map[c].abreviado=-1;
	 gg->numabbgrid--;}
     }
   
   if (gg->map[c].estado!=oldestado)
     gg->map[c].nueva=1;
     
   gg->map[c].ultima_actualizacion=e.clock;
   return 1;
 }
}
 

void reubica_grid(Tgrid *gg, Tvoxel nuevocentro)
 /* PAGE FLIPPING: centra el grid en la actual posicion del robot si el anterior grid no tiene situado al robot mas o menos en la zona central. Actualiza los valores de la estructura grid, centrandolo en la actual posicion del robot */
{

  int i,j,deltax,deltay,c;
  float nuevox,nuevoy;
	float oldcentrox, oldcentroy;
  Tceldilla *oldmap;

	oldcentrox = gg->centro.x;
	oldcentroy = gg->centro.y;

  oldmap=gg->map;
  if (gg->map==gg->map1)
    {
      gg->map=gg->map2;
      gg->abbgrid=gg->abbgrid2;
    }
  else 
    {
      gg->map=gg->map1;
      gg->abbgrid=gg->abbgrid1;
    }
  grid_reset(gg);

  /* Centra el grid en la nueva posicion, la mas proxima entre los posibles */
  if (nuevocentro.x > 0) nuevox = ((int)(nuevocentro.x/gg->resolucion + 0.5))*gg->resolucion;
  else if (nuevocentro.x < 0) nuevox = ((int)(nuevocentro.x/gg->resolucion - 0.5))*gg->resolucion;
  else nuevox = 0.;
  
  if (nuevocentro.y > 0) nuevoy = ((int)(nuevocentro.y/gg->resolucion + 0.5))*gg->resolucion;
  else if (nuevocentro.y < 0) nuevoy = ((int)(nuevocentro.y/gg->resolucion - 0.5))*gg->resolucion;
  else nuevoy = 0.;
  
  gg->centro.x=nuevox;
  gg->centro.y=nuevoy;
  gg->lo.x=nuevox-(gg->size/2)*gg->resolucion;
  gg->lo.y=nuevoy-(gg->size/2)*gg->resolucion;
  gg->hi.x=nuevox+(gg->size/2)*gg->resolucion;
  gg->hi.y=nuevoy+(gg->size/2)*gg->resolucion;
 
/* Actualizacion de las posiciones de cada una de las celdillas del grid en la
nueva posicion, se usan para pasar a puntos graficos y para el EM montado sobre
el grid. OJO tambi�n se actualizan las de las evidencias en sus memorias */
for(i=0;i< gg->size ;i++)
  for(j=0;j< gg->size ;j++)
    {
      c=i*gg->size+j;
      gg->map[c].centro.x= gg->lo.x+(float)j*gg->resolucion;
      gg->map[c].centro.y= gg->lo.y+(float)i*gg->resolucion;
    }

/* Barremos el mapa anterior para actualizar la celdilla homologa en el nuevo mapa */
for(i=0;i< (gg->size*gg->size) ;i++)
    {
      deltax=(int)((oldmap[i].centro.x-gg->lo.x-gg->resolucion/2.)/gg->resolucion);
      deltay=(int)((oldmap[i].centro.y-gg->lo.y-gg->resolucion/2.)/gg->resolucion);
      c=deltay*gg->size+deltax;

      if ((deltax>=gg->size) || (deltay>=gg->size) || (deltax<0) || (deltay<0)) ; /* cae fuera del nuevo grid */
      else 
	{
	  gg->map[c].estado=oldmap[i].estado;
	  gg->map[c].estado_o=oldmap[i].estado_o;
	  gg->map[c].estado_e=oldmap[i].estado_e;

	  gg->map[c].ultima_actualizacion=oldmap[i].ultima_actualizacion;
	  gg->map[c].nueva=1;
	  gg->map[c].abreviado=-1;
	  if (gg->map[c].estado > gg->thresabbgrid)
	    {
	      gg->map[c].abreviado=gg->numabbgrid;
	      gg->abbgrid[gg->numabbgrid]=c;
	      gg->numabbgrid++;
	    }
	  
	  gg->map[c].in=oldmap[i].in;

	  for(j=0; j<gg->longhistoria; j++)
	    {gg->map[c].historia[j].o=oldmap[i].historia[j].o;
	    gg->map[c].historia[j].e=oldmap[i].historia[j].e;
	    gg->map[c].historia[j].angle=oldmap[i].historia[j].angle;
	    gg->map[c].historia[j].clock=oldmap[i].historia[j].clock;
	    }

	  for(j=0; j<gg->cell_angles; j++)
	    {gg->map[c].angles_o[j]=oldmap[i].angles_o[j];
	    gg->map[c].angles_e[j]=oldmap[i].angles_e[j];
	    }


	}
    }     
 
}


void sensor2xy(Tvoxel sensor, float thetasensor, float d,float phi, Tvoxel *point) 
     /*  Calcula las coordenadas absolutas del punto (d,phi) (en coordenadas
     polares respecto del sistema de referencia solidario con el sensor, que
     esta en "sensor" con orientacion absoluta thetasensor. "phi" en grados,
     "d" en milimetros */
{
  float  Xp_sensor, Yp_sensor;

  Xp_sensor = d*cos(DEGTORAD*phi);
  Yp_sensor = d*sin(DEGTORAD*phi);
  (*point).x = Xp_sensor*cos(thetasensor*DEGTORAD) - Yp_sensor*sin(thetasensor*DEGTORAD) + sensor.x;
  (*point).y = Yp_sensor*cos(thetasensor*DEGTORAD) + Xp_sensor*sin(thetasensor*DEGTORAD) + sensor.y;
}


float apertura_max(Tgrid *gg, float radio2)
{
  int i,imax;

  if (gg->sonar_geometria==lobulo_denso) 
    {
      imax = (int) gg->sonar_noobstacle/gg->sonar_lobe_ticks;
      i=0;
      while(i<=imax)
	{
	  if (radio2 > (i*gg->sonar_lobe_ticks+100.)*(i*gg->sonar_lobe_ticks+100.)) i++;
	  else return gg->sonar_lobe_data[i];
	}
      return 0.0;      
    }
  else if (gg->sonar_geometria==cono_denso) return gg->sonar_apertura/2.;
  else return 0.;
}


void incorpora_sonar(Tgrid *gg, Tlectura_sonar sonar)
     /* Aqui interviene el modelo de sensor elegido: gg->sonar_apertura, densidad... Se calculan los puntos-obstaculo por cada cono ultrasonico, del mismo modo con los puntos-vacio. Si para distribuir aleatoriamente los puntos llamamos a la funcion rand() esta ralentiza demasiado y el accessbus se bloquea. Si se hace una distribucion regular salen conchas en el perfil a vista de pajaro. En su lugar tenemos una tabla de valores pseudoaleatorios que se rellena al empezar */
{
  int i,j, independiente, lectura_no_obstaculo=0;
  float angulo, radio2,radio, diferencia,delta,deltar,theta_max;
  float ymax, ymin, xmax, xmin, x1,x2,y1,y2;
  int cymax,cymin,cxmax,cxmin,cx,cy;
  Tevidencia e;
  Tlectura_sonar *filtro;
  float llenosencono, vaciosencono; 
  float dd;

  dd=sonar.distancia;
  lectura_no_obstaculo=0;
  e.clock=sonar.clock;
  if (gg->sonar_filtra==independientes)
    {
      independiente=1; 
      filtro=gg->filtro;
      for(i=MAXFILTRO;i>0;i--)
	/* El orden de recorrido influye en la eficiencia. Vamos de los mas
    recientes a los mas antiguos. Como se salta en cuanto se encuentra una
    observacion parecida, no es necesario recorrer la memoria completa antes de
     concluir que es dependiente */
	{
	  if (filtro[(gg->num_sonarfiltro+i)%MAXFILTRO].clock < 0)
	    continue;
	  /* que el tiempo de clock sea negativo se utiliza para las observaciones neutras en el filtro, que no casan con ninguna */

	  diferencia=filtro[(gg->num_sonarfiltro+i)%MAXFILTRO].thetasensor-sonar.thetasensor;
	  while((diferencia<=-180.)||(diferencia>180.))
	    {if (diferencia<=-180.) diferencia+=360.;
	    else if (diferencia>180.) diferencia-=360.;}
	  diferencia=max(diferencia,-diferencia);
	  if (diferencia>F_THETA) continue;

	  diferencia=filtro[(gg->num_sonarfiltro+i)%MAXFILTRO].fromhere.x-sonar.fromhere.x;
	  diferencia=max(diferencia,-diferencia);
	  if (diferencia>F_DELTA) continue;

	  diferencia=filtro[(gg->num_sonarfiltro+i)%MAXFILTRO].fromhere.y-sonar.fromhere.y;
	  diferencia=max(diferencia,-diferencia);
	  if (diferencia>F_DELTA) continue;

	  diferencia=filtro[(gg->num_sonarfiltro+i)%MAXFILTRO].distancia-sonar.distancia;
	  diferencia=max(diferencia,-diferencia);
	  if (diferencia>F_DELTA) continue;
	  
	  independiente=0; 
	  /*printf(" sonar repetido %d\n",k++);*/
	  break;
	}

      if (independiente==1) 
	{
	  filtro[gg->num_sonarfiltro].distancia=sonar.distancia;
	  filtro[gg->num_sonarfiltro].thetasensor=sonar.thetasensor;
	  filtro[gg->num_sonarfiltro].fromhere.x=sonar.fromhere.x;
	  filtro[gg->num_sonarfiltro].fromhere.y=sonar.fromhere.y;
	  filtro[gg->num_sonarfiltro].clock=sonar.clock;
	  gg->num_sonarfiltro=(gg->num_sonarfiltro++)%MAXFILTRO;
	}
    }
  
  if ((gg->sonar_filtra==todas)||
      ((gg->sonar_filtra==independientes)&&(independiente==1)))
    {

      if (sonar.distancia >= gg->sonar_noobstacle) 
	{ dd=gg->sonar_noobstacle;
	lectura_no_obstaculo=1;
	}
      else dd=sonar.distancia;

      if ((gg->sonar_geometria==eje_denso)||(gg->sonar_geometria==cono_denso)||(gg->sonar_geometria==lobulo_denso))
	{
	  /* OJO como solo se admite una modificacion del estado de celdilla por instante el orden importa: primero inserta la que cae en el centro del arco, y luego las de vacio en el eje */
	  if (lectura_no_obstaculo==0) {
	  x1 = sonar.fromhere.x + dd * cos(DEGTORAD*sonar.thetasensor);
	  y1 = sonar.fromhere.y + dd * sin(DEGTORAD*sonar.thetasensor);
	  cx=(x1-gg->lo.x+gg->resolucion/2.)/gg->resolucion;
	  cy=(y1-gg->lo.y+gg->resolucion/2.)/gg->resolucion;

	  if ((cx>=gg->size) || (cy>=gg->size) || (cx<0) || (cy<0)); /* cae fuera del grid */
	  else 
	    {
	      e.punto.x=gg->map[cy*gg->size+cx].centro.x;
	      e.punto.y=gg->map[cy*gg->size+cx].centro.y;
	      e.angle=(float)atan2((double)(sonar.fromhere.y-e.punto.y),(double)(sonar.fromhere.x-e.punto.x));

	      if (gg->regla==ec_diferencial) 
		{e.o=gg->sonar_o*peso_distancia(gg,dd); 
		e.e=0.;
		if (e.o!=0.) actualiza_celdilla(gg,e); 
		}
	      else if (gg->regla==mayoria) 
		{e.o=gg->sonar_o*peso_distancia(gg,dd);
		e.e=0.;
		if (e.o!=0.) actualiza_celdilla(gg,e);
		}
	      else if (gg->regla==probabilistico) 
		{e.o=(gg->sonar_o-0.5)*peso_distancia(gg,dd)+0.5;
		e.e=0.;
		if (e.o!=0.5) actualiza_celdilla(gg,e);
		}
	      else if (gg->regla==histogramico)
		{e.o=gg->sonar_o*peso_distancia(gg,dd);
		e.e=0.;
		if (e.o!=0.) actualiza_celdilla(gg,e);
		}
	      else if (gg->regla==borroso)
		{e.o=gg->sonar_o*peso_distancia(gg,dd);
		e.e=0.;
		if (e.o!=0.) actualiza_celdilla(gg,e);
		}
	      else if (gg->regla==teoria_evidencia)
		{e.e=0.;
		e.o=gg->sonar_o*peso_distancia(gg,dd);
		if (e.o!=0.) actualiza_celdilla(gg,e);
		}
	    }
	  }
	  
	  delta = gg->resolucion/4.;
	  radio = dd-delta;
	  while(radio>0)
	    {
	      x1 = sonar.fromhere.x + radio * cos(DEGTORAD*sonar.thetasensor);
	      y1 = sonar.fromhere.y + radio * sin(DEGTORAD*sonar.thetasensor);
	      cx=(x1-gg->lo.x+gg->resolucion/2.)/gg->resolucion;
	      cy=(y1-gg->lo.y+gg->resolucion/2.)/gg->resolucion;
	   
	      if ((cx>=gg->size) || (cy>=gg->size) || (cx<0) || (cy<0)) ; /* cae fuera del grid */
	      else {
		e.punto.x=gg->map[cy*gg->size+cx].centro.x;
		e.punto.y=gg->map[cy*gg->size+cx].centro.y;
		e.angle=(float)atan2((double)(sonar.fromhere.y-e.punto.y),(double)(sonar.fromhere.x-e.punto.x));

		if (gg->regla==ec_diferencial) 
		  {e.o=gg->sonar_e*peso_distancia(gg,dd);
		  e.e=0.;
		  if (e.o!=0.) actualiza_celdilla(gg,e);
		  }
		else if (gg->regla==mayoria) 
		  {e.o=gg->sonar_e*peso_distancia(gg,dd);
		  e.e=0.;
		  if (e.o!=0.) actualiza_celdilla(gg,e);
		  }
		else if (gg->regla==probabilistico) 
		  {e.o=(gg->sonar_e-0.5)*peso_distancia(gg,dd)+0.5;
		  e.e=0.;
		  if (e.o!=0.5) actualiza_celdilla(gg,e);
		  }
		else if (gg->regla==histogramico)
		  {e.o=gg->sonar_e*peso_distancia(gg,dd);
		  e.e=0.;
		  if (e.o!=0.) actualiza_celdilla(gg,e);
		  }
		else if (gg->regla==borroso) 
		  {e.e=gg->sonar_e*peso_distancia(gg,dd);
		  e.o=0.;
		  if (e.e!=0.) actualiza_celdilla(gg,e);
		  }
		else if (gg->regla==teoria_evidencia)
		  {e.e=gg->sonar_e*peso_distancia(gg,dd);
		  e.o=0.;
		  if (e.e!=0.) actualiza_celdilla(gg,e);
		  }
	      }
	      radio=radio-delta;
	    }
	}
      
      else if (gg->sonar_geometria==cono_muestreado) 
	{

	  if (lectura_no_obstaculo==0) 
	    {
	    /* Incorpora evidencias de obstaculo */
	    llenosencono = (float)(dd)*gg->sonar_apertura*(2*PI/360.)*densidad/(gg->resolucion);
	    for(i=0;i<(int)(ceil(llenosencono));i++) 
	      {
		angulo = (float) (azar[num_azar++%PSEUDOALEATORIO]-0.5)*gg->sonar_apertura;
		sensor2xy(sonar.fromhere, sonar.thetasensor, dd, (float)angulo, &(e.punto));
		e.angle=(float)atan2((double)(sonar.fromhere.y-e.punto.y),(double)(sonar.fromhere.x-e.punto.x));	
	
		cx=(e.punto.x-gg->lo.x+gg->resolucion/2.)/gg->resolucion;
		cy=(e.punto.y-gg->lo.y+gg->resolucion/2.)/gg->resolucion;
		
		if ((cx>=gg->size) || (cy>=gg->size) || (cx<0) || (cy<0)) ; /* cae fuera del grid */
		else {
		  if (gg->regla==ec_diferencial)
		    {e.o=gg->sonar_o*peso_distancia(gg,dd); 
		    e.e=0.;
		    if (e.o!=0.) actualiza_celdilla(gg,e); 
		    }
		  else if (gg->regla==mayoria) 
		    {e.o=gg->sonar_o*peso_distancia(gg,dd);
		    e.e=0.;
		    if (e.o!=0.) actualiza_celdilla(gg,e); 
		    }
		  else if (gg->regla==probabilistico) 
		    {e.o=(gg->sonar_o-0.5)*peso_distancia(gg,dd)+0.5;
		    e.e=0.;
		    if (e.o!=0.5) actualiza_celdilla(gg,e); 
		    }
		  else if (gg->regla==histogramico)
		    {e.o=gg->sonar_o*peso_distancia(gg,dd);
		    e.e=0.;
		    if (e.o!=0.) actualiza_celdilla(gg,e); 
		    }
		  else if (gg->regla==borroso)
		    {e.o=gg->sonar_o*peso_distancia(gg,dd);
		    e.e=0.;
		    if (e.o!=0.) actualiza_celdilla(gg,e); 
		    }
		  else if (gg->regla==teoria_evidencia)
		    {e.e=0.;
		    e.o=gg->sonar_o*peso_distancia(gg,dd);
		    if (e.o!=0.) actualiza_celdilla(gg,e); 
		    }
		}
	    }
	  
	    }

	  /* Puntos de ausencia por cada cono ultrasonico */
	  vaciosencono = (float)(dd)*(float)(dd)*0.14/(gg->resolucion)/(gg->resolucion)*densidad;
	  for(i=0;i<(int)(vaciosencono);i++) 
	    { 
	      radio = (float)azar[num_azar++%PSEUDOALEATORIO]*no_radial_compensation*(dd);
	      angulo = (float)(azar[num_azar++%PSEUDOALEATORIO]-0.5)*gg->sonar_apertura;
	      sensor2xy(sonar.fromhere,sonar.thetasensor,radio,angulo,&(e.punto));
	      e.angle=(float)atan2((double)(sonar.fromhere.y-e.punto.y),(double)(sonar.fromhere.x-e.punto.x));

	      cx=(e.punto.x-gg->lo.x+gg->resolucion/2.)/gg->resolucion;
	      cy=(e.punto.y-gg->lo.y+gg->resolucion/2.)/gg->resolucion;
	      
	      if ((cx>=gg->size) || (cy>=gg->size) || (cx<0) || (cy<0)) ; /* cae fuera del grid */
	      else {
		if (gg->regla==ec_diferencial) 
		  {e.o=gg->sonar_e*peso_distancia(gg,dd);
		  e.e=0.;
		  if (e.o!=0.) actualiza_celdilla(gg,e);
		  }
		else if (gg->regla==mayoria)
		  {e.o=gg->sonar_e*peso_distancia(gg,dd);
		  e.e=0.;
		  if (e.o!=0.) actualiza_celdilla(gg,e);
		  }
		else if (gg->regla==probabilistico) 
		  {e.o=(gg->sonar_e-0.5)*peso_distancia(gg,dd)+0.5;
		  e.e=0.;
		  if (e.o!=0.5) actualiza_celdilla(gg,e);
		  }
		else if (gg->regla==histogramico)
		  {e.o=gg->sonar_e*peso_distancia(gg,dd);
		  e.e=0.;
		  if (e.o!=0.) actualiza_celdilla(gg,e);
		  }
		else if (gg->regla==borroso) 
		  {e.e=gg->sonar_e*peso_distancia(gg,dd);
		  e.o=0.;
		  if (e.e!=0.) actualiza_celdilla(gg,e);
		  }
		else if (gg->regla==teoria_evidencia)
		  {e.e=gg->sonar_e*peso_distancia(gg,dd);
		  e.o=0.;
		  if (e.e!=0.) actualiza_celdilla(gg,e);
		  }
	      }
	    }	  
	}
      
      if ((gg->sonar_geometria==cono_denso)||(gg->sonar_geometria==lobulo_denso))
	/* OJO no vale else if porque en caso de cono_denso debe incorporar primero el eje, como si fuera eje_denso, y luego (aqui) el resto del cono */
	{
	  x1 = sonar.fromhere.x + dd * cos(DEGTORAD*(sonar.thetasensor-gg->sonar_apertura/2.));
	  y1 = sonar.fromhere.y + dd * sin(DEGTORAD*(sonar.thetasensor-gg->sonar_apertura/2.));
	  x2 = sonar.fromhere.x + dd * cos(DEGTORAD*(sonar.thetasensor+gg->sonar_apertura/2.));
	  y2 = sonar.fromhere.y + dd * sin(DEGTORAD*(sonar.thetasensor+gg->sonar_apertura/2.));
	  
	  while((sonar.thetasensor<=-180.)||(sonar.thetasensor>180.))
	    {if (sonar.thetasensor<=-180.) sonar.thetasensor+=360.;
	    else if (sonar.thetasensor>180.) sonar.thetasensor-=360.;}
	  
	  if (((-gg->sonar_apertura/2.) < sonar.thetasensor) && ( sonar.thetasensor < (gg->sonar_apertura/2.))) xmax=sonar.fromhere.x+dd;
	  else xmax=max(sonar.fromhere.x, max(x1,x2));
	  xmax=min(xmax,gg->hi.x);
	  
	  if ((((180.-gg->sonar_apertura/2.) < sonar.thetasensor) && 
	       (sonar.thetasensor <= 180.)) || 
	      ((sonar.thetasensor < (-180.+gg->sonar_apertura/2.)) &&
	       (sonar.thetasensor > -180.)))
	    xmin=sonar.fromhere.x-dd;
	  else xmin=min(sonar.fromhere.x, min(x1,x2));
	  xmin=max(xmin,gg->lo.x);
	  /* OJO los valores normales de angulos van de (-180,+180] */
	  
	  if (((90.-gg->sonar_apertura/2.) < sonar.thetasensor) && ( sonar.thetasensor < (90.+gg->sonar_apertura/2.))) ymax=sonar.fromhere.y+dd;
	  else ymax=max(sonar.fromhere.y, max(y1,y2));
	  ymax=min(ymax,gg->hi.y);
	  
	  if (((-90.-gg->sonar_apertura/2.) < sonar.thetasensor) && ( sonar.thetasensor < (-90.+gg->sonar_apertura/2.))) ymin=sonar.fromhere.y-dd;
	  else ymin=min(sonar.fromhere.y, min(y1,y2));
	  ymin=max(ymin,gg->lo.y);
	  
	  
	  /* Barremos directamente SOLO el area de interes. Primero se incorporan las medidas de ocupacion y despues las de vacio, por si caen en la misma celdilla, que solo influya la de ocupacion (OJO al filtro que en actualiza_celdilla que no permite mas que una actualizacion de la celdilla por reloj) */
	  cxmin=(xmin-gg->lo.x+gg->resolucion/2.)/gg->resolucion;
	  cxmax=(xmax-gg->lo.x+gg->resolucion/2.)/gg->resolucion;
	  cymin=(ymin-gg->lo.y+gg->resolucion/2.)/gg->resolucion;
	  cymax=(ymax-gg->lo.y+gg->resolucion/2.)/gg->resolucion;

	  for(i=cymin;i<=cymax;i++)
	    for(j=cxmin;j<=cxmax;j++)
	      { if ((gg->map[i*gg->size+j].centro.x <= xmax) &&
		    (gg->map[i*gg->size+j].centro.x >= xmin) &&
		    (gg->map[i*gg->size+j].centro.y <= ymax) &&
		    (gg->map[i*gg->size+j].centro.y >= ymin) )
		
		{
		  
		  e.punto.x=gg->map[i*gg->size+j].centro.x;
		  e.punto.y=gg->map[i*gg->size+j].centro.y;
		  e.angle=(float)atan2((double)(sonar.fromhere.y-e.punto.y),(double)(sonar.fromhere.x-e.punto.x));
		  
		  radio2=(gg->map[i*gg->size+j].centro.x-sonar.fromhere.x)*(gg->map[i*gg->size+j].centro.x-sonar.fromhere.x)+(gg->map[i*gg->size+j].centro.y-sonar.fromhere.y)*(gg->map[i*gg->size+j].centro.y-sonar.fromhere.y);

		  if ((gg->map[i*gg->size+j].centro.y!=sonar.fromhere.y)||(gg->map[i*gg->size+j].centro.x-sonar.fromhere.x))
		    angulo=atan2(gg->map[i*gg->size+j].centro.y-sonar.fromhere.y,gg->map[i*gg->size+j].centro.x-sonar.fromhere.x);
		  else angulo=0.;

		  diferencia=angulo/DEGTORAD-sonar.thetasensor;
		  while((diferencia<=-180.)||(diferencia>180.))
		    {if (diferencia<=-180.) diferencia+=360.;
		    else if (diferencia>180.) diferencia-=360.;}
		  diferencia=max(diferencia,-diferencia);
		  
		  deltar=min(dd*gg->sonar_radialerror/100.,gg->resolucion);
		  theta_max=apertura_max(gg,radio2);

		  if ((radio2 <= (dd+deltar/2.)*(dd+deltar/2.)) &&
		      (radio2 >= (dd-deltar/2.)*(dd-deltar/2.)) && 
		      (diferencia <= theta_max) &&
		      (lectura_no_obstaculo==0))
		    { 
		      if (gg->regla==ec_diferencial) 
			{e.o=gg->sonar_o*peso_distancia(gg,dd);
			e.e=0.;
			if (e.o!=0.) actualiza_celdilla(gg,e);
			}
		      else if (gg->regla==mayoria) 
			{e.o=gg->sonar_o*peso_distancia(gg,dd);
			e.e=0.;
			if (e.o!=0.) actualiza_celdilla(gg,e);
			}
		      else if (gg->regla==probabilistico) 
			{e.o=(gg->sonar_o-0.5)*peso_distancia(gg,dd)+0.5;
			e.e=0.;
			if (e.o!=0.5) actualiza_celdilla(gg,e);
			}
		      else if (gg->regla==histogramico)
			{e.o=gg->sonar_o*peso_distancia(gg,dd);
			e.e=0.;
			if (e.o!=0.) actualiza_celdilla(gg,e);
			}
		      else if (gg->regla==borroso)
			{e.o=gg->sonar_o*peso_distancia(gg,dd);
			e.e=0.;
			if (e.o!=0.) actualiza_celdilla(gg,e);
			}
		      else if (gg->regla==teoria_evidencia)
			{e.e=0.;
			e.o=gg->sonar_o*peso_distancia(gg,dd);
			if (e.o!=0.) actualiza_celdilla(gg,e);
			}
		    }

		  else if ((radio2 < (dd-deltar/2.)*(dd-deltar/2.)) &&
			   (diferencia <= theta_max))
		    {
		      if (gg->regla==ec_diferencial) 
			{e.o=gg->sonar_e*peso_distancia(gg,dd);
			e.e=0.;
			if (e.o!=0.) actualiza_celdilla(gg,e);
			}
		      else if (gg->regla==mayoria) 
			{e.o=gg->sonar_e*peso_distancia(gg,dd);
			e.e=0.;
			if (e.o!=0.) actualiza_celdilla(gg,e);
			}
		      else if (gg->regla==probabilistico) 
			{e.o=(gg->sonar_e-0.5)*peso_distancia(gg,dd)+0.5;
			e.e=0.;
			if (e.o!=0.5) actualiza_celdilla(gg,e);
			}
		      else if (gg->regla==histogramico)
			{e.o=gg->sonar_e*peso_distancia(gg,dd);
			e.e=0.;
			if (e.o!=0.) actualiza_celdilla(gg,e);
			}
		      else if (gg->regla==borroso)
			{e.e=gg->sonar_e*peso_distancia(gg,dd);
			e.o=0.;
			if (e.e!=0.) actualiza_celdilla(gg,e);
			}
		      else if (gg->regla==teoria_evidencia)
			{e.e=gg->sonar_e*peso_distancia(gg,dd);
			e.o=0.;
			if (e.e!=0.) actualiza_celdilla(gg,e);
			}
		    }
		  else;
		}
	      else;
	      }
	}
    }
}


void incorpora_robot(Tgrid *gg, Tlectura_posicionrobot posicionrobot)
     /* Donde el robot esta situado no hay ningun obstaculo, cuando el robot se
     mueva, es muy probable que ese espacio siga SIN obstaculo */
{
  int i,j;
  float radio,radio2,angulo,diferencia,rmax,alfa;
  float ymax, ymin, xmax, xmin;
  int cymax,cymin,cxmax,cxmin;
  Tevidencia e;

  e.clock=posicionrobot.clock;

  if (gg->robot_geometria==cilindro) 
    {
      xmax = posicionrobot.fromhere.x + gg->robot_radio;
      xmin = posicionrobot.fromhere.x - gg->robot_radio;
      ymax = posicionrobot.fromhere.y + gg->robot_radio;
      ymin = posicionrobot.fromhere.y - gg->robot_radio;
      
      /* Barremos directamente SOLO el area de interes. Primero se incorporan las medidas de ocupacion y despues las de vacio, por si caen en la misma celdilla, que solo influya la de ocupacion (OJO al filtro que en actualiza_celdilla que no permite mas que una actualizacion de la celdilla por reloj */
      cxmin=(xmin-gg->lo.x+gg->resolucion/2.)/gg->resolucion;
      cxmax=(xmax-gg->lo.x+gg->resolucion/2.)/gg->resolucion;
      cymin=(ymin-gg->lo.y+gg->resolucion/2.)/gg->resolucion;
      cymax=(ymax-gg->lo.y+gg->resolucion/2.)/gg->resolucion;
      
      for(i=cymin;i<=cymax;i++)
	for(j=cxmin;j<=cxmax;j++)
	  { if ((gg->map[i*gg->size+j].centro.x <= xmax) &&
		(gg->map[i*gg->size+j].centro.x >= xmin) &&
		(gg->map[i*gg->size+j].centro.y <= ymax) &&
		(gg->map[i*gg->size+j].centro.y >= ymin) )
	    
	    {
	      e.punto.x=gg->map[i*gg->size+j].centro.x;
	      e.punto.y=gg->map[i*gg->size+j].centro.y;
	      
	      radio2=(gg->map[i*gg->size+j].centro.x-posicionrobot.fromhere.x)*(gg->map[i*gg->size+j].centro.x-posicionrobot.fromhere.x)+(gg->map[i*gg->size+j].centro.y-posicionrobot.fromhere.y)*(gg->map[i*gg->size+j].centro.y-posicionrobot.fromhere.y);

	      if (radio2 < gg->robot_radio*gg->robot_radio)
		{
		  if (gg->regla==ec_diferencial) 
		    {e.o=gg->robot_e;
		    e.e=0.;
		    }
		  else if (gg->regla==mayoria) 
		    {e.o=gg->robot_e;
		    e.e=0.;}
		  else if (gg->regla==probabilistico) 
		    {e.o=gg->robot_e;
		    e.e=0.;}
		  else if (gg->regla==histogramico) 
		    {e.o=gg->robot_e;
		    e.e=0;
		    }
		  else if (gg->regla==borroso)
		    {e.e=gg->robot_e;
		    e.o=0.;
		    }
		  else if (gg->regla==teoria_evidencia)
		    {e.e=gg->robot_e;
		    e.o=0.;
		    }
		  actualiza_celdilla(gg,e);
		}
	    }
	  else;
	  }
    }
  else if (gg->robot_geometria==rectangulo)
    {
      radio=sqrt(gg->robot_largo*gg->robot_largo+gg->robot_ancho*gg->robot_ancho)/2.+gg->resolucion; /* sumamos una celdilla para curarnos en salud */
      if (gg->robot_largo!=0.)
	alfa=atan(gg->robot_ancho/gg->robot_largo)/DEGTORAD;
      else alfa=0.;

      xmax = posicionrobot.fromhere.x + radio;
      xmin = posicionrobot.fromhere.x - radio;
      ymax = posicionrobot.fromhere.y + radio;
      ymin = posicionrobot.fromhere.y - radio;
      
      /* Barremos directamente SOLO el area de interes */
      cxmin=(xmin-gg->lo.x+gg->resolucion/2.)/gg->resolucion;
      cxmax=(xmax-gg->lo.x+gg->resolucion/2.)/gg->resolucion;
      cymin=(ymin-gg->lo.y+gg->resolucion/2.)/gg->resolucion;
      cymax=(ymax-gg->lo.y+gg->resolucion/2.)/gg->resolucion;
      
      for(i=cymin;i<=cymax;i++)
	for(j=cxmin;j<=cxmax;j++)
	  { if ((gg->map[i*gg->size+j].centro.x <= xmax) &&
		(gg->map[i*gg->size+j].centro.x >= xmin) &&
		(gg->map[i*gg->size+j].centro.y <= ymax) &&
		(gg->map[i*gg->size+j].centro.y >= ymin) )
	    
	    {
	      e.punto.x=gg->map[i*gg->size+j].centro.x;
	      e.punto.y=gg->map[i*gg->size+j].centro.y;
	      
	      radio2=(gg->map[i*gg->size+j].centro.x-posicionrobot.fromhere.x)*(gg->map[i*gg->size+j].centro.x-posicionrobot.fromhere.x)+(gg->map[i*gg->size+j].centro.y-posicionrobot.fromhere.y)*(gg->map[i*gg->size+j].centro.y-posicionrobot.fromhere.y);
	      
	      if ((gg->map[i*gg->size+j].centro.y!=posicionrobot.fromhere.y)||(gg->map[i*gg->size+j].centro.x!=posicionrobot.fromhere.x))
		angulo=atan2(gg->map[i*gg->size+j].centro.y-posicionrobot.fromhere.y,gg->map[i*gg->size+j].centro.x-posicionrobot.fromhere.x);
	      else angulo=0.;

	      diferencia=(angulo-posicionrobot.thetarobot)/DEGTORAD+90.;
	      while((diferencia<=-180.)||(diferencia>180.))
		{if (diferencia<=-180.) diferencia+=360.;
		else if (diferencia>180.) diferencia-=360.;}
	      diferencia=max(diferencia,-diferencia);

	      if (((diferencia<=alfa)&&(diferencia>=-alfa)) ||
		  ((diferencia>=180.-alfa)&&(diferencia<=180.))||
		  ((diferencia<-180.+alfa)&&(diferencia>-180.)))
		rmax=(gg->robot_largo/2.)/cos(diferencia*DEGTORAD);
	      else rmax=(gg->robot_ancho/2.)/sin(diferencia*DEGTORAD);

	      if (radio2 <= rmax*rmax)
		{
		  if (gg->regla==ec_diferencial) 
		    {e.o=gg->robot_e;
		    e.e=0.;}
		  else if (gg->regla==mayoria) 
		    {e.o=gg->robot_e;
		    e.e=0.;}
		  else if (gg->regla==probabilistico) 
		    {e.o=gg->robot_e;
		    e.e=0.;}
		  else if (gg->regla==histogramico) 
		    {e.o=gg->robot_e;
		    e.e=0.;}
		  else if (gg->regla==borroso)
		    { e.e=gg->robot_e;
		    e.o=0.;
		    }
		  else if (gg->regla==teoria_evidencia)
		    {e.e=gg->robot_e;
		    e.o=0.;
		    }
		  actualiza_celdilla(gg,e);
		}
	    }
	  else;
	  }
    }

}


void termina_grid(Tgrid *gg)
{
  int i;


  if (gg->longhistoria >0)
    {
      for(i=0;i< ((gg->size)*(gg->size)) ;i++)
	{
	  free(gg->map1[i].historia);
	  free(gg->map2[i].historia);
	}
    }

  if (gg->cell_angles >0)
    {
      for(i=0;i< ((gg->size)*(gg->size)) ;i++)
	{
	  free(gg->map1[i].angles_o);
	  free(gg->map1[i].angles_e);
	  free(gg->map2[i].angles_o);
	  free(gg->map2[i].angles_e);
	}
    }
  free(gg->map1);
  free(gg->map2);
  free(gg->abbgrid1);
  free(gg->abbgrid2);
  free(gg->sonar_lobe_data);
  free(gg->visualjamb_filtro);
  free(gg->filtro);
  free(gg);
}



void incorpora_laser(Tgrid *gg, Tlectura_laser laser) {
/* Coge todo el array de medidas del laser (361 medidas cada 0.5 grados) y con el fin de agilizar el c�lculo, divide el semicirculo en gg->laser_muestras sectores. En cada uno de ellos muestrea aleatoriamente y solo incorpora una medida por sector. actualiza_celdilla ignora la segunda y posteriores actualizaciones sobre la misma celdilla para el mismo instante de tiempo.*/
  int i=0,j, k,u,lectura_no_obstaculo=0;
  float incremento;
  float angulo, radio, angulo2, radio2, diferencia,delta,deltar;
  float ymax, ymin, xmax, xmin, x1,x2,y1,y2;
  int cymax,cymin,cxmax,cxmin,cx,cy;
  Tevidencia e;
  float dd;

  e.clock=laser.clock;  
  
  if ((gg->laser_geometria==eje_denso)||(gg->laser_geometria==cono_denso))
    {
      /* Solo se incorporan gg->laser_muestras por cada barrido laser,
	 ie se muestrea la informacion del barrido. Primero se incorporan 
	 las medidas de ocupacion y despues las de vacio, por si caen en 
	 la misma celdilla, que solo influya la de ocupacion. */
      incremento=180./gg->laser_muestras;
      for(u=0;u<gg->laser_muestras;u++)
	{
	  angulo=incremento*(u+azar[num_azar++%PSEUDOALEATORIO]);
	  /* angulo=incremento*(u+0.5); */
	  k=(int)ceil(angulo*laser.nummedidas/180.+0.5);
	  angulo=k*180./laser.nummedidas+laser.thetasensor;
	  radio=laser.distancias[k];
	    if (radio >= gg->laser_noobstacle) 
	      { dd=gg->laser_noobstacle;
	      lectura_no_obstaculo=1;
	      }
	    else 
	      { dd=laser.distancias[k];
	      lectura_no_obstaculo=0;
	      }

	    /* first, evidences for occupancy at the end of all the rays of the beam */
	    if (lectura_no_obstaculo==0)
	      {
		x1 = laser.fromhere.x + dd * cos(DEGTORAD*angulo);
		y1 = laser.fromhere.y + dd * sin(DEGTORAD*angulo);
		cx=(x1-gg->lo.x+gg->resolucion/2.)/gg->resolucion;
		cy=(y1-gg->lo.y+gg->resolucion/2.)/gg->resolucion;
	  
		if ((cx>=gg->size) || (cy>=gg->size) || (cx<0) || (cy<0));/* cae fuera del grid */
		else
		  {  
		    e.punto.x=gg->map[cy*gg->size+cx].centro.x;
		    e.punto.y=gg->map[cy*gg->size+cx].centro.y;
		    if ((gg->regla==mayoria)||(gg->regla==ec_diferencial))
		      {e.o=gg->laser_o;
		      e.e=0.;}
		    actualiza_celdilla(gg,e);
		  }
	      }


	    /*No vale else if, porque si es cono_denso debe antes incorporar el eje
	      central, como si fuera eje_denso y luego (aqui) el resto del cono */
	    if (gg->laser_geometria==cono_denso)
	      {
		x1 = laser.fromhere.x + dd * cos(DEGTORAD*(angulo-gg->laser_apertura/2.));
		y1 = laser.fromhere.y + dd * sin(DEGTORAD*(angulo-gg->laser_apertura/2.));
		x2 = laser.fromhere.x + dd * cos(DEGTORAD*(angulo+gg->laser_apertura/2.));
		y2 = laser.fromhere.y + dd * sin(DEGTORAD*(angulo+gg->laser_apertura/2.));
		
		while((angulo<-180.)||(angulo>=180.))
		  { if (angulo<-180.) angulo+=360.;
		    else if (angulo>=180.) angulo-=360.;}
		/* OJO los valores normales de angulos van de (-180,+180] */		

		if (((-gg->laser_apertura/2.) < angulo) && 
		    ( angulo < (gg->laser_apertura/2.))) 
		  xmax=laser.fromhere.x+dd;
		else xmax=max(laser.fromhere.x, max(x1,x2));
		xmax=min(xmax,gg->hi.x);
	  
		if ((((180.-gg->laser_apertura/2.) < angulo) && 
		     (angulo <= 180.)) || 
		    ((angulo < (-180.+gg->laser_apertura/2.)) &&
		     (angulo > -180.)))
		  xmin=laser.fromhere.x-dd;
		else xmin=min(laser.fromhere.x, min(x1,x2));
		xmin=max(xmin,gg->lo.x);
	  
		if (((90.-gg->laser_apertura/2.) < angulo) && 
		    ( angulo < (90.+gg->laser_apertura/2.))) 
		  ymax=laser.fromhere.y+dd;
		else ymax=max(laser.fromhere.y, max(y1,y2));
		ymax=min(ymax,gg->hi.y);
		
		if (((-90.-gg->laser_apertura/2.) < angulo) && 
		    ( angulo < (-90.+gg->laser_apertura/2.))) ymin=laser.fromhere.y-dd;
		else ymin=min(laser.fromhere.y, min(y1,y2));
		ymin=max(ymin,gg->lo.y);
		
		/* Barremos directamente SOLO el area de interes */
		cxmin=(xmin-gg->lo.x+gg->resolucion/2.)/gg->resolucion;
		cxmax=(xmax-gg->lo.x+gg->resolucion/2.)/gg->resolucion;
		cymin=(ymin-gg->lo.y+gg->resolucion/2.)/gg->resolucion;
		cymax=(ymax-gg->lo.y+gg->resolucion/2.)/gg->resolucion;
		
		for(i=cymin;i<=cymax;i++)
		  for(j=cxmin;j<=cxmax;j++)
		    { if ((gg->map[i*gg->size+j].centro.x <= xmax) &&
			  (gg->map[i*gg->size+j].centro.x >= xmin) &&
			  (gg->map[i*gg->size+j].centro.y <= ymax) &&
			  (gg->map[i*gg->size+j].centro.y >= ymin) )
		      /* falls inside the laser beam area */
		      {
			e.punto.x=gg->map[i*gg->size+j].centro.x;
			e.punto.y=gg->map[i*gg->size+j].centro.y;
			
			radio2=(gg->map[i*gg->size+j].centro.x-laser.fromhere.x)*(gg->map[i*gg->size+j].centro.x-laser.fromhere.x)+(gg->map[i*gg->size+j].centro.y-laser.fromhere.y)*(gg->map[i*gg->size+j].centro.y-laser.fromhere.y);
			
			if ((gg->map[i*gg->size+j].centro.y!=laser.fromhere.y)||(gg->map[i*gg->size+j].centro.x!=laser.fromhere.x))
			  angulo2=atan2(gg->map[i*gg->size+j].centro.y-laser.fromhere.y,gg->map[i*gg->size+j].centro.x-laser.fromhere.x);
			else angulo2=0.;
			
			diferencia=angulo2/DEGTORAD-angulo;
			while((diferencia<=-180.)||(diferencia>180.))
			  {if (diferencia<=-180.) diferencia+=360.;
			  else if (diferencia>180.) diferencia-=360.;}
			diferencia=max(diferencia,-diferencia);
			
			deltar=min(dd*0.10,gg->resolucion);
		
			/* occupancy evidences in the arc of all the rays */
			if ((radio2 <= (dd+deltar/2.)*(dd+deltar/2.)) &&
			    (radio2 >= (dd-deltar/2.)*(dd-deltar/2.)) && 
			    (diferencia <= gg->laser_apertura/2.) && 
			    (lectura_no_obstaculo==0))
			  { 
			    if ((gg->regla==mayoria)||(gg->regla==ec_diferencial))
			      {e.o=gg->laser_o;
			      e.e=0.;}
			    actualiza_celdilla(gg,e);
			  }
			/* emptyness evidences inside the cone of the ray */
			else if ((radio2 < (radio-deltar/2.)*(radio-deltar/2.)) &&
			    (diferencia <= gg->laser_apertura/2.))
			  {
			    if ((gg->regla==mayoria)||(gg->regla==ec_diferencial))
			      {e.o=gg->laser_e;
			      e.e=0.;}
			    actualiza_celdilla(gg,e);
			  }
		      }
		    }
	      }


	    /* evidences for emptyness in the central axis */
	    delta = gg->resolucion/4.;
	    radio = radio-delta;
	    while(radio>0)
	      {
		x1 = laser.fromhere.x + radio * cos(DEGTORAD*angulo);
		y1 = laser.fromhere.y + radio * sin(DEGTORAD*angulo);
		cx=(x1-gg->lo.x+gg->resolucion/2.)/gg->resolucion;
		cy=(y1-gg->lo.y+gg->resolucion/2.)/gg->resolucion;
		
		if ((cx>=gg->size) || (cy>=gg->size) || (cx<0) || (cy<0)) ; /* cae fuera del grid */
		else{
		  e.punto.x=gg->map[cy*gg->size+cx].centro.x;
		  e.punto.y=gg->map[cy*gg->size+cx].centro.y;
		  
		  if ((gg->regla==mayoria)||(gg->regla==ec_diferencial))
		    {e.o=gg->laser_e;
		    e.e=0.;}
		  actualiza_celdilla(gg,e);
		}
		radio=radio-delta;
	      }
	}
    }

 else if (gg->laser_geometria==lobulo_denso)
   {
     incremento=180./gg->laser_muestras;
     xmin=xmax=laser.fromhere.x;
     ymin=ymax=laser.fromhere.y;
     for(u=0;u<gg->laser_muestras;u++)
       {
	 angulo=incremento*(u+azar[num_azar++%PSEUDOALEATORIO]);
	 /* angulo=incremento*(u+0.5); */
	 k=(int)ceil(angulo*laser.nummedidas/180.+0.5);
	 angulo=k*180./laser.nummedidas+laser.thetasensor;
	 radio=laser.distancias[k];
	 x1 = laser.fromhere.x + radio * cos(DEGTORAD*angulo);
	 y1 = laser.fromhere.y + radio * sin(DEGTORAD*angulo);
	 if (x1>xmax) xmax=x1;
	 if (x1<xmin) xmin=x1;
	 if (y1>ymax) ymax=y1;
	 if (y1<ymin) ymin=y1;
       }
     ymin=max(ymin,gg->lo.y);
     ymax=min(ymax,gg->hi.y);
     xmax=min(xmax,gg->hi.x);
     xmin=max(xmin,gg->lo.x);
	  
     /* Barremos directamente SOLO el area de interes */
     cxmin=(xmin-gg->lo.x+gg->resolucion/2.)/gg->resolucion;
     cxmax=(xmax-gg->lo.x+gg->resolucion/2.)/gg->resolucion;
     cymin=(ymin-gg->lo.y+gg->resolucion/2.)/gg->resolucion;
     cymax=(ymax-gg->lo.y+gg->resolucion/2.)/gg->resolucion;
     
     for(i=cymin;i<=cymax;i++)
       for(j=cxmin;j<=cxmax;j++)
	 { 
	   if ((gg->map[i*gg->size+j].centro.x <= xmax) &&
	       (gg->map[i*gg->size+j].centro.x >= xmin) &&
	       (gg->map[i*gg->size+j].centro.y <= ymax) &&
	       (gg->map[i*gg->size+j].centro.y >= ymin) )
	     {

	       e.punto.x=gg->map[i*gg->size+j].centro.x;
	       e.punto.y=gg->map[i*gg->size+j].centro.y;
	       
	       if ((gg->map[i*gg->size+j].centro.y!=laser.fromhere.y)||(gg->map[i*gg->size+j].centro.x!=laser.fromhere.x))
		 angulo2=atan2(gg->map[i*gg->size+j].centro.y-laser.fromhere.y,gg->map[i*gg->size+j].centro.x-laser.fromhere.x);
	       else angulo2=0.;
	       
	       diferencia=angulo2/DEGTORAD-laser.thetasensor;
	       while((diferencia<=-180.)||(diferencia>180.))
		 {if (diferencia<=-180.) diferencia+=360.;
		 else if (diferencia>180.) diferencia-=360.;}

	       if ((diferencia >= 0.)&&(diferencia <= 180.))
		 {
		   radio2=(gg->map[i*gg->size+j].centro.x-laser.fromhere.x)*(gg->map[i*gg->size+j].centro.x-laser.fromhere.x)+(gg->map[i*gg->size+j].centro.y-laser.fromhere.y)*(gg->map[i*gg->size+j].centro.y-laser.fromhere.y);
			
		   k=(int)floor(diferencia+0.5);	
		   dd=laser.distancias[k];
		   deltar=min(dd*0.10,gg->resolucion);

		   /* occupancy evidences in the arc of all the rays */
		   if ((radio2 <= (dd+deltar/2.)*(dd+deltar/2.)) &&
		       (radio2 >= (dd-deltar/2.)*(dd-deltar/2.)) && 
		       (dd<gg->laser_noobstacle))
		     { 
		       if ((gg->regla==mayoria)||(gg->regla==ec_diferencial))
			 {e.o=gg->laser_o;
			 e.e=0.;}
		       actualiza_celdilla(gg,e);
		     }
		   /* emptyness evidences inside the cone of the ray */
		   else if (radio2 < (dd-deltar/2.)*(dd-deltar/2.))
		     {if ((gg->regla==mayoria)||(gg->regla==ec_diferencial))
		     {e.o=gg->laser_e;
		     e.e=0.;}
		   actualiza_celdilla(gg,e);
		   }
		 }
	     }   
	 }
   }
}


void add_jamb(Tgrid *g, Tjamb_reading obs) 
{
  int cymax,cymin,cxmax,cxmin,cx,cy;
  float ymax, ymin, xmax, xmin, x1,x2,y1,y2;
  float angulo, radio2,radio, diferencia,delta;
  Tevidencia e;
  int i,j,independiente;
  Tjamb_reading *filtro;

  e.clock=obs.clock;

 if (g->visualjamb_filtra==independientes)
    {
      independiente=1; 
      filtro=g->visualjamb_filtro;
      for(i=MAXFILTRO;i>0;i--)
	/* El orden de recorrido influye en la eficiencia. Vamos de los mas
    recientes a los mas antiguos. Como se salta en cuanto se encuentra una
    observacion parecida, no es necesario recorrer la memoria completa antes de
     concluir que es dependiente */
	{

	  if (filtro[(g->num_visualjambfiltro+i)%MAXFILTRO].clock < 0)
	    continue;
	  /* que el tiempo de clock sea negativo se utiliza para las observaciones neutras en el filtro, que no casan con ninguna */
	  
	  diferencia=filtro[(g->num_visualjambfiltro+i)%MAXFILTRO].thetasensor-obs.thetasensor;
	  while((diferencia<=-180.)||(diferencia>180.))
	    {if (diferencia<=-180.) diferencia+=360.;
	    else if (diferencia>180.) diferencia-=360.;}
	  diferencia=max(diferencia,-diferencia);
	  if (diferencia>F_THETA) continue;

	  diferencia=filtro[(g->num_visualjambfiltro+i)%MAXFILTRO].fromhere.x-obs.fromhere.x;
	  diferencia=max(diferencia,-diferencia);
	  if (diferencia>F_DELTA) continue;

	  diferencia=filtro[(g->num_visualjambfiltro+i)%MAXFILTRO].fromhere.y-obs.fromhere.y;
	  diferencia=max(diferencia,-diferencia);
	  if (diferencia>F_DELTA) continue;

	  independiente=0;
	  /*	  printf("visualjamb repetida %d \n",k++);*/
	  break;
	}

      if (independiente==1) 
	{
	  filtro[g->num_visualjambfiltro].clock=obs.clock;
	  filtro[g->num_visualjambfiltro].thetasensor=obs.thetasensor;
	  filtro[g->num_visualjambfiltro].fromhere.x=obs.fromhere.x;
	  filtro[g->num_visualjambfiltro].fromhere.y=obs.fromhere.y;
	  g->num_visualjambfiltro=(g->num_visualjambfiltro++)%MAXFILTRO;
	  /*	  printf(" visualjamb independiente %d \n",k++);*/
	}
    }



 if ((g->visualjamb_filtra==todas)||
      ((g->visualjamb_filtra==independientes)&&(independiente==1)))
    {

 /* OJO como solo se admite una modificacion del estado de celdilla por instante el orden importa: primero inserta las evidencias de jamba, y luego las de no jamba.*/
  delta = g->resolucion/4.;
  for(i=0; i<obs.nummedidas; i++)
    {
      radio = g->visualjamb_rmax-delta;
      while(radio>0)
	{
	  x1 = obs.fromhere.x + radio * cos(DEGTORAD*(obs.thetasensor+g->visualjamb_aperture/2.-g->visualjamb_aperture*(float)obs.jambpixel[i]/(float)g->visualjamb_aperture_pixels));
	  y1 = obs.fromhere.y + radio * sin(DEGTORAD*(obs.thetasensor+g->visualjamb_aperture/2.-g->visualjamb_aperture*(float)obs.jambpixel[i]/(float)g->visualjamb_aperture_pixels));
	  cx=(x1-g->lo.x+g->resolucion/2.)/g->resolucion;
	  cy=(y1-g->lo.y+g->resolucion/2.)/g->resolucion;
      
	  if ((cx>=g->size) || (cy>=g->size) || (cx<0) || (cy<0) );
	  /* outside the grid */
	  else {
	    e.punto.x=g->map[cy*g->size+cx].centro.x;
	    e.punto.y=g->map[cy*g->size+cx].centro.y;
	    e.o=g->visualjamb_positive;
	    e.e=0.;
	    actualiza_celdilla(g,e);
	  }
	  radio=radio-delta;
	}
    }

  x1 = obs.fromhere.x + g->visualjamb_rmax * cos(DEGTORAD*(obs.thetasensor-g->visualjamb_aperture/2.));
  y1 = obs.fromhere.y + g->visualjamb_rmax * sin(DEGTORAD*(obs.thetasensor-g->visualjamb_aperture/2.));
  x2 = obs.fromhere.x + g->visualjamb_rmax * cos(DEGTORAD*(obs.thetasensor+g->visualjamb_aperture/2.));
  y2 = obs.fromhere.y + g->visualjamb_rmax * sin(DEGTORAD*(obs.thetasensor+g->visualjamb_aperture/2.));

  while((obs.thetasensor<-180.)||(obs.thetasensor>=180.))
    {if (obs.thetasensor<-180.) obs.thetasensor+=360.;
    else if (obs.thetasensor>=180.) obs.thetasensor-=360.;}
 /* WATCH OUT: regular angle values span [-180,180) */  

  if ((obs.thetasensor > - g->visualjamb_aperture/2.) && (obs.thetasensor<g->visualjamb_aperture/2.))
    xmax=obs.fromhere.x+g->visualjamb_rmax;
  else xmax=max(obs.fromhere.x, max(x1,x2));
  xmax=min(xmax,g->hi.x);
  
  if ((((180.-g->visualjamb_aperture/2.) < obs.thetasensor) && 
       (obs.thetasensor < 180.)) || 
      ((obs.thetasensor < (-180.+g->visualjamb_aperture/2.)) &&
       (obs.thetasensor >= -180.)))
    xmin=obs.fromhere.x-g->visualjamb_rmax;
  else xmin=min(obs.fromhere.x, min(x1,x2));
  xmin=max(xmin,g->lo.x);
  
  if (((90.-g->visualjamb_aperture/2.) < obs.thetasensor) && 
      ( obs.thetasensor < (90.+g->visualjamb_aperture/2.))) 
    ymax=obs.fromhere.y+g->visualjamb_rmax;
  else ymax=max(obs.fromhere.y, max(y1,y2));
  ymax=min(ymax,g->hi.y);
  
  if (((-90.-g->visualjamb_aperture/2.) < obs.thetasensor) &&
      ( obs.thetasensor < (-90.+g->visualjamb_aperture/2.)))
    ymin=obs.fromhere.y-g->visualjamb_rmax;
  else ymin=min(obs.fromhere.y, min(y1,y2));
  ymin=max(ymin,g->lo.y);
  
  /* Barremos directamente SOLO el area de interes.*/
  cxmin=(xmin-g->lo.x+g->resolucion/2.)/g->resolucion;
  cxmax=(xmax-g->lo.x+g->resolucion/2.)/g->resolucion;
  cymin=(ymin-g->lo.y+g->resolucion/2.)/g->resolucion;
  cymax=(ymax-g->lo.y+g->resolucion/2.)/g->resolucion;
  
  for(i=cymin;i<=cymax;i++)
    for(j=cxmin;j<=cxmax;j++)
      {
	if ((g->map[i*g->size+j].centro.x <= xmax) &&
	    (g->map[i*g->size+j].centro.x >= xmin) &&
	    (g->map[i*g->size+j].centro.y <= ymax) &&
	    (g->map[i*g->size+j].centro.y >= ymin) )
	  
	  {
	    e.punto.x=g->map[i*g->size+j].centro.x;
	    e.punto.y=g->map[i*g->size+j].centro.y;

	    radio2=(g->map[i*g->size+j].centro.x-obs.fromhere.x)*(g->map[i*g->size+j].centro.x-obs.fromhere.x)+(g->map[i*g->size+j].centro.y-obs.fromhere.y)*(g->map[i*g->size+j].centro.y-obs.fromhere.y);
	    
	    if ((g->map[i*g->size+j].centro.y!=obs.fromhere.y)||(g->map[i*g->size+j].centro.x-obs.fromhere.x))
	      angulo=atan2(g->map[i*g->size+j].centro.y-obs.fromhere.y,g->map[i*g->size+j].centro.x-obs.fromhere.x);
	    else angulo=0.;
	   
	    diferencia=angulo/DEGTORAD-obs.thetasensor;
	    while((diferencia<-180.)||(diferencia>=180.))
	      {if (diferencia<-180.) diferencia+=360.;
	      else if (diferencia>=180.) diferencia-=360.;}
	    diferencia=max(diferencia,-diferencia);

	    if (((g->visualjamb_aperture/2.)> diferencia) && 
		(radio2 < g->visualjamb_rmax*g->visualjamb_rmax))
	      {
		e.o=g->visualjamb_negative;
		e.e=0.;
		actualiza_celdilla(g,e);
	      }
	  }
      }
    }
}
