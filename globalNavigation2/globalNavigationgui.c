/* Form definition file generated with fdesign. */

#include "forms.h"
#include <stdlib.h>
#include "globalNavigationgui.h"

FD_globalNavigationgui *create_form_globalNavigationgui(void)
{
  FL_OBJECT *obj;
  FD_globalNavigationgui *fdui = (FD_globalNavigationgui *) fl_calloc(1, sizeof(*fdui));

  fdui->globalNavigationgui = fl_bgn_form(FL_NO_BOX, 1200, 720);
  obj = fl_add_box(FL_FRAME_BOX,0,0,1200,720,"");
    fl_set_object_lcolor(obj,FL_BLUE);
  fdui->canvasGL = obj = fl_add_glcanvas(FL_NORMAL_CANVAS,290,10,900,790,"");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
  fdui->exit = obj = fl_add_button(FL_NORMAL_BUTTON,30,30,70,40,"EXIT");
    fl_set_object_color(obj,FL_CYAN,FL_COL1);
    fl_set_object_lcolor(obj,FL_DODGERBLUE);
    fl_set_object_lstyle(obj,FL_NORMAL_STYLE+FL_ENGRAVED_STYLE);
  fdui->sala123 = obj = fl_add_checkbutton(FL_PUSH_BUTTON,30,230,70,20,"Sala Com�n");
  fdui->sala122 = obj = fl_add_checkbutton(FL_PUSH_BUTTON,30,260,70,20,"Desp. 122");
  fdui->sala120 = obj = fl_add_checkbutton(FL_PUSH_BUTTON,30,320,70,20,"Desp. 120");
  fdui->sala119 = obj = fl_add_checkbutton(FL_PUSH_BUTTON,30,350,70,20,"LibreFuck");
  fdui->sala118 = obj = fl_add_checkbutton(FL_PUSH_BUTTON,30,380,70,20,"LibreSoft");
  fdui->sala109 = obj = fl_add_checkbutton(FL_PUSH_BUTTON,160,220,70,20,"Desp. 109");
  fdui->sala110 = obj = fl_add_checkbutton(FL_PUSH_BUTTON,160,250,70,20,"Desp. 110");
  fdui->sala111 = obj = fl_add_checkbutton(FL_PUSH_BUTTON,160,280,70,20,"Desp. 111");
  fdui->sala112 = obj = fl_add_checkbutton(FL_PUSH_BUTTON,160,310,70,20,"Desp. 112");
  fdui->sala113 = obj = fl_add_checkbutton(FL_PUSH_BUTTON,160,340,70,20,"Desp. 113");
  fdui->sala114 = obj = fl_add_checkbutton(FL_PUSH_BUTTON,160,370,70,20,"Desp. 114");
  fdui->sala121 = obj = fl_add_checkbutton(FL_PUSH_BUTTON,30,290,70,20,"Desp. 121");
  fdui->sala117 = obj = fl_add_checkbutton(FL_PUSH_BUTTON,30,410,70,20,"Lab. Rob�tica");
  fdui->sala115 = obj = fl_add_checkbutton(FL_PUSH_BUTTON,160,400,70,20,"Desp. 115");
  fdui->sala116 = obj = fl_add_checkbutton(FL_PUSH_BUTTON,160,430,70,20,"LibreRock");
  fdui->stop = obj = fl_add_button(FL_NORMAL_BUTTON,110,30,70,40,"STOP");
    fl_set_object_color(obj,FL_RED,FL_RED);
    fl_set_object_lcolor(obj,FL_YELLOW);
    fl_set_object_lstyle(obj,FL_NORMAL_STYLE+FL_EMBOSSED_STYLE);
  fdui->ventanaA = obj = fl_add_free(FL_NORMAL_FREE,200,30,50,40,"",
			freeobj_ventanaA_handle);
  obj = fl_add_text(FL_NORMAL_TEXT,50,90,190,40,"Choose bureau:");
    fl_set_object_color(obj,FL_CHARTREUSE,FL_SPRINGGREEN);
    fl_set_object_lcolor(obj,FL_DODGERBLUE);
    fl_set_object_lsize(obj,FL_LARGE_SIZE);
    fl_set_object_lalign(obj,FL_ALIGN_CENTER|FL_ALIGN_INSIDE);
    fl_set_object_lstyle(obj,FL_NORMAL_STYLE+FL_EMBOSSED_STYLE);
  obj = fl_add_text(FL_NORMAL_TEXT,80,170,130,30,"Right Wing");
    fl_set_object_color(obj,FL_DODGERBLUE,FL_MCOL);
    fl_set_object_lcolor(obj,FL_CYAN);
    fl_set_object_lsize(obj,FL_MEDIUM_SIZE);
    fl_set_object_lalign(obj,FL_ALIGN_CENTER|FL_ALIGN_INSIDE);
    fl_set_object_lstyle(obj,FL_NORMAL_STYLE+FL_SHADOW_STYLE);
  obj = fl_add_text(FL_NORMAL_TEXT,80,490,130,30,"Left Wing");
    fl_set_object_color(obj,FL_DODGERBLUE,FL_MCOL);
    fl_set_object_lcolor(obj,FL_CYAN);
    fl_set_object_lsize(obj,FL_MEDIUM_SIZE);
    fl_set_object_lalign(obj,FL_ALIGN_CENTER|FL_ALIGN_INSIDE);
    fl_set_object_lstyle(obj,FL_NORMAL_STYLE+FL_SHADOW_STYLE);
  fdui->sala136 = obj = fl_add_checkbutton(FL_PUSH_BUTTON,30,540,70,20,"Desp. 136");
  fdui->sala135 = obj = fl_add_checkbutton(FL_PUSH_BUTTON,30,570,70,20,"Desp. 135");
  fdui->sala133 = obj = fl_add_checkbutton(FL_PUSH_BUTTON,30,630,70,20,"Desp. 133");
  fdui->sala132 = obj = fl_add_checkbutton(FL_PUSH_BUTTON,30,660,70,20,"Servidores");
  fdui->sala131 = obj = fl_add_checkbutton(FL_PUSH_BUTTON,30,690,70,20,"Desp. 131");
  fdui->salaBanios = obj = fl_add_checkbutton(FL_PUSH_BUTTON,160,540,70,20,"Ba�os");
  fdui->sala124 = obj = fl_add_checkbutton(FL_PUSH_BUTTON,160,570,70,20,"Desp. 124");
  fdui->sala125 = obj = fl_add_checkbutton(FL_PUSH_BUTTON,160,600,70,20,"Desp. 125");
  fdui->sala126 = obj = fl_add_checkbutton(FL_PUSH_BUTTON,160,630,70,20,"Desp. 126");
  fdui->sala127 = obj = fl_add_checkbutton(FL_PUSH_BUTTON,160,660,70,20,"Desp. 127");
  fdui->sala128 = obj = fl_add_checkbutton(FL_PUSH_BUTTON,160,690,70,20,"Desp. 128");
  fdui->sala134 = obj = fl_add_checkbutton(FL_PUSH_BUTTON,30,600,70,20,"Desp. 134");
  fdui->sala130 = obj = fl_add_checkbutton(FL_PUSH_BUTTON,30,720,70,20,"LADyR");
  fdui->sala129 = obj = fl_add_checkbutton(FL_PUSH_BUTTON,160,720,70,20,"Lab. Sistemas");
  fl_end_form();

  fdui->globalNavigationgui->fdui = fdui;

  return fdui;
}
/*---------------------------------------*/

