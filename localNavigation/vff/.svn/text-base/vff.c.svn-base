/*
 *  Copyright (C) 2006 José María Cañas Plaza 
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  Authors : José María Cañas Plaza <jmplaza@gsyc.escet.urjc.es>
 *  Authors : Darío Rodríguez de Diego <drd.sqki@gmail.com>
 */

#include "jde.h"
#include <forms.h>
#include "graphics_xforms.h"
#include "pioneer.h"
#include "vff.h"
#include "vffgui.h"
#include "fuzzylib.h"


#define DISPLAY_ROBOT 0x01UL
#define DISPLAY_SONARS 0x04UL
#define DISPLAY_LASER 0x08UL

int finish_flag=0; 

/*Gui declarations*/
Display *mydisplay;
int  *myscreen;

/*Gui callbacks*/
registerbuttons myregister_buttonscallback;
registerdisplay myregister_displaycallback;
deletebuttons mydelete_buttonscallback;
deletedisplay mydelete_displaycallback;


#define joystick_maxRotVel 30 /* deg/sec */
#define joystick_maxTranVel 500 /* mm/sec */


int vff_id=0; 
int vff_brothers[MAX_SCHEMAS];
arbitration vff_callforarbitration;
int vff_cycle=150; /* ms */

int *mylaser=NULL;
resumeFn laserresume;
suspendFn lasersuspend;

float *myencoders=NULL;
resumeFn encodersresume;
suspendFn encoderssuspend;

float *myv=NULL;
float *myw=NULL;
resumeFn motorsresume;
suspendFn motorssuspend;

FD_vffgui *fd_vffgui=NULL;
GC vffgui_gc;
Window  canvas_win;
unsigned long display_state;
int visual_refresh=FALSE;
int iteracion_display=0;
int canvas_mouse_button_pressed=0;
int mouse_button=0;
int robot_mouse_motion=0;
FL_Coord x_canvas,y_canvas,old_x_canvas,old_y_canvas;
float mouse_x, mouse_y;
Tvoxel vfftarget,oldvfftarget;
int mouse_new=0;

#define PUSHED 1
#define RELEASED 0 
#define FORCED_REFRESH 5000 /* ms */
/*Every forced_refresh the display is drawn from scratch. If it is too small it will cause flickering with grid display. No merece la pena una hebra de "display_lento" solo para repintar completamente la pantalla. */

float   escala, width, height;
int trackrobot=FALSE;
float odometrico[5];
#define RANGO_MAX 20000. /* en mm */
#define RANGO_MIN 500. /* en mm */ 
#define RANGO_INICIAL 4000. /* en mm */
float rango=(float)RANGO_INICIAL; /* Rango de visualizacion en milimetros */

#define EGOMAX NUM_SONARS+5
XPoint ego[EGOMAX];
int numego=0;
int visual_delete_ego=FALSE;

XPoint laser_dpy[NUM_LASER];
int visual_delete_laser=FALSE;

XPoint us_dpy[NUM_SONARS*2];
int visual_delete_us=FALSE;

/*Constantes definicion fuerza*/
#define MODULOFATRAC 1 
#define ALPHA 1 /*Modula la fuerza de atraccion*/
#define BETA 8 /*Modula la fuerza de repulsion*/
/*Constantes hacer los dibujos*/
#define AUMENTAFUERZA 1000
#define AUMENTAFUERZAREP AUMENTAFUERZA*8
#define ATRAC_COLOR FL_GREEN
#define REPUL_COLOR FL_RED
#define RESUL_COLOR FL_BLACK
#define VENTANASEG_COLOR FL_YELLOW
/*Constantes para la ventana de seguridad*/
#define ANCHO 150
#define ALTO 600
/*Constantes para el movimiento*/
#define APROXANG 10
#define APROXLLEGADA 175

/*Estados definidos para el robot*/
enum TEstados {normal,finalizado,lejos};

typedef struct
{
	int fin;
	int libre;
	int estado;
}TControl;

typedef struct
{
	float distancia;
	float modresul;
	float argresul;
}TVar;

/*Variables de los botones del gui*/
int draw_forcefield;/* TRUE = pushed*/
int draw_securitywindow;/* TRUE = pushed*/
int move = FALSE;/*TRUE = pushed*/

/*Variables globales*/
int k;
Tvoxel atrac,repul,resul,posrobot,objetivo;
TVar fuzzyvar;/*Variables de la logica borrosa*/
static TControl control={0,1,normal};/*Variables para el control de las transiciones*/
char velocidades[8];

const char *range(FL_OBJECT *ob, double value, int prec)
{
static char buf[32];

sprintf(buf,"%.1f",value/1000.);
return buf;
}

int xy2canvas(Tvoxel point, XPoint* grafico)
     /* return -1 if point falls outside the canvas */
{
float xi, yi;

xi = (point.x * odometrico[3] - point.y * odometrico[4] + odometrico[0])*escala;
yi = (point.x * odometrico[4] + point.y * odometrico[3] + odometrico[1])*escala;
/* Con esto cambiamos al sistema de referencia de visualizacion, centrado en algun punto xy y con alguna orientacion definidos por odometrico. Ahora cambiamos de ese sistema al del display, donde siempre hay un desplazamiento a la esquina sup. izda. y que las y se cuentan para abajo. */

grafico->x = xi + width/2;
grafico->y = -yi + height/2;

 if ((grafico->x <0)||(grafico->x>width)) return -1; 
 if ((grafico->y <0)||(grafico->y>height)) return -1; 
 return 0;
}

int absolutas2relativas(Tvoxel in, Tvoxel *out)
/*  Calcula la posicion relativa respecto del robot de un punto absoluto. El robot se encuentra en robot[0], robot[1] con orientacion robot[2] respecto al sistema de referencia absoluto
*/ 
{
  if (out!=NULL && myencoders!=NULL){
     (*out).x = in.x*myencoders[3] + in.y*myencoders[4] -
           myencoders[0]*myencoders[3] - myencoders[1]*myencoders[4];
     (*out).y = in.y*myencoders[3] - in.x*myencoders[4] +
           myencoders[0]*myencoders[4] - myencoders[1]*myencoders[3];
     return 0;
  }
  return 1;
}

int relativas2absolutas(Tvoxel in, Tvoxel *out)
/*  Calcula la posicion absoluta de un punto expresado en el sistema de coordenadas solidario al robot. El robot se encuentra en robot[0], robot[1] con orientacion robot[2] respecto al sistema de referencia absoluto
*/ 
{
  if (out!=NULL && myencoders!=NULL){
     (*out).x = in.x*myencoders[3] - in.y*myencoders[4] + myencoders[0];
     (*out).y = in.y*myencoders[3] + in.x*myencoders[4] + myencoders[1];
     return 0;
  }
  return 1;
}

int pintaSegmento(Tvoxel a, Tvoxel b, int color)
  /* colores: FL_PALEGREEN */
   {
     XPoint aa,bb;

     fl_set_foreground(vffgui_gc,color);
     xy2canvas(a,&aa);
     xy2canvas(b,&bb);
     XDrawLine(mydisplay,canvas_win,vffgui_gc,aa.x,aa.y,bb.x,bb.y);
     return 0;
   }
/****************************************/
/*CALCULO VENTANA SEGURIDAD             */
/****************************************/
/*Calcula la posicion del robot*/
void posicionRobot(Tvoxel *pos)
{
	pos->x = robot[0];
	pos->y = robot[1];
}
/*Calcula el objetivo que pulsas con el raton*/
void calcularObjetivo(Tvoxel *objetivo)
{
	objetivo->x = mouse_x;
	objetivo->y = mouse_y;
}
/*Calcula la medida del segmento corresondiente a los grados indicados y dado un ancho y un alto especifico*/
float medidaSegmento(float ancho,float alto,int grados)
{
	float aux;
	if (grados == 90)
	{	
		aux = alto;
	}
	else
	{
		aux = ancho*cos(grados*DEGTORAD);
	}
	return aux;
}
/*Calcula si existe o no una ventana de seguridad de medidas anch y alt a traves del laser*/
int ventanaSeguridad(float *laser,float anch,float alt)
{
	int i, seguro;
	float medsegmento;
	i = 0;
	seguro = 1;
	while ((i < NUM_LASER) && (seguro == 1))
	{
		medsegmento = medidaSegmento(anch,alt,i);
		seguro = (laser[i] >= medsegmento);
		i = i+1;
	} 
	return seguro;
}
/*******************************************/
/*CALCULO DE FUERZAS                       */
/*******************************************/
/*Calculamos la fuerza de atraccion en coordenadas cartesianas*/
void fatraccion(Tvoxel *fatrac,Tvoxel objetivo)
{
	Tvoxel aux,auxrel;
	float modulo,arg;
	aux.x = objetivo.x;
	aux.y = objetivo.y;
	absolutas2relativas(aux,&auxrel);
	modulo = MODULOFATRAC;
	arg = atan2(auxrel.y,auxrel.x);
	fatrac->x = modulo*cos(arg);
	fatrac->y = modulo*sin(arg);
}
/*Calcula la fuerza de repulsion dado un haz de laser definido por su medida y el grado al que corresponde*/
void freplaser(float medlaser, int grad,Tvoxel *frep)
{
	float modulo, arg;
	modulo = (1/(medlaser+1));
	arg =((grad*DEGTORAD)-(PI/2))+PI;/*PROBLEMA PORQUE SE ATRAE HACIA DONDE HAY MÁS FUERZA EN VEZ DE REPELERSE*/
	frep->x = modulo*cos(arg);
	frep->y = modulo*sin(arg);
}
/*Calcula la fuerza de repulsion total que ejercen los objetos detectados con el laser*/
void freplaserTotal(float *laser,Tvoxel *fTotal)
{
	Tvoxel fuerzai,acumulador;
	int i;
	acumulador.x = 0;
	acumulador.y = 0;
	for (i=0;i<NUM_LASER;i++)
	{
		freplaser(laser[i],i,&fuerzai);
		acumulador.x = acumulador.x + fuerzai.x;	
		acumulador.y = acumulador.y + fuerzai.y;
	}
	fTotal->x = acumulador.x;
	fTotal->y = acumulador.y;
}
/*Modula la fuerza aumentandola k veces*/
void modulaFuerza(Tvoxel *fuerza,float k)
{
	fuerza->x = fuerza->x*k;
	fuerza->y = fuerza->y*k;
}
/*Calcula la fuerza resultante de la atraccion y la repulsion con las fuerzas moduladas por los valoresd de a y b*/
void fresultante(Tvoxel fatrac,Tvoxel frepul,Tvoxel *fresul,float a,float b)
{
	fresul->x = a*fatrac.x + b*frepul.x;
	fresul->y = a*fatrac.y + b*frepul.y;
}
/*Devuelve 1 si se alcanza el objetivo, 0 sino esta dentro del objetivo*/
int llegada(Tvoxel objetivo,float radio,Tvoxel posrobot)
{
	int aux;
	aux = ((posrobot.x <= objetivo.x+radio) && (posrobot.x >= objetivo.x-radio) && (posrobot.y <= objetivo.y+radio) && (posrobot.y >= objetivo.y-radio));
	return aux;
}
/*Estado de llegada al destino*/
void estado_finalizado(TControl *cont,float **v,float **w)
{
	if ((cont->fin == 0))/*Comprobamos por si estamos en normal*/
	{
		cont->estado = normal;
	}
	**v = 0;
	**w = 0;
}
/*Implementacion del algoritmo de movimiento VFF con logica borrosa*/
void movimientofuzzy(char *ruta,TVar *var,float **v,float **w)
{	
	static int archLeido = 0;
	if (archLeido == 0)
	{
		k=fc_open(ruta);
	}
	if ((k != -1) && (archLeido == 0))
	{
		fc_link(k,"modulo",&var->modresul);
		fc_link(k,"argumento",&var->argresul);
		fc_link(k,"distancia",&var->distancia);
		archLeido = 1;
		printf("**Fuzzy logic loaded**\n");
	}
	printf("Angresul = %f\n",var->argresul);
	printf("Modresul = %f\n",var->modresul);
	fc_output(k,"vlineal",*v);
	fc_output(k,"vangular",*w);
	printf("Vlineal = %f\n",**v);
	printf("Vangular = %f\n",**w);
}
/*Calcula la distancia entre dos puntos*/
float calcularDistancia(Tvoxel a,Tvoxel b)
{
	float aux;
	aux = sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));
	return aux;
}
void estado_normal(char *ruta,TControl *cont,Tvoxel resul,TVar *var,float **v,float **w)
{
	if ((cont->fin == 1) && (cont->libre == 1))/*Comprobamos por si hemos llegado al destino*/
	{
		**v = 0;
		**w = 0;
		cont->estado = finalizado;
	}
	else
	{
		var->argresul = (atan2(resul.y,resul.x))*RADTODEG;
		var->modresul = sqrt((resul.x*resul.x)+(resul.y*resul.y))*100;
		var->distancia = calcularDistancia(posrobot,objetivo);
		printf("Distance = %f\n",var->distancia);
		movimientofuzzy("/home/dario/4.2.1/schemas/VFF/mov.fzz",var,v,w);
	}
}
/****************************
*  Componatamiento del VFF  *
****************************/
void vff_iteration()
{
  int i;
	if (move == TRUE)
	{
  	speedcounter(vff_id);
  	/* printf("vff iteration %d\n",d++);*/

  	if (myencoders!=NULL){
    	 for (i=0;i<5; i++)
    	   robot[i]= myencoders[i];
  	}

  	if (mylaser!=NULL){
    	 for (i=0; i<NUM_LASER; i++)
    	   laser[i]= mylaser[i];
  	}
		posicionRobot(&posrobot);		
		calcularObjetivo(&objetivo);
		fatraccion(&atrac,objetivo);
		freplaserTotal(laser,&repul);
		fresultante(atrac,repul,&resul,ALPHA,BETA);
		control.fin = llegada(objetivo,APROXLLEGADA,posrobot);
		control.libre = ventanaSeguridad(laser,ANCHO,ALTO);
		if (control.estado == normal)
		{
			estado_normal("/home/dario/4.2.1/schemas/VFF/mov.fzz",&control,resul,&fuzzyvar,&myv,&myw);
			printf("State normal\n");
		}
		if (control.estado == finalizado) 
		{
			estado_finalizado(&control,&myv,&myw);
			printf("State ended\n");
		}
	}
	else
	{
		*myv = 0;
		*myw = 0;
	}
}

void vff_suspend()
{
  pthread_mutex_lock(&(all[vff_id].mymutex));
  put_state(vff_id,slept);
  all[vff_id].children[(*(int *)myimport("laser","id"))]=FALSE;
  all[vff_id].children[(*(int *)myimport("motors","id"))]=FALSE;
  all[vff_id].children[(*(int *)myimport("encoders","id"))]=FALSE;
  lasersuspend();
  encoderssuspend();
  motorssuspend();
  printf("vff: off\n");
  pthread_mutex_unlock(&(all[vff_id].mymutex));
}


void vff_resume(int father, int *brothers, arbitration fn)
{
  int i;
 
  pthread_mutex_lock(&(all[vff_id].mymutex));
  /* this schema resumes its execution with no children at all */
  for(i=0;i<MAX_SCHEMAS;i++) all[vff_id].children[i]=FALSE;
 
  all[vff_id].father=father;
  if (brothers!=NULL)
    {
      for(i=0;i<MAX_SCHEMAS;i++) vff_brothers[i]=-1;
      i=0;
      while(brothers[i]!=-1) {vff_brothers[i]=brothers[i];i++;}
    }
  vff_callforarbitration=fn;
  put_state(vff_id,notready);

  mylaser=(int *)myimport("laser","laser");
  laserresume=(resumeFn)myimport("laser","resume");
  lasersuspend=(suspendFn)myimport("laser","suspend");

  myencoders=(float *)myimport("encoders","jde_robot");
  encodersresume=(resumeFn)myimport("encoders","resume");
  encoderssuspend=(suspendFn)myimport("encoders","suspend");

  myv=(float *)myimport("motors","v");
  myw=(float *)myimport("motors","w");
  motorsresume=(resumeFn)myimport("motors","resume");
  motorssuspend=(suspendFn)myimport("motors","suspend");

  printf("vff: on\n");
  pthread_cond_signal(&(all[vff_id].condition));
  pthread_mutex_unlock(&(all[vff_id].mymutex));
}

void *vff_thread(void *not_used) 
{
  struct timeval a,b;
  long diff, next;

  for(;finish_flag==0;)
    {
      pthread_mutex_lock(&(all[vff_id].mymutex));

      if (all[vff_id].state==slept) 
	{
	  v=0; w=0;
	  pthread_cond_wait(&(all[vff_id].condition),&(all[vff_id].mymutex));
	  pthread_mutex_unlock(&(all[vff_id].mymutex));
	}
      else 
	{
	  /* check preconditions. For now, preconditions are always satisfied*/
	  if (all[vff_id].state==notready) put_state(vff_id,ready);
	  else if (all[vff_id].state==ready)	  /* check brothers and arbitrate. For now this is the only winner */
	    { 
	      put_state(vff_id,winner);
	      v=0; w=0; 
	      /* start the winner state from controlled motor values */ 
	      all[vff_id].children[(*(int *)myimport("laser","id"))]=TRUE;
	      all[vff_id].children[(*(int *)myimport("motors","id"))]=TRUE;
	      all[vff_id].children[(*(int *)myimport("encoders","id"))]=TRUE;
	      laserresume(vff_id,NULL,NULL);
	      encodersresume(vff_id,NULL,NULL);
	      motorsresume(vff_id,NULL,NULL);
	    }	  
	  else if (all[vff_id].state==winner);

	  if (all[vff_id].state==winner)
	    /* I'm the winner and must execute my iteration */
	    {
	      pthread_mutex_unlock(&(all[vff_id].mymutex));
	      gettimeofday(&a,NULL);
	      vff_iteration();
	      gettimeofday(&b,NULL);  

	      diff = (b.tv_sec-a.tv_sec)*1000000+b.tv_usec-a.tv_usec;
	      next = vff_cycle*1000-diff-10000; 
	      if (next>0) 
		/* discounts 10ms taken by calling usleep itself */
		usleep(vff_cycle*1000-diff);
	      else 
		/* just let this iteration go away. overhead time negligible */
		{printf("time interval violated: vff\n"); 
		usleep(vff_cycle*1000);
		}
	    }
	  else 
	    {
	      pthread_mutex_unlock(&(all[vff_id].mymutex));
	      usleep(vff_cycle*1000);
	    }
	}
    }
}

void vff_init(){
   if (myregister_buttonscallback==NULL){
      if ((myregister_buttonscallback=(registerbuttons)myimport ("graphics_xforms", "register_buttonscallback"))==NULL){
         printf ("I can't fetch register_buttonscallback from graphics_xforms\n");
         jdeshutdown(1);
      }
      if ((mydelete_buttonscallback=(deletebuttons)myimport ("graphics_xforms", "delete_buttonscallback"))==NULL){
         printf ("I can't fetch delete_buttonscallback from graphics_xforms\n");
         jdeshutdown(1);
      }
      if ((myregister_displaycallback=(registerdisplay)myimport ("graphics_xforms", "register_displaycallback"))==NULL){
         printf ("I can't fetch register_displaycallback from graphics_xforms\n");
         jdeshutdown(1);
      }
      if ((mydelete_displaycallback=(deletedisplay)myimport ("graphics_xforms", "delete_displaycallback"))==NULL){
         jdeshutdown(1);
         printf ("I can't fetch delete_displaycallback from graphics_xforms\n");
      }
   }

   if ((myscreen=(int *)myimport("graphics_xforms", "screen"))==NULL){
      fprintf (stderr, "teleoperator: I can't fetch screen from graphics_xforms\n");
      jdeshutdown(1);
   }
   if ((mydisplay=(Display *)myimport("graphics_xforms", "display"))==NULL){
      fprintf (stderr, "teleoperator: I can't fetch display from graphics_xforms\n");
      jdeshutdown(1);
   }
   init_pioneer();
}

void vff_stop()
{
  finish_flag=1;
  if (fd_vffgui!=NULL)
    {
      if (all[vff_id].guistate==on){
        vff_guisuspend();
        all[vff_id].guistate=off;
      }
    }
  printf ("vff close\n");
}

void vff_startup()
{
  pthread_mutex_lock(&(all[vff_id].mymutex));
  myexport("vff","id",&vff_id);
  myexport("vff","resume",(void *) &vff_resume);
  myexport("vff","suspend",(void *) &vff_suspend);
  printf("vff schema started up\n");
  put_state(vff_id,slept);
  pthread_create(&(all[vff_id].mythread),NULL,vff_thread,NULL);
  vff_init();
  pthread_mutex_unlock(&(all[vff_id].mymutex));

}

void vff_guibuttons(void *obj1)
{
  FL_OBJECT *obj=(FL_OBJECT *)obj1;
 
  if (obj == fd_vffgui->exit) jdeshutdown(0);
  else if (obj == fd_vffgui->escala)  
    {  rango=fl_get_slider_value(fd_vffgui->escala);
    visual_refresh = TRUE; /* activa el flag que limpia el fondo de la pantalla y repinta todo */ 
    escala = width /rango;}
  else if (obj == fd_vffgui->track_robot) 
    {if (fl_get_button(obj)==PUSHED) trackrobot=TRUE;
    else trackrobot=FALSE;
    } 
  else if (obj== fd_vffgui->center)
    /* Se mueve 10%un porcentaje del rango */
    {
      odometrico[0]+=rango*(fl_get_positioner_xvalue(fd_vffgui->center)-0.5)*(-2.)*(0.1);
      odometrico[1]+=rango*(fl_get_positioner_yvalue(fd_vffgui->center)-0.5)*(-2.)*(0.1);
      fl_set_positioner_xvalue(fd_vffgui->center,0.5);
      fl_set_positioner_yvalue(fd_vffgui->center,0.5);
      visual_refresh=TRUE; 
		}
	if (obj == fd_vffgui->securitywindow) 
		{    
		if (fl_get_button(obj)==PUSHED) draw_securitywindow=TRUE;
    else draw_securitywindow=FALSE;
		}
	if (obj == fd_vffgui->forcefield)
		{
		if (fl_get_button(obj)==PUSHED) draw_forcefield=TRUE;
    else draw_forcefield=FALSE;
		}
	if (obj == fd_vffgui->go)
		{
		if (fl_get_button(obj)==PUSHED) move=TRUE;
    else move=FALSE;
		}
}
/****************************************/
/*IMPLEMENTACION LA VISUALIZACION       */
/****************************************/
/*Pinta los vectores correspondientes al campo de fuerzas*/
void pintarCampoFuerzas(Tvoxel atrac,Tvoxel repul,Tvoxel resul,Tvoxel posrobot)
{
	static Tvoxel atracant,repulant,resulant,posant;
	/*Borramos los segmentos anteriores*/
	pintaSegmento(atracant,posant,FL_WHITE);
	pintaSegmento(repulant,posant,FL_WHITE);
	pintaSegmento(resulant,posant,FL_WHITE);
	/*Calculamos y pintamos los segmentos actuales*/
	modulaFuerza(&atrac,AUMENTAFUERZA);
	modulaFuerza(&repul,AUMENTAFUERZAREP);
	modulaFuerza(&resul,AUMENTAFUERZA);
	relativas2absolutas(atrac,&atrac);
	relativas2absolutas(repul,&repul);
	relativas2absolutas(resul,&resul);
	atracant = atrac;
	repulant = repul;
	resulant = resul;
	posant = posrobot;
  pintaSegmento(atrac,posrobot,ATRAC_COLOR);
	pintaSegmento(repul,posrobot,REPUL_COLOR);
	pintaSegmento(resul,posrobot,RESUL_COLOR);	
}
/*Borra los vectores del campo de fuerza*/
void borrarCampoFuerzas(Tvoxel atrac,Tvoxel repul,Tvoxel resul,Tvoxel posrobot)
{
	/*Calculamos y borramos los segmentos actuales*/
	modulaFuerza(&atrac,AUMENTAFUERZA);
	modulaFuerza(&repul,AUMENTAFUERZAREP);
	modulaFuerza(&resul,AUMENTAFUERZA);
	relativas2absolutas(atrac,&atrac);
	relativas2absolutas(repul,&repul);
	relativas2absolutas(resul,&resul);
	pintaSegmento(atrac,posrobot,FL_WHITE);
	pintaSegmento(repul,posrobot,FL_WHITE);
	pintaSegmento(resul,posrobot,FL_WHITE);
}
/*Pinta la ventana de seguridad*/
void pintarVentanaSeguridad(float ancho,float alto)
{	
	static Tvoxel ceroant,unoant,dosant,tresant;
	Tvoxel cero,uno,dos,tres;	
	float angulo,medida;
	angulo = atan2(alto,ancho);
	medida = sqrt((ancho*ancho)+(alto*alto));
	/*Borramos los segmentos anteriores*/
	pintaSegmento(ceroant,unoant,FL_WHITE);
	pintaSegmento(unoant,dosant,FL_WHITE);
	pintaSegmento(dosant,tresant,FL_WHITE);
	/*Calculamos y pintamos los segmentos actuales*/
	cero.x = 0;
	cero.y = ancho*sin(-PI/2);
	uno.x = medida*cos(angulo-PI/2);
	uno.y = medida*sin(angulo-PI/2);
	dos.x = uno.x;
	dos.y = -uno.y;
	tres.x = 0;
	tres.y = ancho*sin(PI/2);
	relativas2absolutas(cero,&cero);
	relativas2absolutas(uno,&uno);
 	relativas2absolutas(dos,&dos);
	relativas2absolutas(tres,&tres);
	ceroant = cero;
	unoant = uno;
	dosant = dos;
	tresant = tres;
  pintaSegmento(cero,uno,VENTANASEG_COLOR);
	pintaSegmento(uno,dos,VENTANASEG_COLOR);
	pintaSegmento(dos,tres,VENTANASEG_COLOR);
} 
/*Borra la ventana de seguridad*/
void borrarVentanaSeguridad(float ancho,float alto)
{
	Tvoxel cero,uno,dos,tres;	
	float angulo,medida;
	angulo = atan2(alto,ancho);
	medida = sqrt((ancho*ancho)+(alto*alto));
	/*Calculamos y pintamos los segmentos actuales*/
	cero.x = 0;
	cero.y = ancho*sin(-PI/2);
	uno.x = medida*cos(angulo-PI/2);
	uno.y = medida*sin(angulo-PI/2);
	dos.x = uno.x;
	dos.y = -uno.y;
	tres.x = 0;
	tres.y = ancho*sin(PI/2);
	relativas2absolutas(cero,&cero);
	relativas2absolutas(uno,&uno);
 	relativas2absolutas(dos,&dos);
	relativas2absolutas(tres,&tres);
  pintaSegmento(cero,uno,FL_WHITE);
	pintaSegmento(uno,dos,FL_WHITE);
	pintaSegmento(dos,tres,FL_WHITE);
}
void vff_guidisplay()
{
  char text[80]="";
  static float k=0;
  int i;
  Tvoxel aa,bb;
  static XPoint targetgraf;
  XPoint a,b;

  /* slow refresh of the complete vff gui, needed because incremental refresh misses window occlusions */
  if (iteracion_display*vff_cycle>FORCED_REFRESH) 
    {iteracion_display=0;
    visual_refresh=TRUE;
    }
  else iteracion_display++;

  k=k+1.;
  sprintf(text,"%.1f",k);
  fl_set_object_label(fd_vffgui->fps,text);

  fl_winset(canvas_win); 
  
  
  if ((trackrobot==TRUE)&&
      ((fabs(myencoders[0]+odometrico[0])>(rango/4.))||
       (fabs(myencoders[1]+odometrico[1])>(rango/4.))))
 {	
    odometrico[0]=-myencoders[0];
    odometrico[1]=-myencoders[1];
    visual_refresh = TRUE;
 }
 
 
 if (visual_refresh==TRUE){
    fl_rectbound(0,0,width,height,FL_WHITE);
    XFlush(mydisplay);
 }

  /* the grid at the floor */
  fl_set_foreground(vffgui_gc,FL_LEFT_BCOL); 
  for(i=0;i<31;i++)
    {
      aa.x=-15000.+(float)i*1000;
      aa.y=-15000.;
      bb.x=-15000.+(float)i*1000;
      bb.y=15000.;
      xy2canvas(aa,&a);
      xy2canvas(bb,&b);
      XDrawLine(mydisplay,canvas_win,vffgui_gc,a.x,a.y,b.x,b.y);
      aa.y=-15000.+(float)i*1000;
      aa.x=-15000.;
      bb.y=-15000.+(float)i*1000;
      bb.x=15000.;
      xy2canvas(aa,&a);
      xy2canvas(bb,&b);
      XDrawLine(mydisplay,canvas_win,vffgui_gc,a.x,a.y,b.x,b.y);
    }
  /* fl_set_foreground(vffgui_gc,FL_RIGHT_BCOL); */
    
  /* VISUALIZACION de una instantanea laser*/
  if ((((display_state&DISPLAY_LASER)!=0)&&(visual_refresh==FALSE))
      || (visual_delete_laser==TRUE))
    {  
      fl_set_foreground(vffgui_gc,FL_WHITE); 
      /* clean last laser, but only if there wasn't a total refresh. In case of total refresh the white rectangle already cleaned all */
      /*for(i=0;i<NUM_LASER;i++) XDrawPoint(display,canvas_win,vffgui_gc,laser_dpy[i].x,laser_dpy[i].y);*/
      XDrawPoints(mydisplay,canvas_win,vffgui_gc,laser_dpy,NUM_LASER,CoordModeOrigin);
    }
 
  if ((display_state&DISPLAY_LASER)!=0){
    /* check to avoid the segmentation fault in case GUI is activated before the schema imports mylaser */
    if (mylaser!=NULL){
    for(i=0;i<NUM_LASER;i++)
      {
				laser2xy(i,mylaser[i],&aa, myencoders);
				xy2canvas(aa,&laser_dpy[i]);
      }
    fl_set_foreground(vffgui_gc,FL_BLUE);
    /*for(i=0;i<NUM_LASER;i++) XDrawPoint(display,canvas_win,vffgui_gc,laser_dpy[i].x,laser_dpy[i].y);*/
    XDrawPoints(mydisplay,canvas_win,vffgui_gc,laser_dpy,NUM_LASER,CoordModeOrigin);
  }
  }

  
  /* VISUALIZACION: pintar o borrar de el PROPIO ROBOT.
     Siempre hay un repintado total. Esta es la ultima estructura que se se pinta, para que ninguna otra se solape encima */
  
  if ((((display_state&DISPLAY_ROBOT)!=0) &&(visual_refresh==FALSE))
      || (visual_delete_ego==TRUE))
    {  
      fl_set_foreground(vffgui_gc,FL_WHITE); 
      /* clean last robot, but only if there wasn't a total refresh. In case of total refresh the white rectangle already cleaned all */
      for(i=0;i<numego;i++) XDrawLine(mydisplay,canvas_win,vffgui_gc,ego[i].x,ego[i].y,ego[i+1].x,ego[i+1].y);
      
    }
  
  if ((display_state&DISPLAY_ROBOT)!=0){
    fl_set_foreground(vffgui_gc,FL_MAGENTA);
    /* relleno los nuevos */
    us2xy(15,0.,0.,&aa, myencoders);
    xy2canvas(aa,&ego[0]);
    us2xy(3,0.,0.,&aa, myencoders);
    xy2canvas(aa,&ego[1]);
    us2xy(4,0.,0.,&aa, myencoders);
    xy2canvas(aa,&ego[2]);
    us2xy(8,0.,0.,&aa, myencoders);
    xy2canvas(aa,&ego[3]);
    us2xy(15,0.,0.,&aa, myencoders);
    xy2canvas(aa,&ego[EGOMAX-1]);
    for(i=0;i<NUM_SONARS;i++)
      {
	us2xy((15+i)%NUM_SONARS,0.,0.,&aa, myencoders); /* Da en el Tvoxel aa las coordenadas del sensor, pues es distancia 0 */
	xy2canvas(aa,&ego[i+4]);       
      }
    
    /* pinto los nuevos */
    numego=EGOMAX-1;
    for(i=0;i<numego;i++) XDrawLine(mydisplay,canvas_win,vffgui_gc,ego[i].x,ego[i].y,ego[i+1].x,ego[i+1].y);
  }


 
  /* visualization of VFF target */
       if ((oldvfftarget.x!=vfftarget.x)||(oldvfftarget.y!=vfftarget.y))
	/* the target has changed, do its last position must be cleaned from the canvas */
	{
	  fl_set_foreground(vffgui_gc,FL_WHITE);
	  XDrawLine(mydisplay,canvas_win,vffgui_gc,targetgraf.x-5,targetgraf.y,targetgraf.x+5,targetgraf.y);
	  XDrawLine(mydisplay,canvas_win,vffgui_gc,targetgraf.x,targetgraf.y-5,targetgraf.x,targetgraf.y+5);
	}
      fl_set_foreground(vffgui_gc,FL_RED);
      xy2canvas(vfftarget,&targetgraf);
      XDrawLine(mydisplay,canvas_win,vffgui_gc,targetgraf.x-5,targetgraf.y,targetgraf.x+5,targetgraf.y);
      XDrawLine(mydisplay,canvas_win,vffgui_gc,targetgraf.x,targetgraf.y-5,targetgraf.x,targetgraf.y+5);

	/*Escribimos las velocidades en los campos text del GUI*/
	
	sprintf(velocidades, "%.1f", *myv);/*Convierte un float en string*/
	fl_set_object_label(fd_vffgui->vspeed,velocidades);

	sprintf(velocidades, "%.1f", *myw);/*Convierte un float en string*/
  fl_set_object_label(fd_vffgui->wspeed,velocidades);

	/*Actualizamos el letrero de objetivo alcanzado*/
	if (control.fin == 1)
	{
		fl_set_object_lcolor(fd_vffgui->reachedobjetive,FL_GREEN);
	}
	else
	{
		fl_set_object_lcolor(fd_vffgui->reachedobjetive,FL_RED);
	}
	/*Pintamos la ventana de seguridad*/
  if (draw_securitywindow == TRUE)
	{
		pintarVentanaSeguridad(ANCHO,ALTO);/*pintar celda de seguridad en amarillo*/
	}
	else
	{
		borrarVentanaSeguridad(ANCHO,ALTO);/*borramos la ventana de seguridad*/
 	}
	/*Pintamos el campo de fuerza*/
	if (draw_forcefield == TRUE)
	{
		pintarCampoFuerzas(atrac,repul,resul,posrobot);/*pintamos el campo de fuerzas del VFF*/
	}
	else
	{
		borrarCampoFuerzas(atrac,repul,resul,posrobot);/*borramos el campo de fuerzas del VFF*/
	}

  /* clear all flags. If they were set at the beginning, they have been already used in this iteration */
  visual_refresh=FALSE;
  visual_delete_us=FALSE; 
  visual_delete_laser=FALSE; 
  visual_delete_ego=FALSE;
}

/* callback function for button pressed inside the canvas object*/
int button_pressed_on_micanvas(FL_OBJECT *ob, Window win, int win_width, int win_height, XEvent *xev, void *user_data)
{
  unsigned int keymap;
  float ygraf, xgraf;
  FL_Coord x,y;

  /* in order to know the mouse button that created the event */
  mouse_button=xev->xkey.keycode;
  if(canvas_mouse_button_pressed==0){
    if (mouse_button==MOUSELEFT)
      {
	/* getting mouse coordenates. win will be always the canvas window, because this callback has been defined only for that canvas */  
      fl_get_win_mouse(win,&x,&y,&keymap);
      /* from graphical coordinates to spatial ones */
      ygraf=((float) (height/2-y))/escala;
      xgraf=((float) (x-width/2))/escala;

      /* Target for VFF navigation getting mouse coordenates */
      oldvfftarget.x=vfftarget.x;
      oldvfftarget.y=vfftarget.y;
      vfftarget.y=(ygraf-odometrico[1])*odometrico[3]+(-xgraf+odometrico[0])*odometrico[4];
      vfftarget.x=(ygraf-odometrico[1])*odometrico[4]+(xgraf-odometrico[0])*odometrico[3];
      mouse_y=vfftarget.y;
      mouse_x=vfftarget.x;
      mouse_new=1;
      }
    else if ((mouse_button==MOUSERIGHT)||(mouse_button==MOUSEMIDDLE))
      {
	canvas_mouse_button_pressed=1;
	/* For canvas displacement on 2D world (mouseright) or teleoperation (mousemiddle) */
	fl_get_win_mouse(win,&x_canvas,&y_canvas,&keymap);
	old_x_canvas=x_canvas;
	old_y_canvas=y_canvas;
      }
    else if(mouse_button==MOUSEWHEELDOWN){
      /* a button has been pressed */
      canvas_mouse_button_pressed=1;

      /* modifing scale of the visualization window */
      rango-=1000;
      if(rango<=RANGO_MIN) rango=RANGO_MIN;
      fl_set_slider_value(fd_vffgui->escala,rango);
      visual_refresh = TRUE; /* activa el flag que limpia el fondo de la pantalla y repinta todo */ 
      escala = width /rango;

    }else if(mouse_button==MOUSEWHEELUP){
      /* a button has been pressed */
      canvas_mouse_button_pressed=1;

      /* modifing scale of the visualization window */
      rango+=1000;
      if(rango>=RANGO_MAX) rango=RANGO_MAX;
      fl_set_slider_value(fd_vffgui->escala,rango);
      visual_refresh = TRUE; /* activa el flag que limpia el fondo de la pantalla y repinta todo */ 
      escala = width /rango;
    }
  }

  return 0;
}

/* callback function for mouse motion inside the canvas object*/
int mouse_motion_on_micanvas(FL_OBJECT *ob, Window win, int win_width, int win_height, XEvent *xev, void *user_data)
{  
  float diff_x,diff_y;
  unsigned int keymap;

  if(canvas_mouse_button_pressed==1)
	{

    /* getting mouse coordenates. win will be always the canvas window, because this callback has been defined only for that canvas */  
    fl_get_win_mouse(win,&x_canvas,&y_canvas,&keymap); 
	}
	else 
		if(mouse_button==MOUSERIGHT)
		{
      /* getting difference between old and new coordenates */
      diff_x=(old_x_canvas-x_canvas);
      diff_y=(y_canvas-old_y_canvas);
      old_x_canvas=x_canvas;
      old_y_canvas=y_canvas;

      /* changing the visualization window position */
      odometrico[0]+=rango*(diff_x)*(0.005);
      odometrico[1]+=rango*(diff_y)*(0.005);
      visual_refresh=TRUE;
    }
  return 0;
}



/* callback function for button released inside the canvas object*/
int button_released_on_micanvas(FL_OBJECT *ob, Window win, int win_width, int win_height, XEvent *xev, void *user_data)
{
  if(canvas_mouse_button_pressed==1)
	{
    if(mouse_button==MOUSEMIDDLE)
		{
			/* robot is being stopped */
			robot_mouse_motion=1;
    }
    /* a button has been released */
    canvas_mouse_button_pressed=0;
  }
  return 0;
}

void vff_guisuspend_aux(void)
{
  /* to make a safety stop when the robot is being teleoperated from GUI */
  mydelete_buttonscallback(vff_guibuttons);
  mydelete_displaycallback(vff_guidisplay);
  fl_hide_form(fd_vffgui->vffgui);
}

void vff_guisuspend(){
   static callback fn=NULL;
   if (fn==NULL){
      if ((fn=(callback)myimport ("graphics_xforms", "suspend_callback"))!=NULL){
         fn ((gui_function)vff_guisuspend_aux);
      }
   }
   else{
      fn ((gui_function)vff_guisuspend_aux);
   }
}

void vff_guiresume_aux(void)
{
  static int k=0;
  XGCValues gc_values;

  if (k==0) /* not initialized */
    {
      k++;

      /* Coord del sistema odometrico respecto del visual */
      odometrico[0]=0.;
      odometrico[1]=0.;
      odometrico[2]=0.;
      odometrico[3]= cos(0.);
      odometrico[4]= sin(0.);

      display_state = display_state | DISPLAY_LASER;
      display_state = display_state | DISPLAY_SONARS;
      display_state = display_state | DISPLAY_ROBOT;

      fd_vffgui = create_form_vffgui();
      fl_set_form_position(fd_vffgui->vffgui,400,50);
      fl_show_form(fd_vffgui->vffgui,FL_PLACE_POSITION,FL_FULLBORDER,"vff");
      canvas_win= FL_ObjWin(fd_vffgui->micanvas);
      gc_values.graphics_exposures = False;
      vffgui_gc = XCreateGC(mydisplay, canvas_win, GCGraphicsExposures, &gc_values);  

      /* canvas handlers */
      fl_add_canvas_handler(fd_vffgui->micanvas,ButtonPress,button_pressed_on_micanvas,NULL);
      fl_add_canvas_handler(fd_vffgui->micanvas,ButtonRelease,button_released_on_micanvas,NULL);
			fl_add_canvas_handler(fd_vffgui->micanvas,MotionNotify,mouse_motion_on_micanvas,NULL);
    }
  else 
    {
      fl_show_form(fd_vffgui->vffgui,FL_PLACE_POSITION,FL_FULLBORDER,"vff");
      canvas_win= FL_ObjWin(fd_vffgui->micanvas);
    }

  /* Empiezo con el canvas en blanco */
  width = fd_vffgui->micanvas->w;
  height = fd_vffgui->micanvas->h;
  fl_winset(canvas_win); 
  fl_rectbound(0,0,width,height,FL_WHITE);   
  /*  XFlush(display);*/
  
  trackrobot=TRUE;
  fl_set_button(fd_vffgui->track_robot,PUSHED);
  
  fl_set_slider_bounds(fd_vffgui->escala,RANGO_MAX,RANGO_MIN);
  fl_set_slider_filter(fd_vffgui->escala,range); /* Para poner el valor del slider en metros en pantalla */
  fl_set_slider_value(fd_vffgui->escala,RANGO_INICIAL);
  escala = width/rango;

  fl_set_positioner_xvalue(fd_vffgui->center,0.5);
  fl_set_positioner_yvalue(fd_vffgui->center,0.5);
	
  myregister_buttonscallback(vff_guibuttons);
  myregister_displaycallback(vff_guidisplay);
}

void vff_guiresume(){
   static callback fn=NULL;
   if (fn==NULL){
      if ((fn=(callback)myimport ("graphics_xforms", "resume_callback"))!=NULL){
         fn ((gui_function)vff_guiresume_aux);
      }
   }
   else{
      fn ((gui_function)vff_guiresume_aux);
   }
}

