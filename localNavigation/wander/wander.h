/*
 *  Copyright (C) 2008 José María Cañas Plaza 
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Authors : Julio Vega Pérez (jm [dot] vegap [at] alumnos [dot] urjc [dot] es)
 *  Authors : José María Cañas (jmplaza [at] gsyc [dot] es
 *  Authors : Darío Rodríguez de Diego (drd [dot] sqki [at] gmail [dot] com)
 *  Authors : Víctor Hidalgo 
 *
 *  This schema was programed for GuideRobot Project http://jde.gsyc.es/index.php/GuideRobot
 *
 */

extern void wander_init(char *configfile);
extern void wander_termina();
extern void wander_stop();
extern void wander_run(int father, int *brothers, arbitration fn);
extern void wander_show();
extern void wander_hide();

extern int wander_id; /* schema identifier */
extern int wander_cycle; /* ms */
