/*
 *  Copyright (C) 2008 José María Cañas Plaza 
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Authors : Julio Vega Pérez (jm [dot] vegap [at] alumnos [dot] urjc [dot] es)
 *  Authors : José María Cañas (jmplaza [at] gsyc [dot] es
 *  Authors : Darío Rodríguez de Diego (drd [dot] sqki [at] gmail [dot] com)
 *  Authors : Víctor Hidalgo 
 *
 *  This schema was programed for GuideRobot Project http://jde.gsyc.es/index.php/GuideRobot
 *
 */

#include "jde.h"
#include "wander.h"
#include "pioneer.h"
#include "configschema.h"
#include "localNavigation.h"

/*Variables propias del esquema*/
int wander_id=0; 
int wander_brothers[MAX_SCHEMAS];
arbitration wander_callforarbitration;
int wander_cycle=150; /* ms */
enum wander_states {init,t1,r1,t2,r2,t3,r3,t4,end};
static int wander_state;

/*Interaccion con otros esquemas*/
int *golocalNav;
int *alwaysOn;
runFn localNavresume;
stopFn localNavsuspend;

/*Constantes*/
#define MAXPORCENTAJE 100/*definimos el valor máximo del porcentaje para calcular el valor máximo que toma el punto respecto al radio*/

/*Variables globales*/
struct timeval timer,actual;/*Variables para recoger el tiempo del sistema*/
Tvoxel *target;

/*Flags*/
static int rand_calculado=0;/*Incializa la semilla del random, se hace solo la primera vez al cargar el esquema*/
short *target_reached;/*0 si hemos llegado al objetivo*/

int diftiempos;/*Diferencia entre el tiempo actual y el de referencia*/

float radio;/*radio máximo para calcular los puntos*/
int maxtime;/*Maxima diferencia de los tiempos*/

/*Devuelve 1 o -1 segun corresponda para tener coordenadas positivas o negativas*/
int multiplicador(int mul)
{
	if (mul <= MAXPORCENTAJE/2)
	{
		return -1;
	}
	else
	{
		return 1;
	}
}

void wander_iteration()
{
	int porcent;
	int multi;
	
	speedcounter(wander_id);
	/*printf("radio = %f\n",radio);
	printf("maxtime = %d\n",maxtime);*/
	
	if (rand_calculado==0)/*Comprobamos si hemos calculado la semilla para el random*/
	{
		printf("Seed initialized\n");
		gettimeofday(&timer,NULL);
		srand(timer.tv_sec);
		rand_calculado=1;
		/*Calculamos el primer punto*/
		porcent=rand()%MAXPORCENTAJE;
		multi=multiplicador(rand()%MAXPORCENTAJE);
		target->x=radio*porcent/MAXPORCENTAJE*multi;
		porcent=rand()%MAXPORCENTAJE;
		multi=multiplicador(rand()%MAXPORCENTAJE);
		target->y=radio*porcent/MAXPORCENTAJE*multi;
	}
	gettimeofday(&actual,NULL);
	diftiempos = actual.tv_sec-timer.tv_sec;/*Hacemos la diferencia entre la hora actual del sistema y cuando creamos el punto*/
	 /*printf("Time= %d \n",diftiempos); 
	 printf("Max time= %d \n",maxtime);
	 printf("tager_reached = %d\n",*target_reached);*/
	if ((diftiempos >= maxtime ) || (*target_reached == 0))/*Comprobamos las condiciones para recalcular un punto nuevo*/
	{
		porcent=rand()%MAXPORCENTAJE;
		multi=multiplicador(rand()%MAXPORCENTAJE);
		target->x=radio*porcent/MAXPORCENTAJE*multi;
		porcent=rand()%MAXPORCENTAJE;
		multi=multiplicador(rand()%MAXPORCENTAJE);
		target->y=radio*porcent/MAXPORCENTAJE*multi;

		gettimeofday(&timer,NULL);
	}
	/*printf("Target = %f, %f\n",target->x,target->y);*/
}

/*Importar símbolos*/
void wander_imports()
{
	/*Pasamos cuando estan hechos los imports en localNavigation*/
	localNavresume=(runFn)myimport("localNavigation","run");
	localNavsuspend=(stopFn)myimport("localNavigation","stop");
	golocalNav=myimport("localNavigation","golocalNavigation");
	alwaysOn = myimport("localNavigation","alwaysOn");
	target=(Tvoxel *)myimport("localNavigation","target");
	target_reached=(short *)myimport("localNavigation","distanciaObjetivo");
}

/*Exportar símbolos*/
void wander_exports()
{
   myexport("wander","id",&wander_id);
   myexport("wander","run",(void *)wander_run);
   myexport("wander","stop",(void *)wander_stop);
}

void wander_terminate()
{
  printf ("wander close\n");
}


void wander_stop()
{
  /* printf("wander: cojo-suspend\n");*/
  pthread_mutex_lock(&(all[wander_id].mymutex));
  put_state(wander_id,slept);
	all[wander_id].children[(*(int *)myimport("localNavigation","id"))]=FALSE;
	localNavsuspend();
  printf("wander: off\n");
  pthread_mutex_unlock(&(all[wander_id].mymutex));
  /*  printf("wander: suelto-suspend\n");*/
}


void wander_run(int father, int *brothers, arbitration fn)
{
  int i;

  /* update the father incorporating this schema as one of its children */
  if (father!=GUIHUMAN && father!=SHELLHUMAN)
    {
      pthread_mutex_lock(&(all[father].mymutex));
      all[father].children[wander_id]=TRUE;
      pthread_mutex_unlock(&(all[father].mymutex));
    }

  pthread_mutex_lock(&(all[wander_id].mymutex));
  /* this schema resumes its execution with no children at all */
  for(i=0;i<MAX_SCHEMAS;i++) all[wander_id].children[i]=FALSE;
  all[wander_id].father=father;
  if (brothers!=NULL)
    {
      for(i=0;i<MAX_SCHEMAS;i++) wander_brothers[i]=-1;
      i=0;
      while(brothers[i]!=-1) {wander_brothers[i]=brothers[i];i++;}
    }
  wander_callforarbitration=fn;
  put_state(wander_id,notready);
  printf("wander: on\n");
  pthread_cond_signal(&(all[wander_id].condition));
  pthread_mutex_unlock(&(all[wander_id].mymutex));
  wander_imports();
	*golocalNav = 1;
	*alwaysOn = 1;
}

void *wander_thread(void *not_used)
{
   struct timeval a,b;
   long n=0; /* iteration */
   long next,bb,aa;

   for(;;)
   {
      pthread_mutex_lock(&(all[wander_id].mymutex));

      if (all[wander_id].state==slept)
      {
	 wander_state=init;
	 pthread_cond_wait(&(all[wander_id].condition),&(all[wander_id].mymutex));
	 pthread_mutex_unlock(&(all[wander_id].mymutex));
      }
      else
      {
	 /* check preconditions. For now, preconditions are always satisfied*/
	 if (all[wander_id].state==notready)
	    put_state(wander_id,ready);
	 /* check brothers and arbitrate. For now this is the only winner */
	 if (all[wander_id].state==ready)
	 {
		put_state(wander_id,winner);
		all[wander_id].children[(*(int *)myimport("localNavigation","id"))]=TRUE;
		localNavresume(wander_id,NULL,NULL);
	 	gettimeofday(&a,NULL);
	 	aa=a.tv_sec*1000000+a.tv_usec;
	 	n=0;
	 }

	 if (all[wander_id].state==winner)
	    /* I'm the winner and must execute my iteration */
	 {
	    pthread_mutex_unlock(&(all[wander_id].mymutex));
	    /*      gettimeofday(&a,NULL);*/
	    n++;
	    wander_iteration();
	    gettimeofday(&b,NULL);
	    bb=b.tv_sec*1000000+b.tv_usec;
	    next=aa+(n+1)*(long)wander_cycle*1000-bb;

	    if (next>5000)
	    {
	       usleep(next-5000);
	       /* discounts 5ms taken by calling usleep itself, on average */
	    }
	    else  ;
	 }
	 else
	    /* just let this iteration go away. overhead time negligible */
	 {
	    pthread_mutex_unlock(&(all[wander_id].mymutex));
	    usleep(wander_cycle*1000);
	 }
      }
   }
}

void wander_init(char *configfile)
{
  pthread_mutex_lock(&(all[wander_id].mymutex));
  printf("wander schema started up\n");
	loadconfig(configfile);
	setvar("radio","float",&radio);
	setvar("maxtime","int",&maxtime);
  wander_exports();
  put_state(wander_id,slept);
  pthread_create(&(all[wander_id].mythread),NULL,wander_thread,NULL);
  pthread_mutex_unlock(&(all[wander_id].mymutex));
}
void wander_show();
void wander_hide();
