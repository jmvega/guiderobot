#include <stdio.h>
#include "configschema.h"

/*Variables que cargaremos de example.conf*/

int entero;
long largo;
float comaflotante;
double doble;


int main()
{
	printf("Vamos a cargar un archivo de configuracion y a coger los valores de varias variables\n");
	
	/*Cargamos la configuracion*/
	loadconfig("/home/dario/configschema/example/example.conf");
	
	/*Recogemos los valores de las variables*/
	setvar("entero","int",&entero);
	setvar("largo","long",&largo);
	setvar("comaflotante","float",&comaflotante);
	setvar("doble","double",&doble);

	/*Las escribimos para comprobarlo*/
	printf("El valor de entero es = %d\n",entero);
	printf("El valor de largo es = %ld\n",largo);
	printf("El valor de comaflotante es = %f\n",comaflotante);
	printf("El valor de doble es = %f\n",doble);	

	return 0;
}
