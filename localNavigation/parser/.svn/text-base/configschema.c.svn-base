/*
 *  Copyright (C) 2008 Darío Rodríguez de Diego 
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  Authors : Darío Rodríguez de Diego <drd [dot] sqki [at] gmail [dot] com>
 */

#include "configschema.h"



/*Nodo de la lista de variables*/
typedef struct TVar
{
	char varname[MAXWORD];/*Nombre de la variable*/
	char value[MAXWORD];/*Valor de la variable*/	
	struct TVar *sig;/*Enlazador de la lista*/
}TVarList; 

FILE *f;/*Variable para cargar el fichero*/
TVarList *vars;/*Cabecera de la lista de variables*/

char *auxchar;

/*Comprueba si la linea es un comentario*/
int comment(char *s)
{
	if (s[0] == '#')
	{
		return 1;
	}
	else
	{
		return 0;
	}	
}

/*Lee un string desde el archivo de texto*/
void readstr(char *s)
{
	int i;
	char aux;
	i = 0;
	while ((i<MAXSTR) && (aux != '\n'))
	{
		fscanf(f,"%c",&aux);
		s[i] = aux;
		i++;
	}	
}
/*Rellena el strin de espacios en blanco*/
void clearstr(char *s,int lenth)
{
	int i;
	for (i=0;i<lenth;i++)
	{
		s[i]='\0';	
	}	
}

/*Lee la linea del texto y recoge el nombre de la variable y su valor*/
void pickvar(char *s,char *name,char *val)
{
	char c;
	int i;
	int space;/*Vale  != 0 si es un espacio*/
	int jump;/*Desfasa entre la linea del texto y la cadena del valor*/
	/*Inicializamos las variables del bucle*/
	i = 0;
	c = s[i];
	space = 0;
	/*Recogemos el nombre de la variable*/
	while ((i < MAXWORD) && (space == 0 ))
	{
		name[i] = c;
		i++;
		c = s[i];
		space = isspace(c);
	}
	/*Avanzamos tres posiciones para salvar el igual y el espacio*/
	i = i + JUMP;
	jump = i;
	c = s[i];/*Cogemos el primer caracter de la siguiente palabra que es el valor*/
	/*Recogemos el valor de la variable*/
	while ((i<MAXWORD) && (c !='\n'))
	{
		val[i-jump] = c;/*Guardamos el caracter en la variable valor con la correccion de cuando avanzamos las posiciones*/
		i++;
		c = s[i];	
	} 
}

/*Añadimos los valores leidos a la lista de variables*/
void addVar(TVarList **l,char *s)
{
	TVarList *aux;
	char name[MAXWORD];/*Nombre de la variable*/
	char val[MAXWORD];/*Valor de la variable*/
	/*Reservamos memoria*/
	aux = (TVarList *)malloc(sizeof(TVarList));
	/*Sacamos la variable y el valor de la linea*/
	clearstr(name,MAXWORD);
	clearstr(val,MAXWORD);
	pickvar(s,name,val);
	/*Copiamos los valores a la lista*/
	strcpy(aux->varname,name);
	strcpy(aux->value,val);
	/*Insertamos por cabecera*/
	aux->sig = *l;
	*l = aux;	
}

/*Leemos le fichero y cogemos las variables que nos interesan*/
void loadconfig(char *config)
{
	int endfile;/*1 if is end of file*/
	char line[MAXSTR];/*string*/
	char aux;
	int notchar;/*Vale 1 si es el caracter retornode carro*/
	int ignore;/*Vale 1 si la linea es un comentario*/

	f=fopen(config,"r");
	if (f == NULL)
	{
		printf("ERROR: can't open configuration file\n");
	}
	else
	{
		endfile = feof(f);/*Comprobamos si es el final del fichero*/
		while (endfile == 0)
		{
			clearstr(line,MAXSTR);/*Limpiamos la variable line*/
			readstr(line);/*Leemos la linea del fichero*/
			ignore = comment(line);/*Comprobamos si es un comentario*/
			notchar = (line[0] == '\n') && (aux == '\n');/*Comprobamos si es un salto de linea*/
			if ((ignore == 1) || (notchar == 1))
			{
				fscanf(f,"%c",&aux);/*Saltamos la linea*/
			}
			else
			{
				/*BUSCAMOS LA NUEVA VARIABLE Y LA AÑADIMOS A LA LISTA*/
				addVar(&vars,line);/*Recogemos el nombre y el valor de la variable*/
				fscanf(f,"%c",&aux);/*Saltamos la linea*/
			}
			endfile = feof(f);
		}
	}
	fclose(f);
}

/*Damos el valor a la variable*/
void setvar(char *name,char *vartype,void *var)
{	
	TVarList *aux;
	int found;/*Vale 0 si hay coincidencia*/
	int type;/*Vale 0 si los tipos coinciden*/

	aux = vars;	
	found = strcmp(name,aux->varname);
	while ((aux != NULL) && (found != 0))/*Buscamos la varible "name" en la lista*/
	{
		aux = aux->sig;
		found = strcmp(name,aux->varname);
	}
	if (found == 0)/*Si esta la variable comprobamos su tipo*/
	{
		type = strcmp(vartype,"int");/*Comprobamos que si es entero*/
		if (type == 0)
		{ 
			*(int *) var = atoi(aux->value);/*Hacemos un cast para int*/
		}
		else
		{	
			type = strcmp(vartype,"long");/*Comprobamos si es long*/
			if (type == 0)
			{
				*(long *) var = atol(aux->value);/*Hacemos un cast para long*/
			}	
			else
			{
				type = strcmp(vartype,"float");/*Comparamos si es float*/
				if (type == 0)
				{
				 *(float *) var = atof(aux->value);	/*Hacemos un cast para float*/
				}
				else
				{
					type = strcmp(vartype,"double");/*Comparamos si es double*/
					if (type == 0)
					{
						*(double *) var = atof(aux->value);/*Hacemos un cast para double*/
					}
				}
			}
		}
	}
}
