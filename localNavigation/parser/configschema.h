#ifndef CONFIGSCHEMA_H_
#define CONFIGSCHEMA_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

/*Tamaño maximo del string*/
#define MAXSTR 255
/*Tamaño maximo de la variable y de su valor*/
#define MAXWORD 50
/*Definimos una constante para saltarnos la combinacionde los caracteres igual y espacio*/
#define JUMP 3

void loadconfig(char *config);
void setvar(char *name,char *vartype,void *var);

#endif /*CONFIGSCHEMA_H_*/
