#include "jde.h"
#include <forms.h>
#include "graphics_xforms.h"
#include "localNavigationgui.h"
#include "pioneer.h"
#include "configschema.h"
#include <math.h>

#define VFF_SIGMA_THETA 30
#define VFF_SIGMA_RADIO_MAX 5000
#define VFF_SIGMA_RADIO_MIN 1000
#define MOUSELEFT 1
#define MOUSEMIDDLE 2
#define MOUSERIGHT 3
#define MOUSEWHEELUP 4
#define MOUSEWHEELDOWN 5
#define LIMITE_LLEGADA_OBJETIVO 40

extern void localNavigation_init(char *configfile);
extern void localNavigation_terminate();
extern void localNavigation_stop();
extern void localNavigation_run(int father, int *brothers, arbitration fn);
extern void localNavigation_show();
extern void localNavigation_hide();
