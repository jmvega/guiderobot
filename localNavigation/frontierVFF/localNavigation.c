#include "localNavigation.h"

/* Gui callbacks */
registerbuttons myregister_buttonscallback;
registerdisplay myregister_displaycallback;
deletebuttons mydelete_buttonscallback;
deletedisplay mydelete_displaycallback;

Display *display;
int *myscreen;

int localNavigation_id=0; 
int localNavigation_brothers[MAX_SCHEMAS];
arbitration localNavigation_callforarbitration;
int localNavigation_cycle=200; /* ms */
enum localNavigation_states {init,t1,r1,t2,r2,t3,r3,t4,end};
static int localNavigation_state;

int *mylaser = NULL;
runFn laserresume;
stopFn lasersuspend;

float *myencoders = NULL;
runFn encodersresume;
stopFn encoderssuspend;

float *myv = NULL;
float *myw = NULL;
runFn motorsresume;
stopFn motorssuspend;

FD_localNavigationgui *fd_localNavigationgui;
GC localNavigationgui_gc;
Window  localNavigation_canvas_win;
unsigned long localNavigation_display_state;
int localNavigation_visual_refresh=FALSE;
int localNavigation_iteracion_display=0;
int localNavigation_canvas_mouse_button_pressed=0;
int localNavigation_mouse_button=0;
int localNavigation_robot_mouse_motion=0;
FL_Coord localNavigation_x_canvas,localNavigation_y_canvas,old_localNavigation_x_canvas,old_localNavigation_y_canvas;
float localNavigation_mouse_x, localNavigation_mouse_y;
int localNavigation_mouse_new=0;

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* VARIABLES PARA CONTROL */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

Tvoxel target;/*Target imported*/
int golocalNavigation=1;/*Vale 1 cuando el esquema de navegacion esta funcionando*/ /*MEJOR CON SEMAFOROS??????*/
int useInput;/*Si es 1 carga miObjetivo con x e y importadas desde otros esquemas*/

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* PARÁMETROS GRAFICOS */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
#define VFF_COLOR 1
#define SECWINDOW_COLOR 0
#define PUSHED 1
#define RELEASED 0 
#define FORCED_REFRESH 5000 /* ms */
static int forcedGradientRefresh = FALSE;
static int forcedOccupancyRefresh = FALSE; /* podremos forzar el refresco según nuestras necesidades*/
/*Every forced_refresh the display is drawn from scratch. If it is too small it will cause flickering with grid display. No merece la pena una hebra de "display_lento" solo para repintar completamente la pantalla. */

float localNavigation_escala, localNavigation_width, localNavigation_height;
int localNavigation_trackrobot=FALSE;
float localNavigation_odometrico[5];
#define RANGO_MAX 40000. /* en mm */
#define RANGO_MIN 500. /* en mm */ 
#define RANGO_INICIAL 30000. /* en mm */
float localNavigation_rango=(float)RANGO_INICIAL; /* Rango de visualizacion en milimetros */

#define EGOMAX NUM_SONARS+5
XPoint localNavigation_ego[EGOMAX];
int numlocalNavigation_ego=0;
int visual_delete_localNavigation_ego=FALSE;

XPoint localNavigation_laser_dpy[NUM_LASER];
int visual_delete_localNavigation_laser=FALSE;

#define joystick_maxRotVel 30 /* deg/sec */
#define joystick_maxTranVel 500 /* mm/sec */

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* CONSTANTES DE VISUALIZACION DE DISTINTOS RECURSOS GRAFICOS */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
unsigned long DISPLAY_ROBOT = 0x01UL;
/*unsigned long DISPLAY_SONARS = 0x04UL;  no se muestran, ya que no hacemos uso de ellos*/
unsigned long DISPLAY_LASER = 0x08UL;
unsigned long DISPLAY_MEMORY = 0x40UL;
unsigned long DISPLAY_ESTELA = 0x80UL;
unsigned long DISPLAY_VENTANA = 0x60UL;
unsigned long DISPLAY_FUERZAS = 0x24UL;
static int DEBUG = FALSE;


/*SON NECESARIOS LOS 9 OBJETIVOS*/
XPoint GraphicGlobalDestination[9];/*Para pintar los destinos que pinchamos, almacena 9 destinos*/
Tvoxel myGlobalDestination;/*LA USAMOS PARA PINTAR LOS 9 OBJETIVOS Y NADA MAS*/

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* CONSTANTES UTILIZADAS PARA ALGORITMO VFF */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* Nota: todas las costantes relacionadas con las distancias o con el cálculo de las mismas, se establecen al cuadrado, porque en código siempre jugamos con distancias al cuadrado, para evitar así tener que sacar raices cuadradas (recordemos todos el fabuloso Teorema de Pitágoras: h*h = a*a + b*b; pues nosotros dejamos la h*h tal cual...) */
#define K 2000*2000                   /* Constantes de repulsión*/
#define K2 3000*3000					/*ANTES 3000*3000 */
#define K3 4000*4000

#define moduloAtractiva 1000          /* Modulo de la fuerza atractiva */

#define alcanceMuyPeligroso 1000*1000
#define alcancePeligroso 2000*2000
#define alcance 5000*5000             /* Distancia a la que las fuerzas repulsivas empiezan a actuar sobre nosotros */
#define alcanceLateral 2000*2000 

#define seguridad 500*500             /* a partir de esta distancia giramos sin avanzar*/
#define seguridadLateral 200*200      /* a partir de esta distancia lateral giramos sin avanzar*/

#define alpha 0.7f                    /* Proporcion de la fuerza atractiva en la fuerza resultante */
#define beta  0.4f                    /* Proporcion de la fuerza repulsiva en la fuerza resultante */
#define vMax 400
#define wPosMax 30
#define wNegMax -30
#define limiteTrayectoriaSimilar 10
#define limiteTrayectoriaDesigual 80

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* PARÁMETROS DE VENTANA DE SEGURIDAD */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
#define ventanaY 850.00 /* 85 cm de longitud lado Y */
#define frenteY 950.00 /* 95 cm de frente */
#define ventanaX 220.00 /* en total tendrá una longitud X de 44 cm */
#define huecoX 370.00 /* en total tendrá una longitud X de 74 cm nuestra macro ventana */
float distanciasVentanaSeguridad [90]; /* tendremos guardados los 180 puntos de delimitación */
float distanciasHuecoSeguridad [90];
float distanciasFrenteSeguridad [90];
Tvoxel ventanaSeguridad [180];
Tvoxel huecoSeguridad [180];
Tvoxel frenteSeguridad [180];
int visual_delete_localNavigation_ventana = FALSE;
XPoint coord_graf_ventana[180];
XPoint coord_graf_hueco[180];
XPoint coord_graf_frente[180];
XPoint coord_graf_puntos_ventana[10];

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* PARÁMETROS DE MEMORIA DE PUNTOS */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
#define existenciaMemoria 0  /* Duracion de un punto en la memoria ANTES: 3*1000000 */
#define tamMemoria 650 /*ANTES 400*/

int visual_delete_localNavigation_memory = FALSE;
XPoint coord_graf_memoria[tamMemoria], coord_graf_objetivo,graf_robot,graf_atr,graf_res,graf_rep;
int num_puntos;

typedef struct datosMemoria {
	int horaInicio[tamMemoria];
	int masViejo;
	int ultimo;
} TdatosMemoria;

typedef struct {
	int   moduloFuerzaAtractiva;
	float direccionFuerzaAtractiva[2];
	float anguloFuerzaAtractiva;
	int   moduloFuerzaRepulsiva;
	int   direccionFuerzaRepulsiva[2];
	float anguloFuerzaRepulsiva;
	int   moduloFuerzaResultante;
	float direccionFuerzaResultante[2];
	float anguloFuerzaResultante;
	float anguloQueLlevo; 
	int   avanzaEnGiro; /* Sera 1 siempre k la distancia sea > seguridad; si la distancia leida es < seguridad, giraremos sin avanzar */
} TdatosResultante;

Tvoxel memoria[tamMemoria];
int validos[tamMemoria];
TdatosMemoria datos;
TdatosMemoria *pdatos;
TdatosResultante resultante;
TdatosResultante *result;
short distanciaObjetivo;
float xRep,yRep,xAtr,yAtr,xRes,yRes;
float difAngle;
float peligro;
Tvoxel vfftarget, oldvfftarget, myAttachForce, myRepForce, myResForce;
Tvoxel miObjetivo,fuerzaResultante, repulsiva, atractiva, robott;

int contadorMemoria;

typedef struct {
	Tvoxel pos;
	int color;
} TEstelaRecorrido;

TEstelaRecorrido estelaRecorrido[tamMemoria/2];

XPoint coordGrafEstela[tamMemoria/2];
int posEstela = 0;
int numEstelas;

/**********************************************************************************/
/**********************************************************************************/
/****************************COMPORTAMIENTO VFF************************************/
/**********************************************************************************/
unsigned long int dameTiempo() {
	struct timeval t; 

	gettimeofday(&t,NULL);
	return t.tv_sec*1000000 + t.tv_usec;	
}

float transformoAngulo(float angulo) {
	/* Transformamos un angulo dado en radianes en grados de tal manera k si el
	angulo introducido es 180>thetha>0 se kda tal cual y si es 180<theta<0 kda
	como -thetha*/ 
	float salida, auxi, auxi1;

	angulo=angulo*RADTODEG; /* PASAMOS la entrada A GRADOS */
	auxi=angulo*100; /* Nos quedamos con 2 decimales */
	auxi1=ceil(auxi);
	angulo=(auxi1/100);

	if ((angulo>180)&&(angulo<=360)) {
		salida=angulo-(2*M_PI*RADTODEG);
		if (salida>=-0.1) salida=0.00;
	}	else salida=angulo;

	if (salida>=360) salida=0.00;

	auxi=salida*100;
	auxi1=ceil(auxi);
	salida=(auxi1/100);  /* Esta conversion la hacemos para quedarnos con 2 decimales */

	return salida;
}

int calculoFuerza (int distancia, int ang) {
  int fuerza;

  if (distancia!=0)
		if (distancia < alcancePeligroso)
			fuerza = K2/distancia; /* generamos MAS fuerza repulsiva; hay cierto peligro */
		else
			fuerza = K/distancia; /* generamos MENOS fuerza repulsiva; no se considera peligro */

  else fuerza=0;

  return fuerza;
}

int mismoAnguloyDistancia(Tvoxel a,int distanciaPuntoMem,int anguloPuntoNuevo,int distanciaNueva) {
	int angulo_aux;
	int salida=FALSE;
	float margen = 500;
	int dist = FALSE;

	dist = distanciaPuntoMem < (distanciaNueva-margen);
	angulo_aux = (int)(atan2(a.y-myencoders[1],a.x-myencoders[0])*RADTODEG);
	salida = ((anguloPuntoNuevo <= (angulo_aux + 0.7)) && (anguloPuntoNuevo >= (angulo_aux - 0.7)));

	return salida && dist;
}

int reemplazoSimilares(Tvoxel a,Tvoxel b,int distanciaGuardado,int distanciaNuevo) {
	int radio;	
	radio = 10;

	return (((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y))<(radio*radio));
}

void rellenoEstela (int color) {
	Tvoxel posActualRobot;
	posActualRobot.x = myencoders[0];
	posActualRobot.y = myencoders[1];

	if (posEstela >= (tamMemoria/2)) posEstela = 0; /* reinicio */

	estelaRecorrido [posEstela].pos = posActualRobot;
	estelaRecorrido [posEstela].color = color;

	posEstela ++;
}

void rellenoMemoria (TdatosMemoria *d,int v[tamMemoria],Tvoxel m[tamMemoria]) {
	int i,j,colocado,masViejo;
	int hora;
	int angulo_nuevo;	
	Tvoxel punto;
	float distancia,distanciaPuntMem; 

	for(i=0;i<NUM_LASER;i++) {
		colocado=FALSE; /* no tenemos ubicado el punto del laser actual */
		masViejo=0;
		j=(*d).ultimo; /* cogemos el indice del ultimo punto de memoria que se ha actualizado, para asi ir comparando (y sólo) con todos los que hay */
		laser2xy(i,mylaser[i],&punto,&myencoders[0]); /* calculamos posicion de punto detectado por laser i */
		angulo_nuevo= (int)(atan2(punto.y-myencoders[1],punto.x-myencoders[0])*RADTODEG); /* angulo del punto vs. robot */
		distancia=((punto.x-myencoders[0])*(punto.x-myencoders[0])+(punto.y-myencoders[1])*(punto.y-myencoders[1]));
		hora=dameTiempo();

		while((!colocado)&&(j>=0)) { /* bucle para ubicar el punto detectado por laser */
			if(v[j]) { /* si hay punto valido */
				/* Si hay punto valido miro si en la memoria hay alguno en la misma proyeccion del laser o algun punto similar. Si se cumple alguna de las dos reemplazo en ese lugar,sino busco un hueco vacio y si no lo hay lo pongo donde el mas viejo */
				distanciaPuntMem=((m[j].x-myencoders[0])*(m[j].x-myencoders[0])+(m[j].y-myencoders[1])*(m[j].y-myencoders[1]));

				if(reemplazoSimilares(m[j],punto,distanciaPuntMem,distancia)) { /* si son puntos muy cercanos (similares) */
					m[j]=punto;
					(*d).horaInicio[j]=hora;
					(*d).ultimo=j;
					v[j]=TRUE;
					colocado=TRUE; /* salimos del bucle, ya esta ubicado */
			}else
			if (mismoAnguloyDistancia(m[j],distanciaPuntMem,angulo_nuevo,distancia)) { 
					/*Si estan en la misma proyeccion y el punto nuevo esta mas alejado que el punto viejo*/
					m[j]=punto;
					(*d).horaInicio[j]=hora;
					(*d).ultimo=j;
					v[j]=TRUE;
					colocado=TRUE; /* salimos del bucle, ya esta ubicado */
				}
			} /* fin if (v[j]) */
			
			if (v[masViejo]) { /* si la posicion del viejo es valida... */
				if (!v[j]) masViejo=j; /* ...y la posicion actual de j es NO VALIDA, entonces hemos encontrado un hueco */
				else if ((*d).horaInicio[j]<(*d).horaInicio[masViejo]) masViejo=j;
			} /* asi, cuando salgamos del bucle "por esta via" tendremos el hueco, siendo la casilla mas vieja */

			j--; /* vamos a comprobar con otro punto, hasta acabar con los existentes en memoria */
		} /* fin while((!colocado)&&(j>=0)) */

		if(!colocado) { /* si no hemos logrado ubicarlo */
			j=(*d).ultimo+1; /* ahora miramos en la otra parte (del ultimo "hacia la derecha") */
			while((!colocado)&&(j<tamMemoria)) {
				if(v[j]) {
				/* Si hay punto valido miro si en la memoria hay alguno en la misma proyeccion del laser o algun punto similar. Si se cumple alguna de las dos reemplazo en ese lugar,sino busco un hueco vacio y si no lo hay lo pongo donde el mas viejo */
					distanciaPuntMem=((m[j].x-myencoders[0])*(m[j].x-myencoders[0])+(m[j].y-myencoders[1])*(m[j].y-myencoders[1]));

					if(reemplazoSimilares(m[j],punto,distanciaPuntMem,distancia)) {
						m[j]=punto;
						(*d).horaInicio[j]=hora;
						(*d).ultimo=j;
						v[j]=TRUE;
						colocado=TRUE;
					}else
				 if(mismoAnguloyDistancia(m[j],distanciaPuntMem,angulo_nuevo,distancia)) {	  
						m[j]=punto;
						(*d).horaInicio[j]=hora;
						(*d).ultimo=j;
						v[j]=TRUE;
						colocado=TRUE;
					}
				} /* fin if (v[j]) */

				if (v[masViejo]) {
					if (!v[j]) masViejo=j;
					else if ((*d).horaInicio[j]<(*d).horaInicio[masViejo]) masViejo=j;
				}

				j++;
			} /* fin while((!colocado)&&(j<tamMemoria)) */
		} /* fin if(!colocado) */

		if (!colocado) { /* finalmente si no lo hemos ubicado, lo insertamos en la casilla mas vieja */
			m[masViejo]=punto;
			(*d).horaInicio[masViejo]=hora;
			(*d).ultimo=masViejo; /* ahora el ultimo sera lo que antes era el mas viejo... */
			v[masViejo]=TRUE;
		}
	} /* fin for(i=0;i<NUM_LASER;i++) */
}

void guardarCoordObjetivo(TdatosResultante *res) {
	float auxi1;
	double radianes;

	(*res).direccionFuerzaAtractiva[0]=miObjetivo.x-myencoders[0];
	(*res).direccionFuerzaAtractiva[1]=miObjetivo.y-myencoders[1];

	(*res).anguloFuerzaAtractiva=atan2((*res).direccionFuerzaAtractiva[1],(*res).direccionFuerzaAtractiva[0]);
		
	(*res).moduloFuerzaAtractiva=moduloAtractiva;

	radianes=(*res).anguloFuerzaAtractiva;

	(*res).direccionFuerzaAtractiva[0]=cos(radianes)*(*res).moduloFuerzaAtractiva;
	(*res).direccionFuerzaAtractiva[1]=sin(radianes)*(*res).moduloFuerzaAtractiva;

	auxi1=transformoAngulo((*res).anguloFuerzaAtractiva);
	(*res).anguloFuerzaAtractiva=auxi1;
}

void calculaResultante(Tvoxel m[tamMemoria],int v[tamMemoria],TdatosResultante *res) {
	int angulo,ang_robot,diff_ang;
	int sumaX, sumaY, i;
	int distancia,modulo;
	float auxi, auxi1;
	double radianes;

	sumaX=0;
	sumaY=0;

	for (i=0; i<tamMemoria; i++) {
		if (v[i]) {
			if (distancia!=0) {
				/* calculo la distancia al robot y el angulo respecto a este */
				distancia=(((m[i].x-myencoders[0])*(m[i].x-myencoders[0]))+((m[i].y-myencoders[1])*(m[i].y-myencoders[1])));
				radianes=atan2((m[i].y-myencoders[1]),(m[i].x-myencoders[0]));
				angulo=(int) (radianes*RADTODEG);
				ang_robot=(int)(myencoders[2]*RADTODEG);

				/* SUTILEZA PARA QUE EL ROBOT TENGA QUE GIRAR LO MENOS POSIBLE */
				if(ang_robot>=180) ang_robot=-360+ang_robot; /* =-(360-ang_robot)-->pasa a negativo */

				diff_ang=abs(angulo-ang_robot); /* calculamos el total que debería girar el robot */

				if (diff_ang>=180) diff_ang=360-diff_ang; /* mejor si gira por el camino más corto */
				/* FIN DE LA SUTILEZA */

				/* Miramos si el punto esta dentro del alcance en el que los puntos generan fuerza de repulsión. La distancia es distinta segun el angulo en el que se encuentre el punto respecto del robot */
				if (((diff_ang>45)&&(diff_ang<135)&&(distancia<alcanceLateral))||
					(((diff_ang<=45)||(diff_ang>=135))&&(distancia<alcance))) { 
					/* Primero miramos si la distancia es menor que la seguridad asignada. Si es menor giramos sin avanzar */
					if (((diff_ang>45)&&(diff_ang<135)&&(distancia<seguridadLateral))||
						((diff_ang<=45)&&(distancia<seguridad)))
          
						(*res).avanzaEnGiro=FALSE; /* inicialmente está a TRUE */

					/* sumo al angulo 180 ya que lo que buscamos es fuerza repulsiva y queremos el angulo opuesto */
					radianes=radianes+(180.0*DEGTORAD);

					/* calculamos el modulo */
					modulo=calculoFuerza(distancia,diff_ang); /* FUERZA REPULSIVA */

					/* Añadimos las componentes a la suma de x y de y.*/
					sumaX+=cos(radianes)*modulo;
					sumaY+=sin(radianes)*modulo;	
				}
			} /* fin if(distancia!=0) */
		} /* fin if(v[i]) */
	} /* fin FOR */

	(*res).direccionFuerzaRepulsiva[0]=sumaX;
	(*res).direccionFuerzaRepulsiva[1]=sumaY;

	/*printf("xRep:%d f_0.yRep:%d\n",(*res).direccionFuerzaRepulsiva[0],(*res).direccionFuerzaRepulsiva[1]);*/

	/* Calculo la fuerza resultante atribuyendo factores de proporción a la fuerza atractiva y repulsiva */
	(*res).direccionFuerzaResultante[0]=(alpha *(*res).direccionFuerzaAtractiva[0]) +(beta*(*res).direccionFuerzaRepulsiva[0]);
	(*res).direccionFuerzaResultante[1]=(alpha *(*res).direccionFuerzaAtractiva[1]) +(beta*(*res).direccionFuerzaRepulsiva[1]);

	sumaX=(*res).direccionFuerzaResultante[0];
	sumaY=(*res).direccionFuerzaResultante[1];

	(*res).moduloFuerzaResultante=sqrt((sumaX*sumaX)+(sumaY*sumaY));
	(*res).anguloFuerzaResultante=atan2(sumaY,sumaX);	  

	auxi1=transformoAngulo((*res).anguloFuerzaResultante);
	(*res).anguloFuerzaResultante=auxi1;
	/*printf("Angulo resultante:%f\n",(*res).anguloFuerzaResultante);*/

	if ((sumaX==(*res).direccionFuerzaAtractiva[0])&&(sumaY==(*res).direccionFuerzaAtractiva[1])) { /* ¿? sumaX y sumaY han variado, por los factores aplicados...esto NO FUNCIONARÁ */
		/* en caso k no actuen fuerzas, solo tenemos la atractiva*/
		(*res).anguloFuerzaResultante=(*res).anguloFuerzaAtractiva;;
		auxi=((*res).anguloFuerzaResultante)*100;
		auxi1=ceil(auxi);
		(*res).anguloFuerzaResultante=auxi1/100;
	}

	if((*res).anguloFuerzaResultante==0.) {
		/* es mas facil detectar 6.28 k 2*Pi, por eso aproximamos*/
		(*res).anguloFuerzaResultante=2*M_PI;
		auxi=((*res).anguloFuerzaResultante)*100;
		auxi1=ceil(auxi);
		(*res).anguloFuerzaResultante=transformoAngulo(auxi1/100);
	}

	auxi=((*res).anguloFuerzaResultante)*100;
	auxi1=ceil(auxi);
	(*res).anguloFuerzaResultante=(auxi1/100);

}

void generarObjetivo(Tvoxel * objetivo, Tvoxel * oldlocalNavigationtarget, Tvoxel * localNavigationtarget) {
 float radio,angulo/*,min,max*/;

	if((contadorMemoria>LIMITE_LLEGADA_OBJETIVO)||((abs((int)((*objetivo).x-myencoders[0]))<200)&&(abs((int)((*objetivo).y-myencoders[1]))<200))) {
		/*radio=applyGaussNoise(VFF_SIGMA_RADIO,0);*/
		radio=getLimitFloatRandom(VFF_SIGMA_RADIO_MIN,VFF_SIGMA_RADIO_MAX);
		angulo=applyGaussNoise(VFF_SIGMA_THETA,0);

		angulo=myencoders[2]*RADTODEG+angulo;
		if(angulo<0) angulo+=360;
		if(angulo>=360) angulo-=360;
		/*printf("  %f  %f\n",myencoders[2]*RADTODEG, angulo);	*/
		(*oldlocalNavigationtarget).x=(*objetivo).x;
		(*oldlocalNavigationtarget).y=(*objetivo).y;

		(*objetivo).x=myencoders[0]+radio*cos(angulo*DEGTORAD);
		(*objetivo).y=myencoders[1]+radio*sin(angulo*DEGTORAD); 		

		(*localNavigationtarget).y=(*objetivo).x;
		(*localNavigationtarget).x=(*objetivo).y;

		contadorMemoria=0;	    	
	} else 
		contadorMemoria++;

}

void comportamientoVFF() {

	float auxi,auxi1;

	resultante.anguloQueLlevo=myencoders[2]; /* Nos quedamos con 2 decimales */
	auxi=resultante.anguloQueLlevo*100;
	auxi1=ceil(auxi);
	resultante.anguloQueLlevo=auxi1/100;

	auxi1=transformoAngulo(resultante.anguloQueLlevo);
	resultante.anguloQueLlevo=auxi1;

	auxi=resultante.anguloQueLlevo*100; /* Nos quedamos con 2 decimales */
	auxi1=ceil(auxi);
	resultante.anguloQueLlevo=(auxi1/100);

	rellenoMemoria(pdatos,validos,memoria);/* relleno memoria en funcion de los lasers */
	
	guardarCoordObjetivo(result); /* guardamos la FUERZA ATRACTIVA */

	/* Inicializo a TRUE avanzaEnGiro para que si no hay obstaculos por debajo de la seguridad se avanza a la vez que se gira */
	resultante.avanzaEnGiro=TRUE;

	calculaResultante(memoria,validos,result);

	/* Miro si he alcanzado el objetivo */
	if ((abs((int)(miObjetivo.x-myencoders[0]))<200)&&(abs((int)(miObjetivo.y-myencoders[1]))<200)) {   
		distanciaObjetivo=0;
		resultante.direccionFuerzaAtractiva[0]=0.0;
		resultante.direccionFuerzaAtractiva[1]=0.0;
	}	else distanciaObjetivo=1;

	/* Obtenemos los siguientes valores, para el posterior pintado de fuerzas */
	xRep=resultante.direccionFuerzaRepulsiva[0]; /* REPULSIVA */
	yRep=resultante.direccionFuerzaRepulsiva[1];

	xAtr=resultante.direccionFuerzaAtractiva[0]; /* ATRACTIVA */
	yAtr=resultante.direccionFuerzaAtractiva[1];

	xRes=resultante.direccionFuerzaResultante[0]; /* RESULTANTE = REPULSIVA vs. ATRACTIVA */
	yRes=resultante.direccionFuerzaResultante[1];

	difAngle=(resultante.anguloFuerzaResultante)-resultante.anguloQueLlevo;

	/* Sutileza para que el robot gire por el lado de menos giro */
	if (difAngle>180) difAngle=difAngle-360;
	if (difAngle<-180) difAngle=360+difAngle;

	peligro=0;

	if (!resultante.avanzaEnGiro) peligro=1;

	if (distanciaObjetivo==0) { /* estamos en el objetivo */
		*myv=0.0;
		*myw=0.0;
	} else { /* reglas AD-HOC */
		if (sitioEstrecho ()) { /* COMPORTAMIENTO VFF VS VENTANA*/
			*myv = vMax; 
			*myw = 0.0; /* a toa leche, podemos seguir recto por el sitio estrecho */
			rellenoEstela (SECWINDOW_COLOR);
		} else { /* COMPORTAMIENTO VFF PURO*/
			rellenoEstela (VFF_COLOR);
			if(peligro==1) { /* LO MAS URGENTE, SI HAY PELIGRO INMINENTE, GIRAMOS LENTAMENTE */
				if(difAngle<0) {*myv=0.0; *myw=wNegMax/2;}
				else {*myv=0.0; *myw=wPosMax/2;}
			}	else if (abs(difAngle)<limiteTrayectoriaSimilar) {*myv=vMax; *myw=0.0;} /* VIENTO EN POPA */
			else if (abs(difAngle)>limiteTrayectoriaDesigual) { /* GIRAMOS RAPIDAMENTE */
				if (difAngle<0) {*myv=0.0; *myw=wNegMax;}
				else {*myv=0.0; *myw=wPosMax;}
			}	else if (difAngle<0) {*myv=vMax/2; *myw=wNegMax/2;} /* MITAD, MITAD... (FIFTY, FIFTY) */
			else {*myv=vMax/2; *myw=wPosMax/2;}
		}
	} /* fin reglas AD-HOC */
}

/**********************************************************************************/
/**********************************************************************************/
/**************************COMPORTAMIENTO VENTANA**********************************/
/**********************************************************************************/   
void rellenarDistanciasVentana () { /* Concretamos las dimensiones de la ventana, según los parámetros ventanaY y ventanaX */
	int phi = 0; /* inicialmente comenzamos con el láser de ángulo 0 */

	while (tan (phi*DEGTORAD) <= (ventanaY/ventanaX)) { /* rellenamos la zona que tiene igual ventanaX */
		distanciasVentanaSeguridad [phi] = ventanaX / (cos (phi*DEGTORAD));
		phi ++;
	}

	while (phi < 90) { /* rellenamos la zona que tiene igual coseno */
		distanciasVentanaSeguridad [phi] = ventanaY / (sin (phi*DEGTORAD));
		phi ++;
	}
}

void rellenarHuecosVentana () { /* Concretamos las dimensiones de la ventana, según los parámetros ventanaY y huecoX */
	int phi = 0; /* inicialmente comenzamos con el láser de ángulo 0 */

	while (tan (phi*DEGTORAD) <= (ventanaY/huecoX)) { /* rellenamos la zona que tiene igual huecoX */
		distanciasHuecoSeguridad [phi] = huecoX / (cos (phi*DEGTORAD));
		phi ++;
	}

	while (phi < 90) { /* rellenamos la zona que tiene igual coseno */
		distanciasHuecoSeguridad [phi] = ventanaY / (sin (phi*DEGTORAD));
		phi ++;
	}
}

void rellenarFrenteVentana () {
	int phi = 0; /* inicialmente comenzamos con el láser de ángulo 0 */

	while ((tan (phi*3.14/180)) <= (frenteY/huecoX)) { /* rellenamos la zona que tiene igual huecoX */
		distanciasFrenteSeguridad [phi] = huecoX / (cos (phi*DEGTORAD));
		phi ++;
	}

	while (phi < 90) { /* rellenamos la zona que tiene igual coseno */
		distanciasFrenteSeguridad [phi] = frenteY / (sin (phi*DEGTORAD));
		phi ++;
	}
}

void rellenarVentanaSeguridad () {
	int i, j;

	for (i = 0;	i < 90; i ++) { /* primer cuadrante relleno */
		laser2xy (i, distanciasVentanaSeguridad [i], &ventanaSeguridad [i],&myencoders[0]);
	}

	for (i = 90, j = 89; i < 180; i ++, j--) { /* segundo cuadrante relleno */
		laser2xy (i, distanciasVentanaSeguridad [j], &ventanaSeguridad [i],&myencoders[0]);
	}
}

void rellenarFrenteSeguridad () {
	int i, j;

	for (i = 0;	i < 90; i ++) { /* primer cuadrante relleno */
		laser2xy (i, distanciasFrenteSeguridad [i], &frenteSeguridad [i],&myencoders[0]);
	}

	for (i = 90, j = 89; i < 180; i ++, j--) { /* segundo cuadrante relleno */
		laser2xy (i, distanciasFrenteSeguridad [j], &frenteSeguridad [i],&myencoders[0]);
	}
}

void rellenarHuecoSeguridad () {
	int i, j;

	for (i = 0;	i < 90; i ++) { /* primer cuadrante relleno */
		laser2xy (i, distanciasHuecoSeguridad [i], &huecoSeguridad [i],&myencoders[0]);
	}

	for (i = 90, j = 89; i < 180; i ++, j--) { /* segundo cuadrante relleno */
		laser2xy (i, distanciasHuecoSeguridad [j], &huecoSeguridad [i],&myencoders[0]);
	}
}

int calcularAnguloExtremo () {
	float phi = atan (ventanaY / huecoX);

	phi = phi*RADTODEG;
	phi = (int) ceil (phi);

	return (phi);
}

int calcularAnguloSemiExtremo () {
	float phi = atan (ventanaY / ventanaX);

	phi = phi*RADTODEG;
	phi = (int) ceil (phi);

	return (phi);
}

int obstaculoDelante () {
	int i, j;
	int obstacleFound = FALSE;
	int phiSemi = calcularAnguloSemiExtremo();

	i = phiSemi;
	while ((i < 90) && (obstacleFound==FALSE)) {
		if ((distanciasVentanaSeguridad[i] < mylaser[i]) && (mylaser[i] < distanciasFrenteSeguridad[i])) /* detectado obstáculo dentro de ventanas de seguridad*/
			obstacleFound = TRUE;
		i ++;
	}

	j = 89;
	if (obstacleFound == FALSE) { /* continuamos buscando en la segunda mitad*/
		while ((i < (180 - phiSemi)) && (obstacleFound==FALSE)) {
			if ((distanciasVentanaSeguridad[j] < mylaser[i]) && (mylaser[i] < distanciasFrenteSeguridad [j]))
				obstacleFound = TRUE;
			i++;
			j--;
		}
	}

	return (obstacleFound);
}

int obstaculoEnVentana () {
	int i = 0, j;
	int obstacleFound = FALSE;

	while ((i < 90) && (obstacleFound==FALSE)) {
		if (mylaser[i] < distanciasVentanaSeguridad[i]) /* detectado obstáculo dentro de ventana de seguridad*/
			obstacleFound = TRUE;
		i ++;
	}

	j = 89;
	if (obstacleFound == FALSE) { /* continuamos buscando en la segunda mitad*/
		while ((i < 180) && (obstacleFound==FALSE)) {
			if (mylaser[i] < distanciasVentanaSeguridad[j])
				obstacleFound = TRUE;
			i++;
			j--;
		}
	}

	return (obstacleFound);
}

int sitioEstrecho () {
	int i = 0, j;
	int obstacleFound = FALSE;

	if (!obstaculoEnVentana ()) { /* si no hay en ventana ningún obstaculo...*/
		if (!obstaculoDelante ()) { /* y si no hay en el frente, comprobamos que sí hay a los lados*/
			while ((i < 90) && (obstacleFound==FALSE)) {
				if ((distanciasVentanaSeguridad[i] < mylaser[i]) && (mylaser[i] < distanciasHuecoSeguridad[i])) /* detectado obstáculo dentro de ventanas de seguridad*/
					obstacleFound = TRUE;
				i ++;
			}

			j = 89;
			if (obstacleFound == FALSE) { /* continuamos buscando en la segunda mitad*/
				while ((i < 180) && (obstacleFound==FALSE)) {
					if ((distanciasVentanaSeguridad[j] < mylaser[i]) && (mylaser[i] < distanciasHuecoSeguridad [j]))
						obstacleFound = TRUE;
					i++;
					j--;
				}
			}
		} /* fin si !obstaculoDelante*/
	} /* fin si !obstaculoEnVentana*/

	return (obstacleFound); /* devuelve si estamos en sitio estrecho*/
}

/**********************************************************************************/
/**********************************************************************************/
/**********************************F I N*******************************************/
/**********************************************************************************/

const char *localNavigation_range(FL_OBJECT *ob, double value, int prec) {
	static char buf[32];

	sprintf(buf,"%.1f",value/1000.);
	return buf;
}

int xy2localNavigationcanvas(Tvoxel point, XPoint* grafico) {
     /* return -1 if point falls outside the canvas */
	float xi, yi;

	xi = (point.x * localNavigation_odometrico[3] - point.y * localNavigation_odometrico[4] + localNavigation_odometrico[0])*localNavigation_escala;
	yi = (point.x * localNavigation_odometrico[4] + point.y * localNavigation_odometrico[3] + localNavigation_odometrico[1])*localNavigation_escala;
	/* Con esto cambiamos al sistema de referencia de visualizacion, centrado en algun punto xy y con alguna orientacion definidos por odometrico. Ahora cambiamos de ese sistema al del display, donde siempre hay un desplazamiento a la esquina sup. izda. y que las y se cuentan para abajo. */

	grafico->x = xi + localNavigation_width/2;
	grafico->y = -yi + localNavigation_height/2;

	if ((grafico->x <0)||(grafico->x>localNavigation_width)) return -1; 
	if ((grafico->y <0)||(grafico->y>localNavigation_height)) return -1; 
	return 0;
}

void localNavigation_iteration() {
	speedcounter(localNavigation_id);
	
	if( golocalNavigation ) /*Si el esquema debe funcionar*/
	{
		/*Escogemos el objetivo de donde nos diga el archivo de configuracion*/
		if (useInput == 1)
		{
			miObjetivo.x = target.x;
			miObjetivo.y = target.y;
		}
		else
		{
			miObjetivo.x=localNavigation_mouse_x;
			miObjetivo.y=localNavigation_mouse_y;
		}

		comportamientoVFF (); /* usamos comportamiento VFF + VENTANA SOBRE MEMORIA --MUY BUENO --*/
	}
}

void localNavigation_imports()
{
  mylaser=(int *)myimport("laser","laser");
  laserresume=(runFn)myimport("laser","run");
  lasersuspend=(stopFn *)myimport("laser","stop");

  myencoders=(float *)myimport("encoders","jde_robot");
  encodersresume=(runFn)myimport("encoders","run");
  encoderssuspend=(stopFn)myimport("encoders","stop");

  myv=(float *)myimport("motors","v");
  myw=(float *)myimport("motors","w");
  motorsresume=(runFn)myimport("motors","run");
  motorssuspend=(stopFn)myimport("motors","stop");
}

void localNavigation_exports()
{
	myexport("localNavigation","id",&localNavigation_id);
  myexport("localNavigation","run",(void *) &localNavigation_run);
  myexport("localNavigation","stop",(void *) &localNavigation_stop);
	myexport("localNavigation","target",&target);
	myexport("localNavigation","golocalNavigation",&golocalNavigation);
	myexport("localNavigation","distanciaObjetivo",&distanciaObjetivo);/*Flag alcanzado objetivo, vale 0 si estamos en el objetivo*/
}

void localNavigation_stop() {
  pthread_mutex_lock(&(all[localNavigation_id].mymutex));
  put_state(localNavigation_id,slept);
  all[localNavigation_id].children[(*(int *)myimport("laser","id"))]=FALSE;
  all[localNavigation_id].children[(*(int *)myimport("motors","id"))]=FALSE;
  all[localNavigation_id].children[(*(int *)myimport("encoders","id"))]=FALSE;
  lasersuspend();
  encoderssuspend();
  motorssuspend();
  printf("localNavigation: off\n");
  pthread_mutex_unlock(&(all[localNavigation_id].mymutex));
}

void localNavigation_run(int father, int *brothers, arbitration fn) {
  int i;
 
  pthread_mutex_lock(&(all[localNavigation_id].mymutex));
  /* this schema resumes its execution with no children at all */
  for(i=0;i<MAX_SCHEMAS;i++) all[localNavigation_id].children[i]=FALSE;
 
  all[localNavigation_id].father=father;
  if (brothers!=NULL)
    {
      for(i=0;i<MAX_SCHEMAS;i++) localNavigation_brothers[i]=-1;
      i=0;
      while(brothers[i]!=-1) {localNavigation_brothers[i]=brothers[i];i++;}
    }
  localNavigation_callforarbitration=fn;
  put_state(localNavigation_id,notready);
	localNavigation_imports();
  printf("localNavigation: on\n");
  pthread_cond_signal(&(all[localNavigation_id].condition));
  pthread_mutex_unlock(&(all[localNavigation_id].mymutex));
}

void *localNavigation_thread(void *not_used) 
{
  struct timeval a,b;
  long diff, next;

  for(;;)
    {
      pthread_mutex_lock(&(all[localNavigation_id].mymutex));

      if (all[localNavigation_id].state==slept) 
	{
	  pthread_cond_wait(&(all[localNavigation_id].condition),&(all[localNavigation_id].mymutex));
	  pthread_mutex_unlock(&(all[localNavigation_id].mymutex));
	}
      else 
	{
	  /* check preconditions. For now, preconditions are always satisfied*/
	  if (all[localNavigation_id].state==notready) put_state(localNavigation_id,ready);
	  else if (all[localNavigation_id].state==ready)	  /* check brothers and arbitrate. For now this is the only winner */
	    { 
	      put_state(localNavigation_id,winner);
	      *myv=0; *myw=0; 
	      /* start the winner state from controlled motor values */ 
	      all[localNavigation_id].children[(*(int *)myimport("laser","id"))]=TRUE;
	      all[localNavigation_id].children[(*(int *)myimport("motors","id"))]=TRUE;
	      all[localNavigation_id].children[(*(int *)myimport("encoders","id"))]=TRUE;
	      laserresume(localNavigation_id,NULL,NULL);
	      encodersresume(localNavigation_id,NULL,NULL);
	      motorsresume(localNavigation_id,NULL,NULL);
	    }	  
	  else if (all[localNavigation_id].state==winner);

	  if (all[localNavigation_id].state==winner)
	    /* I'm the winner and must execute my iteration */
	    {
	      pthread_mutex_unlock(&(all[localNavigation_id].mymutex));
	      gettimeofday(&a,NULL);
	      localNavigation_iteration();
	      gettimeofday(&b,NULL);  

	      diff = (b.tv_sec-a.tv_sec)*1000000+b.tv_usec-a.tv_usec;
	      next = localNavigation_cycle*1000-diff-10000; 
	      if (next>0) 
		/* discounts 10ms taken by calling usleep itself */
		usleep(localNavigation_cycle*1000-diff);
	      else 
		/* just let this iteration go away. overhead time negligible */
		{/*printf("time interval violated: localNavigation\n"); */
		usleep(localNavigation_cycle*1000);
		}
	    }
	  else 
	    {
	      pthread_mutex_unlock(&(all[localNavigation_id].mymutex));
	      usleep(localNavigation_cycle*1000);
	    }
	}
    }
}

void localNavigation_init(char *configfile) {
	int i, hora;/*Manejo de la memoria*/

  pthread_mutex_lock(&(all[localNavigation_id].mymutex));
	loadconfig(configfile);
	setvar("enableInput","int",&useInput);
  localNavigation_exports();
  printf("localNavigation schema started up\n");
  put_state(localNavigation_id,slept);
  pthread_create(&(all[localNavigation_id].mythread),NULL,localNavigation_thread,NULL);

	init_pioneer ();

	if (myregister_buttonscallback==NULL){
    if ((myregister_buttonscallback=(registerbuttons)myimport ("graphics_xforms", "register_buttonscallback"))==NULL){
      if (DEBUG) printf("I can't fetch register_buttonscallback from graphics_xforms\n");
      jdeshutdown(1);
    }
    if ((mydelete_buttonscallback=(deletebuttons)myimport ("graphics_xforms", "delete_buttonscallback"))==NULL){
      if (DEBUG) printf("I can't fetch delete_buttonscallback from graphics_xforms\n");
      jdeshutdown(1);
    }
    if ((myregister_displaycallback=(registerdisplay)myimport ("graphics_xforms", "register_displaycallback"))==NULL){
      if (DEBUG) printf("I can't fetch register_displaycallback from graphics_xforms\n");
      jdeshutdown(1);
    }
    if ((mydelete_displaycallback=(deletedisplay)myimport ("graphics_xforms", "delete_displaycallback"))==NULL){
      jdeshutdown(1);
      if (DEBUG) printf("I can't fetch delete_displaycallback from graphics_xforms\n");
    }
  }

  if ((myscreen=(int *)myimport("graphics_xforms", "screen"))==NULL){
    fprintf(stderr, "localNavigation: I can't fetch screen from graphics_xforms\n");
    jdeshutdown(1);
  }
  if ((display=(Display *)myimport("graphics_xforms", "display"))==NULL){
    fprintf(stderr, "localNavigation: I can't fetch display from graphics_xforms\n");
    jdeshutdown(1);
  }
  pthread_mutex_unlock(&(all[localNavigation_id].mymutex));

	localNavigation_escala = 20000; /* escala del mundillo*/

	numEstelas = 0; /* contador para la estela (rastro) del robot*/


  contadorMemoria=0;	
  result=&resultante;
  pdatos=&datos;
  hora=dameTiempo();
	num_puntos = 0;

  for(i=0;i<tamMemoria;i++)
	  validos[i]=FALSE;

  for(i=0;i<tamMemoria;i++)
      datos.horaInicio[i]=hora;

  datos.ultimo=0;

	/* COMPORTAMIENTO VENTANA: rellenamos nuestras tres ventanas de seguridad */
	rellenarHuecosVentana (); /* ventana con la que conseguimos los huecos laterales*/
	rellenarDistanciasVentana (); /* ventana minima de seguridad (central)*/
	rellenarFrenteVentana (); /* ventana con la que conseguimos el hueco del frente*/

}
void localNavigation_terminate()
{
	printf("localNavigation stop\n");
}
void localNavigation_guibuttons(void *obj1)
{

  FL_OBJECT *obj=(FL_OBJECT *)obj1;

	if (obj == fd_localNavigationgui->exit) localNavigation_hide();
	else if (obj == fd_localNavigationgui->stop) {
		localNavigation_stop ();
	} else if (obj == fd_localNavigationgui->escala) {
		localNavigation_rango = fl_get_slider_value (fd_localNavigationgui->escala);
    localNavigation_visual_refresh = TRUE; /* activa el flag que limpia el fondo de la pantalla y repinta todo */ 
    localNavigation_escala = localNavigation_width / localNavigation_rango;
  } else if (obj== fd_localNavigationgui->center) { /* Se mueve 10%un porcentaje del rango */
		localNavigation_odometrico[0]+=localNavigation_rango*(fl_get_positioner_xvalue(fd_localNavigationgui->center)-0.5)*(-2.)*(0.1);
		localNavigation_odometrico[1]+=localNavigation_rango*(fl_get_positioner_yvalue(fd_localNavigationgui->center)-0.5)*(-2.)*(0.1);
		fl_set_positioner_xvalue(fd_localNavigationgui->center,0.5);
		fl_set_positioner_yvalue(fd_localNavigationgui->center,0.5);
		localNavigation_visual_refresh = TRUE;
  } else if (obj == fd_localNavigationgui->track_robot) {
		if (fl_get_button(obj)==PUSHED) localNavigation_trackrobot=TRUE;
		else localNavigation_trackrobot=FALSE;
	} else if (obj == fd_localNavigationgui->trailButton) {
		if (fl_get_button(obj)==PUSHED) DISPLAY_ESTELA = 0x80UL;
		else DISPLAY_ESTELA = 0x0;
	} else if (obj == fd_localNavigationgui->laserButton) {
		if (fl_get_button(obj)==PUSHED) DISPLAY_LASER = 0x08UL;
		else DISPLAY_LASER = 0x0;
	} else if (obj == fd_localNavigationgui->memoryButton) {
		if (fl_get_button(obj)==PUSHED) DISPLAY_MEMORY = 0x40UL;
		else DISPLAY_MEMORY = 0x0;
	} else if (obj == fd_localNavigationgui->secWindowButton) {
		if (fl_get_button(obj)==PUSHED) DISPLAY_VENTANA = 0x60UL;
		else DISPLAY_VENTANA = 0x0;
	} else if (obj == fd_localNavigationgui->forcesButton) {
		if (fl_get_button(obj)==PUSHED) DISPLAY_FUERZAS = 0x24UL;
		else DISPLAY_FUERZAS = 0x0;
	}
}

void localNavigation_guidisplay() {
	char text[80]="";
	static float k=0;
	int i;
	Tvoxel aa,bb;
	static XPoint targetgraf;
	XPoint a,b;

	/* slow refresh of the complete localNavigation gui, needed because incremental refresh misses window occlusions */
	if (localNavigation_iteracion_display*localNavigation_cycle>FORCED_REFRESH)
	{
		localNavigation_visual_refresh=TRUE;
		localNavigation_iteracion_display = 0;
	}
	else localNavigation_iteracion_display++;

	k=k+1.;
	sprintf(text,"%.1f",k);

	fl_winset(localNavigation_canvas_win); 

	if ((localNavigation_trackrobot==TRUE)&&
		((fabs(myencoders[0]+localNavigation_odometrico[0])>(localNavigation_rango/4.))||
		(fabs(myencoders[1]+localNavigation_odometrico[1])>(localNavigation_rango/4.)))) {

		localNavigation_odometrico[0]=-myencoders[0];
		localNavigation_odometrico[1]=-myencoders[1];
		localNavigation_visual_refresh = TRUE;
	}

	if (localNavigation_visual_refresh==TRUE)	{
		fl_rectbound(0,0,localNavigation_width,localNavigation_height,FL_WHITE);   
		XFlush(display);
	}

	/* the grid at the floor */
	fl_set_foreground(localNavigationgui_gc,FL_LEFT_BCOL); 
	for(i=0;i<31;i++)	{
		aa.x=-15000.+(float)i*1000;
		aa.y=-15000.;
		bb.x=-15000.+(float)i*1000;
		bb.y=15000.;
		xy2localNavigationcanvas(aa,&a);
		xy2localNavigationcanvas(bb,&b);
		XDrawLine(display,localNavigation_canvas_win,localNavigationgui_gc,a.x,a.y,b.x,b.y);
		aa.y=-15000.+(float)i*1000;
		aa.x=-15000.;
		bb.y=-15000.+(float)i*1000;
		bb.x=15000.;
		xy2localNavigationcanvas(aa,&a);
		xy2localNavigationcanvas(bb,&b);
		XDrawLine(display,localNavigation_canvas_win,localNavigationgui_gc,a.x,a.y,b.x,b.y);
	}

	/* CALCULO DE FUERZAS, PARA SU PINTADO */
	fuerzaResultante.x=myencoders[0]+xRes;
	fuerzaResultante.y=myencoders[1]+yRes;
	repulsiva.x=myencoders[0]+xRep;
	repulsiva.y=myencoders[1]+yRep;
	atractiva.x=myencoders[0]+xAtr;
	atractiva.y=myencoders[1]+yAtr;
	robott.x=myencoders[0];
	robott.y=myencoders[1];	

	/* VISUALIZACION DE LA ESTELA DEL ROBOT */
	if ((localNavigation_display_state&DISPLAY_ESTELA)!=0) {
		numEstelas=0;
		for	(i=0;i<tamMemoria/2;i++) {
				xy2localNavigationcanvas(estelaRecorrido[i].pos,&coordGrafEstela[numEstelas]);
				numEstelas++;
				if (estelaRecorrido[i].color == VFF_COLOR)
					fl_set_foreground(localNavigationgui_gc,FL_CYAN);
				else
					fl_set_foreground(localNavigationgui_gc,FL_MAGENTA);
				XDrawPoint(display,localNavigation_canvas_win,localNavigationgui_gc,coordGrafEstela[numEstelas].x,coordGrafEstela[numEstelas].y);
		}
	}

	

	/* VISUALIZACION DE UNA INSTANTANEA LASER */
	if ((((localNavigation_display_state&DISPLAY_LASER)!=0)&&(localNavigation_visual_refresh==FALSE))
	|| (visual_delete_localNavigation_laser==TRUE))	{
		// Limpiamos los puntos viejos  
		fl_set_foreground(localNavigationgui_gc,FL_WHITE); 
		XDrawPoints(display,localNavigation_canvas_win,localNavigationgui_gc,localNavigation_laser_dpy,NUM_LASER,CoordModeOrigin);
	}

	// Pinto los nuevos puntos de los LASERS
	if ((localNavigation_display_state&DISPLAY_LASER)!=0) {
		for(i=0;i<NUM_LASER;i++) {
			laser2xy(i,mylaser[i],&aa,&myencoders[0]);
			xy2localNavigationcanvas(aa,&localNavigation_laser_dpy[i]);
		}
		fl_set_foreground(localNavigationgui_gc,FL_BLUE);
		XDrawPoints(display,localNavigation_canvas_win,localNavigationgui_gc,localNavigation_laser_dpy,NUM_LASER,CoordModeOrigin);
	}

	/* VISUALIZACION DE UNA INSTANTANEA DE MEMORIA DE PUNTOS */
	if ((((localNavigation_display_state&DISPLAY_MEMORY)!=0)&&(localNavigation_visual_refresh==FALSE)) || 		(visual_delete_localNavigation_memory==TRUE)) {
		//pinto en blanco los puntos anteriores
		fl_set_foreground(localNavigationgui_gc,FL_WHITE);
		XDrawPoints(display,localNavigation_canvas_win,localNavigationgui_gc,coord_graf_memoria,num_puntos,CoordModeOrigin);
	}

	// Pinto los nuevos puntos de la MEMORIA DE PUNTOS
	if ((localNavigation_display_state&DISPLAY_MEMORY)!=0) {
		num_puntos=0;
		for	(i=0;i<tamMemoria;i++) {
			if(validos[i]) {  
				xy2localNavigationcanvas(memoria[i],&coord_graf_memoria[num_puntos]);
				num_puntos++;
			}
		}
		fl_set_foreground(localNavigationgui_gc,FL_DARKORANGE);
		XDrawPoints(display,localNavigation_canvas_win,localNavigationgui_gc,coord_graf_memoria,num_puntos,CoordModeOrigin);
	}

	/* VISUALIZACION DE UNA INSTANTANEA DE VENTANA DE SEGURIDAD */
	if ((((localNavigation_display_state&DISPLAY_VENTANA)!=0)&&(localNavigation_visual_refresh==FALSE)) || 					(visual_delete_localNavigation_ventana==TRUE)) {
		//pinto en blanco los puntos anteriores
		fl_set_foreground(localNavigationgui_gc,FL_WHITE);
		XDrawPoints(display,localNavigation_canvas_win,localNavigationgui_gc,coord_graf_ventana,180,CoordModeOrigin);
		XDrawPoints(display,localNavigation_canvas_win,localNavigationgui_gc,coord_graf_hueco,180,CoordModeOrigin);
		XDrawPoints(display,localNavigation_canvas_win,localNavigationgui_gc,coord_graf_frente,180,CoordModeOrigin);
	}

	// Pinto los nuevos puntos de la VENTANA DE SEGURIDAD
	if ((localNavigation_display_state&DISPLAY_VENTANA)!=0) {
		rellenarVentanaSeguridad (); // obtengo las ventanas actuales
		rellenarHuecoSeguridad ();
		rellenarFrenteSeguridad ();

		for (i = 0; i < 180; i ++) { // paso las ventanas actuales a coordenadas gráficas
			xy2localNavigationcanvas(ventanaSeguridad[i],&coord_graf_ventana[i]);
			xy2localNavigationcanvas(huecoSeguridad[i],&coord_graf_hueco[i]);
			xy2localNavigationcanvas(frenteSeguridad[i],&coord_graf_frente[i]);
		}

		fl_set_foreground(localNavigationgui_gc,FL_BLACK); // y finalmente dibujo las ventanas
		XDrawPoints(display,localNavigation_canvas_win,localNavigationgui_gc,coord_graf_ventana,180,CoordModeOrigin);
		XDrawPoints(display,localNavigation_canvas_win,localNavigationgui_gc,coord_graf_hueco,180,CoordModeOrigin);
		XDrawPoints(display,localNavigation_canvas_win,localNavigationgui_gc,coord_graf_frente,180,CoordModeOrigin);
	}

	/* SECCION PINTADO DE FUERZAS */
	if ((distanciaObjetivo!=0) && ((localNavigation_display_state&DISPLAY_FUERZAS)!=0)) {
		//Borro las fuerzas de la iteracion anterior
		fl_set_foreground(localNavigationgui_gc,FL_WHITE);
		XDrawLine(display,localNavigation_canvas_win,localNavigationgui_gc,graf_robot.x,graf_robot.y,coord_graf_objetivo.x,coord_graf_objetivo.y);

		fl_set_foreground(localNavigationgui_gc,FL_WHITE);
		XDrawLine(display,localNavigation_canvas_win,localNavigationgui_gc,graf_robot.x,graf_robot.y,graf_res.x,graf_res.y);

		fl_set_foreground(localNavigationgui_gc,FL_WHITE);
		XDrawLine(display,localNavigation_canvas_win,localNavigationgui_gc,graf_robot.x,graf_robot.y,graf_rep.x,graf_rep.y);

		fl_set_foreground(localNavigationgui_gc,FL_WHITE);
		XDrawLine(display,localNavigation_canvas_win,localNavigationgui_gc,graf_robot.x,graf_robot.y,graf_atr.x,graf_atr.y);

		//pinto las nuevas lineas
		xy2localNavigationcanvas(miObjetivo,&coord_graf_objetivo);
		xy2localNavigationcanvas(robott,&graf_robot);
		xy2localNavigationcanvas(fuerzaResultante,&graf_res);
		xy2localNavigationcanvas(repulsiva,&graf_rep);
		xy2localNavigationcanvas(atractiva,&graf_atr);

		fl_set_foreground(localNavigationgui_gc,FL_YELLOW); /* OBJETIVO */
		XDrawLine(display,localNavigation_canvas_win,localNavigationgui_gc,graf_robot.x,graf_robot.y,coord_graf_objetivo.x,coord_graf_objetivo.y);

		fl_set_foreground(localNavigationgui_gc,FL_BLUE); /* FUERZA RESULTANTE */
		XDrawLine(display,localNavigation_canvas_win,localNavigationgui_gc,graf_robot.x,graf_robot.y,graf_res.x,graf_res.y);

		fl_set_foreground(localNavigationgui_gc,FL_RED); /* FUERZA REPULSIVA */
		XDrawLine(display,localNavigation_canvas_win,localNavigationgui_gc,graf_robot.x,graf_robot.y,graf_rep.x,graf_rep.y);

		fl_set_foreground(localNavigationgui_gc,FL_GREEN); /* FUERZA ATRACTIVA */
		XDrawLine(display,localNavigation_canvas_win,localNavigationgui_gc,graf_robot.x,graf_robot.y,graf_atr.x,graf_atr.y);
	}
	/* FIN PINTADO DE FUERZAS */

	/*VISUALIZACION DEL OBJETIVO*/
  /* visualization of VFF target */
  if ((oldvfftarget.x!=vfftarget.x)||(oldvfftarget.y!=vfftarget.y))
	/* the target has changed, do its last position must be cleaned from the canvas */
	{
	  fl_set_foreground(localNavigationgui_gc,FL_WHITE);
    XDrawLine(display,localNavigation_canvas_win,localNavigationgui_gc,targetgraf.x-5,targetgraf.y,targetgraf.x+5,targetgraf.y);
    XDrawLine(display,localNavigation_canvas_win,localNavigationgui_gc,targetgraf.x,targetgraf.y-5,targetgraf.x,targetgraf.y+5);
	}
      fl_set_foreground(localNavigationgui_gc,FL_RED);
      xy2localNavigationcanvas(miObjetivo,&targetgraf);
      XDrawLine(display,localNavigation_canvas_win,localNavigationgui_gc,targetgraf.x-5,targetgraf.y,targetgraf.x+5,targetgraf.y);
      XDrawLine(display,localNavigation_canvas_win,localNavigationgui_gc,targetgraf.x,targetgraf.y-5,targetgraf.x,targetgraf.y+5);
	/*FIN VISUALIZACION DEL OBJETIVO*/

	/* VISUALIZACION: PINTAR O BORRAR DE EL PROPIO ROBOT.
	Siempre hay un repintado total. Esta es la ultima estructura que se se pinta, para que ninguna otra se solape encima 		*/

	if ((((localNavigation_display_state&DISPLAY_ROBOT)!=0) &&(localNavigation_visual_refresh==FALSE))
	|| (visual_delete_localNavigation_ego==TRUE))	{  
		fl_set_foreground(localNavigationgui_gc,FL_WHITE);

		/* clean last robot, but only if there wasn't a total refresh. In case of total refresh the white rectangle already cleaned all */
		for(i=0;i<numlocalNavigation_ego;i++) 
			XDrawLine(display,localNavigation_canvas_win,localNavigationgui_gc,localNavigation_ego[i].x,localNavigation_ego[i].y,localNavigation_ego[i+1].x,localNavigation_ego[i+1].y);
	}

	if ((localNavigation_display_state&DISPLAY_ROBOT)!=0) {
		fl_set_foreground(localNavigationgui_gc,FL_MAGENTA);
		/* relleno los nuevos */
		us2xy(15,0.,0.,&aa,&myencoders[0]);
		xy2localNavigationcanvas(aa,&localNavigation_ego[0]);
		us2xy(3,0.,0.,&aa,&myencoders[0]);
		xy2localNavigationcanvas(aa,&localNavigation_ego[1]);
		us2xy(4,0.,0.,&aa,&myencoders[0]);
		xy2localNavigationcanvas(aa,&localNavigation_ego[2]);
		us2xy(8,0.,0.,&aa,&myencoders[0]);
		xy2localNavigationcanvas(aa,&localNavigation_ego[3]);
		us2xy(15,0.,0.,&aa,&myencoders[0]);
		xy2localNavigationcanvas(aa,&localNavigation_ego[EGOMAX-1]);
		for(i=0;i<NUM_SONARS;i++) {
			us2xy((15+i)%NUM_SONARS,0.,0.,&aa,&myencoders[0]); /* Da en el Tvoxel aa las coordenadas del sensor, pues es distancia 0 */
			xy2localNavigationcanvas(aa,&localNavigation_ego[i+4]);       
		}

		/* pinto los nuevos */
		numlocalNavigation_ego=EGOMAX-1;
		for (i=0;i<numlocalNavigation_ego;i++) 
			XDrawLine(display,localNavigation_canvas_win,localNavigationgui_gc,localNavigation_ego[i].x,localNavigation_ego[i].y,localNavigation_ego[i+1].x,localNavigation_ego[i+1].y);

		fl_set_foreground(localNavigationgui_gc,FL_BLUE);
		XDrawPoints(display,localNavigation_canvas_win,localNavigationgui_gc,GraphicGlobalDestination,9,CoordModeOrigin);		
	}

	/* clear all flags. If they were set at the beginning, they have been already used in this iteration */
	localNavigation_visual_refresh=FALSE;
	visual_delete_localNavigation_laser=FALSE; 
	visual_delete_localNavigation_ego=FALSE;
	visual_delete_localNavigation_memory = FALSE;

}

/* callback function for button pressed inside the canvas object*/
int localNavigation_button_pressed_on_micanvas(FL_OBJECT *ob, Window win, int win_width, int win_height, XEvent *xev, void *user_data)
{
  unsigned int keymap;
  float ygraf, xgraf;
  FL_Coord x,y;

  /* in order to know the mouse button that created the event */
  localNavigation_mouse_button=xev->xkey.keycode;

  if(localNavigation_canvas_mouse_button_pressed==0){
    if ((localNavigation_mouse_button==MOUSEMIDDLE))
      {
	/* Target for VFF localNavigation getting mouse coordenates */
      fl_get_win_mouse(win,&x,&y,&keymap);
      ygraf=((float) (localNavigation_height/2-y))/localNavigation_escala;
      xgraf=((float) (x-localNavigation_width/2))/localNavigation_escala;
      oldvfftarget.x=vfftarget.x;
      oldvfftarget.y=vfftarget.y;
      vfftarget.y=(ygraf-localNavigation_odometrico[1])*localNavigation_odometrico[3]+(-xgraf+localNavigation_odometrico[0])*localNavigation_odometrico[4];
      vfftarget.x=(ygraf-localNavigation_odometrico[1])*localNavigation_odometrico[4]+(xgraf-localNavigation_odometrico[0])*localNavigation_odometrico[3];
     }
    else if((localNavigation_mouse_button==MOUSELEFT)||(localNavigation_mouse_button==MOUSERIGHT)){
      /* a button has been pressed */
      localNavigation_canvas_mouse_button_pressed=1;
      
      /* getting mouse coordenates. win will be always the canvas window, because this callback has been defined only for that canvas */  
      fl_get_win_mouse(win,&localNavigation_x_canvas,&localNavigation_y_canvas,&keymap);
      old_localNavigation_x_canvas=localNavigation_x_canvas;
      old_localNavigation_y_canvas=localNavigation_y_canvas;
      
      /* from graphical coordinates to spatial ones */
      ygraf=((float) (localNavigation_height/2-localNavigation_y_canvas))/localNavigation_escala;
      xgraf=((float) (localNavigation_x_canvas-localNavigation_width/2))/localNavigation_escala;
      localNavigation_mouse_y=(ygraf-localNavigation_odometrico[1])*localNavigation_odometrico[3]+(-xgraf+localNavigation_odometrico[0])*localNavigation_odometrico[4];
      localNavigation_mouse_x=(ygraf-localNavigation_odometrico[1])*localNavigation_odometrico[4]+(xgraf-localNavigation_odometrico[0])*localNavigation_odometrico[3];
      localNavigation_mouse_new=1;
      
    }else if(localNavigation_mouse_button==MOUSEWHEELDOWN){
      /* a button has been pressed */
      localNavigation_canvas_mouse_button_pressed=1;

      /* modifing scale */
      localNavigation_rango-=1000;
      if(localNavigation_rango<=RANGO_MIN) localNavigation_rango=RANGO_MIN;
      localNavigation_visual_refresh = TRUE; /* activa el flag que limpia el fondo de la pantalla y repinta todo */ 
      localNavigation_escala = localNavigation_width /localNavigation_rango;

    }else if(localNavigation_mouse_button==MOUSEWHEELUP){
      /* a button has been pressed */
      localNavigation_canvas_mouse_button_pressed=1;

      /* modifing scale */
      localNavigation_rango+=1000;
      if(localNavigation_rango>=RANGO_MAX) localNavigation_rango=RANGO_MAX;
      localNavigation_visual_refresh = TRUE; /* activa el flag que limpia el fondo de la pantalla y repinta todo */ 
      localNavigation_escala = localNavigation_width /localNavigation_rango;
    }
  }
  return 0;
}

/* callback function for mouse motion inside the canvas object*/
int localNavigation_mouse_motion_on_micanvas(FL_OBJECT *ob, Window win, int win_width, int win_height, XEvent *xev, void *user_data)
{  
  float diff_x,diff_y,diff_w;
  unsigned int keymap;
  float sqrt_value,acos_value;

  if(localNavigation_canvas_mouse_button_pressed==1){

    /* getting mouse coordenates. win will be always the canvas window, because this callback has been defined only for that canvas */  
    fl_get_win_mouse(win,&localNavigation_x_canvas,&localNavigation_y_canvas,&keymap);
	}
  return 0;
}

/* callback function for button released inside the canvas object*/
int localNavigation_button_released_on_micanvas(FL_OBJECT *ob, Window win, int win_width, int win_height, XEvent *xev, void *user_data)
{
  if(localNavigation_canvas_mouse_button_pressed==1){
    /* a button has been released */
    localNavigation_canvas_mouse_button_pressed=0;
  }
  return 0;
}

void localNavigation_guisuspend_aux(void)
{
  /* to make a safety stop when the robot is being teleoperated from GUI */
  mydelete_buttonscallback(localNavigation_guibuttons);
  mydelete_displaycallback(localNavigation_guidisplay);
  fl_hide_form(fd_localNavigationgui->localNavigationgui);
}

void localNavigation_hide(){
  static callback fn=NULL;
  if (fn==NULL){
    if ((fn=(callback)myimport ("graphics_xforms", "suspend_callback"))!=NULL){
      fn ((gui_function)localNavigation_guisuspend_aux);
    }
  }
  else{
    fn ((gui_function)localNavigation_guisuspend_aux);
  }
}

void localNavigation_guiresume_aux(void)
{
  static int k=0;
  XGCValues gc_values;

  if (k==0) /* not initialized */
    {
      k++;
      /* Coord del sistema odometrico respecto del visual */
      localNavigation_odometrico[0]=0.;
      localNavigation_odometrico[1]=0.;
      localNavigation_odometrico[2]=0.;
      localNavigation_odometrico[3]= cos(0.);
      localNavigation_odometrico[4]= sin(0.);

      localNavigation_display_state = localNavigation_display_state | DISPLAY_LASER;
      localNavigation_display_state = localNavigation_display_state | DISPLAY_ROBOT;
      localNavigation_display_state = localNavigation_display_state | DISPLAY_MEMORY;
			localNavigation_display_state = localNavigation_display_state | DISPLAY_ESTELA;
			localNavigation_display_state = localNavigation_display_state | DISPLAY_FUERZAS;
			localNavigation_display_state = localNavigation_display_state | DISPLAY_VENTANA;

      fd_localNavigationgui = create_form_localNavigationgui();
      fl_set_form_position(fd_localNavigationgui->localNavigationgui,400,50);
      fl_show_form(fd_localNavigationgui->localNavigationgui,FL_PLACE_POSITION,FL_FULLBORDER,"GUIA MUSEO. NAVEGACIÓN LOCAL");
      localNavigation_canvas_win= FL_ObjWin(fd_localNavigationgui->micanvas);
      gc_values.graphics_exposures = False;
      localNavigationgui_gc = XCreateGC(display, localNavigation_canvas_win, GCGraphicsExposures, &gc_values);  

      /* canvas handlers */
      fl_add_canvas_handler(fd_localNavigationgui->micanvas,ButtonPress,localNavigation_button_pressed_on_micanvas,NULL);
      fl_add_canvas_handler(fd_localNavigationgui->micanvas,ButtonRelease,localNavigation_button_released_on_micanvas,NULL);
      fl_add_canvas_handler(fd_localNavigationgui->micanvas,MotionNotify,localNavigation_mouse_motion_on_micanvas,NULL);

			fl_set_button(fd_localNavigationgui->trailButton, RELEASED);
			fl_set_button(fd_localNavigationgui->laserButton, RELEASED);
			fl_set_button(fd_localNavigationgui->memoryButton, RELEASED);
			fl_set_button(fd_localNavigationgui->secWindowButton, RELEASED);
			fl_set_button(fd_localNavigationgui->forcesButton, RELEASED);
    }
  else 
    {
      fl_show_form(fd_localNavigationgui->localNavigationgui,FL_PLACE_POSITION,FL_FULLBORDER,"GUIA MUSEO. NAVEGACIÓN LOCAL");
      localNavigation_canvas_win= FL_ObjWin(fd_localNavigationgui->micanvas);
    }

  /* Empiezo con el canvas en blanco */
  localNavigation_width = fd_localNavigationgui->micanvas->w;
  localNavigation_height = fd_localNavigationgui->micanvas->h;
  fl_winset(localNavigation_canvas_win); 
  fl_rectbound(0,0,localNavigation_width,localNavigation_height,FL_WHITE);   
  /*  XFlush(display);*/
  
  localNavigation_trackrobot=TRUE;
  fl_set_button(fd_localNavigationgui->track_robot,PUSHED);

	fl_set_slider_bounds(fd_localNavigationgui->escala,RANGO_MAX,RANGO_MIN);
	fl_set_slider_filter(fd_localNavigationgui->escala,localNavigation_range); /* Para poner el valor del slider en metros en pantalla */
  fl_set_slider_value(fd_localNavigationgui->escala,RANGO_INICIAL);
  localNavigation_escala = localNavigation_width/localNavigation_rango;
  
	fl_set_positioner_xvalue(fd_localNavigationgui->center,0.5);
	fl_set_positioner_yvalue(fd_localNavigationgui->center,0.5);

  myregister_buttonscallback(localNavigation_guibuttons);
  myregister_displaycallback(localNavigation_guidisplay);
}

void localNavigation_show(){
   static callback fn=NULL;
   if (fn==NULL){
      if ((fn=(callback)myimport ("graphics_xforms", "resume_callback"))!=NULL){
         fn ((gui_function)localNavigation_guiresume_aux);
      }
   }
   else{
      fn ((gui_function)localNavigation_guiresume_aux);
   }
}
