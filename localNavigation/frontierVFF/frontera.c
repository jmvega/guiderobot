/*
 *  Copyright (C) 2008 José María Cañas Plaza 
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Authors : Julio Vega Pérez (julio [dot] vega [at] urjc [dot] es)
 *  Authors : José María Cañas (jmplaza [at] gsyc [dot] es
 *  Authors : Darío Rodríguez de Diego (drd [dot] sqki [at] gmail [dot] com)
 *  Authors : Víctor Hidalgo 
 *
 *  This schema was programed for Guiderobot Project http://jde.gsyc.es/index.php/guiderobot
 *
 */

#include "frontera.h"

int frontera_brothers[MAX_SCHEMAS];
arbitration frontera_callforarbitration;

int DEBUG = 1;

// Control estático de los movimientos de traslación/rotación del robot
int flash = FALSE;
int traslacion = 0, rotacion = 0;
int flashImage = TRUE;

// Variables para guardar las matrices K, R y T de nuestra cámara:
gsl_matrix *K_1,*R_1;
gsl_vector* x_1;
HPoint2D progtest2D;
HPoint3D progtest3D;

int defineObjetivo = FALSE;

struct HSV* myHSV;
TPinHoleCamera virtualcam0; /* para ver el minimundillo */
TPinHoleCamera virtualcam1; /* para moverme por el mundillo */
TPinHoleCamera virtualcam2; /* para ver el entorno circundante al robot */
TPinHoleCamera roboticLabCam0; // cámaras del techo del laboratorio
TPinHoleCamera roboticLabCam1; //
TPinHoleCamera roboticLabCam2; //
TPinHoleCamera roboticLabCam3; //
TPinHoleCamera ceilLabCam;
TPinHoleCamera robotCamera;
TPinHoleCamera robotCamera2;
TPinHoleCamera *myCamera; // puntero temporal

static SofReference mypioneer;

int actualCameraView; // identificador de la cámara de la escena actual

registerdisplay myregister_displaycallback;
deletedisplay mydelete_displaycallback;

char *configFile;

HPoint2D myActualPoint2D; // variables para el cálculo de los rayos virtuales
HPoint3D myActualPoint3D;
HPoint2D myActualPointImage, miPrimerPunto, miSegundoPunto, miTercerPunto, miCuartoPunto;
HPoint3D cameraPos3D;
HPoint3D myEndPoint3D;
HPoint3D intersectionPoint;

HPoint3D groundPoints[SIFNTSC_COLUMNS*SIFNTSC_ROWS*4], fronteraPoints[SIFNTSC_COLUMNS], primerPunto3D, segundoPunto3D, tercerPunto3D, cuartoPunto3D;
int actualGroundPoint = 0, actualFronteraPoint = 0; // contador para el punto actual del suelo...
int primerPunto = FALSE, segundoPunto = FALSE, tercerPunto = FALSE, cuartoPunto = FALSE;
float distanciaPlanoImagen, incremento;

HPoint2D mouse_on_minicanvas, mouse_on_fronteracanvas;

/*GTK variables*/
GladeXML *xml=NULL; /*Fichero xml*/
GtkWidget *win;
GtkWidget *fronteraCanvas, *floorCanvas;
GtkWidget *miniCanvas;

GtkImage *originalImage; // Widgets de las imágenes que se obtienen del Window1 (GTK)
GtkImage *filteredImage;
GtkImage *borderImage;

struct image_struct *imageA = NULL; // Imágenes en crudo
struct image_struct *imageAfiltered = NULL;
struct image_struct *imageAbordered = NULL;

struct image_struct *originalImageRGB = NULL; // Imágenes preparadas para ser pintadas (cambio de bits)
struct image_struct *filteredImageRGB = NULL;
struct image_struct *borderImageRGB = NULL;

/*Some control variable*/
int loadedgui=0;

static int vmode;
static XImage *imagenA, *imagenFiltrada, *imagenSuelo;
static char *imagenA_buf, *imagenFiltrada_buf, *imagenSuelo_buf; /* puntero a memoria para la imagen a visualizar en el servidor X. No compartida con el servidor */
char *image;
char *contourImage;
static long int tabla[256]; 
/* tabla con la traduccion de niveles de gris a numero de pixel en Pseudocolor-8bpp. Cada numero de pixel apunta al valor adecuado del ColorMap, con el color adecuado instalado */
static int pixel8bpp_rojo, pixel8bpp_blanco, pixel8bpp_amarillo;

int floorView = FALSE, worldView = TRUE, whatWorld = 0;

int frontera_id=0; 
int frontera_brothers[MAX_SCHEMAS];
arbitration frontera_callforarbitration;
int frontera_cycle=80; /* ms */

char **myActualCamera;

int *mycolorAwidth		= NULL;
int *mycolorAheight		= NULL;
char **mycolorA;
float *myencoders=NULL;
float *myv=NULL;
float *myw=NULL;
runFn colorArun, encodersrun, motorsrun;
stopFn colorAstop, encodersstop, motorsstop;

int botonPulsado;

float old_x=0., old_y=0.;
float longi_foa = 0.0;
float lati_foa = 0.0;
float foax = 0.0;
float foay = 0.0;
float foaz = 0.0;
float xcam = -1.0;
float ycam = -1.0;
float zcam = -1.0;
float lati = 0.0;
float longi = 0.0;
int foa_mode, cam_mode;
float radius = 500;
float radius_old;
int centrado = 0;
float t = 0.5; // lambda
float phi = 0., theta = 0.;
int boton_pulsado;

float threshold = 50.; // umbral del filtro de CANNY

int	counterColumnsRight;
int	counterColumnsLeft;
int	counterColumnsCenter;

int seCumpleColorObjetivo = 0;
int seCumpleColorRotonda = 0;
/************************************************************************************************************/
/**************** V A R I A B L E S   D E L   M O V I M I E N T O   D E L   P I O N E E R *******************/
/************************************************************************************************************/
Tvoxel memoria[SIFNTSC_COLUMNS];
int validos[SIFNTSC_COLUMNS];
TdatosMemoria datos;
TdatosMemoria *pdatos;
TdatosResultante resultante;
TdatosResultante *result;
short distanciaObjetivo;
float xRep,yRep,xAtr,yAtr,xRes,yRes;
float difAngle;
float vActual,wActual;
float peligro;
int olvido = TRUE;
Tvoxel vfftarget, oldvfftarget, myAttachForce, myRepForce, myResForce;
Tvoxel miObjetivo, fuerzaResultante, repulsiva, atractiva, robott;
Tvoxel localNavigationtarget,oldlocalNavigationtarget;
int contadorMemoria;
int num_puntos;

Tvoxel target;/*Target imported*/
int golocalNavigation=1;/*Vale 1 cuando el esquema de navegacion esta funcionando*/ /*MEJOR CON SEMAFOROS??????*/

void printCameraInformation (TPinHoleCamera* actualCamera) {
	printf ("CAMERA INFORMATION\n");
	printf ("==================\n");
	printf ("Position = [%f, %f, %f]\n", actualCamera->position.X, actualCamera->position.Y, actualCamera->position.Z);
	printf ("K matrix:\n");
	printf ("%f %f %f %f\n", actualCamera->k11, actualCamera->k12, actualCamera->k13, actualCamera->k14);
	printf ("%f %f %f %f\n", actualCamera->k21, actualCamera->k22, actualCamera->k23, actualCamera->k24);
	printf ("%f %f %f %f\n", actualCamera->k31, actualCamera->k32, actualCamera->k33, actualCamera->k34);
	printf ("RT matrix:\n");
	printf ("%f %f %f %f\n", actualCamera->rt11, actualCamera->rt12, actualCamera->rt13, actualCamera->rt14);
	printf ("%f %f %f %f\n", actualCamera->rt21, actualCamera->rt22, actualCamera->rt23, actualCamera->rt24);
	printf ("%f %f %f %f\n", actualCamera->rt31, actualCamera->rt32, actualCamera->rt33, actualCamera->rt34);
	printf ("%f %f %f %f\n", actualCamera->rt41, actualCamera->rt42, actualCamera->rt43, actualCamera->rt44);
}

/*CALLBACKS*/
void floorButton_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myViewButton;

	myViewButton = (GtkToggleButton *)glade_xml_get_widget(xml, "worldButton");
	gtk_toggle_button_set_active (myViewButton, FALSE);

  floorView = TRUE;
	worldView = FALSE;
}

void floorButton_released (GtkButton *button, gpointer user_data) {}

void worldButton_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myViewButton;

	myViewButton = (GtkToggleButton *)glade_xml_get_widget(xml, "floorButton");
	gtk_toggle_button_set_active (myViewButton, FALSE);

  worldView = TRUE;
  floorView = FALSE;
}

void worldButton_released (GtkButton *button, gpointer user_data) {}

void go1Button_pressed (GtkButton *button, gpointer user_data) {
	traslacion += 1000;
}

void go1Button_released (GtkButton *button, gpointer user_data) {}

void go50Button_pressed (GtkButton *button, gpointer user_data) {
	traslacion += 500;
}

void go50Button_released (GtkButton *button, gpointer user_data) {}

void turn45RButton_pressed (GtkButton *button, gpointer user_data) {
	rotacion += 45;
}

void turn45RButton_released (GtkButton *button, gpointer user_data) {}

void turn45LButton_pressed (GtkButton *button, gpointer user_data) {
	rotacion -= 45;
}

void turn45LButton_released (GtkButton *button, gpointer user_data) {}

void turn90RButton_pressed (GtkButton *button, gpointer user_data) {
	rotacion += 90;
}

void turn90RButton_released (GtkButton *button, gpointer user_data) {}

void turn90LButton_pressed (GtkButton *button, gpointer user_data) {
	rotacion -= 90;
}

void turn90LButton_released (GtkButton *button, gpointer user_data) {}

void flashButton_pressed (GtkButton *button, gpointer user_data) {
	flash = TRUE;
}

void flashButton_released (GtkButton *button, gpointer user_data) {}

void camera1Button_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera2Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera3Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera4Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "ceilCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);

  actualCameraView = 1;
}

void camera1Button_released (GtkButton *button, gpointer user_data) {}

void camera2Button_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera1Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera3Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera4Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "ceilCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);

  actualCameraView = 2;
}

void camera2Button_released (GtkButton *button, gpointer user_data) {}

void camera3Button_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera2Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera1Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera4Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "ceilCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);

  actualCameraView = 3;
}

void camera3Button_released (GtkButton *button, gpointer user_data) {}

void camera4Button_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera2Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera3Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera1Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "ceilCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);

  actualCameraView = 4;
}

void camera4Button_released (GtkButton *button, gpointer user_data) {}

void ceilCameraButton_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera2Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera3Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera4Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera1Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);

  actualCameraView = 5;
}

void ceilCameraButton_released (GtkButton *button, gpointer user_data) {}

void userCameraButton_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera2Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera3Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera4Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera1Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "ceilCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);

  actualCameraView = 0;
}

void userCameraButton_released (GtkButton *button, gpointer user_data) {}
/*
static gboolean motion_notify_event (GtkWidget *widget, GdkEventButton *event, gpointer data) {

	printf ("entramos2\n");

   float x=event->x;
   float y=event->y;
  
	event = (GdkEventButton*)event;

	if (2 == botonPulsado) {
		flashImage = TRUE;
	  return TRUE;
	}	

	mouse_on_fronteracanvas.x = ((event->x*360/fronteraCanvas->allocation.width)-180);
	mouse_on_fronteracanvas.y = ((event->y*-180/fronteraCanvas->allocation.height)+90);

 	if (GDK_BUTTON1_MASK){ // Si está pulsado el botón 1
		gtk_widget_queue_draw (widget);
		old_x=x;
		old_y=y;
	}
 return TRUE;
}

/// Controlador de la camara de opengl
void opengl_camara_controller_motion(	GtkWidget	*widget,
					GdkEventMotion	*event,
					gpointer	user_data) {

	printf ("entramos\n");
	float desp = 0.01;
	float x=event->x;
	float y=event->y;
	
	if (event->state & GDK_BUTTON1_MASK) { // Si está pulsado el botón 1
		if ((x - old_x) > 0.0)		longi -= desp;
		else if ((x - old_x) < 0.0)	longi += desp;

		if ((y - old_y) > 0.0)		lati += desp;
		else if ((y - old_y) < 0.0)	lati -= desp;

		xcam=radius*cosf(lati)*cosf(longi) + foax;
		ycam=radius*cosf(lati)*sinf(longi) + foay;
		zcam=radius*sinf(lati) + foaz;
	}

	if (event->state & GDK_BUTTON3_MASK) {
		if ((x - old_x) > 0.0)		longi -= desp;
		else if ((x - old_x) < 0.0)	longi += desp;

		if ((y - old_y) > 0.0)		lati += desp;
		else if ((y - old_y) < 0.0)	lati -= desp;

		foax=radius*cosf(lati_foa)*cosf(lati_foa) + xcam;
		foay=radius*cosf(lati_foa)*sinf(lati_foa) + ycam;
		foaz=radius*sinf(lati_foa) + zcam;
	}

	old_x=x;
	old_y=y;
}

void opengl_camara_controller_zoom(	GtkWidget	*widget,
					GdkEventScroll	*event,
					gpointer	user_data) {
	float vx, vy, vz;

	vx = (foax - xcam)/radius;
	vy = (foay - ycam)/radius;
	vz = (foaz - zcam)/radius;

	if (event->direction == GDK_SCROLL_UP){
		foax = foax + vx;
		foay = foay + vy;
		foaz = foaz + vz;

		xcam = xcam + vx;
		ycam = ycam + vy;
		zcam = zcam + vz;
	}

	if (event->direction == GDK_SCROLL_DOWN){
		foax = foax - vx;
		foay = foay - vy;
		foaz = foaz - vz;

		xcam = xcam - vx;
		ycam = ycam - vy;
		zcam = zcam - vz;
	}
}
*/
/*
static void realize (GtkWidget *widget, gpointer data) {
	pthread_mutex_lock(&gl_mutex);
	{
	GdkGLContext *glcontext = gtk_widget_get_gl_context (widget);
	GdkGLDrawable *gldrawable = gtk_widget_get_gl_drawable (widget);

	if (!gdk_gl_drawable_gl_begin (gldrawable, glcontext)){
	pthread_mutex_unlock(&gl_mutex);
	return;
	}

	gdk_gl_drawable_gl_end (gldrawable);

	}
	pthread_mutex_unlock(&gl_mutex);
}*/

static gboolean button_press_event (GtkWidget *widget, GdkEventButton *event, gpointer data) {
  float boton_x;
  float boton_y;
  int back;

  event = (GdkEventButton*)event;
  old_x=event->x;
  old_y=event->y;

  if (2 == event->button){
  	boton_pulsado=2;
  	flashImage = TRUE;
 	  return TRUE;
  } else {
  	flashImage = FALSE;
  	if (1 == event->button){
  	  boton_pulsado=1;
  	  cam_mode = 1;
   	  foa_mode = 0;
	  } else if (3 == event->button) {
	    boton_pulsado=3;
	    foa_mode = 1;
	    cam_mode = 0;
	  }
		return TRUE;
  }
}

static gboolean motion_notify_event (GtkWidget *widget, GdkEventButton *event, gpointer data) {
   float x=event->x;
   float y=event->y;
  
	event = (GdkEventButton*)event;

	if (2==boton_pulsado) {
		flashImage = TRUE;
	  return TRUE;
	}	

	mouse_on_fronteracanvas.x = ((event->x*360/fronteraCanvas->allocation.width)-180);
	mouse_on_fronteracanvas.y = ((event->y*-180/fronteraCanvas->allocation.height)+90);
	//printf ("Valores de mouse_on_canvas: %f y %f \n",mouse_on_fronteracanvas.x,mouse_on_fronteracanvas.y);
 
	if (GDK_BUTTON1_MASK){ /*Si está pulsado el botón 1*/
      theta -= x - old_x;
      phi -= y - old_y;
      gtk_widget_queue_draw (widget);
      old_x=x;
      old_y=y;
   }
 return TRUE;
}

static gboolean scroll_event (GtkRange *range, GdkEventScroll *event, gpointer data){
   if (event->direction == GDK_SCROLL_DOWN){
	if (radius > MIN_RADIUS_VALUE)
   	radius-=WHEEL_DELTA;
   }
   if (event->direction == GDK_SCROLL_UP){
     if (radius<MAX_RADIUS_VALUE){
      radius+=WHEEL_DELTA;
       }
   }
   if (radius < 0.5) radius = 0.5;
   gtk_widget_queue_draw(GTK_WIDGET((GtkWidget *)data));
   
   return TRUE;
}

/*Callback of window closed*/
void on_delete_window (GtkWidget *widget,GdkEvent *event,gpointer user_data)
{
   gdk_threads_leave();
   frontera_hide();
   gdk_threads_enter();
}

struct image_struct *create_image(int width, int height, int bpp) {
	struct image_struct *w;

	w = (struct image_struct*) malloc(sizeof(struct image_struct));
	w->width = width;
	w->height = height;
	w->bpp = bpp;
	w->image = (char*) malloc(width * height * bpp);

	return w;
}

void remove_image(struct image_struct *w) {
	free(w->image);
	free(w);
}

/** prepare2draw ************************************************************************
* Prepare an image to draw it with a GtkImage in BGR format with 3 pixels per byte.	*
*	src: source image								*
*	dest: destiny image. It has to have the same size as the source image and	*
*	      3 bytes per pixel.
*****************************************************************************************/
void prepare2draw (struct image_struct *src, struct image_struct *dest) {
	int i;

	for (i=0; i<src->width*src->height; i++) {
		dest->image[i*dest->bpp+0] = src->image[i*src->bpp+2];
		dest->image[i*dest->bpp+1] = src->image[i*src->bpp+1];
		dest->image[i*dest->bpp+2] = src->image[i*src->bpp+0];
	}
}

unsigned long int dameTiempo() {
	struct timeval t; 

	gettimeofday(&t,NULL);
	return t.tv_sec*1000000 + t.tv_usec;	
}

float transformoAngulo(float angulo) {
	/* Transformamos un angulo dado en radianes en grados de tal manera k si el
	angulo introducido es 180>thetha>0 se kda tal cual y si es 180<theta<0 kda
	como -thetha*/ 
	float salida, auxi, auxi1;

	angulo=angulo*RADTODEG; /* PASAMOS la entrada A GRADOS */
	auxi=angulo*100; /* Nos quedamos con 2 decimales */
	auxi1=ceil(auxi);
	angulo=(auxi1/100);

	if ((angulo>180)&&(angulo<=360)) {
		salida=angulo-(2*M_PI*RADTODEG);
		if (salida>=-0.1) salida=0.00;
	}	else salida=angulo;

	if (salida>=360) salida=0.00;

	auxi=salida*100;
	auxi1=ceil(auxi);
	salida=(auxi1/100);  /* Esta conversion la hacemos para quedarnos con 2 decimales */

	return salida;
}

int calculoFuerza (int distancia, int ang) {
  int fuerza;

  if (distancia!=0)
		if (distancia < alcancePeligroso)
			fuerza = K2/distancia; /* generamos MAS fuerza repulsiva; hay cierto peligro */
		else
			fuerza = K/distancia; /* generamos MENOS fuerza repulsiva; no se considera peligro */

  else fuerza=0;

  return fuerza;
}

int mismoAnguloyDistancia(Tvoxel a,int distanciaPuntoMem,int anguloPuntoNuevo,int distanciaNueva) {
	int angulo_aux;
	int salida=FALSE;
	float margen = 500;
	int dist = FALSE;

	dist = distanciaPuntoMem < (distanciaNueva-margen);
	angulo_aux = (int)(atan2(a.y-myencoders[1],a.x-myencoders[0])*RADTODEG);
	salida = ((anguloPuntoNuevo <= (angulo_aux + 0.7)) && (anguloPuntoNuevo >= (angulo_aux - 0.7)));

	return salida && dist;
}

int reemplazoSimilares(Tvoxel a,Tvoxel b,int distanciaGuardado,int distanciaNuevo) {
	int radio;	
	radio = 10;

	return (((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y))<(radio*radio));
}

void initCalibration () {

  K_1 = gsl_matrix_calloc(3,3);
  R_1 = gsl_matrix_calloc(3,3);
  x_1 = gsl_vector_alloc(3);

	// Fill K_1 matrix, with information got on Calibrator GUI
	gsl_matrix_set(K_1,0,0,303.0);
  gsl_matrix_set(K_1,0,1,5.38);
  gsl_matrix_set(K_1,0,2,179.09);

	gsl_matrix_set(K_1,1,0,0.0);
  gsl_matrix_set(K_1,1,1,296.01);
  gsl_matrix_set(K_1,1,2,89.94);

	gsl_matrix_set(K_1,2,0,0.);
  gsl_matrix_set(K_1,2,1,0.);
  gsl_matrix_set(K_1,2,2,1.);

	// Fill R_1 matrix, with information got on Calibrator GUI
	gsl_matrix_set(R_1,0,0,0.69);
  gsl_matrix_set(R_1,0,1,-0.72);
  gsl_matrix_set(R_1,0,2,-0.11);

	gsl_matrix_set(R_1,1,0,-0.35);
  gsl_matrix_set(R_1,1,1,-0.46);
  gsl_matrix_set(R_1,1,2,0.82);

	gsl_matrix_set(R_1,2,0,0.63);
  gsl_matrix_set(R_1,2,1,0.53);
  gsl_matrix_set(R_1,2,2,0.57);

	int i,j;
	for(i=0;i<3;i++)
		for(j=0;j<3;j++)
		  printf("R (%d,%d)=%f\n",i,j,gsl_matrix_get(R_1,i,j));

	// Fill x_1 vector, with information got on Calibrator GUI
  gsl_vector_set(x_1, 0, -208.6);
  gsl_vector_set(x_1, 1, -238.0);
  gsl_vector_set(x_1, 2, -261.5);
}

double rad2deg(double alfa){
  return (alfa*180)/3.14159264;
}

void drawRobotCamera(double ppx, double ppy, int camera){
  // Estos incrementos se usan para obtener el siguiente punto
  float inc_x = (1./SIFNTSC_COLUMNS)*100;
  float inc_y = (1./SIFNTSC_ROWS)*100;
  
  int i,j,offset,red,green,blue;

	// dibujamos el rectangulo donde se proyectará la imagen
  glColor3f( 0.0, 0.0, 0.0 );  
  glScalef(0.2,0.2,0.2);
  
  glBegin(GL_LINES);
  v3f( -50, 50, -100 );   
  v3f( 50, 50, -100 );   
  glEnd();
  
  glBegin(GL_LINES);
  v3f( -50, 50, -100 );   
  v3f( -50, -50, -100 );   
  glEnd();

  glBegin(GL_LINES);
  v3f( 50, 50, -100 );   
  v3f( 50, -50, -100 );   
  glEnd();

  glBegin(GL_LINES);
  v3f( 50, -50, -100 );   
  v3f( -50, -50, -100 );   
  glEnd();

  // pintamos la imagen
  if (mycolorA!=NULL)
    for (j=0;j<SIFNTSC_ROWS;j++)
      for(i=0;i<SIFNTSC_COLUMNS;i++) {
			  // obtenemos el color del pixel de la imagen de entrada
				offset = j*320+i;
				if (mycolorA!=NULL) {
				  red = (*mycolorA)[offset*3+2];
				  green = (*mycolorA)[offset*3+1];
				  blue = (*mycolorA)[offset*3+0];
				}
				
				glColor3f( (float)red/255.0, (float)green/255.0, (float)blue/255.0 );  
				
				// pintamos el punto correspondiente en el escenario pintado con OGL
				glBegin(GL_POINTS);
				v3f(-50.+(inc_x*i),50.-(inc_y*j),-100. );
				glEnd();
			}
  
  // fin del dibujo del rectangulo y su imagen interior. Ahora dibujamos los triángulos.
  glColor3f( 1.0, 0.0, 0.0 );  
  glBegin(GL_LINES);
  v3f( 0.0, 0.0, 0.0 );   
  v3f( -50, 50, -100 );   
  glEnd();

  glColor3f( 0.0, 1.0, 0.0 );  
  glBegin(GL_LINES);
  v3f( 0.0, 0.0, 0.0 );   
  v3f( 50, 50, -100 );   
  glEnd();

  glColor3f( 0.0, 0.0, 1.0 );  
  glBegin(GL_LINES);
  v3f( 0.0, 0.0, 0.0 );   
  v3f( 50, -50, -100 );   
  glEnd();
  
  glColor3f( 1.0, 1.0, 0.0 );  
  glBegin(GL_LINES);
  v3f( 0.0, 0.0, 0.0 );   
  v3f( -50, -50, -100 );   
  glEnd();

  glBegin(GL_LINES);
  glColor3f( 0.0, 1.0, 0.0 );  
  v3f( 0.0, 0.0, 0.0 );   
  v3f( -50+ppx,-50+ppy, -100 );   
  glEnd();

  glLoadIdentity();
  glColor3f( 1.0, 0.0, 0.0 );
  glTranslatef( 
	 -(float)gsl_vector_get(x_1,0),
	 -(float)gsl_vector_get(x_1,1),
	 -(float)gsl_vector_get(x_1,2));
  
  glutSolidCube(0.01);
	glScalef(1,1,1); // reestablecemos la escala
}

void setRobotCameraPos () {

  HPoint2D p2;
  HPoint3D p3;
  int i,j=0;
  gsl_matrix *RT,*T,*Res;
  
  RT = gsl_matrix_calloc(4,4);
  T = gsl_matrix_calloc(3,4);
  Res = gsl_matrix_calloc(3,4);
  
  gsl_matrix_set(T,0,0,1);
  gsl_matrix_set(T,1,1,1);
  gsl_matrix_set(T,2,2,1);

  gsl_matrix_set(T,0,3,gsl_vector_get(x_1,0));
  gsl_matrix_set(T,1,3,gsl_vector_get(x_1,1));
  gsl_matrix_set(T,2,3,gsl_vector_get(x_1,2));

	for(i=0;i<3;i++)
			for(j=0;j<3;j++)
			  printf("R_1 (%d,%d)=%f\n",i,j,gsl_matrix_get(R_1,i,j));

		for(i=0;i<3;i++)
			for(j=0;j<4;j++)
			  printf("T (%d,%d)=%f\n",i,j,gsl_matrix_get(T,i,j));

  gsl_linalg_matmult (R_1,T,Res);  
for(i=0;i<3;i++)
			for(j=0;j<4;j++)
			  printf("Res (%d,%d)=%f\n",i,j,gsl_matrix_get(Res,i,j));

  for (i=0;i<3;i++)
    for (j=0;j<4;j++)
      gsl_matrix_set(RT,i,j,gsl_matrix_get(Res,i,j));
  
  /** set 0001 in the last row of RT */
  gsl_matrix_set(RT,3,0,0);
  gsl_matrix_set(RT,3,1,0);
  gsl_matrix_set(RT,3,2,0);
  gsl_matrix_set(RT,3,3,1);

  /** Set camera position*/
  robotCamera.position.X = gsl_vector_get(x_1,0);
  robotCamera.position.Y = gsl_vector_get(x_1,1);
  robotCamera.position.Z = gsl_vector_get(x_1,2);

/*	robotCamera.fdistx=405.399994;
	robotCamera.fdisty=robotCamera.fdistx;
	robotCamera.skew=0.;*/
      
  /** Setting intrinsic matrix*/
  robotCamera.k11 = gsl_matrix_get(K_1,0,0);
  robotCamera.k12 = gsl_matrix_get(K_1,0,1);
  robotCamera.k13 = gsl_matrix_get(K_1,0,2);
  robotCamera.k14 = 0;

  robotCamera.k21 = gsl_matrix_get(K_1,1,0);
  robotCamera.k22 = gsl_matrix_get(K_1,1,1);
  robotCamera.k23 = gsl_matrix_get(K_1,1,2);
  robotCamera.k24 = 0;

  robotCamera.k31 = gsl_matrix_get(K_1,2,0);
  robotCamera.k32 = gsl_matrix_get(K_1,2,1);
  robotCamera.k33 = gsl_matrix_get(K_1,2,2);
  robotCamera.k34 = 0;

  /** Setting extrinsic*/
  robotCamera.rt11 = gsl_matrix_get(RT,0,0);
  robotCamera.rt12 = gsl_matrix_get(RT,0,1);
  robotCamera.rt13 = gsl_matrix_get(RT,0,2);
  robotCamera.rt14 = gsl_matrix_get(RT,0,3);
  
  robotCamera.rt21 = gsl_matrix_get(RT,1,0);
  robotCamera.rt22 = gsl_matrix_get(RT,1,1);
  robotCamera.rt23 = gsl_matrix_get(RT,1,2);
  robotCamera.rt24 = gsl_matrix_get(RT,1,3);

  robotCamera.rt31 = gsl_matrix_get(RT,2,0);
  robotCamera.rt32 = gsl_matrix_get(RT,2,1);
  robotCamera.rt33 = gsl_matrix_get(RT,2,2);
  robotCamera.rt34 = gsl_matrix_get(RT,2,3);

  robotCamera.rt41 = gsl_matrix_get(RT,3,0);
  robotCamera.rt42 = gsl_matrix_get(RT,3,1);
  robotCamera.rt43 = gsl_matrix_get(RT,3,2);
  robotCamera.rt44 = gsl_matrix_get(RT,3,3);

  p3.X = 100;
  p3.Y = 100;
  p3.Z = 100;
  p3.H = 1;

  progtest2D.x = 0;
  progtest2D.y = 0;
  progtest2D.h = 1;

	printCameraInformation (&robotCamera);

  project(p3,&p2,robotCamera);
  
  //if (DEBUG)
  printf("El punto project en %f:%f:%f\n",p2.x,p2.y,p2.h);

  if (backproject(&progtest3D, progtest2D, robotCamera)){
    
    progtest3D.X =  progtest3D.X/progtest3D.H;
    progtest3D.Y =  progtest3D.Y/progtest3D.H;
    progtest3D.Z =  progtest3D.Z/progtest3D.H;

  //if (DEBUG)
    printf("el backproject de %.2f-%.2f es %.2f-%.2f-%.2f-%.2f \n\n",
	   progtest2D.x,progtest2D.y,
	   progtest3D.X, progtest3D.Y, progtest3D.Z, progtest3D.H
	   );
  }
}

void drawFloorInfiniteLines () {
	int i;

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glColor3f( 0.3, 0.3, 0.3 );
  glBegin(GL_LINES);
  for(i=0;i<((int)MAXWORLD+1);i++)
    {
      v3f(-(int)MAXWORLD*1000/2.+(float)i*1000,-(int)MAXWORLD*1000/2.,0.);
      v3f(-(int)MAXWORLD*1000/2.+(float)i*1000,(int)MAXWORLD*1000/2.,0.);
      v3f(-(int)MAXWORLD*1000/2.,-(int)MAXWORLD*1000/2.+(float)i*1000,0.);
      v3f((int)MAXWORLD*1000/2.,-(int)MAXWORLD*1000/2.+(float)i*1000,0.);
    }
  glEnd();
}


void drawMyAxes () {
  glLineWidth(2.0f);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  // Dibujamos el arco de visión de la cámara
	glColor3f (1.,1.,1.);
  glBegin(GL_LINES);
  v3f(-robotCamera.position.X, -robotCamera.position.Y, 0.000000);
  v3f(segundoPunto3D.X, segundoPunto3D.Y, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(-robotCamera.position.X, -robotCamera.position.Y, 0.000000);
  v3f(primerPunto3D.X, primerPunto3D.Y, 0.000000);
  glEnd();

	// Dibujamos 
  glBegin(GL_LINES);
  v3f(primerPunto3D.X, primerPunto3D.Y, 0.000000);
  v3f(segundoPunto3D.X, segundoPunto3D.Y, 0.000000);
	glEnd();

  glBegin(GL_LINES);
  v3f(segundoPunto3D.X, segundoPunto3D.Y, 0.000000);
  v3f(tercerPunto3D.X, tercerPunto3D.Y, 0.000000);
	glEnd();

  glBegin(GL_LINES);
  v3f(tercerPunto3D.X, tercerPunto3D.Y, 0.000000);
  v3f(cuartoPunto3D.X, cuartoPunto3D.Y, 0.000000);
	glEnd();

  glBegin(GL_LINES);
  v3f(cuartoPunto3D.X, cuartoPunto3D.Y, 0.000000);
  v3f(primerPunto3D.X, primerPunto3D.Y, 0.000000);
	glEnd();
}

void drawRoboticLabPlanes () {
	/* LABORATORIO DE ROBÓTICA */
  glLineWidth(2.0f);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

	/* SUELO */
  glColor3f(0.93, 0.93, 0.455);
	// Parte central:
  glBegin(GL_QUADS);
	  v3f(0.000000, 0.000000, 0.000000);
	  v3f(0.000000, 4589.000000, 0.000000);
	  v3f(7925.000000, 4589.000000, 0.000000);
	  v3f(7925.000000, 0.000000, 0.000000);
  glEnd();

	// Lateral de la puerta (1ª parte):
  glBegin(GL_QUADS);
	  v3f(1211.000000, -554.000000, 0.000000);
	  v3f(1211.000000, 5.000000, 0.000000);
	  v3f(5476.000000, 5.000000, 0.000000);
	  v3f(5476.000000, -554.000000, 0.000000);
  glEnd();

	// Lateral de la puerta (trocito intermedio de sobrante defectuoso):
  glBegin(GL_QUADS);
	  v3f(5470.000000, -120.000000, 0.000000);
	  v3f(5470.000000, 10.000000, 0.000000);
	  v3f(6115.000000, 10.000000, 0.000000);
	  v3f(6115.000000, -120.000000, 0.000000);
  glEnd();

	// Lateral de la puerta (2ª parte):
  glBegin(GL_QUADS);
	  v3f(6110.000000, -554.000000, 0.000000);
	  v3f(6110.000000, 5.000000, 0.000000);
	  v3f(7925.000000, 5.000000, 0.000000);
	  v3f(7925.000000, -554.000000, 0.000000);
  glEnd();

	// Lateral enfrente de la puerta (1ª parte):
  glBegin(GL_QUADS);
	  v3f(2419.000000, 4580.000000, 0.000000);
	  v3f(2419.000000, 4879.000000, 0.000000);
	  v3f(5498.000000, 4879.000000, 0.000000);
	  v3f(5498.000000, 4580.000000, 0.000000);
  glEnd();

	// Lateral enfrente de la puerta (2ª parte):
  glBegin(GL_QUADS);
	  v3f(6264.000000, 4580.000000, 0.000000);
	  v3f(6264.000000, 4879.000000, 0.000000);
	  v3f(7925.000000, 4879.000000, 0.000000);
	  v3f(7925.000000, 4580.000000, 0.000000);
  glEnd();

	/* CAMPO DE FUTBOL */
  glColor3f(0., 0.59, 0.);
  glBegin(GL_QUADS); // daremos algo de altura para no causar "conflictos graficos" con el suelo
	  v3f(1560.000000, 596.000000, 25.000000);
	  v3f(1560.000000, 4589.000000, 25.000000);
	  v3f(7925.000000, 4589.000000, 25.000000);
	  v3f(7925.000000, 596.000000, 25.000000);
  glEnd();

	/* PAREDES */

	// Lateral izquierdo:
  glColor3f(0.98, 0.98, 0.98);
  glBegin(GL_QUADS);
	  v3f(0.000000, 0.000000, 3000.000000);
	  v3f(0.000000, 4589.000000, 3000.000000);
	  v3f(0.000000, 4589.000000, 0.000000);
	  v3f(0.000000, 0.000000, 0.000000);
	glEnd();

	// Lateral enfrente de la puerta:
  glBegin(GL_QUADS); // 1ª parte
	  v3f(0.000000, 4589.000000, 0.000000);
	  v3f(0.000000, 4589.000000, 3000.000000);
	  v3f(2419.000000, 4589.000000, 3000.000000);
	  v3f(2419.000000, 4589.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 2ª parte
	  v3f(2419.000000, 4589.000000, 0.000000);
	  v3f(2419.000000, 4589.000000, 3000.000000);
	  v3f(2419.000000, 4879.000000, 3000.000000);
	  v3f(2419.000000, 4879.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 3ª parte
	  v3f(2419.000000, 4879.000000, 0.000000);
	  v3f(2419.000000, 4879.000000, 3000.000000);
	  v3f(5476.000000, 4879.000000, 3000.000000);
	  v3f(5476.000000, 4879.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 4ª parte
	  v3f(5476.000000, 4879.000000, 0.000000);
	  v3f(5476.000000, 4879.000000, 3000.000000);
	  v3f(5476.000000, 4589.000000, 3000.000000);
	  v3f(5476.000000, 4589.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 5ª parte
	  v3f(5476.000000, 4589.000000, 0.000000);
	  v3f(5476.000000, 4589.000000, 3000.000000);
	  v3f(6264.000000, 4589.000000, 3000.000000);
	  v3f(6264.000000, 4589.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 6ª parte
	  v3f(6264.000000, 4589.000000, 0.000000);
	  v3f(6264.000000, 4589.000000, 3000.000000);
	  v3f(6264.000000, 4879.000000, 3000.000000);
	  v3f(6264.000000, 4879.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 6ª parte
	  v3f(6264.000000, 4879.000000, 0.000000);
	  v3f(6264.000000, 4879.000000, 3000.000000);
	  v3f(7925.000000, 4879.000000, 3000.000000);
	  v3f(7925.000000, 4879.000000, 0.000000);
	glEnd();

	// Lateral a la derecha de la puerta:
  glBegin(GL_QUADS); // 6ª parte
	  v3f(7925.000000, 4879.000000, 0.000000);
	  v3f(7925.000000, 4879.000000, 3000.000000);
	  v3f(7925.000000, -554.000000, 3000.000000);
	  v3f(7925.000000, -554.000000, 0.000000);
	glEnd();

	// Lateral de la puerta:
  glBegin(GL_QUADS); // 1ª parte
	  v3f(7925.000000, -554.000000, 0.000000);
	  v3f(7925.000000, -554.000000, 3000.000000);
	  v3f(6110.000000, -554.000000, 3000.000000);
	  v3f(6110.000000, -554.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 2ª parte
	  v3f(6110.000000, -554.000000, 0.000000);
	  v3f(6110.000000, -554.000000, 3000.000000);
	  v3f(6110.000000, -119.000000, 3000.000000);
	  v3f(6110.000000, -119.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 3ª parte
	  v3f(6110.000000, -119.000000, 0.000000);
	  v3f(6110.000000, -119.000000, 3000.000000);
	  v3f(5476.000000, -119.000000, 3000.000000);
	  v3f(5476.000000, -119.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 4ª parte
	  v3f(5476.000000, -554.000000, 0.000000);
	  v3f(5476.000000, -554.000000, 3000.000000);
	  v3f(1211.000000, -554.000000, 3000.000000);
	  v3f(1211.000000, -554.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 5ª parte
	  v3f(1211.000000, -554.000000, 0.000000);
	  v3f(1211.000000, -554.000000, 3000.000000);
	  v3f(1211.000000, 0.000000, 3000.000000);
	  v3f(1211.000000, 0.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 6ª parte
	  v3f(1211.000000, 0.000000, 0.000000);
	  v3f(1211.000000, 0.000000, 3000.000000);
	  v3f(0.000000, 0.000000, 3000.000000);
	  v3f(0.000000, 0.000000, 0.000000);
	glEnd();

	/* PUERTA */
  glColor3f(0.67, 0., 0.);
  glBegin(GL_QUADS);
	  v3f(4613.000000, -554.000000, 0.000000);
	  v3f(4613.000000, -554.000000, 2030.000000);
	  v3f(3879.000000, -554.000000, 2030.000000);
	  v3f(3879.000000, -554.000000, 0.000000);
  glEnd();

	/* VENTANA */
  glColor3f(0.5, 0.5, 1.);
  glBegin(GL_QUADS);
	  v3f(2585.000000, 4879.000000, 835.000000);
	  v3f(2585.000000, 4879.000000, 2532.000000);
	  v3f(5335.000000, 4879.000000, 2532.000000);
	  v3f(5335.000000, 4879.000000, 835.000000);
  glEnd();
}

void drawHallLines () {
	/* el pasillo mide (15x1.99)+1.50 cm. de largo y 1.90 cm. de ancho */

	/* SUELO */
  glBegin(GL_LINES);
	  v3f(0.0, -554.0, 0.0);
	  v3f(31350.0, -554.0, 0.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(31350.0, -554.0, 0.0);
	  v3f(31350.0, -2454.0, 0.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(31350.0, -2454.0, 0.0);
	  v3f(0.0, -2454.0, 0.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(0.0, -2454.0, 0.0);
	  v3f(0.0, -554.0, 0.0);
  glEnd();

	/* TECHO DEL PASILLO */
  glBegin(GL_LINES);
	  v3f(0.0, -554.0, 3000.0);
	  v3f(31350.0, -554.0, 3000.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(31350.0, -554.0, 3000.0);
	  v3f(31350.0, -2454.0, 3000.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(31350.0, -2454.0, 3000.0);
	  v3f(0.0, -2454.0, 3000.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(0.0, -2454.0, 3000.0);
	  v3f(0.0, -554.0, 3000.0);
  glEnd();

	/* ESQUINAS */
  glBegin(GL_LINES);
	  v3f(0.0, -554.0, 0.0);
	  v3f(0.0, -554.0, 3000.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(0.0, -2454.0, 0.0);
	  v3f(0.0, -2454.0, 3000.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(31350.0, -554.0, 0.0);
	  v3f(31350.0, -554.0, 3000.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(31350.0, -2454.0, 0.0);
	  v3f(31350.0, -2454.0, 3000.0);
  glEnd();
}

void drawRoboticLabLines () {
  glColor3f(1., 1., 1.);
  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 3000.000000);
  v3f(0.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 3000.000000);
  v3f(1211.000000, 0.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, 0.000000, 3000.000000);
  v3f(1211.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, -554.000000, 3000.000000);
  v3f(5476.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -554.000000, 3000.000000);
  v3f(5476.000000, -119.000000, 3000.000000);
  glEnd();
	
  glBegin(GL_LINES);
  v3f(5476.000000, -119.000000, 3000.000000);
  v3f(6110.000000, -119.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -119.000000, 3000.000000);
  v3f(6110.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -554.000000, 3000.000000);
  v3f(7925.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, -554.000000, 3000.000000);
  v3f(7925.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, 4879.000000, 3000.000000);
  v3f(6264.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4879.000000, 3000.000000);
  v3f(6264.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 3000.000000);
  v3f(5498.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 3000.000000);
  v3f(5498.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4879.000000, 3000.000000);
  v3f(2419.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4879.000000, 3000.000000);
  v3f(2419.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4589.000000, 3000.000000);
  v3f(0.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 30.000000);
  v3f(0.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 30.000000);
  v3f(1211.000000, 0.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, 0.000000, 30.000000);
  v3f(1211.000000, -554.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, -554.000000, 30.000000);
  v3f(5476.000000, -554.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -554.000000, 30.000000);
  v3f(5476.000000, -119.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -119.000000, 30.000000);
  v3f(6110.000000, -119.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -119.000000, 30.000000);
  v3f(6110.000000, -554.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -554.000000, 30.000000);
  v3f(7925.000000, -554.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, -554.000000, 30.000000);
  v3f(7925.000000, 4879.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, 4879.000000, 30.000000);
  v3f(6264.000000, 4879.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4879.000000, 30.000000);
  v3f(6264.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 30.000000);
  v3f(5498.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 30.000000);
  v3f(5498.000000, 4879.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4879.000000, 30.000000);
  v3f(2419.000000, 4879.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4879.000000, 30.000000);
  v3f(2419.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4589.000000, 30.000000);
  v3f(0.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 0.000000);
  v3f(0.000000, 0.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, 0.000000, 0.000000);
  v3f(1211.000000, 0.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, -554.000000, 0.000000);
  v3f(1211.000000, -554.000000, 3000.000000);
  glEnd();

	// LINEAS LIMITROFES PUERTA-------------------
  glColor3f(1., 0.5, 0.);
  glBegin(GL_LINES);
  v3f(3879.000000, -554.000000, 0.000000);
  v3f(3879.000000, -554.000000, 2030.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(3879.000000, -554.000000, 2030.000000);
  v3f(4613.000000, -554.000000, 2030.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4613.000000, -554.000000, 2030.000000);
  v3f(4613.000000, -554.000000, 0.000000);
  glEnd();
	//--------------------------------------------

  glColor3f(1., 1., 1.);
  glBegin(GL_LINES);
  v3f(5476.000000, -554.000000, 0.000000);
  v3f(5476.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -119.000000, 0.000000);
  v3f(5476.000000, -119.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -119.000000, 0.000000);
  v3f(6110.000000, -119.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -554.000000, 0.000000);
  v3f(6110.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, -554.000000, 0.000000);
  v3f(7925.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, 4879.000000, 0.000000);
  v3f(7925.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4879.000000, 0.000000);
  v3f(6264.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 0.000000);
  v3f(6264.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 0.000000);
  v3f(5498.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4879.000000, 0.000000);
  v3f(5498.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4879.000000, 0.000000);
  v3f(2419.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4589.000000, 0.000000);
  v3f(2419.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 4589.000000, 0.000000);
  v3f(0.000000, 4589.000000, 3000.000000);
  glEnd();

	// LINEAS LIMITROFES DE LA VENTANA-----------
  glColor3f(1., 0.5, 0.);
  glBegin(GL_LINES);
  v3f(2585.000000, 4879.000000, 835.000000);
  v3f(5335.000000, 4879.000000, 835.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2585.000000, 4879.000000, 2532.000000);
  v3f(5335.000000, 4879.000000, 2532.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2585.000000, 4879.000000, 835.000000);
  v3f(2585.000000, 4879.000000, 2532.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5335.000000, 4879.000000, 835.000000);
  v3f(5335.000000, 4879.000000, 2532.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(3960.000000, 4879.000000, 835.000000);
  v3f(3960.000000, 4879.000000, 2532.000000);
  glEnd();
	//--------------------------------------------

  glColor3f(1., 1., 1.);
	// LINEAS LIMITROFES DEL CAMPO FUTBOL
  glBegin(GL_LINES);
  v3f(1560.000000, 596.000000, 30.000000);
  v3f(1560.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 30.000000);
  v3f(2419.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1560.000000, 596.000000, 30.000000);
  v3f(7925.000000, 596.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 30.000000);
  v3f(7925.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 796.000000, 30.000000);
  v3f(2160.000000, 4301.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 796.000000, 30.000000);
  v3f(7560.000000, 796.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 4301.000000, 30.000000);
  v3f(7560.000000, 4301.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 796.000000, 30.000000);
  v3f(7560.000000, 4301.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4860.000000, 796.000000, 30.000000);
  v3f(4860.000000, 4301.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 2051.000000, 30.000000);
  v3f(2810.000000, 2051.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2810.000000, 2051.000000, 30.000000);
  v3f(2810.000000, 3351.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2810.000000, 3351.000000, 30.000000);
  v3f(2160.000000, 3351.000000, 30.000000 );
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 2051.000000, 30.000000);
  v3f(6910.000000, 2051.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 3351.000000, 30.000000);
  v3f(6910.000000, 3351.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6910.000000, 2051.000000, 30.000000);
  v3f(6910.000000, 3351.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 2341.000000, 30.000000);
  v3f(4500.000000, 3061.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5220.000000, 2341.000000, 30.000000);
  v3f(5220.000000, 3061.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 2341.000000, 30.000000);
  v3f(5220.000000, 2341.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 3061.000000, 30.000000);
  v3f(5220.000000, 3061.000000, 30.000000);
  glEnd();
}

void drawRoboticLabGround () {
  glLineWidth(2.0f);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

	glColor3f (1.,1.,1.);
  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 0.000000);
  v3f(0.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 0.000000);
  v3f(1211.000000, 0.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, 0.000000, 0.000000);
  v3f(1211.000000, -554.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, -554.000000, 0.000000);
  v3f(5476.000000, -554.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -554.000000, 0.000000);
  v3f(5476.000000, -119.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -119.000000, 0.000000);
  v3f(6110.000000, -119.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -119.000000, 0.000000);
  v3f(6110.000000, -554.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -554.000000, 0.000000);
  v3f(7925.000000, -554.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, -554.000000, 0.000000);
  v3f(7925.000000, 4879.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, 4879.000000, 0.000000);
  v3f(6264.000000, 4879.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4879.000000, 0.000000);
  v3f(6264.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 0.000000);
  v3f(5498.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 0.000000);
  v3f(5498.000000, 4879.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4879.000000, 0.000000);
  v3f(2419.000000, 4879.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4879.000000, 0.000000);
  v3f(2419.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4589.000000, 0.000000);
  v3f(0.000000, 4589.000000, 0.000000);
  glEnd();

  glColor3f(1., 1., 1.);
	// LINEAS LIMITROFES DEL CAMPO FUTBOL
  glBegin(GL_LINES);
  v3f(1560.000000, 596.000000, 0.000000);
  v3f(1560.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 0.000000);
  v3f(2419.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1560.000000, 596.000000, 0.000000);
  v3f(7925.000000, 596.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 0.000000);
  v3f(7925.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 796.000000, 0.000000);
  v3f(2160.000000, 4301.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 796.000000, 0.000000);
  v3f(7560.000000, 796.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 4301.000000, 0.000000);
  v3f(7560.000000, 4301.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 796.000000, 0.000000);
  v3f(7560.000000, 4301.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4860.000000, 796.000000, 0.000000);
  v3f(4860.000000, 4301.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 2051.000000, 0.000000);
  v3f(2810.000000, 2051.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2810.000000, 2051.000000, 0.000000);
  v3f(2810.000000, 3351.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2810.000000, 3351.000000, 0.000000);
  v3f(2160.000000, 3351.000000, 0.000000 );
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 2051.000000, 0.000000);
  v3f(6910.000000, 2051.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 3351.000000, 0.000000);
  v3f(6910.000000, 3351.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6910.000000, 2051.000000, 0.000000);
  v3f(6910.000000, 3351.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 2341.000000, 0.000000);
  v3f(4500.000000, 3061.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5220.000000, 2341.000000, 0.000000);
  v3f(5220.000000, 3061.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 2341.000000, 0.000000);
  v3f(5220.000000, 2341.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 3061.000000, 0.000000);
  v3f(5220.000000, 3061.000000, 0.000000);
  glEnd();
}

void drawForces () {
	fuerzaResultante.x=myencoders[0]+xRes;
	fuerzaResultante.y=myencoders[1]+yRes;
	repulsiva.x=myencoders[0]+xRep;
	repulsiva.y=myencoders[1]+yRep;
	atractiva.x=myencoders[0]+xAtr;
	atractiva.y=myencoders[1]+yAtr;

  glLoadIdentity ();

  glColor3f(1., 1., 0.);
  glBegin(GL_LINES);
		v3f(myencoders[0], myencoders[1], 500.000000);
		v3f(miObjetivo.x, miObjetivo.y, 500.000000);
  glEnd();

  glColor3f(0., 0., 1.);
  glBegin(GL_LINES);
		v3f(myencoders[0], myencoders[1], 500.000000);
		v3f(fuerzaResultante.x, fuerzaResultante.y, 500.000000);
  glEnd();

  glColor3f(1., 0., 0.);
  glBegin(GL_LINES);
		v3f(myencoders[0], myencoders[1], 500.000000);
		v3f(repulsiva.x, repulsiva.y, 500.000000);
  glEnd();

  glColor3f(0., 1., 0.);
  glBegin(GL_LINES);
		v3f(myencoders[0], myencoders[1], 500.000000);
		v3f(atractiva.x, atractiva.y, 500.000000);
  glEnd();
}

/* We want to know the intersection between a line given by two points (A and B) on 3D, and a plan (the ground, for example). Then, we'll get another point; it'll be our solution */
int linePlaneIntersection (HPoint3D A, HPoint3D B, HPoint3D *intersectionPoint) {
	HPoint3D v;	// Line director vector: it the same to take A or B as origin or destination extrem...

	A.X = -A.X;
	A.Y = -A.Y;
	A.Z = -A.Z;

	B.X = -B.X;
	B.Y = -B.Y;
	B.Z = -B.Z;

	v.X = (A.X - B.X);
	v.Y = (A.Y - B.Y);
	v.Z = (A.Z - B.Z);

	// We'll calculate the groun intersection (Z = 0) on our robot system. Parametric equations:
	intersectionPoint->Z = 0; // intersectionPoint->Z = A.Z + t*v.Z => t = (-A.Z / v.Z)
	t = (-A.Z) / (v.Z);

	intersectionPoint->X = A.X + (t*v.X);
	intersectionPoint->Y = A.Y + (t*v.Y);
}

void pointProjection (HPoint2D point, char **myColor, int puntosExtremos) {
	float temp;

	if (myColor == mycolorA) {
		myCamera = &robotCamera;
	}
	myActualPoint2D.y = point.x;
	myActualPoint2D.x = point.y;
	myActualPoint2D.h = 1.;

	// Proyectamos tal punto en 3D sobre el Plano Imagen de nuestra cámara virtual
	backproject(&myActualPoint3D, myActualPoint2D, *myCamera);

	// Coordenadas en 3D de la posicion de la cámara
	cameraPos3D.X = -myCamera->position.X;
	cameraPos3D.Y = -myCamera->position.Y;
	cameraPos3D.Z = -myCamera->position.Z;
	cameraPos3D.H = 1;

	// Distancia entre los puntos myActualPoint3D - cameraPos3D
	distanciaPlanoImagen=(float)sqrt((double)((myActualPoint3D.X-cameraPos3D.X)*(myActualPoint3D.X-cameraPos3D.X)+(myActualPoint3D.Y-cameraPos3D.Y)*(myActualPoint3D.Y-cameraPos3D.Y)+(myActualPoint3D.Z-cameraPos3D.Z)*(myActualPoint3D.Z-cameraPos3D.Z)));
	incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"
	
	// Alargamos tal distancia (lanzamiento del rayo óptico)
	myEndPoint3D.X = ((incremento * myActualPoint3D.X + (1. - incremento) * cameraPos3D.X));
	myEndPoint3D.Y = ((incremento * myActualPoint3D.Y + (1. - incremento) * cameraPos3D.Y));
	myEndPoint3D.Z = ((incremento * myActualPoint3D.Z + (1. - incremento) * cameraPos3D.Z));
	myEndPoint3D.H = 1.;

	linePlaneIntersection (myActualPoint3D, cameraPos3D, &intersectionPoint); // luego negaremos los puntos

	if (puntosExtremos == TRUE) {
		if (primerPunto == FALSE) { // no está establecido aun el primer punto
			primerPunto3D.X = intersectionPoint.X;
			primerPunto3D.Y = intersectionPoint.Y;
			primerPunto3D.Z = intersectionPoint.Z;
			primerPunto = TRUE;
		} else if (segundoPunto == FALSE) { // no está establecido aun el segundo punto
			segundoPunto3D.X = intersectionPoint.X;
			segundoPunto3D.Y = intersectionPoint.Y;
			segundoPunto3D.Z = intersectionPoint.Z;
			segundoPunto = TRUE;
		} else if (tercerPunto == FALSE) { // no está establecido aun el segundo punto
			tercerPunto3D.X = intersectionPoint.X;
			tercerPunto3D.Y = intersectionPoint.Y;
			tercerPunto3D.Z = intersectionPoint.Z;
			tercerPunto = TRUE;
		} else if (cuartoPunto == FALSE) { // no está establecido aun el segundo punto
			cuartoPunto3D.X = intersectionPoint.X;
			cuartoPunto3D.Y = intersectionPoint.Y;
			cuartoPunto3D.Z = intersectionPoint.Z;
			cuartoPunto = TRUE;
		}
	}
	else {
		if ((worldView == FALSE) && (floorView == TRUE)) { // cuando teníamos el otro minicanvas
			glColor3f (0.5,0.5,0.5);
		  glBegin(GL_LINES);
			  v3f(-cameraPos3D.X, -cameraPos3D.Y, -cameraPos3D.Z);  
			  v3f(-myActualPoint3D.X, -myActualPoint3D.Y, -myActualPoint3D.Z);
		  glEnd();
		} else if (worldView == TRUE) { // en el actual minicanvas
			if (actualFronteraPoint == ((SIFNTSC_COLUMNS) - 1))
				actualFronteraPoint = 0;

			fronteraPoints[actualFronteraPoint].X = myencoders[0] + (-intersectionPoint.X+417.2) * cos (myencoders[2]-0.78) - (-intersectionPoint.Y+476.0) * sin (myencoders[2]-0.78);
			fronteraPoints[actualFronteraPoint].Y = myencoders[1] + (-intersectionPoint.X+417.2) * sin (myencoders[2]-0.78) + (-intersectionPoint.Y+476.0) * cos (myencoders[2]-0.78);
			fronteraPoints[actualFronteraPoint].Z = intersectionPoint.Z;

/*
			fronteraPoints[actualFronteraPoint].X = myencoders[0]-208.6 + intersectionPoint.X * cos (myencoders[2]) - intersectionPoint.Y * sin (myencoders[2]);
			fronteraPoints[actualFronteraPoint].Y = myencoders[1]-238.0 + intersectionPoint.X * sin (myencoders[2]) + intersectionPoint.Y * cos (myencoders[2]);
			fronteraPoints[actualFronteraPoint].Z = intersectionPoint.Z;
*/
/*
			fronteraPoints[actualFronteraPoint].X = intersectionPoint.X;
			fronteraPoints[actualFronteraPoint].Y = intersectionPoint.Y;
			fronteraPoints[actualFronteraPoint].Z = intersectionPoint.Z;
*/
			actualFronteraPoint ++;
		}
	}
}

void drawOpticalRays (TPinHoleCamera *cam) {
  HPoint3D a3A;
  HPoint2D a;

	glColor3f( 0.7, 0.7, 0.7 );

	cameraPos3D.X=0.;
	cameraPos3D.Y=0.;
	cameraPos3D.Z=0.;
	cameraPos3D.H=1.;

	/* optical axis of camera */
	a.x = SIFNTSC_ROWS-1;
	a.y = SIFNTSC_COLUMNS-1;
	a.h = 1.;
	backproject (&a3A, a, *cam);

	distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
	incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

	a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
	a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
	a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
	a3A.H = 1.;

	glBegin(GL_LINES);
		v3f( a3A.X, a3A.Y, a3A.Z );
		v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
	glEnd();

	/**/
	a.x = 0;
	a.y = 0;
	a.h = 1.;
	backproject (&a3A, a, *cam);

	distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
	incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

	a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
	a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
	a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
	a3A.H = 1.;

	glBegin(GL_LINES);
		v3f( a3A.X, a3A.Y, a3A.Z );
		v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
	glEnd();

	/**/
	a.x = SIFNTSC_ROWS-1;
	a.y = 0;
	a.h = 1.;
	backproject (&a3A, a, *cam);

	distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
	incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

	a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
	a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
	a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
	a3A.H = 1.;

	glBegin(GL_LINES);
		v3f( a3A.X, a3A.Y, a3A.Z );
		v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
	glEnd();

	/**/
	a.x = 0;
	a.y = SIFNTSC_COLUMNS-1;
	a.h = 1.;
	backproject (&a3A, a, *cam);

	distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
	incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

	a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
	a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
	a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
	a3A.H = 1.;

	glBegin(GL_LINES);
		v3f( a3A.X, a3A.Y, a3A.Z );
		v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
	glEnd();
}

void drawCam(TPinHoleCamera *cam, int b)
{
  HPoint3D a3A, a3B, a3C, a3D;
  HPoint2D a;

/*	if (b==2){
		glColor3f( 0.7, 0.7, 0.7 );
	} else if (b==3){
		glColor3f( 0.7, 0.7, 0.7 );
	} else if (b==4){
		glColor3f( 0.7, 0.7, 0.7 );
	} else if (b==5){
		glColor3f( 0.7, 0.7, 0.7 );
	}
*/
	glColor3f( 0.7, 0.7, 0.7 );

	cameraPos3D.X=cam->position.X;
	cameraPos3D.Y=cam->position.Y;
	cameraPos3D.Z=cam->position.Z;
	cameraPos3D.H=cam->position.H;

	/* optical axis of camera */
	if ((mycolorA!=NULL) && (cam == &robotCamera)) {
		a.x = SIFNTSC_ROWS-1;
		a.y = SIFNTSC_COLUMNS-1;
		a.h = 1.;
		//a.y = GRAPHIC_TO_OPTICAL_Y(a.y,a.x);
		//a.x = GRAPHIC_TO_OPTICAL_X(a.y,a.x);
		backproject (&a3A, a, *cam);

		a3A.X = -a3A.X;
		a3A.Y = -a3A.Y;
		a3A.Z = -a3A.Z;

		distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
		incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

		a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
		a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
		a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
		a3A.H = 1.;

		glBegin(GL_LINES);
			v3f( a3A.X, a3A.Y, a3A.Z );
			v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glEnd();

		/**/
		a.x = 0;
		a.y = 0;
		a.h = 1.;
		//a.y = GRAPHIC_TO_OPTICAL_Y(a.y,a.x);
		//a.x = GRAPHIC_TO_OPTICAL_X(a.y,a.x);

		backproject (&a3A, a, *cam);

		distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
		incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

		a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
		a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
		a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
		a3A.H = 1.;

		glBegin(GL_LINES);
			v3f( a3A.X, a3A.Y, a3A.Z );
			v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glEnd();

		/**/
		a.x = SIFNTSC_ROWS-1;
		a.y = 0;
		a.h = 1.;
		//a.y = GRAPHIC_TO_OPTICAL_Y(a.y,a.x);
		//a.x = GRAPHIC_TO_OPTICAL_X(a.y,a.x);

		backproject (&a3A, a, *cam);

		a3A.X = -a3A.X;
		a3A.Y = -a3A.Y;
		a3A.Z = -a3A.Z;

		distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
		incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

		a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
		a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
		a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
		a3A.H = 1.;

		glBegin(GL_LINES);
			v3f( a3A.X, a3A.Y, a3A.Z );
			v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glEnd();

		/**/
		a.x = 0;
		a.y = SIFNTSC_COLUMNS-1;
		a.h = 1.;
		//a.y = GRAPHIC_TO_OPTICAL_Y(a.y,a.x);
		//a.x = GRAPHIC_TO_OPTICAL_X(a.y,a.x);

		backproject (&a3A, a, *cam);

		a3A.X = -a3A.X;
		a3A.Y = -a3A.Y;
		a3A.Z = -a3A.Z;

		distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
		incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

		a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
		a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
		a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
		a3A.H = 1.;

		glBegin(GL_LINES);
			v3f( a3A.X, a3A.Y, a3A.Z );
			v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glEnd();
	} // fin if (dibujamos los rayos opticos, si estamos en la cámara oportuna...)

	/* FieldOfView of camera */
	a.x = SIFNTSC_ROWS - 1.;
	a.y = SIFNTSC_COLUMNS - 1.;
	a.h = 1.;
	backproject(&a3A, a, *cam);
	a.x = 0.;
	a.y = SIFNTSC_COLUMNS - 1.;
	a.h = 1.;
	backproject(&a3B, a, *cam);
	a.x = 0.;
	a.y = 0.;
	a.h = 1.;
	backproject(&a3C, a, *cam);
	a.x = SIFNTSC_ROWS - 1.;
	a.y = 0.;
	a.h = 1.;
	backproject(&a3D, a, *cam);

	// first triangle
	glBegin(GL_LINES);
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glVertex3f(a3A.X, a3A.Y, a3A.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(a3A.X, a3A.Y, a3A.Z );
		glVertex3f(a3B.X, a3B.Y, a3B.Z );
	glEnd ();

	glBegin(GL_LINES);
		glVertex3f(a3B.X, a3B.Y, a3B.Z );
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
	glEnd ();

	glColor3f( 0.3, 0.3, 0.3 );
	// second triangle
	glBegin(GL_LINES);
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glVertex3f(a3B.X, a3B.Y, a3B.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(a3B.X, a3B.Y, a3B.Z );
		glVertex3f(a3C.X, a3C.Y, a3C.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(a3C.X, a3C.Y, a3C.Z );
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
	glEnd();

	glColor3f( 0.7, 0.7, 0.7 );
	// third triangle
	glBegin(GL_LINES);
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glVertex3f(a3C.X, a3C.Y, a3C.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(a3C.X, a3C.Y, a3C.Z );
		glVertex3f(a3D.X, a3D.Y, a3D.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(a3D.X, a3D.Y, a3D.Z );
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
	glEnd();

	glColor3f( 0.3, 0.3, 0.3 );
	// last triangle
	glBegin(GL_LINES);
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glVertex3f(a3D.X, a3D.Y, a3D.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(a3D.X, a3D.Y, a3D.Z );
		glVertex3f(a3A.X, a3A.Y, a3A.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(a3A.X, a3A.Y, a3A.Z );
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
	glEnd();

	glColor3f( 0.1, 0.1, 0.1 );
	// square
	glBegin(GL_LINES);
		glVertex3f(a3A.X, a3A.Y, a3A.Z );
		glVertex3f(a3D.X, a3D.Y, a3D.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(a3D.X, a3D.Y, a3D.Z );
		glVertex3f(a3C.X, a3C.Y, a3C.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(a3C.X, a3C.Y, a3C.Z );
		glVertex3f(a3B.X, a3B.Y, a3B.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(a3B.X, a3B.Y, a3B.Z );
		glVertex3f(a3A.X, a3A.Y, a3A.Z );
	glEnd();
}

void rellenoMemoria (TdatosMemoria *d,int v[SIFNTSC_COLUMNS],Tvoxel m[SIFNTSC_COLUMNS]) {
	int i,j,colocado,masViejo;
	int hora;
	int angulo_nuevo;	
	Tvoxel punto;
	float distancia,distanciaPuntMem; 

	for(i=0;i<SIFNTSC_COLUMNS;i++) {
		colocado=FALSE; /* no tenemos ubicado el punto del laser actual */
		masViejo=0;
		j=(*d).ultimo; /* cogemos el indice del ultimo punto de memoria que se ha actualizado, para asi ir comparando (y sólo) con todos los que hay */
		//laser2xy(i,mylaser[i],&punto,&myencoders[0]); /* calculamos posicion de punto detectado por laser i */
		punto.x = fronteraPoints[i].X;
		punto.y = fronteraPoints[i].Y;
		angulo_nuevo= (int)(atan2(punto.y-myencoders[1],punto.x-myencoders[0])*RADTODEG); /* angulo del punto vs. robot */
		distancia=((punto.x-myencoders[0])*(punto.x-myencoders[0])+(punto.y-myencoders[1])*(punto.y-myencoders[1]));
		hora=dameTiempo();

		while((!colocado)&&(j>=0)) { /* bucle para ubicar el punto detectado por laser */
			if(v[j]) { /* si hay punto valido */
				/* Si hay punto valido miro si en la memoria hay alguno en la misma proyeccion del laser o algun punto similar. Si se cumple alguna de las dos reemplazo en ese lugar,sino busco un hueco vacio y si no lo hay lo pongo donde el mas viejo */
				distanciaPuntMem=((m[j].x-myencoders[0])*(m[j].x-myencoders[0])+(m[j].y-myencoders[1])*(m[j].y-myencoders[1]));

				if(reemplazoSimilares(m[j],punto,distanciaPuntMem,distancia)) { /* si son puntos muy cercanos (similares) */
					m[j]=punto;
					(*d).horaInicio[j]=hora;
					(*d).ultimo=j;
					v[j]=TRUE;
					colocado=TRUE; /* salimos del bucle, ya esta ubicado */
				} else if (mismoAnguloyDistancia(m[j],distanciaPuntMem,angulo_nuevo,distancia)) { 
					/*Si estan en la misma proyeccion y el punto nuevo esta mas alejado que el punto viejo*/
					m[j]=punto;
					(*d).horaInicio[j]=hora;
					(*d).ultimo=j;
					v[j]=TRUE;
					colocado=TRUE; /* salimos del bucle, ya esta ubicado */
				}
			} /* fin if (v[j]) */
			
			if (v[masViejo]) { /* si la posicion del viejo es valida... */
				if (!v[j]) masViejo=j; /* ...y la posicion actual de j es NO VALIDA, entonces hemos encontrado un hueco */
				else if ((*d).horaInicio[j]<(*d).horaInicio[masViejo]) masViejo=j;
			} /* asi, cuando salgamos del bucle "por esta via" tendremos el hueco, siendo la casilla mas vieja */

			j--; /* vamos a comprobar con otro punto, hasta acabar con los existentes en memoria */
		} /* fin while((!colocado)&&(j>=0)) */

		if(!colocado) { /* si no hemos logrado ubicarlo */
			j=(*d).ultimo+1; /* ahora miramos en la otra parte (del ultimo "hacia la derecha") */
			while((!colocado)&&(j<SIFNTSC_COLUMNS)) {
				if(v[j]) {
				/* Si hay punto valido miro si en la memoria hay alguno en la misma proyeccion del laser o algun punto similar. Si se cumple alguna de las dos reemplazo en ese lugar,sino busco un hueco vacio y si no lo hay lo pongo donde el mas viejo */
					distanciaPuntMem=((m[j].x-myencoders[0])*(m[j].x-myencoders[0])+(m[j].y-myencoders[1])*(m[j].y-myencoders[1]));

					if(reemplazoSimilares(m[j],punto,distanciaPuntMem,distancia)) {
						m[j]=punto;
						(*d).horaInicio[j]=hora;
						(*d).ultimo=j;
						v[j]=TRUE;
						colocado=TRUE;
					}else
				 if(mismoAnguloyDistancia(m[j],distanciaPuntMem,angulo_nuevo,distancia)) {	  
						m[j]=punto;
						(*d).horaInicio[j]=hora;
						(*d).ultimo=j;
						v[j]=TRUE;
						colocado=TRUE;
					}
				} /* fin if (v[j]) */

				if (v[masViejo]) {
					if (!v[j]) masViejo=j;
					else if ((*d).horaInicio[j]<(*d).horaInicio[masViejo]) masViejo=j;
				}

				j++;
			} /* fin while((!colocado)&&(j<tamMemoria)) */
		} /* fin if(!colocado) */

		if (!colocado) { /* finalmente si no lo hemos ubicado, lo insertamos en la casilla mas vieja */
			m[masViejo]=punto;
			(*d).horaInicio[masViejo]=hora;
			(*d).ultimo=masViejo; /* ahora el ultimo sera lo que antes era el mas viejo... */
			v[masViejo]=TRUE;
		}
	} /* fin for(i=0;i<SIFNTSC_COLUMNS;i++) */
}

void guardarCoordObjetivo(TdatosResultante *res) {
	float auxi1;
	double radianes;

	(*res).direccionFuerzaAtractiva[0]=miObjetivo.x-myencoders[0];
	(*res).direccionFuerzaAtractiva[1]=miObjetivo.y-myencoders[1];

	(*res).anguloFuerzaAtractiva=atan2((*res).direccionFuerzaAtractiva[1],(*res).direccionFuerzaAtractiva[0]);
		
	(*res).moduloFuerzaAtractiva=moduloAtractiva;

	radianes=(*res).anguloFuerzaAtractiva;

	(*res).direccionFuerzaAtractiva[0]=cos(radianes)*(*res).moduloFuerzaAtractiva;
	(*res).direccionFuerzaAtractiva[1]=sin(radianes)*(*res).moduloFuerzaAtractiva;

	auxi1=transformoAngulo((*res).anguloFuerzaAtractiva);
	(*res).anguloFuerzaAtractiva=auxi1;
}

void calculaResultante(Tvoxel m[SIFNTSC_COLUMNS],int v[SIFNTSC_COLUMNS],TdatosResultante *res) {
	int angulo,ang_robot,diff_ang;
	int sumaX, sumaY, i;
	int distancia,modulo;
	float auxi, auxi1;
	double radianes;

	sumaX=0;
	sumaY=0;

	for (i=0; i<SIFNTSC_COLUMNS; i++) {
		if (v[i]) {
			if (distancia!=0) {
				/* calculo la distancia al robot y el angulo respecto a este */
				distancia=(((m[i].x-myencoders[0])*(m[i].x-myencoders[0]))+((m[i].y-myencoders[1])*(m[i].y-myencoders[1])));
				radianes=atan2((m[i].y-myencoders[1]),(m[i].x-myencoders[0]));
				angulo=(int) (radianes*RADTODEG);
				ang_robot=(int)(myencoders[2]*RADTODEG);

				/* SUTILEZA PARA QUE EL ROBOT TENGA QUE GIRAR LO MENOS POSIBLE */
				if(ang_robot>=180) ang_robot=-360+ang_robot; /* =-(360-ang_robot)-->pasa a negativo */

				diff_ang=abs(angulo-ang_robot); /* calculamos el total que debería girar el robot */

				if (diff_ang>=180) diff_ang=360-diff_ang; /* mejor si gira por el camino más corto */
				/* FIN DE LA SUTILEZA */

				/* Miramos si el punto esta dentro del alcance en el que los puntos generan fuerza de repulsión. La distancia es distinta segun el angulo en el que se encuentre el punto respecto del robot */
				if (((diff_ang>45)&&(diff_ang<135)&&(distancia<alcanceLateral))||
					(((diff_ang<=45)||(diff_ang>=135))&&(distancia<alcance))) { 
					/* Primero miramos si la distancia es menor que la seguridad asignada. Si es menor giramos sin avanzar */
					if (((diff_ang>45)&&(diff_ang<135)&&(distancia<seguridadLateral))||
						((diff_ang<=45)&&(distancia<seguridad)))
          
						(*res).avanzaEnGiro=FALSE; /* inicialmente está a TRUE */

					/* sumo al angulo 180 ya que lo que buscamos es fuerza repulsiva y queremos el angulo opuesto */
					radianes=radianes+(180.0*DEGTORAD);

					/* calculamos el modulo */
					modulo=calculoFuerza(distancia,diff_ang); /* FUERZA REPULSIVA */

					/* Añadimos las componentes a la suma de x y de y.*/
					sumaX+=cos(radianes)*modulo;
					sumaY+=sin(radianes)*modulo;	
				}
			} /* fin if(distancia!=0) */
		} /* fin if(v[i]) */
	} /* fin FOR */

	(*res).direccionFuerzaRepulsiva[0]=sumaX;
	(*res).direccionFuerzaRepulsiva[1]=sumaY;

	/*printf("xRep:%d f_0.yRep:%d\n",(*res).direccionFuerzaRepulsiva[0],(*res).direccionFuerzaRepulsiva[1]);*/

	/* Calculo la fuerza resultante atribuyendo factores de proporción a la fuerza atractiva y repulsiva */
	(*res).direccionFuerzaResultante[0]=(alpha *(*res).direccionFuerzaAtractiva[0]) +(beta*(*res).direccionFuerzaRepulsiva[0]);
	(*res).direccionFuerzaResultante[1]=(alpha *(*res).direccionFuerzaAtractiva[1]) +(beta*(*res).direccionFuerzaRepulsiva[1]);

	sumaX=(*res).direccionFuerzaResultante[0];
	sumaY=(*res).direccionFuerzaResultante[1];

	(*res).moduloFuerzaResultante=sqrt((sumaX*sumaX)+(sumaY*sumaY));
	(*res).anguloFuerzaResultante=atan2(sumaY,sumaX);	  

	auxi1=transformoAngulo((*res).anguloFuerzaResultante);
	(*res).anguloFuerzaResultante=auxi1;
	/*printf("Angulo resultante:%f\n",(*res).anguloFuerzaResultante);*/

	if ((sumaX==(*res).direccionFuerzaAtractiva[0])&&(sumaY==(*res).direccionFuerzaAtractiva[1])) { /* ¿? sumaX y sumaY han variado, por los factores aplicados...esto NO FUNCIONARÁ */
		/* en caso k no actuen fuerzas, solo tenemos la atractiva*/
		(*res).anguloFuerzaResultante=(*res).anguloFuerzaAtractiva;;
		auxi=((*res).anguloFuerzaResultante)*100;
		auxi1=ceil(auxi);
		(*res).anguloFuerzaResultante=auxi1/100;
	}

	if((*res).anguloFuerzaResultante==0.) {
		/* es mas facil detectar 6.28 k 2*Pi, por eso aproximamos*/
		(*res).anguloFuerzaResultante=2*M_PI;
		auxi=((*res).anguloFuerzaResultante)*100;
		auxi1=ceil(auxi);
		(*res).anguloFuerzaResultante=transformoAngulo(auxi1/100);
	}

	auxi=((*res).anguloFuerzaResultante)*100;
	auxi1=ceil(auxi);
	(*res).anguloFuerzaResultante=(auxi1/100);

}

void comportamientoVFF() {
	float auxi,auxi1;

	resultante.anguloQueLlevo=myencoders[2]; /* Nos quedamos con 2 decimales */
	auxi=resultante.anguloQueLlevo*100;
	auxi1=ceil(auxi);
	resultante.anguloQueLlevo=auxi1/100;

	auxi1=transformoAngulo(resultante.anguloQueLlevo);
	resultante.anguloQueLlevo=auxi1;

	auxi=resultante.anguloQueLlevo*100; /* Nos quedamos con 2 decimales */
	auxi1=ceil(auxi);
	resultante.anguloQueLlevo=(auxi1/100);

	rellenoMemoria(pdatos,validos,memoria); /* relleno memoria en funcion de los puntos dados por la cámara!! */

	guardarCoordObjetivo(result); /* guardamos la FUERZA ATRACTIVA */
	/* Inicializo a TRUE avanzaEnGiro para que si no hay obstaculos por debajo de la seguridad se avanza a la vez que se gira */
	resultante.avanzaEnGiro=TRUE;

	calculaResultante(memoria,validos,result);

	/* Miro si he alcanzado el objetivo */
	if ((abs((int)(miObjetivo.x-myencoders[0]))<200)&&(abs((int)(miObjetivo.y-myencoders[1]))<200)) {   
		distanciaObjetivo=0;
		resultante.direccionFuerzaAtractiva[0]=0.0;
		resultante.direccionFuerzaAtractiva[1]=0.0;
	}	else distanciaObjetivo=1;

	/* Obtenemos los siguientes valores, para el posterior pintado de fuerzas */
	xRep=resultante.direccionFuerzaRepulsiva[0]; /* REPULSIVA */
	yRep=resultante.direccionFuerzaRepulsiva[1];

	xAtr=resultante.direccionFuerzaAtractiva[0]; /* ATRACTIVA */
	yAtr=resultante.direccionFuerzaAtractiva[1];

	xRes=resultante.direccionFuerzaResultante[0]; /* RESULTANTE = REPULSIVA vs. ATRACTIVA */
	yRes=resultante.direccionFuerzaResultante[1];

	difAngle=(resultante.anguloFuerzaResultante)-resultante.anguloQueLlevo;

	/* Sutileza para que el robot gire por el lado de menos giro */
	if (difAngle>180) difAngle=difAngle-360;
	if (difAngle<-180) difAngle=360+difAngle;

	peligro=0;

	if (!resultante.avanzaEnGiro) peligro=1;

	if (seCumpleColorObjetivo) { /* estamos en el objetivo */
		*myv=0.0;
		*myw=0.0;
		vActual=*myv;
		wActual=*myw;
		printf ("He encontrado la pelotita\n");
		system ("sudo killall -9 jderobot");
	} else { /* reglas AD-HOC */
/*		if (((localNavigationBehavior == secWindow) || (localNavigationBehavior == forgetMemory)) && (sitioEstrecho ())) { // COMPORTAMIENTO VFF VS VENTANA
			*myv = vMax; 
			*myw = 0.0; // a toa leche, podemos seguir recto por el sitio estrecho
			rellenoEstela (SECWINDOW_COLOR);
		} else { // COMPORTAMIENTO VFF PURO*/
			//rellenoEstela (VFF_COLOR);
			if (seCumpleColorRotonda) {
				*myw=wPosMax*4;
				*myv=vMax/2;
			} else if ((counterColumnsLeft > umbralPixelesMAX) && (counterColumnsRight < umbralPixelesMIN)) {
				*myw=wNegMax;
				*myv=vMax;
			} else if ((counterColumnsRight > umbralPixelesMAX) && (counterColumnsLeft < umbralPixelesMIN)) {
				*myw=wPosMax;
				*myv=vMax;
			} else if ((counterColumnsRight > umbralPixelesMAX) && (counterColumnsLeft > umbralPixelesMAX) && (counterColumnsCenter > umbralPixelesMAX)) {
				*myv=0.0; *myw=0.0;
				//if(difAngle<0) {*myv=0.0; *myw=wNegMax*5;}
				//else {*myv=0.0; *myw=wPosMax*5;}
/*			}	else if(peligro==1) { /* LO MAS URGENTE, SI HAY PELIGRO INMINENTE, GIRAMOS LENTAMENTE **
				//printf ("peligro!!\n");
				if(difAngle<0) {*myv=-100.0; *myw=wPosMax;}
				else {*myv=-100.0; *myw=wNegMax;}*/
			}	/*else if (abs(difAngle)<limiteTrayectoriaSimilar) {*myv=vMax; *myw=0.0;} // VIENTO EN POPA *
			else if (abs(difAngle)>limiteTrayectoriaDesigual) { // GIRAMOS RAPIDAMENTE *
				if (difAngle<0) {*myv=0.0; *myw=wNegMax;}
				else {*myv=0.0; *myw=wPosMax;}
			}	else if (difAngle<0) {*myv=vMax/2; *myw=wNegMax/2;} // MITAD, MITAD... (FIFTY, FIFTY) *
			else {*myv=vMax/2; *myw=wPosMax/2;}*/
			else if (/*(counterColumnsRight > umbralPixelesMAX) && (counterColumnsLeft > umbralPixelesMAX) && */(counterColumnsCenter > umbralPixelesMAX)) {
				if(difAngle<0) {*myv=0.0; *myw=wNegMax;}
				else {*myv=0.0; *myw=wPosMax;}
			} else {*myv=vMax; *myw=0.;}
		//}
	} /* fin reglas AD-HOC */
}

void frontera_iteration() {
/*	if( golocalNavigation ) {
		miObjetivo.x = target.x;
		miObjetivo.y = target.y;

		comportamientoVFF (); /* usamos comportamiento VFF + VENTANA SOBRE MEMORIA --MUY BUENO --**
	}*/

//	if (!defineObjetivo) {
	  laser2xy(90,500,&miObjetivo,&myencoders[0]); // el objetivo estará siempre 2 metros por delante :)
	  defineObjetivo = TRUE;
//	}
	comportamientoVFF ();
}

/*Importar símbolos*/
void frontera_imports() {
	mycolorA = (char**) myimport("colorA","colorA");
	colorArun = (runFn) myimport("colorA", "run");
	colorAstop	= (stopFn) myimport("colorA", "stop");
	mycolorAwidth	= (int*) myimport("colorA", "width");
	mycolorAheight = (int*) myimport("colorA", "height");

	if (mycolorA == NULL || colorArun == NULL || colorAstop == NULL) {
	  fprintf(stderr, "I can't import variables from colorA\n");
	  jdeshutdown(1);
	}

	myencoders=(float *)myimport("encoders","jde_robot");
	encodersrun=(runFn)myimport("encoders","run");
	encodersstop=(stopFn)myimport("encoders","stop");

  myv=(float *)myimport("motors","v");
  myw=(float *)myimport("motors","w");
  motorsrun=(runFn)myimport("motors","run");
  motorsstop=(stopFn)myimport("motors","stop");
}

void frontera_stop()
{
	colorAstop();

	RGB2HSV_destroyTable();

	pthread_mutex_lock(&(all[frontera_id].mymutex));
	put_state(frontera_id,slept);
	printf("frontera: off\n");
	pthread_mutex_unlock(&(all[frontera_id].mymutex));

	free(imageA);
	free(imageAbordered);
	free(imageAfiltered);

	remove_image(originalImageRGB);
	remove_image(borderImageRGB);
	remove_image(filteredImageRGB);
}


void frontera_run(int father, int *brothers, arbitration fn)
{
	int i;

  pthread_mutex_lock(&(all[frontera_id].mymutex)); // CERROJO -- LOCK
  /* this schema resumes its execution with no children at all */
  for(i=0;i<MAX_SCHEMAS;i++) all[frontera_id].children[i]=FALSE;
 
  all[frontera_id].father=father;
  if (brothers!=NULL)
    {
      for(i=0;i<MAX_SCHEMAS;i++) frontera_brothers[i]=-1;
      i=0;
      while(brothers[i]!=-1) {frontera_brothers[i]=brothers[i];i++;}
    }
  frontera_callforarbitration=fn;
  put_state(frontera_id,notready);
  frontera_imports();
  printf("frontera: on\n");
  pthread_cond_signal(&(all[frontera_id].condition));
  pthread_mutex_unlock(&(all[frontera_id].mymutex)); // CERROJO -- UNLOCK

  /* Wake up drivers schemas */	
  colorArun(frontera_id, NULL, NULL);

	myHSV = (struct HSV*)malloc(sizeof(struct HSV));

	image = (char*) malloc(SIFNTSC_COLUMNS * SIFNTSC_ROWS * 3);
	contourImage = (char*) malloc(SIFNTSC_COLUMNS * SIFNTSC_ROWS * 3);

  //imageA = create_image(*mycolorAwidth, *mycolorAheight, 3);
  imageA = create_image(SIFNTSC_COLUMNS, SIFNTSC_ROWS, 3);
  free(imageA->image); // vaciamos el espacio reservado para la imagen
  imageA->image = (char *) *mycolorA; // asignamos directamente lo que viene de mycolorA

  //imageAfiltered = create_image(*mycolorAwidth, *mycolorAheight, 3);
  imageAfiltered = create_image(SIFNTSC_COLUMNS, SIFNTSC_ROWS, 3);
  //free(imageAfiltered->image); // vaciamos el espacio reservado para la imagen
  //drawFilteredImage ();

  //imageAbordered = create_image(*mycolorAwidth, *mycolorAheight, 3);
  imageAbordered = create_image(SIFNTSC_COLUMNS, SIFNTSC_ROWS, 3);
  //free(imageAbordered->image); // vaciamos el espacio reservado para la imagen
  //drawGroundImage ();

	//originalImageRGB = create_image(*mycolorAwidth, *mycolorAheight, 3);
  originalImageRGB = create_image(SIFNTSC_COLUMNS, SIFNTSC_ROWS, 3);
	//filteredImageRGB = create_image(*mycolorAwidth, *mycolorAheight, 3);
  filteredImageRGB = create_image(SIFNTSC_COLUMNS, SIFNTSC_ROWS, 3);
	//borderImageRGB = create_image(*mycolorAwidth, *mycolorAheight, 3);
  borderImageRGB = create_image(SIFNTSC_COLUMNS, SIFNTSC_ROWS, 3);
}

void *frontera_thread(void *not_used)
{
  struct timeval a,b;
  long diff, next;

  for(;;)
    {
      pthread_mutex_lock(&(all[frontera_id].mymutex));

      if (all[frontera_id].state==slept) 
	{
	  pthread_cond_wait(&(all[frontera_id].condition),&(all[frontera_id].mymutex));
	  pthread_mutex_unlock(&(all[frontera_id].mymutex));
	}
      else 
	{
	  /* check preconditions. For now, preconditions are always satisfied*/
	  if (all[frontera_id].state==notready) put_state(frontera_id,ready);
	  else if (all[frontera_id].state==ready)	  /* check brothers and arbitrate. For now this is the only winner */
	    { 
	      put_state(frontera_id,winner);
	    }	  
	  else if (all[frontera_id].state==winner);

	  if (all[frontera_id].state==winner)
	    /* I'm the winner and must execute my iteration */
	    {
	      pthread_mutex_unlock(&(all[frontera_id].mymutex));
	      gettimeofday(&a,NULL);
	      frontera_iteration();
	      gettimeofday(&b,NULL);  

	      diff = (b.tv_sec-a.tv_sec)*1000000+b.tv_usec-a.tv_usec;
	      next = frontera_cycle*1000-diff-10000; 
	      if (next>0) 
		/* discounts 10ms taken by calling usleep itself */
		usleep(frontera_cycle*1000-diff);
	      else 
		/* just let this iteration go away. overhead time negligible */
		{printf("time interval violated: frontera\n"); 
		usleep(frontera_cycle*1000);
		}
	    }
	  else 
	    {
	      pthread_mutex_unlock(&(all[frontera_id].mymutex));
	      usleep(frontera_cycle*1000);
	    }
	}
    }
}

void frontera_terminate()
{
  pthread_mutex_lock(&(all[frontera_id].mymutex));
  frontera_stop();  
  pthread_mutex_unlock(&(all[frontera_id].mymutex));
  sleep(2);
  //fl_free_form(fd_fronteragui->fronteragui);
//  free(imagenA_buf);
//  free(imagenFiltrada_buf);
//  free(imagenSuelo_buf);
}

void createConfigFile () {
	FILE *salida;
	salida = fopen (configFile,"w");

	if (salida == NULL) {
		fprintf(stderr,"Parser: I can't found file %s\n", configFile); exit(-1);
	}

	printf("Building config file %s...\n", configFile);
	fprintf(salida,"dimension = %d\n", 4000);
	fprintf(salida,"resolucion = 50\n");
	fprintf(salida,"tipo = ec_diferencial\n");
	fprintf(salida,"ec_diferencial_speed = 1\n");
	fprintf(salida,"paso_tiempo = nulo\n");
	fprintf(salida,"cell_angles = 0\n");
	fprintf(salida,"mayoria_saturacion = 30\n");
	fprintf(salida,"mayoria_ruido = 10\n");
	fprintf(salida,"long_historia = 0\n");
	fprintf(salida,"\n");
	fprintf(salida,"sonar_filtra = independientes\n");
	fprintf(salida,"sonar_geometria = cono_denso\n");
	fprintf(salida,"sonar_apertura = 20.\n");
	fprintf(salida,"sonar_noobstacle = 3000.\n");
	fprintf(salida,"sonar_radialerror = 10.\n");
	fprintf(salida,"sonar_fdistancia = lineal\n");
	fprintf(salida,"sonar_residuo = 0.05\n");
	fprintf(salida,"sonar_o = 0.4\n");
	fprintf(salida,"sonar_e = -0.5\n");
	fprintf(salida,"sonar_mind = 700\n");
	fprintf(salida,"sonar_maxd = 1100.\n");
	fprintf(salida,"\n");
	fprintf(salida,"robot_geometria = cilindro\n");
	fprintf(salida,"robot_radio = 248.\n");
	fprintf(salida,"robot_e = -0.8\n");
	fprintf(salida,"\n");
	fprintf(salida,"laser_geometria = cono_denso\n");
	fprintf(salida,"laser_apertura = 0.5\n");
	fprintf(salida,"laser_muestras = 90\n");
	fprintf(salida,"laser_noobstacle = 8000.\n");
	fprintf(salida,"laser_o = 1\n");
	fprintf(salida,"laser_e = -0.7\n");

	fclose(salida);
}

/*Exportar símbolos*/
void frontera_exports()
{
	myexport("frontera","id",&frontera_id);
	myexport("frontera","cycle",&frontera_cycle);
	myexport("frontera","run",(void *)frontera_run);
	myexport("frontera","stop",(void *)frontera_stop);
	myexport("frontera","target",&target);
	myexport("frontera","golocalNavigation",&golocalNavigation);
	myexport("frontera","distanciaObjetivo",&distanciaObjetivo);/*Flag alcanzado objetivo, vale 0 si estamos en el objetivo*/
}

void frontera_init(char *configfile)
{
	int i, hora;

  pthread_mutex_lock(&(all[frontera_id].mymutex)); // CERROJO -- LOCK

  printf("frontera schema started up\n");
  frontera_exports();
  put_state(frontera_id,slept);
  pthread_create(&(all[frontera_id].mythread),NULL,frontera_thread,NULL);
  if (myregister_displaycallback==NULL){
		if ((myregister_displaycallback=(registerdisplay)myimport ("graphics_gtk", "register_displaycallback"))==NULL)
		{
		  printf ("I can't fetch register_displaycallback from graphics_gtk\n");
		  jdeshutdown(1);
		}
		if ((mydelete_displaycallback=(deletedisplay)myimport ("graphics_gtk", "delete_displaycallback"))==NULL)
		{
		  printf ("I can't fetch delete_displaycallback from graphics_gtk\n");
		  jdeshutdown(1);
		}
  }

  pthread_mutex_unlock(&(all[frontera_id].mymutex)); // CERROJO -- UNLOCK

	// Valores iniciales para la cámara virtual con la que observo la escena de puntos 3D en el suelo
  virtualcam0.position.X=4000.;
  virtualcam0.position.Y=4000.;
  virtualcam0.position.Z=6000.;
  virtualcam0.position.H=1.;
  virtualcam0.foa.X=0.;
  virtualcam0.foa.Y=0.;
  virtualcam0.foa.Z=0.;
  virtualcam0.position.H=1.;
  virtualcam0.roll=0.;

	// Valores iniciales para la cámara virtual con la que observo la escena
  virtualcam1.position.X=4000.;
  virtualcam1.position.Y=4000.;
  virtualcam1.position.Z=6000.;
  virtualcam1.position.H=1.;
  virtualcam1.foa.X=0.;
  virtualcam1.foa.Y=0.;
  virtualcam1.foa.Z=0.;
  virtualcam1.position.H=1.;
  virtualcam1.roll=0.;

	// Valores para la cámara virtual con la que observo el entorno de frontera al robot
  virtualcam2.position.X=4000.;
  virtualcam2.position.Y=4000.;
  virtualcam2.position.Z=6000.;
  virtualcam2.position.H=1.;
  virtualcam2.foa.X=0.;
  virtualcam2.foa.Y=0.;
  virtualcam2.foa.Z=0.;
  virtualcam2.foa.H=1.;
  virtualcam2.roll=0.;

	// Valores para la cámara del techo del laboratorio
  ceilLabCam.position.X=0.000000;
  ceilLabCam.position.Y=0.000000;
  ceilLabCam.position.Z=12000.000000;
  ceilLabCam.position.H=1.;
  ceilLabCam.foa.X=0.000000;
  ceilLabCam.foa.Y=0.0000000;
  ceilLabCam.foa.Z=0.00000;
  ceilLabCam.foa.H=1.;
  ceilLabCam.fdistx=405.399994;
	ceilLabCam.fdisty=ceilLabCam.fdistx;
	ceilLabCam.skew=0.;
  ceilLabCam.u0=142.600006;
  ceilLabCam.v0=150.399994;
  ceilLabCam.roll=1.55;

	// Valores para la cámara virtual de la esquina derecha del laboratorio (myColorA)
  roboticLabCam0.position.X=7875.000000;
  roboticLabCam0.position.Y=-514.000000;
  roboticLabCam0.position.Z=3000.000000;
  roboticLabCam0.position.H=1.;
  roboticLabCam0.foa.X=6264.000000;
  roboticLabCam0.foa.Y=785.0000000;
  roboticLabCam0.foa.Z=1950.00000;
  roboticLabCam0.foa.H=1.;
  roboticLabCam0.fdistx=405.399994;
	roboticLabCam0.fdisty=roboticLabCam0.fdistx;
	roboticLabCam0.skew=0.;
  roboticLabCam0.u0=142.600006;
  roboticLabCam0.v0=150.399994;
  roboticLabCam0.roll=3.107262;

	// Valores para la cámara virtual de la esquina derecha de enfrente del laboratorio (myColorB)
  roboticLabCam1.position.X=7875.000000;
  roboticLabCam1.position.Y=4749.000000;
  roboticLabCam1.position.Z=3000.000000;
  roboticLabCam1.position.H=1.;
  roboticLabCam1.foa.X=6264.000000;
  roboticLabCam1.foa.Y=3700.000000;
  roboticLabCam1.foa.Z=1950.00000;
  roboticLabCam1.foa.H=1.;
	roboticLabCam1.fdistx=405.399994;
	roboticLabCam1.fdisty=roboticLabCam1.fdistx;
	roboticLabCam1.skew=0.;
  roboticLabCam1.u0=142.600006;
  roboticLabCam1.v0=142.600006;
  roboticLabCam1.roll=3.007028;

	// Valores para la cámara virtual de la esquina izquierda de enfrente del laboratorio (myColorC)
  roboticLabCam2.position.X=50.000000;
  roboticLabCam2.position.Y=4471.000000;
  roboticLabCam2.position.Z=2955.000000;
  roboticLabCam2.position.H=1.;
  roboticLabCam2.foa.X=2432.399902;
  roboticLabCam2.foa.Y=2918.000000;
  roboticLabCam2.foa.Z=1300.000000;
  roboticLabCam2.foa.H=1.;
	roboticLabCam2.fdistx=405.399994;
	roboticLabCam2.fdisty=roboticLabCam2.fdistx;
	roboticLabCam2.skew=0.;
  roboticLabCam2.u0=142.600006;
  roboticLabCam2.v0=150.399994;
  roboticLabCam2.roll=3.107262;

	// Valores para la cámara virtual de la esquina izquierda del laboratorio (myColorD)
  roboticLabCam3.position.X=50.000000;
  roboticLabCam3.position.Y=100.000000;
  roboticLabCam3.position.Z=2955.000000;
  roboticLabCam3.position.H=1.;
  roboticLabCam3.foa.X=2160.000000;
  roboticLabCam3.foa.Y=1151.000000;
  roboticLabCam3.foa.Z=1550.000000;
  roboticLabCam3.foa.H=1.;
	roboticLabCam3.fdistx=405.399994;
	roboticLabCam3.fdisty=roboticLabCam3.fdistx;
	roboticLabCam3.skew=0.;
  roboticLabCam3.u0=142.600006;
  roboticLabCam3.v0=150.399994;
  roboticLabCam3.roll=2.956911;

	update_camera_matrix (&roboticLabCam0);
	update_camera_matrix (&roboticLabCam1);
	update_camera_matrix (&roboticLabCam2);
	update_camera_matrix (&roboticLabCam3);
	update_camera_matrix (&ceilLabCam);

  actualCameraView = 0; // empezamos con la cámara del user

	initCalibration (); // calibramos la cámara del robot
	setRobotCameraPos (); // con los parámetros de calibración, posicionamos nuestra robotCamera

  xcam = robotCamera.position.X;
  ycam = robotCamera.position.X;
  zcam = robotCamera.position.X;

	configFile = "./gradientPlanning.conf";
	createConfigFile ();

	RGB2HSV_init();
	RGB2HSV_createTable();

	// Inicializamos lo correspondiente al comportamiento de navegación
  contadorMemoria = 0;	
  result = &resultante;
  pdatos = &datos;
  hora = dameTiempo();
	num_puntos = 0;

  for(i=0;i<SIFNTSC_COLUMNS;i++)
	  validos[i]=FALSE;

  for(i=0;i<SIFNTSC_COLUMNS;i++)
      datos.horaInicio[i]=hora;

  datos.ultimo=0;
}

static int initFloorOGL (int w, int h) {
	glClearColor(0.f, 0.8f, 0.5f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glDisable(GL_DEPTH_TEST);
	glMatrixMode (GL_PROJECTION);
	glLoadIdentity ();
	glOrtho (0, w, h, 0, 0, 1);

	return 0;
}

static int initFronteraOGL(int w, int h)
 /* Inicializa OpenGL con los parametros que diran como se visualiza. */
{
	GLfloat ambient[] = {1.0, 1.0, 1.0, 1.0};
	GLfloat diffuse[] = {1.0, 1.0, 1.0, 1.0};
	GLfloat position[] = {0.0, 3.0, 3.0, 0.0};
	GLfloat lmodel_ambient[] = {0.2, 0.2, 0.2, 1.0};
	GLfloat local_view[] = {0.0};

	glViewport(0,0,(GLint)w,(GLint)h);  
	glDrawBuffer(GL_BACK);
	glClearColor(0.6f, 0.8f, 1.0f, 0.0f);
	glClearDepth(1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/* With this, the pioneer appears correctly, but the cubes don't */
	glLightfv (GL_LIGHT0, GL_AMBIENT, ambient);
	glLightfv (GL_LIGHT0, GL_DIFFUSE, diffuse);
	glLightfv (GL_LIGHT0, GL_POSITION, position);
	glLightModelfv (GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
	glLightModelfv (GL_LIGHT_MODEL_LOCAL_VIEWER, local_view);
	glEnable (GL_LIGHT0);
	/*glEnable (GL_LIGHTING);*/

	glEnable(GL_TEXTURE_2D);     /* Enable Texture Mapping */
	glEnable (GL_AUTO_NORMAL);
	glEnable (GL_NORMALIZE);  
	glEnable(GL_DEPTH_TEST);     /* Enables Depth Testing */
	glDepthFunc(GL_LESS);  
	glShadeModel(GL_SMOOTH);     /* Enables Smooth Color Shading */
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	return 0;
}

void initWorldCanvasSettings () {
  float r,lati,longi;

	if (!flashImage) {
	  if (foa_mode) {
	    longi=2*PI*mouse_on_fronteracanvas.x/360.;
	    lati=2*PI*mouse_on_fronteracanvas.y/360.;
	    r=500;
	    virtualcam2.position.X=radius*(virtualcam2.position.X)/radius_old;
	    virtualcam2.position.Y=radius*(virtualcam2.position.Y)/radius_old;
	    virtualcam2.position.Z=radius*(virtualcam2.position.Z)/radius_old;
	    if (centrado==0){
				//Si centrado = 0 se ha pulsado el boton de centrar. Si vale 1, no está pulsado.
				virtualcam2.foa.X=r*cos(lati)*cos(longi);
				virtualcam2.foa.Y=r*cos(lati)*sin(longi);
				virtualcam2.foa.Z=r*sin(lati);
			}
	  }
	  
	  if (cam_mode) {
			centrado = 0;

	    longi=2*PI*mouse_on_fronteracanvas.x/360.;
	    lati=2*PI*mouse_on_fronteracanvas.y/360.;
	    
	    virtualcam2.position.X=radius*cos(lati)*cos(longi);
	    virtualcam2.position.Y=radius*cos(lati)*sin(longi);
	    virtualcam2.position.Z=radius*sin(lati);
	    
//	    virtualcam2.foa.X=0;
//	    virtualcam2.foa.Y=0;
//	    virtualcam2.foa.Z=0;
	  }
		radius_old=radius;
	}

	initFronteraOGL(624,568);
	  
	/* Virtual camera */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity(); 

	/* perspective projection. intrinsic parameters + frustrum */
	gluPerspective(45.,(GLfloat)624/(GLfloat)568,1.0,50000.0);
	/* extrinsic parameters */
	if (actualCameraView == 0) // user camera view
		//gluLookAt(virtualcam2.position.X,ycam,zcam,foax,foay,foaz,0.,0.,1.);
	  gluLookAt(virtualcam2.position.X,virtualcam2.position.Y,virtualcam2.position.Z,
	          virtualcam2.foa.X,virtualcam2.foa.Y,virtualcam2.foa.Z,
	          0.,0.,1.);
	else if (actualCameraView == 1) // robotics lab camera 1
		gluLookAt(roboticLabCam0.position.X,roboticLabCam0.position.Y,roboticLabCam0.position.Z,roboticLabCam0.foa.X,roboticLabCam0.foa.Y,roboticLabCam0.foa.Z,0.,0.,1.);
	else if (actualCameraView == 2) // robotics lab camera 2
		gluLookAt(roboticLabCam1.position.X,roboticLabCam1.position.Y,roboticLabCam1.position.Z,roboticLabCam1.foa.X,roboticLabCam1.foa.Y,roboticLabCam1.foa.Z,0.,0.,1.);
	else if (actualCameraView == 3) // robotics lab camera 3
		gluLookAt(roboticLabCam2.position.X,roboticLabCam2.position.Y,roboticLabCam2.position.Z,roboticLabCam2.foa.X,roboticLabCam2.foa.Y,roboticLabCam2.foa.Z,0.,0.,1.);
	else if (actualCameraView == 4) // robotics lab camera 4
		gluLookAt(roboticLabCam3.position.X,roboticLabCam3.position.Y,roboticLabCam3.position.Z,roboticLabCam3.foa.X,roboticLabCam3.foa.Y,roboticLabCam3.foa.Z,0.,0.,1.);
	else if (actualCameraView == 5) // robotics lab ceil camera
		gluLookAt(myencoders[0],myencoders[1],12000,myencoders[0],myencoders[1],0.,0.,0.,1.);
}

void cannyFilter() {
	IplImage *src;
	IplImage *gray;
	IplImage *edge;
	IplImage *cedge;
	IplImage *contour;
	CvMemStorage* storage = cvCreateMemStorage(0);
	CvSeq* contours = 0;

	src = cvCreateImage(cvSize(SIFNTSC_COLUMNS, SIFNTSC_ROWS), IPL_DEPTH_8U, 3);
	cedge = cvCreateImage(cvSize(SIFNTSC_COLUMNS, SIFNTSC_ROWS), IPL_DEPTH_8U, 3);
	gray = cvCreateImage(cvSize(SIFNTSC_COLUMNS, SIFNTSC_ROWS), IPL_DEPTH_8U, 1);
	edge = cvCreateImage(cvSize(SIFNTSC_COLUMNS, SIFNTSC_ROWS), IPL_DEPTH_8U, 1);
	contour = cvCreateImage(cvSize(SIFNTSC_COLUMNS, SIFNTSC_ROWS), IPL_DEPTH_8U, 1);
	
	memcpy(src->imageData, image, src->width*src->height*src->nChannels);  
/*
	// hago filtrado normal, para luego usar en imageAfiltered (con la frontera)
	cvCvtColor(src, gray, CV_RGB2GRAY);
	cvCanny(gray, edge, threshold, threshold*3, 3);

	// Con colores:
 	cvZero(cedge);
	cvCopy(src, cedge, edge );

	// Sin colores:
	// cvCvtColor(edge, cedge, CV_GRAY2RGB);
*/
	cvCvtColor(src, gray, CV_RGB2GRAY);	
	cvCanny(gray, edge, threshold, threshold*3, 3);
 	cvZero(cedge);
	cvCopy(src, cedge, edge);

	memcpy(imageAbordered->image, cedge->imageData, src->width*src->height*src->nChannels);

	// ahora detecto contornos y meto el contenido en imageAbordered, la que se muestra en pantalla
	cvThreshold(gray, contour, threshold, 255, CV_THRESH_BINARY);
	cvFindContours(contour, storage, &contours, sizeof(CvContour),CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE,cvPoint(0,0));

	if(contours) {  
		cvCvtColor (contour, cedge, CV_GRAY2RGB);  
		contours = cvApproxPoly(contours, sizeof(CvContour), storage, CV_POLY_APPROX_DP, 3, 1);  
		cvDrawContours(cedge, contours, CV_RGB(255,0,0), CV_RGB(0,255,0), 2, 2, CV_AA, cvPoint(0,0));
	}  

	memcpy(contourImage, cedge->imageData, src->width*src->height*src->nChannels);  

	cvReleaseImage(&src);
	cvReleaseImage(&gray);
	cvReleaseImage(&edge);
	cvReleaseImage(&cedge);
}

int cumpleColorFrontera (float myR, float myG, float myB) {
	int seCumple = FALSE;

	myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

/*	if (((myHSV->H*DEGTORAD > 5.83) && (myHSV->H*DEGTORAD < 6.28) ||
		(myHSV->H > 0.) && (myHSV->H < 0.6)) &&
		(myHSV->S > 0.3) && (myHSV->S < 0.81) &&
		(myHSV->V > 0.) && (myHSV->V < 255.)) { // si pasamos el filtro de rojo
		seCumple = TRUE;
	}
*/
	if ((myHSV->H*DEGTORAD > 0.8189) && (myHSV->H*DEGTORAD < 1.2189) &&
		(myHSV->S > 0.04) && (myHSV->S < 0.81) &&
		(myHSV->V > 205.1) && (myHSV->V < 255.)) { // o si pasamos el filtro de amarillo con ventanas cerrás
		seCumple = TRUE;
	}

	return (seCumple);
}

int cumpleColorObjetivo (float myR, float myG, float myB) {
	int seCumple = 0;

	myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

	if (((myHSV->H*DEGTORAD > 6.10) && (myHSV->H*DEGTORAD < 6.28) ||
		(myHSV->H > 0.) && (myHSV->H < 0.22)) &&
		(myHSV->S > 0.58) && (myHSV->S < 0.78) &&
		(myHSV->V > 202.) && (myHSV->V < 255.)) { // si pasamos el filtro de rojo
		seCumple = TRUE;
	}

	return (seCumple);
}

int cumpleColorRotonda (float myR, float myG, float myB) {
	int seCumple = 0;

	myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

	if ((myHSV->H*DEGTORAD > 3.47) && (myHSV->H*DEGTORAD < 3.87) &&
		(myHSV->S > 0.56) && (myHSV->S < 0.76) &&
		(myHSV->V > 198.) && (myHSV->V < 255.)) { // si pasamos el filtro de azul
		seCumple = 1;
	}

	return (seCumple);
}

void drawFronteraImage () {
  int i, j, pos, c, row;//, pos, columnPoint, rowPoint, posPoint;

  //double H, S, V;
  float myR = 0., myG = 0., myB = 0.;

	int esFrontera;
	//int v1, v2, v3, v4, v5, v6, v7, v8;

	for(i=0; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS; i++) { // limpiamos imageAfiltered de residuos previos
		c=i%SIFNTSC_COLUMNS;
		row=i/SIFNTSC_COLUMNS;
		j=row*SIFNTSC_COLUMNS+c;

		if (image!= NULL) {
		  imageAfiltered->image[i*3]=0.;
		  imageAfiltered->image[i*3+1]=0.;
		  imageAfiltered->image[i*3+2]=0.;
		}
	}

	counterColumnsRight = 0;
	counterColumnsLeft = 0;
	counterColumnsCenter = 0;

	j = 0;

	// Frontier image
	while ((j<SIFNTSC_COLUMNS) && (!seCumpleColorObjetivo)) { // recorrido en columnas
		i = SIFNTSC_ROWS-1;
		esFrontera = FALSE;
		while ((i>=0) && (esFrontera == FALSE) && (!seCumpleColorObjetivo)) { // recorrido en filas
			if (image!=NULL) {
			  pos = i*SIFNTSC_COLUMNS+j; // posicion actual

				myR = (float)(unsigned int)(unsigned char)imageAbordered->image[pos*3+2];
				myG = (float)(unsigned int)(unsigned char)imageAbordered->image[pos*3+1];
				myB = (float)(unsigned int)(unsigned char)imageAbordered->image[pos*3+0];

				seCumpleColorObjetivo = cumpleColorObjetivo(myB, myG, myR); // si soy el objetivo me olvido del resto
				seCumpleColorRotonda = cumpleColorRotonda(myB, myG, myR);

				if ((!((myR <= 0.) && (myG <= 0.) && (myB <= 0.))) && (cumpleColorFrontera(myB, myG, myR))) {// si no soy negro -> es que soy frontera

					if (j < NUMCOLUMNSLEFT) // estamos en el lado izquierdo
						counterColumnsLeft ++;

					else if ((j >= NUMCOLUMNSLEFT) && (j <= NUMCOLUMNSRIGHT))
						counterColumnsCenter ++;

					else if (j > NUMCOLUMNSRIGHT)
						counterColumnsRight ++;

					esFrontera = TRUE;

					imageAfiltered->image[pos*3]=imageAbordered->image[pos*3+2];
					imageAfiltered->image[pos*3+1]=imageAbordered->image[pos*3+1];
					imageAfiltered->image[pos*3+2]=imageAbordered->image[pos*3+0];

					myActualPointImage.x = i;
					myActualPointImage.y = j;
					pointProjection (myActualPointImage, mycolorA, 0);
				} else {
					esFrontera = FALSE;
					imageAfiltered->image[pos*3]=0.;
					imageAfiltered->image[pos*3+1]=0.;
					imageAfiltered->image[pos*3+2]=0.;
				}

				if (primerPunto == FALSE) {
					miPrimerPunto.x = 0;
					miPrimerPunto.y = 0;
					pointProjection (miPrimerPunto, mycolorA, 1);
				} else if (segundoPunto == FALSE) {
					miSegundoPunto.x = 0;
					miSegundoPunto.y = ANCHO_IMAGEN - 1;
					pointProjection (miSegundoPunto, mycolorA, 1);
				} else if (tercerPunto == FALSE) {
					miTercerPunto.x = LARGO_IMAGEN - 1;
					miTercerPunto.y = ANCHO_IMAGEN - 1;
					pointProjection (miTercerPunto, mycolorA, 1);
				} else if (cuartoPunto == FALSE) {
					miCuartoPunto.x = LARGO_IMAGEN - 1;
					miCuartoPunto.y = 0;
					pointProjection (miCuartoPunto, mycolorA, 1);
				}

			} // fin if myColorX != NULL
			i --;
		} // fin while (filas)
		j ++;
 	} // fin for (columnas)
}

void drawGroundPoints () {
	int i;

	glColor3f(0.0f, 0.0f, 1.0f);
	glBegin (GL_POINTS);
		for (i = 0; i < SIFNTSC_COLUMNS*SIFNTSC_ROWS*4; i ++) {
			glVertex3f (groundPoints [i].X, groundPoints [i].Y, groundPoints [i].Z);
		}
	glEnd ();
}

void drawFronteraPoints (int mode) {
	int i;

	glColor3f(0.0f, 0.0f, 1.0f);
	glBegin (GL_POINTS);
		for (i = 0; i < SIFNTSC_COLUMNS; i ++) {
			if (mode == 0) {
				//printf ("%f, %f, %f\n", fronteraPoints [i].X, fronteraPoints [i].Y, fronteraPoints [i].Z);
				glVertex3f (fronteraPoints [i].X, fronteraPoints [i].Y, fronteraPoints [i].Z);
			}
			else if (mode == 1)
				glVertex2f (fronteraPoints [i].Y, fronteraPoints [i].X);

		}
	glEnd ();
}

static gboolean expose_event (GtkWidget *widget, GdkEventExpose *event, gpointer data) {
	GdkGLContext *glcontext;
	GdkGLDrawable *gldrawable;
	static pthread_mutex_t gl_mutex;
	float dxPioneer, dyPioneer, dzPioneer, longiPioneer, latiPioneer, rPioneer;

	pthread_mutex_lock(&gl_mutex);

	glcontext = gtk_widget_get_gl_context (widget);
	gldrawable = gtk_widget_get_gl_drawable (widget);

	if (!gdk_gl_drawable_gl_begin (gldrawable, glcontext)){
	  pthread_mutex_unlock(&gl_mutex);
	  return FALSE;
	}

	if (worldView == TRUE) {
		initWorldCanvasSettings ();

	  /** Robot Frame of Reference **/
	  glMatrixMode(GL_MODELVIEW);
	  glLoadIdentity();
	  if (myencoders!=NULL){
	     mypioneer.posx=myencoders[0];
	     mypioneer.posy=myencoders[1];
	     mypioneer.posz=0.;
	     mypioneer.foax=myencoders[0];
	     mypioneer.foay=myencoders[1];
	     mypioneer.foaz=10.;
	     mypioneer.roll=myencoders[2]*RADTODEG;
	  }
	  else{
	     mypioneer.posx=0.;
	     mypioneer.posy=0.;
	     mypioneer.posz=0.;
	     mypioneer.foax=0.;
	     mypioneer.foay=0.;
	     mypioneer.foaz=10.;
	     mypioneer.roll=0.;
	  }
	  glTranslatef(mypioneer.posx,mypioneer.posy,mypioneer.posz);
	  dxPioneer = (mypioneer.foax-mypioneer.posx);
	  dyPioneer = (mypioneer.foay-mypioneer.posy);
	  dzPioneer = (mypioneer.foaz-mypioneer.posz);
	  longiPioneer = (float)atan2(dyPioneer,dxPioneer)*360./(2.*PI);
	  glRotatef (longiPioneer,0.,0.,1.);
	  rPioneer = sqrt(dxPioneer*dxPioneer+dyPioneer*dyPioneer+dzPioneer*dzPioneer);
	  if (rPioneer<0.00001) latiPioneer=0.;
	  else latiPioneer=acos(dzPioneer/rPioneer)*360./(2.*PI);
	  glRotatef(latiPioneer,0.,1.,0.);
	  glRotatef(mypioneer.roll,0.,0.,1.);

		glEnable (GL_LIGHTING); // LUCES Y.... ACCION!!
		glPushMatrix();
		glTranslatef(1.,0.,0.);
		/* the body it is not centered. With this translation we center it */
		glScalef (100., 100., 100.);
		loadModel();
		glPopMatrix();
		glDisable (GL_LIGHTING); // FUERA LUCES, VOLVEMOS A LA VIDA REAL

		glLoadIdentity ();
		drawFloorInfiniteLines ();
		drawMyAxes ();
		//drawRoboticLabGround ();
		//drawRoboticLabPlanes ();
		drawRoboticLabLines ();		
		drawHallLines ();
		glLoadIdentity ();
		drawCam (&roboticLabCam0, 2); // dibujamos la cámara virtual de visualización de imagen
		drawCam (&roboticLabCam1, 3); // dibujamos la cámara virtual de visualización de imagen
		drawCam (&roboticLabCam2, 4); // dibujamos la cámara virtual de visualización de imagen
		drawCam (&roboticLabCam3, 5); // dibujamos la cámara virtual de visualización de imagen
		glLoadIdentity ();
		drawFronteraPoints (0); // sólo vemos los puntos frontera

		drawForces ();
	} else if (floorView == TRUE) {
		initFloorOGL (472, 568);
	  glLineWidth(10.0f);
		glMatrixMode (GL_MODELVIEW);
		drawFronteraPoints (1); // sólo vemos los puntos frontera
	}

	if (gdk_gl_drawable_is_double_buffered (gldrawable)){
	  gdk_gl_drawable_swap_buffers (gldrawable);
	}
	else{
	  glFlush ();
	}

	gdk_gl_drawable_gl_end (gldrawable);

	pthread_mutex_unlock(&gl_mutex);
	return TRUE;
}

void frontera_guidisplay()
{
	int i, j, c, row;

	drawFronteraImage();

	for(i=0; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS; i++) {
		c=i%SIFNTSC_COLUMNS;
		row=i/SIFNTSC_COLUMNS;
		j=row*SIFNTSC_COLUMNS+c;

		if (mycolorA!= NULL) {
		  image[i*3]=(*mycolorA)[j*3+2];
		  image[i*3+1]=(*mycolorA)[j*3+1];
		  image[i*3+2]=(*mycolorA)[j*3];
		}
	}

	cannyFilter ();

	prepare2draw(imageAfiltered, filteredImageRGB);
	prepare2draw(imageA, originalImageRGB);
	//prepare2draw(imageAbordered, borderImageRGB);

	gdk_threads_enter();
		if (worldView == TRUE) if (fronteraCanvas!=NULL) expose_event(fronteraCanvas, NULL, NULL);
		if (floorView == TRUE) if (floorCanvas!=NULL) expose_event(floorCanvas, NULL, NULL);
	gdk_threads_leave();

	gdk_threads_enter();
		gtk_widget_queue_draw(GTK_WIDGET(win));
	gdk_threads_leave();
}

void frontera_hide() {
  if (win!=NULL) {
      gdk_threads_enter();
      gtk_widget_hide(win);
      gdk_threads_leave();
	}
  mydelete_displaycallback(frontera_guidisplay);
	loadedgui=0;
	all[frontera_id].guistate=off;
}

void frontera_show()
{
	static pthread_mutex_t frontera_gui_mutex;
	GtkButton *floorButton, *worldButton, *go1Button, *go50Button, *turn45RButton, *turn45LButton, *turn90RButton, *turn90LButton, *flashButton;
	GtkToggleButton *camera1Button, *camera2Button, *camera3Button, *camera4Button, *ceilCameraButton, *userCameraButton;
	GtkWidget *widget1;

	pthread_mutex_lock(&frontera_gui_mutex);
	if (!loadedgui){
		loadglade ld_fn;
		loadedgui=1;
		pthread_mutex_unlock(&frontera_gui_mutex);

		/*Load the window from the .glade xml file*/
		gdk_threads_enter();  
		if ((ld_fn=(loadglade)myimport("graphics_gtk","load_glade"))==NULL){
		    fprintf (stderr,"I can't fetch 'load_glade' from 'graphics_gtk'.\n");
		    jdeshutdown(1);
		}

		xml = ld_fn ("frontera.glade");
		if (xml==NULL){
		    fprintf(stderr, "Error loading graphical frontera on xml\n");
		    jdeshutdown(1);
		}

		// Set OpenGL Parameters
		GdkGLConfig *glconfig;
		/* Try double-buffered visual */
		glconfig = gdk_gl_config_new_by_mode ((GdkGLConfigMode)(GDK_GL_MODE_RGB |	GDK_GL_MODE_DEPTH |	GDK_GL_MODE_DOUBLE));
		if (glconfig == NULL)  {
			g_print ("*** Cannot find the double-buffered visual.\n");
			g_print ("*** Trying single-buffered visual.\n");

			/* Try single-buffered visual */
			glconfig = gdk_gl_config_new_by_mode ((GdkGLConfigMode)(GDK_GL_MODE_RGB |	GDK_GL_MODE_DEPTH));
			if (glconfig == NULL) {
				g_print ("*** No appropriate OpenGL-capable visual found.\n");
				jdeshutdown(1);
			}
		}

		fronteraCanvas = glade_xml_get_widget(xml, "fronteraCanvas");
		floorCanvas = glade_xml_get_widget(xml, "floorCanvas");

		gtk_widget_unrealize(fronteraCanvas);
		gtk_widget_unrealize(floorCanvas);
		if (fronteraCanvas == NULL) printf ("Frontera Canvas is NULL\n");
		if (floorCanvas == NULL) printf ("Floor Canvas is NULL\n");

		// Set OpenGL-capability to the widget
		if (gtk_widget_set_gl_capability (fronteraCanvas,	glconfig,	NULL,	TRUE,	GDK_GL_RGBA_TYPE)==FALSE) {
			printf ("No Gl capability on fronteraCanvas\n");
			jdeshutdown(1);
		}
		if (gtk_widget_set_gl_capability (floorCanvas,	glconfig,	NULL,	TRUE,	GDK_GL_RGBA_TYPE)==FALSE) {
			printf ("No Gl capability on floorCanvas\n");
			jdeshutdown(1);
		}

		if (whatWorld == 0) {
			gtk_widget_realize(fronteraCanvas);
			whatWorld = 1;
		}	else if (whatWorld == 1) {
			gtk_widget_realize(floorCanvas);
			whatWorld = 0;
		}

		gtk_widget_set_child_visible (GTK_WIDGET(fronteraCanvas), TRUE);
		gtk_widget_set_child_visible (GTK_WIDGET(floorCanvas), TRUE);
		//gtk_widget_set_child_visible (GTK_WIDGET(glade_xml_get_widget(xml, "vbox1")), TRUE);

		gtk_widget_add_events ( fronteraCanvas,
		                        GDK_BUTTON1_MOTION_MASK    |
		                        GDK_BUTTON2_MOTION_MASK    |
		                        GDK_BUTTON3_MOTION_MASK    |
		                        GDK_BUTTON_PRESS_MASK      |
		                        GDK_BUTTON_RELEASE_MASK    |
		                        GDK_VISIBILITY_NOTIFY_MASK);	

		widget1=(GtkWidget *)glade_xml_get_widget(xml, "fronteraCanvas");

/*		g_signal_connect (G_OBJECT (widget1), "motion_notify_event", G_CALLBACK (opengl_camara_controller_motion), NULL);
		g_signal_connect (G_OBJECT (widget1), "scroll-event", G_CALLBACK (opengl_camara_controller_zoom), NULL);*/

//		g_signal_connect_after (G_OBJECT (widget1), "realize", G_CALLBACK (realize), NULL);
//		g_signal_connect (G_OBJECT (widget1), "configure_event", G_CALLBACK (configure_event), NULL);
		g_signal_connect (G_OBJECT (widget1), "button_press_event", G_CALLBACK (button_press_event), NULL);
		g_signal_connect (G_OBJECT (widget1), "motion_notify_event", G_CALLBACK (motion_notify_event), NULL);
		g_signal_connect (G_OBJECT (widget1), "scroll-event", G_CALLBACK (scroll_event), fronteraCanvas);

		// CONNECT CALLBACKS
		win = glade_xml_get_widget(xml, "window1");
		glade_xml_signal_autoconnect (xml); // Conectar los callbacks

		// OpenGl worlds control buttons:
		floorButton = (GtkToggleButton *)glade_xml_get_widget(xml, "floorButton");
		g_signal_connect (G_OBJECT (floorButton), "pressed", G_CALLBACK (floorButton_pressed), NULL);
		g_signal_connect (G_OBJECT (floorButton), "released", G_CALLBACK (floorButton_released), NULL);

		worldButton = (GtkToggleButton *)glade_xml_get_widget(xml, "worldButton");
		g_signal_connect (G_OBJECT (worldButton), "pressed", G_CALLBACK (worldButton_pressed), NULL);
		g_signal_connect (G_OBJECT (worldButton), "released", G_CALLBACK (worldButton_released), NULL);

		// Movement control buttons:
		go1Button = (GtkButton *)glade_xml_get_widget(xml, "go1Button");
		g_signal_connect (G_OBJECT (go1Button), "pressed", G_CALLBACK (go1Button_pressed), NULL);
		g_signal_connect (G_OBJECT (go1Button), "released", G_CALLBACK (go1Button_released), NULL);

		go50Button = (GtkButton *)glade_xml_get_widget(xml, "go50Button");
		g_signal_connect (G_OBJECT (go50Button), "pressed", G_CALLBACK (go50Button_pressed), NULL);
		g_signal_connect (G_OBJECT (go50Button), "released", G_CALLBACK (go50Button_released), NULL);

		turn45RButton = (GtkButton *)glade_xml_get_widget(xml, "turn45RButton");
		g_signal_connect (G_OBJECT (turn45RButton), "pressed", G_CALLBACK (turn45RButton_pressed), NULL);
		g_signal_connect (G_OBJECT (turn45RButton), "released", G_CALLBACK (turn45RButton_released), NULL);

		turn45LButton = (GtkButton *)glade_xml_get_widget(xml, "turn45LButton");
		g_signal_connect (G_OBJECT (turn45LButton), "pressed", G_CALLBACK (turn45LButton_pressed), NULL);
		g_signal_connect (G_OBJECT (turn45LButton), "released", G_CALLBACK (turn45LButton_released), NULL);

		turn90RButton = (GtkButton *)glade_xml_get_widget(xml, "turn90RButton");
		g_signal_connect (G_OBJECT (turn90RButton), "pressed", G_CALLBACK (turn90RButton_pressed), NULL);
		g_signal_connect (G_OBJECT (turn90RButton), "released", G_CALLBACK (turn90RButton_released), NULL);

		turn90LButton = (GtkButton *)glade_xml_get_widget(xml, "turn90LButton");
		g_signal_connect (G_OBJECT (turn90LButton), "pressed", G_CALLBACK (turn90LButton_pressed), NULL);
		g_signal_connect (G_OBJECT (turn90LButton), "released", G_CALLBACK (turn90LButton_released), NULL);

		flashButton = (GtkButton *)glade_xml_get_widget(xml, "flashButton");
		g_signal_connect (G_OBJECT (flashButton), "pressed", G_CALLBACK (flashButton_pressed), NULL);
		g_signal_connect (G_OBJECT (flashButton), "released", G_CALLBACK (flashButton_released), NULL);

		camera1Button = (GtkToggleButton *)glade_xml_get_widget(xml, "camera1Button");
		g_signal_connect (G_OBJECT (camera1Button), "pressed", G_CALLBACK (camera1Button_pressed), NULL);
		g_signal_connect (G_OBJECT (camera1Button), "released", G_CALLBACK (camera1Button_released), NULL);

		camera2Button = (GtkToggleButton *)glade_xml_get_widget(xml, "camera2Button");
		g_signal_connect (G_OBJECT (camera2Button), "pressed", G_CALLBACK (camera2Button_pressed), NULL);
		g_signal_connect (G_OBJECT (camera2Button), "released", G_CALLBACK (camera2Button_released), NULL);

		camera3Button = (GtkToggleButton *)glade_xml_get_widget(xml, "camera3Button");
		g_signal_connect (G_OBJECT (camera3Button), "pressed", G_CALLBACK (camera3Button_pressed), NULL);
		g_signal_connect (G_OBJECT (camera3Button), "released", G_CALLBACK (camera3Button_released), NULL);

		camera4Button = (GtkToggleButton *)glade_xml_get_widget(xml, "camera4Button");
		g_signal_connect (G_OBJECT (camera4Button), "pressed", G_CALLBACK (camera4Button_pressed), NULL);
		g_signal_connect (G_OBJECT (camera4Button), "released", G_CALLBACK (camera4Button_released), NULL);

		ceilCameraButton = (GtkToggleButton *)glade_xml_get_widget(xml, "ceilCameraButton");
		g_signal_connect (G_OBJECT (ceilCameraButton), "pressed", G_CALLBACK (ceilCameraButton_pressed), NULL);
		g_signal_connect (G_OBJECT (ceilCameraButton), "released", G_CALLBACK (ceilCameraButton_released), NULL);

		userCameraButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
		g_signal_connect (G_OBJECT (userCameraButton), "pressed", G_CALLBACK (userCameraButton_pressed), NULL);
		g_signal_connect (G_OBJECT (userCameraButton), "released", G_CALLBACK (userCameraButton_released), NULL);

		if (win==NULL){
			fprintf(stderr, "Error loading graphic interface\n");
			jdeshutdown(1);
		} else {
			gtk_widget_show(win);
			gtk_widget_queue_draw(GTK_WIDGET(win));
		}
		gdk_threads_leave();
	} else {
		pthread_mutex_unlock(&frontera_gui_mutex);
		gdk_threads_enter();
		gtk_widget_show(win);
		gtk_widget_queue_draw(GTK_WIDGET(win));
		gdk_threads_leave();
	}

	gdk_threads_enter();
	// Initialization of the image buffers
	myregister_displaycallback(frontera_guidisplay);

	GdkPixbuf *originalImageBuf, *filteredImageBuf, *borderImageBuf;

	originalImage = GTK_IMAGE(glade_xml_get_widget(xml, "originalImage"));
	filteredImage = GTK_IMAGE(glade_xml_get_widget(xml, "filteredImage"));
	borderImage = GTK_IMAGE(glade_xml_get_widget(xml, "borderImage"));

	originalImageBuf = gdk_pixbuf_new_from_data(originalImageRGB->image,
					GDK_COLORSPACE_RGB,0,8,
					originalImageRGB->width,originalImageRGB->height,
					originalImageRGB->width*3,NULL,NULL);
	filteredImageBuf = gdk_pixbuf_new_from_data(filteredImageRGB->image,
					GDK_COLORSPACE_RGB,0,8,
					filteredImageRGB->width,filteredImageRGB->height,
					filteredImageRGB->width*3,NULL,NULL);
	borderImageBuf = gdk_pixbuf_new_from_data(contourImage,
					GDK_COLORSPACE_RGB,0,8,
					SIFNTSC_COLUMNS,SIFNTSC_ROWS,
					SIFNTSC_COLUMNS*3,NULL,NULL);

	gtk_image_set_from_pixbuf(originalImage, originalImageBuf);
	gtk_image_set_from_pixbuf(filteredImage, filteredImageBuf);
	gtk_image_set_from_pixbuf(borderImage, borderImageBuf);

	int a = 0;
	//glutInit(&a, NULL);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	gdk_threads_leave();
}
