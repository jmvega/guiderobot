/** Header file generated with fdesign on Thu Mar 26 23:57:51 2009.**/

#ifndef FD_localNavigationgui_h_
#define FD_localNavigationgui_h_

/** Callbacks, globals and object handlers **/


/**** Forms and Objects ****/
typedef struct {
	FL_FORM *localNavigationgui;
	void *vdata;
	char *cdata;
	long  ldata;
	FL_OBJECT *trailButton;
	FL_OBJECT *exit;
	FL_OBJECT *micanvas;
	FL_OBJECT *stop;
	FL_OBJECT *center;
	FL_OBJECT *track_robot;
	FL_OBJECT *escala;
	FL_OBJECT *laserButton;
	FL_OBJECT *memoryButton;
	FL_OBJECT *secWindowButton;
	FL_OBJECT *forcesButton;
} FD_localNavigationgui;

extern FD_localNavigationgui * create_form_localNavigationgui(void);

#endif /* FD_localNavigationgui_h_ */
