#include <GL/gl.h>              
#include <GL/glx.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <GL/freeglut_std.h>
#include <forms.h>
#include <glcanvas.h>

#include <glade/glade.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gtk/gtkgl.h>
#include <gtkextra/gtkextra.h>

#include <jde.h>
#include <pioneer.h>
#include <graphics_gtk.h>
#include <progeo.h>
#include <colorspaces.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_multifit.h>

#include <opencv/cv.h>

#define ROOM_MAX_X 7925.
#define v3f glVertex3f
#define PI 3.141592654

#define MAX_RADIUS_VALUE 10000000000
#define MIN_RADIUS_VALUE 0
#define WHEEL_DELTA 10

#define ANCHO_IMAGEN 320
#define LARGO_IMAGEN 240
#define MAXWORLD 20000.

#define K 3000*3000                   /* Constantes de repulsión*/
#define K2 4000*4000					/*ANTES 3000*3000 */
#define K3 4000*4000

#define moduloAtractiva 500          /* Modulo de la fuerza atractiva */

#define alcanceMuyPeligroso 1000*1000
#define alcancePeligroso 2000*2000
#define alcance 5000*5000             /* Distancia a la que las fuerzas repulsivas empiezan a actuar sobre nosotros */
#define alcanceLateral 2000*2000 
#define seguridad 500*500             /* a partir de esta distancia giramos sin avanzar*/
#define seguridadLateral 200*200      /* a partir de esta distancia lateral giramos sin avanzar*/

#define alpha 0.6f                    /* Proporcion de la fuerza atractiva en la fuerza resultante */
#define beta  0.4f                    /* Proporcion de la fuerza repulsiva en la fuerza resultante */
#define vMax 200
#define wPosMax 30
#define wNegMax -30
#define limiteTrayectoriaSimilar 10
#define limiteTrayectoriaDesigual 80

#define umbralPixelesMAX 10
#define umbralPixelesMIN 5
#define NUMCOLUMNSLEFT 100
#define NUMCOLUMNSRIGHT 221


extern void frontera_init();
extern void frontera_stop();
extern void frontera_run(int father, int *brothers, arbitration fn);
extern int frontera_cycle;

typedef struct SoRtype{
  struct SoRtype *father;
  float posx;
  float posy;
  float posz;
  float foax;
  float foay;
  float foaz;
  float roll;
} SofReference;

struct image_struct {
	int width;
	int height;
	int bpp;	// bytes per pixel
	char *image;
};

typedef struct {
	int   moduloFuerzaAtractiva;
	float direccionFuerzaAtractiva[2];
	float anguloFuerzaAtractiva;
	int   moduloFuerzaRepulsiva;
	int   direccionFuerzaRepulsiva[2];
	float anguloFuerzaRepulsiva;
	int   moduloFuerzaResultante;
	float direccionFuerzaResultante[2];
	float anguloFuerzaResultante;
	float anguloQueLlevo; 
	int   avanzaEnGiro; /* Sera 1 siempre k la distancia sea > seguridad; si la distancia leida es < seguridad, giraremos sin avanzar */
} TdatosResultante;

typedef struct datosMemoria {
	int horaInicio[SIFNTSC_COLUMNS];
	int masViejo;
	int ultimo;
} TdatosMemoria;

