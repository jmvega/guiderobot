#include "jde.h"
#include <forms.h>
#include "graphics_xforms.h"
#include "localNavigationgui.h"
#include "pioneer.h"
#include "gridslib.h"
#include "fuzzylib.h"

#define VFF_SIGMA_THETA 30
#define VFF_SIGMA_RADIO_MAX 5000
#define VFF_SIGMA_RADIO_MIN 1000
#define MOUSELEFT 1
#define MOUSEMIDDLE 2
#define MOUSERIGHT 3
#define MOUSEWHEELUP 4
#define MOUSEWHEELDOWN 5
#define LIMITE_LLEGADA_OBJETIVO 40

extern float xAtr;
extern float yAtr;
extern short distanciaObjetivo;/* 0 si llega al objetivo */

extern void localNavigation_startup();
extern void localNavigation_suspend();
extern void localNavigation_resume(int father, int *brothers, arbitration fn);
extern void localNavigation_guiresume();
extern void localNavigation_guisuspend();
