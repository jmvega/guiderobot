#include "localNavigation.h"

/* Gui callbacks */
registerbuttons myregister_buttonscallback;
registerdisplay myregister_displaycallback;
deletebuttons mydelete_buttonscallback;
deletedisplay mydelete_displaycallback;

Display *display;
int *myscreen;

int localNavigation_id=0; 
int localNavigation_brothers[MAX_SCHEMAS];
arbitration localNavigation_callforarbitration;
int localNavigation_cycle=100; /* ms */

int *mylaser = NULL;
resumeFn laserresume;
suspendFn lasersuspend;

float *myencoders = NULL;
resumeFn encodersresume;
suspendFn encoderssuspend;

float *myv = NULL;
float *myw = NULL;
resumeFn motorsresume;
suspendFn motorssuspend;

enum localNavigationBehaviors {vff, secWindow, forgetMemory, localGPP};
int localNavigationBehavior;

FD_localNavigationgui *fd_localNavigationgui;
GC localNavigationgui_gc;
Window  localNavigation_canvas_win;
unsigned long localNavigation_display_state;
int localNavigation_visual_refresh=FALSE;
int localNavigation_iteracion_display=0;
int localNavigation_canvas_mouse_button_pressed=0;
int localNavigation_mouse_button=0;
int localNavigation_robot_mouse_motion=0;
FL_Coord localNavigation_x_canvas,localNavigation_y_canvas,old_localNavigation_x_canvas,old_localNavigation_y_canvas;
float localNavigation_mouse_x, localNavigation_mouse_y;
int localNavigation_mouse_new=0;

/**** COORDINACION ENTRE ESQUEMAS ****/

pthread_mutex_t oklocalNavigation;

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* PARÁMETROS GRAFICOS */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
#define VFF_COLOR 1
#define SECWINDOW_COLOR 0
#define PUSHED 1
#define RELEASED 0 
#define FORCED_REFRESH 50000 /* ms */
static int forcedGradientRefresh = FALSE;
static int forcedOccupancyRefresh = FALSE; // podremos forzar el refresco según nuestras necesidades
/*Every forced_refresh the display is drawn from scratch. If it is too small it will cause flickering with grid display. No merece la pena una hebra de "display_lento" solo para repintar completamente la pantalla. */

float localNavigation_escala, localNavigation_width, localNavigation_height;
int localNavigation_trackrobot=FALSE;
float localNavigation_odometrico[5];
#define RANGO_MAX 40000. /* en mm */
#define RANGO_MIN 500. /* en mm */ 
#define RANGO_INICIAL 30000. /* en mm */
float localNavigation_rango=(float)RANGO_INICIAL; /* Rango de visualizacion en milimetros */

#define EGOMAX NUM_SONARS+5
XPoint localNavigation_ego[EGOMAX];
int numlocalNavigation_ego=0;
int visual_delete_localNavigation_ego=FALSE;

XPoint localNavigation_laser_dpy[NUM_LASER];
int visual_delete_localNavigation_laser=FALSE;

XPoint localNavigation_us_dpy[NUM_SONARS*2];
int visual_delete_localNavigation_us=FALSE;

#define joystick_maxRotVel 30 /* deg/sec */
#define joystick_maxTranVel 500 /* mm/sec */

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* CONSTANTES DE VISUALIZACION DE DISTINTOS RECURSOS GRAFICOS */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
unsigned long DISPLAY_ROBOT = 0x01UL;
unsigned long DISPLAY_SONARS = 0x04UL; // no se muestran, ya que no hacemos uso de ellos
unsigned long DISPLAY_LASER = 0x08UL;
unsigned long DISPLAY_MEMORY = 0x40UL;
unsigned long DISPLAY_ESTELA = 0x80UL;
unsigned long DISPLAY_VENTANA = 0x60UL;
unsigned long DISPLAY_VENTANAGPP = 0;//0x22UL;
unsigned long DISPLAY_OCCUPANCYGRID = 0;//0x26UL;
unsigned long DISPLAY_GRADIENTGRID = 0;//0x20UL;
unsigned long DISPLAY_FUERZAS = 0x24UL;
static int DEBUG = FALSE;

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* CONSTANTES UTILIZADAS PARA ALGORITMO VFF */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* Nota: todas las costantes relacionadas con las distancias o con el cálculo de las mismas, se establecen al cuadrado, porque en código siempre jugamos con distancias al cuadrado, para evitar así tener que sacar raices cuadradas (recordemos todos el fabuloso Teorema de Pitágoras: h*h = a*a + b*b; pues nosotros dejamos la h*h tal cual...) */
#define K 2000*2000                   /* Constantes de repulsión*/
#define K2 3000*3000
#define K3 4000*4000

#define moduloAtractiva 1000          /* Modulo de la fuerza atractiva */

#define alcanceMuyPeligroso 1000*1000
#define alcancePeligroso 2000*2000
#define alcance 5000*5000             /* Distancia a la que las fuerzas repulsivas empiezan a actuar sobre nosotros */
#define alcanceLateral 2000*2000 

#define seguridad 500*500             /* a partir de esta distancia giramos sin avanzar*/
#define seguridadLateral 200*200      /* a partir de esta distancia lateral giramos sin avanzar*/

#define alpha 0.7f                    /* Proporcion de la fuerza atractiva en la fuerza resultante */
#define beta  0.4f                    /* Proporcion de la fuerza repulsiva en la fuerza resultante */
#define vMax 400
#define wPosMax 30
#define wNegMax -30
#define limiteTrayectoriaSimilar 10
#define limiteTrayectoriaDesigual 80

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* PARÁMETROS DE VENTANA DE SEGURIDAD */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
#define ventanaY 850.00 /* 85 cm de longitud lado Y */
#define frenteY 950.00 /* 95 cm de frente */
#define ventanaX 220.00 /* en total tendrá una longitud X de 44 cm */
#define huecoX 370.00 /* en total tendrá una longitud X de 74 cm nuestra macro ventana */
float distanciasVentanaSeguridad [90]; /* tendremos guardados los 180 puntos de delimitación */
float distanciasHuecoSeguridad [90];
float distanciasFrenteSeguridad [90];
Tvoxel ventanaSeguridad [180];
Tvoxel huecoSeguridad [180];
Tvoxel frenteSeguridad [180];
int visual_delete_localNavigation_ventana = FALSE;
XPoint coord_graf_ventana[180];
XPoint coord_graf_hueco[180];
XPoint coord_graf_frente[180];

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* PARÁMETROS DE ALGORITMO GPP */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
// Posibles estados de las celdas de la rejilla
#define OCCUPIED -1
#define EMPTY -2
#define UNKNOWN -5

#define MAX_CASILLAS 12000
#define MAX_SEGMENTOS 1250
#define alcanceGPP 700
#define ENCAPILLA -3
#define OPTIMIZED -4
#define DISTANCIA_OBSTACULOS 97 /* distancia maxima a la que propaga el campo de los obstaculos */
#define PESO 90 /* peso inicial con el que propagan los obstaculos */
#define CORRECCION 2 /* este factor se aplica al frente de los obstaculos, 
		       de tal manera que "invierte" el campo haciendolo mayor 
		       cuanto mas cerca se está de uno */
#define ventanaGPPX 500.00 /* mm de longitud lado X */
#define ventanaGPPY 500.00 /* mm de longitud lado Y */
#define numPuntosX 25 /* 25 puntos en ventanaGPPX para dibujar gráficamente */
#define numPuntosY 25 /* 25 puntos en ventanaGPPY para dibujar gráficamente */
#define totalPuntos (4*numPuntosX + 4*numPuntosY) /* 4 porque tenemos 4 laterales en una ventana*/
#define MAX_TIME_ON_GRID 99999 * 1000000 /* tiempo máximo de permanencia de un punto en la rejilla (dado en microseg.) */

typedef struct {
  int fila;
  int columna;
  float estado;
} Tcasilla;

typedef struct nodo {
  float distancia;
  Tcasilla* casillas; /* cuidado con el tamaño, nos podemos quedar sin memoria */
  int casillasmax;
  struct nodo *siguiente;
} Tnodo;

typedef struct map {
	Tvoxel *point;
	int *state;
	unsigned long int *updatingTime;
} Tmap;

static int refreshCounter = 0;
#define REFRESH_TIMER 5 /* a cada REFRESH_TIMER iteraciones refrescaremos gráficamente las Grids */
#define INCREMENT 100 /* mm a los que vamos saltando en el recorrido de rayos para relleno de occupancyGrid */
#define FOLLOWGPP_COLOR 31
#define MAX_CHANGES_ON_GRID 999999 /* maximos cambios permitidos en el occupancyGrid para regenerar el gradiente */

pthread_mutex_t GPPdisplayMutex; // evitar conflictos freeGradientMemory vs. displayGradientGPP

Tmap tempMap; /* aquí guardamos el mapa en (t-1), para si se produce una reubicación en (t) podamos solapar los puntos cuyos estados conocemos previamente */
Tmap mapOnMoveTime; /* guardamos el mapa en el momento de generar el gradiente (t-1), e iremos comprobando si hay demasiadas novedades con respecto a la rejilla de ocupacion en tiempos sucesivos (t, t + 1, t + 2, ...) */
Tmap previousOccupancy;

Tnodo *lista; /* frente de onda*/

int kfrente = 0;
float menor = 1;
int destino_alcanzado = 0;
float dist_max = 0;
int destino_elegido = 0;
int origen_elegido = 1;
int obstaculos_propagados = 0;
int obstaculos_normalizados = 1;
int primera_vez = 0;			
int buscar = 0;
int ctrl = 0;
int fin_espera = 0;
int fin_plan = 0;
int en_objetivo = 0;

int objetivoGlobal = 0;
int isInitGrid = FALSE;

float origenpoint_x, origenpoint_y;
Tvoxel myLocalDestination, myGlobalDestination;
XPoint GraphicGlobalDestination[9];
float q_llevo;
int theta_robot;
int x_rob;
int y_rob;
int fil, col;

int myColor;

Tvoxel centrogrid;

float *aux; /*vector auxiliar en el k marcaremos las casillas que han sido alcanzadas por el frente de los obstaculos*/

int coste_optimo;
int coste_real;

Tgrid *gradientGrid = NULL; // grid que contiene información del gradiente
Tgrid *occupancyGrid = NULL; // grid que contiene información recibida por los lásers

char *configFile;

int follow_grid_cycle = 100; /* ms */
int follow_grid_controller; 
float dif_angle;
float v_actual,w_actual;
int estado;
float lento;
float anticipo;

int mensajeEsperaDestino = FALSE, /* booleanas para eventos de mensajes de información */
		mensajeEsperaOrigen = FALSE, 
		mensajeDestinoElegido = FALSE,
		mensajeNuevoGradiente = FALSE;
		showMemory = FALSE;
int visual_delete_localNavigation_grid = FALSE;
XRectangle mapaGrafico [26000]; // 6561 = 81 * 81 --> Representa al grid en nuestro canvas
XRectangle gradienteGrafico [26000]; // 6561 = 81 * 81 --> Representa al grid en nuestro canvas
int pendienteDeReubicacionGrafica = FALSE; // evento activado cuando se produce una reubicación del grid, para que gráficamentet se produzca su actualización correctamente
int movedInformation = FALSE; // evento activado cuando se produce una reubicación del grid, ya que hay que mover la información de estados del occupancyGrid -> gradientGrid

Tvoxel ventanaGPP [totalPuntos];
int visual_delete_localNavigation_ventanaGPP = FALSE;
XPoint coord_graf_ventanaGPP[totalPuntos];

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* PARÁMETROS DE MEMORIA DE PUNTOS */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
#define existenciaMemoria 3*1000000  /* Duracion de un punto en la memoria */
#define tamMemoria 400

int visual_delete_localNavigation_memory = FALSE;
XPoint coord_graf_memoria[tamMemoria], coord_graf_objetivo,graf_robot,graf_atr,graf_res,graf_rep;
int num_puntos;

typedef struct datosMemoria {
	int horaInicio[tamMemoria];
	int masViejo;
	int ultimo;
} TdatosMemoria;

typedef struct {
	int   moduloFuerzaAtractiva;
	float direccionFuerzaAtractiva[2];
	float anguloFuerzaAtractiva;
	int   moduloFuerzaRepulsiva;
	int   direccionFuerzaRepulsiva[2];
	float anguloFuerzaRepulsiva;
	int   moduloFuerzaResultante;
	float direccionFuerzaResultante[2];
	float anguloFuerzaResultante;
	float anguloQueLlevo; 
	int   avanzaEnGiro; /* Sera 1 siempre k la distancia sea > seguridad; si la distancia leida es < seguridad, giraremos sin avanzar */
} TdatosResultante;

Tvoxel memoria[tamMemoria];
int validos[tamMemoria];
TdatosMemoria datos;
TdatosMemoria *pdatos;
TdatosResultante resultante;
TdatosResultante *result;
short distanciaObjetivo;
float xRep,yRep,xAtr,yAtr,xRes,yRes;
float difAngle;
float vActual,wActual;
float peligro;
int olvido = TRUE;
Tvoxel vfftarget, oldvfftarget, myAttachForce, myRepForce, myResForce;
Tvoxel miObjetivo, fuerzaResultante, repulsiva, atractiva, robott;
Tvoxel localNavigationtarget,oldlocalNavigationtarget;
int contadorMemoria;

typedef struct {
	Tvoxel pos;
	int color;
} TEstelaRecorrido;

TEstelaRecorrido estelaRecorrido[tamMemoria/2];

XPoint coordGrafEstela[tamMemoria/2];
int posEstela = 0;
int numEstelas;

/**********************************************************************************/
/**********************************************************************************/
/****************************COMPORTAMIENTO VFF************************************/
/**********************************************************************************/
unsigned long int dameTiempo() {
	struct timeval t; 

	gettimeofday(&t,NULL);
	return t.tv_sec*1000000 + t.tv_usec;	
}

float transformoAngulo(float angulo) {
	/* Transformamos un angulo dado en radianes en grados de tal manera k si el
	angulo introducido es 180>thetha>0 se kda tal cual y si es 180<theta<0 kda
	como -thetha*/ 
	float salida, auxi, auxi1;

	angulo=angulo*RADTODEG; /* PASAMOS la entrada A GRADOS */
	auxi=angulo*100; /* Nos quedamos con 2 decimales */
	auxi1=ceil(auxi);
	angulo=(auxi1/100);

	if ((angulo>180)&&(angulo<=360)) {
		salida=angulo-(2*M_PI*RADTODEG);
		if (salida>=-0.1) salida=0.00;
	}	else salida=angulo;

	if (salida>=360) salida=0.00;

	auxi=salida*100;
	auxi1=ceil(auxi);
	salida=(auxi1/100);  /* Esta conversion la hacemos para quedarnos con 2 decimales */

	return salida;
}

int calculoFuerza (int distancia, int ang) {
  int fuerza;

  if (distancia!=0)
		if (distancia < alcancePeligroso)
			fuerza = K2/distancia; /* generamos MAS fuerza repulsiva; hay cierto peligro */
		else
			fuerza = K/distancia; /* generamos MENOS fuerza repulsiva; no se considera peligro */

  else fuerza=0;

  return fuerza;
}

int mismoAngulo(Tvoxel a,int distanciaPuntoMem,int anguloPuntoNuevo,int distanciaNueva) {
	int angulo_aux;
	int salida=FALSE;

	angulo_aux = (int)(atan2(a.y-myencoders[1],a.x-myencoders[0])*RADTODEG);
	salida =(anguloPuntoNuevo==angulo_aux);
	salida = ((anguloPuntoNuevo <= (angulo_aux + 0.7)) && (anguloPuntoNuevo >= (angulo_aux - 0.7)));

	return salida;
}

int reemplazoSimilares(Tvoxel a,Tvoxel b,int distanciaGuardado,int distanciaNuevo) {
	int radio;
	radio=10;

	return (((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y))<(radio*radio));
}

void olvidoTemporalPuntos() {
	int hora,i;

	hora=dameTiempo();

	for(i=0;i<tamMemoria;i++)
		if (hora-datos.horaInicio[i]>existenciaMemoria) validos[i]=FALSE;   
}

void rellenoEstela (int color) {
	Tvoxel posActualRobot;
	posActualRobot.x = myencoders[0];
	posActualRobot.y = myencoders[1];

	if (posEstela >= (tamMemoria/2)) posEstela = 0; /* reinicio */

	estelaRecorrido [posEstela].pos = posActualRobot;
	estelaRecorrido [posEstela].color = color;

	posEstela ++;
}

void rellenoMemoria (TdatosMemoria *d,int v[tamMemoria],Tvoxel m[tamMemoria]) {
	int i,j,colocado,masViejo;
	int hora;
	int angulo_nuevo;	
	Tvoxel punto;
	int distancia,distanciaPuntMem; 
	struct timeval a;

	for(i=0;i<NUM_LASER;i++) {
		colocado=FALSE; /* no tenemos ubicado el punto del laser actual */
		masViejo=0;
		j=(*d).ultimo; /* cogemos el indice del ultimo punto de memoria que se ha actualizado, para asi ir comparando (y sólo) con todos los que hay */
		gettimeofday(&a,NULL); /* ¿? */
		laser2xy(i,mylaser[i],&punto,&myencoders[0]); /* calculamos posicion de punto detectado por laser i */
		angulo_nuevo= (int)(atan2(punto.y-myencoders[1],punto.x-myencoders[0])*RADTODEG); /* angulo del punto vs. robot */
		distancia=((punto.x-myencoders[0])*(punto.x-myencoders[0])+(punto.y-myencoders[1])*(punto.y-myencoders[1]));
		hora=dameTiempo();

		while((!colocado)&&(j>=0)) { /* bucle para ubicar el punto detectado por laser */
			if(v[j]) { /* si hay punto valido */
				/* Si hay punto valido miro si en la memoria hay alguno en la misma proyeccion del laser o algun punto similar. Si se cumple alguna de las dos reemplazo en ese lugar,sino busco un hueco vacio y si no lo hay lo pongo donde el mas viejo */
				distanciaPuntMem=((m[j].x-myencoders[0])*(m[j].x-myencoders[0])+(m[j].y-myencoders[1])*(m[j].y-myencoders[1]));

				if((reemplazoSimilares(m[j],punto,distanciaPuntMem,distancia))) { /* si son puntos muy cercanos (similares) */
					m[j]=punto;
					(*d).horaInicio[j]=hora;
					(*d).ultimo=j;
					v[j]=TRUE;
					colocado=TRUE; /* salimos del bucle, ya esta ubicado */
				}	else if (mismoAngulo(m[j],distanciaPuntMem,angulo_nuevo,distancia)) { /* si son puntos de igual proyeccion */
					m[j]=punto;
					(*d).horaInicio[j]=hora;
					(*d).ultimo=j;
					v[j]=TRUE;
					colocado=TRUE; /* salimos del bucle, ya esta ubicado */
				}
			} /* fin if (v[j]) */
			
			if (v[masViejo]) { /* si la posicion del viejo es valida... */
				if (!v[j]) masViejo=j; /* ...y la posicion actual de j es NO VALIDA, entonces hemos encontrado un hueco */
				else if ((*d).horaInicio[j]<(*d).horaInicio[masViejo]) masViejo=j;
			} /* asi, cuando salgamos del bucle "por esta via" tendremos el hueco, siendo la casilla mas vieja */

			j--; /* vamos a comprobar con otro punto, hasta acabar con los existentes en memoria */
		} /* fin while((!colocado)&&(j>=0)) */

		if(!colocado) { /* si no hemos logrado ubicarlo */
			j=(*d).ultimo+1; /* ahora miramos en la otra parte (del ultimo "hacia la derecha") */
			while((!colocado)&&(j<tamMemoria)) {
				if(v[j]) {
				/* Si hay punto valido miro si en la memoria hay alguno en la misma proyeccion del laser o algun punto similar. Si se cumple alguna de las dos reemplazo en ese lugar,sino busco un hueco vacio y si no lo hay lo pongo donde el mas viejo */
					distanciaPuntMem=((m[j].x-myencoders[0])*(m[j].x-myencoders[0])+(m[j].y-myencoders[1])*(m[j].y-myencoders[1]));

					if((reemplazoSimilares(m[j],punto,distanciaPuntMem,distancia))) {
						m[j]=punto;
						(*d).horaInicio[j]=hora;
						(*d).ultimo=j;
						v[j]=TRUE;
						colocado=TRUE;
					} else if(mismoAngulo(m[j],distanciaPuntMem,angulo_nuevo,distancia)) {	  
						m[j]=punto;
						(*d).horaInicio[j]=hora;
						(*d).ultimo=j;
						v[j]=TRUE;
						colocado=TRUE;
					}
				} /* fin if (v[j]) */

				if (v[masViejo]) {
					if (!v[j]) masViejo=j;
					else if ((*d).horaInicio[j]<(*d).horaInicio[masViejo]) masViejo=j;
				}

				j++;
			} /* fin while((!colocado)&&(j<tamMemoria)) */
		} /* fin if(!colocado) */

		if (!colocado) { /* finalmente si no lo hemos ubicado, lo insertamos en la casilla mas vieja */
			m[masViejo]=punto;
			(*d).horaInicio[masViejo]=hora;
			(*d).ultimo=masViejo; /* ahora el ultimo sera lo que antes era el mas viejo... */
			v[masViejo]=TRUE;
		}
	} /* fin for(i=0;i<NUM_LASER;i++) */
}

void guardarCoordObjetivo(TdatosResultante *res) {
	float auxi1;
	double radianes;

	(*res).direccionFuerzaAtractiva[0]=miObjetivo.x-myencoders[0];
	(*res).direccionFuerzaAtractiva[1]=miObjetivo.y-myencoders[1];

	(*res).anguloFuerzaAtractiva=atan2((*res).direccionFuerzaAtractiva[1],(*res).direccionFuerzaAtractiva[0]);
		
	(*res).moduloFuerzaAtractiva=moduloAtractiva;

	radianes=(*res).anguloFuerzaAtractiva;

	(*res).direccionFuerzaAtractiva[0]=cos(radianes)*(*res).moduloFuerzaAtractiva;
	(*res).direccionFuerzaAtractiva[1]=sin(radianes)*(*res).moduloFuerzaAtractiva;

	auxi1=transformoAngulo((*res).anguloFuerzaAtractiva);
	(*res).anguloFuerzaAtractiva=auxi1;
}

void calculaResultante(Tvoxel m[tamMemoria],int v[tamMemoria],TdatosResultante *res) {
	int angulo,ang_robot,diff_ang;
	int sumaX, sumaY, i;
	int distancia,modulo;
	float auxi, auxi1;
	double radianes;

	sumaX=0;
	sumaY=0;

	for (i=0; i<tamMemoria; i++) {
		if (v[i]) {
			if (distancia!=0) {
				/* calculo la distancia al robot y el angulo respecto a este */
				distancia=(((m[i].x-myencoders[0])*(m[i].x-myencoders[0]))+((m[i].y-myencoders[1])*(m[i].y-myencoders[1])));
				radianes=atan2((m[i].y-myencoders[1]),(m[i].x-myencoders[0]));
				angulo=(int) (radianes*RADTODEG);
				ang_robot=(int)(myencoders[2]*RADTODEG);

				/* SUTILEZA PARA QUE EL ROBOT TENGA QUE GIRAR LO MENOS POSIBLE */
				if(ang_robot>=180) ang_robot=-360+ang_robot; /* =-(360-ang_robot)-->pasa a negativo */

				diff_ang=abs(angulo-ang_robot); /* calculamos el total que debería girar el robot */

				if (diff_ang>=180) diff_ang=360-diff_ang; /* mejor si gira por el camino más corto */
				/* FIN DE LA SUTILEZA */

				/* Miramos si el punto esta dentro del alcance en el que los puntos generan fuerza de repulsión. La distancia es distinta segun el angulo en el que se encuentre el punto respecto del robot */
				if (((diff_ang>45)&&(diff_ang<135)&&(distancia<alcanceLateral))||
					(((diff_ang<=45)||(diff_ang>=135))&&(distancia<alcance))) { 
					/* Primero miramos si la distancia es menor que la seguridad asignada. Si es menor giramos sin avanzar */
					if (((diff_ang>45)&&(diff_ang<135)&&(distancia<seguridadLateral))||
						((diff_ang<=45)&&(distancia<seguridad)))
          
						(*res).avanzaEnGiro=FALSE; /* inicialmente está a TRUE */

					/* sumo al angulo 180 ya que lo que buscamos es fuerza repulsiva y queremos el angulo opuesto */
					radianes=radianes+(180.0*DEGTORAD);

					/* calculamos el modulo */
					modulo=calculoFuerza(distancia,diff_ang); /* FUERZA REPULSIVA */

					// Añadimos las componentes a la suma de x y de y.
					sumaX+=cos(radianes)*modulo;
					sumaY+=sin(radianes)*modulo;	
				}
			} /* fin if(distancia!=0) */
		} /* fin if(v[i]) */
	} /* fin FOR */

	(*res).direccionFuerzaRepulsiva[0]=sumaX;
	(*res).direccionFuerzaRepulsiva[1]=sumaY;

	//printf("xRep:%d f_0.yRep:%d\n",(*res).direccionFuerzaRepulsiva[0],(*res).direccionFuerzaRepulsiva[1]);

	/* Calculo la fuerza resultante atribuyendo factores de proporción a la fuerza atractiva y repulsiva */
	(*res).direccionFuerzaResultante[0]=(alpha *(*res).direccionFuerzaAtractiva[0]) +(beta*(*res).direccionFuerzaRepulsiva[0]);
	(*res).direccionFuerzaResultante[1]=(alpha *(*res).direccionFuerzaAtractiva[1]) +(beta*(*res).direccionFuerzaRepulsiva[1]);

	sumaX=(*res).direccionFuerzaResultante[0];
	sumaY=(*res).direccionFuerzaResultante[1];

	(*res).moduloFuerzaResultante=sqrt((sumaX*sumaX)+(sumaY*sumaY));
	(*res).anguloFuerzaResultante=atan2(sumaY,sumaX);	  

	auxi1=transformoAngulo((*res).anguloFuerzaResultante);
	(*res).anguloFuerzaResultante=auxi1;
	//printf("Angulo resultante:%f\n",(*res).anguloFuerzaResultante);

	if ((sumaX==(*res).direccionFuerzaAtractiva[0])&&(sumaY==(*res).direccionFuerzaAtractiva[1])) { /* ¿? sumaX y sumaY han variado, por los factores aplicados...esto NO FUNCIONARÁ */
		// en caso k no actuen fuerzas, solo tenemos la atractiva
		(*res).anguloFuerzaResultante=(*res).anguloFuerzaAtractiva;;
		auxi=((*res).anguloFuerzaResultante)*100;
		auxi1=ceil(auxi);
		(*res).anguloFuerzaResultante=auxi1/100;
	}

	if((*res).anguloFuerzaResultante==0.) {
		// es mas facil detectar 6.28 k 2*Pi, por eso aproximamos
		(*res).anguloFuerzaResultante=2*M_PI;
		auxi=((*res).anguloFuerzaResultante)*100;
		auxi1=ceil(auxi);
		(*res).anguloFuerzaResultante=transformoAngulo(auxi1/100);
	}

	auxi=((*res).anguloFuerzaResultante)*100;
	auxi1=ceil(auxi);
	(*res).anguloFuerzaResultante=(auxi1/100);

}

void generarObjetivo(Tvoxel * objetivo, Tvoxel * oldlocalNavigationtarget, Tvoxel * localNavigationtarget) {
 float radio,angulo/*,min,max*/;

	if((contadorMemoria>LIMITE_LLEGADA_OBJETIVO)||((abs((int)((*objetivo).x-myencoders[0]))<200)&&(abs((int)((*objetivo).y-myencoders[1]))<200))) {
		//radio=applyGaussNoise(VFF_SIGMA_RADIO,0);
		radio=getLimitFloatRandom(VFF_SIGMA_RADIO_MIN,VFF_SIGMA_RADIO_MAX);
		angulo=applyGaussNoise(VFF_SIGMA_THETA,0);

		angulo=myencoders[2]*RADTODEG+angulo;
		if(angulo<0) angulo+=360;
		if(angulo>=360) angulo-=360;
		//printf("  %f  %f\n",myencoders[2]*RADTODEG, angulo);	
		(*oldlocalNavigationtarget).x=(*objetivo).x;
		(*oldlocalNavigationtarget).y=(*objetivo).y;

		(*objetivo).x=myencoders[0]+radio*cos(angulo*DEGTORAD);
		(*objetivo).y=myencoders[1]+radio*sin(angulo*DEGTORAD); 		

		(*localNavigationtarget).y=(*objetivo).x;
		(*localNavigationtarget).x=(*objetivo).y;

		contadorMemoria=0;	    	
	} else 
		contadorMemoria++;

}

void comportamientoVFF() {
	float auxi,auxi1;

	resultante.anguloQueLlevo=myencoders[2]; /* Nos quedamos con 2 decimales */
	auxi=resultante.anguloQueLlevo*100;
	auxi1=ceil(auxi);
	resultante.anguloQueLlevo=auxi1/100;

	auxi1=transformoAngulo(resultante.anguloQueLlevo);
	resultante.anguloQueLlevo=auxi1;

	auxi=resultante.anguloQueLlevo*100; /* Nos quedamos con 2 decimales */
	auxi1=ceil(auxi);
	resultante.anguloQueLlevo=(auxi1/100);

	rellenoMemoria(pdatos,validos,memoria); /* relleno memoria en funcion de los lasers */

	guardarCoordObjetivo(result); /* guardamos la FUERZA ATRACTIVA */
	/* Inicializo a TRUE avanzaEnGiro para que si no hay obstaculos por debajo de la seguridad se avanza a la vez que se gira */
	resultante.avanzaEnGiro=TRUE;

	calculaResultante(memoria,validos,result);

	/* Miro si he alcanzado el objetivo */
	if ((abs((int)(miObjetivo.x-myencoders[0]))<200)&&(abs((int)(miObjetivo.y-myencoders[1]))<200)) {   
		distanciaObjetivo=0;
		resultante.direccionFuerzaAtractiva[0]=0.0;
		resultante.direccionFuerzaAtractiva[1]=0.0;
	}	else distanciaObjetivo=1;

	/* Obtenemos los siguientes valores, para el posterior pintado de fuerzas */
	xRep=resultante.direccionFuerzaRepulsiva[0]; /* REPULSIVA */
	yRep=resultante.direccionFuerzaRepulsiva[1];

	xAtr=resultante.direccionFuerzaAtractiva[0]; /* ATRACTIVA */
	yAtr=resultante.direccionFuerzaAtractiva[1];

	xRes=resultante.direccionFuerzaResultante[0]; /* RESULTANTE = REPULSIVA vs. ATRACTIVA */
	yRes=resultante.direccionFuerzaResultante[1];

	difAngle=(resultante.anguloFuerzaResultante)-resultante.anguloQueLlevo;

	/* Sutileza para que el robot gire por el lado de menos giro */
	if (difAngle>180) difAngle=difAngle-360;
	if (difAngle<-180) difAngle=360+difAngle;

	peligro=0;

	if (!resultante.avanzaEnGiro) peligro=1;

	if (distanciaObjetivo==0) { /* estamos en el objetivo */
		*myv=0.0;
		*myw=0.0;
		vActual=*myv;
		wActual=*myw;
	} else { /* reglas AD-HOC */
		if (((localNavigationBehavior == secWindow) || (localNavigationBehavior == forgetMemory)) && (sitioEstrecho ())) { // COMPORTAMIENTO VFF VS VENTANA
			*myv = vMax; 
			*myw = 0.0; /* a toa leche, podemos seguir recto por el sitio estrecho */
			rellenoEstela (SECWINDOW_COLOR);
		} else { // COMPORTAMIENTO VFF PURO
			rellenoEstela (VFF_COLOR);
			if(peligro==1) { /* LO MAS URGENTE, SI HAY PELIGRO INMINENTE, GIRAMOS LENTAMENTE */
				if(difAngle<0) {*myv=0.0; *myw=wNegMax/2;}
				else {*myv=0.0; *myw=wPosMax/2;}
			}	else if (abs(difAngle)<limiteTrayectoriaSimilar) {*myv=vMax; *myw=0.0;} /* VIENTO EN POPA */
			else if (abs(difAngle)>limiteTrayectoriaDesigual) { /* GIRAMOS RAPIDAMENTE */
				if (difAngle<0) {*myv=0.0; *myw=wNegMax;}
				else {*myv=0.0; *myw=wPosMax;}
			}	else if (difAngle<0) {*myv=vMax/2; *myw=wNegMax/2;} /* MITAD, MITAD... (FIFTY, FIFTY) */
			else {*myv=vMax/2; *myw=wPosMax/2;}
		}
	} /* fin reglas AD-HOC */
}

/**********************************************************************************/
/**********************************************************************************/
/**************************COMPORTAMIENTO VENTANA**********************************/
/**********************************************************************************/   
void rellenarDistanciasVentana () { /* Concretamos las dimensiones de la ventana, según los parámetros ventanaY y ventanaX */
	int phi = 0; /* inicialmente comenzamos con el láser de ángulo 0 */

	while (tan (phi*DEGTORAD) <= (ventanaY/ventanaX)) { /* rellenamos la zona que tiene igual ventanaX */
		distanciasVentanaSeguridad [phi] = ventanaX / (cos (phi*DEGTORAD));
		phi ++;
	}

	while (phi < 90) { /* rellenamos la zona que tiene igual coseno */
		distanciasVentanaSeguridad [phi] = ventanaY / (sin (phi*DEGTORAD));
		phi ++;
	}
}

void rellenarHuecosVentana () { /* Concretamos las dimensiones de la ventana, según los parámetros ventanaY y huecoX */
	int phi = 0; /* inicialmente comenzamos con el láser de ángulo 0 */

	while (tan (phi*DEGTORAD) <= (ventanaY/huecoX)) { /* rellenamos la zona que tiene igual huecoX */
		distanciasHuecoSeguridad [phi] = huecoX / (cos (phi*DEGTORAD));
		phi ++;
	}

	while (phi < 90) { /* rellenamos la zona que tiene igual coseno */
		distanciasHuecoSeguridad [phi] = ventanaY / (sin (phi*DEGTORAD));
		phi ++;
	}
}

void rellenarFrenteVentana () {
	int phi = 0; /* inicialmente comenzamos con el láser de ángulo 0 */

	while ((tan (phi*3.14/180)) <= (frenteY/huecoX)) { /* rellenamos la zona que tiene igual huecoX */
		distanciasFrenteSeguridad [phi] = huecoX / (cos (phi*DEGTORAD));
		phi ++;
	}

	while (phi < 90) { /* rellenamos la zona que tiene igual coseno */
		distanciasFrenteSeguridad [phi] = frenteY / (sin (phi*DEGTORAD));
		phi ++;
	}
}

void rellenarVentanaSeguridad () {
	int i, j;

	for (i = 0;	i < 90; i ++) { /* primer cuadrante relleno */
		laser2xy (i, distanciasVentanaSeguridad [i], &ventanaSeguridad [i],&myencoders[0]);
	}

	for (i = 90, j = 89; i < 180; i ++, j--) { /* segundo cuadrante relleno */
		laser2xy (i, distanciasVentanaSeguridad [j], &ventanaSeguridad [i],&myencoders[0]);
	}
}

void rellenarFrenteSeguridad () {
	int i, j;

	for (i = 0;	i < 90; i ++) { /* primer cuadrante relleno */
		laser2xy (i, distanciasFrenteSeguridad [i], &frenteSeguridad [i],&myencoders[0]);
	}

	for (i = 90, j = 89; i < 180; i ++, j--) { /* segundo cuadrante relleno */
		laser2xy (i, distanciasFrenteSeguridad [j], &frenteSeguridad [i],&myencoders[0]);
	}
}

void rellenarHuecoSeguridad () {
	int i, j;

	for (i = 0;	i < 90; i ++) { /* primer cuadrante relleno */
		laser2xy (i, distanciasHuecoSeguridad [i], &huecoSeguridad [i],&myencoders[0]);
	}

	for (i = 90, j = 89; i < 180; i ++, j--) { /* segundo cuadrante relleno */
		laser2xy (i, distanciasHuecoSeguridad [j], &huecoSeguridad [i],&myencoders[0]);
	}
}

int calcularAnguloExtremo () {
	float phi = atan (ventanaY / huecoX);

	phi = phi*RADTODEG;
	phi = (int) ceil (phi);

	return (phi);
}

int calcularAnguloSemiExtremo () {
	float phi = atan (ventanaY / ventanaX);

	phi = phi*RADTODEG;
	phi = (int) ceil (phi);

	return (phi);
}

int obstaculoDelante () {
	int i, j;
	int obstacleFound = FALSE;
	int phiSemi = calcularAnguloSemiExtremo();

	i = phiSemi;
	while ((i < 90) && (obstacleFound==FALSE)) {
		if ((distanciasVentanaSeguridad[i] < mylaser[i]) && (mylaser[i] < distanciasFrenteSeguridad[i])) // detectado obstáculo dentro de ventanas de seguridad
			obstacleFound = TRUE;
		i ++;
	}

	j = 89;
	if (obstacleFound == FALSE) { // continuamos buscando en la segunda mitad
		while ((i < (180 - phiSemi)) && (obstacleFound==FALSE)) {
			if ((distanciasVentanaSeguridad[j] < mylaser[i]) && (mylaser[i] < distanciasFrenteSeguridad [j]))
				obstacleFound = TRUE;
			i++;
			j--;
		}
	}

	return (obstacleFound);
}

int obstaculoEnVentana () {
	int i = 0, j;
	int obstacleFound = FALSE;

	while ((i < 90) && (obstacleFound==FALSE)) {
		if (mylaser[i] < distanciasVentanaSeguridad[i]) // detectado obstáculo dentro de ventana de seguridad
			obstacleFound = TRUE;
		i ++;
	}

	j = 89;
	if (obstacleFound == FALSE) { // continuamos buscando en la segunda mitad
		while ((i < 180) && (obstacleFound==FALSE)) {
			if (mylaser[i] < distanciasVentanaSeguridad[j])
				obstacleFound = TRUE;
			i++;
			j--;
		}
	}

	return (obstacleFound);
}

int sitioEstrecho () {
	int i = 0, j;
	int obstacleFound = FALSE;

	if (!obstaculoEnVentana ()) { // si no hay en ventana ningún obstaculo...
		if (!obstaculoDelante ()) { // y si no hay en el frente, comprobamos que sí hay a los lados
			while ((i < 90) && (obstacleFound==FALSE)) {
				if ((distanciasVentanaSeguridad[i] < mylaser[i]) && (mylaser[i] < distanciasHuecoSeguridad[i])) // detectado obstáculo dentro de ventanas de seguridad
					obstacleFound = TRUE;
				i ++;
			}

			j = 89;
			if (obstacleFound == FALSE) { // continuamos buscando en la segunda mitad
				while ((i < 180) && (obstacleFound==FALSE)) {
					if ((distanciasVentanaSeguridad[j] < mylaser[i]) && (mylaser[i] < distanciasHuecoSeguridad [j]))
						obstacleFound = TRUE;
					i++;
					j--;
				}
			}
		} // fin si !obstaculoDelante
	} // fin si !obstaculoEnVentana

	return (obstacleFound); // devuelve si estamos en sitio estrecho
}

/**********************************************************************************/
/**********************************************************************************/
/****************************COMPORTAMIENTO GPP************************************/
/**********************************************************************************/   
float localizar_angulo(int col,int fil) {
	float salida;

	if((col==0)&&(fil==0))
		salida=0.;
	else if(((col==1)&&(fil==0))||((col==2)&&(fil==0)))
		salida=0.;
	else if((col==2)&&(fil==1))
		salida=22.5;
	else if (((col==1)&&(fil==1))||((col==2)&&(fil==2)))
		salida=45.;
	else if((col==1)&&(fil==2))
		salida=67.5;
	else if(((col==0)&&(fil==1))||((col==0)&&(fil==2)))
		salida=90.;
	else if((col==-1)&&(fil==2))
		salida=112.5;
	else if(((col==-1)&&(fil==1))||((col==-2)&&(fil==2)))
		salida=135.;
	else if((col==-2)&&(fil==1))
		salida=157.5;
	else if(((col==-1)&&(fil==0))||((col==-2)&&(fil==0)))
		salida=179.;
	else if((col==-2)&&(fil==-1))
		salida=-157.5;
	else if(((col==-1)&&(fil==-1))||((col==-2)&&(fil==-2)))
		salida=-135.;
	else if((col==-1)&&(fil==-2))
		salida=-112.5;
	else if(((col==0)&&(fil==-1))||((col==0)&&(fil==-2)))
		salida=-90.;
	else if((col==1)&&(fil==-2))
		salida=-67.5;
	else if(((col==1)&&(fil==-1))||((col==2)&&(fil==-2)))
		salida=-45.;
	else if((col==2)&&(fil==-1))
		salida=-22.5;

	return salida;
}

void followGradient () {
	int col,fil;
	float follow_grid_v,follow_grid_w,auxi;
	int i,j,col_ant, fil_ant;
	int ok1=0, ok2=0;
	static int previousVzero = FALSE;

	if (((myencoders[0] < myGlobalDestination.x + 200) && (myencoders[0] > myGlobalDestination.x - 200)) && ((myencoders[1] < myGlobalDestination.y + 200) && (myencoders[1] > myGlobalDestination.y - 200))) {
			printf("THE END: ARRIVAL TO DESTINATION!\n"); // llegada a objetivo global (FIN)
			objetivoGlobal = 1;
			*myv=0;
			*myw=0;
			en_objetivo=1;
	} else if (((myencoders[0] < myLocalDestination.x + 200) && (myencoders[0] > myLocalDestination.x - 200)) && ((myencoders[1] < myLocalDestination.y + 200) && (myencoders[1] > myLocalDestination.y - 200))) { // Aunque no hayamos llegado al fin, vamos a pararnos hasta calcular el siguiente GPP
			*myv=0;
			*myw=0;
			en_objetivo=1; // llegada a objetivo local
	} else { // vamos siguiendo el gradiente
		col=((int)(myencoders[0]-gradientGrid->lo.x-gradientGrid->resolucion/2)/gradientGrid->resolucion);
		fil=((int)(myencoders[1]-gradientGrid->lo.y-gradientGrid->resolucion/2)/gradientGrid->resolucion);

		estado=gradientGrid->map[(fil*gradientGrid->size)+col].estado;

		for (i=2;i>=-2;i--) {
			for(j=2;j>=-2;j--) {
				if ((fil+i>=0)&&(fil+i<gradientGrid->size)&&
					(col+j>=0)&&(col+j<gradientGrid->size)&&
					(gradientGrid->map[(fil+i)*gradientGrid->size+(col+j)].estado<=estado)&&
					(gradientGrid->map[(fil+i)*gradientGrid->size+(col+j)].estado>0))	{
					estado=gradientGrid->map[(fil+i)*gradientGrid->size+(col+j)].estado;
					auxi=localizar_angulo(j,i);

					if (auxi<0)
						auxi=auxi+360;
				}
			}
		}

		dif_angle=auxi-q_llevo;

		if (dif_angle > 180)
			dif_angle=dif_angle-360;

		if (dif_angle < -180)
			dif_angle=360+dif_angle;

		if (estado!=0) {
			if (estado < 20) lento = 2;
			else if (estado<5) lento = 1;
			else lento=0;

			for (i=0;i<NUM_LASER;i++)	{
				if ((i>=67.5)&&(i<=112.5)) {
					if ((mylaser[i]<alcanceGPP+v_actual)&&(mylaser[i]!=0)) anticipo=1;
					else anticipo=0;
				}
			}

			anticipo=0;

			if ((ok1=fc_output(follow_grid_controller,"velocidad_angular",&follow_grid_w))<0) {
				follow_grid_w=0.0;
			}
			if ((ok2=fc_output(follow_grid_controller,"velocidad_traccion",&follow_grid_v))<0) {
				if (previousVzero == TRUE) { // para no quedar estancados en ciertos puntos
					follow_grid_v=120.0;
					previousVzero = FALSE;
				} else {
					follow_grid_v=0.0;
					previousVzero = TRUE; // para la siguiente vuelta sabemos que la previa a sido un 0
				}
			}	// si para alguno de los controladores no hay reglas aplicables fc_output devuelve -1 y se aplica la velocidad_angular o de traccion nula. Si hay alguna regla aplicable se manda el valor que ella recomiende:

			fc_output(follow_grid_controller,"velocidad_angular",&follow_grid_w);
			fc_output(follow_grid_controller,"velocidad_traccion",&follow_grid_v);

			if ((follow_grid_v>v_actual)&&(follow_grid_v>0)) //si aceleramos
				if (follow_grid_v-v_actual>follow_grid_cycle*1)
					follow_grid_v=v_actual+follow_grid_cycle*1;

			*myv=follow_grid_v;
			*myw=follow_grid_w;
			v_actual=follow_grid_v;
			w_actual=follow_grid_w;

			q_llevo=myencoders[2]*RADTODEG;

			if ((myencoders[2]*RADTODEG)>180)
				q_llevo=(myencoders[2]*RADTODEG)-360;
			else if ((int)myencoders[2]*RADTODEG==360)
				q_llevo=0;
			else q_llevo=myencoders[2]*RADTODEG;

			if (aux[(fil)*gradientGrid->size+col]!=2)
				coste_real=coste_real+gradientGrid->map[(fil)*gradientGrid->size+col].estado;

			aux[(fil*gradientGrid->size)+col]=2;
			fil_ant=fil;
			col_ant=col;

			rellenoEstela (FOLLOWGPP_COLOR);
		} // fin if (estado!=0)
	} // fin "vamos siguiendo el gradiente"
}

void ingresar_celdilla_en_frente(int columna,int fila, float estado, float dist) {
	// por construccion jamas se va a insertar un nodo antes del primero, lo cual simplifica los algoritmos
	Tnodo *nuevo,*p,*q;
	int cont=0; // parametro meramente informativo en los printfs

	kfrente++; // parametro meramente informativo en los printfs
	p = lista;
	q = p;

	while((p->siguiente!=NULL)&&((p->distancia<dist))) {
		q = p;
		p = p->siguiente; 
		cont++;
	} // p siempre apuntará al último y q al penúltimo

	if (p->distancia<dist) { // insertamos el nuevo nodo el último, ya que su distancia es mayor
		nuevo=(Tnodo*) malloc (sizeof(Tnodo));
		nuevo->casillas = (Tcasilla*) malloc (MAX_CASILLAS*sizeof (Tcasilla));
		nuevo->distancia=dist;
		nuevo->casillasmax=1;
		nuevo->casillas[0].fila=fila;
		nuevo->casillas[0].columna=columna;
		nuevo->casillas[0].estado=estado;

		nuevo->siguiente=p->siguiente;
		p->siguiente=nuevo;
	}	else if (p->distancia>dist)	{ // insertamos el nuevo nodo antes del último y tras el penúltimo
		nuevo=(Tnodo*) malloc (sizeof(Tnodo));
		nuevo->casillas = (Tcasilla*) malloc (MAX_CASILLAS*sizeof (Tcasilla));
		nuevo->distancia=dist;
		nuevo->casillasmax=1;
		nuevo->casillas[0].fila=fila;
		nuevo->casillas[0].columna=columna;
		nuevo->casillas[0].estado=estado;

		nuevo->siguiente=p;
		q->siguiente=nuevo;
	}	else { // nueva casilla a insertar en el último nodo (el de máxima distancia)
		p->casillas[p->casillasmax].fila=fila;
		p->casillas[p->casillasmax].columna=columna;
		p->casillas[p->casillasmax].estado=estado;
		p->casillasmax++;
	}
}

void optimizeWay(int c, int f) {
	int i,j;
	Tcasilla caminito;
	// busca desde una celdilla del grid su vecina de menor estado, que no haya sido visitada antes. A ésta la marca con el estado de OPTIMIZED

	if (!((c == 0) && (f == 0))) { // para no causar conflicto con estas coordenadas
		if (gradientGrid->map[f*gradientGrid->size+c].estado>=0)
			menor=gradientGrid->map[f*gradientGrid->size+c].estado;

		if (gradientGrid->map[(f*gradientGrid->size)+c].estado!=0) { // si no estamos en el objetivo
			for (i=1;i>=-1;i--)
				for(j=1;j>=-1;j--)
					if ((f+i>=0)&&(f+i<gradientGrid->size)&&
					(c+j>=0)&&(c+j<gradientGrid->size)&&
					(menor>=gradientGrid->map[((f+i)*gradientGrid->size)+(c+j)].estado)&&
					(aux[((f+i)*gradientGrid->size)+(c+j)]!=OPTIMIZED)&&
					(gradientGrid->map[((f+i)*gradientGrid->size)+(c+j)].estado!=OCCUPIED)&&
					(gradientGrid->map[((f+i)*gradientGrid->size)+(c+j)].estado!=EMPTY)) {
						menor=gradientGrid->map[((f+i)*gradientGrid->size)+(c+j)].estado;
						caminito.fila=f+i;
						caminito.columna=c+j;
						//vemos cual es la de menor estado de los vecinitos y nos quedamos con ella
					}

			if ((caminito.fila>=0)&&(caminito.fila<gradientGrid->size)&&
					(caminito.columna>=0)&&(caminito.columna<gradientGrid->size)&&
					(gradientGrid->map[(caminito.fila*gradientGrid->size)+caminito.columna].estado!=0)) {
				aux[(caminito.fila*gradientGrid->size)+caminito.columna]=OPTIMIZED; //la marcamos para no volver atras
				coste_optimo=coste_optimo+gradientGrid->map[(caminito.fila*gradientGrid->size)+caminito.columna].estado;
				optimizeWay(caminito.columna, caminito.fila); // RECURSIVO !!
			}
		}
	} // fin if (!((c == 0) && (f == 0)))
}

void propagar (Tnodo *frente, int obstaculo) {
	int i,j,k,cont=0, co,fo;
	float d;

	// Expandir todas las casillas de una determinada distancia en el frente de onda. Cada distancia tiene un nodo. En el frente de onda los nodos estan ordenados por distancia, de menor a mayor, y siempre se recorre en ese orden
	if (frente!=NULL)	{
		for(k=0;k<frente->casillasmax;k++) {
			co=frente->casillas[k].columna;
			fo=frente->casillas[k].fila;

			if (frente->casillas[k].estado!=OCCUPIED) { // si es obstaculo no se modifica, sino actualizaremos su estado
				if ((fo>=0)&&(fo<gradientGrid->size) && (co>=0)&&(co<gradientGrid->size)) {
					gradientGrid->map[fo*gradientGrid->size+co].estado=frente->distancia;

					if ((fo==(int)((origenpoint_y-gradientGrid->lo.y-gradientGrid->resolucion/2)/gradientGrid->resolucion))&&
					(co==(int)((origenpoint_x-gradientGrid->lo.x-gradientGrid->resolucion/2)/gradientGrid->resolucion))&&
					(obstaculo==1)) {
						destino_alcanzado=1;
					}
					if (obstaculo==0) // obstaculos aun no propagados del todo, asi que...
						aux[fo*gradientGrid->size+co] = 1; // ...la introducimos en el vector auxiliar
				}
			}

			for (i=1;i>=-1;i--) { // para cada vecinito de la casilla actual
				for(j=1;j>=-1;j--) { // incorporo los vecinos no recorridos previamente al frente de onda de la siguiente iteracion
					if ((fo+i>=0)&&(fo+i<gradientGrid->size)&&
						(co+j>=0)&&(co+j<gradientGrid->size)&&
						(gradientGrid->map[(fo+i)*gradientGrid->size+(co+j)].estado==EMPTY)) { // si el vecino está vacío y está en la rejilla
						if ((i+j)%2!=0) d = frente->distancia +1.; // los vecinos transversales se les impone distancia 1
						else d = frente->distancia +1.414213562; // los vecinos diagonales se les impone más distancia
						/* Distribucion de distancias en vecinos:
													pi 1 pi
													1  0  1
													pi 1 pi                  */

						cont++; // parametro meramente informativo en los printfs
						ingresar_celdilla_en_frente(co+j,fo+i,gradientGrid->map[(fo+i)*gradientGrid->size+(co+j)].estado,d);
						// para evitar la insercion multiple de la misma celdilla
						gradientGrid->map[(fo+i)*gradientGrid->size+(co+j)].estado=ENCAPILLA;
					}//introducimos en el frente las celdillas propagadas por los obstaculos, con su valor
					else if ((fo+i>=0)&&(fo+i<gradientGrid->size)&&
						(co+j>=0)&&(co+j<gradientGrid->size)&&
						(gradientGrid->map[(fo+i)*gradientGrid->size+(co+j)].estado<=dist_max)&&
						(aux[(fo+i)*gradientGrid->size+(co+j)]==1)&&
						(gradientGrid->map[(fo+i)*gradientGrid->size+(co+j)].estado>0)) {
						if ((i+j)%2!=0)
							d=frente->distancia+gradientGrid->map[(fo+i)*gradientGrid->size+(co+j)].estado+1.;
						else 
							d=frente->distancia+gradientGrid->map[(fo+i)*gradientGrid->size+(co+j)].estado+1.414213562;

						cont++; // parametro meramente informativo en los printfs
						ingresar_celdilla_en_frente(co+j,fo+i,gradientGrid->map[(fo+i)*gradientGrid->size+(co+j)].estado,d); 
						// para evitar la insercion multiple de la misma celdilla
						gradientGrid->map[(fo+i)*gradientGrid->size+(co+j)].estado=ENCAPILLA;
					}
				}
			}
			kfrente--; // parametro meramente informativo en los printfs
		} // fin for(k=0;k<frente->casillasmax;k++)

		lista=frente->siguiente;
		free(frente);	// liberamos la memoria del nodo que acabamos de propagar
	} else // (fin if (frente!=NULL))
		destino_alcanzado=1;
}

void generateGradient () {
	Tnodo *inicio;

	int i,f,c,valor;

	// 1ª FASE: expandimos el frente de los obstaculos
	if (obstaculos_propagados==0)	{
		if (DEBUG) printf("-->Loading gradient. 1st level: \"Obstacle gradient expansion\"...\n");
		if (ctrl==0) { //nos indica si hemos terminado de ingresar todos los obstaculos en el frente
			// ingreso en el frente de onda todos las celdillas que son obstaculo
			for (f=0;f<gradientGrid->size;f++) {
				for (c=0;c<gradientGrid->size;c++) {
					if (gradientGrid->map[f*gradientGrid->size+c].estado==OCCUPIED) {
						// if (DEBUG) printf("Detectado obstaculo en %i, %i\n", f, c);
						if (primera_vez==0)	{ // creamos el 1er nodo de la lista, esto se hace en el pcpo de los tiempos
							//printf ("START: myTime is %u\n", dameTiempo ()); // para toma de tiempos con obstaculos
							inicio = (Tnodo*)malloc(sizeof(Tnodo));
							inicio->casillas = (Tcasilla*) malloc (MAX_CASILLAS*sizeof (Tcasilla));
							inicio->distancia = PESO;
							inicio->casillas[0].fila = f;
							inicio->casillas[0].columna = c;
							inicio->casillas[0].estado = OCCUPIED;
							inicio->casillasmax = 1;
							inicio->siguiente = NULL;
							lista = inicio;
						}	else // el resto de las celdas se introducen en la lista
							ingresar_celdilla_en_frente(c,f,OCCUPIED,PESO);

						primera_vez ++;
					}
				}
			}
		}	else if (lista != NULL) { // hay obstaculos en la rejilla
			if (lista->distancia<DISTANCIA_OBSTACULOS) { // cuando todas las celdas obstaculo se han ingresado, propagamos el frente, hasta una distancia limite
				propagar(lista,obstaculos_propagados);
			} else { //acaba esta fase y liberamos la lista
				lista=NULL;
				obstaculos_propagados=1;
				obstaculos_normalizados=0;
			}
		} else { // no hay obstaculos en la rejilla, asi que los damos por propagados
			lista=NULL;
			obstaculos_propagados=1;
			obstaculos_normalizados=0;
		}
		ctrl ++; // fin de ingreso de obstaculos
	}

	//2ª FASE: limpiamos las celdas del frente que stén marcadas ENCAPILLA y las que sean mayores que la distancia limite
	if (obstaculos_normalizados==0) {
		//printf ("START: myTime is %u\n", dameTiempo ()); // para toma de tiempos cuando no hay obstaculos
		if (DEBUG) printf("-->Loading gradient. 2nd level: \"Improving obstacle gradient\"...\n");
		for (i=0;i<gradientGrid->size*gradientGrid->size;i++) { //limpiamos las celdillas con estado ENCAPILLA para que no nos hagan de frontera
			if (gradientGrid->map[i].estado==ENCAPILLA) {
				gradientGrid->map[i].estado=EMPTY;
				aux[i]=0;//las kitamos del vector auxiliar creado pa llevar cuenta de las celdas propagadas
			}
			//invertimos el campo, para que las celdas + próximas a los obstáculos tengan mayor peso
			else if ((gradientGrid->map[i].estado!=EMPTY)&&(gradientGrid->map[i].estado!=OCCUPIED)) {
				valor=gradientGrid->map[i].estado-PESO;
				gradientGrid->map[i].estado=(gradientGrid->map[i].estado-(valor*CORRECCION))+6;

				if (gradientGrid->map[i].estado>dist_max)
					dist_max=gradientGrid->map[i].estado; //con esto controlamos hasta donde propagan los obstaculos
			}
			if ((gradientGrid->map[i].estado<=0) && (gradientGrid->map[i].estado!=OCCUPIED))	{//comprobamos que no hemos "invertido demasiado"
				gradientGrid->map[i].estado=EMPTY;
				aux[i]=0;//las kitamos del vector auxiliar creado pa llevar cuenta de las celdas propagadas
			}
			obstaculos_normalizados=1;
		} // fin for (i=0;i<gradientGrid->size*gradientGrid->size;i++)
	}

	//3ª FASE: elegimos el destino a donde viajará el robot
	if ((obstaculos_normalizados==1)&&(obstaculos_propagados==1)&&(destino_elegido==0)) {
		if (DEBUG) printf("-->Loading gradient. 3rd level: \"Setting destination (%f, %f)\"...\n", myLocalDestination.x, myLocalDestination.y);
		inicio = (Tnodo*)malloc(sizeof(Tnodo));
		inicio->casillas = (Tcasilla*) malloc (MAX_CASILLAS*sizeof (Tcasilla));
		inicio->distancia=0.; // la casilla de destino tiene distancia 0
		inicio->casillas[0].fila=(int)(myLocalDestination.y-gradientGrid->lo.y-gradientGrid->resolucion/2)/gradientGrid->resolucion;
		inicio->casillas[0].columna=(int)(myLocalDestination.x-gradientGrid->lo.x-gradientGrid->resolucion/2)/gradientGrid->resolucion;
		inicio->casillasmax=1;
		inicio->siguiente=NULL;
		lista=inicio;

	  destino_elegido=1;
	  origen_elegido=0;
	}

	//4ª FASE: elegimos el origen desde donde viajará el robot
	if (origen_elegido==0) {
		origenpoint_x=myencoders [0];
		origenpoint_y=myencoders [1];
		if (DEBUG) printf("-->Loading gradient. 4th level: \"Setting origin (%f, %f)\"...\n", origenpoint_x, origenpoint_y);
		origen_elegido=1;
	}

	//5ª FASE: propagamos el gradiente de distancias
	if ((obstaculos_propagados==1)&&(obstaculos_normalizados==1)&&(buscar==0)&&(destino_elegido==1)&&(origen_elegido==1))	{
		if (DEBUG) printf("-->Loading gradient. 5th level: \"Distance gradient expansion\"...\n");
		if ((lista!=NULL)||(destino_alcanzado==0)) {
			for(i=0;i<60;i++) propagar(lista,obstaculos_propagados);
		}

		if (destino_alcanzado==1) {
			for(i=0;i<500;i++) propagar(lista,2);
			buscar=1;
		}
		if (lista==NULL) buscar=1;
	}

	//6ª FASE: buscamos la ruta mínima al objetivo
	if ((obstaculos_propagados==1)&&(obstaculos_normalizados==1)&&(buscar==1)&&(destino_elegido==1)&&(origen_elegido==1)) {
		if (DEBUG) printf("-->Loading gradient. 6th level: \"Searching minimal cost way\"...\n");
		if (menor!=0) {
			forcedGradientRefresh = TRUE; // forzamos el refresco del nuevo gradiente a seguir
			optimizeWay((int)(origenpoint_x-gradientGrid->lo.x-gradientGrid->resolucion/2)/gradientGrid->resolucion,(int)(origenpoint_y-gradientGrid->lo.y-gradientGrid->resolucion/2)/gradientGrid->resolucion);

			if (DEBUG) printf("Way to destination was found!\n");
			//printf ("END: myTime is %u\n", dameTiempo ()); // para toma de tiempos
			fin_plan=1;
		}
	}
}

void waitingForDestination () {
	if (localNavigation_mouse_new==0) {
		if (!mensajeEsperaDestino) {
			if (DEBUG) printf("Waiting for destination...\n"); // esperamos a que nos pinchen el destino
			mensajeEsperaDestino = TRUE;
		}
	}	else {
		if (DEBUG) printf("Chosen destination on mouse position = [%f, %f]\n", myGlobalDestination.x, myGlobalDestination.y);
		fin_espera = 1;
	}
}

void createConfigFile () {
	FILE *salida;
	salida = fopen (configFile,"w");

	if (salida == NULL) {
		fprintf(stderr,"Parser: I can't found file %s\n", configFile); exit(-1);
	}

	printf("Building config file %s...\n", configFile);
	fprintf(salida,"dimension = %d\n", 4000);
	fprintf(salida,"resolucion = 50\n");
	fprintf(salida,"tipo = ec_diferencial\n");
	fprintf(salida,"ec_diferencial_speed = 1\n");
	fprintf(salida,"paso_tiempo = nulo\n");
	fprintf(salida,"cell_angles = 0\n");
	fprintf(salida,"mayoria_saturacion = 30\n");
	fprintf(salida,"mayoria_ruido = 10\n");
	fprintf(salida,"long_historia = 0\n");
	fprintf(salida,"\n");
	fprintf(salida,"sonar_filtra = independientes\n");
	fprintf(salida,"sonar_geometria = cono_denso\n");
	fprintf(salida,"sonar_apertura = 20.\n");
	fprintf(salida,"sonar_noobstacle = 3000.\n");
	fprintf(salida,"sonar_radialerror = 10.\n");
	fprintf(salida,"sonar_fdistancia = lineal\n");
	fprintf(salida,"sonar_residuo = 0.05\n");
	fprintf(salida,"sonar_o = 0.4\n");
	fprintf(salida,"sonar_e = -0.5\n");
	fprintf(salida,"sonar_mind = 700\n");
	fprintf(salida,"sonar_maxd = 1100.\n");
	fprintf(salida,"\n");
	fprintf(salida,"robot_geometria = cilindro\n");
	fprintf(salida,"robot_radio = 248.\n");
	fprintf(salida,"robot_e = -0.8\n");
	fprintf(salida,"\n");
	fprintf(salida,"laser_geometria = cono_denso\n");
	fprintf(salida,"laser_apertura = 0.5\n");
	fprintf(salida,"laser_muestras = 90\n");
	fprintf(salida,"laser_noobstacle = 8000.\n");
	fprintf(salida,"laser_o = 1\n");
	fprintf(salida,"laser_e = -0.7\n");

	fclose(salida);
}

void initGradientGrid () {
	int i;

	coste_real = 0; // inicialización de parámetros...
	coste_optimo = 0;
	kfrente = 0;
	menor = 1;
	destino_alcanzado = 0;
	dist_max = 0;
	destino_elegido = 0;
	origen_elegido = 1;
	obstaculos_propagados = 0;
	obstaculos_normalizados = 1;
	primera_vez = 0;			
	buscar = 0;
	ctrl = 0;
	fin_espera = 0;
	fin_plan = 0;
	en_objetivo = 0;
	mensajeNuevoGradiente = 0; // mostraremos nueva información del nuevo gradiente

	centrogrid.x=myencoders[0];
	centrogrid.y=myencoders[1];

	gradientGrid = inicia_grid (centrogrid, configFile); // gradiente

	aux = (float*)malloc((gradientGrid->size*gradientGrid->size)*sizeof(float));

	mapOnMoveTime.point = (Tvoxel*)malloc((gradientGrid->size*gradientGrid->size)*sizeof(Tvoxel));
	mapOnMoveTime.state = (int*)malloc((gradientGrid->size*gradientGrid->size)*sizeof(int));

	for(i=0;i< ((gradientGrid->size)*(gradientGrid->size)) ;i++) {
		gradientGrid->map[i].estado = EMPTY;
	}

	for(i=0;i< ((gradientGrid->size)*(gradientGrid->size)) ;i++)
		aux[i]=0;

	if ((myencoders[2]*RADTODEG)>180)
		q_llevo=(myencoders[2]*RADTODEG)-360;
	else if ((int)myencoders[2]*RADTODEG==360)
		q_llevo=0;
	else
		q_llevo=myencoders[2]*RADTODEG;     

	if (gradientGrid!=NULL) { // si no hubo ningún problema en la inicialización
		if (DEBUG) printf("Gradient Grid initialized correctly--> Size %.0f mm, each cell %.0f mm\n",gradientGrid->size*gradientGrid->resolucion,gradientGrid->resolucion);
	} else {
		if (DEBUG) printf("Not enough memory for gradient grid\n");
		exit (-1);
	}
}

void initPreviousOccupancy () {
	int i;

	for(i=0;i< ((occupancyGrid->size)*(occupancyGrid->size)) ;i++) {
		previousOccupancy.state[i] = -31; // para hacer una ocupación distina y forzar pintado...
		previousOccupancy.point[i].x = occupancyGrid->map[i].centro.x;
		previousOccupancy.point[i].y = occupancyGrid->map[i].centro.y;
	}
}

void initOccupancyGrid () {
	int i;

	centrogrid.x=myencoders[0];
	centrogrid.y=myencoders[1];

	occupancyGrid = inicia_grid (centrogrid, configFile); // memoria

	tempMap.point = (Tvoxel*)malloc((occupancyGrid->size*occupancyGrid->size)*sizeof(Tvoxel));
	tempMap.state = (int*)malloc((occupancyGrid->size*occupancyGrid->size)*sizeof(int));
	tempMap.updatingTime = (unsigned long int*)malloc((occupancyGrid->size*occupancyGrid->size)*sizeof(unsigned long int));
	previousOccupancy.state = (int*)malloc((occupancyGrid->size*occupancyGrid->size)*sizeof(int));
	previousOccupancy.point = (Tvoxel*)malloc((occupancyGrid->size*occupancyGrid->size)*sizeof(Tvoxel));

	for(i=0;i< ((occupancyGrid->size)*(occupancyGrid->size)) ;i++) {
		occupancyGrid->map[i].estado = EMPTY;
		occupancyGrid->map[i].ultima_actualizacion = dameTiempo ();
		tempMap.point[i] = occupancyGrid->map[i].centro;
		tempMap.state[i] = EMPTY;
		tempMap.updatingTime[i] = occupancyGrid->map[i].ultima_actualizacion;
	}

	if (occupancyGrid!=NULL) { // si no hubo ningún problema en la inicialización
		if (DEBUG) printf("Occupancy Grid initialized correctly--> Size %.0f mm, each cell %.0f mm\n",occupancyGrid->size*occupancyGrid->resolucion,occupancyGrid->resolucion);
	} else {
		if (DEBUG) printf("Not enough memory for occupancy grid\n");
		exit (-1);
	}
} 

int celdaRejilla2xy (Tgrid *myGrid, int celda, int *j, int *i) {
	if ((celda >= (myGrid->size * myGrid->size)) || (celda < 0)) // celda no perteneciente
		return (-1);
	else {
		*i = celda/myGrid->size;
		*j = celda%myGrid->size;
		return (0); // celda OK
	}
}

int xy2celdaRejilla(Tgrid *myGrid, Tvoxel mi_punto) {
  int celda=-1;
  int i,j;
  
  int resolucion=myGrid->resolucion;
  
  j=(mi_punto.x-myGrid->lo.x-resolucion/2)/resolucion;
  i=(mi_punto.y-myGrid->lo.y-resolucion/2)/resolucion;

	if ((i>=0) && (i<myGrid->size) && (j>=0) && (j<myGrid->size)) { // punto perteneciente a rejilla
	  if ((i>=0) && (j>=0)) celda=i*myGrid->size+j;
	}
  
  return celda;
}

void rellenarMiniRejillaGPP () {
/* Vamor a ir rellenando una ventana solidaria al centro del grid. Cuando éste cambie, cambiara la ubicación de esta ventana. Nos sirve para ver gráficamente cuando sale el robot de los límites de reubicación de grid. Hecho que nos permitirá ahorrar CPU y no reubicar el grid constantemente. */
	int i, j;
	// según los puntos y la dimensión de la ventana, habremos de dibujar los puntos a una determinada distancia u otra...
	int separacionEnX = ventanaGPPX / numPuntosX; 
	int separacionEnY = ventanaGPPY / numPuntosY;

	// El relleno será en sentido anti-horario, hasta completar la cuadratura de la ventana completamente
	for (i = 0;	i < numPuntosY; i ++) {
		ventanaGPP[i].x = centrogrid.x + ventanaGPPX;
		ventanaGPP[i].y = centrogrid.y + (i*separacionEnY);
	}

	for (i = numPuntosY, j = 0;	j < numPuntosX; i ++, j++) {
		ventanaGPP[i].x = centrogrid.x + (j*separacionEnX);
		ventanaGPP[i].y = centrogrid.y + ventanaGPPY;
	}

	for (i = numPuntosY + numPuntosX, j = 0;	j < numPuntosX; i ++, j++) {
		ventanaGPP[i].x = centrogrid.x - (j*separacionEnX);
		ventanaGPP[i].y = centrogrid.y + ventanaGPPY;
	}

	for (i = numPuntosY + (2*numPuntosX), j = 0;	j < numPuntosY; i ++, j++) {
		ventanaGPP[i].x = centrogrid.x - ventanaGPPX;
		ventanaGPP[i].y = centrogrid.y + (j*separacionEnY);
	}

	for (i = (2*numPuntosY) + (2*numPuntosX), j = 0;	j < numPuntosY; i ++, j++) {
		ventanaGPP[i].x = centrogrid.x - ventanaGPPX;
		ventanaGPP[i].y = centrogrid.y - (j*separacionEnY);
	}

	for (i = (3*numPuntosY) + (2*numPuntosX), j = 0;	j < numPuntosX; i ++, j++) {
		ventanaGPP[i].x = centrogrid.x - (j*separacionEnX);
		ventanaGPP[i].y = centrogrid.y - ventanaGPPY;
	}

	for (i = (3*numPuntosY) + (3*numPuntosX), j = 0;	j < numPuntosX; i ++, j++) {
		ventanaGPP[i].x = centrogrid.x + (j*separacionEnX);
		ventanaGPP[i].y = centrogrid.y - ventanaGPPY;
	}

	for (i = (3*numPuntosY) + (4*numPuntosX), j = 0;	j < numPuntosY; i ++, j++) {
		ventanaGPP[i].x = centrogrid.x + ventanaGPPX;
		ventanaGPP[i].y = centrogrid.y - (j*separacionEnY);
	}
}

int outsideMiniGPP () {
/* Esta función nos indica si el robot se ha salido de los límites de reubicación, dados por la ventana de "Mini GPP".
	En caso de que efectivamente se haya salido fuera, establece el nuevo centrogrid y vuelve a actualizar los datos de nuestra mini ventana limítrofe */
	int outsideMiniGPP = TRUE;
	Tvoxel posRobot;

	posRobot.x = myencoders [0];
	posRobot.y = myencoders [1];

	if ((posRobot.x > ventanaGPP[99].x) && (posRobot.x < ventanaGPP[0].x) && (posRobot.y < ventanaGPP[49].y) && (posRobot.y > ventanaGPP[149].y))
		outsideMiniGPP = FALSE;
	else {
		outsideMiniGPP = TRUE;
	}

	return (outsideMiniGPP);
}

int outsideDestination () {
	/* Nos indica si el destino marcado para el robot, se escapa fuera de nuestros límites de Grid */
	int celda = xy2celdaRejilla (gradientGrid, myGlobalDestination);

	if (celda == -1) {
		if (!mensajeDestinoElegido) {
			if (DEBUG) printf("Chosen destination out of Grid\n");
			mensajeDestinoElegido = TRUE;
		}
		return TRUE;
	} else {
		if (!mensajeDestinoElegido) {
			if (DEBUG) printf("Chosen destination inside of Grid\n");
			mensajeDestinoElegido = TRUE;
		}
		return FALSE;
	}
}

void calibrateDestination (Tvoxel* myDestination) {
	Tvoxel posRobot, actual;
	float deltaX, deltaY;
	float incremento = 0.001;
	int celdilla;
	int i = 1;

	posRobot.x = myencoders[0];
	posRobot.y = myencoders[1];

	deltaX = myDestination->x - posRobot.x;
	deltaY = myDestination->y - posRobot.y;

	actual = posRobot;

	celdilla = xy2celdaRejilla (gradientGrid, actual);

	while (celdilla != -1) { // recorremos el rayo hasta llegar al límite de nuestra rejilla
		actual.x = posRobot.x + i*incremento*deltaX;
		actual.y = posRobot.y + i*incremento*deltaY;
		celdilla = xy2celdaRejilla (gradientGrid, actual);

		i ++;
	} // cuando salgamos será porque celdilla ya pertenece al exterior de la rejilla

	// retornamos a una posicion anterior, que será la últimísima casilla dentro de rejilla
	actual.x -= incremento*deltaX;
	actual.y -= incremento*deltaY;

	myDestination->x = actual.x;
	myDestination->y = actual.y;
}

void updateGridTiming () {
	int j;

	for(j=0;j< ((occupancyGrid->size)*(occupancyGrid->size)); j++) {
		if ((dameTiempo() - occupancyGrid->map[j].ultima_actualizacion) > MAX_TIME_ON_GRID) { // es punto viejete
			occupancyGrid->map[j].estado = EMPTY; // lo marcamos a vacio para no me haga de frontera
			occupancyGrid->map[j].ultima_actualizacion = dameTiempo ();
		}
	}
}

void updateRejillaGPP () {
	int i, j, k, celda;
	Tvoxel tope, posRobot, actual;
	int finRayo;
	int celdilla;

	updateGridTiming ();

	// 1º.- Actualizaremos los puntos de forma que solaparemos los que ya teníamos, en la nueva rejilla
	if (pendienteDeReubicacionGrafica) { // se ha reubicado la rejilla, por lo que puede haber solapamiento
		if (DEBUG) printf("Updating occupancy grid according to previous occupancy grid and actual laser information...\n");
		for(j=0;j< ((occupancyGrid->size)*(occupancyGrid->size)); j++) {
			occupancyGrid->map[j].estado = EMPTY; // inicialización: desconocemos la nueva zona
			occupancyGrid->map[j].ultima_actualizacion = dameTiempo ();
			for(k=0;k< ((occupancyGrid->size)*(occupancyGrid->size)); k++) {
				if ((occupancyGrid->map[j].centro.x == tempMap.point[k].x) && (occupancyGrid->map[j].centro.y == tempMap.point[k].y)) {
					occupancyGrid->map[j].estado = tempMap.state[k]; // solapamos estado: es zona conocida
					occupancyGrid->map[j].ultima_actualizacion = tempMap.updatingTime[k]; // nos quedamos con su tiempo
					break;
				}
			}
		}
		pendienteDeReubicacionGrafica = FALSE;
	}

	posRobot.x = myencoders [0];
	posRobot.y = myencoders [1];

	// 2º.- Ahora actualizaremos según la información que nos llega de los lásers: zona conocida también y actualizada
	for(j=0;j<NUM_LASER;j++) {
		laser2xy (j, mylaser[j], &tope,&myencoders[0]);

		i = 1;
		finRayo = FALSE;
		actual.x = posRobot.x;
		actual.y = posRobot.y;

		celdilla = xy2celdaRejilla (occupancyGrid, actual);

		while ((celdilla != -1) && (!finRayo)) { // estamos dentro de la rejilla y no hemos llegado al tope del rayo
			if ((((posRobot.x < tope.x) && (actual.x < tope.x)) ||
				((posRobot.x > tope.x) && (actual.x > tope.x))) &&
				((posRobot.y < tope.y) && (actual.y < tope.y)) ||
				((posRobot.y > tope.y) && (actual.y > tope.y))) { // estamos aun dentro del rayo

				occupancyGrid->map[celdilla].estado = EMPTY;
				occupancyGrid->map[celdilla].ultima_actualizacion = dameTiempo ();
				finRayo = FALSE;

				laser2xy (j, i*INCREMENT, &actual,&myencoders[0]);
				celdilla = xy2celdaRejilla (occupancyGrid, actual);

				i++;
			} else finRayo = TRUE;
		}

		if (finRayo) { // si hemos salido del bucle anterior por llegar al fin de rayo, marcamos las restantes a UNKNOWN
									 // --> si hemos salido porque estaba nuestra de nuestra rejilla, no podemos hacer nada más...
			if (celdilla != -1) { // el punto del láser es el único que se da por ocupado
				occupancyGrid->map[celdilla].estado = OCCUPIED;
				occupancyGrid->map[celdilla].ultima_actualizacion = dameTiempo ();

				laser2xy (j, i*INCREMENT, &actual,&myencoders[0]);
				celdilla = xy2celdaRejilla (occupancyGrid, actual);
			} // los puntos detrás del laser se dejarán como están (recuerdo de objetos "tapados")
		} // fin if (finRayo)
	} // fin for(j=0;j<NUM_LASER;j++)
}

void drawRejillaGPP () {
	int i=0, R, G, B;

  for (i = 0 ; i < ((occupancyGrid->size)*(occupancyGrid->size)) ; i++) {
		if ((occupancyGrid->map[i].centro.x == previousOccupancy.point[i].x) &&
				(occupancyGrid->map[i].centro.y == previousOccupancy.point[i].y) &&
			  (occupancyGrid->map[i].estado != previousOccupancy.state[i])) { // solo el punto que varía se pintará
			previousOccupancy.state[i] = occupancyGrid->map[i].estado; // nos guardamos ya como anterior, para la siguiente vuelta
			if (occupancyGrid->map[i].estado == OCCUPIED) { // si está ocupada la celdilla
				R=(int)255;
				G=(int)0;
				B=(int)0;
			} else if (occupancyGrid->map[i].estado == EMPTY) { // si está vacía la celdilla
				R=(int)0;
				G=(int)255;
				B=(int)0;
			} else if (occupancyGrid->map[i].estado == UNKNOWN) { // será desconocida
				R=(int)0;
				G=(int)0;
				B=(int)255;
			}
			fl_mapcolor(myColor,R,G,B);
			fl_set_foreground(localNavigationgui_gc,myColor);

			xy2localNavigationcanvas(occupancyGrid->map[i].centro,&(mapaGrafico[i]));

			XDrawRectangle(display,localNavigation_canvas_win,localNavigationgui_gc,mapaGrafico[i].x,mapaGrafico[i].y,5,5);
			XFillRectangle(display,localNavigation_canvas_win,localNavigationgui_gc,mapaGrafico[i].x,mapaGrafico[i].y,5,5);
		}
	}
}

void drawRejillaGradienteGPP () {
	int i, R, G, B;

	pthread_mutex_lock (&GPPdisplayMutex);

	if (lista != NULL) { // no hay gradiente a dibujar
	  for (i = 0 ; i < ((gradientGrid->size)*(gradientGrid->size)) ; i++) {	  
			if ((gradientGrid->map[i].estado*3) > 255) { // si nos pasamos del límite lo establecemos a 255
				R=(int)255;
				G=(int)255;
				B=(int)255;
			}	else if (((gradientGrid->map[i].estado) >= 0) && ((gradientGrid->map[i].estado) < 3)) { // OBJETIVO
				R=(int)255;
				G=(int)0;
				B=(int)0;
			}	else { // gradiente en escala de grises
				R=(int)gradientGrid->map[i].estado*3;
				G=(int)gradientGrid->map[i].estado*3;
				B=(int)gradientGrid->map[i].estado*3;
			}
			
			fl_mapcolor (myColor, R, G, B);
			fl_set_foreground (localNavigationgui_gc, myColor);

			xy2localNavigationcanvas (gradientGrid->map[i].centro,&(gradienteGrafico[i]));

			XDrawRectangle(display,localNavigation_canvas_win,localNavigationgui_gc,gradienteGrafico[i].x,gradienteGrafico[i].y,5,5);
			XFillRectangle(display,localNavigation_canvas_win,localNavigationgui_gc,gradienteGrafico[i].x,gradienteGrafico[i].y,5,5);
		}
	} else {
		if (DEBUG) printf ("Gradient grid not generated\n");
	}

	pthread_mutex_unlock (&GPPdisplayMutex);

}

void initFollower () {
	// Obtención de reglas para la actuación según el gradiente que se genere
	follow_grid_controller = fc_open("follow_grid.fzz"); // abrimos nuestro fichero de reglas borrosas

	if (follow_grid_controller!=-1) { // si hemos abierto correctamente el fichero de reglas borrosas
		fc_link(follow_grid_controller,"angulo",&dif_angle);
		fc_link(follow_grid_controller,"v_actual",&v_actual);
		fc_link(follow_grid_controller,"w_actual",&w_actual);
		fc_link(follow_grid_controller,"cerca",&lento);
		fc_link(follow_grid_controller,"anticipo",&anticipo);
	}
}

void saveTempMap () {
	int i;

	for(i=0;i< ((occupancyGrid->size)*(occupancyGrid->size)) ;i++) {
		tempMap.point[i].x = occupancyGrid->map[i].centro.x;
		tempMap.point[i].y = occupancyGrid->map[i].centro.y;
		tempMap.state[i] = occupancyGrid->map[i].estado;
		tempMap.updatingTime[i] = occupancyGrid->map[i].ultima_actualizacion;
	}
}

void moveInformation () {
	int j;

	if (DEBUG) printf("Updating gradient information according to the actual occupancy state...\n");
	for(j=0;j< ((occupancyGrid->size)*(occupancyGrid->size)); j++) {
		gradientGrid->map[j].estado = occupancyGrid->map[j].estado; // partimos de la informacion de ocupacion
		mapOnMoveTime.point[j].x = occupancyGrid->map[j].centro.x; // almacenamos tambien en la temporal
		mapOnMoveTime.point[j].y = occupancyGrid->map[j].centro.y;
		mapOnMoveTime.state[j] = occupancyGrid->map[j].estado;
	}

	movedInformation = TRUE;
}

int thereAreChanges () { // nos informa si hay "novedades" -> regeneraremos el gradiente
	int j, k, novedad = 0;
	int changes = FALSE;

	for(j=0;j< ((occupancyGrid->size)*(occupancyGrid->size)); j++) {
		for(k=0;k< ((occupancyGrid->size)*(occupancyGrid->size)); k++) {
			if ((occupancyGrid->map[j].centro.x == mapOnMoveTime.point[k].x) && (occupancyGrid->map[j].centro.y == mapOnMoveTime.point[k].y)) { // si tenemos el mismo punto (aunque esté solapado), vemos si su estado es el mismo
				if (occupancyGrid->map[j].estado != mapOnMoveTime.state[k]) // hay novedad
					novedad ++;

				break; // pasaremos a comprobar otro punto
			}
		}
		if (k == (((occupancyGrid->size)*(occupancyGrid->size)) - 1)) // no hemos encontrado su "solapado"...
			novedad ++; // ...por tanto es un punto novedad
	}

	if (novedad >= MAX_CHANGES_ON_GRID) { // si tenemos el máximo de novedades permitidos, hemos de regenerar el GPP
		if (DEBUG) printf("Several changes (%i) on occupancy grid vs. gradient grid were detected!\n", novedad);
		changes = TRUE;
	}

	return (changes);
}

void freeGradientMemory () {
	Tnodo* listAux;

	if (DEBUG) printf("Releasing gradient memory...\n");
	termina_grid (gradientGrid);

	free (aux);
	aux = NULL;

	listAux = lista;
	while (listAux != NULL) {
		free (listAux->casillas);
		listAux = listAux->siguiente;
	}

	free (lista);
	lista = NULL;

	free (mapOnMoveTime.point);
	mapOnMoveTime.point = NULL;
	free (mapOnMoveTime.state);
	mapOnMoveTime.state = NULL;
}

void comportamientoGPP () {
	if (fin_espera == 0) { // hay que pinchar con el ratón al destino para poder generar el gradiente y empezar!
		if (showMemory == FALSE) { // podremos visualizar la información aun sin pinchar con el ratón
			initOccupancyGrid ();
			centrogrid.x = myencoders[0]; // reubicación de mini rejilla
			centrogrid.y = myencoders[1];
			rellenarMiniRejillaGPP ();
			reubica_grid (occupancyGrid, centrogrid);
			pendienteDeReubicacionGrafica = TRUE;
			initPreviousOccupancy ();
			updateRejillaGPP ();
			showMemory = TRUE;
		}
		if (outsideMiniGPP ()) {
			centrogrid.x = myencoders[0]; // reubicación de mini rejilla
			centrogrid.y = myencoders[1];
			rellenarMiniRejillaGPP ();
			saveTempMap ();
			reubica_grid (occupancyGrid, centrogrid);
			pendienteDeReubicacionGrafica = TRUE;
			updateRejillaGPP ();
			forcedOccupancyRefresh = TRUE; // forzamos refresco
		}

		waitingForDestination ();
	}	else if ((fin_plan == 0) || (((objetivoGlobal == 0) && (en_objetivo == 1)) || (thereAreChanges() == TRUE)) || (!isInitGrid) || (outsideMiniGPP())) { // sólo nos falta ir siguiendo el gradiente generado; sólo se generará cuando haya reubicación
		if (!isInitGrid) { // FASE INICIAL: INICIALIZACION DE ESTRUCTURAS Y DEMÁS... sólo se hará una vez
			initOccupancyGrid ();
			initGradientGrid ();
			initFollower ();
			rellenarMiniRejillaGPP ();
			reubica_grid (occupancyGrid, centrogrid);
			pendienteDeReubicacionGrafica = TRUE;
			initPreviousOccupancy ();
			updateRejillaGPP ();
			moveInformation ();
			isInitGrid = TRUE;		

		} else if (outsideMiniGPP()) { // REUBICACIÓN DE REJILLA DE INFORMACIÓN, cuando salgamos de miniGrid
			centrogrid.x = myencoders[0]; // reubicación de mini rejilla
			centrogrid.y = myencoders[1];
			rellenarMiniRejillaGPP ();
			saveTempMap (); // nos guardamos información para posible "solape"
			reubica_grid (occupancyGrid, centrogrid);
			pendienteDeReubicacionGrafica = TRUE;
			updateRejillaGPP (); // posibilidad de solape, así como nueva información de láser
			forcedOccupancyRefresh = TRUE;			

		} else if (((objetivoGlobal == 0) && (en_objetivo == 1)) || (thereAreChanges() == TRUE)) { // REUBICACIÓN DE REJILLA GRADIENTE, cuando lleguemos al objetivo local o haya novedades considerables
			*myv = 0; // para que no esté con el ultimo valor que tenía... hasta que se tomen nuevas decisiones
			*myw = 0;
			pthread_mutex_lock (&GPPdisplayMutex);
			freeGradientMemory ();
			initGradientGrid ();
			pthread_mutex_unlock (&GPPdisplayMutex);

			moveInformation (); // copiamos información procedente de la actual occupancyGrid
		}	else if (fin_plan == 0) {
			myLocalDestination.x = myGlobalDestination.x;
			myLocalDestination.y = myGlobalDestination.y;

			if (outsideDestination ()) // si el destino está fuera de la rejilla
				calibrateDestination (&myLocalDestination); // reubicamos destino al marco de la rejilla

			generateGradient ();
		}
	} else if ((en_objetivo) == 0) {
		// TODO: FORZAMOS REGENERACION DE GRADIENTE
		//en_objetivo = 1;
		followGradient ();
		if (!mensajeNuevoGradiente) {
			if (DEBUG) printf("\nFollowing new optimized way according to the actual gradient\n");
			celdaRejilla2xy (gradientGrid, xy2celdaRejilla (gradientGrid, myLocalDestination), &col, &fil);
			if (DEBUG) printf("--->Actual destination = [%f, %f]. Celdilla = [%i, %i]\n", myLocalDestination.x, myLocalDestination.y, fil, col);
			Tvoxel temporalPoint;
			temporalPoint.x = origenpoint_x;
			temporalPoint.y = origenpoint_y;
			celdaRejilla2xy (gradientGrid, xy2celdaRejilla (gradientGrid, temporalPoint), &col, &fil);
			if (DEBUG) printf("--->Actual origin = [%f, %f]. Celdilla = [%i, %i]\n", temporalPoint.x, temporalPoint.y, fil, col);

			mensajeNuevoGradiente = 1;
		}
	}
}
/**********************************************************************************/
/**********************************************************************************/
/**********************************F I N*******************************************/
/**********************************************************************************/

const char *localNavigation_range(FL_OBJECT *ob, double value, int prec) {
	static char buf[32];

	sprintf(buf,"%.1f",value/1000.);
	return buf;
}

int xy2localNavigationcanvas(Tvoxel point, XPoint* grafico) {
     /* return -1 if point falls outside the canvas */
	float xi, yi;

	xi = (point.x * localNavigation_odometrico[3] - point.y * localNavigation_odometrico[4] + localNavigation_odometrico[0])*localNavigation_escala;
	yi = (point.x * localNavigation_odometrico[4] + point.y * localNavigation_odometrico[3] + localNavigation_odometrico[1])*localNavigation_escala;
	/* Con esto cambiamos al sistema de referencia de visualizacion, centrado en algun punto xy y con alguna orientacion definidos por odometrico. Ahora cambiamos de ese sistema al del display, donde siempre hay un desplazamiento a la esquina sup. izda. y que las y se cuentan para abajo. */

	grafico->x = xi + localNavigation_width/2;
	grafico->y = -yi + localNavigation_height/2;

	if ((grafico->x <0)||(grafico->x>localNavigation_width)) return -1; 
	if ((grafico->y <0)||(grafico->y>localNavigation_height)) return -1; 
	return 0;
}

void localNavigation_iteration() {
	speedcounter(localNavigation_id);

	if ((localNavigationBehavior == vff) || (localNavigationBehavior == secWindow)) {
		comportamientoVFF (); // usamos comportamiento VFF + VENTANA --MUY BUENO--
	} else if (localNavigationBehavior == forgetMemory) {
		olvidoTemporalPuntos();
		comportamientoVFF ();
	} else if (localNavigationBehavior == localGPP) {
		comportamientoGPP ();
	}
}

void localNavigation_suspend() {
  pthread_mutex_lock(&(all[localNavigation_id].mymutex));
  put_state(localNavigation_id,slept);
  all[localNavigation_id].children[(*(int *)myimport("laser","id"))]=FALSE;
  all[localNavigation_id].children[(*(int *)myimport("motors","id"))]=FALSE;
  all[localNavigation_id].children[(*(int *)myimport("encoders","id"))]=FALSE;
  lasersuspend();
  encoderssuspend();
  motorssuspend();
  printf("localNavigation: off\n");
  pthread_mutex_unlock(&(all[localNavigation_id].mymutex));
}

void localNavigation_resume(int father, int *brothers, arbitration fn) {
  int i;
 
  pthread_mutex_lock(&(all[localNavigation_id].mymutex));
  /* this schema resumes its execution with no children at all */
  for(i=0;i<MAX_SCHEMAS;i++) all[localNavigation_id].children[i]=FALSE;
 
  all[localNavigation_id].father=father;
  if (brothers!=NULL)
    {
      for(i=0;i<MAX_SCHEMAS;i++) localNavigation_brothers[i]=-1;
      i=0;
      while(brothers[i]!=-1) {localNavigation_brothers[i]=brothers[i];i++;}
    }
  localNavigation_callforarbitration=fn;
  put_state(localNavigation_id,notready);

  mylaser=(int *)myimport("laser","laser");
  laserresume=(resumeFn)myimport("laser","resume");
  lasersuspend=(suspendFn *)myimport("laser","suspend");

  myencoders=(float *)myimport("encoders","jde_robot");
  encodersresume=(resumeFn)myimport("encoders","resume");
  encoderssuspend=(suspendFn)myimport("encoders","suspend");

  myv=(float *)myimport("motors","v");
  myw=(float *)myimport("motors","w");
  motorsresume=(resumeFn)myimport("motors","resume");
  motorssuspend=(suspendFn)myimport("motors","suspend");

  printf("localNavigation: on\n");
  pthread_cond_signal(&(all[localNavigation_id].condition));
  pthread_mutex_unlock(&(all[localNavigation_id].mymutex));
}

void *localNavigation_thread(void *not_used) 
{
  struct timeval a,b;
  long diff, next;

  for(;;)
    {
      pthread_mutex_lock(&(all[localNavigation_id].mymutex));

      if (all[localNavigation_id].state==slept) 
	{
	  pthread_cond_wait(&(all[localNavigation_id].condition),&(all[localNavigation_id].mymutex));
	  pthread_mutex_unlock(&(all[localNavigation_id].mymutex));
	}
      else 
	{
	  /* check preconditions. For now, preconditions are always satisfied*/
	  if (all[localNavigation_id].state==notready) put_state(localNavigation_id,ready);
	  else if (all[localNavigation_id].state==ready)	  /* check brothers and arbitrate. For now this is the only winner */
	    { 
	      put_state(localNavigation_id,winner);
	      *myv=0; *myw=0; 
	      /* start the winner state from controlled motor values */ 
	      all[localNavigation_id].children[(*(int *)myimport("laser","id"))]=TRUE;
	      all[localNavigation_id].children[(*(int *)myimport("motors","id"))]=TRUE;
	      all[localNavigation_id].children[(*(int *)myimport("encoders","id"))]=TRUE;
	      laserresume(localNavigation_id,NULL,NULL);
	      encodersresume(localNavigation_id,NULL,NULL);
	      motorsresume(localNavigation_id,NULL,NULL);
	    }	  
	  else if (all[localNavigation_id].state==winner);

	  if (all[localNavigation_id].state==winner)
	    /* I'm the winner and must execute my iteration */
	    {
	      pthread_mutex_unlock(&(all[localNavigation_id].mymutex));
	      gettimeofday(&a,NULL);
	      localNavigation_iteration();
	      gettimeofday(&b,NULL);  

	      diff = (b.tv_sec-a.tv_sec)*1000000+b.tv_usec-a.tv_usec;
	      next = localNavigation_cycle*1000-diff-10000; 
	      if (next>0) 
		/* discounts 10ms taken by calling usleep itself */
		usleep(localNavigation_cycle*1000-diff);
	      else 
		/* just let this iteration go away. overhead time negligible */
		{//printf("time interval violated: localNavigation\n"); 
		usleep(localNavigation_cycle*1000);
		}
	    }
	  else 
	    {
	      pthread_mutex_unlock(&(all[localNavigation_id].mymutex));
	      usleep(localNavigation_cycle*1000);
	    }
	}
    }
}

void localNavigation_startup() {
  pthread_mutex_lock(&(all[localNavigation_id].mymutex));
  myexport("localNavigation","id",&localNavigation_id);
  myexport("localNavigation","resume",(void *) &localNavigation_resume);
  myexport("localNavigation","suspend",(void *) &localNavigation_suspend);
  printf("localNavigation schema started up\n");
  put_state(localNavigation_id,slept);
  pthread_create(&(all[localNavigation_id].mythread),NULL,localNavigation_thread,NULL);

	init_pioneer ();

	if (myregister_buttonscallback==NULL){
    if ((myregister_buttonscallback=(registerbuttons)myimport ("graphics_xforms", "register_buttonscallback"))==NULL){
      if (DEBUG) printf("I can't fetch register_buttonscallback from graphics_xforms\n");
      jdeshutdown(1);
    }
    if ((mydelete_buttonscallback=(deletebuttons)myimport ("graphics_xforms", "delete_buttonscallback"))==NULL){
      if (DEBUG) printf("I can't fetch delete_buttonscallback from graphics_xforms\n");
      jdeshutdown(1);
    }
    if ((myregister_displaycallback=(registerdisplay)myimport ("graphics_xforms", "register_displaycallback"))==NULL){
      if (DEBUG) printf("I can't fetch register_displaycallback from graphics_xforms\n");
      jdeshutdown(1);
    }
    if ((mydelete_displaycallback=(deletedisplay)myimport ("graphics_xforms", "delete_displaycallback"))==NULL){
      jdeshutdown(1);
      if (DEBUG) printf("I can't fetch delete_displaycallback from graphics_xforms\n");
    }
  }

  if ((myscreen=(int *)myimport("graphics_xforms", "screen"))==NULL){
    fprintf(stderr, "localNavigation: I can't fetch screen from graphics_xforms\n");
    jdeshutdown(1);
  }
  if ((display=(Display *)myimport("graphics_xforms", "display"))==NULL){
    fprintf(stderr, "localNavigation: I can't fetch display from graphics_xforms\n");
    jdeshutdown(1);
  }
  pthread_mutex_unlock(&(all[localNavigation_id].mymutex));

	localNavigation_escala = 20000; // escala del mundillo

	numEstelas = 0; // contador para la estela (rastro) del robot

	/* COMPORTAMIENTO VFF */
  localNavigationBehavior = localGPP; /* comportamiento inicial */

	int i, hora;
  contadorMemoria=0;	
  result=&resultante;
  pdatos=&datos;
  hora=dameTiempo();
	num_puntos = 0;

  for(i=0;i<tamMemoria;i++)
	  validos[i]=FALSE;

  for(i=0;i<tamMemoria;i++)
      datos.horaInicio[i]=hora;

  datos.ultimo=0;

	/* COMPORTAMIENTO VENTANA: rellenamos nuestras tres ventanas de seguridad */
	rellenarHuecosVentana (); // ventana con la que conseguimos los huecos laterales
	rellenarDistanciasVentana (); // ventana minima de seguridad (central)
	rellenarFrenteVentana (); // ventana con la que conseguimos el hueco del frente

	/* COMPORTAMIENTO GPP */
	pthread_mutex_init (&GPPdisplayMutex, NULL);
	// Esta inicializacion es equivalente a: pthread_mutex_t GPPdisplayMutex = PTHREAD_MUTEX_INITIALIZER

	configFile = "./gradientPlanning.conf";
	createConfigFile ();
}

void localNavigation_guibuttons(void *obj1)
{

  FL_OBJECT *obj=(FL_OBJECT *)obj1;

	if (obj == fd_localNavigationgui->exit) localNavigation_guisuspend();
	else if (obj == fd_localNavigationgui->stop) {
		localNavigation_suspend ();
	} else if (obj == fd_localNavigationgui->escala) {
		localNavigation_rango = fl_get_slider_value (fd_localNavigationgui->escala);
    localNavigation_visual_refresh = TRUE; /* activa el flag que limpia el fondo de la pantalla y repinta todo */ 
    localNavigation_escala = localNavigation_width / localNavigation_rango;
  } else if (obj== fd_localNavigationgui->center) { /* Se mueve 10%un porcentaje del rango */
		localNavigation_odometrico[0]+=localNavigation_rango*(fl_get_positioner_xvalue(fd_localNavigationgui->center)-0.5)*(-2.)*(0.1);
		localNavigation_odometrico[1]+=localNavigation_rango*(fl_get_positioner_yvalue(fd_localNavigationgui->center)-0.5)*(-2.)*(0.1);
		fl_set_positioner_xvalue(fd_localNavigationgui->center,0.5);
		fl_set_positioner_yvalue(fd_localNavigationgui->center,0.5);
		localNavigation_visual_refresh = TRUE;
  } else if (obj == fd_localNavigationgui->track_robot) {
		if (fl_get_button(obj)==PUSHED) localNavigation_trackrobot=TRUE;
		else localNavigation_trackrobot=FALSE;
	} else if (obj == fd_localNavigationgui->trailButton) {
		if (fl_get_button(obj)==PUSHED) DISPLAY_ESTELA = 0x80UL;
		else DISPLAY_ESTELA = 0x0;
	} else if (obj == fd_localNavigationgui->laserButton) {
		if (fl_get_button(obj)==PUSHED) DISPLAY_LASER = 0x08UL;
		else DISPLAY_LASER = 0x0;
	} else if (obj == fd_localNavigationgui->memoryButton) {
		if (fl_get_button(obj)==PUSHED) DISPLAY_MEMORY = 0x40UL;
		else DISPLAY_MEMORY = 0x0;
	} else if (obj == fd_localNavigationgui->secWindowButton) {
		if (fl_get_button(obj)==PUSHED) DISPLAY_VENTANA = 0x60UL;
		else DISPLAY_VENTANA = 0x0;
	} else if (obj == fd_localNavigationgui->gppWindowButton) {
		if (fl_get_button(obj)==PUSHED) DISPLAY_VENTANAGPP = 0x22UL;
		else DISPLAY_VENTANAGPP = 0x0;
	} else if (obj == fd_localNavigationgui->forcesButton) {
		if (fl_get_button(obj)==PUSHED) DISPLAY_FUERZAS = 0x24UL;
		else DISPLAY_FUERZAS = 0x0;
	} else if (obj == fd_localNavigationgui->gradientGridButton) {
		if (fl_get_button(obj)==PUSHED) DISPLAY_GRADIENTGRID = 0x26UL;
		else DISPLAY_GRADIENTGRID = 0x0;
	} else if (obj == fd_localNavigationgui->memoryGridButton) {
		if (fl_get_button(obj)==PUSHED) DISPLAY_OCCUPANCYGRID = 0x26UL;
		else DISPLAY_OCCUPANCYGRID = 0x0;
	} else if (obj == fd_localNavigationgui->vff) { // comportamiento VFF
		localNavigationBehavior = vff;
		fl_set_button(fd_localNavigationgui->secWindow, 0);
		fl_set_button(fd_localNavigationgui->forgetMemory, 0);
		fl_set_button(fd_localNavigationgui->localGPP, 0);
	} else if (obj == fd_localNavigationgui->secWindow) { // comportamiento Ventana
		localNavigationBehavior = secWindow;
		fl_set_button(fd_localNavigationgui->vff, 0);
		fl_set_button(fd_localNavigationgui->forgetMemory, 0);
		fl_set_button(fd_localNavigationgui->localGPP, 0);
	} else if (obj == fd_localNavigationgui->forgetMemory) { // comportamiento VFF + olvido temporal puntos
		localNavigationBehavior = forgetMemory;
		fl_set_button(fd_localNavigationgui->secWindow, 0);
		fl_set_button(fd_localNavigationgui->vff, 0);
		fl_set_button(fd_localNavigationgui->localGPP, 0);
	} else if (obj == fd_localNavigationgui->localGPP) { // comportamiento GPP local
		localNavigationBehavior = localGPP;
		fl_set_button(fd_localNavigationgui->secWindow, 0);
		fl_set_button(fd_localNavigationgui->forgetMemory, 0);
		fl_set_button(fd_localNavigationgui->vff, 0);
	}
}

void localNavigation_guidisplay() {
	char text[80]="";
	static float k=0;
	int i;
	Tvoxel aa,bb;
	static XPoint targetgraf;
	XPoint a,b;

	/* slow refresh of the complete localNavigation gui, needed because incremental refresh misses window occlusions */
	if (((localNavigation_iteracion_display*localNavigation_cycle>FORCED_REFRESH) ||
	 		((forcedOccupancyRefresh&&(localNavigation_display_state&DISPLAY_OCCUPANCYGRID)!=0))) && (k != 0)) {
		localNavigation_iteracion_display=0;
		localNavigation_visual_refresh=TRUE;
	}
	else localNavigation_iteracion_display++;

	k=k+1.;
	sprintf(text,"%.1f",k);

	fl_winset(localNavigation_canvas_win); 

	if ((localNavigation_trackrobot==TRUE)&&
		((fabs(myencoders[0]+localNavigation_odometrico[0])>(localNavigation_rango/4.))||
		(fabs(myencoders[1]+localNavigation_odometrico[1])>(localNavigation_rango/4.)))) {

		localNavigation_odometrico[0]=-myencoders[0];
		localNavigation_odometrico[1]=-myencoders[1];
		localNavigation_visual_refresh = TRUE;
	}

	if (localNavigation_visual_refresh==TRUE)	{
		fl_rectbound(0,0,localNavigation_width,localNavigation_height,FL_WHITE);   
		XFlush(display);

		if ((localNavigation_display_state&DISPLAY_OCCUPANCYGRID) != 0) {
			initPreviousOccupancy ();
			forcedOccupancyRefresh = FALSE;
		} else if ((localNavigation_display_state&DISPLAY_GRADIENTGRID) != 0) {
			forcedGradientRefresh = TRUE;
		}
	}

	/* the grid at the floor */
	fl_set_foreground(localNavigationgui_gc,FL_LEFT_BCOL); 
	for(i=0;i<31;i++)	{
		aa.x=-15000.+(float)i*1000;
		aa.y=-15000.;
		bb.x=-15000.+(float)i*1000;
		bb.y=15000.;
		xy2localNavigationcanvas(aa,&a);
		xy2localNavigationcanvas(bb,&b);
		XDrawLine(display,localNavigation_canvas_win,localNavigationgui_gc,a.x,a.y,b.x,b.y);
		aa.y=-15000.+(float)i*1000;
		aa.x=-15000.;
		bb.y=-15000.+(float)i*1000;
		bb.x=15000.;
		xy2localNavigationcanvas(aa,&a);
		xy2localNavigationcanvas(bb,&b);
		XDrawLine(display,localNavigation_canvas_win,localNavigationgui_gc,a.x,a.y,b.x,b.y);
	}

	/* CALCULO DE FUERZAS, PARA SU PINTADO */
	fuerzaResultante.x=myencoders[0]+xRes;
	fuerzaResultante.y=myencoders[1]+yRes;
	repulsiva.x=myencoders[0]+xRep;
	repulsiva.y=myencoders[1]+yRep;
	atractiva.x=myencoders[0]+xAtr;
	atractiva.y=myencoders[1]+yAtr;
	robott.x=myencoders[0];
	robott.y=myencoders[1];	

	/* VISUALIZACION DE LA ESTELA DEL ROBOT */
	if ((localNavigation_display_state&DISPLAY_ESTELA)!=0) {
		numEstelas=0;
		for	(i=0;i<tamMemoria/2;i++) {
				xy2localNavigationcanvas(estelaRecorrido[i].pos,&coordGrafEstela[numEstelas]);
				numEstelas++;
				if (estelaRecorrido[i].color == VFF_COLOR)
					fl_set_foreground(localNavigationgui_gc,FL_CYAN);
				else
					fl_set_foreground(localNavigationgui_gc,FL_MAGENTA);
				XDrawPoint(display,localNavigation_canvas_win,localNavigationgui_gc,coordGrafEstela[numEstelas].x,coordGrafEstela[numEstelas].y);
		}
	}

	/* VISUALIZACION DE UNA INSTANTANEA DE MEMORIA DE PUNTOS */
	if ((((localNavigation_display_state&DISPLAY_MEMORY)!=0)&&(localNavigation_visual_refresh==FALSE)) || 		(visual_delete_localNavigation_memory==TRUE)) {
		//pinto en blanco los puntos anteriores
		fl_set_foreground(localNavigationgui_gc,FL_WHITE);
		XDrawPoints(display,localNavigation_canvas_win,localNavigationgui_gc,coord_graf_memoria,num_puntos,CoordModeOrigin);
	}

	/* VISUALIZACION DE UNA INSTANTANEA LASER */
	if ((((localNavigation_display_state&DISPLAY_LASER)!=0)&&(localNavigation_visual_refresh==FALSE))
	|| (visual_delete_localNavigation_laser==TRUE))	{
		// Limpiamos los puntos viejos  
		fl_set_foreground(localNavigationgui_gc,FL_WHITE); 
		XDrawPoints(display,localNavigation_canvas_win,localNavigationgui_gc,localNavigation_laser_dpy,NUM_LASER,CoordModeOrigin);
	}

	/* VISUALIZACION DE UNA INSTANTANEA DE VENTANA DE SEGURIDAD */
	if ((((localNavigation_display_state&DISPLAY_VENTANA)!=0)&&(localNavigation_visual_refresh==FALSE)) || 		(visual_delete_localNavigation_ventana==TRUE)) {
		//pinto en blanco los puntos anteriores
		fl_set_foreground(localNavigationgui_gc,FL_WHITE);
		XDrawPoints(display,localNavigation_canvas_win,localNavigationgui_gc,coord_graf_ventana,180,CoordModeOrigin);
		XDrawPoints(display,localNavigation_canvas_win,localNavigationgui_gc,coord_graf_hueco,180,CoordModeOrigin);
		XDrawPoints(display,localNavigation_canvas_win,localNavigationgui_gc,coord_graf_frente,180,CoordModeOrigin);
	}

	/* VISUALIZACION DE UNA INSTANTANEA DE VENTANA DE MINI GPP */
	if ((((localNavigation_display_state&DISPLAY_VENTANAGPP)!=0)&&(localNavigation_visual_refresh==FALSE)) || 		(visual_delete_localNavigation_ventanaGPP==TRUE)) {
		//pinto en blanco los puntos anteriores
		fl_set_foreground(localNavigationgui_gc,FL_WHITE);
		XDrawPoints(display,localNavigation_canvas_win,localNavigationgui_gc,coord_graf_ventanaGPP,totalPuntos,CoordModeOrigin);
	}

	/* VISUALIZACION DE UNA INSTANTANEA DE LA REJILLA DE MEMORIA *//*
	if ((((localNavigation_display_state&DISPLAY_OCCUPANCYGRID)!=0)&&(localNavigation_visual_refresh==FALSE)&&(refreshCounter%REFRESH_TIMER==0)) || 		(visual_delete_localNavigation_grid==TRUE)) {
		fl_set_foreground(localNavigationgui_gc,FL_WHITE);
		for (i = 0 ; i < ((occupancyGrid->size)*(occupancyGrid->size)) ; i++) {
//			XDrawRectangles(display,localNavigation_canvas_win,localNavigationgui_gc,&(mapaGrafico[i]),1);
//			XFillRectangles(display,localNavigation_canvas_win,localNavigationgui_gc,&(mapaGrafico[i]),1);
			XDrawPoint(display,localNavigation_canvas_win,localNavigationgui_gc,mapaGrafico[i].x,mapaGrafico[i].y);
		}
	}*/

	/* VISUALIZACION DE UNA INSTANTANEA DE LA REJILLA DE GRADIENTE *//*
	if ((((localNavigation_display_state&DISPLAY_GRADIENTGRID)!=0)&&(localNavigation_visual_refresh==FALSE)&&(refreshCounter%REFRESH_TIMER==0)) || 		(visual_delete_localNavigation_grid==TRUE)) {
		fl_set_foreground(localNavigationgui_gc,FL_WHITE);
		if (gradientGrid != NULL) {
			for (i = 0 ; i < ((gradientGrid->size)*(gradientGrid->size)) ; i++) {
				XDrawPoint(display,localNavigation_canvas_win,localNavigationgui_gc,gradienteGrafico[i].x,gradienteGrafico[i].y);
			}
		}
	}
*/
	// Pinto los nuevos puntos de la VENTANA DE SEGURIDAD
	if ((localNavigation_display_state&DISPLAY_VENTANA)!=0) {
		rellenarVentanaSeguridad (); // obtengo las ventanas actuales
		rellenarHuecoSeguridad ();
		rellenarFrenteSeguridad ();

		for (i = 0; i < 180; i ++) { // paso las ventanas actuales a coordenadas gráficas
			xy2localNavigationcanvas(ventanaSeguridad[i],&coord_graf_ventana[i]);
			xy2localNavigationcanvas(huecoSeguridad[i],&coord_graf_hueco[i]);
			xy2localNavigationcanvas(frenteSeguridad[i],&coord_graf_frente[i]);
		}

		fl_set_foreground(localNavigationgui_gc,FL_BLACK); // y finalmente dibujo las ventanas
		XDrawPoints(display,localNavigation_canvas_win,localNavigationgui_gc,coord_graf_ventana,180,CoordModeOrigin);
		XDrawPoints(display,localNavigation_canvas_win,localNavigationgui_gc,coord_graf_hueco,180,CoordModeOrigin);
		XDrawPoints(display,localNavigation_canvas_win,localNavigationgui_gc,coord_graf_frente,180,CoordModeOrigin);
	}

	// Pinto los nuevos puntos de la MEMORIA DE PUNTOS
	if ((localNavigation_display_state&DISPLAY_MEMORY)!=0) {
		num_puntos=0;
		for	(i=0;i<tamMemoria;i++) {
			if(validos[i]) {  
				xy2localNavigationcanvas(memoria[i],&coord_graf_memoria[num_puntos]);
				num_puntos++;
			}
		}
		fl_set_foreground(localNavigationgui_gc,FL_DARKORANGE);
		XDrawPoints(display,localNavigation_canvas_win,localNavigationgui_gc,coord_graf_memoria,num_puntos,CoordModeOrigin);
	}

	// Pinto los nuevos puntos de los LASERS
	if ((localNavigation_display_state&DISPLAY_LASER)!=0) {
		for(i=0;i<NUM_LASER;i++) {
			laser2xy(i,mylaser[i],&aa,&myencoders[0]);
			xy2localNavigationcanvas(aa,&localNavigation_laser_dpy[i]);
		}
		fl_set_foreground(localNavigationgui_gc,FL_BLUE);
		XDrawPoints(display,localNavigation_canvas_win,localNavigationgui_gc,localNavigation_laser_dpy,NUM_LASER,CoordModeOrigin);
	}

	// Pinto los nuevos puntos de la REJILLA DE MEMORIA GPP LOCAL
	if ((((localNavigation_display_state&DISPLAY_OCCUPANCYGRID)!=0)&&(localNavigation_visual_refresh==FALSE)&&(refreshCounter%REFRESH_TIMER==0)) || (visual_delete_localNavigation_grid==TRUE)) {
		updateRejillaGPP(); // tenemos occupancyGrid actualizado
		drawRejillaGPP (); // compararemos y sólo dibujaremos los "novedosos"
	}

	// Pinto los nuevos puntos de la REJILLA GRADIENTE GPP LOCAL
	if ((((localNavigation_display_state&DISPLAY_GRADIENTGRID)!=0)&&(localNavigation_visual_refresh==FALSE)&&(refreshCounter%REFRESH_TIMER==0))&&(forcedGradientRefresh) || (visual_delete_localNavigation_grid==TRUE)) {
		forcedGradientRefresh = FALSE;
		drawRejillaGradienteGPP ();
	}

	// Pinto los nuevos puntos de la VENTANA DE MINI GPP
	if ((localNavigation_display_state&DISPLAY_VENTANAGPP)!=0) {
		for (i = 0; i < totalPuntos; i ++) { // paso las ventanas actuales a coordenadas gráficas
			xy2localNavigationcanvas(ventanaGPP[i],&coord_graf_ventanaGPP[i]);
		}

		fl_set_foreground(localNavigationgui_gc,FL_BLACK); // y finalmente dibujo las ventanas
		XDrawPoints(display,localNavigation_canvas_win,localNavigationgui_gc,coord_graf_ventanaGPP,totalPuntos,CoordModeOrigin);
	}

	/* SECCION PINTADO DE FUERZAS */
	if ((distanciaObjetivo!=0) && ((localNavigation_display_state&DISPLAY_FUERZAS)!=0)) {
		//Borro las fuerzas de la iteracion anterior
		fl_set_foreground(localNavigationgui_gc,FL_WHITE);
		XDrawLine(display,localNavigation_canvas_win,localNavigationgui_gc,graf_robot.x,graf_robot.y,coord_graf_objetivo.x,coord_graf_objetivo.y);

		fl_set_foreground(localNavigationgui_gc,FL_WHITE);
		XDrawLine(display,localNavigation_canvas_win,localNavigationgui_gc,graf_robot.x,graf_robot.y,graf_res.x,graf_res.y);

		fl_set_foreground(localNavigationgui_gc,FL_WHITE);
		XDrawLine(display,localNavigation_canvas_win,localNavigationgui_gc,graf_robot.x,graf_robot.y,graf_rep.x,graf_rep.y);

		fl_set_foreground(localNavigationgui_gc,FL_WHITE);
		XDrawLine(display,localNavigation_canvas_win,localNavigationgui_gc,graf_robot.x,graf_robot.y,graf_atr.x,graf_atr.y);

		//pinto las nuevas lineas
		xy2localNavigationcanvas(miObjetivo,&coord_graf_objetivo);
		xy2localNavigationcanvas(robott,&graf_robot);
		xy2localNavigationcanvas(fuerzaResultante,&graf_res);
		xy2localNavigationcanvas(repulsiva,&graf_rep);
		xy2localNavigationcanvas(atractiva,&graf_atr);

		fl_set_foreground(localNavigationgui_gc,FL_YELLOW); /* OBJETIVO */
		XDrawLine(display,localNavigation_canvas_win,localNavigationgui_gc,graf_robot.x,graf_robot.y,coord_graf_objetivo.x,coord_graf_objetivo.y);

		fl_set_foreground(localNavigationgui_gc,FL_BLUE); /* FUERZA RESULTANTE */
		XDrawLine(display,localNavigation_canvas_win,localNavigationgui_gc,graf_robot.x,graf_robot.y,graf_res.x,graf_res.y);

		fl_set_foreground(localNavigationgui_gc,FL_RED); /* FUERZA REPULSIVA */
		XDrawLine(display,localNavigation_canvas_win,localNavigationgui_gc,graf_robot.x,graf_robot.y,graf_rep.x,graf_rep.y);

		fl_set_foreground(localNavigationgui_gc,FL_GREEN); /* FUERZA ATRACTIVA */
		XDrawLine(display,localNavigation_canvas_win,localNavigationgui_gc,graf_robot.x,graf_robot.y,graf_atr.x,graf_atr.y);
	}
	/* FIN PINTADO DE FUERZAS */

	/* VISUALIZACION: PINTAR O BORRAR DE EL PROPIO ROBOT.
	Siempre hay un repintado total. Esta es la ultima estructura que se se pinta, para que ninguna otra se solape encima 		*/

	if ((((localNavigation_display_state&DISPLAY_ROBOT)!=0) &&(localNavigation_visual_refresh==FALSE))
	|| (visual_delete_localNavigation_ego==TRUE))	{  
		fl_set_foreground(localNavigationgui_gc,FL_WHITE);

		/* clean last robot, but only if there wasn't a total refresh. In case of total refresh the white rectangle already cleaned all */
		for(i=0;i<numlocalNavigation_ego;i++) 
			XDrawLine(display,localNavigation_canvas_win,localNavigationgui_gc,localNavigation_ego[i].x,localNavigation_ego[i].y,localNavigation_ego[i+1].x,localNavigation_ego[i+1].y);
	}

	if ((localNavigation_display_state&DISPLAY_ROBOT)!=0) {
		fl_set_foreground(localNavigationgui_gc,FL_MAGENTA);
		/* relleno los nuevos */
		us2xy(15,0.,0.,&aa,&myencoders[0]);
		xy2localNavigationcanvas(aa,&localNavigation_ego[0]);
		us2xy(3,0.,0.,&aa,&myencoders[0]);
		xy2localNavigationcanvas(aa,&localNavigation_ego[1]);
		us2xy(4,0.,0.,&aa,&myencoders[0]);
		xy2localNavigationcanvas(aa,&localNavigation_ego[2]);
		us2xy(8,0.,0.,&aa,&myencoders[0]);
		xy2localNavigationcanvas(aa,&localNavigation_ego[3]);
		us2xy(15,0.,0.,&aa,&myencoders[0]);
		xy2localNavigationcanvas(aa,&localNavigation_ego[EGOMAX-1]);
		for(i=0;i<NUM_SONARS;i++) {
			us2xy((15+i)%NUM_SONARS,0.,0.,&aa,&myencoders[0]); /* Da en el Tvoxel aa las coordenadas del sensor, pues es distancia 0 */
			xy2localNavigationcanvas(aa,&localNavigation_ego[i+4]);       
		}

		/* pinto los nuevos */
		numlocalNavigation_ego=EGOMAX-1;
		for (i=0;i<numlocalNavigation_ego;i++) 
			XDrawLine(display,localNavigation_canvas_win,localNavigationgui_gc,localNavigation_ego[i].x,localNavigation_ego[i].y,localNavigation_ego[i+1].x,localNavigation_ego[i+1].y);

		fl_set_foreground(localNavigationgui_gc,FL_BLUE);
		XDrawPoints(display,localNavigation_canvas_win,localNavigationgui_gc,GraphicGlobalDestination,9,CoordModeOrigin);		
	}

	/* clear all flags. If they were set at the beginning, they have been already used in this iteration */
	localNavigation_visual_refresh=FALSE;
	visual_delete_localNavigation_us=FALSE; 
	visual_delete_localNavigation_laser=FALSE; 
	visual_delete_localNavigation_ego=FALSE;
	visual_delete_localNavigation_memory = FALSE;
	visual_delete_localNavigation_ventana = FALSE;
	visual_delete_localNavigation_ventanaGPP = FALSE;
	visual_delete_localNavigation_grid = FALSE;

	refreshCounter ++;
}

/* callback function for button pressed inside the canvas object*/
int localNavigation_button_pressed_on_micanvas(FL_OBJECT *ob, Window win, int win_width, int win_height, XEvent *xev, void *user_data)
{
  unsigned int keymap;
  float ygraf, xgraf;
	int i, f, c;
	Tvoxel temporalPoint;
  FL_Coord x,y;

  /* in order to know the mouse button that created the event */
  localNavigation_mouse_button=xev->xkey.keycode;

  if(localNavigation_canvas_mouse_button_pressed==0){
    if ((localNavigation_mouse_button==MOUSEMIDDLE))
      {
	/* Target for VFF localNavigation getting mouse coordenates */
      fl_get_win_mouse(win,&x,&y,&keymap);
      ygraf=((float) (localNavigation_height/2-y))/localNavigation_escala;
      xgraf=((float) (x-localNavigation_width/2))/localNavigation_escala;
      oldvfftarget.x=vfftarget.x;
      oldvfftarget.y=vfftarget.y;
      vfftarget.y=(ygraf-localNavigation_odometrico[1])*localNavigation_odometrico[3]+(-xgraf+localNavigation_odometrico[0])*localNavigation_odometrico[4];
      vfftarget.x=(ygraf-localNavigation_odometrico[1])*localNavigation_odometrico[4]+(xgraf-localNavigation_odometrico[0])*localNavigation_odometrico[3];
     }
    else if((localNavigation_mouse_button==MOUSELEFT)||(localNavigation_mouse_button==MOUSERIGHT)){
      /* a button has been pressed */
      localNavigation_canvas_mouse_button_pressed=1;
      
      /* getting mouse coordenates. win will be always the canvas window, because this callback has been defined only for that canvas */  
      fl_get_win_mouse(win,&localNavigation_x_canvas,&localNavigation_y_canvas,&keymap);
      old_localNavigation_x_canvas=localNavigation_x_canvas;
      old_localNavigation_y_canvas=localNavigation_y_canvas;
      
      /* from graphical coordinates to spatial ones */
      ygraf=((float) (localNavigation_height/2-localNavigation_y_canvas))/localNavigation_escala;
      xgraf=((float) (localNavigation_x_canvas-localNavigation_width/2))/localNavigation_escala;
      localNavigation_mouse_y=(ygraf-localNavigation_odometrico[1])*localNavigation_odometrico[3]+(-xgraf+localNavigation_odometrico[0])*localNavigation_odometrico[4];
      localNavigation_mouse_x=(ygraf-localNavigation_odometrico[1])*localNavigation_odometrico[4]+(xgraf-localNavigation_odometrico[0])*localNavigation_odometrico[3];
      localNavigation_mouse_new=1;

			/* Guardamos el OBJETIVO pinchado con ratón */
			myGlobalDestination.x = localNavigation_mouse_x;
			myGlobalDestination.y = localNavigation_mouse_y;

			i = 0;

			for (f = -1; f <= 1 ; f ++) {
				for (c = -1; c <= 1; c ++) {
					temporalPoint.x = myGlobalDestination.x+f;
					temporalPoint.y = myGlobalDestination.y+c;

					xy2localNavigationcanvas (temporalPoint, &GraphicGlobalDestination[i]);
					i ++;
				}
			}

			miObjetivo.x=localNavigation_mouse_x;
			miObjetivo.y=localNavigation_mouse_y;
      
    }else if(localNavigation_mouse_button==MOUSEWHEELDOWN){
      /* a button has been pressed */
      localNavigation_canvas_mouse_button_pressed=1;

      /* modifing scale */
      localNavigation_rango-=1000;
      if(localNavigation_rango<=RANGO_MIN) localNavigation_rango=RANGO_MIN;
      localNavigation_visual_refresh = TRUE; /* activa el flag que limpia el fondo de la pantalla y repinta todo */ 
      localNavigation_escala = localNavigation_width /localNavigation_rango;

    }else if(localNavigation_mouse_button==MOUSEWHEELUP){
      /* a button has been pressed */
      localNavigation_canvas_mouse_button_pressed=1;

      /* modifing scale */
      localNavigation_rango+=1000;
      if(localNavigation_rango>=RANGO_MAX) localNavigation_rango=RANGO_MAX;
      localNavigation_visual_refresh = TRUE; /* activa el flag que limpia el fondo de la pantalla y repinta todo */ 
      localNavigation_escala = localNavigation_width /localNavigation_rango;
    }
  }
  return 0;
}

/* callback function for mouse motion inside the canvas object*/
int localNavigation_mouse_motion_on_micanvas(FL_OBJECT *ob, Window win, int win_width, int win_height, XEvent *xev, void *user_data)
{  
  float diff_x,diff_y,diff_w;
  unsigned int keymap;
  float sqrt_value,acos_value;

  if(localNavigation_canvas_mouse_button_pressed==1){

    /* getting mouse coordenates. win will be always the canvas window, because this callback has been defined only for that canvas */  
    fl_get_win_mouse(win,&localNavigation_x_canvas,&localNavigation_y_canvas,&keymap);
	}
  return 0;
}

/* callback function for button released inside the canvas object*/
int localNavigation_button_released_on_micanvas(FL_OBJECT *ob, Window win, int win_width, int win_height, XEvent *xev, void *user_data)
{
  if(localNavigation_canvas_mouse_button_pressed==1){
    /* a button has been released */
    localNavigation_canvas_mouse_button_pressed=0;
  }
  return 0;
}

void localNavigation_guisuspend_aux(void)
{
  /* to make a safety stop when the robot is being teleoperated from GUI */
  mydelete_buttonscallback(localNavigation_guibuttons);
  mydelete_displaycallback(localNavigation_guidisplay);
  fl_hide_form(fd_localNavigationgui->localNavigationgui);
}

void localNavigation_guisuspend(){
  static callback fn=NULL;
  if (fn==NULL){
    if ((fn=(callback)myimport ("graphics_xforms", "suspend_callback"))!=NULL){
      fn ((gui_function)localNavigation_guisuspend_aux);
    }
  }
  else{
    fn ((gui_function)localNavigation_guisuspend_aux);
  }
}

void localNavigation_guiresume_aux(void)
{
  static int k=0;
  XGCValues gc_values;

  if (k==0) /* not initialized */
    {
      k++;
      /* Coord del sistema odometrico respecto del visual */
      localNavigation_odometrico[0]=0.;
      localNavigation_odometrico[1]=0.;
      localNavigation_odometrico[2]=0.;
      localNavigation_odometrico[3]= cos(0.);
      localNavigation_odometrico[4]= sin(0.);

      localNavigation_display_state = localNavigation_display_state | DISPLAY_LASER;
      localNavigation_display_state = localNavigation_display_state | DISPLAY_SONARS;
      localNavigation_display_state = localNavigation_display_state | DISPLAY_ROBOT;
      localNavigation_display_state = localNavigation_display_state | DISPLAY_MEMORY;
			localNavigation_display_state = localNavigation_display_state | DISPLAY_ESTELA;
			localNavigation_display_state = localNavigation_display_state | DISPLAY_FUERZAS;
			localNavigation_display_state = localNavigation_display_state | DISPLAY_VENTANA;
			localNavigation_display_state = localNavigation_display_state | DISPLAY_VENTANAGPP;
			localNavigation_display_state = localNavigation_display_state | DISPLAY_GRADIENTGRID;
			localNavigation_display_state = localNavigation_display_state | DISPLAY_OCCUPANCYGRID;

      fd_localNavigationgui = create_form_localNavigationgui();
      fl_set_form_position(fd_localNavigationgui->localNavigationgui,400,50);
      fl_show_form(fd_localNavigationgui->localNavigationgui,FL_PLACE_POSITION,FL_FULLBORDER,"GUIA MUSEO. NAVEGACIÓN LOCAL");
      localNavigation_canvas_win= FL_ObjWin(fd_localNavigationgui->micanvas);
      gc_values.graphics_exposures = False;
      localNavigationgui_gc = XCreateGC(display, localNavigation_canvas_win, GCGraphicsExposures, &gc_values);  

      /* canvas handlers */
      fl_add_canvas_handler(fd_localNavigationgui->micanvas,ButtonPress,localNavigation_button_pressed_on_micanvas,NULL);
      fl_add_canvas_handler(fd_localNavigationgui->micanvas,ButtonRelease,localNavigation_button_released_on_micanvas,NULL);
      fl_add_canvas_handler(fd_localNavigationgui->micanvas,MotionNotify,localNavigation_mouse_motion_on_micanvas,NULL);

			fl_set_button(fd_localNavigationgui->forgetMemory, RELEASED);
			fl_set_button(fd_localNavigationgui->secWindow, RELEASED);
			fl_set_button(fd_localNavigationgui->vff, RELEASED);
			fl_set_button(fd_localNavigationgui->localGPP, PUSHED);
			fl_set_button(fd_localNavigationgui->trailButton, RELEASED);
			fl_set_button(fd_localNavigationgui->laserButton, RELEASED);
			fl_set_button(fd_localNavigationgui->memoryButton, RELEASED);
			fl_set_button(fd_localNavigationgui->secWindowButton, RELEASED);
			fl_set_button(fd_localNavigationgui->gppWindowButton, RELEASED);
			fl_set_button(fd_localNavigationgui->forcesButton, RELEASED);
			fl_set_button(fd_localNavigationgui->gradientGridButton, RELEASED);
			fl_set_button(fd_localNavigationgui->memoryGridButton, RELEASED);
    }
  else 
    {
      fl_show_form(fd_localNavigationgui->localNavigationgui,FL_PLACE_POSITION,FL_FULLBORDER,"GUIA MUSEO. NAVEGACIÓN LOCAL");
      localNavigation_canvas_win= FL_ObjWin(fd_localNavigationgui->micanvas);
    }

  /* Empiezo con el canvas en blanco */
  localNavigation_width = fd_localNavigationgui->micanvas->w;
  localNavigation_height = fd_localNavigationgui->micanvas->h;
  fl_winset(localNavigation_canvas_win); 
  fl_rectbound(0,0,localNavigation_width,localNavigation_height,FL_WHITE);   
  /*  XFlush(display);*/
  
  localNavigation_trackrobot=TRUE;
  fl_set_button(fd_localNavigationgui->track_robot,PUSHED);

	fl_set_slider_bounds(fd_localNavigationgui->escala,RANGO_MAX,RANGO_MIN);
	fl_set_slider_filter(fd_localNavigationgui->escala,localNavigation_range); /* Para poner el valor del slider en metros en pantalla */
  fl_set_slider_value(fd_localNavigationgui->escala,RANGO_INICIAL);
  localNavigation_escala = localNavigation_width/localNavigation_rango;
  
	fl_set_positioner_xvalue(fd_localNavigationgui->center,0.5);
	fl_set_positioner_yvalue(fd_localNavigationgui->center,0.5);

  myregister_buttonscallback(localNavigation_guibuttons);
  myregister_displaycallback(localNavigation_guidisplay);
}

void localNavigation_guiresume(){
   static callback fn=NULL;
   if (fn==NULL){
      if ((fn=(callback)myimport ("graphics_xforms", "resume_callback"))!=NULL){
         fn ((gui_function)localNavigation_guiresume_aux);
      }
   }
   else{
      fn ((gui_function)localNavigation_guiresume_aux);
   }
}
