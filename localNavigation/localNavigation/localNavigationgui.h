/** Header file generated with fdesign on Sun Sep 21 12:54:45 2008.**/

#ifndef FD_localNavigationgui_h_
#define FD_localNavigationgui_h_

/** Callbacks, globals and object handlers **/


/**** Forms and Objects ****/
typedef struct {
	FL_FORM *localNavigationgui;
	void *vdata;
	char *cdata;
	long  ldata;
	FL_OBJECT *trailButton;
	FL_OBJECT *vff;
	FL_OBJECT *exit;
	FL_OBJECT *micanvas;
	FL_OBJECT *stop;
	FL_OBJECT *secWindow;
	FL_OBJECT *forgetMemory;
	FL_OBJECT *localGPP;
	FL_OBJECT *center;
	FL_OBJECT *track_robot;
	FL_OBJECT *escala;
	FL_OBJECT *laserButton;
	FL_OBJECT *memoryButton;
	FL_OBJECT *secWindowButton;
	FL_OBJECT *gppWindowButton;
	FL_OBJECT *forcesButton;
	FL_OBJECT *gradientGridButton;
	FL_OBJECT *memoryGridButton;
} FD_localNavigationgui;

extern FD_localNavigationgui * create_form_localNavigationgui(void);

#endif /* FD_localNavigationgui_h_ */
