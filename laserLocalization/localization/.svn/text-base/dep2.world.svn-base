# Author: Julio M. Vega
# Date: 29 Octubre 2007

# Desc: 1 pioneer robot with laser	
# CVS: $Id: simple.world,v 1.63 2006/03/22 00:22:44 rtv Exp $

# defines Pioneer-like robots
include "pioneer.inc"

# defines 'map' object used for floorplans
include "map.inc"

# defines sick laser
include "sick.inc"

# size of the world in meters
size [30 30]

# set the resolution of the underlying raytrace model in meters
resolution 0.02

# update the screen every 10ms (we need fast update for the stest demo)
gui_interval 20

# configure the GUI window
window
( 
  size [ 591.000 638.000 ] 
  center [5.5 -10] 
  scale 0.028 
)

# load an environment bitmap
map
( 
  bitmap "bitmaps/dep2.pgm"
  size [30 30]
  name "dep2"
  color "SaddleBrown"
)

# Éstas son las mesas alargás
define mesa model(
  size [ 2.0 0.75 ]
  gripper_return 1
  gui_movemask 3
  gui_nose 0
  fiducial_return 10
)

# Éstas son las mesas rechonchas
define mesa2 model(
  size [ 1.5 1.1 ]
  gripper_return 1
  gui_movemask 3
  gui_nose 0
  fiducial_return 10
)

# Aquí se define cómo tiene la tarra una persona
define cabeza laser(
  color "goldenrod"
  size [ 0.18 0.18 ]	
)

# La definición de un cuerpazo humano
define humano model(
  size [0.6 0.2]

  origin [0.0 0.0 0]

  mass 70.0
	
  polygons 2
  polygon[0].points 8
  polygon[0].point[0] [  0.15  0.05 ]
  polygon[0].point[1] [  0.07  0.15 ]
  polygon[0].point[2] [ -0.07  0.15 ]
  polygon[0].point[3] [ -0.15  0.05 ]
  polygon[0].point[4] [ -0.15 -0.05 ]
  polygon[0].point[5] [ -0.07 -0.15 ]
  polygon[0].point[6] [  0.07 -0.15 ]
  polygon[0].point[7] [  0.15 -0.05 ]
	cabeza ()
)

# mesa fotocopiadora
mesa ( pose [ 2 9.9 0 ] color "SandyBrown" )

# personal del departamento. Se puede intetificar a todos...
humano ( pose [ 9.3 4.2 90] color "SandyBrown" )
humano ( pose [ 8.5 -2.5 90] color "ForestGreen" )
humano ( pose [ 9.5 -9.5 0] color "OliveDrab" )
humano ( pose [ 14 -1 45] color "LightYellow" )
humano ( pose [ 4 -12.7 0] color "RosyBrown" )
humano ( pose [ 14 -11 90] color "peru" )
humano ( pose [ 14 -13 45] color "DarkSalmon" )
humano ( pose [ 5 -12.7 0] color "OrangeRed" )
humano ( pose [ 6 -12.7 0] color "coral" )
humano ( pose [ 6.7 -12.5 90] color "maroon" )
humano ( pose [ 4 10.2 0] color "MediumVioletRed" )
humano ( pose [ -10.3 -1 0] color "magenta" )
humano ( pose [ -10.3 -1.6 0] color "BlueViolet" )
humano ( pose [ -9 10 90] color "seashell2" )
humano ( pose [ -8 10 -45] color "bisque3" )
humano ( pose [ -8.5 9.7 0] color "PeachPuff3" )
humano ( pose [ 3.5 9.8 -45] color "LemonChiffon2" )
humano ( pose [ -3 9 90] color "honeydew2" )

# mesas del laboratorio
mesa ( pose [ 3.97 -13.3 0 ] color "SandyBrown" )
mesa ( pose [ 5.97 -13.3 0 ] color "SandyBrown" )
mesa2 ( pose [ 7.55 -12.92 90 ] color "SandyBrown" )

# tenemos otro robot exactamente igual que el pioneer2dx, que actua como mosca cojonera
define moscaCojonera pioneer2dx ()

# create a robot
pioneer2dx
(
  name "robot1"
  color "SteelBlue1"
  pose [6 -10 0]
  sick_laser( samples 361 laser_sample_skip 4 )
)

# nos creamos nuestra moscaCojonera
moscaCojonera
(
  name "robot2"
  color "OrangeRed"
  pose [7 -5 -90]
  sick_laser( samples 361 laser_sample_skip 4 )
)
