/*
  * Esquema localizador con:
  *
  * 1 -> El algoritmo del filtro de partícula:
  * 2 -> El algortimo evolutivo multimodal  
  * 
  * Autor : Ángel Cortés Maya <angelillo_fenix@hotmail.com>
  */
#include "jde.h"
#include <forms.h>
#include "graphics_xforms.h"
#include "gridslib.h"
#include "localizationgui.h"
#include "localization.h"
#include "pioneer.h"
#include <math.h>
#include <stdlib.h>

#define DISPLAY_ROBOT 0x01UL
#define DISPLAY_SONARS 0x04UL
#define DISPLAY_LASER 0x08UL
#define DISPLAY_MY_LASER 0x02UL
#define DISPLAY_LOCALIZATION_LASER_TEORICO 0x10UL
#define DISPLAY_MAPA_MUNDO 0x20UL

#define MOUSELEFT 1
#define MOUSEMIDDLE 2
#define MOUSERIGHT 3
#define MOUSEWHEELUP 4
#define MOUSEWHEELDOWN 5
#define PI 3.141592654

/*Gui callbacks*/
pthread_mutex_t mymutex;
pthread_cond_t condition;
registerbuttons myregister_buttonscallback;
registerdisplay myregister_displaycallback;
deletebuttons mydelete_buttonscallback;
deletedisplay mydelete_displaycallback;

Display *display;
int  *myscreen;

//VARIABLES FILTRO PARTÍCULAS

// estos puntos seran la esquina inferior izqda y la superior dcha de la zona
// que sera cubierta con particulas.
Tvoxel A,B;
Tvoxel AINICIAL,BINICIAL;

int PARTICLES_ACTIVE = 0;

TParticles_population particles_population[MAX_PARTICLES];
TParticles_population particles_population_aux[MAX_PARTICLES];
TParticles_population *poblacion1,*poblacion2;
Tparticle *p_aux;
graphic_particles_type graphic_particles[MAX_PARTICLES];
TCorrelacion correlacion;



// en este array almacenamos las antiguas particulas, para borrarlas 
// en la seguiente iteracion
XRectangle particulas_antiguas[MAX_PARTICLES];
XRectangle celdillaGraf;
XRectangle *occupation_gridGraf;
XRectangle *laserloc_gridGraf;
int angulo;
int celdaa;


int dibujar=0;
float num_particles[MAX_PARTICLES];
float angulos[180];
float num_angulos[360];
float pruebas[360];
float misma_comp[MAX_PARTICLES];
float misma_comp2[MAX_PARTICLES];
int niveles_gris[(int)max_gris]; // tiene el nivel de grises
int mi_suelo = 34;
int mi_rojo = 33;
float distancia;
int cont_iteraciones=0;
Tlaser_point  my_laser[NUM_LASER];
Tlaser_point  laser_teorico_actual[NUM_LASER];
float  distancias[NUM_LASER];
float  distancias2[NUM_LASER];
float  laser_real[NUM_LASER];
int laser_correlacion[NUM_LASER];
float laser_teorico_graf[NUM_LASER];
float distance_range = 250.0;
float distancia_mov=0,distancia_obs=0;
int diff_obs_theta;
float diff_obs_y,diff_obs_x;
int disp_obs,disp_mov;
float disp_obs_x,disp_obs_y,disp_obs_theta;
float disc = (THETA_MAX/MAX_THETA_NUM); //granularidad del theta_in
int laser_real_aux[NUM_LASER];
float laser_real_prueba[NUM_LASER];
Tvoxel punto_mas_probable,mi_punto;
int pos_mas_prob;
// para incorporar el movimiento de las particulas
float disp_x,disp_y,disp_theta;
float dif_x=0,dif_y=0;
float dif_theta=0;

// para incorporar el movimiento en las trayectorias
float disp_noise_x,disp_noise_y,disp_noise_theta;
float dif_noise_x=0,dif_noise_y=0;
float dif_noise_theta=0;
float modulo_noise,ruidox,ruidoy,ruidoang;

// para incorporar el movimiento en la posicion estimada
float disp_estimate_x,disp_estimate_y,disp_estimate_theta;
float dif_estimate_x=0,dif_estimate_y=0;
float dif_estimate_theta=0;

float dim_mundo;

//variable que determina el borrado
int visual_delete_my_laser=FALSE;
int visual_delete_localization_laser_teorico=FALSE;
int visual_delete_localization_mapa_mundo=FALSE;
int borrar_particles;

int imin,imax,jmin,jmax;
Trejilla_rayo rejilla_rayo;

//FIN VARIABLES FILTRO PARTICULAS

// VARIABLES MULTIOBJETO

//puntero alista de explotadores
plistaExp explotadores=NULL;
//array de población de exploradores
Tparticle exploradores[MAX_EXPLORADORES],exploradores2[MAX_EXPLORADORES];
//puntero a exploradores
Tparticle *pexploradores=NULL;
Tparticle *pexploradores2=NULL;
//puntero a exploradores o explotadores
Tparticle *pexp1=NULL;
Tparticle *pexp2=NULL;

//lista que guarda las particulas ordenadas
plistapart ordenadas=NULL;

//estructura con los diferentes colores de los explotadores
TColorExplotador colores;


XRectangle exploradores_antiguos[MAX_EXPLORADORES];//draw_exploradores
graphic_explorers_type graphic_explorers[MAX_EXPLORADORES];

int EXPLORERS_ACTIVE = 0;
int SOLO_MAX_PROB_ACTIVE = 0;
float SVelitism,SVrandommutation,SVcrossover,SVthermalnoise;
float repulsiondistance; /* mm */
float sigma_thermalnoise;
int exploradoresReset,explotadoresReset;
int repulsionOff=TRUE;

// para incorporar el movimiento de las exploradores (expr) y explotadores (expt)
float disp_x_expr,disp_y_expr,disp_theta_expr;
float dif_x_expr=0,dif_y_expr=0;
float dif_theta_expr=0;

float disp_x_expt,disp_y_expt,disp_theta_expt;
float dif_x_expt=0,dif_y_expt=0;
float dif_theta_expt=0;

// Para aumentar memoria de la raza superior si el robot se ha desplazado

float disp_x_mem,disp_y_mem,disp_theta_mem;
float dif_x_mem=0,dif_y_mem=0;
float dif_theta_mem=0;

//variable con probabilidad de la mejor raza
float prob_max;

//mis trayectorias
Ttrayectoria mi_trayectoria,trayectoria_noise;
int pos_tray=0;
//odometria cruda y estinada exportadas
float raw_odom[5];
float encoders_ini[5];
float pose_estimate[5];
//auxiliar que guarda coordenadas de raza más saludable
float pose_aux[3];
//indica si se ha localizado al robot ya
int encontrada_pos_real=FALSE;
int pos_raza_maxima=-1;
int pos_raza_maxima_anterior=-1;

//variables slam
int guarderia;
Tvoxel pos_inicial_slam;
// FIN VARIABLES MULTIOBJETO



//VARIABLES LECTURA MUNDO Y SU VISUALIZACION

float x_rob;
float y_rob;
 
Tgrid *laserloc_grid=NULL;

int borrar;

//FIN VARIABLES LECTURA MUNDO Y SU VISUALIZACION


int localization_id=0; 
int localization_brothers[MAX_SCHEMAS];
arbitration localization_callforarbitration;
int localization_cycle=100; /* ms */

TipoMemoriaSegmentos *mymapa;

int *mylaser;
runFn laserresume;
stopFn lasersuspend;

float *myencoders;
runFn encodersresume;
stopFn encoderssuspend;

float *myv;
float *myw;
runFn motorsresume;
stopFn motorssuspend;

enum localizationstatesdis {simple,sumatorio,correlation};
int localization_statedis;

enum localizationstatesprob {potencial,exponencial};
int localization_stateprob;

enum localizationstatesalg {aparticulas,amultiobjeto};
int localization_statealg;

enum localizationstatesmode {loc,slam};
int localization_statemode;

FD_localizationgui *fd_localizationgui;
GC localizationgui_gc;
Window  localization_canvas_win;
unsigned long localization_display_state;
int localization_visual_refresh=FALSE;
int localization_iteracion_display=0;
int localization_canvas_mouse_button_pressed=0;
int localization_mouse_button=0;
int localization_robot_mouse_motion=0;
FL_Coord localization_x_canvas,localization_y_canvas,old_localization_x_canvas,old_localization_y_canvas;
float localization_mouse_x, localization_mouse_y;
Tvoxel vfftarget,oldvfftarget;
int localization_mouse_new=0;

#define PUSHED 1
#define RELEASED 0 
#define FORCED_REFRESH 5000 /* ms */
/*Every forced_refresh the display is drawn from scratch. If it is too small it will cause flickering with grid display. No merece la pena una hebra de "display_lento" solo para repintar completamente la pantalla. */

float   localization_escala, localization_width, localization_height;
int localization_trackrobot=FALSE;
float localization_odometrico[5];
#define RANGO_MAX 60000. /* en mm */
#define RANGO_MIN 500. /* en mm */ 
#define RANGO_INICIAL 55000. /* en mm */
float localization_rango=(float)RANGO_INICIAL; /* Rango de visualizacion en milimetros */

#define EGOMAX NUM_SONARS+5
XPoint localization_ego[EGOMAX];
int numlocalization_ego=0;
int visual_delete_localization_ego=FALSE;

XPoint localization_laser_dpy[NUM_LASER];
int visual_delete_localization_laser=FALSE;

XPoint localization_us_dpy[NUM_SONARS*2];
int visual_delete_localization_us=FALSE;

#define joystick_maxRotVel 30 /* deg/sec */
#define joystick_maxTranVel 500 /* mm/sec */
float v_teleop, w_teleop;

float last_iteration=0.0,now_iteration,inicio_tiempo=0.0,actual_tiempo;
int pos_max_fit;
const char *localization_range(FL_OBJECT *ob, double value, int prec)
{
static char buf[32];
 
 sprintf(buf,"%.1f",value/1000.);
 return buf;
}

int xy2localizationcanvas(Tvoxel point, XPoint* grafico)
     /* return -1 if point falls outside the canvas */
{
float xi, yi;

xi = (point.x * localization_odometrico[3] - point.y * localization_odometrico[4]
      + localization_odometrico[0])*localization_escala;
yi = (point.x * localization_odometrico[4] + point.y * localization_odometrico[3] 
      + localization_odometrico[1])*localization_escala;
/* Con esto cambiamos al sistema de referencia de visualizacion, centrado en algun
   punto xy y con alguna orientacion definidos por odometrico. Ahora cambiamos de 
   ese sistema al del display, donde siempre hay un desplazamiento a la esquina sup.
   izda. y que las y se cuentan para abajo. */

 grafico->x = xi + localization_width/2;
 grafico->y = -yi + localization_height/2;

 if ((grafico->x <0)||(grafico->x>localization_width)) return -1; 
 if ((grafico->y <0)||(grafico->y>localization_height)) return -1; 
 return 0;
}

int absolutas2relativas(Tvoxel in, Tvoxel *out)
/*  Calcula la posicion relativa respecto del robot de un punto absoluto. 
    El robot se encuentra en robot[0], robot[1] con orientacion robot[2] 
    respecto al sistema de referencia absoluto
*/ 
{
  if (out!=NULL)
    {
      (*out).x = in.x*myencoders[3] + in.y*myencoders[4] - myencoders[0]*myencoders[3] 
	- myencoders[1]*myencoders[4];
      (*out).y = in.y*myencoders[3] - in.x*myencoders[4] + myencoders[0]*myencoders[4] 
	- myencoders[1]*myencoders[3];
    }
  return 0;
}

int relativas2absolutas(Tvoxel in, Tvoxel *out)
/*  Calcula la posicion absoluta de un punto expresado en el sistema de coordenadas solidario al robot. El robot se encuentra en robot[0], robot[1] con orientacion robot[2] respecto al sistema de referencia absoluto
 */ 
{
  if (out!=NULL)
    {
      (*out).x = in.x*myencoders[3] - in.y*myencoders[4] + myencoders[0];
      (*out).y = in.y*myencoders[3] + in.x*myencoders[4] + myencoders[1];
    }
  return 0;
}

int relativas2absolutas_reales(Tvoxel in, Tvoxel *out)
/*  Calcula la posicion absoluta de un punto expresado en el sistema de coordenadas solidario al robot. El robot se encuentra en robot[0], robot[1] con orientacion robot[2] respecto al sistema de referencia absoluto
 */ 
{
  if (out!=NULL)
    {
      (*out).x = in.x*encoders_ini[3] - in.y*encoders_ini[4] + encoders_ini[0];
      (*out).y = in.y*encoders_ini[3] + in.x*encoders_ini[4] + encoders_ini[1];
    }
  return 0;
}
int pintaSegmento(Tvoxel a, Tvoxel b, int color)
/* colores: FL_PALEGREEN */
   {
     XPoint aa,bb;
     
     fl_set_foreground(localizationgui_gc,color);
     xy2localizationcanvas(a,&aa);
     xy2localizationcanvas(b,&bb);
     XDrawLine(display,localization_canvas_win,localizationgui_gc,aa.x,aa.y,bb.x,bb.y);
     return 0;
   }

int xy2celda_rejilla(Tvoxel mi_punto,Tgrid *grid)
{
  int celda=-1;
  int i,j;
  
  int resolucion=grid->resolucion; 
  
  j=(mi_punto.x-grid->lo.x-resolucion/2)/resolucion;
  i=(mi_punto.y-grid->lo.y-resolucion/2)/resolucion;
  
  
  /*	printf("\n\nSIZE->%d lo->(%f,%f) hi->(%f,%f)\n",grid->size,grid->lo.x,grid->lo.y,grid->hi.x,grid->hi.y);  
    printf("j=> %d =%f-%f-%d/2 div %d\n",j,mi_punto.x,grid->lo.x,resolucion,resolucion);  
    printf("i=> %d =%f-%f-%d/2 div %d\n\n",i,mi_punto.y,grid->lo.y,resolucion,resolucion);  */
  
  
  if ((i>=0) && (j>=0)) celda=i*grid->size+j;
  
  return celda;
}

int xy2celda(int x, int y, int dim)
{
  int celda;
  
  celda=(y*dim)+x;
  //printf("XY2CELDA=> celda:%d=y:%d*dim:%d+x:%d\n",celda,y,dim,x); 
  return celda;
  
}

int grid2localizationcanvas(Tvoxel point, XRectangle* rectangulo, Tgrid *grid)
{
  unsigned int lado;
  XPoint esquinaGraf;
  Tvoxel esquina;
  
  esquina.x=point.x-grid->resolucion/2.;
  esquina.y=point.y+grid->resolucion/2.;

  
  
  if (xy2localizationcanvas(esquina,&esquinaGraf)>=0)
    {
     lado=(unsigned int) (grid->resolucion*localization_escala);
     if (lado==0) lado=1;
     
     rectangulo->x=esquinaGraf.x;
     rectangulo->y=esquinaGraf.y;
     rectangulo->width=lado;
     rectangulo->height=lado;
     return 0;
   }
  else return -1; /* point falls outside the canvas */ 
}
int laserloc_grid_displaysetup()
{
  int i;

  laserloc_gridGraf=malloc(sizeof(XRectangle)*laserloc_grid->size*laserloc_grid->size);
  
  if (laserloc_gridGraf==NULL) {printf("gui: not enough memory for laserloc_grid grid display\n"); exit(-1);}
  
  for(i=0;i< ((laserloc_grid->size)*(laserloc_grid->size));i++) 
    { 
      
      grid2localizationcanvas(laserloc_grid->map[i].centro,&laserloc_gridGraf[i],laserloc_grid);
    } 
  
  return 1;
}


//dibuja el laser teórico
void pintar_laser_teorico(Tlaser_point *laser_points) 
  
{
  int i=0;
  
  //for (i=0;i<NUM_LASER;i++)
	for (i=0;i<NUM_LASER;i=i+RESOLUCION_LASER_TEORICO)
    {
      fl_set_foreground(localizationgui_gc,FL_RED);
      
      //Dibujamos solo punto obstaculo	
      //xy2localizationcanvas(laser_points[i].teorico_point,&laser_points[i].graf_point);	
      //XDrawPoint(display,localization_canvas_win,localizationgui_gc,laser_points[i].graf_point.x,laser_points[i].graf_point.y);	
      //dibujamos recta hacia obstaculo
      xy2localizationcanvas(laser_points[i].teorico_point,&laser_points[i].graf_point);	
      xy2localizationcanvas(laser_points[i].robot_point,&laser_points[i].graf_point_robot);			
      XDrawLine(display,localization_canvas_win,localizationgui_gc,laser_points[i].graf_point.x,
		laser_points[i].graf_point.y,laser_points[i].graf_point_robot.x,
		laser_points[i].graf_point_robot.y);	
      
    }
	
	
	
	
} 
void delete_particles()
{
  int i;
  
  for (i=0 ; i<MAX_PARTICLES ; i++) 
    {
      
      if (grid2localizationcanvas(poblacion1->particles_population[i].point,&celdillaGraf,laserloc_grid)>=0)
	{	
	  //borramos las antiguas particulas
	  fl_set_foreground(localizationgui_gc,mi_suelo);
	  XDrawRectangles(display,localization_canvas_win,localizationgui_gc,&particulas_antiguas[i],1);
	  XFillRectangles(display,localization_canvas_win,localizationgui_gc,&particulas_antiguas[i],1);
	  XDrawLine(display,localization_canvas_win,localizationgui_gc,graphic_particles[i].point.x,
		    graphic_particles[i].point.y,graphic_particles[i].theta_point.x,
		    graphic_particles[i].theta_point.y);
    
	  
	} 
    }
}
// dibuja las particulas
void draw_particles() 
  
{
  
  int R,G,B,i;
  Tvoxel aux_point;
  
  for (i=0 ; i<MAX_PARTICLES ; i++) {
	  
    //borramos las antiguas particulas    
    fl_set_foreground(localizationgui_gc,mi_suelo);
    XDrawRectangles(display,localization_canvas_win,localizationgui_gc,&particulas_antiguas[i],1);
    XFillRectangles(display,localization_canvas_win,localizationgui_gc,&particulas_antiguas[i],1);
    XDrawLine(display,localization_canvas_win,localizationgui_gc,graphic_particles[i].point.x,
	      graphic_particles[i].point.y,graphic_particles[i].theta_point.x,
	      graphic_particles[i].theta_point.y);
    if(poblacion1->particles_population[i].prob>0.2)  	  
      if (grid2localizationcanvas(poblacion1->particles_population[i].point,&celdillaGraf,laserloc_grid)>=0)
	
	{	
	  
	  //inicializamos el nivel de rojo depndiendo de la probablidad
	  R=(int)255;
	  G=(int)((1.-poblacion1->particles_population[i].prob)*255);
	  B=(int)((1.-poblacion1->particles_population[i].prob)*255);
	  fl_mapcolor(mi_rojo,R,G,B);
	  fl_set_foreground(localizationgui_gc,mi_rojo);
	  
	  //dibujamos la celdilla en la que cae el punto
	  XDrawRectangles(display,localization_canvas_win,localizationgui_gc,&celdillaGraf,1);
	  XFillRectangles(display,localization_canvas_win,localizationgui_gc,&celdillaGraf,1);
	  particulas_antiguas[i] = celdillaGraf;
	  
	  // aux_point tendra el con despz 200 en set
	  aux_point.x=poblacion1->particles_population[i].point.x+200*
	    cos(poblacion1->particles_population[i].theta*DEGTORAD);
	  aux_point.y=poblacion1->particles_population[i].point.y+200*
	    sin(poblacion1->particles_population[i].theta*DEGTORAD);
	  xy2localizationcanvas(poblacion1->particles_population[i].point,&graphic_particles[i].point);
	  xy2localizationcanvas(aux_point,&graphic_particles[i].theta_point);

	  //dibujamos una linea entre los dos puntos
	  XDrawLine(display,localization_canvas_win,localizationgui_gc,graphic_particles[i].point.x,
		    graphic_particles[i].point.y,graphic_particles[i].theta_point.x,
		    graphic_particles[i].theta_point.y);
	  
	} 
  }
  
}

void draw_exploradores() 
  
{
  
  int R,G,B,i;
  Tvoxel aux_point;
  
  for (i=0 ; i<MAX_EXPLORADORES ; i++) {
    
    if (grid2localizationcanvas(exploradores[i].point,&celdillaGraf,laserloc_grid)>=0)
      
      {	
	//borramos las antiguas particulas
	fl_set_foreground(localizationgui_gc,mi_suelo);
	XDrawRectangles(display,localization_canvas_win,localizationgui_gc,&exploradores_antiguos[i],1);
	XFillRectangles(display,localization_canvas_win,localizationgui_gc,&exploradores_antiguos[i],1);
	XDrawLine(display,localization_canvas_win,localizationgui_gc,graphic_explorers[i].point.x,graphic_explorers[i].point.y,graphic_explorers[i].theta_point.x,graphic_explorers[i].theta_point.y);
	
	//inicializamos el nivel de rojo depndiendo de la probablidad
	R=(int)255;
	G=(int)((1.-exploradores[i].prob)*255);
	B=(int)((1.-exploradores[i].prob)*255);
	fl_mapcolor(mi_rojo,R,G,B);
	fl_set_foreground(localizationgui_gc,mi_rojo);
	//fl_set_foreground(localizationgui_gc,colores.color[0]);
	
	//dibujamos la celdilla en la que cae el punto
	XDrawRectangles(display,localization_canvas_win,localizationgui_gc,&celdillaGraf,1);
	XFillRectangles(display,localization_canvas_win,localizationgui_gc,&celdillaGraf,1);
	exploradores_antiguos[i] = celdillaGraf;
	
	// aux_point tendra el con despz 200 en set
	aux_point.x=exploradores[i].point.x+200*cos(exploradores[i].theta*DEGTORAD);
	aux_point.y=exploradores[i].point.y+200*sin(exploradores[i].theta*DEGTORAD);
	xy2localizationcanvas(exploradores[i].point,&graphic_explorers[i].point);
	xy2localizationcanvas(aux_point,&graphic_explorers[i].theta_point);
	
	//dibujamos una linea entre los dos puntos
	XDrawLine(display,localization_canvas_win,localizationgui_gc,graphic_explorers[i].point.x,
		  graphic_explorers[i].point.y,graphic_explorers[i].theta_point.x,
		  graphic_explorers[i].theta_point.y);
	/**/
      }
  }
  
}


void draw_explotadores(plistaExp *lista) 
  
{
  int R,G,B;
  int i,cont=1;
  float probabilidad,aux;	
  plistaExp plista,lista_aux;
  Tvoxel aux_point;
  float maxima_prob=0.0;
  int pos=0;
  if(SOLO_MAX_PROB_ACTIVE)
    {

      fl_set_slider_value(fd_localizationgui->vis_explotadores,prob_max);
      VISUALIZADOR_EXPLOTADORES=prob_max;
    }	
  
  
  plista=*lista;
  pos = 0;
  i=0;
  while(plista !=NULL){
    if (maxima_prob< plista->exp1[MAX_EXPLOTADORES-1].prob){
      maxima_prob = plista->exp1[MAX_EXPLOTADORES-1].prob;
      pos = i;
    }
    i++;
    plista = plista->siguiente;
  }
  plista=*lista;
  i=0;
  int pos_particle=0;
  while (plista!=NULL) {

    //printf("\n");	
    for (i=0 ; i<MAX_EXPLOTADORES ; i++) {  
      
      if (grid2localizationcanvas(plista->exp1[i].point,&celdillaGraf,laserloc_grid)>=0)
	
	{	
	  ////borramos las antiguas particulas
	  fl_set_foreground(localizationgui_gc,mi_suelo);
	  XDrawRectangles(display,localization_canvas_win,localizationgui_gc,&plista->antiguos[i],1);
	  XFillRectangles(display,localization_canvas_win,localizationgui_gc,&plista->antiguos[i],1);
	  XDrawLine(display,localization_canvas_win,localizationgui_gc,plista->graphic_exp[i].point.x,plista->graphic_exp[i].point.y,plista->graphic_exp[i].theta_point.x,plista->graphic_exp[i].theta_point.y);
	  
	  if(plista->exp1[MAX_EXPLOTADORES-1].prob>= VISUALIZADOR_EXPLOTADORES)    
	    {   
	      fl_set_foreground(localizationgui_gc,plista->color);
	      
	      //dibujamos la celdilla en la que cae el punto
	      XDrawRectangles(display,localization_canvas_win,localizationgui_gc,&celdillaGraf,1);
	      XFillRectangles(display,localization_canvas_win,localizationgui_gc,&celdillaGraf,1);
	      plista->antiguos[i] = celdillaGraf;
	      
	      // aux_point tendra el con despz 200 en set
	      aux_point.x=plista->exp1[i].point.x+200*cos(plista->exp1[i].theta*DEGTORAD);
	      aux_point.y=plista->exp1[i].point.y+200*sin(plista->exp1[i].theta*DEGTORAD);
	      xy2localizationcanvas(plista->exp1[i].point,&plista->graphic_exp[i].point);
	      xy2localizationcanvas(aux_point,&plista->graphic_exp[i].theta_point);
	      
	      //dibujamos una linea entre los dos puntos
	      XDrawLine(display,localization_canvas_win,localizationgui_gc,plista->graphic_exp[i].point.x,plista->graphic_exp[i].point.y,plista->graphic_exp[i].theta_point.x,plista->graphic_exp[i].theta_point.y);
	    }
	  if(!(SOLO_MAX_PROB_ACTIVE))
	    {  
	      if(plista->exp1[MAX_EXPLOTADORES-1].prob>= VISUALIZADOR_EXPLOTADORES)
		{			   
		  fl_set_foreground(localizationgui_gc,plista->color);
		  
		  //dibujamos la celdilla en la que cae el punto
		  XDrawRectangles(display,localization_canvas_win,localizationgui_gc,&celdillaGraf,1);
		  XFillRectangles(display,localization_canvas_win,localizationgui_gc,&celdillaGraf,1);
		  plista->antiguos[i] = celdillaGraf;
		  
		  // aux_point tendra el con despz 200 en set
		  aux_point.x=plista->exp1[i].point.x+200*cos(plista->exp1[i].theta*DEGTORAD);
		  aux_point.y=plista->exp1[i].point.y+200*sin(plista->exp1[i].theta*DEGTORAD);
		  xy2localizationcanvas(plista->exp1[i].point,&plista->graphic_exp[i].point);
		  xy2localizationcanvas(aux_point,&plista->graphic_exp[i].theta_point);
		  
		  //dibujamos una linea entre los dos puntos
		  XDrawLine(display,localization_canvas_win,localizationgui_gc,
			    plista->graphic_exp[i].point.x,plista->graphic_exp[i].point.y,
			    plista->graphic_exp[i].theta_point.x,plista->graphic_exp[i].theta_point.y);
		}				
	    }
	    else if(plista->exp1[MAX_EXPLOTADORES-1].prob>= VISUALIZADOR_EXPLOTADORES)
	   
	    {   
	      fl_set_foreground(localizationgui_gc,FL_GREEN);
	    }
	  else
	    {   
	      if (plista->exp1[MAX_EXPLOTADORES-1].prob>0.85)
		aux=0.2;
	      else if (plista->exp1[MAX_EXPLOTADORES-1].prob>0.6)
		aux=0.5;
	      else
		aux=0.8;
	      if (pos_particle == pos){
		R=(int)0;
		G=(int)255;
		B=(int)0;
		fl_mapcolor(100,R,G,B);
		fl_set_foreground(localizationgui_gc,FL_GREEN);
	      }
	      else {
		R=(int)255;
		//G=(int)((aux)*255);
		//B=(int)((aux)*255);
		G=(int)0;
		B=(int)0;
		
		fl_mapcolor(mi_rojo,R,G,B);
		//fl_mapcolor(100,R,G,B);
		fl_set_foreground(localizationgui_gc,mi_rojo);
	      }
		
	    }
	  
	  //dibujamos la celdilla en la que cae el punto
	  XDrawRectangles(display,localization_canvas_win,localizationgui_gc,&celdillaGraf,1);
	  XFillRectangles(display,localization_canvas_win,localizationgui_gc,&celdillaGraf,1);
	  plista->antiguos[i] = celdillaGraf;
	  
	  // aux_point tendra el con despz 200 en set
	  aux_point.x=plista->exp1[i].point.x+200*cos(plista->exp1[i].theta*DEGTORAD);
	  aux_point.y=plista->exp1[i].point.y+200*sin(plista->exp1[i].theta*DEGTORAD);
	  xy2localizationcanvas(plista->exp1[i].point,&plista->graphic_exp[i].point);
	  xy2localizationcanvas(aux_point,&plista->graphic_exp[i].theta_point);
	  
	  //dibujamos una linea entre los dos puntos
	  XDrawLine(display,localization_canvas_win,localizationgui_gc,plista->graphic_exp[i].point.x,
		    plista->graphic_exp[i].point.y,plista->graphic_exp[i].theta_point.x,
		    plista->graphic_exp[i].theta_point.y);
	    
	}
    }
    plista=plista->siguiente;cont++;
    pos_particle++;
  }
  
}

void draw_trajectoria(Ttrayectoria * mitray)
{
	int i;

	
	for (i=0 ; i<MAX_POS_TRAY ; i++) {  
	  if(mitray->ocupado[i])
	    {
	      if (grid2localizationcanvas(mitray->tray[i],&celdillaGraf,laserloc_grid)>=0)
		
		{	
		  ////borramos las antiguas particulas
		  fl_set_foreground(localizationgui_gc,mi_suelo);
		  XDrawRectangles(display,localization_canvas_win,localizationgui_gc,&mitray->antiguos[i],1);
		  XFillRectangles(display,localization_canvas_win,localizationgui_gc,&mitray->antiguos[i],1);
		  
		  fl_set_foreground(localizationgui_gc,mitray->color);
		  
		  //dibujamos la celdilla en la que cae el punto
		  XDrawRectangles(display,localization_canvas_win,localizationgui_gc,&celdillaGraf,1);
		  XFillRectangles(display,localization_canvas_win,localizationgui_gc,&celdillaGraf,1);
		  mitray->antiguos[i] = celdillaGraf;
		  
		  
		}
	    }
	}
	
}

void reiniciar_trayectoria(Ttrayectoria * mitray)
{
  int i;
  
  for(i=0;i<MAX_POS_TRAY;i++)
    {
      mitray->ocupado[i]=FALSE;
    }
  mitray->pos=0;
}

void set_pos(Ttrayectoria * mitray, float x, float y)
{
  mitray->tray[mitray->pos].x=x;
  mitray->tray[mitray->pos].y=y;
  mitray->ocupado[mitray->pos]=TRUE;
  mitray->pos=(mitray->pos+1)%MAX_POS_TRAY;
}


void borrar_laser_teorico(Tlaser_point *laser_points) 
  
{
  int i=0;
  
  //for (i=0;i<NUM_LASER;i++)
  for (i=0;i<NUM_LASER;i=i+RESOLUCION_LASER_TEORICO)
    {
      
      //eso hay que ver como hacerlo
      fl_set_foreground(localizationgui_gc,mi_suelo);
      //solo punto obstaculo
      XDrawPoint(display,localization_canvas_win,localizationgui_gc,laser_points[i].graf_point.x,
		 laser_points[i].graf_point.y);
      
      //linea entre punto obstaculo y robot
      //XDrawLine(display,localization_canvas_win,localizationgui_gc,laser_points[i].graf_point.x,laser_points[i].graf_point.y,laser_points[i].graf_point_robot.x,laser_points[i].graf_point_robot.y);		
    }
	
      
      
    
} 
//FIN FUNCIONES PARA EL GUIDISPLAY

//PROCEDIMIENTOS MULTIOBJETO

int numElementosLista=0;

void inicializoColoresExplotadores(TColorExplotador *colores)
{
  int i;
	
  for(i=0;i<NUM_RAZAS;i++)
    {
      (*colores).ocupado[i]=FALSE;
    }
       
  
  (*colores).color[0]=FL_GREEN;
  (*colores).color[1]= FL_ORCHID;
  (*colores).color[2]=FL_DARKCYAN;
  (*colores).color[3]=FL_CHARTREUSE;
  (*colores).color[4]=FL_YELLOW ;
  (*colores).color[5]=FL_DARKVIOLET;
  (*colores).color[6]=FL_COL1 ;
  (*colores).color[7]=FL_SPRINGGREEN;
  (*colores).color[8]=FL_DARKGOLD;
  (*colores).color[9]=FL_BLUE;
  /*(*colores).color[10]=FL_DARKTOMATO;
    (*colores).color[11]=FL_WHEAT;
    (*colores).color[12]=FL_DARKORANGE;
    (*colores).color[13]=FL_DEEPPINK;
    (*colores).color[14]=FL_DODGERBLUE;
    (*colores).color[15]=FL_RIGHT_BCOL;
    (*colores).color[16]=FL_BOTTOM_BCOL;
    (*colores).color[17]=FL_TOP_BCOL;
    (*colores).color[18]=FL_LEFT_BCOL;
    (*colores).color[19]=FL_MCOL;*/
  
  
  
  (*colores).libres=NUM_RAZAS;
  printf("Inicializado array de colores\n");
  
}

int cogerColor(TColorExplotador *colores)
{
  
  int encontrado=FALSE;
  int i=0;	
  
  if((*colores).libres>0)
    {
      while((!encontrado)&&(i<NUM_RAZAS))
        if(!((*colores).ocupado[i]))
		{
		  encontrado=TRUE;
		  (*colores).ocupado[i]=TRUE;
		  (*colores).libres--;
		}
        else
	  {
	    i++;	
	  }
      //printf("Asigando color %d a nueva raza\n",i);	
    }
  else
    {
      //printf("No se pueden crear mas razas\n");
      i=-1;	   
   }
  
  return i;
  
}

void meterColor(TColorExplotador *colores,int pos)
{
  (*colores).ocupado[pos]=FALSE;
  (*colores).libres++;
  //printf("Desasigno color %d. Raza eliminada\n",pos);	
}

void InsertarNodoPartOrdenadas(plistapart *lista, Tparticle particula) {
  plistapart nuevo,plista,anterior;

  /* Crear un nuevo nodo */
  nuevo = (plistapart) malloc(sizeof(Tnodopart));
  /* Inicializo valores */
  nuevo->p=particula;	
  
  nuevo->siguiente = NULL;
	/* Añadir el nuevo nodo 'al final' de la lista */
  if (*lista==NULL)
    {
      *lista = nuevo;
      
    }
  else {
    
    plista = *lista;
    anterior=NULL;
    while((nuevo->p.prob > plista->p.prob)&&(plista->siguiente!=NULL))
      {
	anterior=plista;
	plista = plista->siguiente;
      }
    if(plista->siguiente==NULL)
      if(nuevo->p.prob > plista->p.prob)
	{   
	  plista->siguiente=nuevo;
			}
      else if(anterior==NULL)
	{  
	  nuevo->siguiente=plista;
	  *lista=nuevo;	 			   
	}			   
      else
	{	
	  nuevo->siguiente=plista;
	  anterior->siguiente=nuevo;		
	}
    else
      {
	if(anterior==NULL)
	  {   
	    nuevo->siguiente=plista;
	    *lista=nuevo;				   
	  }			   
	else
	  {   
	    nuevo->siguiente=plista;
	    anterior->siguiente=nuevo;
	  }
      }			
    
    
  }
  
}

void BorrarListaPartOrdenadas(plistapart *lista) {
  plistapart antiguo;
  
  while(*lista) {
    antiguo = *lista;
    *lista = antiguo->siguiente;
    free(antiguo);
	}
  
}

void InsertarNodo(plistaExp *lista, Tparticle particula, TColorExplotador *colores) {
  plistaExp nuevo,plista;
  int i=0;
  XRectangle aux;
  graphic_explorers_type aux2;
  /* Crear un nuevo nodo */
  nuevo = (plistaExp) malloc(sizeof(TnodoExp));
  /* Inicializo valores */
  for (i = 0; i < MAX_EXPLOTADORES; i++ ) {
    nuevo->exp1[i] = particula;
    nuevo->exp2[i] = particula;
	}	
  nuevo->Explotadores = nuevo->exp1;
  nuevo->Explotadores2 = nuevo->exp2;
  
  for (i = 0; i < MAX_EXPLOTADORES; i++ ) {
    nuevo->graphic_exp[i] = aux2;
  }	
  nuevo->Gexp = nuevo->graphic_exp;
  
  for (i = 0; i < MAX_EXPLOTADORES; i++ ) {
    nuevo->antiguos[i] = aux;
  }	
  nuevo->Ant = nuevo->antiguos;
  
  nuevo->pos_color=cogerColor(colores);
  nuevo->color=colores->color[nuevo->pos_color];
  nuevo->pos_siguiente=0;
  
  nuevo->max=FALSE;
  nuevo->memoria=0;
  
  nuevo->disp_x=myencoders[0];  
  nuevo->disp_y=myencoders[1];
  nuevo->disp_theta=(float)((int)((RADTODEG*myencoders[2])+360)% 360);
  //printf("Quedan %d colores libres\n",colores->libres);
  nuevo->siguiente = NULL;
  /* Añadir el nuevo nodo 'al final' de la lista */
  if (*lista==NULL)
    *lista = nuevo;
  else {
    plista = *lista;
    while (plista->siguiente!=NULL)
      plista = plista->siguiente;
    plista->siguiente = nuevo;
    
    
  }
//	printf("Creo nodo %p\n",&(*nuevo));
  numElementosLista++;
}

void EliminarNodo(plistaExp *lista, int posicion,TColorExplotador *colores) {
  plistaExp antiguo, anterior, plista;
  int x,i;
  
  if (posicion == 1) {
    antiguo = *lista;
    *lista = antiguo->siguiente;
  } else {
    plista = *lista;
    //me coloco en la posicion a eliminar adecuada
    for(x=1;x<posicion;x++) {
      anterior = plista;
      plista = plista->siguiente;
    }
    antiguo = plista;
    anterior->siguiente = antiguo->siguiente;
  }
  //elimino
  //	printf("Elimino nodo %p\n",&(*antiguo));
  meterColor(colores,antiguo->pos_color);
  //printf("Quedan %d colores libres\n",colores->libres);
  for (i=0 ; i<MAX_EXPLOTADORES ; i++) {
    
    if (grid2localizationcanvas(antiguo->exp1[i].point,&celdillaGraf,laserloc_grid)>=0)
      
      {	
	////borramos las antiguas particulas
	fl_set_foreground(localizationgui_gc,mi_suelo);
	XDrawRectangles(display,localization_canvas_win,localizationgui_gc,&antiguo->antiguos[i],1);
	XFillRectangles(display,localization_canvas_win,localizationgui_gc,&antiguo->antiguos[i],1);
	XDrawLine(display,localization_canvas_win,localizationgui_gc,antiguo->graphic_exp[i].point.x,
		  antiguo->graphic_exp[i].point.y,antiguo->graphic_exp[i].theta_point.x,
		  antiguo->graphic_exp[i].theta_point.y);
      }
  }
  free(antiguo);
  numElementosLista--;
}

void BorrarLista(plistaExp *lista,TColorExplotador *colores) {
  plistaExp antiguo;
  int i;
  
  while(*lista!=NULL) {
    antiguo = *lista;
    *lista = antiguo->siguiente;
    //printf("Elimino nodo %p\n",&(*antiguo));
    //devolvemos el color y borramos los ultimos exploradores pintados
    meterColor(colores,antiguo->pos_color);
    //printf("Quedan %d colores libres\n",colores->libres);
    for (i=0 ; i<MAX_EXPLOTADORES ; i++) {
      
      if (grid2localizationcanvas(antiguo->exp1[i].point,&celdillaGraf,laserloc_grid)>=0)
	
	{	
	  ////borramos las antiguas particulas
	  fl_set_foreground(localizationgui_gc,mi_suelo);
	  XDrawRectangles(display,localization_canvas_win,localizationgui_gc,&antiguo->antiguos[i],1);
	  XFillRectangles(display,localization_canvas_win,localizationgui_gc,&antiguo->antiguos[i],1);
	  XDrawLine(display,localization_canvas_win,localizationgui_gc,antiguo->graphic_exp[i].point.x,
		    antiguo->graphic_exp[i].point.y,antiguo->graphic_exp[i].theta_point.x,
		    antiguo->graphic_exp[i].theta_point.y);
	}
    }
    free(antiguo);
    numElementosLista--;
  }
  //inteligentStop = FALSE;
}

void MeterParticula(Tparticle particula, plistaExp lista, int Posicion) {
  plistaExp plista;
  int x,i;
  
  if (lista!=NULL) {
    plista = lista;
    for(x=1;x<Posicion;x++)
      plista = plista->siguiente;

    if(plista->Explotadores[0].prob<particula.prob)
      {
	for(i=0;i<MAX_EXPLOTADORES;i++)
	  {
	    plista->Explotadores[i] = particula;
	  }
      }
    //       plista->Explotadores[MAX_EXPLOTADORES-1] = particula;
  }
}


Tparticle *DevuelveExp1(plistaExp lista,int veces, int * pos){
  plistaExp plista;
  int x;
  
  if (lista==NULL) {
    return NULL;
  } else {
    plista = lista;
    for(x=1;x<veces;x++)
      plista = plista->siguiente;
    
    if(plista->max)
      *pos=veces;			
    
    return plista->Explotadores;
  }
}

Tparticle *DevuelveExp2(plistaExp lista,int veces, int * pos){
  plistaExp plista;
  int x;
  
  if (lista==NULL) {
    return NULL;
  } else {
    plista = lista;
    for(x=1;x<veces;x++)
      plista = plista->siguiente;
    
    if(plista->max)
      *pos=veces;
    
    return plista->Explotadores2;
  }
}

XRectangle *DevuelveAnt(plistaExp lista,int veces){
  plistaExp plista;
  int x;
  
  if (lista==NULL) {
    return NULL;
  } else {
    plista = lista;
    for(x=1;x<veces;x++)
      plista = plista->siguiente;
    
    return plista->Ant;
  }
}
graphic_explorers_type *DevuelveGexp(plistaExp lista,int veces){
  plistaExp plista;
  int x;
  
  if (lista==NULL) {
    return NULL;
  } else {
    plista = lista;
    for(x=1;x<veces;x++)
      plista = plista->siguiente;
    
    return plista->Gexp;
  }
}

int DevuelveGexpAntColor(plistaExp lista,int veces,XRectangle *ant,graphic_explorers_type *g,float *color){
  plistaExp plista;
  int x;
  
  if (lista==NULL) {
    return FALSE;
  } else {
    plista = lista;
    for(x=1;x<veces;x++)
      plista = plista->siguiente;
    
    ant=plista->Ant;
    g=plista->Gexp;
    *color=plista->color;
    return TRUE;
  }
}

void ActualizaExp1(plistaExp lista,int veces,Tparticle *puntero){
  plistaExp plista;
  int x,i;
  
  
  
  plista = lista;
  if (lista==NULL)
    printf("Imposible actualizar puntero Exp1.\n");
  else
    for(x=1;x<veces;x++)
      plista = plista->siguiente;
  
  
  plista->Explotadores=puntero;
  
}

void ActualizaExp2(plistaExp lista,int veces,Tparticle *puntero){
  plistaExp plista;
  int x,i;
	
  plista = lista;
  if (lista==NULL)
    printf("Imposible actualizar puntero Exp2.\n");
  else
    for(x=1;x<veces;x++)
      plista = plista->siguiente;
  
  plista->Explotadores2=puntero;
  
}

void BorraMemoria(plistaExp lista,int veces){
  plistaExp plista;
  int x;
  
  plista = lista;
  if (lista==NULL)
    printf("Imposible actualizar puntero Gexp.\n");
  else
    for(x=1;x<veces;x++)
      plista = plista->siguiente;
	
  plista->max=FALSE;	
  plista->memoria=0;
}

void AumentaMemoria(plistaExp lista,int veces,int *encontrada){
  plistaExp plista;
  int x;
  
  plista = lista;
  if (lista==NULL)
    printf("Imposible actualizar puntero Gexp.\n");
  else
    for(x=1;x<veces;x++)
      plista = plista->siguiente;
  
  plista->max=TRUE;
  plista->memoria++;
  if(plista->memoria>MEMORIA_RAZA)
    *encontrada=TRUE;		
  //printf("RAZA MAXIMA->%d\n",plista->memoria);
}

void ActualizaGexp(plistaExp lista,int veces,graphic_explorers_type *puntero){
  plistaExp plista;
  int x;
  
  plista = lista;
  if (lista==NULL)
    printf("Imposible actualizar puntero Gexp.\n");
  else
    for(x=1;x<veces;x++)
      plista = plista->siguiente;
  
  plista->Gexp=puntero;
}
void ActualizaAnt(plistaExp lista,int veces,XRectangle *puntero){
  plistaExp plista;
  int x;
	
  plista = lista;
  if (lista==NULL)
    printf("Imposible actualizar puntero Ant.\n");
	else
	  for(x=1;x<veces;x++)
	    plista = plista->siguiente;
  
  plista->Ant=puntero;
}

void ActualizaAntGexp(plistaExp lista,int veces,XRectangle *puntero,graphic_explorers_type *puntero2){
  plistaExp plista;
  int x;
  
  plista = lista;
  if (lista==NULL)
    printf("Imposible actualizar punteros Ant y Gexp.\n");
  else
    for(x=1;x<veces;x++)
      plista = plista->siguiente;
	
  plista->Ant=puntero;
  plista->Gexp=puntero2;	
}



int Check_Distance(Tparticle p1, Tparticle p2) {	// s1:recta - s2:punto 
  
  
  float aux;
  int ang;
  
  //printf("\n\n(%f,%f)(%f,%f)\n",p1.point.x,p1.point.y,p2.point.x,p2.point.y);
  //printf("%f<%f\n\n",aux,UMBRAL_SIMILITUD_PARTICULAS);
  
  aux=((p1.point.x-p2.point.x)*(p1.point.x-p2.point.x)+(p1.point.y-p2.point.y)*(p1.point.y-p2.point.y));
  ang=abs(p1.theta-p2.theta);
  if(ang>180)  ang= 360 - ang;
  
  return ((aux<(UMBRAL_SIMILITUD_PARTICULAS*UMBRAL_SIMILITUD_PARTICULAS))&&
	  (ang<UMBRAL_ANGULAR_PARTICULAS));
  
}

void Check_Similarity(plistaExp *lista, Tparticle particula, int *Igual, float *saludDelIgual) {
  plistaExp plista;
  int x, Similar;
  
  Similar = FALSE;
  *Igual = 0;
  *saludDelIgual = 0;
  if ((*lista)!=NULL) {			//solo comprueba similitud si hay explotadores
    plista = *lista;
    for (x=1; x<=numElementosLista; x++) {
      //			printf("Comparo con %d\n",x);
      Similar = Check_Distance(particula, plista->Explotadores[MAX_EXPLOTADORES-1]);
      if (Similar) {
	*Igual = x;
	*saludDelIgual = plista->Explotadores[MAX_EXPLOTADORES-1].prob;
	break;
      }
      plista = plista->siguiente;
		}
  }
}

void Clean_Not_Valid (plistaExp *lista,TColorExplotador *colores){
  plistaExp plista,anterior,antiguo;
  int x,i;
  
  plista = *lista;
  anterior=NULL;
  while (plista!=NULL)
    {   
      if(plista->exp1[MAX_EXPLOTADORES-1].prob<UMBRAL_PERDIDA)
	{  
	  if(anterior==NULL)
	    {   
	      antiguo=plista;
	      (*lista)=plista->siguiente;
	      plista=plista->siguiente;
	    }
	  else
	    {
	      anterior->siguiente=plista->siguiente;
	      antiguo=plista;
	      plista=plista->siguiente;
	    }
	  //devolvemos el color 
	  meterColor(colores,antiguo->pos_color);
	  
	  free(antiguo);
	  numElementosLista--;
	}
      else
	{
	  anterior=plista;
	  plista=plista->siguiente;			
	}
    }
  
  
}


void Clean_Similarity(plistaExp *lista,TColorExplotador *colores) {
  plistaExp plista, plista2, antiguo, anterior;
  int x, y, Similar,i;

  Similar = FALSE;
  if ((*lista)!=NULL) {			//solo comprueba similitud si hay explotadores
    plista = *lista;
    for (x=1; x<numElementosLista; x++) {
      anterior = plista;
      plista2 = plista->siguiente;
      for (y=x+1; y<=numElementosLista; y++) 
	{
	  //				printf("Comparo %d con %d\n",x,y);
	  Similar = Check_Distance(plista->Explotadores[MAX_EXPLOTADORES-1], 
				   plista2->Explotadores[MAX_EXPLOTADORES-1]);
	  //printf("%d-> ( %f , %f )\n",y,plista2->Explotadores[MAX_EXPLOTADORES-1].prob,UMBRAL_PERDIDA);
	  if (((Similar )&& 
	       (plista->Explotadores[MAX_EXPLOTADORES-1].prob>=plista2->Explotadores[MAX_EXPLOTADORES-1].prob))
	      ||(plista2->Explotadores[MAX_EXPLOTADORES-1].prob<UMBRAL_PERDIDA))
	    {
	      //printf("Son similares %d con %d - total %d\n",x,y,numElementosLista);
	      //recoloco los punteros
	      antiguo = plista2;
	      anterior->siguiente = antiguo->siguiente;
	      plista2 = plista2->siguiente;
	      //elimino
	      meterColor(colores,antiguo->pos_color);
	      //printf("Quedan %d colores libres\n",colores->libres);
	      for (i=0 ; i<MAX_EXPLOTADORES ; i++) {
		
		if (grid2localizationcanvas(antiguo->exp1[i].point,&celdillaGraf,laserloc_grid)>=0)
		  
		  {	
							////borramos las antiguas particulas
		    fl_set_foreground(localizationgui_gc,mi_suelo);
		    XDrawRectangles(display,localization_canvas_win,localizationgui_gc,
				    &antiguo->antiguos[i],1);
		    XFillRectangles(display,localization_canvas_win,localizationgui_gc,
				    &antiguo->antiguos[i],1);
		    XDrawLine(display,localization_canvas_win,localizationgui_gc,
			      antiguo->graphic_exp[i].point.x,antiguo->graphic_exp[i].point.y,
			      antiguo->graphic_exp[i].theta_point.x,antiguo->graphic_exp[i].theta_point.y);
						  }
	      }
	      //					printf("Elimino nodo %p\n",&(*antiguo));
	      free(antiguo);
	      numElementosLista--;
	      y--;
	    }
	  /*esto lo hago para que el bucle vuelva a comparar con la misma 
	    posición ya que ahora es otra raza distinta*/
	  else if((Similar )||(plista->Explotadores[MAX_EXPLOTADORES-1].prob<UMBRAL_PERDIDA))
	    {
	      //recoloco los punteros
	      /*if (plista->Explotadores[MAX_EXPLOTADORES-1].prob<UMBRAL_PERDIDA)
		printf("Borro la raza salud %f \n",plista->Explotadores[MAX_EXPLOTADORES-1].prob);*/
	     
	      for(i=0;i<MAX_EXPLOTADORES;i++)
		{
		  plista->exp1[i]=plista2->exp2[i];
		}
	      plista->disp_x=plista2->disp_x;
	      plista->disp_y=plista2->disp_y;
	      plista->disp_theta=plista2->disp_theta;
	      plista->max=plista2->max;
	      plista->memoria=plista2->memoria;
	      antiguo = plista2;
	      anterior->siguiente = antiguo->siguiente;
	      plista2 = plista2->siguiente;
	      //elimino
	      meterColor(colores,antiguo->pos_color);
	      //printf("Quedan %d colores libres\n",colores->libres);
	      for (i=0 ; i<MAX_EXPLOTADORES ; i++) {
		
		if (grid2localizationcanvas(antiguo->exp1[i].point,&celdillaGraf,laserloc_grid)>=0)
		  
		  {	
		    ////borramos las antiguas particulas
		    fl_set_foreground(localizationgui_gc,mi_suelo);
		    XDrawRectangles(display,localization_canvas_win,localizationgui_gc,
				    &antiguo->antiguos[i],1);
		    XFillRectangles(display,localization_canvas_win,localizationgui_gc,
				    &antiguo->antiguos[i],1);
		    XDrawLine(display,localization_canvas_win,
			      localizationgui_gc,antiguo->graphic_exp[i].point.x,
			      antiguo->graphic_exp[i].point.y,antiguo->graphic_exp[i].theta_point.x,
			      antiguo->graphic_exp[i].theta_point.y);
		  }
	      }
	      //					printf("Elimino nodo %p\n",&(*antiguo));
	      free(antiguo);
	      numElementosLista--;
	      y--;
	    }
	  
	  else 
	    {
	      anterior = anterior->siguiente;
	      plista2 = plista2->siguiente;
	    }
	}
      plista = plista->siguiente;
    }
  }
}

/* Generación de números aleatorios uniformes */
float number_generationbv_s(float min, float max) {
  float x;
  x = (float) ((max - min) * rand() / RAND_MAX) + min ;
  return x;
}

void sort_exp(Tparticle exp[], int left, int right) {
   
  
  
  int i;  //ordena de menor a mayor
  Tparticle aux;	
   for(i=right;i>left;i--)
     {
       if(exp[i].prob<exp[i-1].prob)
	 {
	   aux=exp[i];
	   exp[i]=exp[i-1];
	   exp[i-1]=aux; 
	 }   		  
     }	
   
   if(right>left)
     sort_exp(exp,left+1,right);/**/
}


void q_sortbvv(Tparticle seg3dv[], int left, int right) {
	int l_hold, r_hold;
	Tparticle pivot;
  	l_hold = left;
  	r_hold = right;
  	pivot.point.x = seg3dv[left].point.x;
    pivot.point.y = seg3dv[left].point.y;
    pivot.theta = seg3dv[left].theta;
    pivot.prob = seg3dv[left].prob;	
    while (left < right) {
      while ((seg3dv[right].prob >= pivot.prob) && (left < right))
	right--;
      if (left != right) {
	seg3dv[left].point.x = seg3dv[right].point.x;
	seg3dv[left].point.y = seg3dv[right].point.y;
	seg3dv[left].theta = seg3dv[right].theta;
	seg3dv[left].prob = seg3dv[right].prob;
	left++;
	
      }
      while ((seg3dv[left].prob <= pivot.prob) && (left < right))
      		left++;
      if (left != right) {
	seg3dv[right].point.x = seg3dv[left].point.x;
	seg3dv[right].point.y = seg3dv[left].point.y;
	seg3dv[right].theta = seg3dv[left].theta;
	seg3dv[right].prob = seg3dv[left].prob;
	right--;
      }
    }
    seg3dv[left].point.x = seg3dv[right].point.x;
    seg3dv[left].point.y = seg3dv[right].point.y;
    seg3dv[left].theta = seg3dv[right].theta;
    seg3dv[left].prob = seg3dv[right].prob;
    pivot.xl = left;
    left = l_hold;
    right = r_hold;
    if (left < pivot.xl)
      q_sortbvv(seg3dv, left, pivot.xl - 1);
    if (right > pivot.xl)
      q_sortbvv(seg3dv, pivot.xl + 1, right);
}


//FIN PROCEDIMIENTOS MULTIOBJETO

//LECTURA MUNDO
//variables que he añadido en el esquema 
#define MAX 1250
#define VACIO -2
#define OCUPADO -1
#define ENCAPILLA -3
#define OZ -4
#define STAGE 1
#define SRIsim 2

typedef struct
{
  int x_inicio;
  int y_inicio;
  int x_final;
  int y_final;
} Tsegmento;
typedef struct
{
  int ancho;
  int alto;
  int dimension;
  float resolucion;
  int x_max;
  int y_max;
  int x_min;
  int y_min;
  int n_segmentos;
  float x_robot;
  float y_robot;
  float theta_robot;
  Tsegmento segmentos[1250];
}Tmundo;
char* fich_imagen;
Tmundo mi_mundo;

void insertar_ordenando_por_x(Tvoxel *p)
{
  int i,j,colocado,poner;
  Tvoxel aux,aux2;
  
  i=0;
  colocado=FALSE;
  while ((rejilla_rayo.order_x[i].ocupado)&& (!colocado))
    {   
      if(rejilla_rayo.order_x[i].pnt.x>(*p).x)
	{ 
	  
	  aux=*p;
	  j=i;	
	  poner=TRUE;
	  while(poner)
	    {
	      
	      poner=rejilla_rayo.order_x[j+1].ocupado;
	      if(poner)
		{				  
		  aux2=rejilla_rayo.order_x[j].pnt;
		  rejilla_rayo.order_x[j].pnt=aux;
		  rejilla_rayo.order_x[j].ocupado=TRUE; 
		  aux=aux2;
		  j++; 
		}
	      else
		{
		  aux2=rejilla_rayo.order_x[j].pnt;
		  rejilla_rayo.order_x[j].pnt=aux;
		  rejilla_rayo.order_x[j].ocupado=TRUE; 
		  rejilla_rayo.order_x[j+1].pnt=aux2;
		  rejilla_rayo.order_x[j+1].ocupado=TRUE;   
		}
	      
	    }
	  
	  
	  colocado=TRUE;		   
	}
      else if(rejilla_rayo.order_x[i].pnt.x==(*p).x)
	{ 	          
	  if(rejilla_rayo.order_x[i].pnt.y>(*p).y)
	    {			
	      aux=*p;
	      j=i;	
	      poner=TRUE;
	      while(poner)
		{
		  
		  poner=(rejilla_rayo.order_x[j+1].ocupado);
		  if(poner)
		    {				  
		      aux2=rejilla_rayo.order_x[j].pnt;
		      rejilla_rayo.order_x[j].pnt=aux;
		      rejilla_rayo.order_x[j].ocupado=TRUE; 
		      aux=aux2;
		      j++; 
		    }
		  else
		    {
		      aux2=rejilla_rayo.order_x[j].pnt;
		      rejilla_rayo.order_x[j].pnt=aux;
		      rejilla_rayo.order_x[j].ocupado=TRUE; 
		      rejilla_rayo.order_x[j+1].pnt=aux2;
		      rejilla_rayo.order_x[j+1].ocupado=TRUE;   
		    }
		  
		}
	      
	      
	      colocado=TRUE;
	    }
	  else
	    {
	      i++;	 
	    }			 
	}
      
      else
	{
	  i++;
	}
    }
  
  if(!colocado)
    {
      rejilla_rayo.order_x[i].pnt=*p;
      rejilla_rayo.order_x[i].ocupado=TRUE;
      
    }
  
  
}
void asignarLimites_x()
{
  int i,j,aux;
  int posx,posy;
  float actual;
  
  posx=-1;
  posy=0;
  rejilla_rayo.order_x[0].prevx=posx;
  rejilla_rayo.order_x[0].firsty=posy;
  actual=rejilla_rayo.order_x[0].pnt.x;
  
  for(i=1;i<rejilla_rayo.ocup;i++)
    {
      if(rejilla_rayo.order_x[i].pnt.x==actual)
	{
	  if(!(i==rejilla_rayo.ocup-1))
	    {
	      rejilla_rayo.order_x[i].prevx=posx;
	      rejilla_rayo.order_x[i].firsty=posy;
	    }
	  else
	    {
	      rejilla_rayo.order_x[i].prevx=posx;
	      rejilla_rayo.order_x[i].firsty=posy;
	      posx=-1;
	      aux=posy;
	      posy=i;
	      for(j=i;j>=aux;j--)
		{
		  
		  rejilla_rayo.order_x[j].nextx=posx;
		  rejilla_rayo.order_x[j].lasty=posy;
		}
	    }
	}
      else
	{
	  actual=rejilla_rayo.order_x[i].pnt.x;
	  rejilla_rayo.order_x[i-1].prevx=posx;
	  rejilla_rayo.order_x[i-1].firsty=posy;
	  posx=i;
	  aux=posy;
	  posy=i-1;
	  for(j=i-1;j>=aux;j--)
	    {
	      
	      rejilla_rayo.order_x[j].nextx=posx;
	      rejilla_rayo.order_x[j].lasty=posy;
	    }
	  posx--;
	  posy++;
	  rejilla_rayo.order_x[i].prevx=posx;
	  rejilla_rayo.order_x[i].firsty=posy;
	}
    }
  
}

void asignarLimites_y()
{
  int i,j,aux;
  int posy,posx;
  float actual;
  
  posy=-1;
  posx=0;
  rejilla_rayo.order_y[0].prevy=posy;
  rejilla_rayo.order_y[0].firsty=posx;
  actual=rejilla_rayo.order_y[0].pnt.y;
  
  for(i=1;i<rejilla_rayo.ocup;i++)
    {
      if(rejilla_rayo.order_y[i].pnt.y==actual)
	{
	  if(!(i==rejilla_rayo.ocup-1))
	    {
	      rejilla_rayo.order_y[i].prevy=posy;
	      rejilla_rayo.order_y[i].firstx=posx;
	    }
	  else
	    {
	      rejilla_rayo.order_y[i].prevy=posy;
	      rejilla_rayo.order_y[i].firstx=posx;
	      posy=-1;
	      aux=posx;
	      posx=i;
	      for(j=i;j>=aux;j--)
		{
		  
		  rejilla_rayo.order_y[j].nexty=posy;
		  rejilla_rayo.order_y[j].lastx=posx;
		}
	    }
	}
      else
	{
	  actual=rejilla_rayo.order_y[i].pnt.y;
	  rejilla_rayo.order_y[i-1].prevy=posy;
	  rejilla_rayo.order_y[i-1].firstx=posx;
	  posy=i;
	  aux=posx;
	  posx=i-1;
	  for(j=i-1;j>=aux;j--)
	    {
	      
	      rejilla_rayo.order_y[j].nexty=posy;
	      rejilla_rayo.order_y[j].lastx=posx;
	    }
	  posy--;
	  posx++;
	  rejilla_rayo.order_y[i].prevy=posy;
	  rejilla_rayo.order_y[i].firstx=posx;
	}
    }
  
}

void insertar_ordenando_por_y(Tvoxel *p)
{
  int i,j,colocado,poner;
  Tvoxel aux,aux2;
  
  i=0;
  colocado=FALSE;
  
  while ((rejilla_rayo.order_y[i].ocupado)&& (!colocado))
    {   
      if(rejilla_rayo.order_y[i].pnt.y>(*p).y)
	{ 
	  
	  aux=*p;
	  j=i;	
	  poner=TRUE;
	  while(poner)
	    {
		
	      poner=rejilla_rayo.order_y[j+1].ocupado;
	      if(poner)
		{				  
		  aux2=rejilla_rayo.order_y[j].pnt;
		  rejilla_rayo.order_y[j].pnt=aux;
		  rejilla_rayo.order_y[j].ocupado=TRUE; 
		  aux=aux2;
		  j++; 
		}
	      else
		{
		  aux2=rejilla_rayo.order_y[j].pnt;
		  rejilla_rayo.order_y[j].pnt=aux;
		  rejilla_rayo.order_y[j].ocupado=TRUE; 
		  rejilla_rayo.order_y[j+1].pnt=aux2;
		  rejilla_rayo.order_y[j+1].ocupado=TRUE;   
		}
							   
	    }
	  
	  
	  colocado=TRUE;		   
	}
      else if(rejilla_rayo.order_y[i].pnt.y==(*p).y)
	{ 	          
	  if(rejilla_rayo.order_y[i].pnt.x>(*p).x)
	    {			
	      aux=*p;
	      j=i;	
	      poner=TRUE;
	      while(poner)
		{
	
		  poner=(rejilla_rayo.order_y[j+1].ocupado);
		  if(poner)
		    {				  
		      aux2=rejilla_rayo.order_y[j].pnt;
		      rejilla_rayo.order_y[j].pnt=aux;
		      rejilla_rayo.order_y[j].ocupado=TRUE; 
		      aux=aux2;
		      j++; 
		    }
		  else
		    {
		      aux2=rejilla_rayo.order_y[j].pnt;
		      rejilla_rayo.order_y[j].pnt=aux;
		      rejilla_rayo.order_y[j].ocupado=TRUE; 
		      rejilla_rayo.order_y[j+1].pnt=aux2;
		      rejilla_rayo.order_y[j+1].ocupado=TRUE;   
		    }
		  
		}
	      
	      
	      colocado=TRUE;
	    }
	  else
	    {
	      i++;	 
	    }			 
			}
      
      else
	{
	  i++;
	}
    }
  
  if(!colocado)
    {
      rejilla_rayo.order_y[i].pnt=*p;
      rejilla_rayo.order_y[i].ocupado=TRUE;		
    }
  
	
}

Tmundo procesa_mundo_stage(char *fich_sal,float *ancho, float *alto) 
{
  Tmundo mundo;
  FILE *world;
  FILE *salida;
  FILE *imagen;
  char *temp; 
  //char *temp="bitmaps/cave.pgm";		
  //char *path="./worlds/";
  char *path="./worlds/";
  //char *fich_mundo="./worlds/simple.world";	 
  //char *fich_mundo="./worlds/tabla.world";
  char *fich_mundo="./worlds/dep2.world";	
  
  
  /*fich_imagen=(char*)malloc((strlen(temp)+strlen(path))*sizeof(char));
    fich_imagen=strncpy(fich_imagen,path,strlen(path));
  printf("-------------  %s \n",fich_imagen);
  fich_imagen=strncat(fich_imagen,temp,strlen(temp));
  printf("-------------  %s \n",fich_imagen);*/	
  int i,j,k,leidos,linea,cont;
  float size;
  int proximo_size;  
  char buf[MAX], aux[MAX], val[MAX],x0[MAX],y0[MAX],x1[MAX],y1[MAX];
  char buf_image[80*1024];
  
  proximo_size=FALSE;
  world= fopen(fich_mundo,"r");
  //world=open(fich_mundo,O_RDONLY);
  
  if (world == NULL) 
    {fprintf(stderr,"parser1: No encuentro el fichero %s\n",fich_mundo); exit(-1);}
  else 
    printf("fich %s abierto\n",fich_mundo);
  

  leidos=1;
  j=0;
  while (leidos!=0)
    {
      i=0; 
      buf[0]=' ';
      while ((leidos==1)&&(buf[i]!='\n'))
	leidos=fread(&buf[++i],1,1,world);
      
      
      linea=i-1;
      i=0;
      while (buf[i]==' ') i++;
      
      if ((buf[i]=='\n')||(i>=linea));
      else 
	{
	  sscanf(&buf[i],"%s %s %s %s %s %s",aux,val,x0,y0,x1,y1);
	  
	  
	  //printf("aux=%s : val=%s  \n",aux,val);
	  
	  
 	  if (strcmp(aux,"bitmap")==0)
	    {
	      cont=0;
	      temp=(char*)malloc((strlen(val)-2)*sizeof(char)); 
	      
	      
	      for (k=1;k<strlen(val)-1;k++) 
		{ 
		  temp[cont++]=val[k];
		}
	      temp[cont]='\0';
	      
	      
	      fich_imagen=(char*)malloc((strlen(temp)+strlen(path))*sizeof(char));
	      
	      sprintf(fich_imagen,"%s%s",path,temp);	
	      printf("Fichero a usar:%s\n",fich_imagen); 
	      
	    }
	  else if(strcmp(aux,"map")==0)
	    {
	      //asignamos a la variable true para que el proximo size que se encuentre se sepa queremos
		  //que se refiere al mundo
          proximo_size=TRUE;			
	      
	    }	
	  else if
		  (strcmp(aux,"size")==0)
	    {
	      if(proximo_size)
		{
		  *ancho=atof(x0)*1000;
		  *alto=atof(y0)*1000; 
		  if(*alto>*ancho)
		    size=*alto;
		  else
		    size=*ancho;				
		  printf("SIZE de map =>[ %f , %f ]\nUtilizamos %f  para crear rejilla cuadrada.\n",*ancho,*alto,size);
		  proximo_size=FALSE;
		  mundo.dimension=(int)size;
		  printf("Tamaño de la rejilla %d mm.\n",mundo.dimension);
		}
	    }
	  /*if (strcmp(aux,"resolution")==0)
	    {
	    
	      mundo.resolucion=atof(val)*1000;
	      printf("Resolucion de stage=%f\n",mundo.resolucion);
	      }*/
	  else if (strcmp(aux,"pose")==0)
	    {
	      	
	      mundo.x_robot=atof(x0)*1000;
	      //mundo.x_robot=100.0;
	      mundo.y_robot=atof(y0)*1000;
	      //mundo.y_robot=1000;
	      mundo.theta_robot=atof(x1); 
	      printf("Robot situado en X:%f Y:%f Theta:%f\n ",mundo.x_robot,mundo.y_robot,mundo.theta_robot);
	    }
	  
	}//else
      
    }//leidos!=0
  
  fclose(world);
 
  printf("Fichero %s cerrado\n",fich_mundo);

  printf(" ___________________________________ \n"); 

  imagen = fopen(fich_imagen,"r");//fich_imagen
  if (imagen == NULL) 
    {fprintf(stderr,"parser2: No encuentro el fichero %s\n",fich_imagen); exit(-1);}
  else 
    printf("fich %s abierto\n",fich_imagen);


  cont=0;
  leidos=1;
  j=0;
  while (leidos!=0)
    {
      i=0; 
      buf_image[0]=' ';
      while ((leidos==1)&&(buf_image[i]!='\n'))
	leidos=fread(&buf_image[++i],1,1,imagen);
      
      
      
      linea=i-1;
      i=0;
      while (buf_image[i]==' ') i++;
      
      if (!((buf_image[i]=='\n')||(i>=linea)))
	
	{
	  if (cont<3)
	    {
	      sscanf(&buf_image[i],"%s %s",aux,val); 	
	      cont++;
	      if (cont==3)
		{
		  mundo.alto=(int)atoi(val);
		  mundo.ancho=(int)atoi(aux);
		  printf("La imagen tiene %d filas y %d columnas\n",mundo.alto, mundo.ancho); 
		  leidos=0;  
		  }
	    }
	  
	}
    }
  
  fclose(imagen);
  printf("Fichero %s cerrado\n",fich_imagen);
  printf(" ___________________________________ \n"); 
  
  

  if (mundo.alto>=mundo.ancho)
    mundo.resolucion=((float)mundo.dimension/(float)mundo.alto);
  else
    mundo.resolucion=((float)mundo.dimension/(float)mundo.ancho);
   
  printf("Resolucion del mundo es %f\n",mundo.resolucion);
	
  salida= fopen(fich_sal,"w");
  if (salida == NULL) 
    {fprintf(stderr,"parser3: No encuentro el fichero %s\n",fich_sal); exit(-1);}
  
  printf("creando fichero de configuracion %s\n",fich_sal);
  printf(">>>>>>>>>>< %d\n",mundo.dimension);
  
  fprintf(salida,"dimension = %d\n",mundo.dimension);
  
  fprintf(salida,"resolucion = 130.434784\n"); //para dep2
  //fprintf(salida,"resolucion = 65.217392\n"); //para la mitad
  //fprintf(salida,"resolucion = 60.00\n"); //para simple	
  //fprintf(salida,"resolucion = 40.00\n"); //para tabla		
  fprintf(salida,"tipo = ec_diferencial\n");
  fprintf(salida,"ec_diferencial_speed = 1\n");
  fprintf(salida,"paso_tiempo = nulo\n");
  fprintf(salida,"cell_angles = 0\n");
  fprintf(salida,"mayoria_saturacion = 30\n");
  fprintf(salida,"mayoria_ruido = 10\n");
  fprintf(salida,"long_historia = 0\n");
  fprintf(salida,"\n");
  fprintf(salida,"sonar_filtra = independientes\n");
  fprintf(salida,"sonar_geometria = cono_denso\n");
  fprintf(salida,"sonar_apertura = 20.\n");
  fprintf(salida,"sonar_noobstacle = 3000.\n");
  fprintf(salida,"sonar_radialerror = 10.\n");
  fprintf(salida,"sonar_fdistancia = lineal\n"); 
  fprintf(salida,"sonar_residuo = 0.05\n");
  fprintf(salida,"sonar_o = 0.4\n");
  fprintf(salida,"sonar_e = -0.5\n");
  fprintf(salida,"sonar_mind = 700\n");
  fprintf(salida,"sonar_maxd = 1100.\n");
  fprintf(salida,"\n");
  fprintf(salida,"robot_geometria = cilindro\n");
  fprintf(salida,"robot_radio = 248.\n");
  fprintf(salida,"robot_e = -0.8\n");
  fprintf(salida,"\n");
  fprintf(salida,"laser_geometria = cono_denso\n");
  fprintf(salida,"laser_apertura = 0.5\n");
  fprintf(salida,"laser_muestras = 90\n");
  fprintf(salida,"laser_noobstacle = 8000.\n");
  fprintf(salida,"laser_o = 1\n");
  fprintf(salida,"laser_e = -0.7\n");
  
  fclose(salida);

  return mundo;

}



void relleno_grid(Tmundo mundo, Tgrid *grid)
{
  FILE *imagen; 
  int cont, leidos, i, j,k,kk,pos, linea;
  char *buff_imagen;
  char *casillas;
  int x_world, y_world,celda;
  int lado_mayor,diferencia;
  Tvoxel centro,esquina;	
  int tam,ocup,pos_comp_principal,pos_comp_secundaria;  
  tceldas_ocupadas co;	
	
  imagen= fopen(fich_imagen,"r"); 

  if (imagen == NULL) 
    {
      fprintf(stderr,"parser: No encuentro el fichero %s\n",fich_imagen); 
      exit(-1);
    }
  else 
    printf("formando grid usando --> %s \n",fich_imagen); 
  
  //buff_imagen=(char*)malloc((mundo.alto*mundo.ancho)*sizeof(char));
  //casillas=(char*)malloc((mundo.alto*mundo.ancho)*sizeof (char));
  if(mundo.alto>mundo.ancho)	  
     lado_mayor=mundo.alto; 
  else	  
     lado_mayor=mundo.ancho;	 
  
  diferencia=(mundo.alto-mundo.ancho);
  if(diferencia<0) diferencia=0;
	  
  buff_imagen=(char*)malloc((lado_mayor*lado_mayor)*sizeof(char));
  casillas=(char*)malloc((lado_mayor*lado_mayor)*sizeof (char)); 

  
  kk=0;
  cont=0;
  leidos=1;
  j=0;
  pos=-1;
  while (leidos!=0)
  {
      
    i=0; 
    buff_imagen[0]=' ';
    while ((leidos==1)&&(buff_imagen[i]!='\n'))
	  {
	    leidos=fread(&buff_imagen[++i],1,1,imagen);	
	    kk++;
	    //if(cont==4)  
	    // printf("%d ",(int)buff_imagen[i]);		  
  
	  }
    linea=i-1;
    i=0;
    
    while (buff_imagen[i]==' ') 
      i++;
    
    

    if ((buff_imagen[i]=='\n')||(i>=linea));
    
    else 
      {
	
	//printf("count --> %d",cont); 
	if (cont==4)
	  {
	    printf(" kk : i --> %d  %d \n", kk,i);
	    
	    if (cont==4)
		  {
		    //for (k=0;k<kk-1;k++) 
		    for (k=0;k<lado_mayor*lado_mayor-1;k++)   
		      { 	
			casillas[k]=buff_imagen[i+k];
			
		      } 
		    
		  }
	    
	    cont++;
	    kk=0;
	    
	  }
	
	//printf(" salgo del bucle \n"); 
	cont++;
	//kk=0;
      }	
    
  }
  
  printf(" fichero2 cerrado --> %s \n",fich_imagen); 
  
  printf("Tamaño->%d\n",grid->size);
  for(k=0;k<grid->size*grid->size;k++)
    if(((k%grid->size==0)&&(k/grid->size<=mundo.alto))||
       ((k/grid->size==mundo.alto)&&(k%grid->size<=mundo.ancho))||
       ((k/grid->size==0)&&(k%grid->size<=mundo.ancho))||
       ((k%grid->size==mundo.ancho)&&(k/grid->size<=mundo.alto)))
      
      
      (*grid).map[k].estado =OCUPADO;
  
    for (k=0;k<grid->size*grid->size;k++)
      {          
	y_world= ((k/(*grid).size) * (*grid).resolucion )/mundo.resolucion; 
	x_world= ((k%(*grid).size)*(*grid).resolucion)/mundo.resolucion; 
	
	if ((x_world<mundo.ancho)&&(y_world<=mundo.alto))
	  //if ((x_world<=lado_mayor)&&(y_world<=lado_mayor))
 	{ 
 	  celda=xy2celda(x_world,mundo.alto-y_world-1,mundo.ancho);
	  if (!((int) casillas[celda] ==-1))  		  			
	    {
	      (*grid).map[k].estado = OCUPADO;
	    }
	   
	}
	
      }
    
    tam=0;
    ocup=0;
    for (k=0;k<grid->size*grid->size;k++)
      {
	
	y_world= ((k/(*grid).size) * (*grid).resolucion )/mundo.resolucion; 
	x_world= ((k%(*grid).size)*(*grid).resolucion)/mundo.resolucion; 
	
      
	
	if ((x_world<mundo.ancho)&&(y_world<=mundo.alto))
	  
	  { 
	    tam++;	
	    celda=xy2celda(x_world,mundo.alto-y_world-1,mundo.ancho);
	    
	  if ((*grid).map[k].estado == OCUPADO) 		  			
	    {
	      ocup++;
	      
	  }
	  
	  }
            
    }
    printf("Numero de celdillas:\nTotal     ->%d\nMimundo  -> %d\nOcupadas -> %d\n",((*grid).size)*((*grid).size),tam,ocup);
    
    //rejilla_rayo.celdas_ocupadas_rayo=(int*)malloc(ocup*sizeof(int));
    rejilla_rayo.celdas_ocupadas=(tceldas_ocupadas*)malloc(ocup*sizeof(tceldas_ocupadas)); 	
    rejilla_rayo.ocup=ocup;	
    
    //Asigno memoria para las listas ordenadas segun la x y la y y en la pasada siguiente voy asignando valores
    rejilla_rayo.order_x=(lista_ordenada *) malloc(rejilla_rayo.ocup*sizeof(lista_ordenada));
    rejilla_rayo.order_y=(lista_ordenada *) malloc(rejilla_rayo.ocup*sizeof(lista_ordenada));
    for(i=0;i<rejilla_rayo.ocup;i++)
      {
	rejilla_rayo.order_x[i].ocupado=FALSE;
	rejilla_rayo.order_y[i].ocupado=FALSE;
      }
    
    
    ocup=0;
    pos_comp_principal=-1;
    pos_comp_secundaria=-1;
    for (k=0;k<grid->size*grid->size;k++)
      {
	
	y_world= ((k/(*grid).size) * (*grid).resolucion )/mundo.resolucion; 
	x_world= ((k%(*grid).size)*(*grid).resolucion)/mundo.resolucion; 
	
	
	
	if ((x_world<mundo.ancho)&&(y_world<=mundo.alto))
	  
	  { 
	    
	    celda=xy2celda(x_world,mundo.alto-y_world-1,mundo.ancho);
	    
	    
	    if ((*grid).map[k].estado == OCUPADO) 		  			
	      {   
		
		co.num=k;
		
		/*co.p.y=y_world;
		  co.p.x=x_world; 	
		  
		  rejilla_rayo.celdas_ocupadas[ocup]=co;*/
		  rejilla_rayo.celdas_ocupadas[ocup].p=(*grid).map[k].centro;
		  rejilla_rayo.celdas_ocupadas[ocup].num=k;
		  //rejilla_rayo.celdas_ocupadas_rayo[ocup]=FALSE;
		  
		  rejilla_rayo.order_x[ocup].ocupado=FALSE;
		  rejilla_rayo.order_y[ocup].ocupado=FALSE;
		  insertar_ordenando_por_x(&(*grid).map[k].centro);
		  insertar_ordenando_por_y(&(*grid).map[k].centro);
		  asignarLimites_x();
		  asignarLimites_y();
		  
		  
		  ocup++;
	        
	  }
	   
	 }
          
    }
		

  //inicializo el valor de la mitad de la diagonal de la celdilla. Para obtener este valor utilizamos 
  //el centro de una celda cualquiera y una esquina y se obtiene la distancia entre ambos.
  //Despues aumentamos esta distancia 1 cm (10mm) para evitar saltos de obstaculos 
  centro=laserloc_grid->map[10].centro; 
  esquina.x=centro.x + laserloc_grid->resolucion/2;
  esquina.y=centro.y + laserloc_grid->resolucion/2;
  

  rejilla_rayo.diagonalmedia=(sqrt(pow(centro.x-esquina.x,2)+pow(centro.y-esquina.y,2)))+10;	
  //printf("c(%f,%f) e(%f,%f) d->%f\n",centro.x,centro.y,esquina.x,esquina.y,rejilla_rayo.diagonalmedia);		   
  printf("\n resolucion de la rejilla : -->  %f \n",(*grid).resolucion); 
  printf("Relleno de grid correcto.");	 
}


//FIN LECTURA MUNDO

//FUNCIONES Y PROCEDIMIENTOS FILTRO PARTICULAS

void normalizar(XYZ *p)
{
  if( p->Z != 0 )
    {
      p->X=p->X/p->Z;
      p->Y=p->Y/p->Z;
      p->Z=1.;
      //printf("Normalizado -> (%f,%f,%f)\n",p->X,p->Y,p->Z);
    }
}

void reset_correlacion()
{
   correlacion.x=0;
   correlacion.y=0;
   correlacion.xy=0;
   correlacion.x2=0;
   correlacion.y2=0;
   correlacion.xto2=0;
   correlacion.yto2=0;	
}

void relleno_correlacion(int * laser_r,Tlaser_point *laser_t)
{
  int i;
  
  correlacion.y=0;
  correlacion.y2=0;
  correlacion.xy=0;
  correlacion.yto2=0;
  for(i=0;i<NUM_LASER;i=i+RESOLUCION_LASER_TEORICO)
    {
      correlacion.y+= (int)laser_t[i].dis;
      correlacion.y2+= (int)laser_t[i].dis * (int)laser_t[i].dis;
      correlacion.xy+= (int)laser_t[i].dis * laser_r[i];		
    }
  correlacion.yto2=correlacion.y*correlacion.y;
}

float prob_correlacion()
{
  float prob,num,den;
  int n;
  
  
	//printf("x   ->%f\ny   ->%f\nxy  ->%f\nx2  ->%f\ny2  ->%f\nxto2->%f\nyto2->%f\n\n",correlacion.x,correlacion.y,correlacion.xy,correlacion.x2,correlacion.y2,correlacion.xto2,correlacion.yto2);
  n=NUM_LASER/RESOLUCION_LASER_TEORICO;
  
  if(correlacion.y<0.1)
    {
      prob=0;	
    }
  else
    {
      num=((n*correlacion.xy)-(correlacion.x*correlacion.y));
      den=((n*correlacion.x2)-(correlacion.xto2))*((n*correlacion.y2)-(correlacion.yto2));
      den=sqrt(den);
      prob=num/den;
    }
  if(prob<0)
    prob=-prob;
  
  //printf(" %f / %f = %f\n",num,den,prob);
  return prob;
}



// modelo de observacion
float my_random ()
{
  
  return ((float)random())/RAND_MAX;
  	
}


// funcion que aplica ruido guassiano
float gauss_rndm(float std, float mean)
{
  double x;
  double pi, r1, r2;
  
  pi = 4 * atan (1);
  r1 = -log (1 - my_random());
  r2 = 2 * pi * my_random();
  r1 = sqrt (2 * r1);
  x = r1 * cos (r2);	
  return std * x + mean;
}
void init_particles_population(Tvoxel A,Tvoxel B)
{
  
  // A Y B son las esquinas inf izq y sup dcha del "mundo de particulas"
  
  int i;
  float rnd;
  float ancho = B.x-A.x;
  float alto = B.y-A.y;
  struct timeval a;
  
  // utilizamso una semilla aleatoria (el numoro de segundos del reloj del sistema
  // para que la secuencia de numeros aleatorios no sera la misma.
  
  gettimeofday(&a,NULL);
  srand(a.tv_sec);
  
  for(i=0; i< MAX_PARTICLES ; i++)
  {
      //obtenemos un numero aleatorio entre 0-1
    rnd = my_random();
    poblacion1->particles_population[i].point.x = ((float)(ancho*rnd))+ A.x;
    
    rnd = my_random();
    poblacion1->particles_population[i].point.y = ((float)(alto*rnd))+ A.y;
    
    rnd = my_random();
    poblacion1->particles_population[i].theta = (float) (THETA_MAX)*rnd;
    
    poblacion1->particles_population[i].prob = 0.1;
    
    //poblacion1->particles_population[i].diff = 1;
    
    if (i>0)
      {
	poblacion1->particles_population[i].acumulation = poblacion1->particles_population[i].prob 
	  + poblacion1->particles_population[i-1].acumulation;
	// poblacion1->particles_population[i].acumulation = poblacion1->particles_population[i].diff + poblacion1->particles_population[i-1].acumulation;
      }
    else
      {
	poblacion1->particles_population[i].acumulation = poblacion1->particles_population[i].prob;
	//poblacion1->particles_population[i].acumulation = poblacion1->particles_population[i].diff;
      }
    
  }
  
}
// dado un punto (x,y,theta) obtiene la "observación láser teorica" a partir del mismo.

void init_explorer_population(Tvoxel A,Tvoxel B)
{
  
  // A Y B son las esquinas inf izq y sup dcha del "mundo de particulas"
  
  int i;
  float rnd;
  float ancho = B.x-A.x;
  float alto = B.y-A.y;
  struct timeval a;
  
  // utilizamso una semilla aleatoria (el numoro de segundos del reloj del sistema
  // para que la secuencia de numeros aleatorios no sera la misma.
  
  gettimeofday(&a,NULL);
  srand(a.tv_sec);
  
  for(i=0; i< MAX_EXPLORADORES ; i++)
    {
      //obtenemos un numero aleatorio entre 0-1
      rnd = my_random();
      exploradores[i].point.x = ((float)(ancho*rnd))+ A.x;
      
      rnd = my_random();
      exploradores[i].point.y = ((float)(alto*rnd))+ A.y;
      
      rnd = my_random();
      exploradores[i].theta = (float) (THETA_MAX)*rnd;
      
      exploradores[i].prob = 0.1;
      //exploradores[i].prob = colores.color[4];
  }
  
}

inline float MagnitudeSquare( XYZ *punto1, XYZ *punto2 )
{
    XYZ Vector;
    
    Vector.X = punto2->X - punto1->X;
    Vector.Y = punto2->Y - punto1->Y;
    Vector.Z = punto2->Z - punto1->Z;
    
    return (float)( Vector.X * Vector.X + Vector.Y * Vector.Y + Vector.Z * Vector.Z );
}






inline void distanciaPuntoLinea( TipoSegmento s,Tvoxel p,float *l,float *distancia,Tvoxel *punto)
{
  float LineMag;
  float numerador;
  float U;
  //float distancia;
  XYZ interseccion,p1,p2,p3;
  
  p1.X=s.pI.x;   p1.Y=s.pI.y;   p1.Z=1.;
  p2.X=s.pF.x;   p2.Y=s.pF.y;   p2.Z=1.;
  p3.X=p.x;      p3.Y=p.y;      p3.Z=1.;
  
  LineMag = MagnitudeSquare( &p2, &p1 );
  
  numerador=( ( p3.X - p1.X ) * ( p2.X - p1.X ) +
          	    ( p3.Y - p1.Y ) * ( p2.Y - p1.Y ) +
	      ( p3.Z - p1.Z ) * ( p2.Z - p1.Z ) );
  numerador=( p3.X - p1.X ) * ( p2.X - p1.X )+( p3.Y - p1.Y ) * ( p2.Y - p1.Y )+( p3.Z - p1.Z )
    * ( p2.Z - p1.Z )  ;
  U =  numerador/
        ( LineMag );
 
   
  interseccion.X = p1.X + U * ( p2.X - p1.X );
  interseccion.Y = p1.Y + U * ( p2.Y - p1.Y );
  interseccion.Z = p1.Z + U * ( p2.Z - p1.Z );
  
  (*punto).x=interseccion.X;
  (*punto).y=interseccion.Y;
  //asigno el valor de landa
  (*l)=U;
  
  (*distancia)=(pow((p3.X-interseccion.X),2)+pow((p3.Y-interseccion.Y),2));
  
}


inline int pos_celdilla_xmin(lista_ordenada * lista ,int a,int b,float acumulation){
  
  int centro,derecha=b,izquierda=a;
  int found=0;
  
  //printf("Buscamos entre %d y %d\n",a,b);
  while ((!found)&&(derecha>=izquierda)) {
    
    centro = (izquierda+derecha)/2;
    //printf("%d\n",centro);
    found = ((acumulation <=lista[centro].pnt.x ) 
	     && ((acumulation > lista[centro-1].pnt.x )||((centro-1)==-1))) ;
    
    if (!found) {
      if (acumulation <= lista[centro].pnt.x) {
	derecha = centro - 1;
      }else{
	izquierda = centro + 1;
      }
    }
    
  }
  
  if(!found)
    {
      centro=-1; 
  }
  

  return centro;
  
} 
inline int pos_celdilla_ymin(lista_ordenada * lista ,int a,int b,float acumulation){
  
  int centro,derecha=b,izquierda=a;
  int found=0;
  
  
  while ((!found)&&(derecha>=izquierda)) {
    
    centro = (izquierda+derecha)/2;
    //printf("%d\n",centro);
    found = ((acumulation <=lista[centro].pnt.y ) 
	     && ((acumulation > lista[centro-1].pnt.y )||((centro-1)==-1))) ;
    
    if (!found) {
      if (acumulation <= lista[centro].pnt.y) {
	derecha = centro - 1;
      }else{
	izquierda = centro + 1;
      }
    }
    
  }
  
  if(!found)
  {
	 centro=-1; 
  }
  

  return centro;
  
} 
inline int pos_celdilla_xmax(lista_ordenada * lista ,int a,int b,float acumulation){
  
  int centro,derecha=b,izquierda=a;
  int found=0;
  
  
  while ((!found)&&(derecha>=izquierda)) {
    
    centro = (izquierda+derecha)/2;
    //printf("%d\n",centro);
    found = ((acumulation >=lista[centro].pnt.x ) 
	     && ((acumulation < lista[centro+1].pnt.x )||((centro+1)==rejilla_rayo.ocup))) ;
    
    if (!found) {
      if (acumulation >= lista[centro].pnt.x) {
	izquierda = centro + 1;
      }else{
	derecha = centro - 1;
      }
    }
    
  }
  
  if(!found)
  {
	 centro=-1; 
  }
  

  return centro;
  
} 
inline int pos_celdilla_ymax(lista_ordenada * lista ,int a,int b,float acumulation){
  
  int centro,derecha=b,izquierda=a;
  int found=0;
  
  
  while ((!found)&&(derecha>=izquierda)) {
    
    centro = (izquierda+derecha)/2;
    //printf("%d\n",centro);
    found = ((acumulation >=lista[centro].pnt.y ) 
	     && ((acumulation < lista[centro+1].pnt.y )||((centro+1)==rejilla_rayo.ocup))) ;
    
    if (!found) {
      if (acumulation >= lista[centro].pnt.y) {
	izquierda = centro + 1;
      }else{
	derecha = centro - 1;
      }
    }
    
  }
  
  if(!found)
  {
	 centro=-1; 
  }
  

  return centro;
  
}



inline void obtener_laser_teorico(Tvoxel point,float* theta_ref,Tlaser_point *laser_points,Tmundo mundo)    
{
  int i=0,aux;
  int encontrado,celda,salida;
  float distancia,xmin,ymin,xmax,ymax;	
  float landa,dis;
  Tvoxel interseccion;		
  TipoSegmento ray;	
	
  // se resta -90 pq el laser es perpendicular
	
  float ang = *theta_ref - 90 - ALFA;			
 // printf("ANGULO -> %f   \n",ang);	
  //ang+=RAYO;
	
  // hay que calcular el laser teorico teniendo en cuenta el dspz que hay
  // entre el laser y el centro del robot
  Tvoxel point_d,point_i;
  
  point_i.x = point.x;// + DSPZ_CENTRO_LASER*cos(BETA*DEGTORAD);
  point_i.y = point.y;// + DSPZ_CENTRO_LASER*sin(BETA*DEGTORAD);

	for (i=0;i<NUM_LASER;i=i+RESOLUCION_LASER_TEORICO)
    { 	
      //---------------prepara el punto destino---------------------
      laser_points[i].des_point.x = point_i.x+RADIO*cos(DEGTORAD*ang);
      laser_points[i].des_point.y = point_i.y+RADIO*sin(DEGTORAD*ang);	  	
      
      point_d.x = laser_points[i].des_point.x; 
      point_d.y = laser_points[i].des_point.y;
			 
      //------------------------------------------------------------
      
      //Obtenemos el segmento del rayo_laser
      ray.pI=point_i;
      ray.pF=point_d;
      
      if(point_i.x>point_d.x)
	{
	  
	  xmin=point_d.x-laserloc_grid->resolucion/2;
	  xmax=point_i.x+laserloc_grid->resolucion/2;			
	}
      else
	{
	  xmin=point_i.x-laserloc_grid->resolucion/2;
	  xmax=point_d.x+laserloc_grid->resolucion/2;	
	}
      if(point_i.y>point_d.y)
	{
	  
	  ymin=point_d.y-laserloc_grid->resolucion/2;
	  ymax=point_i.y+laserloc_grid->resolucion/2; 			
	}
      else
	{
	  ymin=point_i.y-laserloc_grid->resolucion/2;
	  ymax=point_d.y+laserloc_grid->resolucion/2;	
	}
      //printf("%f\n",ang); 	
      encontrado=FALSE;
      salida=FALSE;		
      
      
      if(((ang<=45)&&(ang>0))||((ang>360)&&(ang<=405)))
	{
	  celda=pos_celdilla_ymin(rejilla_rayo.order_y,0,rejilla_rayo.ocup-1,ymin);
	  aux=celda;
	  
	  
	  while((celda>-1)&&(!encontrado)&&(rejilla_rayo.order_y[celda].pnt.y<=ymax))
	    {
	      celda=pos_celdilla_xmin(rejilla_rayo.order_y,rejilla_rayo.order_y[celda].firstx,rejilla_rayo.order_y[celda].lastx,xmin);	
	      
	      if(celda==-1)
		{  
		  celda=rejilla_rayo.order_y[aux].nexty;
		  aux=celda;
		}
	      else
		{  
		  if(rejilla_rayo.order_y[celda].pnt.x>xmax)
		    {
		      celda=rejilla_rayo.order_y[aux].nexty;
		      aux=celda;  
		    }						  
		  else
		    {
		      salida=FALSE; 
		      while((!encontrado)&&(!salida))
			{
			  distanciaPuntoLinea(ray,rejilla_rayo.order_y[celda].pnt,&landa,&dis,&point_d);
			  
			  if(dis<=rejilla_rayo.diagonalmedia*rejilla_rayo.diagonalmedia)
			    { 
			      encontrado=TRUE;
			    }
			  else
			    {
			      celda++;
			      if((celda>rejilla_rayo.order_y[celda-1].lastx)||(rejilla_rayo.order_y[celda].pnt.x>xmax))
				{
				  salida=TRUE;
				  celda=rejilla_rayo.order_y[aux].nexty;
								 aux=celda;							  
				}						  
			    }
			}
		    }
		  
		  
		}
	    }
	}
      

      else if(((ang<=90)&&(ang>45))||((ang>405)&&(ang<=450)))
	{
	  celda=pos_celdilla_xmin(rejilla_rayo.order_x,0,rejilla_rayo.ocup-1,xmin);
	  aux=celda;
	  
	  while((celda>-1)&&(!encontrado)&&(rejilla_rayo.order_x[celda].pnt.x<=xmax))
	    {
	      celda=pos_celdilla_ymin(rejilla_rayo.order_x,rejilla_rayo.order_x[celda].firsty,
				      rejilla_rayo.order_x[celda].lasty,ymin);	
				   
	      if(celda==-1)
		{  
		  celda=rejilla_rayo.order_x[aux].nextx;
		  aux=celda;
		}
	      else
		{
		  if(rejilla_rayo.order_x[celda].pnt.y>ymax)
		    {
		      celda=rejilla_rayo.order_x[aux].nextx;
		      aux=celda;  
		    }						  
		  else
		    {
		      salida=FALSE; 
		      while((!encontrado)&&(!salida))
			{
			  distanciaPuntoLinea(ray,rejilla_rayo.order_x[celda].pnt,&landa,&dis,&point_d);
			  
			  if(dis<=rejilla_rayo.diagonalmedia*rejilla_rayo.diagonalmedia)
			    { 
			      encontrado=TRUE;
			    }
			  else
			    {
			      celda++;
			      if((celda>rejilla_rayo.order_x[celda-1].lasty)||
				 (rejilla_rayo.order_x[celda].pnt.y>ymax))
				{
				  salida=TRUE;
				  celda=rejilla_rayo.order_x[aux].nextx;
				  aux=celda;							  
				}						  
			    }
			}
		    }
		  
		  
		}
	    }
	  
	}
      else if((ang<=135)&&(ang>90))
	{
	  celda=pos_celdilla_xmax(rejilla_rayo.order_x,0,rejilla_rayo.ocup-1,xmax);
	  aux=celda;
	  
	  
	  while((celda>-1)&&(!encontrado)&&(rejilla_rayo.order_x[celda].pnt.x>=xmin))
	    {
	      celda=pos_celdilla_ymin(rejilla_rayo.order_x,
				      rejilla_rayo.order_x[celda].firsty,
				      rejilla_rayo.order_x[celda].lasty,ymin);	
	      
	      if(celda==-1)
		{  
		  celda=rejilla_rayo.order_x[aux].prevx;
		  aux=celda;
		}
	      else
		{
		  if(rejilla_rayo.order_x[celda].pnt.y>ymax)
		    {
		      celda=rejilla_rayo.order_x[aux].prevx;
		      aux=celda;  
		    }						  
		  else
		    {
		      salida=FALSE; 
		      while((!encontrado)&&(!salida))
			{
			  distanciaPuntoLinea(ray,rejilla_rayo.order_x[celda].pnt,&landa,&dis,&point_d);
			  
			  if(dis<=rejilla_rayo.diagonalmedia*rejilla_rayo.diagonalmedia)
			    { 
			      encontrado=TRUE;
			    }
			  else
			    {
			      celda++;
			      if((celda>rejilla_rayo.order_x[celda-1].lasty)||
				 (rejilla_rayo.order_x[celda].pnt.y>ymax))
				{
				  salida=TRUE;
				  celda=rejilla_rayo.order_x[aux].prevx;
				  aux=celda;							  
				}						  
			    }
			}
		    }
		  
		  
		}
	    }            			
	}
      else if((ang<=180)&&(ang>135))
	{
	  celda=pos_celdilla_ymin(rejilla_rayo.order_y,0,rejilla_rayo.ocup-1,ymin);
	  aux=celda;
	  
	  
	  while((celda>-1)&&(!encontrado)&&(rejilla_rayo.order_y[celda].pnt.y<=ymax))
	    {
	      celda=pos_celdilla_xmax(rejilla_rayo.order_y,
				      rejilla_rayo.order_y[celda].firstx,rejilla_rayo.order_y[celda].lastx,
				      xmax);	
	      
	      if(celda==-1)
		{  
		  celda=rejilla_rayo.order_y[aux].nexty;
		  aux=celda;
		}
	      else
		{  
		  if(rejilla_rayo.order_y[celda].pnt.x<xmin)
		    {
		      celda=rejilla_rayo.order_y[aux].nexty;
		      aux=celda;  
		    }						  
		  else
		    {
		      salida=FALSE; 
		      while((!encontrado)&&(!salida))
			{
			  distanciaPuntoLinea(ray,rejilla_rayo.order_y[celda].pnt,&landa,&dis,&point_d);
			  
			  if(dis<=rejilla_rayo.diagonalmedia*rejilla_rayo.diagonalmedia)
			    { 
			      encontrado=TRUE;
			    }
			  else
			    {
			      celda--;
			      if((celda<rejilla_rayo.order_y[celda+1].firstx)||
				 (rejilla_rayo.order_y[celda].pnt.x<xmin))
				{
				  salida=TRUE;
				  celda=rejilla_rayo.order_y[aux].nexty;
				  aux=celda;							  
				}						  
			    }
			}
		    }

		  
		}
	    }
	}						
      else if((ang<=225)&&(ang>180))
	{
	  celda=pos_celdilla_ymax(rejilla_rayo.order_y,0,rejilla_rayo.ocup-1,ymax);
	  aux=celda;
	  
	  
	  while((celda>-1)&&(!encontrado)&&(rejilla_rayo.order_y[celda].pnt.y>=ymin))
	    {
	      celda=pos_celdilla_xmax(rejilla_rayo.order_y,
				      rejilla_rayo.order_y[celda].firstx,
				      rejilla_rayo.order_y[celda].lastx,xmax);	
				   
	      if(celda==-1)
		{  
		  celda=rejilla_rayo.order_y[aux].prevy;
					  aux=celda;
					  
		}
	      else
		{  
		  if(rejilla_rayo.order_y[celda].pnt.x<xmin)
		    {
		      
		      celda=rejilla_rayo.order_y[aux].prevy;
		      aux=celda; 
		      
		    }						  
		  else
		    {
		      
		      salida=FALSE; 
		      while((!encontrado)&&(!salida))
			{
			  distanciaPuntoLinea(ray,rejilla_rayo.order_y[celda].pnt,&landa,&dis,&point_d);
			  
			  if(dis<=rejilla_rayo.diagonalmedia*rejilla_rayo.diagonalmedia)
			    { 
			      encontrado=TRUE;
			    }
			  else
			    {
			      celda--;
			      if((celda<rejilla_rayo.order_y[celda+1].firstx)||(rejilla_rayo.order_y[celda].pnt.x<xmin))
				{
				  salida=TRUE;
				  celda=rejilla_rayo.order_y[aux].prevy;
				  aux=celda;							  
				}						  
			    }
			}
		    }
		  
		}
	    }
	}						
      else if((ang<=270)&&(ang>225))
	{
	  celda=pos_celdilla_xmax(rejilla_rayo.order_x,0,rejilla_rayo.ocup-1,xmax);
	  aux=celda;
	  
	  while((celda>-1)&&(!encontrado)&&(rejilla_rayo.order_x[celda].pnt.x>=xmin))
	    {
	      celda=pos_celdilla_ymax(rejilla_rayo.order_x,
				      rejilla_rayo.order_x[celda].firsty,
				      rejilla_rayo.order_x[celda].lasty,ymax);	
	      
	      if(celda==-1)
		{  
		  celda=rejilla_rayo.order_x[aux].prevx;
		  aux=celda;
		}
	      else
		{
		  if(rejilla_rayo.order_x[celda].pnt.y<ymin)
		    {
		      celda=rejilla_rayo.order_x[aux].prevx;
		      aux=celda;  
		    }						  
		  else
		    {
		      salida=FALSE; 
		      while((!encontrado)&&(!salida))
			{
			  distanciaPuntoLinea(ray,rejilla_rayo.order_x[celda].pnt,&landa,&dis,&point_d);
			  
			  if(dis<=rejilla_rayo.diagonalmedia*rejilla_rayo.diagonalmedia)
			    { 
			      encontrado=TRUE;
			    }
			  else
			    {
			      celda--;
			      if((celda<rejilla_rayo.order_x[celda+1].firsty)||(rejilla_rayo.order_x[celda].pnt.y<ymin))
				{
				  salida=TRUE;
				  celda=rejilla_rayo.order_x[aux].prevx;
				  aux=celda;							  
				}						  
			    }
			}
		    }
		  
		}
	    }						
	}
      else if((ang<=325)&&(ang>270))
	{
	  celda=pos_celdilla_xmin(rejilla_rayo.order_x,0,rejilla_rayo.ocup-1,xmin);
	  aux=celda;
	  
	  while((celda>-1)&&(!encontrado)&&(rejilla_rayo.order_x[celda].pnt.x<=xmax))
	    {
	      celda=pos_celdilla_ymax(rejilla_rayo.order_x,rejilla_rayo.order_x[celda].firsty,
				      rejilla_rayo.order_x[celda].lasty,ymax);	
	      
	      if(celda==-1)
		{  
		  celda=rejilla_rayo.order_x[aux].nextx;
		  aux=celda;
		}
	      else
		{
		  if(rejilla_rayo.order_x[celda].pnt.y<ymin)
		    {
		      celda=rejilla_rayo.order_x[aux].nextx;
		      aux=celda;  
		    }						  
		  else
		    {
		      salida=FALSE; 
		      while((!encontrado)&&(!salida))
			{
			  distanciaPuntoLinea(ray,rejilla_rayo.order_x[celda].pnt,&landa,&dis,&point_d);
			  
			  if(dis<=rejilla_rayo.diagonalmedia*rejilla_rayo.diagonalmedia)
			    { 
			      encontrado=TRUE;
			    }
			  else
			    {
			      celda--;
			      if((celda<rejilla_rayo.order_x[celda+1].firsty)||(rejilla_rayo.order_x[celda].pnt.y<ymin))
				{
				  salida=TRUE;
				  celda=rejilla_rayo.order_x[aux].nextx;
				  aux=celda;							  
				}						  
			    }
			}
		    }
		  
		}
	    }						
	}
      else
	{
	  celda=pos_celdilla_ymax(rejilla_rayo.order_y,0,rejilla_rayo.ocup-1,ymax);
	  aux=celda;
	  
	  while((celda>-1)&&(!encontrado)&&(rejilla_rayo.order_y[celda].pnt.y>=ymin))
	    {
	      celda=pos_celdilla_xmin(rejilla_rayo.order_y,
				      rejilla_rayo.order_y[celda].firstx,
				      rejilla_rayo.order_y[celda].lastx,xmin);	
	      
	      if(celda==-1)
		{  
		  celda=rejilla_rayo.order_y[aux].prevy;
		  aux=celda;
		}
	      else
		{  
		  if(rejilla_rayo.order_y[celda].pnt.x>xmax)
		    {
		      celda=rejilla_rayo.order_y[aux].prevy;
		      aux=celda;  
		    }						  
		  else
		    {
		      salida=FALSE; 
		      while((!encontrado)&&(!salida))
			{
			  distanciaPuntoLinea(ray,rejilla_rayo.order_y[celda].pnt,&landa,&dis,&point_d);
			  
			  if(dis<=rejilla_rayo.diagonalmedia*rejilla_rayo.diagonalmedia)
			    { 
			      encontrado=TRUE;
			    }
			  else
			    {
			      celda++;
			      if((celda>rejilla_rayo.order_y[celda-1].lastx)||
				 (rejilla_rayo.order_y[celda].pnt.x>xmax))
				{
				  salida=TRUE;
				  celda=rejilla_rayo.order_y[aux].prevy;
				  aux=celda;							  
				}						  
			    }
			}
		    }
		  
		}
	    }
	}						
      
      
      if(encontrado)
	{
	  
          interseccion.x = ray.pI.x + landa * ( ray.pF.x - ray.pI.x );
          interseccion.y = ray.pI.y + landa * ( ray.pF.y - ray.pI.y );
	  
          distancia=sqrt(pow(interseccion.x-point_i.x,2)+pow(interseccion.y-point_i.y,2));		
	  
	}
      else 
	{ 
	  distancia=RADIO;			
	}
      
      point_d.x = point_i.x + distancia*cos(DEGTORAD*ang);
      point_d.y = point_i.y + distancia*sin(DEGTORAD*ang);
		   
      laser_points[i].teorico_point=point_d;
      laser_points[i].robot_point=point_i;			
      laser_points[i].dis = distancia;
      
      if(!PARTICLES_ACTIVE)
	distancias[i] = distancia; 
      ang = ang + INC_ANG*RESOLUCION_LASER_TEORICO;
      
    }
}

inline void obtener_laser_teorico2(Tvoxel point,float* theta_ref,Tlaser_point *laser_points,Tmundo mundo)    
{
  int i=0,col,fil,j,aux,cont=0,cont2=0;
  int celda,a,b,encontrado;
  int anterior,distancia;	
  TipoSegmento ray;	
  Tvoxel interseccion;
  float dis,landa;	
  // se resta -90 pq el laser es perpendicular
  float ang = *theta_ref - 90 - ALFA;			
	//printf("ANGULO -> %f   \n",(*theta_ref));	
  //ang+=RAYO;
  // hay que calcular el laser teorico teniendo en cuenta el dspz que hay
  // entre el laser y el centro del robot
  Tvoxel point_d,point_i;
  
  point_i.x = point.x + DSPZ_CENTRO_LASER*cos(BETA*DEGTORAD);
  point_i.y = point.y + DSPZ_CENTRO_LASER*sin(BETA*DEGTORAD);
  //point_i.x = point.x;
  //point_i.y = point.y;
  
  
  //for (i=0;i<NUM_LASER;i++)
  for (i=0;i<NUM_LASER;i=i+RESOLUCION_LASER_TEORICO)
    { 	
	  
      free(rejilla_rayo.celdas);
      free(rejilla_rayo.ocupado);
      free(rejilla_rayo.landas);	
	  	
      //---------------prepara el punto destino---------------------
      laser_points[i].des_point.x = point_i.x+RADIO*cos(DEGTORAD*ang);
      laser_points[i].des_point.y = point_i.y+RADIO*sin(DEGTORAD*ang);	  	
  	
      point_d.x = laser_points[i].des_point.x; 
      point_d.y = laser_points[i].des_point.y;
		
	 
      //------------------------------------------------------------
      
	  //Obtenemos el segmento del rayo_laser
      ray.pI=point_i;
      ray.pF=point_d;
      
	    		   
      celda=-1;	
	  	
      jmin=(point_i.x-laserloc_grid->lo.x+laserloc_grid->resolucion/2)/laserloc_grid->resolucion;
      imin=(point_i.y-laserloc_grid->lo.y+laserloc_grid->resolucion/2)/laserloc_grid->resolucion; 	
		
      jmax=(point_d.x-laserloc_grid->lo.x+laserloc_grid->resolucion/2)/laserloc_grid->resolucion;
      imax=(point_d.y-laserloc_grid->lo.y+laserloc_grid->resolucion/2)/laserloc_grid->resolucion; 
      
      
      if(imin>imax)
	{
	  aux=imin;
	  imin=imax;
	  imax=aux;			
	}
      if(jmin>jmax)
	{
	  aux=jmin;
	  jmin=jmax;
	  jmax=aux; 			
	}
		
      if(jmin<0)
	jmin=0;
      if(imin<0)
	imin=0;	
      if(jmax<0)
	jmax=0;	
      if(imax<0)
	imax=0;
      if(jmin>mundo.ancho)
	jmin=mundo.ancho;
      if(imin>laserloc_grid->size)
	imin=laserloc_grid->size;	
      if(jmax>mundo.ancho)
	jmax=mundo.ancho;	
      if(imax>laserloc_grid->size)
	imax=laserloc_grid->size;
      
      
      aux=(imax-imin);
      if(aux==0)
		{
		  aux=1;
		}
      else
	{
	  aux++;
	}			
      rejilla_rayo.tamanyo=aux;
      
      aux=(jmax-jmin);
      if(aux==0)
	{
	  aux=1;
	}
      else
	{
	  aux++; 
	}
      
      
      rejilla_rayo.tamanyo=rejilla_rayo.tamanyo*aux;
      
      rejilla_rayo.celdas=(int*)malloc(rejilla_rayo.tamanyo*sizeof (int));
      rejilla_rayo.ocupado=(int*)malloc(rejilla_rayo.tamanyo*sizeof (int));
      rejilla_rayo.landas=(int*)malloc(rejilla_rayo.tamanyo*sizeof (int));
      
      rejilla_rayo.posMinima=-1;
      j=0;
      //printf("ANGULO -> %f   \n",ang);
      if(((ang<=180)&&(ang>0))||(ang>360))
	{
	  if((ang>90)&&(ang<=180))//primer cuadrante
	    { 
	      cont=0;cont2=0;encontrado=FALSE;
	      for(fil=imin;fil<=imax;fil++)
		{  
		  for(col=jmax;col>=jmin;col--)
		    {  
		      rejilla_rayo.celdas[j]=fil*laserloc_grid->size+col;
		      aux= rejilla_rayo.celdas[j];
		      if((laserloc_grid->map[aux].estado==-1)&&(!encontrado))
			{
			  cont2++;											  
			  distanciaPuntoLinea(ray,laserloc_grid->map[aux].centro,&landa,&dis,&point_d);
				    //printf("d->\n",dis);
			  if(dis<rejilla_rayo.diagonalmedia*rejilla_rayo.diagonalmedia)
			    { 
			      cont++; 
			      encontrado=TRUE;  
			      rejilla_rayo.ocupado[j]=TRUE;  
			      rejilla_rayo.landas[j]=landa;
			      
			      //printf(" %f\n",rejilla_rayo.landas[j]);
			      if(rejilla_rayo.posMinima==-1)
				{
				  rejilla_rayo.posMinima=j;
				}
			      else
				{		
				  if(rejilla_rayo.landas[rejilla_rayo.posMinima]>
				     rejilla_rayo.landas[j])	
				    {
				      rejilla_rayo.posMinima=j;
				    }
				}
			      
												
			    }
			  if(!encontrado)
			    {
			      rejilla_rayo.ocupado[j]=FALSE; 
					 } 
			}
		      else
			{
			  rejilla_rayo.ocupado[j]=FALSE;
			}				 
		      //printf("pos[%d,%d] celda->%d\n",fil,col,rejilla_rayo.celdas[j]);
				 //printf("pos[%d,%d] \n",fil,col);			  
		      j++;
		      if(encontrado)	 
			break;					 
		    }
		  if(encontrado){
		    //printf("(%d,%d)(%d,%d)\n",fil,col,imax,jmin);
		    break;
		  }
		} 
	      // printf("ANGULO->%f Interseccionan->%d Ocupados->%d Total->%d i(%d,%d)j(%d,%d)\n",ang,cont,cont2,rejilla_rayo.tamanyo,imin,imax,jmin,jmax);			
	    }
	  else
		   { 
		     cont=0;cont2=0;encontrado=FALSE;  
		     for(fil=imin;fil<=imax;fil++)
		       {  
			 for(col=jmin;col<=jmax;col++)
			   {  
			     rejilla_rayo.celdas[j]=fil*laserloc_grid->size+col;
			     aux= rejilla_rayo.celdas[j];
			     if((laserloc_grid->map[aux].estado==-1)&&(!encontrado))
			       {
				 cont2++;											  
				 distanciaPuntoLinea(ray,laserloc_grid->map[aux].centro,&landa,&dis,&point_d);
				 //printf("d->\n",dis);
				 if(dis<rejilla_rayo.diagonalmedia*rejilla_rayo.diagonalmedia)
				   { 
				     cont++; 
				     encontrado=TRUE;  
				     rejilla_rayo.ocupado[j]=TRUE;  
				     rejilla_rayo.landas[j]=landa;
				     
				     //printf(" %f\n",rejilla_rayo.landas[j]);
				     if(rejilla_rayo.posMinima==-1)
				       {
					 rejilla_rayo.posMinima=j;
				       }
				     else
				       {		
					 if(rejilla_rayo.landas[rejilla_rayo.posMinima]>
					    rejilla_rayo.landas[j])	
					   {
					     rejilla_rayo.posMinima=j;
					   }
				       }
				     
				     
				   }
				 if(!encontrado)
				   {
				     rejilla_rayo.ocupado[j]=FALSE; 
				   } 
			       }
			     else
			       {
				 rejilla_rayo.ocupado[j]=FALSE;
			       }				 
			     //printf("pos[%d,%d] celda->%d\n",fil,col,rejilla_rayo.celdas[j]);
			     //printf("pos[%d,%d] \n",fil,col);			  
			     j++;
			     if(encontrado)
			       break;					 
			   }
			 if(encontrado)
			   break;
		       }
		     //printf("ANGULO->%f Interseccionan->%d Ocupados->%d Total->%d i(%d,%d)j(%d,%d)\n",ang,cont,cont2,rejilla_rayo.tamanyo,imin,imax,jmin,jmax);
		   }			   
	}
      else
	{
	  if((ang>270)||(ang<=0))
	    { 
	      cont=0;cont2=0;encontrado=FALSE;
	      for(fil=imax;fil>=imin;fil--)
		{  
		  for(col=jmin;col<=jmax;col++)
		    {  
		      rejilla_rayo.celdas[j]=fil*laserloc_grid->size+col;
		      aux= rejilla_rayo.celdas[j];
		      if(laserloc_grid->map[aux].estado==-1)
			{ 
			  cont2++;											  
			  
			  distanciaPuntoLinea(ray,laserloc_grid->map[aux].centro,&landa,&dis,&point_d);
			  //dis=sqrt(laserloc_grid->map[aux].centro.x-interseccion.x*laserloc_grid->map[aux].centro.x-interseccion.x+laserloc_grid->map[aux].centro.y-interseccion.y*laserloc_grid->map[aux].centro.y-interseccion.y);
					 //printf("p1->(%f,%f)p2->(%f,%f)d->%f\n",laserloc_grid->map[aux].centro.x,laserloc_grid->map[aux].centro.y,interseccion.x,interseccion.y,dis);
			  if(dis<rejilla_rayo.diagonalmedia*rejilla_rayo.diagonalmedia)
			    { 
			      cont++; 
			      encontrado=TRUE;  
			      rejilla_rayo.ocupado[j]=TRUE;  
			      rejilla_rayo.landas[j]=landa;
			      
			      //printf(" %f\n",rejilla_rayo.landas[j]);
			      if(rejilla_rayo.posMinima==-1)
				{
				  rejilla_rayo.posMinima=j;
				}
			      else
				{		
				  if(rejilla_rayo.landas[rejilla_rayo.posMinima]>
				     rejilla_rayo.landas[j])	
				    {
				      rejilla_rayo.posMinima=j;
				    }
				}
			      
			      
			    }
			  if(!encontrado)
			    {
			      rejilla_rayo.ocupado[j]=FALSE; 
			    } 
			}
		      else
			{
			  rejilla_rayo.ocupado[j]=FALSE;
			}				 
		      //printf("pos[%d,%d] celda->%d\n",fil,col,rejilla_rayo.celdas[j]);
		      //printf("pos[%d,%d] \n",fil,col);			  
		      j++;
		      if(encontrado)
			break;				 
		    }
		  if(encontrado)
		    break;
		}
	      //printf("ANGULO->%f Interseccionan->%d Ocupados->%d Total->%d i(%d,%d)j(%d,%d)\n",ang,cont,cont2,rejilla_rayo.tamanyo,imin,imax,jmin,jmax);
	    }
	  else
	    { 
	      cont=0;cont2=0;encontrado=FALSE;
	      for(fil=imax;fil>=imin;fil--)
		{  
		  for(col=jmax;col>=jmin;col--)
		    {  
		      rejilla_rayo.celdas[j]=fil*laserloc_grid->size+col;
		      aux= rejilla_rayo.celdas[j];
		      if(laserloc_grid->map[aux].estado==-1)
			{ 
			  cont2++;											  
			  distanciaPuntoLinea(ray,laserloc_grid->map[aux].centro,&landa,&dis,&point_d);
			  //printf("d->\n",dis);
			  if(dis<rejilla_rayo.diagonalmedia*rejilla_rayo.diagonalmedia)
			    { 
			      cont++; 
			      encontrado=TRUE;  
			      rejilla_rayo.ocupado[j]=TRUE;  
			      rejilla_rayo.landas[j]=landa;
			      
			      //printf(" %f\n",rejilla_rayo.landas[j]);
			      if(rejilla_rayo.posMinima==-1)
				{
				  rejilla_rayo.posMinima=j;
				}
			      else
				{		
				  if(rejilla_rayo.landas[rejilla_rayo.posMinima]>
				     rejilla_rayo.landas[j])	
				    {
				      rejilla_rayo.posMinima=j;
				    }
				}
			      
			      
			    }
			  if(!encontrado)
			    {
			      rejilla_rayo.ocupado[j]=FALSE; 
			    } 
			}
		      else
			{
			  rejilla_rayo.ocupado[j]=FALSE;
			}				 
		      //printf("pos[%d,%d] celda->%d\n",fil,col,rejilla_rayo.celdas[j]);
		      //printf("pos[%d,%d] \n",fil,col);			  
		      j++;
		      if(encontrado)
			break;				 
		    }
		  if(encontrado)
		    break;
		}
	      //printf("ANGULO->%f Interseccionan->%d Ocupados->%d Total->%d i(%d,%d)j(%d,%d)\n",ang,cont,cont2,rejilla_rayo.tamanyo,imin,imax,jmin,jmax);
	    }
	}			
      //printf("\n(%d,%d),(%d,%d) TAMANYO-> %d\n",imin,imax,jmin,jmax,rejilla_rayo.tamanyo); 	
      
        
      
      if(rejilla_rayo.posMinima!=-1)
	{
	  
          distancia=sqrt(pow(point_d.x-point_i.x,2)+pow(point_d.y-point_i.y,2));		
	  
	}
      else
	{
	  distancia=RADIO;
	  point_d.x = point_i.x + distancia*cos(DEGTORAD*ang);
	  point_d.y = point_i.y + distancia*sin(DEGTORAD*ang);	
	  
	}
      
      
      
      //printf("\n(%d,%d),(%d,%d) TAMANYO-> %d\n",imin,imax,jmin,jmax,rejilla_rayo.tamanyo); 	
      //printf("%d %d\n",rejilla_rayo.tamanyo,j);
		   
      laser_points[i].teorico_point=point_d;
      laser_points[i].robot_point=point_i;			
      laser_points[i].dis = distancia;
      
      if(!PARTICLES_ACTIVE)
	distancias[i] = distancia;			  
      
      //printf("ANGULO -> %f   ",ang);
      //ang = ang + INC_ANG;
      ang = ang + INC_ANG*RESOLUCION_LASER_TEORICO;
      
    }
  
}

// devuelve el numero de posiciones que NO coinciden dado un cierto rango
// de distancia laser (si coincide la pos se resta uno a sum)
inline float distance_manhatan(Tlaser_point *laser_teorico,int *laser_real) 
  
{ 
  int i;
  float sum;
  float prob;
  
  
  
	
  
  if(localization_statedis==simple)
    {	   
      sum=NUM_LASER/RESOLUCION_LASER_TEORICO;
      
      for (i=0;i<NUM_LASER;i=i+RESOLUCION_LASER_TEORICO)
	{            
	  if (abs(laser_real[i]-(int)laser_teorico[i].dis)<distance_range) sum -= 1;
	}
      
      if(localization_stateprob==potencial)
	{
	  prob = pow((float)((NUM_LASER/RESOLUCION_LASER_TEORICO)-sum)/
		     (NUM_LASER/RESOLUCION_LASER_TEORICO),POTENCIAS); 
	  //printf("distancia->%f, prob->%f",sum,prob);
	}
      else if(localization_stateprob==exponencial)	
	{
	  prob=(float)(exp(-sum/(NUM_LASER/RESOLUCION_LASER_TEORICO/EXPONENTES))); 
	}
      
    }  
  
  else if(localization_statedis==sumatorio)
    {	  
      
      sum=0;
      
      for (i=0;i<NUM_LASER;i=i+RESOLUCION_LASER_TEORICO)
	{            
	  
	  sum+=abs(laser_real[i]-(int)laser_teorico[i].dis);
	  
	}
      
      /*if(localization_stateprob==potencial)	
	{
	prob=pow((float)((720000)-sum)/(720000),POTENCIAD); 
	
	}
	else if(localization_stateprob==exponencial)	
	{
	prob=(float)(exp(-sum/(720000/EXPONENTED))); 
	}*/
      
      if(localization_stateprob==potencial)	
	{
	  if(sum>(720000)/POTENCIAD)
	    prob=0.1;
	  else
	    //prob=pow((float)((720000)-sum)/(720000),POTENCIAD); 
	    prob=pow((float)((720000/POTENCIAD)-sum)/(720000/POTENCIAD),1); 
	  
	}
      else if(localization_stateprob==exponencial)	
	{
	  if(sum>(720000)/EXPONENTED)
	    prob=0.1;
	  else
	    prob=(float)(exp(-sum/(720000/EXPONENTED/1))); 
	  //prob=(float)(exp(-sum/(720000/EXPONENTED))); 
	}/**/
      
    }
  else if(localization_statedis==correlation)
    {  
      
      //relleno_correlacion(&laser_correlacion,laser_teorico);
      relleno_correlacion(laser_correlacion,laser_teorico); 
      if(localization_stateprob==potencial)	
	{  
	  prob=pow(prob_correlacion(),POTENCIAC);
	  //printf("dist->%f, prob->%f\n",prob_correlacion(),prob);		 
	}
      else if(localization_stateprob==exponencial)	
	{  
	  prob=prob_correlacion();
	  if(prob==0) prob=0.1; //para evitar la division por cero
	  prob=exp((1-(1/prob))*(EXPONENTEC));
	  //printf("dist->%f , prob->%f\n",prob_correlacion(),prob);
	} 
      
    }	
  
  
  return prob;
  
}

inline void incorpora_obs_particulas(Tmundo mundo)
{
  int i;
  float prob,mayor;

  mayor=0.;

  	
  for(i=0; i< MAX_PARTICLES ; i++)
    //for(i=5900; i< 5918 ; i++)	
    {
      
      //calcular la distancia del laser teorico, respecto al laser real
      //printf("%d ",i);
      obtener_laser_teorico(poblacion1->particles_population[i].point,&poblacion1->particles_population[i].theta,my_laser,mundo);
      //prob = distance_manhatan(&my_laser,&mylaser);
      prob = distance_manhatan(my_laser,mylaser);
      
	 
      
      //printf("prob->%f\n",prob);  
      if (prob == 1.) prob = 0.99;
      if (prob == 0.) prob = 0.01;
      //else prob=0.9;
	  
      
      
      //printf("pos->%d dist->%d prob->%f\n",i,distancia,prob);
      
      //poblacion1->particles_population[i].diff = distancia;
      poblacion1->particles_population[i].prob = prob;
      //misma_comp[i]=prob;
      //pruebas[i]=prob;
      
      
	
      // para evitar accesos fuera del array
      if (i>0)
	  {
	    poblacion1->particles_population[i].acumulation = 
	      poblacion1->particles_population[i].prob + poblacion1->particles_population[i-1].acumulation;
	    //  poblacion1->particles_population[i].acumulation = poblacion1->particles_population[i].diff + poblacion1->particles_population[i-1].acumulation;
	  }
      else
	{
	  poblacion1->particles_population[i].acumulation = poblacion1->particles_population[i].prob;
	  //  poblacion1->particles_population[i].acumulation = poblacion1->particles_population[i].diff;
	}
      
      
      //if(prob>0.9)
      //	  poblacion1->particles_population[i].acumulation+=prob*100;
      
    }
  
  
}

void enPosicionMenorProbabilidad( plistaExp *lista, Tparticle p )
{
  plistaExp aux,menor;
  float probmenor=2;
  int i,pos=-1;
  
  aux=*lista;
  while (aux!=NULL) 
    {
      if (aux->exp1[MAX_EXPLOTADORES-1].prob<probmenor)
	{
	  probmenor=aux->exp1[MAX_EXPLOTADORES-1].prob;
	  menor=aux;			
	  pos=0;
	}
      
      
      aux=aux->siguiente;
    }
  
  if(p.prob>menor->exp1[MAX_EXPLOTADORES-1].prob)
    {
      
      /*if (pos ==0)
	printf("PUES SI LA ELIMINARON CON PROBABILIDAD %f \n",menor->exp1[MAX_EXPLOTADORES-1].prob);*/
      for(i=0;i<MAX_EXPLOTADORES;i++)
	{
	  menor->exp1[i]=p;
	}		  
    }	   
}

inline void incorpora_mov_raw_odom(float *ruidox, float *ruidoy,float *ruidoang,float r_noise[],float delta_r, float delta_theta)
{
  float ruido_radiox=0,ruido_radioy=0,ruido_theta=0;
  float nuevo_x,nuevo_y,nueva_theta,ang;
  
  ruido_radiox = gauss_rndm(SIGMA_RADIO_NOISE,0.);
  ruido_radioy = gauss_rndm(SIGMA_RADIO_NOISE,0.);
  ruido_theta = gauss_rndm(SIGMA_THETA_NOISE,0.);
  
  (*ruidox)=ruido_radiox;
  (*ruidoy)=ruido_radioy;
  (*ruidoang)=ruido_theta;
	  
  //printf("(x-> %f ,ang -> %f)\n",ruido_radiox,ruido_theta);
  ang=r_noise[2]*RADTODEG; 
  //ang=ang+delta_theta/2;  
  
  nuevo_x = r_noise[0] + ruido_radiox + (delta_r)*cos((ang)*DEGTORAD);
  nuevo_y = r_noise[1] + ruido_radioy + (delta_r)*sin((ang)*DEGTORAD);  
  nueva_theta =  (r_noise[2]*RADTODEG)+delta_theta+ruido_theta ;
  
  
  if(nueva_theta>=360)
    nueva_theta-=360;
  else if(nueva_theta<0)
    nueva_theta+=360;
  
  r_noise[0] = nuevo_x;
  r_noise[1] = nuevo_y;		
  r_noise[2] = nueva_theta*DEGTORAD;
  r_noise[3] = cos(r_noise[2]);
  r_noise[4] = sin(r_noise[2]);
  
  
}

inline void incorpora_mov_estimate_odom(float ruidox, float ruidoy, float ruidoang, float r_estimate[],float delta_r, float delta_theta)
{
  float nuevo_x,nuevo_y,nueva_theta,ang;
  ruidox=0.0;
  ruidoy=0.0;
  ruidoang=0.0;
  ang=r_estimate[2]*RADTODEG;  
  ruidox = 0.0; ruidoy=0.0;ruidoang=0.0;
  nuevo_x = r_estimate[0] + ruidox +(delta_r)*cos((ang)*DEGTORAD);
  nuevo_y = r_estimate[1] + ruidoy +(delta_r)*sin((ang)*DEGTORAD);
  nueva_theta =  (r_estimate[2]*RADTODEG)+delta_theta+ruidoang;
  if(nueva_theta>=360)
    nueva_theta-=360;
  else if(nueva_theta<0)
    nueva_theta+=360;
  
  
  
  r_estimate[0] = nuevo_x;
  r_estimate[1] = nuevo_y;
  r_estimate[2] = nueva_theta*DEGTORAD;
  r_estimate[3] = cos(r_estimate[2]);
  r_estimate[4] = sin(r_estimate[2]);	  
  
}

inline void incorpora_obs_exploradores(Tparticle *exp, Tmundo mundo,TColorExplotador *colores)
{

  int i,Similar;
  float prob,mayor;
  float saludDelSimilar,saludDelIgual;
  mayor=0.;

  	
  for(i=0; i< MAX_EXPLORADORES ; i++)
  {
      
    //calcular la distancia del laser teorico, respecto al laser real
      //printf("%d ",i);
    if(localization_statemode==loc)
      obtener_laser_teorico(exp[i].point,&exp[i].theta,my_laser,mundo);
    else
      obtener_laser_teorico2(exp[i].point,&exp[i].theta,my_laser,mundo); 
    //prob = distance_manhatan(&my_laser,&mylaser);
    prob = distance_manhatan(my_laser,mylaser);
    
    //printf("  %f \n ",prob);
	  
    if (prob == 1) prob = 0.99;
    if (prob < 0.01) prob = 0.01;
    
    //printf("  %f \n ",prob);
    
    //printf("pos->%d  prob->%f\n",i,prob);
    
    //poblacion1->particles_population[i].diff = distancia;
    //exp[i].prob = prob;
    exp[i].prob = prob*0.5;//0.85
    //misma_comp[i]=prob;
    //pruebas[i]=prob;
    InsertarNodoPartOrdenadas(&ordenadas,exp[i]);
    //if (exp[i].prob >= UMBRAL_EXPLOTADORES) {
       if (exp[i].prob >= UMBRAL_EXPLOTADORES*0.5) {
      //exp[i].prob = exp[i].prob*NEW_ALPHA;
      if (numElementosLista < NUM_RAZAS) {
	Check_Similarity(&explotadores, exp[i], &Similar, &saludDelSimilar);
	if (Similar==0) {
	  InsertarNodo(&explotadores, exp[i],colores);
	  
	} else {
	  MeterParticula(exp[i], explotadores, Similar);
	  
	}
      }
      else
	{
	  enPosicionMenorProbabilidad(&explotadores,exp[i]);
	}
    }
	 //if(exp[i].prob>UMBRAL_EXPLOTADORES)
	 //      printf("prob->%f\n",exp[i].prob);
    
    if((i%(MAX_EXPLOTADORES*4))==0)
      {
	dif_noise_x = myencoders[0]-disp_noise_x;
	dif_noise_y = myencoders[1]-disp_noise_y;
	dif_noise_theta = (RADTODEG*myencoders[2])-disp_noise_theta; 
	modulo_noise=pow(dif_noise_x*dif_noise_x+dif_noise_y*dif_noise_y,0.5);			
	//incorpora_mov_raw_odom(&ruidox,&ruidoy,&ruidoang,raw_odom,modulo_noise,dif_noise_theta);	  	
	incorpora_mov_estimate_odom(ruidox,ruidoy,ruidoang,pose_estimate,modulo_noise,dif_noise_theta);	  		
	disp_noise_x = myencoders[0];
	disp_noise_y = myencoders[1];  
	disp_noise_theta = (RADTODEG*myencoders[2]);
	//printf("E(%f,%f,%f)real(%f,%f,%f)\n",raw_odom[0],raw_odom[1],raw_odom[2]*RADTODEG,myencoders[0],myencoders[1],myencoders[2]*RADTODEG);
      }
  }
  
  
}



inline void incorpora_obs_explotadores(Tparticle *lista, Tmundo mundo, int lider)
{
  
  int i;
  float prob;
  plistaExp plista;
  
  
  
  //printf("entro --");		
  for(i=0; i< MAX_EXPLOTADORES ; i++)
    {
      
      //calcular la distancia del laser teorico, respecto al laser real
      //printf("%d ",i);
      if(localization_statemode==loc)
	obtener_laser_teorico(lista[i].point,&lista[i].theta,my_laser,mundo);
      else
	obtener_laser_teorico2(lista[i].point,&lista[i].theta,my_laser,mundo); 
      //prob = distance_manhatan(&my_laser,&mylaser);
      prob = distance_manhatan(my_laser,mylaser);
      
      //printf("  %f \n ",prob);
      
      if (prob == 1.) prob = 0.99;
      if (prob < 0.01) prob = 0.01;
      
      //printf("  %f \n ",prob);
      
      //printf("pos->%d dist->%d prob->%f\n",i,distancia,prob);
      
      //poblacion1->particles_population[i].diff = distancia;
      if (lider)
	lista[i].prob = (lista[i].prob*0.7 + (1-0.3)*prob);
      else 
	lista[i].prob = (lista[i].prob*ALPHA + (1-ALPHA)*prob);//2
      //lista[i].prob=prob;
      InsertarNodoPartOrdenadas(&ordenadas,lista[i]);
      //misma_comp[i]=prob;
      //pruebas[i]=prob;
      //printf("(%f,%f,%f) (%f)\n",lista[i].point.x,lista[i].point.y,lista[i].theta,lista[i].prob);
      
    } 
  
  //printf("SALIMOS\n");
}



// modelo de movimiento
inline void incorpora_mov_particulas(float delta_x, float delta_y, float delta_theta)
{
  int i;
  float ruido_radiox=0,ruido_radioy=0,ruido_theta=0;
  float nuevo_x,nuevo_y,nueva_theta,delta_r,ang;
  //printf(" %f -- %f \n", delta_r,delta_theta);
	
  float a,b;  
	
  delta_r=pow((delta_x*delta_x)+(delta_y*delta_y),0.5);	
  for(i=0; i< MAX_PARTICLES ; i++)
    {
      
      ruido_radiox = gauss_rndm(SIGMA_RADIO,0.);
      ruido_radioy = gauss_rndm(SIGMA_RADIO,0.);
      ruido_theta = gauss_rndm(SIGMA_THETA,0.);
      
      ang=poblacion1->particles_population[i].theta; 
      
      
      nuevo_x = poblacion1->particles_population[i].point.x + (delta_r + ruido_radiox)*cos((ang)*DEGTORAD);
      nuevo_y = poblacion1->particles_population[i].point.y + (delta_r + ruido_radioy)*sin((ang)*DEGTORAD);
      nueva_theta = (poblacion1->particles_population[i].theta+delta_theta+ruido_theta);
      if(nueva_theta>=360)
	nueva_theta-=360;
      else if(nueva_theta<0)
	nueva_theta+=360;
      
      // verificamos si la nueva particula cae dentro de nuestro mundo, en caso contrario no se hace
      // nada con la particula antigua, lo suyo sera volver a poner esta particula en una posición dentro
      // del mundo sino esta particula nunca se moverá de este punto.
      
      // la condicion se aplica sobre las dos variables a la vez (el punto (x,y,theta) tiene que cair dentro del mundo)
      
      if ((nuevo_x>A.x) && (nuevo_x < B.x) && (nuevo_y>A.y) && (nuevo_y<B.y)) {
	
	poblacion1->particles_population[i].point.x = nuevo_x;
	poblacion1->particles_population[i].point.y = nuevo_y;
	poblacion1->particles_population[i].theta = nueva_theta;
		  
      }
     
    }
	
}





inline void incorpora_mov_explotadores(Tparticle *lista,float delta_r, float delta_theta)
{
  int i;
  float ruido_radiox=0,ruido_radioy=0,ruido_theta=0;
  float nuevo_x,nuevo_y,nueva_theta,ang;
  plistaExp plista;
	

  for(i=0; i< MAX_EXPLOTADORES ; i++)
    {
      
      
      ang=lista[i].theta; 
      
      
      
      nuevo_x = lista[i].point.x + (delta_r)*cos((ang)*DEGTORAD);
      nuevo_y = lista[i].point.y + (delta_r)*sin((ang)*DEGTORAD);			
      nueva_theta =  lista[i].theta+delta_theta;
      if(nueva_theta>=360)
	nueva_theta-=360;
      else if(nueva_theta<0)
	nueva_theta+=360;
      
      
      // verificamos si la nueva particula cae dentro de nuestro mundo, en caso contrario no se hace
      // nada con la particula antigua, lo suyo sera volver a poner esta particula en una posición dentro
      // del mundo sino esta particula nunca se moverá de este punto.
      
      // la condicion se aplica sobre las dos variables a la vez (el punto (x,y,theta) 
      //tiene que cair dentro del mundo)
      
      if ((nuevo_x>A.x) && (nuevo_x < B.x) && (nuevo_y>A.y) && (nuevo_y<B.y)) 
	{
	  
	  lista[i].point.x = nuevo_x;
	  lista[i].point.y = nuevo_y;
	  lista[i].theta = nueva_theta;			  
	}
      
    }
  
  
}

// buscar la pos de la particula usando busqueda binaria dada su "acumulation"
inline int pos_particula(float acumulation){
  
  int centro,derecha=MAX_PARTICLES-1,izquierda=0;
  int found=0;
  
  
  while (!found) {
    
    centro = (izquierda+derecha)/2;
   
    found = ((acumulation >= poblacion1->particles_population[centro].acumulation) 
	     && (acumulation < poblacion1->particles_population[centro+1].acumulation)) ;
    
    if (!found) {
      if (acumulation < poblacion1->particles_population[centro].acumulation) {
	derecha = centro - 1;
      }else{
	izquierda = centro + 1;
      }
    }
    
  }
  
  //devolvemos la posicion mas cercana a la acumulacion

  if (abs(acumulation-poblacion1->particles_population[centro].acumulation) < 
      abs(acumulation-poblacion1->particles_population[centro+1].acumulation) ) {
    return centro;
  }else{
    return centro+1;
  }
  
} 

inline void remuestreo_particulas(){
  
  int i,pos;
  float dado;
  float ancho = B.x-A.x;
  float alto = B.y-A.y;
  TParticles_population *aux;	
  for(i=0; i< MAX_PARTICLES ; i++)
  {
      
      // para cada particula tiramos el dado entre 0-acumulation donde acumulation es la suma de todas 
      // las probabilidades de todas las practicas (este valor lo almacena el campo acumulation de la utlima
      // particula)
      
      if (i<0.90*MAX_PARTICLES)
	  { 
	    //printf("%f,%d\n",my_random(),RAND_MAX);
	    dado = my_random()*poblacion1->particles_population[MAX_PARTICLES-1].acumulation;
	    
	    // para evitar un bucle infinito en el caso de que la dado es menor que la primera
	    // acumulacion
	    pos=0;
	    if (dado > poblacion1->particles_population[0].acumulation) 
	      {
		pos = pos_particula(dado);
	      }
	    
	     // el campo acumulation (de una particula) no se sobre escribe, pq es necesario
	    // hasta que se termina el proceso de remuestreo luego sera actualizado cuando
	    // incorporamos de nuevo las observaciones.
	    
	    poblacion2->particles_population[i].point = poblacion1->particles_population[pos].point;
	    poblacion2->particles_population[i].theta = poblacion1->particles_population[pos].theta;
	    poblacion2->particles_population[i].prob = poblacion1->particles_population[pos].prob;
	    //para sumatorio de diferencias
	    //poblacion2->particles_population[i].diff = poblacion1->particles_population[pos].diff;
	  }
      else
	{
	  
	  poblacion2->particles_population[i].point.x = ((float)(ancho*my_random()))+ A.x;
	  poblacion2->particles_population[i].point.y = ((float)(alto*my_random()))+ A.y;
	  poblacion2->particles_population[i].theta = (float) (THETA_MAX)*my_random();
	  
	}
      
  }
  
  //cambio el valor de los punteros para que poblacion1 apunte a la poblacion mas reciente
  aux=poblacion1;
  poblacion1=poblacion2;
  poblacion2=aux;
  
} 

/////////////////////  FIN funciones para el tratamiento de particulas ////////////////////////////////////
//FIN FUNCIONES Y PROCEDIMIENTOS FILTRO DE PARTICULAS

//vacio grid
void vacio_grid(Tgrid *grid, Tmundo mundo)
{
  int k;
  for (k=0;k<grid->size*grid->size;k++)
    {
      (*grid).map[k].estado = VACIO;
    }
}

//relleno de grid con el mapa de mapping 
void relleno_grid_mapping(TipoMemoriaSegmentos * mapa, Tgrid *grid)
{
  int k,i,y_world,x_world,celda,cont;
  Tvoxel p;
  float distancia,tic,j;
  A.x=pose_estimate[0];
  A.y=pose_estimate[1];
  B.x=pose_estimate[0];
  B.y=pose_estimate[1];
  for(i=0;i<MAX_SEGM;i++)	
    {
      
      if((*mapa).validos[i])		
	{
	  distancia=sqrt(pow((*mapa).segm[i].pI.x-(*mapa).segm[i].pF.x,2)+pow((*mapa).segm[i].pI.y-(*mapa).segm[i].pF.y,2));
	  if (distancia>0)
	    {  
	      tic=(laserloc_grid->resolucion/1)/distancia;
	      for(j=0.0;j<=1.0;j=j+tic)
		{  
		  p.x = (*mapa).segm[i].pI.x + j * ( (*mapa).segm[i].pF.x - (*mapa).segm[i].pI.x );
                  p.y = (*mapa).segm[i].pI.y + j * ( (*mapa).segm[i].pF.y - (*mapa).segm[i].pI.y ); 
		  
		  celda = xy2celda_rejilla(p,laserloc_grid);
		  if ((celda>=0)&&(celda<=laserloc_grid->size*laserloc_grid->size))
		    {
		      (*grid).map[celda].estado = OCUPADO; 
		      if((*grid).map[celda].centro.x<=A.x)
			A.x=(*grid).map[celda].centro.x;
		      else if((*grid).map[celda].centro.x>=B.x)
			B.x=(*grid).map[celda].centro.x;
		      if((*grid).map[celda].centro.y<=A.y)
			A.y=(*grid).map[celda].centro.y;
		      else if((*grid).map[celda].centro.y>=B.y)
			B.y=(*grid).map[celda].centro.y;
		    }   
		}
	      
	      celda = xy2celda_rejilla((*mapa).segm[i].pF,laserloc_grid);
	      if ((celda>=0)&&(celda<=laserloc_grid->size*laserloc_grid->size))
		{
		  (*grid).map[celda].estado = OCUPADO; 
		  if((*grid).map[celda].centro.x<=A.x)
		    A.x=(*grid).map[celda].centro.x;
		  else if((*grid).map[celda].centro.x>=B.x)
		    B.x=(*grid).map[celda].centro.x;
		  if((*grid).map[celda].centro.y<=A.y)
		    A.y=(*grid).map[celda].centro.y;
		  else if((*grid).map[celda].centro.y>=B.y)
		    B.y=(*grid).map[celda].centro.y;
		} 
	      
	      
	    }   
	}
    }
  cont=0;
  for (k=0;k<grid->size*grid->size;k++)
    {  if((*grid).map[k].estado == OCUPADO)
	cont++;
    }
	printf("Numero celdillas->%d\n",cont);
	
}
 
void relleno_grid_localization( Tgrid *grid)
{
	int i,celda;
	A=AINICIAL;
	B=BINICIAL;
	for (i=0;i<rejilla_rayo.ocup;i++)
	{
		(*grid).map[rejilla_rayo.celdas_ocupadas[i].num].estado = OCUPADO; 
    }

}

void localization_iteration()
{  

  struct timeval q,w,time;
  static int d=0,d2=0;
  
  float color;
  
  long int tiempo;
  int pos;
  int from,crossovertrials,overlapcount,randomtrials,thermaltrials;;
  int i,j,p1,p2,overlap,insert;
  int cutElitism, cutCrossover, cutRandommutation,cutThermalnoise;
  int veces=0, max_poblacion=0, Similar;  
  float ruido_radiox=0,ruido_radioy=0,ruido_theta=0;
  float nuevo_x,nuevo_y,nueva_theta;		
  float saludDelSimilar,saludDelIgual;
  float rnd;
  float ancho = B.x-A.x;
  float alto = B.y-A.y;
  float modulo,modulo_memoria,prob_max_aux;
  struct timeval a;
  plistapart part_ord;	  
  Tparticle *aux;
  Tparticle c,part;
  plistaExp p,lista;
  static int pasada =0;
  static int primera = 0;

  pthread_mutex_lock (&mymutex);
  if (pasada ==0){
    Tparticle prueba;
    prueba.point.x = 700.80542;
    prueba.point.y = -13000.02832;
    prueba.prob = 0.745022;
    prueba.theta = 319;
    prueba.diff = 0.0;
    prueba.acumulation =0.0;
    prueba.xl=0.0;
    //
    
    InsertarNodo(&explotadores, prueba, &colores);
    pasada++;
    
    encoders_ini[0]= myencoders[0];
    encoders_ini[1]= myencoders[1];
    encoders_ini[2]= myencoders[2];
    encoders_ini[3]= myencoders[3];
    encoders_ini[4]= myencoders[4];
    }

  //plistaExp
  plistaExp lista_aux;
  lista_aux =explotadores;
  float max_fit=0.0;
  pos_max_fit = 0;
  int contador;
  while (lista_aux != NULL){
    if (lista_aux->exp1[MAX_EXPLOTADORES-1].prob > max_fit) {
      max_fit = lista_aux->exp1[MAX_EXPLOTADORES-1].prob;
      pos_max_fit = contador;
    }
    contador++;
    lista_aux = lista_aux->siguiente;
  }
  speedcounter(localization_id);
  /* printf("localization iteration %d\n",d++);*/
  d++;
  //para ver el tiempo por iteracion
  gettimeofday(&q,NULL); 
  if(d==1)
  {
    disp_noise_x=myencoders[0];
       disp_noise_y=myencoders[1];
       disp_noise_theta=myencoders[2]*RADTODEG;
       raw_odom[0]=myencoders[0];
       raw_odom[1]=myencoders[1];
       raw_odom[2]=myencoders[2];
       raw_odom[3]=myencoders[3];
       raw_odom[4]=myencoders[4];
       
  }
  
  if((pow(myencoders[0]-mi_trayectoria.tray[pos_tray].x,2)+pow(myencoders[1]-mi_trayectoria.tray[pos_tray].y,2))>(DIST_TRAY*DIST_TRAY))
    {   
      set_pos(&mi_trayectoria,myencoders[0],myencoders[1]);
      
      dif_noise_x = myencoders[0]-disp_noise_x;
      dif_noise_y = myencoders[1]-disp_noise_y;
      dif_noise_theta = (RADTODEG*myencoders[2])-disp_noise_theta;   
      //incorpora_mov_raw_odom(&ruidox,&ruidoy,&ruidoang,raw_odom,
      //			     pow(dif_noise_x*dif_noise_x+dif_noise_y*dif_noise_y,0.5),dif_noise_theta);	  	
      disp_noise_x = myencoders[0];
      disp_noise_y = myencoders[1];  
      disp_noise_theta = (RADTODEG*myencoders[2]); 
      
      set_pos(&trayectoria_noise,raw_odom[0],raw_odom[1]);
      if(mi_trayectoria.pos==0)
	pos_tray=MAX_POS_TRAY-1;
      else
	pos_tray=mi_trayectoria.pos-1; 
      
    //printf("(%f,%f,%f)real(%f,%f,%f)\n",raw_odom[0],raw_odom[1],raw_odom[2]*RADTODEG,myencoders[0],myencoders[1],myencoders[2]*RADTODEG);	
    }
  //printf("DIRECCION en LOC-> %f,%f\n",raw_odom[0],raw_odom[1]);     
  //printf("DIRECCION en LOC-> %f,%f\n",pose_estimate[0],pose_estimate[1]);  
  if(localization_statealg==amultiobjeto)
    {
      
      if(EXPLORERS_ACTIVE)
	{
	  //printf("Incorporamos\n");
	  d2++;
	  if(d2==1)
	    {
	      pose_estimate[0]=myencoders[0];
	      pose_estimate[1]=myencoders[1];
	      pose_estimate[2]=myencoders[2];
	      pose_estimate[3]=myencoders[3];
	      pose_estimate[4]=myencoders[4];
	      
	      disp_x_mem = myencoders[0];
	      disp_y_mem = myencoders[1];
	      //disp_theta_mem = (RADTODEG*myencoders[2]);
	      
	      pos_inicial_slam.x=pose_estimate[0];			  
	      pos_inicial_slam.y=pose_estimate[1];
	    }
	  
	  if(localization_statemode==slam)
	    {
	      vacio_grid(laserloc_grid, mi_mundo);
	      relleno_grid_mapping(mymapa, laserloc_grid);
	    }
	  
	  //Renovamos la poblacion
	  if(explotadoresReset)
	    {
	      BorrarLista(&explotadores,&colores);
	      explotadoresReset=FALSE;
	    }
	  
	  prob_max_aux=0;

	  for (veces=0;veces<=numElementosLista;veces++) {
	    
	    
			  
	    //compruebo si estoy repitiendo razas y ademas si estas razas son validas
	    //pero solo antes de explotar las razas y si hay más de una raza
	    
	    if (veces==1 && numElementosLista>1)
	      {
		Clean_Similarity(&explotadores,&colores);
	      }
	    if(veces==1)
	      {
		lista=explotadores;
	      }
	    
	    /* new population */
	    insert=0;			//contador para el número de particulas de la nueva población
	    overlapcount=0;
	    from=0; crossovertrials=0;  thermaltrials=0; randomtrials=0;
	    if (veces==0) {
	      cutElitism=(int)(MAX_EXPLORADORES*EXPLORADOR_ELITISM/100.);
	      cutCrossover=(int)(MAX_EXPLORADORES*EXPLORADOR_CROSSOVER/100.);
	      cutRandommutation=(int)(MAX_EXPLORADORES*EXPLORADOR_RANDOMMUTATION/100.);
	      cutThermalnoise=(int)(MAX_EXPLORADORES*EXPLORADOR_THERMALNOISE/100.);
	      max_poblacion = MAX_EXPLORADORES;
	      pexp1=pexploradores;
	      pexp2=pexploradores2;
	    } else {
	      cutElitism=(int)(MAX_EXPLOTADORES*EXPLOTADOR_ELITISM/100.);
	      cutCrossover=(int)(MAX_EXPLOTADORES*EXPLOTADOR_CROSSOVER/100.);
	      cutThermalnoise=(int)(MAX_EXPLOTADORES*EXPLOTADOR_THERMALNOISE/100.);
	      cutRandommutation=(int)(MAX_EXPLOTADORES*EXPLOTADOR_RANDOMMUTATION/100.);
	      max_poblacion = MAX_EXPLOTADORES;
	      pexp1=DevuelveExp1(explotadores,veces,&pos_raza_maxima_anterior);
	      pexp2=DevuelveExp2(explotadores,veces,&pos_raza_maxima_anterior);
	    }
	    
	    //esto hace que se resetee toda la población de exploradores
	    if (exploradoresReset && veces==0) {
	      cutElitism=0;
	      cutCrossover=0;
	      cutThermalnoise=0;
	      exploradoresReset=FALSE;
	    }
	    
	    /* generacion de las particulas candidatas a pertenecer en la nueva poblacion */
	
	    while(insert<max_poblacion) 
	      {   
		//incorporo ruido a la posicion estimada según avanza el robot
		if((insert%(MAX_EXPLOTADORES*4))==0)
		  {
		    //printf("ENtro en el if raro \n");
		    dif_noise_x = myencoders[0]-disp_noise_x;
		    dif_noise_y = myencoders[1]-disp_noise_y;
		    dif_noise_theta = (RADTODEG*myencoders[2])-disp_noise_theta; 
                    modulo_noise=pow(dif_noise_x*dif_noise_x+dif_noise_y*dif_noise_y,0.5);			
		    //incorpora_mov_raw_odom(&ruidox,&ruidoy,&ruidoang,raw_odom,modulo_noise,dif_noise_theta);	  	
		    ruidox=0.0;
		    ruidoy=0.0;
		    ruidoang=0.0;
		    incorpora_mov_estimate_odom(ruidox,ruidoy,ruidoang,pose_estimate,
						modulo_noise,dif_noise_theta);	  		
		    disp_noise_x = myencoders[0];
		    disp_noise_y = myencoders[1];  
		    disp_noise_theta = (RADTODEG*myencoders[2]);
		    //printf("E(%f,%f,%f)real(%f,%f,%f)\n",raw_odom[0],raw_odom[1],raw_odom[2]*RADTODEG,myencoders[0],myencoders[1],myencoders[2]*RADTODEG);
		  }				
		
		if ((insert<cutElitism)&&(from<max_poblacion)) { //printf("entra1\n");
		  // elitism //
		  // mete en la nueva poblacion los seg3dv con buena salud
		  c.point.x=pexp1[max_poblacion-1-from].point.x;	//la variable c es el candidato de tipo seg3d
		  c.point.y=pexp1[max_poblacion-1-from].point.y;
                  c.theta=pexp1[max_poblacion-1-from].theta;
		  //c.prob=pexp1[max_poblacion-1-from].prob;	
		  from++;
		}
		else if ((insert>=cutElitism)&&(insert<(cutElitism+cutCrossover))&&(crossovertrials<max_poblacion)) {
		  // crossover 
		  // Selección aleatoria de los padres 
		  //printf("entra2\n");	
		  p1 = (int) number_generationbv_s(max_poblacion-1-cutCrossover,max_poblacion-1);
		  p2 = (int) number_generationbv_s(max_poblacion-1-cutCrossover,max_poblacion-1);		
		  // Seleccion aleatoria del punto intermedio entre ambos padres
		  c.point.x=((pexp1[p1].point.x)+(pexp1[p2].point.x))/2;
		  c.point.y=((pexp1[p1].point.y)+(pexp1[p2].point.y))/2;
                  
		  //selecciono el angulo de la mejor practicula
		  if (pexp1[p1].prob >= pexp1[p2].prob) {
		    c.theta=pexp1[p1].theta;
		  } else {
		    c.theta=pexp1[p2].theta;
		  }
		  c.prob=0.01;
		  crossovertrials++;
			   }
		else if ((insert>=(cutElitism+cutCrossover))&&
			 (insert<(cutElitism+cutCrossover+cutThermalnoise))&&(thermaltrials<max_poblacion)) {
		  // thermal noise  
		  
		  
		  p1 = (int) number_generationbv_s(0,max_poblacion-1); 
		  
		  ruido_radiox = number_generationbv_s(-SIGMA_THERMALNOISE,SIGMA_THERMALNOISE);
                  ruido_radioy = number_generationbv_s(-SIGMA_THERMALNOISE,SIGMA_THERMALNOISE);
                  ruido_theta = number_generationbv_s(-SIGMA_ANGLE_THERMALNOISE,SIGMA_ANGLE_THERMALNOISE);
                  /*ruido_radiox = gauss_rndm(SIGMA_RADIO,0.);
		    ruido_radioy = gauss_rndm(SIGMA_RADIO,0.);
		    ruido_theta = gauss_rndm(SIGMA_THETA,0.);
		  */
                  c.point.x = pexp1[p1].point.x + (ruido_radiox);
                  c.point.y= pexp1[p1].point.y + (ruido_radioy);
		  
                  c.theta = (float) ((int) (pexp1[p1].theta+ruido_theta) % THETA_MAX); 
		  
		  
		  thermaltrials++;
		} else{
		  
		  //printf("entra4\n"); 
		  c.point.x = number_generationbv_s(A.x,B.x);
		  c.point.y = number_generationbv_s(A.y,B.y);
		  c.theta= number_generationbv_s(0.,360.); 
		  //c.prob=0.1;
		  randomtrials++;
		  
		}
		
		/* repulsion */
		overlap=-1;
		if(veces==0)
		  for(j=0;j<insert;j++)
		    {  if(((c.point.x-pexp2[j].point.x)*(c.point.x-pexp2[j].point.x)+
			   (c.point.y-pexp2[j].point.y)*(c.point.y-pexp2[j].point.y))<
			  DISTANCIA_REPULSION*DISTANCIA_REPULSION)
			{	  
			  overlap=j;
			  break;
			}
		    }
		
                if((c.point.x>A.x) && (c.point.x < B.x) && (c.point.y>A.y) && (c.point.y<B.y))
		  {   
		    if ((overlap==-1)||(repulsionOff)) {
		      /* Si no esta cerca de ninguna mosca existente */
		      /* Se inserta en la nueva poblacion *///printf("1\n");
		      pexp2[insert].point.x=c.point.x;
		      pexp2[insert].point.y=c.point.y;
		      pexp2[insert].theta=c.theta;
		      pexp2[insert].prob=c.prob;
		      insert++;
		    } else if (c.prob > pexp1[overlap].prob) {
		      /* Si esta cerca de alguna mosca existente y es mejor mosca*/
		      /* Se reemplaza con esa mosca */

		      /* replacement *///printf("2\n");
		      pexp2[insert].point.x=c.point.x;
		      pexp2[insert].point.x=c.point.x;
		      pexp2[insert].theta=c.theta;
		      pexp2[insert].prob=c.prob;
		    }
		    else; /* try again */
		  }
		else; /* try again */
	      }
	    
	    if(localization_statedis==correlation)
	      {	   
		reset_correlacion();  
		for (i=0;i<NUM_LASER;i=i+RESOLUCION_LASER_TEORICO)  
		  {            
		    laser_correlacion[i]=mylaser[i];
		    correlacion.x+=mylaser[i];
		    correlacion.x2+=mylaser[i]*mylaser[i];		
		  }
		correlacion.xto2=correlacion.x*correlacion.x;
	      }
	    
	    //Incorporamos el movimiento del robot y calculamos la probabilidad
	    if(veces==0)
			{
			  incorpora_obs_exploradores(pexp2,mi_mundo,&colores);       	
			}
	    else 
	      {  
		
		dif_x_expt = myencoders[0]-lista->disp_x;
		dif_y_expt = myencoders[1]-lista->disp_y;
		dif_theta_expt = (RADTODEG*myencoders[2])-lista->disp_theta;
		modulo=pow(dif_x_expt*dif_x_expt+dif_y_expt*dif_y_expt,0.5);				
		if(modulo>HIPER_ESPACIO)
               {   
		 modulo=0.;
		 dif_theta_expt=0.;
	       }
		
		incorpora_mov_explotadores(pexp2,modulo,dif_theta_expt);		
		
		lista->disp_x = myencoders[0];
		lista->disp_y = myencoders[1];
		lista->disp_theta=(RADTODEG*myencoders[2]);
		
		if (pos_max_fit == veces -1 )
		  incorpora_obs_explotadores(pexp2,mi_mundo,1);				
		else incorpora_obs_explotadores(pexp2,mi_mundo,0);				
	       
	       lista=lista->siguiente;	
	      }
			
	    //ordeno por probabilidad (salud de los exploradores)
	    part_ord=ordenadas;
	    for(i=0;i<max_poblacion;i++)
	      {
		if(part_ord==NULL) break;
		pexp2[i]=part_ord->p;
		part_ord=part_ord->siguiente;
		//printf("(%f,%f,%f)->(%f)\n",pexp2[i].point.x,pexp2[i].point.y,pexp2[i].theta,pexp2[i].prob);
	      }
	    //printf("\n");
	    BorrarListaPartOrdenadas(&ordenadas);
	    ordenadas=NULL;
	    
	    if(veces>0)
	      {
		
		dif_noise_x = myencoders[0]-disp_noise_x;
		dif_noise_y = myencoders[1]-disp_noise_y;
		dif_noise_theta = (RADTODEG*myencoders[2])-disp_noise_theta; 
		modulo_noise=pow(dif_noise_x*dif_noise_x+dif_noise_y*dif_noise_y,0.5);			
		//incorpora_mov_raw_odom(&ruidox,&ruidoy,&ruidoang,raw_odom,modulo_noise,dif_noise_theta);	  	
		ruidox=0.0;ruidoy=0.0,ruidoang=0.0;
		incorpora_mov_estimate_odom(ruidox,ruidoy,ruidoang,pose_estimate,modulo_noise,dif_noise_theta);	  		
		disp_noise_x = myencoders[0];
		disp_noise_y = myencoders[1];  
		disp_noise_theta = (RADTODEG*myencoders[2]);
		//printf("E(%f,%f,%f)real(%f,%f,%f)\n",raw_odom[0],raw_odom[1],raw_odom[2]*RADTODEG,myencoders[0],myencoders[1],myencoders[2]*RADTODEG);
		
		//actualizo valor de prob_max
		if(pexp2[MAX_EXPLOTADORES-1].prob>prob_max_aux)
		  {
		    prob_max_aux=pexp2[MAX_EXPLOTADORES-1].prob;
		    
		    pose_aux[0]=pexp2[MAX_EXPLOTADORES-1].point.x;
                    pose_aux[1]=pexp2[MAX_EXPLOTADORES-1].point.y;
                    pose_aux[2]=pexp2[MAX_EXPLOTADORES-1].theta*DEGTORAD;
		    
                    pos_raza_maxima=veces;					
		  }
		if(veces==numElementosLista)
		  {
		    prob_max=prob_max_aux;
		    if((pow(pos_inicial_slam.x-myencoders[0],2)+pow(pos_inicial_slam.y-myencoders[1],2))>(START_SLAM*START_SLAM))
		      guarderia=FALSE;	
		    
		    if(((localization_statemode==slam)&&(!guarderia))||(localization_statemode==loc))
		      {
			pose_estimate[0]=pose_aux[0];
			pose_estimate[1]=pose_aux[1];
			pose_estimate[2]=pose_aux[2];
			pose_estimate[3]=cos(pose_estimate[2]);
			pose_estimate[4]=sin(pose_estimate[2]);
		      }						
		    if(pos_raza_maxima!=-1)
		      {
			dif_x_mem = myencoders[0]-disp_x_mem;
			dif_y_mem = myencoders[1]-disp_y_mem;
                        //dif_theta_mem = (RADTODEG*myencoders[2])-disp_theta_mem;		
			modulo_memoria=pow(dif_x_mem*dif_x_mem+dif_y_mem*dif_y_mem,0.5);
			if(modulo_memoria!=0)
			  {
			    
			    if(pos_raza_maxima==pos_raza_maxima_anterior)
			      {   
				AumentaMemoria(explotadores,pos_raza_maxima,&encontrada_pos_real);
				disp_x_mem = myencoders[0];
				disp_y_mem = myencoders[1];
				//disp_theta_mem = (RADTODEG*myencoders[2]);							
			      }
			    else
			      {
				if(pos_raza_maxima_anterior==-1)
				  { 
				    AumentaMemoria(explotadores,pos_raza_maxima,&encontrada_pos_real);
				    disp_x_mem = myencoders[0];
				    disp_y_mem = myencoders[1];
				    //disp_theta_mem = (RADTODEG*myencoders[2]);							   
				  }
				else
				  { 							   
				    BorraMemoria(explotadores,pos_raza_maxima_anterior);
				    AumentaMemoria(explotadores,pos_raza_maxima,&encontrada_pos_real);
				    disp_x_mem = myencoders[0];
				    disp_y_mem = myencoders[1];
				    //disp_theta_mem = (RADTODEG*myencoders[2]);							   
				  }
			      }
			    
			  }
		      }
		    
		    
		  }
	      }
	    
	    /* double buffer swap */
	    aux=pexp1;
	    pexp1=pexp2; /* hopefully this will be atomic */
	    pexp2=aux;
            
	    
	    
	    //Actualizo el puntero de los exploradores que se visualizan
	    if (veces==0) {
	      pexploradores=pexp1;
	      pexploradores2=pexp2;
	      
	      //inicializo pos raza máxima
	      pos_raza_maxima=-1;
	      pos_raza_maxima_anterior=-1;				  
	      
	    }else {
	      
	      ActualizaExp1(explotadores,veces,pexp1);
	      ActualizaExp2(explotadores,veces,pexp2);
	      
	    } 
	    //printf("Numero de razas-> %d\n",numElementosLista);
	  }
	  /*if (numElementosLista > 0){
	    plistaExp lista_mierda=explotadores;
	    printf("Si entro numElementosLista a%d \n",numElementosLista);
	    float salud=0.0; int pos=0;
	    for(i=0;i<numElementosLista;i++){
	      
	      for(j=0;j<MAX_EXPLOTADORES;j++)
		if (lista_mierda->exp1[j].prob>salud){
		  pos=j;
		  salud=lista_mierda->exp1[j].prob;
		}
	      
	      lista_mierda = lista_mierda->siguiente;
	    }
	    printf("pod %d salud maxima %f \n",pos,salud);
	    }*/

	  
		  
	}
      else
	{	  
	  
	  
	  Tvoxel mi_puntoahora;
	  float my_thetaahora;
	  //for (i=0;i<NUM_LASER;i=i+RESOLUCION_LASER_TEORICO)  
	  for (i=0;i<NUM_LASER;i++)  
	    {            
	      
	      laser_real[i]=mylaser[i]; 
	      
	    }  
	  
	  mi_puntoahora.x=myencoders[0];
	  mi_puntoahora.y=myencoders[1];
	  my_thetaahora=myencoders[2]*RADTODEG;
	  
        if(localization_statemode==loc)		  
	  obtener_laser_teorico(mi_puntoahora,&my_thetaahora,&my_laser,mi_mundo); 
        else
	  obtener_laser_teorico2(mi_puntoahora,&my_thetaahora,&my_laser,mi_mundo); 	
	
      }
    }	  
  else 
    {
      //printf("Estamos en particulas\n");
      if (PARTICLES_ACTIVE){ 
	
	
	// actualizamos la variable laser_real_aux que contiene el valor del ultimo
	// laser real usado para rellenar la rejilla de probabilidad     
	
	//gettimeofday(&q,NULL);     
	
	if(localization_statedis==correlation)
	  {	   
	    reset_correlacion();  
	    for (i=0;i<NUM_LASER;i=i+RESOLUCION_LASER_TEORICO)  
	      {            
		laser_correlacion[i]=mylaser[i];
		correlacion.x+=mylaser[i];
		correlacion.x2+=mylaser[i]*mylaser[i];		
	      }
	    correlacion.xto2=correlacion.x*correlacion.x;
		}		   
        dif_x = myencoders[0]-disp_x;
	dif_y = myencoders[1]-disp_y;
	//dif_theta = ((int)((RADTODEG*myencoders[2])+360)% 360)-disp_theta;
        dif_theta = (RADTODEG*myencoders[2])-disp_theta;		
	incorpora_mov_particulas(dif_x,dif_y,dif_theta);		
	disp_x = myencoders[0];
	disp_y = myencoders[1];
		//disp_theta = ((int)((RADTODEG*myencoders[2])+360)% 360);
	disp_theta = (RADTODEG*myencoders[2]);
        incorpora_obs_particulas(mi_mundo);		
	
	remuestreo_particulas();
	
	//gettimeofday(&w,NULL);  
	//tiempo = (w.tv_sec-q.tv_sec)*1000000+w.tv_usec-q.tv_usec;
	//printf("LOCALIZACION ha tardado : %ld sec\n",tiempo/1000000);
	//printf("%d\n%d\n\n",q.tv_sec,w.tv_sec);
	
	
      }
      else
	  { 
	    
	    Tvoxel mi_puntoahora;
	    float my_thetaahora;
	    //for (i=0;i<NUM_LASER;i=i+RESOLUCION_LASER_TEORICO)  
	    for (i=0;i<NUM_LASER;i++)  
	      {            
		
		laser_real[i]=mylaser[i]; 
		
	      }  
	    
	    mi_puntoahora.x=myencoders[0];
	    mi_puntoahora.y=myencoders[1];
	    my_thetaahora=myencoders[2]*RADTODEG;
	    
	    float prob;		  
	    obtener_laser_teorico(mi_puntoahora,&my_thetaahora,&my_laser,mi_mundo); 
	    prob = distance_manhatan(my_laser,mylaser);
	    //printf("prob->%f\n",prob);  
	    /**/
	    
	    
		
	  } 
  }
  gettimeofday(&w,NULL);
  tiempo = (w.tv_sec-q.tv_sec)*1000000+w.tv_usec-q.tv_usec;
  //printf("Posicion x %.2f  , y %.2f thehta %.2f \n",myencoders[0],myencoders[1],myencoders[2]*RADTODEG);
  /*  //printf("%d.%d\n%d.%d\n\n",q.tv_sec,q.tv_usec,w.tv_sec,w.tv_usec);
  if(EXPLORERS_ACTIVE){
    lista=explotadores;
    float fit_max =0.0;
    Tparticle good_part;
    int entro=0;
    while (lista !=NULL){
      entro=1;
      if (lista->exp1[MAX_EXPLOTADORES-1].prob > fit_max){
	fit_max= lista->exp1[MAX_EXPLOTADORES-1].prob;
	good_part.point.x = lista->exp1[MAX_EXPLOTADORES-1].point.x;
	good_part.point.y = lista->exp1[MAX_EXPLOTADORES-1].point.y;
	good_part.theta = lista->exp1[MAX_EXPLOTADORES-1].theta;      
	
      }
      lista = lista->siguiente;
    }
    
    
    
    gettimeofday(&time, NULL);
    now_iteration = time.tv_sec * 1000000 + time.tv_usec;  
    float total_time;
    //printf("Diferencia %f \n",(now_iteration-last_iteration)/ 1000000.0);
    if (abs((now_iteration-last_iteration)/ 1000000.0)> 0.99)
      {
	total_time = (now_iteration-inicio_tiempo)/ 1000000.0;
	last_iteration= abs(now_iteration);
	
	
      }
    else entro =0;
    
    
    if (entro ==1 ){
      
      
      if (primera==0)
	{ 
	  primera++;
	  inicio_tiempo = now_iteration;
	}
      else 
	actual_tiempo = now_iteration;
      float aux=0.0,aux_theta;
      aux = sqrt(((myencoders[0]-good_part.point.x)*(myencoders[0]-good_part.point.x)) +
		 ((myencoders[1]-good_part.point.y)*(myencoders[1]-good_part.point.y)));
      aux_theta = (myencoders[2]*RADTODEG) - good_part.theta;
      if(aux_theta>=360)
	aux_theta-=360;
      else if(aux_theta<0)
	aux_theta+=360;    
      if (aux_theta > 180)
	aux_theta=360.0 - aux_theta;
    //printf("Dif 1 %f \n",myencoders[0]-part.point.x);
      //printf("%.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f  %.2f\n",myencoders[0],myencoders[1],myencoders[2]*RADTODEG,good_part.point.x,good_part.point.y,good_part.theta,aux,aux_theta,total_time);
      printf("%.2f %.2f %.2f\n",aux/10.0,aux_theta,total_time);
    }
    }*/
  pthread_mutex_unlock (&mymutex);
}


void localization_stop()
{
  pthread_mutex_lock(&(all[localization_id].mymutex));
  put_state(localization_id,slept);
  all[localization_id].children[(*(int *)myimport("laser","id"))]=FALSE;
  all[localization_id].children[(*(int *)myimport("motors","id"))]=FALSE;
  all[localization_id].children[(*(int *)myimport("encoders","id"))]=FALSE;	
  lasersuspend();
  encoderssuspend();
  motorssuspend();
  printf("localization: off\n");
  pthread_mutex_unlock(&(all[localization_id].mymutex));
}

void localization_terminate()
{

}


void localization_run(int father, int *brothers, arbitration fn)
{
  int i;
  float pr;
  int cont;
  //inicializar los niveles de gris
  pr=1;
  for (cont=0;cont<max_gris;cont++){
    
    niveles_gris[cont]=FL_FREE_COL1+cont;
    fl_mapcolor(niveles_gris[cont],(int)255*pr,(int)255*pr,(int)255*pr);
    pr-=(1/max_gris);
  }
  printf("Inicializo los niveles de gris\n");
  // cambia el suelo de blanco a al valor RGB (250,223,151)
  fl_mapcolor(mi_suelo,250,223,151);
	
  pthread_mutex_lock(&(all[localization_id].mymutex));
  /* this schema resumes its execution with no children at all */
  for(i=0;i<MAX_SCHEMAS;i++) all[localization_id].children[i]=FALSE;
  
  all[localization_id].father=father;
  if (brothers!=NULL)
    {
      for(i=0;i<MAX_SCHEMAS;i++) localization_brothers[i]=-1;
      i=0;
      while(brothers[i]!=-1) {localization_brothers[i]=brothers[i];i++;}
    }
  localization_callforarbitration=fn;
  put_state(localization_id,notready);

  mymapa=(TipoMemoriaSegmentos *)myimport("mapping","memoria_segmentos");	
	
  mylaser=(int *)myimport("laser","laser");
  laserresume=(runFn)myimport("laser","run");
  lasersuspend=(stopFn *)myimport("laser","stop");

  myencoders=(float *)myimport("encoders","jde_robot");
  encodersresume=(runFn)myimport("encoders","run");
  encoderssuspend=(stopFn)myimport("encoders","stop");
  printf("********* Myencoders de 0 %f ********************\n",myencoders[0]);
  myv=(float *)myimport("motors","v");
  myw=(float *)myimport("motors","w");
  motorsresume=(runFn)myimport("motors","run");
  motorssuspend=(stopFn)myimport("motors","stop");

  printf("localization: on\n");
  pthread_cond_signal(&(all[localization_id].condition));
  pthread_mutex_unlock(&(all[localization_id].mymutex));
}

void *localization_thread(void *not_used) 
{
  struct timeval a,b;
  long diff, next;

  for(;;)
    {
      pthread_mutex_lock(&(all[localization_id].mymutex));

      if (all[localization_id].state==slept) 
	{
//esto	  *myv=0; *myw=0;
	  pthread_cond_wait(&(all[localization_id].condition),&(all[localization_id].mymutex));
	  pthread_mutex_unlock(&(all[localization_id].mymutex));
	}
      else 
	{
	  /* check preconditions. For now, preconditions are always satisfied*/
	  if (all[localization_id].state==notready) put_state(localization_id,ready);
	  else if (all[localization_id].state==ready)	  
	    /* check brothers and arbitrate. For now this is the only winner */
	    { 
	      put_state(localization_id,winner);
	      //esto	      *myv=0; *myw=0; 
	      v_teleop=0; w_teleop=0;
	      /* start the winner state from controlled motor values */ 
	      all[localization_id].children[(*(int *)myimport("laser","id"))]=TRUE;
	      all[localization_id].children[(*(int *)myimport("motors","id"))]=TRUE;
	      all[localization_id].children[(*(int *)myimport("encoders","id"))]=TRUE;
	      laserresume(localization_id,NULL,NULL);
	      encodersresume(localization_id,NULL,NULL);
	      motorsresume(localization_id,NULL,NULL);
	    }	  
	  else if (all[localization_id].state==winner);

	  if (all[localization_id].state==winner)
	    /* I'm the winner and must execute my iteration */
	    {
	      pthread_mutex_unlock(&(all[localization_id].mymutex));
	      gettimeofday(&a,NULL);
	      localization_iteration();
	      gettimeofday(&b,NULL);  

	      diff = (b.tv_sec-a.tv_sec)*1000000+b.tv_usec-a.tv_usec;
	      next = localization_cycle*1000-diff-10000; 
	      if (next>0) 
		/* discounts 10ms taken by calling usleep itself */
		usleep(localization_cycle*1000-diff);
	      else 
		/* just let this iteration go away. overhead time negligible */
		{//printf("time interval violated: localization\n"); 
		usleep(localization_cycle*1000);
		}
	    }
	  else 
	    {
	      pthread_mutex_unlock(&(all[localization_id].mymutex));
	      usleep(localization_cycle*1000);
	    }
	}
    }
}

void localization_init(char *file)
{
  pthread_mutex_lock(&(all[localization_id].mymutex));
  myexport("localization","encontrada_pos_real",&encontrada_pos_real);	
  myexport("localization","pose_estimate",&pose_estimate);	
  myexport("localization","raw_odom",&raw_odom);	
  myexport("localization","id",&localization_id);
  myexport("localization","run",(void *) &localization_run);
  myexport("localization","stop",(void *) &localization_stop);
  printf("localization schema started up\n");
  put_state(localization_id,slept);
  pthread_create(&(all[localization_id].mythread),NULL,localization_thread,NULL);
  if (myregister_buttonscallback==NULL){
    if ((myregister_buttonscallback=(registerbuttons)myimport ("graphics_xforms", "register_buttonscallback"))==NULL){
      printf ("I can't fetch register_buttonscallback from graphics_xforms\n");
      jdeshutdown(1);
    }
    if ((mydelete_buttonscallback=(deletebuttons)myimport ("graphics_xforms", "delete_buttonscallback"))==NULL){
      printf ("I can't fetch delete_buttonscallback from graphics_xforms\n");
      jdeshutdown(1);
    }
    if ((myregister_displaycallback=(registerdisplay)myimport ("graphics_xforms", "register_displaycallback"))==NULL){
      printf ("I can't fetch register_displaycallback from graphics_xforms\n");
      jdeshutdown(1);
    }
    if ((mydelete_displaycallback=(deletedisplay)myimport ("graphics_xforms", "delete_displaycallback"))==NULL){
      jdeshutdown(1);
      printf ("I can't fetch delete_displaycallback from graphics_xforms\n");
    }
  }
  
  if ((myscreen=(int *)myimport("graphics_xforms", "screen"))==NULL){
    fprintf (stderr, "navigation: I can't fetch screen from graphics_xforms\n");
    jdeshutdown(1);
  }
  if ((display=(Display *)myimport("graphics_xforms", "display"))==NULL){
    fprintf (stderr, "navigation: I can't fetch display from graphics_xforms\n");
    jdeshutdown(1);
  }
  pthread_mutex_unlock(&(all[localization_id].mymutex));

  localization_statedis=sumatorio;
  localization_stateprob=potencial;	
  localization_statealg=amultiobjeto;
  localization_statemode=loc;	
  //Variables filtro particulas	
  int cont,i;
  float ancho,alto;	
  //Fin variables filtro particulas	
	
  //para dep2
//  myencoders[0]-=4270;
//    myencoders[0]-=2135;
  
  //LECTURA MUNDO
  Tvoxel centrogrid;
  Tvoxel centro_occup_grid;

  char *configfile="./occup_grid.conf"; 
  
  printf(" esquema arrancado .\n"); 
  //LEEMOS MUNDO STAGE    
  printf(" stage ===> \n");
  mi_mundo=procesa_mundo_stage(configfile,&ancho,&alto); 
  //centrogrid.y=(mi_mundo.dimension/2);
  //centrogrid.x=(mi_mundo.dimension/2); 
  //centrogrid.y=3090.0;
  //centrogrid.x=4500.0;
  centrogrid.y=0.0;
  centrogrid.x=0.0; 
  x_rob=mi_mundo.x_robot;
  y_rob=mi_mundo.y_robot;
  printf("Pos del robot %f %f \n",x_rob,y_rob);
  printf("configuration for gradient_planning in %s\n",configfile);
  
  laserloc_grid = inicia_grid(centrogrid,configfile);
  //centro_occup_grid.x = mi_mundo.dimension/2;
  //centro_occup_grid.y = mi_mundo.dimension/2;
  printf("\ncentro rejilla => %f : %f \n",(*laserloc_grid).centro.x,(*laserloc_grid).centro.y);
  
  printf(" inicializado %d \n",(*laserloc_grid).size);  
  relleno_grid(mi_mundo, laserloc_grid);
  printf("localization termino de leer el mapa aaa\n");
  //LECTURA MUNDO
  //FILTRO DE PARTICULAS
  dim_mundo = mi_mundo.dimension;
 
  // Los punots A y B representan las esquina inferior izquierda y superior
  // derecha de "la ventana de particulas" de esta manera podemos especificar
  // la porción del mundo que queremos que cubran las particulas.
  printf("%f %f\n",ancho,alto);
  if(alto==ancho)
  { 
    A.x =-mi_mundo.dimension/2; //esquina inf izq
    A.y =-mi_mundo.dimension/2;
  
    B.x =mi_mundo.dimension/2;  //esquina sup drcha
    B.y =mi_mundo.dimension/2;
  
  }
  else if(ancho>alto)
  {  
     A.x =-ancho/2; //esquina inf izq
     A.y =-ancho/2;
  
     B.x =ancho/2;  //esquina sup drcha
     B.y =alto-ancho/2;

  }
  else
  {  
	 A.x =-alto/2; //esquina inf izq
     A.y =-alto/2;
  
     B.x =ancho-alto/2;  //esquina sup drcha
     B.y =alto/2; 
  }
  AINICIAL=A;
  BINICIAL=B;  
  
  guarderia=TRUE;
  
  //inicializamos punteros de poblacion para realizar el double-buffering
  poblacion1=&particles_population;
  poblacion2=&particles_population_aux;
  
  pexploradores=exploradores;
  pexploradores2=exploradores2;
  pexp1=exploradores;
  pexp2=exploradores2;
  
  init_particles_population(A,B); // inicializa la población de particulas	
  init_explorer_population(A,B); // inicializa la población de exploradores
  
  inicializoColoresExplotadores(&colores);

  /*disp_x=myencoders[0];        
    disp_y=myencoders[1];
    disp_theta=(int)((RADTODEG*myencoders[2])+360)% 360;*/
  
  
	
  //FIN FILTRO DE PARTICULAS
  //INSTRUCCIONES INICIALES DE GUIDISPLAY
 
  // grid setup 
  laserloc_grid_displaysetup();
  //variable que se usa para el pintado o borrado del mapa 	   
  borrar=TRUE;
  borrar_particles=TRUE;
  //inicializar el eje de angulos
  for (cont=0;cont<NUM_LASER;cont++){
    angulos[cont]=(float)cont;
  }
  //inicializar eje distancias
  for (cont=0;cont<360;cont++){
    num_angulos[cont]=(float)cont;
 
  }
  for (cont=0;cont<MAX_PARTICLES;cont++){
    num_particles[cont]=(float)cont;  
  }
  
  //Multiobjeto
  //inicializao la posicion en la que insertar y el color de la trayectoria
  mi_trayectoria.pos=0;
  mi_trayectoria.color=FL_BLUE;  
  trayectoria_noise.pos=0;
  trayectoria_noise.color=FL_RED; 
  /*Creo mi propia raza*/  

}

void localization_guibuttons(void *obj1)
{
  double delta, deltapos;
  FL_OBJECT *obj=(FL_OBJECT *)obj1;
 
  if (obj == fd_localizationgui->exit) jdeshutdown(0);
  else if (obj == fd_localizationgui->escala)  
    {  localization_rango=fl_get_slider_value(fd_localizationgui->escala);
      localization_visual_refresh = TRUE; /* activa el flag que limpia el fondo de la pantalla y repinta todo */ 
      localization_escala = localization_width /localization_rango;}
  else if (obj == fd_localizationgui->track_robot) 
    {if (fl_get_button(obj)==PUSHED) localization_trackrobot=TRUE;
      else localization_trackrobot=FALSE;
    } 
  else if (obj== fd_localizationgui->center)
    /* Se mueve 10%un porcentaje del rango */
    {
      localization_odometrico[0]+=localization_rango*(fl_get_positioner_xvalue(fd_localizationgui->center)-0.5)*(-2.)*(0.1);
      localization_odometrico[1]+=localization_rango*(fl_get_positioner_yvalue(fd_localizationgui->center)-0.5)*(-2.)*(0.1);
      fl_set_positioner_xvalue(fd_localizationgui->center,0.5);
      fl_set_positioner_yvalue(fd_localizationgui->center,0.5);
      localization_visual_refresh=TRUE;  }
  else if (obj==fd_localizationgui->incorpora_part)
  {
    PARTICLES_ACTIVE = (fl_get_button(fd_localizationgui->incorpora_part)==PUSHED);	
  }
  else if (obj==fd_localizationgui->incorpora_exp)
  {
    EXPLORERS_ACTIVE = (fl_get_button(fd_localizationgui->incorpora_exp)==PUSHED);	
  }
  else if (obj==fd_localizationgui->solo_max_prob)
  {
    SOLO_MAX_PROB_ACTIVE = (fl_get_button(fd_localizationgui->solo_max_prob)==PUSHED);	
  }
  else if (obj == fd_localizationgui->vislocalizationlaser)
    {
      if (fl_get_button(fd_localizationgui->vislocalizationlaser)==RELEASED)
	   {localization_display_state = localization_display_state & ~DISPLAY_MY_LASER;
	    visual_delete_my_laser=TRUE;
	   }
      else 
	    localization_display_state=localization_display_state | DISPLAY_MY_LASER;

    }
  else if (obj == fd_localizationgui->vislocalizationlaserteorico)
    {
      if (fl_get_button(fd_localizationgui->vislocalizationlaserteorico)==RELEASED)
	   {localization_display_state = localization_display_state & ~DISPLAY_LOCALIZATION_LASER_TEORICO;
	    visual_delete_localization_laser_teorico=TRUE;
	   }
      else 
	    localization_display_state=localization_display_state | DISPLAY_LOCALIZATION_LASER_TEORICO;

    }	
   else if (obj == fd_localizationgui->vismapamundo)
    {
      if (fl_get_button(fd_localizationgui->vismapamundo)==RELEASED)
	   {//localization_display_state = localization_display_state & ~DISPLAY_MAPA_MUNDO;
	    //visual_delete_localization_mapa_mundo=TRUE;
		borrar=TRUE;   
	   }
     // else 
	    //localization_display_state=localization_display_state | DISPLAY_MAPA_MUNDO;
	    

    }

	else if (obj == fd_localizationgui->dibuja_particulas)
    {
      if (fl_get_button(fd_localizationgui->dibuja_particulas)==RELEASED)
	   {
		   borrar_particles=TRUE;
	   }
       

    }
	else if (obj == fd_localizationgui->simple)
    {
      if (fl_get_button(fd_localizationgui->simple)==PUSHED) localization_statedis=simple;
    }
	else if (obj == fd_localizationgui->sumatorio)
    {
      if (fl_get_button(fd_localizationgui->sumatorio)==PUSHED) localization_statedis=sumatorio;
    }
	else if (obj == fd_localizationgui->correlation)
    {
      if (fl_get_button(fd_localizationgui->correlation)==PUSHED) localization_statedis=correlation;
    }
	else if (obj == fd_localizationgui->potencial)
    {
      if (fl_get_button(fd_localizationgui->potencial)==PUSHED) localization_stateprob=potencial;
    }
	else if (obj == fd_localizationgui->exponencial)
    {
      if (fl_get_button(fd_localizationgui->exponencial)==PUSHED) localization_stateprob=exponencial;
    }
	else if (obj == fd_localizationgui->ssimplep)  
    {  
		POTENCIAS=fl_get_slider_value(fd_localizationgui->ssimplep);
    }
	else if (obj == fd_localizationgui->ssumatoriop)  
    {  
		POTENCIAD=fl_get_slider_value(fd_localizationgui->ssumatoriop);
    }
	else if (obj == fd_localizationgui->scorrelacionp)  
    {  
		POTENCIAC=fl_get_slider_value(fd_localizationgui->scorrelacionp);
    }
	else if (obj == fd_localizationgui->ssimplee)  
    {  
		EXPONENTES=fl_get_slider_value(fd_localizationgui->ssimplee);
    }
	else if (obj == fd_localizationgui->ssumatorioe)  
    {  
		EXPONENTED=fl_get_slider_value(fd_localizationgui->ssumatorioe);
    }
	else if (obj == fd_localizationgui->scorrelacione)  
    {  
		EXPONENTEC=fl_get_slider_value(fd_localizationgui->scorrelacione);
    }
	else if (obj == fd_localizationgui->vis_explotadores)  
    {  
		VISUALIZADOR_EXPLOTADORES=fl_get_slider_value(fd_localizationgui->vis_explotadores);
    }
	else if (obj == fd_localizationgui->screar_raza)  
    {  
		UMBRAL_EXPLOTADORES=fl_get_slider_value(fd_localizationgui->screar_raza);
    }
	else if (obj == fd_localizationgui->sperder_raza)  
    {  
		UMBRAL_PERDIDA=fl_get_slider_value(fd_localizationgui->sperder_raza);
    }
	else if (obj == fd_localizationgui->snoise_x)  
    {  
		SIGMA_RADIO_NOISE=fl_get_slider_value(fd_localizationgui->snoise_x);
    }
	else if (obj == fd_localizationgui->snoise_ang)  
    {  
		SIGMA_THETA_NOISE=fl_get_slider_value(fd_localizationgui->snoise_ang);
    }
	else if (obj == fd_localizationgui->aparticulas)  
    {  
		if(fl_get_button(fd_localizationgui->aparticulas)==PUSHED)  
		  localization_statealg=aparticulas;
    }
	else if (obj == fd_localizationgui->amultiobjeto)  
    {  
	
      if(fl_get_button(fd_localizationgui->amultiobjeto)==PUSHED)  localization_statealg=amultiobjeto;
    }
	else if (obj == fd_localizationgui->borrar_explotadores)  
	  {  
	    explotadoresReset=1;
	    
	    
	  }
	else if (obj == fd_localizationgui->reiniciar_exploradores)  
	  {  
	    exploradoresReset=1;
	    
	  }
	else if (obj == fd_localizationgui->repulsion)		
	  {  
	    
	    repulsionOff=!(fl_get_button(fd_localizationgui->repulsion)==PUSHED);
	  }
	else if (obj == fd_localizationgui->reiniciar_particulas)  
	  {  
	    init_particles_population(A,B);
	    
	  }
	else if (obj == fd_localizationgui->resetreal)  
	  {  
	    reiniciar_trayectoria(&mi_trayectoria);

		
	}
	else if (obj == fd_localizationgui->resetruidoso)  
    {  
      reiniciar_trayectoria(&trayectoria_noise);
      raw_odom[0]=myencoders[0];
      raw_odom[1]=myencoders[1];
      raw_odom[2]=myencoders[2];
      raw_odom[3]=myencoders[3];
      raw_odom[4]=myencoders[4];
      disp_noise_x=myencoders[0];
      disp_noise_y=myencoders[1];
      disp_noise_theta=myencoders[2]*RADTODEG;
    }
	else if (obj == fd_localizationgui->modeloc)  
	  {  
	    if(fl_get_button(fd_localizationgui->modeloc)==PUSHED)  
	      {
		localization_statemode=loc;
		encontrada_pos_real=FALSE;
		vacio_grid(laserloc_grid, mi_mundo);
		relleno_grid_localization(laserloc_grid);
	      }
	  }
	else if (obj == fd_localizationgui->modeslam)  
	  {  
	    if(fl_get_button(fd_localizationgui->modeslam)==PUSHED)  
	      {
		localization_statemode=slam;
			
		guarderia=TRUE;
		
		pose_estimate[0]=myencoders[0];
		pose_estimate[1]=myencoders[1];
		pose_estimate[2]=myencoders[2];
		pose_estimate[3]=myencoders[3];
		pose_estimate[4]=myencoders[4];
		
		encontrada_pos_real=TRUE;
		vacio_grid(laserloc_grid, mi_mundo);
		relleno_grid_mapping(mymapa, laserloc_grid);
	      }
	  }
  
}

void localization_guidisplay()
{
  char text[80]="";
  static float k=0;
  int i;
  Tvoxel aa,bb;
  static XPoint targetgraf,old;
  XPoint a,b;

  /* slow refresh of the complete localization gui, needed because incremental refresh misses window occlusions */
  if (localization_iteracion_display*localization_cycle>FORCED_REFRESH) 
    {localization_iteracion_display=0;
    localization_visual_refresh=TRUE;
    }
  else localization_iteracion_display++;


  k=k+1.;
  sprintf(text,"%.1f",k);
  fl_set_object_label(fd_localizationgui->fps,text);

  fl_winset(localization_canvas_win); 
  
  
  if ((localization_trackrobot==TRUE)&&
      ((fabs(myencoders[0]+localization_odometrico[0])>(localization_rango/4.))||
       (fabs(myencoders[1]+localization_odometrico[1])>(localization_rango/4.))))
    {localization_odometrico[0]=-myencoders[0];
      localization_odometrico[1]=-myencoders[1];
      localization_visual_refresh = TRUE;
    }
    
 
  if (localization_visual_refresh==TRUE)
    {
      fl_rectbound(0,0,localization_width,localization_height,mi_suelo);   
      XFlush(display);
    }
	
  //FILTRO DE PARTICULAS

    // para pintar las dos graficas de laser torico y real
  
  if(fl_get_button(fd_localizationgui->gprobabilidad)==PUSHED)
    {
      
      //distancia para los 360 angulos y mismas x e y
      /*
	fl_set_xyplot_ybounds(fd_localizationgui->graf_laser_teorico,0.0,NUM_LASER/RESOLUCION_LASER_TEORICO);
	fl_add_xyplot_overlay(fd_localizationgui->graf_laser_teorico,2,num_angulos,pruebas,NUM_ANGLES,FL_RED);  
	fl_set_xyplot_data(fd_localizationgui->graf_laser_teorico,num_angulos,pruebas,NUM_ANGLES,"","","");
      */
      //prob para los 360 angulos y mismas x e y
      fl_set_xyplot_ybounds(fd_localizationgui->graf_laser_teorico,0.0,1.0);
      fl_add_xyplot_overlay(fd_localizationgui->graf_laser_teorico,2,num_angulos,pruebas,NUM_ANGLES,FL_RED);  
      fl_set_xyplot_data(fd_localizationgui->graf_laser_teorico,num_angulos,pruebas,NUM_ANGLES,"","","");
      /* */
      
    }
  else
    {
      fl_set_xyplot_ybounds(fd_localizationgui->graf_laser_teorico,0.0,9000.0);
      
      if(fl_get_button(fd_localizationgui->teorico)==PUSHED)
	{
	  fl_add_xyplot_overlay(fd_localizationgui->graf_laser_teorico,2,angulos,distancias,NUM_LASER,FL_RED);  
	  fl_set_xyplot_data(fd_localizationgui->graf_laser_teorico,angulos,distancias,NUM_LASER,"","","");
	  
	  //actualizo valor de la distancia entre laser real y el teorico

      //sprintf(text,"%f",distancia);
      //fl_set_object_label(fd_localizationgui->msgdistancia,text);
  }
  if(fl_get_button(fd_localizationgui->real)==PUSHED)
  {
	  fl_add_xyplot_overlay(fd_localizationgui->graf_laser_teorico,3,angulos,laser_real,NUM_LASER,FL_BLUE);
	  fl_set_xyplot_data(fd_localizationgui->graf_laser_teorico,angulos,laser_real,NUM_LASER,"","","");
      
     
  }/**/
  }
    
  
	  
	  

	  
	  
      //distancia para las particulas mismas y
     
	  /*fl_set_xyplot_ybounds(fd_localizationgui->graf_laser_teorico,0.0,NUM_LASER/RESOLUCION_LASER_TEORICO);
	  fl_add_xyplot_overlay(fd_localizationgui->graf_laser_teorico,2,num_particles,misma_comp2,MAX_PARTICLES,FL_RED);  
	  fl_set_xyplot_data(fd_localizationgui->graf_laser_teorico,num_particles,misma_comp2,MAX_PARTICLES,"","","");
     */
	  //prob para las particulas mismas y
    /*fl_set_xyplot_ybounds(fd_localizationgui->graf_laser_teorico,0.0,1.0);
	  fl_add_xyplot_overlay(fd_localizationgui->graf_laser_teorico,2,num_particles,misma_comp,MAX_PARTICLES,FL_RED);  
	  fl_set_xyplot_data(fd_localizationgui->graf_laser_teorico,num_particles,misma_comp,MAX_PARTICLES,"","","");
  */    
    
  
/*    //borramos las particulas si no hay que pintarlas	
    if ((fl_get_button(fd_localizationgui->dibuja_particulas)==RELEASED)){
      delete_particles();
	  borrar_particles=FALSE;	
    } 
*/
    if ((fl_get_button(fd_localizationgui->vismapamundo)==RELEASED)&&(borrar))   
      { 
	borrar=FALSE;	
	fl_set_foreground(localizationgui_gc,mi_suelo);
	
	for (i=0;i<laserloc_grid->size*laserloc_grid->size;i++)
	  {
	    if  (grid2localizationcanvas(laserloc_grid->map[i].centro,&celdillaGraf,laserloc_grid) >=0)
	      {		 
		XDrawRectangles(display,localization_canvas_win,localizationgui_gc,&celdillaGraf,1);
		XFillRectangles(display,localization_canvas_win,localizationgui_gc,&celdillaGraf,1); 
	      }
	  }		  
    }
    
    
    
    if (((fl_get_button(fd_localizationgui->vismapamundo)==PUSHED)&&(!borrar))||
	((fl_get_button(fd_localizationgui->vismapamundo)==PUSHED)&&(localization_visual_refresh==TRUE)))
      
      {
	borrar=TRUE;   
        // pintamos la rejilla (el mundo de ocupación 2D)
        if(localization_statemode==slam)
	  {			
	    for (i=0;i<laserloc_grid->size*laserloc_grid->size;i++)
	      { 
		if (laserloc_grid->map[i].estado==OCUPADO)
		  fl_set_foreground(localizationgui_gc,FL_BLACK);
		else
		  fl_set_foreground(localizationgui_gc,mi_suelo);  
		if  (grid2localizationcanvas(laserloc_grid->map[i].centro,&celdillaGraf,laserloc_grid) >=0)
		  {		 
		    XDrawRectangles(display,localization_canvas_win,localizationgui_gc,&celdillaGraf,1);
		    XFillRectangles(display,localization_canvas_win,localizationgui_gc,&celdillaGraf,1); 
		  }
	      }
	  }		  
	else
	  {
	    for (i=0;i<rejilla_rayo.ocup;i++) 
	      {
		
		fl_set_foreground(localizationgui_gc,FL_BLACK);
		if(i==celdaa)
		  fl_set_foreground(localizationgui_gc,FL_BLUE);
		if  (grid2localizationcanvas(laserloc_grid->map[rejilla_rayo.celdas_ocupadas[i].num].centro,
					     &celdillaGraf,laserloc_grid) >=0)
		  {		 
		    XDrawRectangles(display,localization_canvas_win,localizationgui_gc,&celdillaGraf,1);
		    XFillRectangles(display,localization_canvas_win,localizationgui_gc,&celdillaGraf,1); 
		  }
	      } 
	    
	  }
	
      }
    
    
    //pintamos las particulas
    
    
    if((localization_statealg==aparticulas)&& (fl_get_button(fd_localizationgui->dibuja_particulas)==PUSHED)){
	
	draw_particles();
	borrar_particles=TRUE; 
    }
    
    
    //FIN FILTRO DE PARTICULAS	
    
    //MULTIOBJETO
    
    //pinto trayectorias
    if (fl_get_button(fd_localizationgui->treal)==PUSHED)
      draw_trajectoria(&mi_trayectoria);
  
    if (fl_get_button(fd_localizationgui->truidosa)==PUSHED)
      draw_trajectoria(&trayectoria_noise);
  
  if(localization_statealg==amultiobjeto)
  {  
	  if (fl_get_button(fd_localizationgui->dibuja_exploradores)==PUSHED){
	    
		draw_exploradores();	

	  }
	  if (fl_get_button(fd_localizationgui->dibuja_explotadores)==PUSHED){
		//printf("Pinto explotadores:\n");
	    pthread_mutex_lock (&mymutex);
	    draw_explotadores(&explotadores);
	    pthread_mutex_unlock (&mymutex);
        //BorrarLista(&explotadores,&colores);		  
	  }
  }  
  //FIN MULTIOBJETO

  
  /* VISUALIZACION: pintar o borrar de el PROPIO ROBOT.
     Siempre hay un repintado total. Esta es la ultima estructura que se se pinta, para que ninguna otra se solape encima */
  
  /* VISUALIZACION de una instantanea laser*/
  if (((((localization_display_state&DISPLAY_LASER)!=0)&&(localization_visual_refresh==FALSE))
      || (visual_delete_localization_laser==TRUE))&&
      ((((localization_display_state&DISPLAY_MY_LASER)!=0)&&(localization_visual_refresh==FALSE))
      || (visual_delete_my_laser==TRUE)))
    {  
      fl_set_foreground(localizationgui_gc,mi_suelo); 	
      XDrawPoints(display,localization_canvas_win,localizationgui_gc,
		  localization_laser_dpy,NUM_LASER,CoordModeOrigin);
    }
  
  if (((localization_display_state&DISPLAY_LASER)!=0)&&
	  ((localization_display_state&DISPLAY_MY_LASER)!=0)){
    for(i=0;i<NUM_LASER;i++)
      {
	laser2xy(i,mylaser[i],&aa,myencoders);

	xy2localizationcanvas(aa,&localization_laser_dpy[i]);
      }
    fl_set_foreground(localizationgui_gc,FL_BLUE); 
    XDrawPoints(display,localization_canvas_win,localizationgui_gc,localization_laser_dpy,NUM_LASER,CoordModeOrigin);
  }
  /* VISUALIZACION de una instantanea laser_teorico*/
  
  
  if ((((localization_display_state&DISPLAY_LOCALIZATION_LASER_TEORICO)!=0)&&(localization_visual_refresh==FALSE))
      || (visual_delete_localization_laser_teorico==TRUE))
    {  
      
      borrar_laser_teorico(my_laser);
    }
  
  if ((localization_display_state&DISPLAY_LOCALIZATION_LASER_TEORICO)!=0){
    
    pintar_laser_teorico(my_laser);
  }
  
  
  
  if ((((localization_display_state&DISPLAY_ROBOT)!=0) &&(localization_visual_refresh==FALSE))
      || (visual_delete_localization_ego==TRUE))
    {  
      fl_set_foreground(localizationgui_gc,mi_suelo); 
      /* clean last robot, but only if there wasn't a total refresh. In case of total refresh the white rectangle already cleaned all */
      for(i=0;i<numlocalization_ego;i++) 
	XDrawLine(display,localization_canvas_win,localizationgui_gc,localization_ego[i].x,
		  localization_ego[i].y,localization_ego[i+1].x,localization_ego[i+1].y);
      
    }
  
  if ((localization_display_state&DISPLAY_ROBOT)!=0){
    fl_set_foreground(localizationgui_gc,FL_MAGENTA);
    /* relleno los nuevos */
    us2xy(15,0.,0.,&aa,myencoders);
    xy2localizationcanvas(aa,&localization_ego[0]);
    us2xy(3,0.,0.,&aa,myencoders);
    xy2localizationcanvas(aa,&localization_ego[1]);
    us2xy(4,0.,0.,&aa,myencoders);
    xy2localizationcanvas(aa,&localization_ego[2]);
    us2xy(8,0.,0.,&aa,myencoders);
    xy2localizationcanvas(aa,&localization_ego[3]);
    us2xy(15,0.,0.,&aa,myencoders);
    xy2localizationcanvas(aa,&localization_ego[EGOMAX-1]);
    for(i=0;i<NUM_SONARS;i++)
      {
	us2xy((15+i)%NUM_SONARS,0.,0.,&aa,myencoders); /* Da en el Tvoxel aa las coordenadas del sensor, pues es distancia 0 */
	xy2localizationcanvas(aa,&localization_ego[i+4]);       
      }
    
    /* pinto los nuevos */
    numlocalization_ego=EGOMAX-1;
    for(i=0;i<numlocalization_ego;i++) 
      XDrawLine(display,localization_canvas_win,localizationgui_gc,localization_ego[i].x,
		localization_ego[i].y,localization_ego[i+1].x,localization_ego[i+1].y);
  }
  
  
  //Pinto robot en posicion mas probable
  /*if ((((localization_display_state&DISPLAY_ROBOT)!=0) &&(localization_visual_refresh==FALSE))
      || (visual_delete_localization_ego==TRUE))
    {  
      //fl_set_foreground(localizationgui_gc,FL_WHITE); 
	  fl_set_foreground(localizationgui_gc,mi_suelo); 	
      // clean last robot, but only if there wasn't a total refresh. In case of total refresh the white rectangle already cleaned all 
      for(i=0;i<numlocalization_ego;i++) XDrawLine(display,localization_canvas_win,localizationgui_gc,localization_max_prob[i].x,localization_max_prob[i].y,localization_max_prob[i+1].x,localization_max_prob[i+1].y);
      
    }
  
  if ((localization_display_state&DISPLAY_ROBOT)!=0){
    fl_set_foreground(localizationgui_gc,FL_BLUE);
    // relleno los nuevos 
    u2xy(15,0.,0.,punto_mas_probable,&aa);
    xy2localizationcanvas(aa,&localization_max_prob[0]);
    u2xy(3,0.,0.,punto_mas_probable,&aa);
    xy2localizationcanvas(aa,&localization_max_prob[1]);
    u2xy(4,0.,0.,punto_mas_probable,&aa);
    xy2localizationcanvas(aa,&localization_max_prob[2]);
    u2xy(8,0.,0.,punto_mas_probable,&aa);
    xy2localizationcanvas(aa,&localization_max_prob[3]);
    u2xy(15,0.,0.,punto_mas_probable,&aa);
    xy2localizationcanvas(aa,&localization_max_prob[EGOMAX-1]);
    for(i=0;i<NUM_SONARS;i++)
      {
	u2xy((15+i)%NUM_SONARS,0.,0.,punto_mas_probable,&aa); // Da en el Tvoxel aa las coordenadas del sensor, pues es distancia 0 
	xy2localizationcanvas(aa,&localization_max_prob[i+4]);       
      }
    
    // pinto los nuevos 
    numlocalization_ego=EGOMAX-1;
    for(i=0;i<numlocalization_ego;i++) XDrawLine(display,localization_canvas_win,localizationgui_gc,localization_max_prob[i].x,localization_max_prob[i].y,localization_max_prob[i+1].x,localization_max_prob[i+1].y);
  }*/
  
 
  /* visualization of VFF target */

   /*   if ((oldvfftarget.x!=vfftarget.x)||(oldvfftarget.y!=vfftarget.y))
	// the target has changed, do its last position must be cleaned from the canvas 
	{
	  fl_set_foreground(localizationgui_gc,mi_suelo);
	  XDrawLine(display,localization_canvas_win,localizationgui_gc,targetgraf.x-5,targetgraf.y,targetgraf.x+5,targetgraf.y);
	  XDrawLine(display,localization_canvas_win,localizationgui_gc,targetgraf.x,targetgraf.y-5,targetgraf.x,targetgraf.y+5);
	}
      fl_set_foreground(localizationgui_gc,FL_RED);
      xy2localizationcanvas(vfftarget,&targetgraf);
      XDrawLine(display,localization_canvas_win,localizationgui_gc,targetgraf.x-5,targetgraf.y,targetgraf.x+5,targetgraf.y);
      XDrawLine(display,localization_canvas_win,localizationgui_gc,targetgraf.x,targetgraf.y-5,targetgraf.x,targetgraf.y+5);*/
    
  

   /* clear all flags. If they were set at the beginning, they have been already used in this iteration */
  localization_visual_refresh=FALSE;
  visual_delete_localization_us=FALSE; 
  visual_delete_localization_laser=FALSE; 
  visual_delete_localization_ego=FALSE;
  visual_delete_my_laser=FALSE; 	
  visual_delete_localization_laser_teorico=FALSE;
  visual_delete_localization_mapa_mundo=FALSE;
   
}


/* callback function for button pressed inside the canvas object*/
int localization_button_pressed_on_micanvas(FL_OBJECT *ob, Window win, int win_width, int win_height, XEvent *xev, void *user_data)
{
  unsigned int keymap;
  float ygraf, xgraf;
  FL_Coord x,y;

  /* in order to know the mouse button that created the event */
  localization_mouse_button=xev->xkey.keycode;
  if(localization_canvas_mouse_button_pressed==0){
    if ((localization_mouse_button==MOUSEMIDDLE))
      {
	/* Target for VFF navigation getting mouse coordenates */
	fl_get_win_mouse(win,&x,&y,&keymap);
	ygraf=((float) (localization_height/2-y))/localization_escala;
	xgraf=((float) (x-localization_width/2))/localization_escala;
	oldvfftarget.x=vfftarget.x;
	oldvfftarget.y=vfftarget.y;
	vfftarget.y=(ygraf-localization_odometrico[1])*localization_odometrico[3]+(-xgraf+localization_odometrico[0])*localization_odometrico[4];
	vfftarget.x=(ygraf-localization_odometrico[1])*localization_odometrico[4]+(xgraf-localization_odometrico[0])*localization_odometrico[3];
      }
    else if((localization_mouse_button==MOUSELEFT)||(localization_mouse_button==MOUSERIGHT)){
      /* a button has been pressed */
      localization_canvas_mouse_button_pressed=1;
      
      /* getting mouse coordenates. win will be always the canvas window, because this callback has been defined only for that canvas */  
      fl_get_win_mouse(win,&localization_x_canvas,&localization_y_canvas,&keymap);
      old_localization_x_canvas=localization_x_canvas;
      old_localization_y_canvas=localization_y_canvas;
      
      /* from graphical coordinates to spatial ones */
      ygraf=((float) (localization_height/2-localization_y_canvas))/localization_escala;
      xgraf=((float) (localization_x_canvas-localization_width/2))/localization_escala;
      localization_mouse_y=(ygraf-localization_odometrico[1])*localization_odometrico[3]+(-xgraf+localization_odometrico[0])*localization_odometrico[4];
      localization_mouse_x=(ygraf-localization_odometrico[1])*localization_odometrico[4]+(xgraf-localization_odometrico[0])*localization_odometrico[3];
      localization_mouse_new=1;
      
	   printf("(%d,%d) Canvas: Click on (%.2f,%.2f)\n",localization_x_canvas,localization_y_canvas,localization_mouse_x,localization_mouse_y);
      /*printf("(%d,%d) Canvas: Click on (%.2f,%.2f)\n",x,y,localization_mouse_x,localization_mouse_y);
	printf("robot_x=%.2f robot_y=%.2f robot_theta=%.2f\n",myencoders[0],myencoders[1],myencoders[2]);*/
     
    
		
    }else if(localization_mouse_button==MOUSEWHEELDOWN){
      /* a button has been pressed */
      localization_canvas_mouse_button_pressed=1;

      /* modifing scale */
      localization_rango-=1000;
      if(localization_rango<=RANGO_MIN) localization_rango=RANGO_MIN;
      fl_set_slider_value(fd_localizationgui->escala,localization_rango);
      localization_visual_refresh = TRUE; /* activa el flag que limpia el fondo de la pantalla y repinta todo */ 
      localization_escala = localization_width /localization_rango;

    }else if(localization_mouse_button==MOUSEWHEELUP){
      /* a button has been pressed */
      localization_canvas_mouse_button_pressed=1;

      /* modifing scale */
      localization_rango+=1000;
      if(localization_rango>=RANGO_MAX) localization_rango=RANGO_MAX;
      fl_set_slider_value(fd_localizationgui->escala,localization_rango);
      localization_visual_refresh = TRUE; /* activa el flag que limpia el fondo de la pantalla y repinta todo */ 
      localization_escala = localization_width /localization_rango;
    }
  }

  return 0;
}

/* callback function for mouse motion inside the canvas object*/
int localization_mouse_motion_on_micanvas(FL_OBJECT *ob, Window win, int win_width, int win_height, XEvent *xev, void *user_data)
{  
  float diff_x,diff_y,diff_w;
  unsigned int keymap;
  float sqrt_value,acos_value;

  if(localization_canvas_mouse_button_pressed==1){

    /* getting mouse coordenates. win will be always the canvas window, because this callback has been defined only for that canvas */  
    fl_get_win_mouse(win,&localization_x_canvas,&localization_y_canvas,&keymap);

    if(localization_mouse_button==MOUSELEFT){

      /* robot is being moved using the canvas */
      localization_robot_mouse_motion=1;
      
      /* getting difference between old and new coordenates */
      diff_x=(localization_x_canvas-old_localization_x_canvas);
      diff_y=(old_localization_y_canvas-localization_y_canvas);
      
      sqrt_value=sqrt((diff_x*diff_x)+(diff_y*diff_y));
      if(diff_y>=0) acos_value=acos(diff_x/sqrt_value);
      else acos_value=2*PI-acos(diff_x/sqrt_value);
      diff_w=myencoders[2]-acos_value;
      
      /* shortest way to the robot theta*/
      if(diff_w>0){
	if(diff_w>=2*PI-diff_w){
	  if(2*PI-diff_w<=PI*0.7) w_teleop=RADTODEG*(diff_w)*(0.3);
	  else w_teleop=2.;
	  
	}else{
	  if(diff_w<=PI*0.7) w_teleop=RADTODEG*(diff_w)*(-0.3);
	  else w_teleop=-2.;
	}
      }else if(diff_w<0){
	  /* changing signus to diff_w */
	diff_w=diff_w*(-1);
	if(diff_w>=2*PI-diff_w){
	  if(2*PI-diff_w<=PI*0.7) w_teleop=RADTODEG*(diff_w)*(-0.3);
	  else w_teleop=-2.;
	}else{
	  if(diff_w<=2*PI-diff_w) w_teleop=RADTODEG*(diff_w)*(0.3);
	  else w_teleop=2;
	}	  
      }else w_teleop=0.;
      if(w_teleop<-joystick_maxRotVel) w_teleop=-joystick_maxRotVel;
      else if(w_teleop>joystick_maxRotVel) w_teleop=joystick_maxRotVel;
      
      /* setting new value for v */
      if((diff_w>=PI/2)&&(2*PI-diff_w>=PI/2)) v_teleop=(-1)*sqrt_value;
      else v_teleop=sqrt_value;
      if(v_teleop<-joystick_maxTranVel) v_teleop=-joystick_maxTranVel;
      else if(v_teleop>joystick_maxTranVel) v_teleop=joystick_maxTranVel;
      
    }else if(localization_mouse_button==MOUSERIGHT){
      
      /* getting difference between old and new coordenates */
      diff_x=(old_localization_x_canvas-localization_x_canvas);
      diff_y=(localization_y_canvas-old_localization_y_canvas);
      old_localization_x_canvas=localization_x_canvas;
      old_localization_y_canvas=localization_y_canvas;
      
      /* changing odometric range */
      localization_odometrico[0]+=localization_rango*(diff_x)*(0.005);
      localization_odometrico[1]+=localization_rango*(diff_y)*(0.005);
      localization_visual_refresh=TRUE;
    }
  }
  
  return 0;
}

/* callback function for button released inside the canvas object*/
int localization_button_released_on_micanvas(FL_OBJECT *ob, Window win, int win_width, int win_height, XEvent *xev, void *user_data)
{
  
  if(localization_canvas_mouse_button_pressed==1){

    if(localization_mouse_button==MOUSELEFT){
   
	/* robot is being stopped */
	localization_robot_mouse_motion=1;

	/* stopping robot */
	v_teleop=0.;
	w_teleop=0.;      
    }

    /* a button has been released */
    localization_canvas_mouse_button_pressed=0;
  }

  return 0;
}


void localization_guisuspend_aux(void){
  v_teleop=0; w_teleop=0; 
  /* to make a safety stop when the robot is being teleoperated from GUI */
  mydelete_buttonscallback(localization_guibuttons);
  mydelete_displaycallback(localization_guidisplay);
  fl_hide_form(fd_localizationgui->localizationgui);
}
void localization_hide(void)
{
   callback fn;
  if (fn==NULL){
    if ((fn=(callback)myimport ("graphics_xforms", "suspend_callback"))!=NULL){
      fn ((gui_function)localization_guisuspend_aux);
    }
  }
  else{
    fn ((gui_function)localization_guisuspend_aux);
  }
}

void localization_guiresume_aux(void)
{
  static int k=0;
  XGCValues gc_values;
	printf("2\n");

  if (k==0) /* not initialized */
    {
      k++;

      /* Coord del sistema odometrico respecto del visual */
      localization_odometrico[0]=0.;
      localization_odometrico[1]=0.;
      localization_odometrico[2]=0.;
      localization_odometrico[3]= cos(0.);
      localization_odometrico[4]= sin(0.);

      localization_display_state = localization_display_state | DISPLAY_LASER;
      localization_display_state = localization_display_state | DISPLAY_SONARS;
      localization_display_state = localization_display_state | DISPLAY_ROBOT;

      fd_localizationgui = create_form_localizationgui();
      fl_set_form_position(fd_localizationgui->localizationgui,400,50);
      fl_show_form(fd_localizationgui->localizationgui,FL_PLACE_POSITION,FL_FULLBORDER,"localization");
      localization_canvas_win= FL_ObjWin(fd_localizationgui->micanvas);
      gc_values.graphics_exposures = False;
      localizationgui_gc = XCreateGC(display, localization_canvas_win, GCGraphicsExposures, &gc_values);  

			printf("3\n");

      /* canvas handlers */
      fl_add_canvas_handler(fd_localizationgui->micanvas,ButtonPress,
			    localization_button_pressed_on_micanvas,NULL);
      fl_add_canvas_handler(fd_localizationgui->micanvas,ButtonRelease,
			    localization_button_released_on_micanvas,NULL);
      fl_add_canvas_handler(fd_localizationgui->micanvas,MotionNotify,
			    localization_mouse_motion_on_micanvas,NULL);
    }
  else 
    {
      fl_show_form(fd_localizationgui->localizationgui,FL_PLACE_POSITION,FL_FULLBORDER,"localization");
      localization_canvas_win= FL_ObjWin(fd_localizationgui->micanvas);
    }
  
  /* Empiezo con el canvas en blanco */
  localization_width = fd_localizationgui->micanvas->w;
  localization_height = fd_localizationgui->micanvas->h;
  fl_winset(localization_canvas_win); 
  fl_rectbound(0,0,localization_width,localization_height,mi_suelo);   
  /*  XFlush(display);*/ 
  
  localization_trackrobot=TRUE;
  fl_set_button(fd_localizationgui->track_robot,PUSHED);
  
  fl_set_slider_bounds(fd_localizationgui->escala,RANGO_MAX,RANGO_MIN);
  fl_set_slider_filter(fd_localizationgui->escala,localization_range); /* Para poner el valor del slider en metros en pantalla */
  fl_set_slider_value(fd_localizationgui->escala,RANGO_INICIAL);
  localization_escala = localization_width/localization_rango;
	
  //slider parametrizadores de las funciones de distancia y probabilidad	
  fl_set_slider_bounds(fd_localizationgui->ssimplep,RANGO_MAX_SP,RANGO_MIN_SP);
  fl_set_slider_value(fd_localizationgui->ssimplep,RANGO_INICIAL_SP);
	
  fl_set_slider_bounds(fd_localizationgui->ssumatoriop,RANGO_MAX_DP,RANGO_MIN_DP);
  fl_set_slider_value(fd_localizationgui->ssumatoriop,RANGO_INICIAL_DP);
  
  fl_set_slider_bounds(fd_localizationgui->scorrelacionp,RANGO_MAX_CP,RANGO_MIN_CP);
  fl_set_slider_value(fd_localizationgui->scorrelacionp,RANGO_INICIAL_CP);
  
  fl_set_slider_bounds(fd_localizationgui->ssimplee,RANGO_MAX_SE,RANGO_MIN_SE);
  fl_set_slider_value(fd_localizationgui->ssimplee,RANGO_INICIAL_SE);
  
  fl_set_slider_bounds(fd_localizationgui->ssumatorioe,RANGO_MAX_DE,RANGO_MIN_DE);
  fl_set_slider_value(fd_localizationgui->ssumatorioe,RANGO_INICIAL_DE);
  
  fl_set_slider_bounds(fd_localizationgui->scorrelacione,RANGO_MAX_CE,RANGO_MIN_CE);
  fl_set_slider_value(fd_localizationgui->scorrelacione,RANGO_INICIAL_CE);
	
  //Valor inicial de visualizador de explotadores
  fl_set_slider_bounds(fd_localizationgui->vis_explotadores,0.,1.);
  fl_set_slider_value(fd_localizationgui->vis_explotadores,VISUALIZADOR_INICIAL_EXPLOTADORES);	
  
  fl_set_slider_bounds(fd_localizationgui->screar_raza,1.,0.);
  fl_set_slider_value(fd_localizationgui->screar_raza,UMBRAL_EXPLOTADORES_INICIAL);	
  
  fl_set_slider_bounds(fd_localizationgui->sperder_raza,1.,0.);
  fl_set_slider_value(fd_localizationgui->sperder_raza,UMBRAL_PERDIDA_INICIAL);	
  
  //slider de ruido
  fl_set_slider_bounds(fd_localizationgui->snoise_x,SIGMA_RADIO_MIN,SIGMA_RADIO_MAX);
  fl_set_slider_value(fd_localizationgui->snoise_x,SIGMA_RADIO_NOISE_INICIAL);	
  	 
  fl_set_slider_bounds(fd_localizationgui->snoise_ang,SIGMA_THETA_MIN,SIGMA_THETA_MAX);
  fl_set_slider_value(fd_localizationgui->snoise_ang,SIGMA_THETA_NOISE_INICIAL);
	
  localization_statedis=sumatorio;
  fl_set_button(fd_localizationgui->sumatorio,PUSHED);
  localization_stateprob=potencial;
  fl_set_button(fd_localizationgui->potencial,PUSHED);
  localization_statealg=amultiobjeto;
  fl_set_button(fd_localizationgui->amultiobjeto,PUSHED);
  localization_statemode=loc;
  fl_set_button(fd_localizationgui->modeloc,PUSHED);   
  v_teleop=0.;
  w_teleop=0.;

  fl_set_positioner_xvalue(fd_localizationgui->center,0.5);
  fl_set_positioner_yvalue(fd_localizationgui->center,0.5);
	
  myregister_buttonscallback(localization_guibuttons);
  myregister_displaycallback(localization_guidisplay);
}
void localization_show(){
   static callback fn=NULL;

		printf("1\n");
   if (fn==NULL){
     if ((fn=(callback)myimport ("graphics_xforms", "resume_callback"))!=NULL){
       fn ((gui_function)localization_guiresume_aux);
      }
   }
   else{
      fn ((gui_function)localization_guiresume_aux);
   }
}
