/* Copyright 2001,2002 Jos� Maria Ca�as */
/*
    This file is part of gridslib.

    Gridslib is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

#ifdef __cplusplus
extern "C" {
#endif

#ifndef tvoxel
#define tvoxel
typedef struct voxel{
  float x;
  float y;}Tvoxel;
#endif

#define MAXJAMB_INIMAGE 100
typedef struct jamb_reading{
  int jambpixel[MAXJAMB_INIMAGE];
  int nummedidas;
  Tvoxel fromhere;
  float thetasensor;
  long int clock; 
}Tjamb_reading;


typedef struct lectura_sonar{
  float distancia;
  Tvoxel fromhere;
  float thetasensor;
  long int clock; 
}Tlectura_sonar;

typedef struct laser_scan{
  float distancias[361];
  int nummedidas;
  Tvoxel fromhere;
  float thetasensor;
  long int clock;
}Tlectura_laser;

typedef struct lectura_posicionrobot{
  Tvoxel fromhere;
  float thetarobot;
  long int clock;
}Tlectura_posicionrobot;


  typedef struct evidencia{
    float o,e;
    Tvoxel punto;
    float angle;
    long int clock;
  }Tevidencia;
  
  typedef struct {
    float theta,prob,distancia;
  }theta_prob;
  

#define MAX_ESTADO 100.
#define MAX_LINE 300
#define MAX_THETA_NUM 8

typedef struct celdilla{
  Tvoxel centro;
  float estado;
  float estado_o,estado_e;
  float *angles_o;
  float *angles_e;
  Tevidencia *historia;
  int in;
  long int ultima_actualizacion;
  int abreviado;
  int nueva;
  theta_prob theta_array[MAX_THETA_NUM];
}Tceldilla;

/* MAX_LINE es la longitud maxima de las lineas en el fichero de configuracion del grid. "centro" es la localizacion espacial del centro de la celdilla., para no andar recalculandola constantemente. "abreviado" es un flag para recorrer mas rapidamente todo el grid buscando solo las celdillas cuyo estado sea mayor que cierto umbral (thresabbgrid). "nueva" es un flag para refrescar la visualizacion de modo incremental, solo las celdillas que hayan cambiado. El estado fluctua entre [-MAXESTADO,MAXESTADO]. "historia" es un puntero a una array circular con las ultimas (grid->longhistoria) evidencias que han caido en esa celdilla. El puntero de insercion sobre ese array circular es "in".*/

enum etiquetas{probabilistico,mayoria,ec_diferencial,histogramico,borroso,teoria_evidencia,independientes,todas,cono_denso,lobulo_denso,eje_denso,cono_muestreado,lobulo_muestreado,constante,lineal,nulo,multiplicar,observacion_neutra,suma_acotada,producto_algebraico,cilindro,rectangulo};


typedef struct grid{
  float resolucion;
  float eje;
  int size;
  Tvoxel lo,hi,centro;
  int regla;
  float ec_diferencial_speed;
  float mayoria_saturacion, mayoria_ruido;
  int operador_union;
  int paso_tiempo;
  float factor_envejecimiento;
  int longhistoria;
  int cell_angles;

  int* abbgrid;
  int* abbgrid1;
  int* abbgrid2;
  int numabbgrid;
  float thresabbgrid;

  Tceldilla *map;
  Tceldilla *map1;
  Tceldilla *map2;

  /* Sensor models */
  int sonar_geometria;
  float sonar_apertura;
  int sonar_muestreo;
  float sonar_noobstacle;
  float sonar_radialerror;
  float sonar_lobe_ticks;
  float sonar_lobe_datanumber;
  float *sonar_lobe_data;
  int sonar_fdistancia;
  float sonar_residuo,sonar_maxd,sonar_mind;
  float sonar_o,sonar_e;
  int sonar_filtra;
  int num_sonarfiltro;
  Tlectura_sonar *filtro;
 
  int laser_geometria;
  float laser_apertura;
  int laser_muestras;
  float laser_noobstacle;
  float laser_o,laser_e;

  int robot_geometria;
  float robot_radio;
  float robot_largo;
  float robot_ancho;
  float robot_e;

  float visualjamb_rmax;
  float visualjamb_aperture;
  int visualjamb_aperture_pixels;
  float visualjamb_positive, visualjamb_negative;
  int visualjamb_filtra;
  int num_visualjambfiltro;
  Tjamb_reading *visualjamb_filtro;

}Tgrid;

/* "resolucion" es la dimension en milimetros de una celdilla, que se asumen cuadradas. "size" es el numero de pixels en un eje. El total de celdillas en grid es size*size. "lo, hi, centro" son las coordenadas de la esquina inferior derecha, superior izquierda y el centro respectivamente. La esquina inferior (lo) se utiliza para acelerar los computos y no tener que calcularla en cada incorporacion u olvido. "regla" indica la regla de actualizacion que se utiliza para actualizar los valores de las celdillas desde las nuevas observaciones. "abbgrid" array de indices, con los indices que apuntan a celdillas de grid cuyo estado es no nulo (en realidad con estado>thresabbgrid). Este indice es el que se utiliza para barrer el grid a la hora de segmentar. 
*/


Tgrid *inicia_grid(Tvoxel centro,char *fichero);
void termina_grid(Tgrid *gg);
void grid_reset(Tgrid *gg);
/* Inicializa el estado de todas las celdillas a 0 */
void envejece_grid(Tgrid *gg);
void reubica_grid(Tgrid *gg, Tvoxel nuevocentro);

void incorpora_sonar(Tgrid *gg, Tlectura_sonar sonar);
void incorpora_robot(Tgrid *gg, Tlectura_posicionrobot posicionrobot);
void incorpora_laser(Tgrid *gg, Tlectura_laser laser);
void add_jamb(Tgrid *g, Tjamb_reading obs);

int actualiza_celdilla(Tgrid *gg, Tevidencia e);

#ifdef __cplusplus
}
#endif


/*  
FORMATO DEL FICHERO DE CONFIGURACION DE GRIDS:

Un parametro por linea, compuesta de: "nombre_de_parametro = valor_de_parametro". Se admiten lineas en blanco. Admite comentarios, con lineas que comienzan por #. Watch out: one empty space is required before and after = .

# Parametros generales y regla de combinacion:
* dimension = <float>, tama�o en milimetros de un eje del grid (p.e. 10000)
* resolucion = <float>, tama�o en milimetros de cada celdilla (pe 50)
* tipo = [probabilistico,mayoria,ec_diferencial,histogramico,borroso,teoria_evidencia], regla de actualizacion utilizada en el grid.
* ec_diferencial_speed = <float>, [0,1] modula la velocidad de cambio de creencia, al multiplicar el incremento que propone cada lectura en las celdillas.
* mayoria_saturacion = <float>, [0,100] (por ejemplo 50.)
* mayoria_ruido = <float>, [0,100] (por ejemplo 10.) modulan la funcion de decision por mayoria. Se exige que la suma acumulada de evidencias supere el tanto por cierto "mayoria_ruido" de la historia para que la creencia no sea nula. Si supera ademas el tanto por cierto "mayoria_saturacion" entonces la creencia es maxima (o minima). Entre ambos umbrales la creencia varia linealmente.
* paso_tiempo = [nulo,multiplicar,observacion_neutra]
* long_historia = <int>, numero de observaciones que caben en la memoria local a cada celdilla.
* factor_envejecimiento = <float>, [0,1] (por ejemplo 0.95)
* ec_diferencial_speed = <float> [0,1] velocidad de cambio del modelo con ecuacion diferencial.
* cell_angles = <int> [>1] number of incidence angles taken into account for grids with sonar data, in each cell.

# Parametros modelo sonar:
* sonar_filtra = [independientes,todas]
* sonar_geometria = [cono_denso,cono_muestreado,lobulo_denso,eje_denso]
* sonar_apertura = <float>, [0,90] apertura en grados de la onda ultrasonica (por ejemplo 22.)
* sonar_noobstacle = <float>, mm. For readings further than this distance, no obstacle has been observed in the influence area of the observation. Emptyness evidence should be included in such cells.
* sonar_radialerror = %. Maximum radial error that any reading deviates from real distance. Given as percentage of distance reading. All cells closer than radialerror/2 to measured distance are considered occupied.
* sonar_lobe_tics = mm. 
* sonar_lobe_data = data1 data2 data3 ... [degrees] set of maximum aperture angles, given at n sonar_lobe_tics, with n increasing each new data
* sonar_fdistancia = [constante,lineal]
* sonar_residuo = <float>, [0,1] segun modelo de influencia de la medida en su peso.
* sonar_maxd = <float>, >0, milimetros (pe 2000)
* sonar_mind = <float>, >0, milimetros

#Para el caso probabilistico
* sonar_o = <float>, [0,1] p(celdilla=ocupada/observacion) (pe 0.7)
* sonar_e = <float>, [0,1] p(celdilla=ocupada/observacion) (pe 0.3)
#Para el caso borroso y teoria de evidencia
* sonar_o = <float>, [0,1], kw o m(F)
* sonar_e = <float>, [0,1], ke o m(E)  (es positivo)
#Para el caso ec diferencial
* sonar_o = <float>, [0,1], delta ocupacion
* sonar_e = <float>, [0,1], delta vacio (debe incluir signo negativo)  
#Para el caso mayoria
* sonar_o = <float>, delta ocupacion 
* sonar_e = <float>, delta vacio (debe incluir signo negativo)  

#Parametros laser:
* laser_geometria = [cono_denso,eje_denso]
* laser_apertura = <float>, grados (por ejemplo 0.5).
* laser_noobstacle = <float>, mm. For readings further than or equal to this distance, no obstacle has been observed in the influence area of the observation. Emptyness evidence should be included in such cells.
* laser_muestras = <int>, de las 361 medidas posibles en cada barrido, con cuantas me quedo (aleatoriamente) para realmente incorporar al grid. (por ejemplo 50).
* laser_o = <float>, delta ocupacion
* laser_e = <float>, delta vacio (debe incluir signo negativo)  

#Parametros del modelo de robot:
* robot_geometria = [cilindro,rectangulo]
* robot_radio = <float>, milimetros (por ejemplo 248 para Hermes)
* robot_largo = <float>, milimetros
* robot_ancho = <float>, milimetros
* robot_e = <float>, delta vacio (debe incluir signo negativo para enforque por mayoria)   (en el caso de probabilistico es p(celdilla=ocupada/observacion) (pe 0.3) ) 

# Parametros modelo visualjambs:
* visualjamb_rmax = <float>, milimeters
* visualjamb_aperture = <float>, degrees;
* visualjamb_aperture_pixels = <int>, pixel columns in the image, spanning the whole aperture;
* visualjamb_positive = <float>
* visualjamb_negative = <float>
* visualjamb_filtra = [independientes,todas]
*/



