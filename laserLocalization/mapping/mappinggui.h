/** Header file generated with fdesign on Sat Jul 18 23:40:48 2009.**/

#ifndef FD_mappinggui_h_
#define FD_mappinggui_h_

/** Callbacks, globals and object handlers **/


/**** Forms and Objects ****/
typedef struct {
	FL_FORM *mappinggui;
	void *vdata;
	char *cdata;
	long  ldata;
	FL_OBJECT *fps;
	FL_OBJECT *exit;
	FL_OBJECT *escala;
	FL_OBJECT *track_robot;
	FL_OBJECT *micanvas;
	FL_OBJECT *center;
	FL_OBJECT *visMapping;
	FL_OBJECT *vissegment;
	FL_OBJECT *textsegm;
	FL_OBJECT *text_segm_memory;
	FL_OBJECT *vissegmentmemory;
	FL_OBJECT *opc_segm_memory;
	FL_OBJECT *pb_olvido;
	FL_OBJECT *textolvido;
	FL_OBJECT *vislasersegment;
	FL_OBJECT *odometria;
	FL_OBJECT *breal_odom;
	FL_OBJECT *braw_odom;
	FL_OBJECT *bestimate_odom;
} FD_mappinggui;

extern FD_mappinggui * create_form_mappinggui(void);

#endif /* FD_mappinggui_h_ */
