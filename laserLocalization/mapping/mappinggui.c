/* Form definition file generated with fdesign. */

#include "forms.h"
#include <stdlib.h>
#include "mappinggui.h"

FD_mappinggui *create_form_mappinggui(void)
{
  FL_OBJECT *obj;
  FD_mappinggui *fdui = (FD_mappinggui *) fl_calloc(1, sizeof(*fdui));

  fdui->mappinggui = fl_bgn_form(FL_NO_BOX, 882, 525);
  obj = fl_add_box(FL_UP_BOX,0,0,882,525,"");
    fl_set_object_lcolor(obj,FL_BLUE);
  fdui->fps = obj = fl_add_text(FL_NORMAL_TEXT,754,15,51,24,"");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
    fl_set_object_lalign(obj,FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
  fdui->exit = obj = fl_add_button(FL_NORMAL_BUTTON,816,15,51,24,"EXIT");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
    fl_set_object_color(obj,FL_DARKCYAN,FL_COL1);
    fl_set_object_lcolor(obj,FL_CYAN);
    fl_set_object_lsize(obj,FL_NORMAL_SIZE);
  fdui->escala = obj = fl_add_valslider(FL_VERT_NICE_SLIDER,610,32,44,224,"scale,m");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
    fl_set_object_color(obj,FL_COL1,FL_YELLOW);
    fl_set_object_lcolor(obj,FL_YELLOW);
    fl_set_object_lsize(obj,FL_DEFAULT_SIZE);
    fl_set_object_lalign(obj,FL_ALIGN_TOP);
    fl_set_object_lstyle(obj,FL_BOLD_STYLE);
  fdui->track_robot = obj = fl_add_button(FL_PUSH_BUTTON,673,141,43,42,"track\n robot");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
    fl_set_object_color(obj,FL_DARKER_COL1,FL_BOTTOM_BCOL);
    fl_set_object_lcolor(obj,FL_YELLOW);
  fdui->micanvas = obj = fl_add_canvas(FL_NORMAL_CANVAS,14,10,582,500,"canvas");
    fl_set_object_lcolor(obj,FL_RIGHT_BCOL);
  fdui->center = obj = fl_add_positioner(FL_NORMAL_POSITIONER,665,196,60,60,"");
    fl_set_object_color(obj,FL_DARKER_COL1,FL_YELLOW);
    fl_set_object_lcolor(obj,FL_YELLOW);
    fl_set_object_lalign(obj,FL_ALIGN_TOP);
  fdui->visMapping = obj = fl_add_labelframe(FL_ENGRAVED_FRAME,611,277,255,157,"Visualizacion");
    fl_set_object_color(obj,FL_COL1,FL_COL1);
    fl_set_object_lcolor(obj,FL_YELLOW);
    fl_set_object_lsize(obj,FL_NORMAL_SIZE);
    fl_set_object_lstyle(obj,FL_TIMESBOLD_STYLE);
  fdui->vissegment = obj = fl_add_button(FL_PUSH_BUTTON,621,344,26,25,"");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
    fl_set_object_color(obj,FL_COL1,FL_YELLOW);
    fl_set_object_lsize(obj,FL_NORMAL_SIZE);
    fl_set_object_lstyle(obj,FL_TIMESBOLD_STYLE);
  fdui->textsegm = obj = fl_add_text(FL_NORMAL_TEXT,656,347,157,23,"Segmentos del laser");
    fl_set_object_lsize(obj,FL_NORMAL_SIZE);
    fl_set_object_lalign(obj,FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
    fl_set_object_lstyle(obj,FL_TIMESBOLD_STYLE);
  fdui->text_segm_memory = obj = fl_add_text(FL_NORMAL_TEXT,655,389,176,23,"Memoria de segmentos");
    fl_set_object_lsize(obj,FL_NORMAL_SIZE);
    fl_set_object_lalign(obj,FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
    fl_set_object_lstyle(obj,FL_TIMESBOLD_STYLE);
  fdui->vissegmentmemory = obj = fl_add_button(FL_PUSH_BUTTON,621,388,26,25,"");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
    fl_set_object_color(obj,FL_COL1,FL_YELLOW);
    fl_set_object_lsize(obj,FL_NORMAL_SIZE);
    fl_set_object_lstyle(obj,FL_TIMESBOLD_STYLE);
  fdui->opc_segm_memory = obj = fl_add_labelframe(FL_ENGRAVED_FRAME,611,446,256,63,"Opciones memoria segmentos");
    fl_set_object_lcolor(obj,FL_YELLOW);
    fl_set_object_lsize(obj,FL_NORMAL_SIZE);
    fl_set_object_lstyle(obj,FL_TIMESBOLD_STYLE);
  fdui->pb_olvido = obj = fl_add_button(FL_PUSH_BUTTON,624,465,26,25,"");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
    fl_set_object_color(obj,FL_COL1,FL_YELLOW);
    fl_set_object_lsize(obj,FL_NORMAL_SIZE);
    fl_set_object_lstyle(obj,FL_TIMESBOLD_STYLE);
  fdui->textolvido = obj = fl_add_text(FL_NORMAL_TEXT,659,466,197,25,"Olvido temporal segmentos");
    fl_set_object_lsize(obj,FL_NORMAL_SIZE);
    fl_set_object_lalign(obj,FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
    fl_set_object_lstyle(obj,FL_TIMESBOLD_STYLE);
  fdui->vislasersegment = obj = fl_add_button(FL_PUSH_BUTTON,620,303,26,25,"");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
    fl_set_object_color(obj,FL_COL1,FL_YELLOW);
    fl_set_object_lsize(obj,FL_NORMAL_SIZE);
    fl_set_object_lstyle(obj,FL_TIMESBOLD_STYLE);
  obj = fl_add_text(FL_NORMAL_TEXT,655,304,157,23,"Laser");
    fl_set_object_color(obj,FL_COL1,FL_YELLOW);
    fl_set_object_lsize(obj,FL_NORMAL_SIZE);
    fl_set_object_lalign(obj,FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
    fl_set_object_lstyle(obj,FL_TIMESBOLD_STYLE);
  obj = fl_add_text(FL_NORMAL_TEXT,744,139,122,19,"Odometria");
    fl_set_object_lcolor(obj,FL_YELLOW);
    fl_set_object_lalign(obj,FL_ALIGN_CENTER|FL_ALIGN_INSIDE);

  fdui->odometria = fl_bgn_group();
  fdui->breal_odom = obj = fl_add_lightbutton(FL_RADIO_BUTTON,743,165,123,29,"Real");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
  fdui->braw_odom = obj = fl_add_lightbutton(FL_RADIO_BUTTON,743,195,123,29,"Raw");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
  fdui->bestimate_odom = obj = fl_add_lightbutton(FL_RADIO_BUTTON,743,226,123,29,"Estimate");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
  fl_end_group();

  fl_end_form();

  fdui->mappinggui->fdui = fdui;

  return fdui;
}
/*---------------------------------------*/

