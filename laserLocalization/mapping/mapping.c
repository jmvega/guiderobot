/*
 *  Copyright (C) 2006 José María Cañas Plaza 
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  Authors : José María Cañas Plaza <jmplaza@gsyc.escet.urjc.es>
 */

#include "jde.h"
#include <forms.h>
#include "graphics_xforms.h"
#include "mappinggui.h"
#include "mapping.h"
#include "pioneer.h"
#include <math.h>
#include <stdlib.h>


#define DISPLAY_ROBOT 0x01UL
#define DISPLAY_SONARS 0x04UL
#define DISPLAY_LASER 0x08UL
#define DISPLAY_LASER_SEGMENT 0x08UL
#define DISPLAY_SEGMENT 0x10UL
#define DISPLAY_SEGMENT_MEMORY 0x20UL
#define MOUSELEFT 1
#define MOUSEMIDDLE 2
#define MOUSERIGHT 3
#define MOUSEWHEELUP 4
#define MOUSEWHEELDOWN 5
/*SEGMENTACION LASER*/
 
/*Segmentación*/

#define MAX_SEGM 2500
#define DIST_PTOS 500.
#define DIST_SEGM 50.
#define DIST_PARALELAS_INSTANTANEOS 100
/*Gui callbacks*/
registerbuttons myregister_buttonscallback;
registerdisplay myregister_displaycallback;
deletebuttons mydelete_buttonscallback;
deletedisplay mydelete_displaycallback;

Display *mydisplay;
int  *myscreen;

 
typedef struct {
	Tvoxel pF, pI;
} TipoSegmento;

typedef struct {
	TipoSegmento segm[MAX_SEGM];
	int tam;
} TipoSegmentos;



typedef struct {
	float Sum_XY, Sum_X2, Sum_X, Sum_Y, N;
}TSegmHip;

typedef struct {
	XPoint pF, pI;
} TipoSegmGraf;

TSegmHip sh;
TipoSegmentos segmentos;
TipoSegmGraf coord_graf_segmentos[MAX_SEGM];

int ini[MAX_SEGM], fin[MAX_SEGM];
int asignados[MAX_SEGM];
int num_segm;

int visual_delete_mapping_segment=FALSE;
/*FIN SEGMENTACION LASER*/

/*MEMORIA SEGMENTOS*/

#define DIST_PARALELAS 30.
#define DIST_RAYOLASER 50.
#define DIST_SEGM2 200
#define MARGEN_RAYO_LASER 150
#define EXISTENCIA_MEMORIA_SEGMENTOS 30*1000000 /*10 segundos*/
#define INCREMENTO_LANDA 0.2
#define INCREMENTO_FRAGMENTACION 350 
#define DIFERENCIA_ANGULO cos(2)*cos(2)
#define w_pos_max 30

typedef struct {
	TipoSegmento segm[MAX_SEGM];
	int validos[MAX_SEGM];
	int hora_inicio[MAX_SEGM];
	int tam;

} TipoMemoriaSegmentos;

typedef struct tagXYZ
{
    float X, Y, Z;
}
XYZ;

TipoSegmGraf coord_graf_segment_memory[MAX_SEGM];
TipoSegmGraf coord_graf_segment_laser[NUM_LASER];
TipoMemoriaSegmentos memoria_segmentos;
TipoSegmento segmentosLaser[NUM_LASER];
int val[NUM_LASER];
int val_pintados[NUM_LASER];
int olvido;
int num_segm_memory,num_segm_laser;

int visual_delete_mapping_segment_memory=FALSE;
int visual_delete_mapping_laser_segment=FALSE;
/*FIN MEMORIA SEGMENTOS*/


int mapping_id=0; 
int mapping_brothers[MAX_SCHEMAS];
arbitration mapping_callforarbitration;
int mapping_cycle=100; /* ms */

int *myencontrada_pos_real;
float *myraw_odom;
float *mypos_estimate;

int *mylaser;
runFn laserresume;
stopFn lasersuspend;

float *myencoders;
runFn encodersresume;
stopFn encoderssuspend;

float *myv;
float *myw;
runFn motorsresume;
stopFn motorssuspend;

enum mappingodom {real,raw,estimate};
int mapping_odom;

FD_mappinggui *fd_mappinggui;
GC mappinggui_gc;
Window  mapping_canvas_win;
unsigned long mapping_display_state;
int mapping_visual_refresh=FALSE;
int mapping_iteracion_display=0;
int mapping_canvas_mouse_button_pressed=0;
int mapping_mouse_button=0;
int mapping_robot_mouse_motion=0;
FL_Coord mapping_x_canvas,mapping_y_canvas,old_mapping_x_canvas,old_mapping_y_canvas;
float mapping_mouse_x, mapping_mouse_y;
Tvoxel vfftarget,oldvfftarget;
int mapping_mouse_new=0;

#define PUSHED 1
#define RELEASED 0 
#define FORCED_REFRESH 5000 /* ms */
/*Every forced_refresh the display is drawn from scratch. If it is too small it will cause flickering with grid display. No merece la pena una hebra de "display_lento" solo para repintar completamente la pantalla. */

float   mapping_escala, mapping_width, mapping_height;
int mapping_trackrobot=FALSE;
float mapping_odometrico[5];
#define RANGO_MAX 60000. /* en mm */
#define RANGO_MIN 500. /* en mm */ 
#define RANGO_INICIAL 55000. /* en mm */
float mapping_rango=(float)RANGO_INICIAL; /* Rango de visualizacion en milimetros */

#define EGOMAX NUM_SONARS+5
XPoint mapping_ego[EGOMAX];
int nummapping_ego=0;
int visual_delete_mapping_ego=FALSE;

XPoint mapping_laser_dpy[NUM_LASER];
int visual_delete_mapping_laser=FALSE;

XPoint mapping_us_dpy[NUM_SONARS*2];
int visual_delete_mapping_us=FALSE;

#define joystick_maxRotVel 30 /* deg/sec */
#define joystick_maxTranVel 500 /* mm/sec */
float v_teleop, w_teleop;


const char *mapping_range(FL_OBJECT *ob, double value, int prec)
{
static char buf[32];

sprintf(buf,"%.1f",value/1000.);
return buf;
}

int xy2mappingcanvas(Tvoxel point, XPoint* grafico)
     /* return -1 if point falls outside the canvas */
{
float xi, yi;

xi = (point.x * mapping_odometrico[3] - point.y * mapping_odometrico[4] 
      + mapping_odometrico[0])*mapping_escala;
yi = (point.x * mapping_odometrico[4] + point.y * mapping_odometrico[3] 
      + mapping_odometrico[1])*mapping_escala;
/* Con esto cambiamos al sistema de referencia de visualizacion, centrado 
   en algun punto xy y con alguna orientacion definidos por odometrico. 
   Ahora cambiamos de ese sistema al del display, donde siempre hay un desplazamiento 
   a la esquina sup. izda. y que las y se cuentan para abajo. */

 grafico->x = xi + mapping_width/2;
 grafico->y = -yi + mapping_height/2;

 if ((grafico->x <0)||(grafico->x>mapping_width)) return -1; 
 if ((grafico->y <0)||(grafico->y>mapping_height)) return -1; 
 return 0;
}

int absolutas2relativas(Tvoxel in, Tvoxel *out)
/*  Calcula la posicion relativa respecto del robot de un punto absoluto.
    El robot se encuentra en robot[0], robot[1] con orientacion robot[2] 
    respecto al sistema de referencia absoluto
*/ 
{
  if (out!=NULL)
    {
      (*out).x = in.x*myencoders[3] + in.y*myencoders[4] - myencoders[0]*myencoders[3] 
	- myencoders[1]*myencoders[4];
      (*out).y = in.y*myencoders[3] - in.x*myencoders[4] + myencoders[0]*myencoders[4] 
	- myencoders[1]*myencoders[3];
    }
  return 0;
}

int relativas2absolutas(Tvoxel in, Tvoxel *out)
/*  Calcula la posicion absoluta de un punto expresado en el sistema 
    de coordenadas solidario al robot. El robot se encuentra en robot[0], 
    robot[1] con orientacion robot[2] respecto al sistema de referencia absoluto
 */ 
{
  if (out!=NULL)
    {
      (*out).x = in.x*myencoders[3] - in.y*myencoders[4] + myencoders[0];
      (*out).y = in.y*myencoders[3] + in.x*myencoders[4] + myencoders[1];
    }
  return 0;
}

void mapping_init_aux(){
   if (myregister_buttonscallback==NULL){
      if ((myregister_buttonscallback=(registerbuttons)myimport ("graphics_xforms", "register_buttonscallback"))==NULL){
         printf ("I can't fetch register_buttonscallback from graphics_xforms\n");
         jdeshutdown(1);
      }
      if ((mydelete_buttonscallback=(deletebuttons)myimport ("graphics_xforms", "delete_buttonscallback"))==NULL){
         printf ("I can't fetch delete_buttonscallback from graphics_xforms\n");
         jdeshutdown(1);
      }
      if ((myregister_displaycallback=(registerdisplay)myimport ("graphics_xforms", "register_displaycallback"))==NULL){
         printf ("I can't fetch register_displaycallback from graphics_xforms\n");
         jdeshutdown(1);
      }
      if ((mydelete_displaycallback=(deletedisplay)myimport ("graphics_xforms", "delete_displaycallback"))==NULL){
         jdeshutdown(1);
         printf ("I can't fetch delete_displaycallback from graphics_xforms\n");
      }
   }
  if ((myscreen=(int *)myimport("graphics_xforms", "screen"))==NULL){
    fprintf (stderr, "navigation: I can't fetch screen from graphics_xforms\n");
    jdeshutdown(1);
  }
  if ((mydisplay=(Display *)myimport("graphics_xforms", "display"))==NULL){
    fprintf (stderr, "navigation: I can't fetch display from graphics_xforms\n");
    jdeshutdown(1);
  }
}


int pintaSegmento(Tvoxel a, Tvoxel b, int color)
/* colores: FL_PALEGREEN */
{
  XPoint aa,bb;
  
  fl_set_foreground(mappinggui_gc,color);
  xy2mappingcanvas(a,&aa);
  xy2mappingcanvas(b,&bb);
  XDrawLine(mydisplay,mapping_canvas_win,mappinggui_gc,aa.x,aa.y,bb.x,bb.y);
  return 0;
}

/*Funciones y procedimientos auxiliares*/
float anguloSegmTo2(TipoSegmento s, TipoSegmento t,float *angulo)
{
  float v1x,v1y,v2x,v2y;
  float numerador,mod1,mod2,denominador;
  int vale=TRUE;
  
  /*printf("s1(%f,%f)(%f,%f)\n",s.pI.x,s.pI.y,s.pF.x,s.pF.y);
    printf("s2(%f,%f)(%f,%f)\n",t.pI.x,t.pI.y,t.pF.x,t.pF.y);
    printf("(%f,%f),(%f,%f)\n",v1x,v1y,v2x,v2y);
    printf("%f,%f,%f,%f\n\n",mod1,mod2,numerador,denominador);*/
  
  v1x=s.pF.x-s.pI.x;
  v1y=s.pF.y-s.pI.y;
  v2x=t.pF.x-t.pI.x;
  v2y=t.pF.y-t.pI.y;
  
  
  numerador=v1x*v2x+v1y*v2y;
  numerador=abs(numerador);
  
  mod1=pow(v1x,2)+pow(v1y,2);
  mod2=pow(v2x,2)+pow(v2y,2);
  denominador=mod1*mod2;
	
  
  
  if(denominador==0)
    {
      /*printf("Denominador 0.\n");*/
      vale=FALSE;
    }
  else
	{
	  /*(*angulo)=(numerador/sqrt(denominador));*/
	  (*angulo)=((pow(numerador,2))/(denominador)); 	
	  /*printf("%f\n",(*angulo));*/
	}
  
  return vale;
}   
   
unsigned long int DameTiempo()
{
  struct timeval t; 

  gettimeofday(&t,NULL);
  return t.tv_sec*1000000 + t.tv_usec;	
}   

/*SEGMENTOS LASER */
/*Funciones y procedimientos de segmentación */
int distanciaAlRobot(Tvoxel pto)
{
   return sqrt(pow(pto.x-myencoders[0],2)+pow(pto.y-myencoders[1],2));	
}
float distancia(Tvoxel a, Tvoxel b)
{
  return ((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));	
}
float  segmentosEnDistancia(Tvoxel *a, Tvoxel *b)
{
  float num1,num2,num3;

  num1=(a->x-b->x)*(a->x-b->x);
  num2=(a->y-b->y)*(a->y-b->y);	
  num3=num1+num2;
  
  /*printf("(%f,%f),(%f,%f) ->(%f,%f)\n",a->x,a->y,b->x,b->y,num1,num2);*/
  /*printf("Distancia->%f\n",num3);*/
  
  return ((num3)<(pow(DIST_SEGM2,2)));

}
void inicializarAsignados()
{
  int i;
  for(i=0;i<MAX_SEGM;i++)
    asignados[i]=FALSE;	   
}

int PerteneceRecta(Tvoxel pto, float A , float C, float dist)
{    
  return abs(A * pto.x - pto.y + C) / sqrt(pow(A,2) + 1) <= dist; 	
}

int EnDistancia(Tvoxel p0, Tvoxel p1)
{
  return sqrt( pow(p1.x - p0.x, 2) + pow(p1.y - p0.y, 2) ) <= DIST_PTOS;	
}

void CrearRecta(float *a, float *b) /*Por mínimos cuadrados*/
{
  *a = ( sh.Sum_XY - (1./sh.N) * sh.Sum_Y * sh.Sum_X ) / 
    ( sh.Sum_X2 - (1./sh.N) * pow(sh.Sum_X, 2) ); 	
  
  *b = ( sh.Sum_Y * sh.Sum_X2 - sh.Sum_X * sh.Sum_XY ) /
    ( sh.N * sh.Sum_X2 - pow(sh.Sum_X, 2) );	
}

void InsertaPunto(Tvoxel pto)
{
  sh.N++;
  sh.Sum_XY += (pto.x * pto.y);
  sh.Sum_X2 += (pto.x * pto.x);
  sh.Sum_X += pto.x;
  sh.Sum_Y += pto.y;
}

void InicializaSegmHip()
{
  sh.Sum_XY = sh.Sum_X2 = sh.Sum_X = sh.Sum_Y = sh.N = 0.;	
}
float MagnitudeSquare( XYZ *punto1, XYZ *punto2 )
{
  XYZ Vector;
  
  Vector.X = punto2->X - punto1->X;
  Vector.Y = punto2->Y - punto1->Y;
  Vector.Z = punto2->Z - punto1->Z;
  
  return (float)( Vector.X * Vector.X + Vector.Y * Vector.Y + Vector.Z * Vector.Z );
}

/*int DistancePointLine( XYZ *Point, XYZ *LineStart, XYZ *LineEnd, float *Distance )*/
void distanciaPuntoLinea( TipoSegmento s,Tvoxel p,float *l,float *distancia,Tvoxel *punto)
{
  float LineMag;
  float numerador;
  float U;
  /*float distancia;*/
  XYZ interseccion,p1,p2,p3;
  
  p1.X=s.pI.x;   p1.Y=s.pI.y;   p1.Z=1.;
  p2.X=s.pF.x;   p2.Y=s.pF.y;   p2.Z=1.;
  p3.X=p.x;      p3.Y=p.y;      p3.Z=1.;
  
  LineMag = MagnitudeSquare( &p2, &p1 );
  
  numerador=( ( p3.X - p1.X ) * ( p2.X - p1.X ) +
	      ( p3.Y - p1.Y ) * ( p2.Y - p1.Y ) +
	      ( p3.Z - p1.Z ) * ( p2.Z - p1.Z ) );
  numerador=( p3.X - p1.X ) * ( p2.X - p1.X )+( p3.Y - p1.Y ) *
    ( p2.Y - p1.Y )+( p3.Z - p1.Z ) * ( p2.Z - p1.Z )  ;
  U =  numerador/
    ( LineMag );
  
  /*printf("%f,%f,%f\n",p3.X - p1.X ,p3.Y - p1.Y,p3.Z - p1.Z);*/
  
  interseccion.X = p1.X + U * ( p2.X - p1.X );
  interseccion.Y = p1.Y + U * ( p2.Y - p1.Y );
  interseccion.Z = p1.Z + U * ( p2.Z - p1.Z );
  
  (*punto).x=interseccion.X;
  (*punto).y=interseccion.Y;
  /*asigno el valor de landa*/
  (*l)=U;
  (*distancia)=MagnitudeSquare( &p3, &interseccion );
  /*printf("%f\n",distancia);
  printf("p1->(%f,%f)p2->(%f,%f)\np3->(%f,%f)Interseccion->(%f,%f)\nNumerador->%f,LineMag->%f U->%f\n\n",p1.X,p1.Y,p2.X,p2.Y,p3.X,p3.Y,interseccion.X,interseccion.Y,numerador,LineMag,U);*/
  
}
int paralelasInstantaneos(TipoSegmento s,TipoSegmento s2,float *l1, float *l2,Tvoxel *p1,Tvoxel *p2)
{ 
  
  float dist1,dist2,ang;
  int valido;
  
  distanciaPuntoLinea(s,s2.pI,l1,&dist1,p1);
  distanciaPuntoLinea(s,s2.pF,l2,&dist2,p2);
  valido=anguloSegmTo2(s,s2,&ang);
  /*printf("dist1->%f\ndist2->%f\nlanda3->%f\nlanda4->%f\np1->(%f,%f)\np2->(%f,%f)\n",dist1,dist2,(*l1),(*l2),(*p1).x,(*p1).y,(*p2).x,(*p2).y);*/
  return ((dist1<(DIST_PARALELAS_INSTANTANEOS*DIST_PARALELAS_INSTANTANEOS)) && 
	  (dist2<(DIST_PARALELAS_INSTANTANEOS*DIST_PARALELAS_INSTANTANEOS))&&
	  (valido)&&(ang>=DIFERENCIA_ANGULO));	
  
}

void fusionarSegmentoInstantaneos(float l1,float l2,float l3,float l4,Tvoxel p1,Tvoxel p2,Tvoxel p3,Tvoxel p4,TipoSegmento *s)
{
  /*printf("[ %f , %f , %f , %f ]\n",l1,l2,l3,l4);*/
  /*minima landa*/
  if((l1<=l2)&&(l1<=l3)&&(l1<=l4))
    {
      /*s->pI=memoria_segmentos.segm[pos].pI;*/
      s->pI=p1;
      /*printf("[ %f ,]",l1);*/
    }
  else if ((l2<=l1)&&(l2<=l3)&&(l2<=l4))
    {
      /*s->pI=memoria_segmentos.segm[pos].pF;*/
      s->pI=p2;
      /*printf("[ %f ,",l2);*/
    }
  else if ((l3<=l1)&&(l3<=l2)&&(l3<=l4))
    {
      s->pI=p3;
      /*printf("[ %f ,",l3);*/
		}
  else
    {
      s->pI=p4;
      //printf("[ %f ,",l4);
    }
  
  //maxima landa
  if((l1>=l2)&&(l1>=l3)&&(l1>=l4))
    {
      //s->pF=memoria_segmentos.segm[pos].pI;
      s->pF=p1;
      //printf(" %f ]\n",l1);
    }
  else if ((l2>=l1)&&(l2>=l3)&&(l2>=l4))
    {
      //s->pF=memoria_segmentos.segm[pos].pF;
      s->pF=p2;
      //printf(" %f ]\n",l2);
    }
  else if ((l3>=l1)&&(l3>=l2)&&(l3>=l4))
    {
      s->pF=p3;
      //printf(" %f ]\n",l3);
    }
  else
    {
      s->pF=p4;
      //printf(" %f ]\n",l4);
    }
}

int siEnDistanciaInstantaneos(float l1,float l2,float l3, float l4,Tvoxel p1, Tvoxel p2,Tvoxel p3, Tvoxel p4,int pos)
{
	
  int fusion=FALSE;
  TipoSegmento aux;
  
  
	
  
  if((segmentosEnDistancia(&p1,&p3))||(segmentosEnDistancia(&p1,&p4))||
     (segmentosEnDistancia(&p2,&p3))||(segmentosEnDistancia(&p2,&p4)))
    {
      fusionarSegmentoInstantaneos(l1,l2,l3,l4,p1,p2,p3,p4,&aux);   
      
      segmentos.segm[pos]=aux;  
       fusion=TRUE;  
       
    }
  
  
  return fusion;
}




	

int esProlongacionInstantaneos(TipoSegmento seg,TipoSegmento seg2, int pos)
//int esProlongacion(TipoSegmento seg,int pos)
  
{   
  int fusion;
   float landa1,landa2,landa3,landa4;
   Tvoxel p1,p2,p3,p4;
   TipoSegmento aux;	

   
   fusion=FALSE;
   if(paralelasInstantaneos(seg,seg2,&landa1,&landa2,&p1,&p2))
     
     {   
       
       
       //Damos los valores de landa
       landa3=0.;
       landa4=1.;
       p3=seg.pI;
       p4=seg.pF;
       
       if(((landa1>=landa3)&&(landa1<=landa4))||((landa2>=landa3)&&(landa2<=landa4)))
	 {
	   //fusionarSegmentoInstantaneos(landa1,landa2,landa3,landa4,p1,p2,p3,p4,&aux);   
	   fusionarSegmentoInstantaneos(landa1,landa2,landa3,landa4,seg2.pI,seg2.pF,p3,p4,&aux);   
	   segmentos.segm[pos]=aux;
           fusion=TRUE;		   
	 }
       else if (((landa1<0)&&(landa2>1))||((landa1>1)&&(landa2<0)))
	 {
	   
	   
	   
	   //aux.pI=p1;
	   //aux.pF=p2;
           //segmentos.segm[pos]=aux;
	   segmentos.segm[pos]=seg2;
	   fusion=TRUE;
           
	 }	
       
       
       else
	   {
	     //fusion=siEnDistanciaInstantaneos(landa1,landa2,landa3,landa4,p1,p2,p3,p4,pos);
	     fusionarSegmentoInstantaneos(landa1,landa2,landa3,landa4,seg2.pI,seg2.pF,p3,p4,&aux);   
	   }
     }
   
   
   return fusion;
   
}



void InsertarSegm(Tvoxel p0, Tvoxel p1, int i, int f)
{
  int n = segmentos.tam;
  
  if ( n < MAX_SEGM ) {
    segmentos.segm[n].pI.x = p0.x;
    segmentos.segm[n].pI.y = p0.y; 
    segmentos.segm[n].pF.x = p1.x;
    segmentos.segm[n].pF.y = p1.y; 
    ini[n] = i;
    fin[n] = f;
    
    if ( (n > 0) && esProlongacionInstantaneos(segmentos.segm[n-1], segmentos.segm[n],n-1)){ 
      fin[n-1] = fin[n];
    }else 		
      segmentos.tam++;
  }
}

void laser2xyraw(int reading, float d, Tvoxel *point)
  
/*  Calcula la posicion respecto de sistema de referencia inicial (sistema odometrico) 
    del punto detectado en el sistema de coordenadas solidario al sensor. OJO depende 
    de estructura posiciones y de por el sensor, sabiendo que:
    a) el robot se encuentra en robot[0], robot[1] con orientacion robot[2] respecto 
    al sistema de referencia externo,
    b) que el sensor se encuentra en xsen, ysen con orientacion asen respecto del 
    sistema centrado en el robot apuntando hacia su frente, 
*/ 
{
  float  Xp_sensor, Yp_sensor, Xp_robot, Yp_robot,phi;
  
  phi=-90.+180.*reading/NUM_LASER;
  Xp_sensor = d*cos(DEGTORAD*phi);
  Yp_sensor = d*sin(DEGTORAD*phi);
  Xp_robot = laser_coord[0] + Xp_sensor*laser_coord[3] - Yp_sensor*laser_coord[4];
  Yp_robot = laser_coord[1] + Yp_sensor*laser_coord[3] + Xp_sensor*laser_coord[4];
  /* Coordenadas del punto detectado por el laser con respecto al robot */
  (*point).x = Xp_robot*(myraw_odom)[3] - Yp_robot*(myraw_odom)[4] + (myraw_odom)[0];
  (*point).y = Yp_robot*(myraw_odom)[3] + Xp_robot*(myraw_odom)[4] + (myraw_odom)[1];
  /* Coordenadas del punto con respecto al origen del SdeR */
}

void laser2xyestimate(int reading, float d, Tvoxel *point)

/*  Calcula la posicion respecto de sistema de referencia inicial (sistema odometrico) 
    del punto detectado en el sistema de coordenadas solidario al sensor. 
    OJO depende de estructura posiciones y de por el sensor, sabiendo que:
    a) el robot se encuentra en robot[0], robot[1] con orientacion robot[2] 
    respecto al sistema de referencia externo,
    b) que el sensor se encuentra en xsen, ysen con orientacion asen respecto 
    del sistema centrado en el robot apuntando hacia su frente, 
*/ 
{
  float  Xp_sensor, Yp_sensor, Xp_robot, Yp_robot,phi;
  
  phi=-90.+180.*reading/NUM_LASER;
  Xp_sensor = d*cos(DEGTORAD*phi);
  Yp_sensor = d*sin(DEGTORAD*phi);
  Xp_robot = laser_coord[0] + Xp_sensor*laser_coord[3] - Yp_sensor*laser_coord[4];
  Yp_robot = laser_coord[1] + Yp_sensor*laser_coord[3] + Xp_sensor*laser_coord[4];
  /* Coordenadas del punto detectado por el laser con respecto al robot */
  (*point).x = Xp_robot*(mypos_estimate)[3] - Yp_robot*(mypos_estimate)[4] + (mypos_estimate)[0];
  (*point).y = Yp_robot*(mypos_estimate)[3] + Xp_robot*(mypos_estimate)[4] + (mypos_estimate)[1];
  /* Coordenadas del punto con respecto al origen del SdeR */
}


void segm_laser()
{   
  Tvoxel p1, p0, pInicial;
  float a, b;
  int i=0, inicio, final;
  int ok;
  
  //printf("JDEx->%f,y->%f,a->%f,c->%f,s->%f \n",myencoders[0],myencoders[1],myencoders[2],myencoders[3],myencoders[4]);
  //printf("RAWx->%f,y->%f,a->%f,c->%f,s->%f \n",(myraw_odom)[0],(myraw_odom)[1],(myraw_odom)[2],(myraw_odom)[3],(myraw_odom)[4]);
  
  segmentos.tam = 0;
  
  while(i < NUM_LASER-1) {
    if(mapping_odom==raw)
      {			
	laser2xyraw(i, mylaser[i], &p0);
	laser2xyraw(i+1, mylaser[i+1], &p1);
	//printf("(%f,%f)(%f,%f)\n",p0.x,p0.y,p1.x,p1.y);
      }
    else if(mapping_odom==estimate)
      {
	laser2xyestimate(i, mylaser[i], &p0);
	laser2xyestimate(i+1, mylaser[i+1], &p1); 
	//printf("(%f,%f)(%f,%f)\n",p0.x,p0.y,p1.x,p1.y);
      }
    else
      {	
	laser2xy(i, mylaser[i], &p0,myencoders);
	laser2xy(i+1, mylaser[i+1], &p1,myencoders); 
	//printf("(%f,%f)(%f,%f)\n",p0.x,p0.y,p1.x,p1.y);
      }
    if ( EnDistancia(p0, p1) && mylaser[i]<7800) {
      InicializaSegmHip();
      InsertaPunto(p0);
      InsertaPunto(p1);
      CrearRecta(&a, &b);
      pInicial = p0;
      inicio = i;
      final = i+1;
      ok = TRUE; 
      while( (i < NUM_LASER-1) && ok ) {
	i++;
	if(mapping_odom==raw)
	  {
	    laser2xyraw(i+1, mylaser[i+1], &p0);	
	  }
	else if(mapping_odom==estimate)
	  {
	    laser2xyestimate(i+1, mylaser[i+1], &p0);	
	  }
	else
	  {
	    laser2xy(i+1, mylaser[i+1], &p0,myencoders);
	  }
	if ( ( ok = ( EnDistancia(p0, p1) && 
		      PerteneceRecta(p0, a ,b, DIST_SEGM/sqrt(sh.N)) ) ) ) { 
	  InsertaPunto(p0);
	  CrearRecta(&a, &b);
	  p1 = p0;	
	  final = i+1;
	}              				
      }
      InsertarSegm(pInicial, p1, inicio, final);
    }
    else
      i++;			
  }  
}

//Fin de funciones y procedimientos de segmentación   
//FIN DE SEGMENTOS LASER  

//MEMORIA SEGMENTOS

//Funciones y procedimientos para memoria de segmentos
void inicializarMemoriaSegmentos()
{ 
   int i;
   
   for(i=0;i<MAX_SEGM;i++)
     {	   
      memoria_segmentos.validos[i]=FALSE;
     }
   
}




int paralelas(TipoSegmento s, int pos,float *l1, float *l2,Tvoxel *p1,Tvoxel *p2)
{ 
  
	float dist1,dist2,ang;
	int valido;
	
	distanciaPuntoLinea(s,memoria_segmentos.segm[pos].pI,l1,&dist1,p1);
	distanciaPuntoLinea(s,memoria_segmentos.segm[pos].pF,l2,&dist2,p2);
	valido=anguloSegmTo2(s,memoria_segmentos.segm[pos],&ang);
	//printf("dist1->%f\ndist2->%f\nlanda3->%f\nlanda4->%f\np1->(%f,%f)\np2->(%f,%f)\n",dist1,dist2,(*l1),(*l2),(*p1).x,(*p1).y,(*p2).x,(*p2).y);
	return ((dist1<(DIST_PARALELAS*DIST_PARALELAS)) && (dist2<(DIST_PARALELAS*DIST_PARALELAS))&&
	        (valido)&&(ang>=DIFERENCIA_ANGULO));	
	
}

void fusionarSegmento(float l1,float l2,float l3,float l4,Tvoxel p1,Tvoxel p2,Tvoxel p3,Tvoxel p4,TipoSegmento *s,int pos)
{
  //printf("[ %f , %f , %f , %f ]\n",l1,l2,l3,l4);
  //minima landa
  if((l1<=l2)&&(l1<=l3)&&(l1<=l4))
    {
      //s->pI=memoria_segmentos.segm[pos].pI;
      s->pI=p1;
      //printf("[ %f ,]",l1);
    }
  else if ((l2<=l1)&&(l2<=l3)&&(l2<=l4))
    {
      //s->pI=memoria_segmentos.segm[pos].pF;
      s->pI=p2;
      //printf("[ %f ,",l2);
    }
  else if ((l3<=l1)&&(l3<=l2)&&(l3<=l4))
    {
      s->pI=p3;
      //printf("[ %f ,",l3);
    }
  else
    {
      s->pI=p4;
      //printf("[ %f ,",l4);
    }
  
  //maxima landa
  if((l1>=l2)&&(l1>=l3)&&(l1>=l4))
    {
      //s->pF=memoria_segmentos.segm[pos].pI;
      s->pF=p1;
      //printf(" %f ]\n",l1);
    }
  else if ((l2>=l1)&&(l2>=l3)&&(l2>=l4))
    {
      //s->pF=memoria_segmentos.segm[pos].pF;
      s->pF=p2;
      //printf(" %f ]\n",l2);
    }
  else if ((l3>=l1)&&(l3>=l2)&&(l3>=l4))
    {
      s->pF=p3;
      //printf(" %f ]\n",l3);
    }
  else
    {
      s->pF=p4;
      //printf(" %f ]\n",l4);
    }
}

int siEnDistancia(TipoSegmento seg ,float l1,float l2,float l3, float l4,Tvoxel p1, Tvoxel p2,Tvoxel p3, Tvoxel p4,int pos,int *colocado, int *pos_colocado)
{
  
  int fusion=FALSE;
  TipoSegmento aux;
  
  
	
  
  if((segmentosEnDistancia(&p1,&p3))||(segmentosEnDistancia(&p1,&p4))||
     (segmentosEnDistancia(&p2,&p3))||(segmentosEnDistancia(&p2,&p4)))
	{
	  fusionarSegmento(l1,l2,l3,l4,p1,p2,p3,p4,&aux,pos);   
	  
	  
	  if(*colocado)
	   {
	     memoria_segmentos.validos[pos]=FALSE;
	   }    
       else
	 {
	   (*colocado)=TRUE;
	   (*pos_colocado)=pos;			   
	 }
	  
	   memoria_segmentos.segm[*pos_colocado]=aux; 
	   memoria_segmentos.validos[*pos_colocado]=TRUE;
	   memoria_segmentos.hora_inicio[*pos_colocado]=DameTiempo();
	   fusion=TRUE;
	   //printf("SEGMENTO NUEVO ->(%f,%f)(%f,%f)\n",aux.pI.x,aux.pI.y,aux.pF.x,aux.pF.y); 	   
	   //printf("2izq o 2der\n");  
	   
	   
	}
  
  
  return fusion;
}



	
	

int esProlongacion(TipoSegmento seg,int pos,int *colocado,int *pos_colocado)
//int esProlongacion(TipoSegmento seg,int pos)

{   
  int fusion;
  float landa1,landa2,landa3,landa4;
  Tvoxel p1,p2,p3,p4;
  TipoSegmento aux;	
  
  
  fusion=FALSE;
  if(paralelas(seg,pos,&landa1,&landa2,&p1,&p2))
    
    {   
      /*Si son paralelas obtenemos los puntos de proyección sobre la recta 
	a la cual pertenece el segmentos y calculamos si se pueden fusionar 
	segun el valor de landa (ecuacion parametrica de la recta) para estos puntos
	 
	Para calcular los puntos de proyección sobre la recta calculamos el punto de corte de dos rectas:
	1-La recta a la que pertenece el segmentos
	2-La recta perpendicular a la primera que pasa por el punto que 
	delimita el segmento guardado en la memoria*/
	   
      //printf("Son paralelas.\n");
      //printf("landa3->%f\nlanda4->%f\np1->(%f,%f)\np2->(%f,%f)\n",landa3,landa4,p1.x,p1.y,p2.x,p2.y);	   
      
      //Damos los valores de landa
      landa3=0.;
      landa4=1.;
      p3=seg.pI;
      p4=seg.pF;
      //printf("%f\n%f\n%f\n%f\n\n",landa1,landa2,landa3,landa4);
      
      //Miramos si se puede fusionar
      
      /*printf("LANDAS\n l1->%f\n l2->%f\n",landa1,landa2);
	printf("SEGMENTO NUEVO ->(%f,%f)(%f,%f)\n",p3.x,p3.y,p4.x,p4.y);
	printf("PROYECCIONES   ->(%f,%f)(%f,%f)\n",p1.x,p1.y,p2.x,p2.y);
	printf("PUNTOS NUEVOS\n");*/
      if(((landa1>=landa3)&&(landa1<=landa4))||((landa2>=landa3)&&(landa2<=landa4)))
	{
	  fusionarSegmento(landa1,landa2,landa3,landa4,p1,p2,p3,p4,&aux,pos);   
	  
	  if(*colocado)
	    {
	      memoria_segmentos.validos[pos]=FALSE;
	    }  
	  else
	    {
	      (*colocado)=TRUE;
              (*pos_colocado)=pos;			   
		   }
	  memoria_segmentos.segm[*pos_colocado]=aux; 
	  memoria_segmentos.validos[*pos_colocado]=TRUE;
	  memoria_segmentos.hora_inicio[*pos_colocado]=DameTiempo();
	  fusion=TRUE;
	  //printf("2 o 1 medio\n");
	  //printf("(%f,%f)(%f,%f)\n\n",seg.pI.x,seg.pI.y,seg.pF.x,seg.pF.y);		   
	   }
      else if (((landa1<0)&&(landa2>1))||((landa1>1)&&(landa2<0)))
	{
	  
	  
	  
	  aux.pI=p1;
	  aux.pF=p2;
	  //aux.pI=memoria_segmentos.segm[pos].pI;
	  //aux.pF=memoria_segmentos.segm[pos].pF;
	  
	  if(*colocado)
	    {
	      memoria_segmentos.validos[pos]=FALSE;
	    }  
           else
	     {
	       (*colocado)=TRUE;
	       (*pos_colocado)=pos;			   
	     }		   
	  
	  memoria_segmentos.segm[*pos_colocado]=aux; 
	  memoria_segmentos.validos[*pos_colocado]=TRUE;
	  memoria_segmentos.hora_inicio[*pos_colocado]=DameTiempo(); 
	  fusion=TRUE;		
	  //printf("1izq 1der\n");
	  //printf("LANDAS [ %f , %f , %f , %f ]\n[ %f , %f ]\n",l1,l2,l3,l4,l1,l2);	   
	  //printf("SEGMENTO NUEVO ->(%f,%f)(%f,%f)\n",aux.pI.x,aux.pI.y,aux.pF.x,aux.pF.y);
	}	
      
      
      else
	{
	  fusion=siEnDistancia(seg,landa1,landa2,landa3,landa4,p1,p2,p3,p4,pos,colocado,pos_colocado);
	  //fusion=FALSE;
	  
	   }
    }
  
  
  return fusion;
  
}
int posicionMasVieja(TipoMemoriaSegmentos *mem_segm)
{
  //Cuando llamamos a esta funcion todas las posiciones de la memoria estan ocupadas y hay que buscar 
  //la posicion del segmento mas viejo
  int i;
  int mas_vieja;
  int novacia;
  
  novacia=TRUE;
  mas_vieja=0;
  if(!(*mem_segm).validos[mas_vieja])
    {
      novacia=FALSE;	
    }
  
  i=1;
  while((i<MAX_SEGM)&& (novacia))
    {
      if(!(*mem_segm).validos[i])
	{
	  mas_vieja=i;
	  novacia=FALSE;
	}
      
      else if((*mem_segm).hora_inicio[i]<(*mem_segm).hora_inicio[mas_vieja])
	{
	  mas_vieja=i;	
	}
      i++;
    }
  
  return mas_vieja;
}
void olvidoTemporal()
{
  int i;
  int hora;	
  
  //Solo se eliminan los segmentos por tiempo de vida si se activa en la gui
  
  for(i=0;i<MAX_SEGM;i++)	
    {	   
      if(memoria_segmentos.validos[i])
	{   
	  hora=DameTiempo();
	  if(hora-memoria_segmentos.hora_inicio[i]>EXISTENCIA_MEMORIA_SEGMENTOS)
	    {
	      memoria_segmentos.validos[i]=FALSE;   
	    }			   
	}
      
    }    
}
void reemplazar(TipoSegmento so)
{
  int i,mas_viejo;
  int colocado,colocado2;
  int pos_colocado;
  TipoSegmento aux;
  
  colocado=FALSE;
  colocado2=FALSE;
  aux=so;
  pos_colocado=0;
  mas_viejo=0;
  i=0;
  //while((!colocado) && (i<MAX_SEGM))
  for(i=0;i<MAX_SEGM;i++)	
    {
      
      //if((memoria_segmentos.validos[i])&&(!colocado))
      if((memoria_segmentos.validos[i])&&(!colocado2))	
	//if((memoria_segmentos.validos[i]))	
	{			
	  colocado2=esProlongacion(aux,i,&colocado,&pos_colocado);
	  //if(colocado2)
	  //	aux=memoria_segmentos.segm[pos_colocado];
	  
	}		
      //i++;
      
    }
  
	
  if(!colocado)
    {		
      //printf("Nuevo\n");
      mas_viejo=posicionMasVieja(&memoria_segmentos);		
      memoria_segmentos.segm[mas_viejo]=so; 
      memoria_segmentos.validos[mas_viejo]=TRUE;
      memoria_segmentos.hora_inicio[mas_viejo]=DameTiempo();
      
    }
}

//Eliminar incoherencias con geometria plana
TipoSegmento obtenerSegm(int pos)
{
  
  Tvoxel aux;
  TipoSegmento s;
  
  //Obtengo angulo respecto al robot
  if(mapping_odom==raw)
	{
	  laser2xyraw(pos, mylaser[pos]-MARGEN_RAYO_LASER, &aux);
	  
	  s.pI.x=(myraw_odom)[0];
	  s.pI.y=(myraw_odom)[1];
	  s.pF=aux;
	}
  else if(mapping_odom==estimate)
    {
      laser2xyestimate(pos, mylaser[pos]-MARGEN_RAYO_LASER, &aux);
      
      s.pI.x=(mypos_estimate)[0];
      s.pI.y=(mypos_estimate)[1];
      s.pF=aux;
    }
	else
	  {
	    laser2xy(pos, mylaser[pos]-MARGEN_RAYO_LASER, &aux,myencoders);
	    
	    s.pI.x=myencoders[0];
	    s.pI.y=myencoders[1];
	    s.pF=aux;
    }
  
  return s;
}
void obtenerRecta(XYZ *recta,TipoSegmento seg)
{
  Tvoxel p1,p2;
	
  p1=seg.pI;
  p2=seg.pF;
  
  /*recta->X=p2.y*p1.x-p1.y*p1.x;
    recta->Y=(p2.x-p1.x)*p1.x;
    recta->Z=(p1.y*p2.x-p2.y*p1.x)*p1.x;*/
  recta->X=p2.y-p1.y;
  recta->Y=-(p2.x-p1.x);
  recta->Z=(p1.y*p2.x-p2.y*p1.x);
  //printf("Recta->(%f,%f,%f)\n",recta->X,recta->Y,recta->Z);
}

void obtenerPuntoComun(XYZ recta1,XYZ recta2, XYZ *punto)
{
    punto->X=recta1.Y*recta2.Z-recta1.Z*recta2.Y;
    punto->Y=recta1.Z*recta2.X-recta1.X*recta2.Z;
    punto->Z=recta1.X*recta2.Y-recta1.Y*recta2.X;
    
    //printf("Punto->(%f,%f,%f)\n",punto->X,punto->Y,punto->Z);	
}
int noCercaExtremo(TipoSegmento s1, TipoSegmento s2)
{	
  float dist1,dist2;
  float l,s;
  Tvoxel t,r;
  //s1 el el segmento robot-puntolaser.
  //s2 el segmento de la memoria
	//Se mira cuando se cortan ambos segmentos si los extremos del segmento guardado en memoria
  //estan del segmento laser a mayor distancia de una dada. Esto se hace para no eliminar segmentos
  //cuyo punto de corte este muy proximo a algun extremo
  
  distanciaPuntoLinea(s1,s2.pI,&l,&dist1,&t);
  distanciaPuntoLinea(s1,s2.pF,&s,&dist2,&r);
  
  return ((dist1>(DIST_RAYOLASER*DIST_RAYOLASER)) && (dist2>(DIST_RAYOLASER*DIST_RAYOLASER)));
}
/*int noCercaExtremo(TipoSegmento s1, TipoSegmento s2, XYZ p)
  {
  float dist1,dist2;
	
  dist1=pow((p.X-s2.pI.x),2)+pow((p.Y-s2.pI.y),2);
  dist2=pow((p.X-s2.pF.x),2)+pow((p.Y-s2.pF.y),2);
  //printf("%f,%f\n",dist1,dist2);
	return ((dist1>50*50)&&(dist2>50*50));
	}*/
int intersectan(TipoSegmento s1, TipoSegmento s2, XYZ p)
{
  Tvoxel p1,p2,t1,t2;
  
  p1=s1.pI;
  p2=s1.pF;
  t1=s2.pI;
  t2=s2.pF;
  
  //Como hemos normalizado el punto antes de comprobar se los segmentos se intersectan,
	//obviamos las z ya que son 1.
  
  return ((((p.X-p1.x)*(p.X-p2.x)+(p.Y-p1.y)*(p.Y-p2.y))<=0)&&
	  (((p.X-t1.x)*(p.X-t2.x)+(p.Y-t1.y)*(p.Y-t2.y))<=0)&&
	  (noCercaExtremo(s1,s2)));
}
void normalizar(XYZ *p)
{
	if( p->Z != 0 )
	  {
	    p->X=p->X/p->Z;
	    p->Y=p->Y/p->Z;
	    p->Z=1.;
	    //printf("Normalizado -> (%f,%f,%f)\n",p->X,p->Y,p->Z);
	  }
}
float obtenerLanda(TipoSegmento aux, XYZ pComun)
{
  float LineMag;
  float numerador;
  float U;
  //float distancia;
  XYZ p1,p2,p3;
  
  p1.X=aux.pI.x;   p1.Y=aux.pI.y;   p1.Z=1.;
  p2.X=aux.pF.x;   p2.Y=aux.pF.y;   p2.Z=1.;
  p3.X=pComun.X;   p3.Y=pComun.Y;   p3.Z=1.;
  
  LineMag = MagnitudeSquare( &p2, &p1 );
  
  /*numerador=( ( p3.X - p1.X ) * ( p2.X - p1.X ) +
          	    ( p3.Y - p1.Y ) * ( p2.Y - p1.Y ) +
	            ( p3.Z - p1.Z ) * ( p2.Z - p1.Z ) );*/
  numerador=( p3.X - p1.X ) * ( p2.X - p1.X )+( p3.Y - p1.Y ) * ( p2.Y - p1.Y )+( p3.Z - p1.Z ) * ( p2.Z - p1.Z )  ;
  U =  numerador/
    ( LineMag );
  
  //printf("%f,%f,%f\n",p3.X - p1.X ,p3.Y - p1.Y,p3.Z - p1.Z);
   
  
  //asigno el valor de landa
  return U;
  
}
int fragmentar(TipoSegmento aux,XYZ pComun,int i,TipoMemoriaSegmentos *mem_segm)
{
  
  float landa,inc_izq,inc_der,dist,mi_distancia;
  float lim_izq,lim_der;
  Tvoxel p;
  TipoSegmento seg;
  int pos,elimino;
  XYZ p1,p2;
  
  elimino=TRUE;
  landa=obtenerLanda(aux,pComun);
  //printf("LANDA->%f\n",landa);
  p1.X=aux.pI.x;   p1.Y=aux.pI.y;   p1.Z=1.;
  p2.X=aux.pF.x;   p2.Y=aux.pF.y;   p2.Z=1.;
  mi_distancia=MagnitudeSquare(&p1,&p2);
  mi_distancia=sqrt(mi_distancia);
  dist=mi_distancia*landa;
  
  /*p.x = aux.pI.x + landa * ( aux.pF.x - aux.pI.x );
    p.y = aux.pI.y + landa * ( aux.pF.y - aux.pI.y );
    
    printf("(%f,%f)\n(%F,%f)\n",pComun.X,pComun.Y,p.x,p.y);*/
  
  
  inc_izq=dist-INCREMENTO_FRAGMENTACION;
  inc_der=dist+INCREMENTO_FRAGMENTACION;
  
  lim_izq=inc_izq/mi_distancia;
  lim_der=inc_der/mi_distancia;
  //Primero elimino el anterior para que si no hay fragmentos que todavia persistan se borre el segmento
  (*mem_segm).validos[i]=FALSE;
  
  pos=i;
  if(lim_izq > 0.)
    {  	
      p.x = aux.pI.x + lim_izq * ( aux.pF.x - aux.pI.x );
      p.y = aux.pI.y + lim_izq * ( aux.pF.y - aux.pI.y );
      seg.pI=aux.pI;
      seg.pF=p;
      if(distancia(seg.pI,seg.pF)>1500*1500)
	{
	  (*mem_segm).segm[pos]=seg;
          (*mem_segm).validos[pos]=TRUE;
          (*mem_segm).hora_inicio[pos]=DameTiempo();
          pos=posicionMasVieja(mem_segm);	
          elimino=FALSE;
	}		
    }
  
  if(lim_der < 1.)
    {  	
      p.x = aux.pI.x + lim_der * ( aux.pF.x - aux.pI.x );
      p.y = aux.pI.y + lim_der * ( aux.pF.y - aux.pI.y );
      seg.pI=p;
      seg.pF=aux.pF;
      if(distancia(seg.pI,seg.pF)>1500*1500)
	{
	  (*mem_segm).segm[pos]=seg;
          (*mem_segm).validos[pos]=TRUE;
          (*mem_segm).hora_inicio[pos]=DameTiempo();
          elimino=FALSE;
	   }		   
    }
  
  return elimino;
}

int eliminarIncoherencias(int pos)
{
  
  int elimina=FALSE;
  int i;
  XYZ recta1,recta2,pComun;
  TipoSegmento aux;
  
  //Primero obtenemos la recta laser
  segmentosLaser[pos]=obtenerSegm(pos);
  obtenerRecta(&recta1,segmentosLaser[pos]);
  
  //Para todos los segmentos de la memoria, si intersectan con este segmento
  //lo eliminamos
  for(i=0;i<MAX_SEGM;i++)
    {
      if(memoria_segmentos.validos[i])
	{
	  aux=memoria_segmentos.segm[i];
	  obtenerRecta(&recta2,aux);
	  obtenerPuntoComun(recta1,recta2,&pComun);
	  if (pComun.Z!=0)
	    {
	      //Normalizamos el punto para obviar las Z
	      normalizar(&pComun);
	      if(intersectan(segmentosLaser[pos],aux,pComun))
		{   
		  if((distancia(aux.pI,aux.pF))>(3000*3000))
		    {
		      //printf("SEGMENTO GRANDE\n");
		      elimina=fragmentar(aux,pComun,i,&memoria_segmentos);	
					}
		  else
		    {
		      memoria_segmentos.validos[i]=FALSE;
		      elimina=TRUE;
		    }
		}
	    }
	}
    }
  
  return elimina;
}


//Fin de eliminar incoherencias con geometria plana

void memoriaSegmentos()
{
  int i;
	
  for(i=0;i<NUM_LASER;i++)
    {
      
      val[i]=eliminarIncoherencias(i);	
		   //val[i]=TRUE;
    }
  for(i=0;i<segmentos.tam;i++)
    {   
      if(abs(*myw)<w_pos_max)	
	reemplazar(segmentos.segm[i]);
      
    }
  //printf("MAP -> %f\n",memoria_segmentos.segm[0].pI.x);
  
  
}

//Fin de funciones y procedimientos para memoria de segmentos

//FIN MEMORIA SEGMENTOS


void mapping_iteration()
{  
 

  speedcounter(mapping_id);
  /* printf("mapping iteration %d\n",d++);*/
  
  
  //printf("DIRECCION en MAP-> %f,%f\n",(myraw_odom)[0],(myraw_odom)[1]);
  //printf("DIRECCION en MAP-> %f,%f\n",(mypos_estimate)[0],(mypos_estimate)[1]);	
  if((*myencontrada_pos_real)||(mapping_odom!=estimate))
    {   //printf("entro\n");  	
      segm_laser();	
      memoriaSegmentos();
	  if (olvido)
	    {
	      olvidoTemporal();  
	    }  
    }
}


void mapping_stop()
{
  pthread_mutex_lock(&(all[mapping_id].mymutex));
  put_state(mapping_id,slept);
  all[mapping_id].children[(*(int *)myimport("laser","id"))]=FALSE;
  all[mapping_id].children[(*(int *)myimport("motors","id"))]=FALSE;
  all[mapping_id].children[(*(int *)myimport("encoders","id"))]=FALSE;	
  lasersuspend();
  encoderssuspend();
  motorssuspend();
  printf("mapping: off\n");
  pthread_mutex_unlock(&(all[mapping_id].mymutex));
}

void localization_terminate()
{

}

void mapping_run(int father, int *brothers, arbitration fn)
{
  int i;
 
  pthread_mutex_lock(&(all[mapping_id].mymutex));
  /* this schema resumes its execution with no children at all */
  for(i=0;i<MAX_SCHEMAS;i++) all[mapping_id].children[i]=FALSE;
  
  all[mapping_id].father=father;
  if (brothers!=NULL)
    {
      for(i=0;i<MAX_SCHEMAS;i++) mapping_brothers[i]=-1;
      i=0;
      while(brothers[i]!=-1) {mapping_brothers[i]=brothers[i];i++;}
    }
  mapping_callforarbitration=fn;
  put_state(mapping_id,notready);
  
  myencontrada_pos_real=(int *)myimport("localization","encontrada_pos_real");	 
  myraw_odom=(float *)myimport("localization","raw_odom");	
  mypos_estimate=(float *)myimport("localization","pose_estimate");	
  
  mylaser=(int *)myimport("laser","laser");
  laserresume=(runFn)myimport("laser","resume");
  lasersuspend=(stopFn *)myimport("laser","suspend");
  
  myencoders=(float *)myimport("encoders","jde_robot");
  encodersresume=(runFn)myimport("encoders","resume");
  encoderssuspend=(stopFn)myimport("encoders","suspend");
  
  myv=(float *)myimport("motors","v");
  myw=(float *)myimport("motors","w");
  motorsresume=(runFn)myimport("motors","resume");
  motorssuspend=(stopFn)myimport("motors","suspend");

  printf("mapping: on\n");
  pthread_cond_signal(&(all[mapping_id].condition));
  pthread_mutex_unlock(&(all[mapping_id].mymutex));
}

void *mapping_thread(void *not_used) 
{
  struct timeval a,b;
  long diff, next;

  for(;;)
    {
      pthread_mutex_lock(&(all[mapping_id].mymutex));

      if (all[mapping_id].state==slept) 
	{
/*esto	  *myv=0; *myw=0;*/
	  pthread_cond_wait(&(all[mapping_id].condition),&(all[mapping_id].mymutex));
	  pthread_mutex_unlock(&(all[mapping_id].mymutex));
	}
      else 
	{
	  /* check preconditions. For now, preconditions are always satisfied*/
	  if (all[mapping_id].state==notready) put_state(mapping_id,ready);
	  else if (all[mapping_id].state==ready)	  
	    /* check brothers and arbitrate. For now this is the only winner */
	    { 
	      put_state(mapping_id,winner);
	      /*esto	      *myv=0; *myw=0; */
	      /*esto	      v_teleop=0; w_teleop=0;*/
	      /* start the winner state from controlled motor values */ 
	      all[mapping_id].children[(*(int *)myimport("laser","id"))]=TRUE;
	      all[mapping_id].children[(*(int *)myimport("motors","id"))]=TRUE;
	      all[mapping_id].children[(*(int *)myimport("encoders","id"))]=TRUE;		
	      laserresume(mapping_id,NULL,NULL);
	      encodersresume(mapping_id,NULL,NULL);
	      motorsresume(mapping_id,NULL,NULL);
	    }	  
	  else if (all[mapping_id].state==winner);

	  if (all[mapping_id].state==winner)
	    /* I'm the winner and must execute my iteration */
	    {
	      pthread_mutex_unlock(&(all[mapping_id].mymutex));
	      gettimeofday(&a,NULL);
	      mapping_iteration();
	      gettimeofday(&b,NULL);  

	      diff = (b.tv_sec-a.tv_sec)*1000000+b.tv_usec-a.tv_usec;
	      next = mapping_cycle*1000-diff-10000; 
	      if (next>0) 
		/* discounts 10ms taken by calling usleep itself */
		usleep(mapping_cycle*1000-diff);
	      else 
		/* just let this iteration go away. overhead time negligible */
		{printf("time interval violated: mapping\n"); 
		usleep(mapping_cycle*1000);
		}
	    }
	  else 
	    {
	      pthread_mutex_unlock(&(all[mapping_id].mymutex));
	      usleep(mapping_cycle*1000);
	    }
	}
    }
}

void mapping_init()
{
  pthread_mutex_lock(&(all[mapping_id].mymutex));
  myexport("mapping","memoria_segmentos",&memoria_segmentos);	
  myexport("mapping","id",&mapping_id);
  myexport("mapping","run",(void *) &mapping_run);
  myexport("mapping","stop",(void *) &mapping_stop);
  printf("mapping schema started up\n");
  put_state(mapping_id,slept);
  pthread_create(&(all[mapping_id].mythread),NULL,mapping_thread,NULL);
  mapping_init_aux();
  pthread_mutex_unlock(&(all[mapping_id].mymutex));
  mapping_odom=estimate;
	
	
  int i;	
  int hora;

  /*mapping_state=teleoperator;	*/
  /*mapping_state=vff;*/
  /*SEGMENTOS LASER*/
  num_segm=0;
  for(i=0;i<NUM_LASER;i++)
  {
	val_pintados[i]=FALSE;  
  }	  
  //FIN SEGMENTOS LASER	
  //MEMORIA SEGMENTOS LASER
  num_segm_memory=0;
  num_segm_laser=0;	
  for(i=0;i<MAX_SEGM;i++)
      memoria_segmentos.hora_inicio[i]=hora;
  inicializarMemoriaSegmentos();	
  //FIN MEMORIA DE SEGMENTOS LASER	
  
}

void mapping_guibuttons(void *obj1)
{
  FL_OBJECT *obj=(FL_OBJECT *)obj1;
 
  if (obj == fd_mappinggui->exit) jdeshutdown(0);
  else if (obj == fd_mappinggui->escala)  
    {  mapping_rango=fl_get_slider_value(fd_mappinggui->escala);
      mapping_visual_refresh = TRUE; /* activa el flag que limpia el fondo de la pantalla y repinta todo */ 
      mapping_escala = mapping_width /mapping_rango;}
  else if (obj == fd_mappinggui->track_robot) 
    {if (fl_get_button(obj)==PUSHED) mapping_trackrobot=TRUE;
      else mapping_trackrobot=FALSE;
    } 
  else if (obj== fd_mappinggui->center)
    /* Se mueve 10%un porcentaje del rango */
    {
      mapping_odometrico[0]+=mapping_rango*(fl_get_positioner_xvalue(fd_mappinggui->center)-0.5)*(-2.)*(0.1);
      mapping_odometrico[1]+=mapping_rango*(fl_get_positioner_yvalue(fd_mappinggui->center)-0.5)*(-2.)*(0.1);
      fl_set_positioner_xvalue(fd_mappinggui->center,0.5);
      fl_set_positioner_yvalue(fd_mappinggui->center,0.5);
      mapping_visual_refresh=TRUE;  }
  
  else if (obj == fd_mappinggui->vissegment)
    {
      if (fl_get_button(fd_mappinggui->vissegment)==RELEASED)
	{mapping_display_state = mapping_display_state & ~DISPLAY_SEGMENT;
	  visual_delete_mapping_segment=TRUE;
	}
      else 
	mapping_display_state=mapping_display_state | DISPLAY_SEGMENT;
      
    }	
  else if (obj == fd_mappinggui->vissegmentmemory)
    {
      if (fl_get_button(fd_mappinggui->vissegmentmemory)==RELEASED)
	   {mapping_display_state = mapping_display_state & ~DISPLAY_SEGMENT_MEMORY;
	     visual_delete_mapping_segment_memory=TRUE;
	   }
      else 
	mapping_display_state=mapping_display_state | DISPLAY_SEGMENT_MEMORY;
      
    }
  
  else if (obj == fd_mappinggui->vislasersegment)
    {
      if (fl_get_button(fd_mappinggui->vislasersegment)==RELEASED)
	{mapping_display_state = mapping_display_state & ~DISPLAY_LASER_SEGMENT;
	  visual_delete_mapping_laser_segment=TRUE;
	}
      else 
	mapping_display_state=mapping_display_state | DISPLAY_LASER_SEGMENT;

    }
  
  else if (obj == fd_mappinggui->pb_olvido)
    {
      olvido=(fl_get_button(fd_mappinggui->pb_olvido)==PUSHED); 		   
    }
  else if (obj == fd_mappinggui->breal_odom)
    {
      if (fl_get_button(fd_mappinggui->breal_odom)==PUSHED) mapping_odom=real;
    }
  else if (obj == fd_mappinggui->bestimate_odom)
    {
      if (fl_get_button(fd_mappinggui->bestimate_odom)==PUSHED) mapping_odom=estimate;
    }	
  else if (obj == fd_mappinggui->braw_odom)
    {
      if (fl_get_button(fd_mappinggui->braw_odom)==PUSHED) mapping_odom=raw;
    }	
}

void mapping_guidisplay()
{
  char text[80]="";
  static float k=0;
  int i;
  Tvoxel aa;
  Tvoxel kaka;	
  static XPoint targetgraf;
  
  //fl_redraw_object(fd_mappinggui->joystick);
  
  /* slow refresh of the complete mapping gui, needed because incremental refresh misses window occlusions */
  if (mapping_iteracion_display*mapping_cycle>FORCED_REFRESH) 
    {mapping_iteracion_display=0;
      mapping_visual_refresh=TRUE;
    }
  else mapping_iteracion_display++;
  
  
  k=k+1.;
  sprintf(text,"%.1f",k);
  fl_set_object_label(fd_mappinggui->fps,text);
  
  fl_winset(mapping_canvas_win); 
  
  
  if ((mapping_trackrobot==TRUE)&&
      ((fabs(myencoders[0]+mapping_odometrico[0])>(mapping_rango/4.))||
       (fabs(myencoders[1]+mapping_odometrico[1])>(mapping_rango/4.))))
    {mapping_odometrico[0]=-myencoders[0];
      mapping_odometrico[1]=-myencoders[1];
      mapping_visual_refresh = TRUE;
    }
  
  
  if (mapping_visual_refresh==TRUE)
    {
      fl_rectbound(0,0,mapping_width,mapping_height,FL_WHITE);   
      XFlush(mydisplay);
    }
  
  /* the grid at the floor */
/*  fl_set_foreground(mappinggui_gc,FL_LEFT_BCOL); 
    for(i=0;i<31;i++)
    {
    aa.x=-15000.+(float)i*1000;
    aa.y=-15000.;
    bb.x=-15000.+(float)i*1000;
    bb.y=15000.;
    xy2mappingcanvas(aa,&a);
    xy2mappingcanvas(bb,&b);
    XDrawLine(display,mapping_canvas_win,mappinggui_gc,a.x,a.y,b.x,b.y);
    aa.y=-15000.+(float)i*1000;
    aa.x=-15000.;
    bb.y=-15000.+(float)i*1000;
    bb.x=15000.;
    xy2mappingcanvas(aa,&a);
    xy2mappingcanvas(bb,&b);
    XDrawLine(display,mapping_canvas_win,mappinggui_gc,a.x,a.y,b.x,b.y);
    }*/
  /* fl_set_foreground(mappinggui_gc,FL_RIGHT_BCOL); */
  
  
  
  /* VISUALIZACION de una instantanea ultrasonica **
     if ((((mapping_display_state&DISPLAY_SONARS)!=0)&&(mapping_visual_refresh==FALSE))
      || (visual_delete_mapping_us==TRUE))
      {  
      fl_set_foreground(mappinggui_gc,FL_WHITE); 
      ** clean last sonars, but only if there wasn't a total refresh. In case of total refresh the white rectangle already cleaned all **
      for(i=0;i<NUM_SONARS*2;i+=2) XDrawLine(display,mapping_canvas_win,mappinggui_gc,mapping_us_dpy[i].x,mapping_us_dpy[i].y,mapping_us_dpy[i+1].x,mapping_us_dpy[i+1].y);
      
      }
  
      if ((mapping_display_state&DISPLAY_SONARS)!=0){
    for(i=0;i<NUM_SONARS;i++)
      {us2xy(i,0.,0.,&aa); ** Da en el Tvoxel aa las coordenadas del sensor, pues es distancia 0 **
      xy2mappingcanvas(aa,&mapping_us_dpy[2*i]);
      us2xy(i,us[i],0.,&aa);
      xy2mappingcanvas(aa,&mapping_us_dpy[2*i+1]);
      }
    fl_set_foreground(mappinggui_gc,FL_PALEGREEN);
    for(i=0;i<NUM_SONARS*2;i+=2) XDrawLine(display,mapping_canvas_win,mappinggui_gc,mapping_us_dpy[i].x,mapping_us_dpy[i].y,mapping_us_dpy[i+1].x,mapping_us_dpy[i+1].y);
  }
  */
  
  /* VISUALIZACION de una instantanea laser*/
  /*if ((((mapping_display_state&DISPLAY_LASER)!=0)&&(mapping_visual_refresh==FALSE))
      || (visual_delete_mapping_laser==TRUE))
    {  
      fl_set_foreground(mappinggui_gc,FL_WHITE); 
      // clean last laser, but only if there wasn't a total refresh. In case of total refresh the white rectangle already cleaned all 
      //for(i=0;i<NUM_LASER;i++) XDrawPoint(display,mapping_canvas_win,mappinggui_gc,mapping_laser_dpy[i].x,mapping_laser_dpy[i].y);
      XDrawPoints(display,mapping_canvas_win,mappinggui_gc,mapping_laser_dpy,NUM_LASER,CoordModeOrigin);
    }
  
  if ((mapping_display_state&DISPLAY_LASER)!=0){
    for(i=0;i<NUM_LASER;i++)
      {
	laser2xy(i,mylaser[i],&aa);
	xy2mappingcanvas(aa,&mapping_laser_dpy[i]);
      }
    fl_set_foreground(mappinggui_gc,FL_BLUE);
    //for(i=0;i<NUM_LASER;i++) XDrawPoint(display,mapping_canvas_win,mappinggui_gc,mapping_laser_dpy[i].x,mapping_laser_dpy[i].y);
    XDrawPoints(display,mapping_canvas_win,mappinggui_gc,mapping_laser_dpy,NUM_LASER,CoordModeOrigin);
  }*/
  
  
    /* VISUALIZACION de una instantanea de MEMORIA DE PUNTOS*/
  
	
   /* VISUALIZACION de una instantanea laser*/
  if (((((mapping_display_state&DISPLAY_LASER)!=0)&&(mapping_visual_refresh==FALSE))
      || (visual_delete_mapping_laser==TRUE))&&
      ((((mapping_display_state&DISPLAY_LASER_SEGMENT)!=0)&&(mapping_visual_refresh==FALSE))
      || (visual_delete_mapping_laser_segment==TRUE))) 
    {  
      fl_set_foreground(mappinggui_gc,FL_WHITE); 
      /* clean last laser, but only if there wasn't a total refresh. In case of total refresh the white rectangle already cleaned all */
      /*for(i=0;i<NUM_LASER;i++) XDrawPoint(display,mapping_canvas_win,mappinggui_gc,mapping_laser_dpy[i].x,mapping_laser_dpy[i].y);*/
      XDrawPoints(mydisplay,mapping_canvas_win,mappinggui_gc,mapping_laser_dpy,NUM_LASER,CoordModeOrigin);
    
	  //SEGMENTOS LASER	 
	  //Primero borro los segmentos laser anteriores
	   //printf("SEGMENTOS ->%d\n",segmentos.tam);	
	   fl_set_foreground(mappinggui_gc,FL_WHITE);
 	
	     for(i=0;i<NUM_LASER;i++)
	     {	
           if(val_pintados[i])			 
		     XDrawLine(mydisplay,mapping_canvas_win,mappinggui_gc,coord_graf_segment_laser[i].pI.x,coord_graf_segment_laser[i].pI.y,coord_graf_segment_laser[i].pF.x,coord_graf_segment_laser[i].pF.y);
		
	     }	 
	  //FIN SEGMENTOS LASER	
	}		
	

  
  
  
  if (((mapping_display_state&DISPLAY_LASER)!=0)&&
      ((mapping_display_state&DISPLAY_LASER_SEGMENT)!=0)){
    for(i=0;i<NUM_LASER;i++)
      {
	laser2xy(i,mylaser[i],&kaka,myencoders);
	xy2mappingcanvas(kaka,&mapping_laser_dpy[i]);
      }
    fl_set_foreground(mappinggui_gc,FL_BLUE);
    /*for(i=0;i<NUM_LASER;i++) XDrawPoint(display,mapping_canvas_win,mappinggui_gc,mapping_laser_dpy[i].x,mapping_laser_dpy[i].y);*/
    XDrawPoints(mydisplay,mapping_canvas_win,mappinggui_gc,mapping_laser_dpy,NUM_LASER,CoordModeOrigin);
    
    //SEGMENTOS LASER
    num_segm_laser=0;
    for(i=0;i<NUM_LASER;i++)
      
      {
	if(val[i])
	  {
	    num_segm_laser++; 
	    xy2mappingcanvas(segmentosLaser[i].pI,&coord_graf_segment_laser[i].pI);
	    xy2mappingcanvas(segmentosLaser[i].pF,&coord_graf_segment_laser[i].pF);
	    val_pintados[i]=TRUE;  
	  }
	else
	  {
	    val_pintados[i]=FALSE;  
	  }
      }
    
    fl_set_foreground(mappinggui_gc,FL_GREEN);
    for(i=0;i<NUM_LASER;i++)
      
      {
        if(val[i])		   
	  {
	    XDrawLine(mydisplay,mapping_canvas_win,mappinggui_gc,coord_graf_segment_laser[i].pI.x,
		      coord_graf_segment_laser[i].pI.y,coord_graf_segment_laser[i].pF.x,
		      coord_graf_segment_laser[i].pF.y);
	    //printf("pI->(%f,%f)  pF->(%f,%f)\n",segmentos.segm[i].pI.x,segmentos.segm[i].pI.y,segmentos.segm[i].pF.x,segmentos.segm[i].pF.y);     
	  }
      }
    //FIN SEGMENTOS LASER  
  }
  
  
  //Pintar memoria segmentos
  
  if ((((mapping_display_state&DISPLAY_SEGMENT_MEMORY)!=0)&&(mapping_visual_refresh==FALSE))
      || (visual_delete_mapping_segment_memory==TRUE)) 
    {	
      //Primero borro los segmentos anteriores
      //printf("SEGMENTOS ->%d\n",segmentos.tam);	
      fl_set_foreground(mappinggui_gc,FL_WHITE);
      if((*myv!=0)||(*myw!=0))
	for(i=0;i<num_segm_memory;i++)
	  {	   
	    XDrawLine(mydisplay,mapping_canvas_win,mappinggui_gc,coord_graf_segment_memory[i].pI.x,
		      coord_graf_segment_memory[i].pI.y,coord_graf_segment_memory[i].pF.x,
		      coord_graf_segment_memory[i].pF.y);
		
	   }
    }
   //Pinto los nuevos segmentos
  if ((mapping_display_state&DISPLAY_SEGMENT_MEMORY)!=0)
     {   	
       num_segm_memory=0;
       for(i=0;i<MAX_SEGM;i++)
	 
	 {
	   if(memoria_segmentos.validos[i])
	     {
	       
	       xy2mappingcanvas(memoria_segmentos.segm[i].pI,&coord_graf_segment_memory[num_segm_memory].pI);
	       xy2mappingcanvas(memoria_segmentos.segm[i].pF,&coord_graf_segment_memory[num_segm_memory].pF);
	       num_segm_memory++;
	     }		  
	 }
	   
       fl_set_foreground(mappinggui_gc,FL_RED);
       for(i=0;i<num_segm_memory;i++)
	 
	 {	   
	   XDrawLine(mydisplay,mapping_canvas_win,mappinggui_gc,coord_graf_segment_memory[i].pI.x,
		     coord_graf_segment_memory[i].pI.y,coord_graf_segment_memory[i].pF.x,
		     coord_graf_segment_memory[i].pF.y);
	   
	 }
       //printf("Memoria  ->%d\n",num_segm_memory);
     }
  
  //Fin pintar memoria segmentos 
  
  //Pintar SEGMENTOS
  
  if ((((mapping_display_state&DISPLAY_SEGMENT)!=0)&&(mapping_visual_refresh==FALSE))
      || (visual_delete_mapping_segment==TRUE))
    {	
      //Primero borro los segmentos anteriores
      //printf("SEGMENTOS ->%d\n",segmentos.tam);	
      fl_set_foreground(mappinggui_gc,FL_WHITE);	
      for(i=0;i<num_segm;i++)
	{	   
	  XDrawLine(mydisplay,mapping_canvas_win,mappinggui_gc,coord_graf_segmentos[i].pI.x,
		    coord_graf_segmentos[i].pI.y,coord_graf_segmentos[i].pF.x,coord_graf_segmentos[i].pF.y);
	  
	}
    }
  //Pinto los nuevos segmentos
  if ((mapping_display_state&DISPLAY_SEGMENT)!=0)
    {	
      num_segm=0;
      for(i=0;i<segmentos.tam;i++)
	
	{
	  num_segm++; 
	  xy2mappingcanvas(segmentos.segm[i].pI,&coord_graf_segmentos[i].pI);
	  xy2mappingcanvas(segmentos.segm[i].pF,&coord_graf_segmentos[i].pF);
	}
	   
	   fl_set_foreground(mappinggui_gc,FL_BLACK);
	   for(i=0;i<segmentos.tam;i++)
	   
	   {	   
		XDrawLine(mydisplay,mapping_canvas_win,mappinggui_gc,coord_graf_segmentos[i].pI.x,
			  coord_graf_segmentos[i].pI.y,coord_graf_segmentos[i].pF.x,
			  coord_graf_segmentos[i].pF.y);
		//printf("pI->(%f,%f)  pF->(%f,%f)\n",segmentos.segm[i].pI.x,segmentos.segm[i].pI.y,segmentos.segm[i].pF.x,segmentos.segm[i].pF.y);     
	   }
	   //printf("Laser  ->%d \n ",num_segm);
    }
  //Fin de pintar segmentos
  
  
  //PINTAR OBJETIVO VFF
  /* visualization of VFF target */
  
  if ((oldvfftarget.x!=vfftarget.x)||(oldvfftarget.y!=vfftarget.y))
    /* the target has changed, do its last position must be cleaned from the canvas */
    {
      fl_set_foreground(mappinggui_gc,FL_WHITE);
      XDrawLine(mydisplay,mapping_canvas_win,mappinggui_gc,targetgraf.x-5,targetgraf.y,targetgraf.x+5,
		targetgraf.y);
      XDrawLine(mydisplay,mapping_canvas_win,mappinggui_gc,targetgraf.x,targetgraf.y-5,
		targetgraf.x,targetgraf.y+5);
	}
      fl_set_foreground(mappinggui_gc,FL_RED);
      xy2mappingcanvas(vfftarget,&targetgraf);
      XDrawLine(mydisplay,mapping_canvas_win,mappinggui_gc,targetgraf.x-5,targetgraf.y,
		targetgraf.x+5,targetgraf.y);
      XDrawLine(mydisplay,mapping_canvas_win,mappinggui_gc,targetgraf.x,targetgraf.y-5,
		targetgraf.x,targetgraf.y+5);
       	
  /* VISUALIZACION: pintar o borrar de el PROPIO ROBOT.
     Siempre hay un repintado total. Esta es la ultima estructura que se se pinta,
     para que ninguna otra se solape encima */
  
  if ((((mapping_display_state&DISPLAY_ROBOT)!=0) &&(mapping_visual_refresh==FALSE))
      || (visual_delete_mapping_ego==TRUE))
    {  
      fl_set_foreground(mappinggui_gc,FL_WHITE); 
      /* clean last robot, but only if there wasn't a total refresh. In case of total refresh the white rectangle already cleaned all */
      for(i=0;i<nummapping_ego;i++) XDrawLine(mydisplay,mapping_canvas_win,mappinggui_gc,mapping_ego[i].x,mapping_ego[i].y,mapping_ego[i+1].x,mapping_ego[i+1].y);
      
    }
  
  if ((mapping_display_state&DISPLAY_ROBOT)!=0){
    fl_set_foreground(mappinggui_gc,FL_MAGENTA);
    /* relleno los nuevos */
    us2xy(15,0.,0.,&aa,myencoders);
    xy2mappingcanvas(aa,&mapping_ego[0]);
    us2xy(3,0.,0.,&aa,myencoders);
    xy2mappingcanvas(aa,&mapping_ego[1]);
    us2xy(4,0.,0.,&aa,myencoders);
    xy2mappingcanvas(aa,&mapping_ego[2]);
    us2xy(8,0.,0.,&aa,myencoders);
    xy2mappingcanvas(aa,&mapping_ego[3]);
    us2xy(15,0.,0.,&aa,myencoders);
    xy2mappingcanvas(aa,&mapping_ego[EGOMAX-1]);
    for(i=0;i<NUM_SONARS;i++)
      {
	us2xy((15+i)%NUM_SONARS,0.,0.,&aa,myencoders); 
	/* Da en el Tvoxel aa las coordenadas del sensor, pues es distancia 0 */
	xy2mappingcanvas(aa,&mapping_ego[i+4]);       
      }
    
    /* pinto los nuevos */
    nummapping_ego=EGOMAX-1;
    for(i=0;i<nummapping_ego;i++) 
      XDrawLine(mydisplay,mapping_canvas_win,mappinggui_gc,mapping_ego[i].x,
		mapping_ego[i].y,mapping_ego[i+1].x,mapping_ego[i+1].y);
  }
  

 
  
  

   /* clear all flags. If they were set at the beginning, they have been already used in this iteration */
  mapping_visual_refresh=FALSE;
  visual_delete_mapping_us=FALSE; 
  visual_delete_mapping_laser=FALSE; 
  visual_delete_mapping_ego=FALSE;
  visual_delete_mapping_laser_segment=FALSE;
  visual_delete_mapping_segment=FALSE;
  visual_delete_mapping_segment_memory=FALSE;
}


/* callback function for button pressed inside the canvas object*/
int mapping_button_pressed_on_micanvas(FL_OBJECT *ob, Window win, int win_width, int win_height, XEvent *xev, void *user_data)
{
  unsigned int keymap;
  float ygraf, xgraf;
  FL_Coord x,y;
  
  /* in order to know the mouse button that created the event */
  mapping_mouse_button=xev->xkey.keycode;
  if(mapping_canvas_mouse_button_pressed==0){
    if ((mapping_mouse_button==MOUSEMIDDLE))
      {
	/* Target for VFF navigation getting mouse coordenates */
      fl_get_win_mouse(win,&x,&y,&keymap);
      ygraf=((float) (mapping_height/2-y))/mapping_escala;
      xgraf=((float) (x-mapping_width/2))/mapping_escala;
      oldvfftarget.x=vfftarget.x;
      oldvfftarget.y=vfftarget.y;
      vfftarget.y=(ygraf-mapping_odometrico[1])*mapping_odometrico[3]+
	(-xgraf+mapping_odometrico[0])*mapping_odometrico[4];
      vfftarget.x=(ygraf-mapping_odometrico[1])*mapping_odometrico[4]+
	(xgraf-mapping_odometrico[0])*mapping_odometrico[3];
		  	      
      }
    else if((mapping_mouse_button==MOUSELEFT)||(mapping_mouse_button==MOUSERIGHT)){
      /* a button has been pressed */
      mapping_canvas_mouse_button_pressed=1;
      
      /* getting mouse coordenates. win will be always the canvas window, because this callback has been defined only for that canvas */  
      fl_get_win_mouse(win,&mapping_x_canvas,&mapping_y_canvas,&keymap);
      old_mapping_x_canvas=mapping_x_canvas;
      old_mapping_y_canvas=mapping_y_canvas;
      
      /* from graphical coordinates to spatial ones */
      ygraf=((float) (mapping_height/2-mapping_y_canvas))/mapping_escala;
      xgraf=((float) (mapping_x_canvas-mapping_width/2))/mapping_escala;
      mapping_mouse_y=(ygraf-mapping_odometrico[1])*mapping_odometrico[3]+
	(-xgraf+mapping_odometrico[0])*mapping_odometrico[4];
      mapping_mouse_x=(ygraf-mapping_odometrico[1])*mapping_odometrico[4]+
	(xgraf-mapping_odometrico[0])*mapping_odometrico[3];
      mapping_mouse_new=1;
      
      /*printf("(%d,%d) Canvas: Click on (%.2f,%.2f)\n",x,y,mapping_mouse_x,mapping_mouse_y);
	printf("robot_x=%.2f robot_y=%.2f robot_theta=%.2f\n",myencoders[0],myencoders[1],myencoders[2]);*/
      
    }else if(mapping_mouse_button==MOUSEWHEELDOWN){
      /* a button has been pressed */
      mapping_canvas_mouse_button_pressed=1;

      /* modifing scale */
      mapping_rango-=1000;
      if(mapping_rango<=RANGO_MIN) mapping_rango=RANGO_MIN;
      fl_set_slider_value(fd_mappinggui->escala,mapping_rango);
      mapping_visual_refresh = TRUE; /* activa el flag que limpia el fondo de la pantalla y repinta todo */ 
      mapping_escala = mapping_width /mapping_rango;

    }else if(mapping_mouse_button==MOUSEWHEELUP){
      /* a button has been pressed */
      mapping_canvas_mouse_button_pressed=1;

      /* modifing scale */
      mapping_rango+=1000;
      if(mapping_rango>=RANGO_MAX) mapping_rango=RANGO_MAX;
      fl_set_slider_value(fd_mappinggui->escala,mapping_rango);
      mapping_visual_refresh = TRUE; /* activa el flag que limpia el fondo de la pantalla y repinta todo */ 
      mapping_escala = mapping_width /mapping_rango;
    }
  }

  return 0;
}

/* callback function for mouse motion inside the canvas object*/
int mapping_mouse_motion_on_micanvas(FL_OBJECT *ob, Window win, int win_width, int win_height, XEvent *xev, void *user_data)
{  
  float diff_x,diff_y;
  unsigned int keymap;

  if(mapping_canvas_mouse_button_pressed==1){

    /* getting mouse coordenates. win will be always the canvas window,
       because this callback has been defined only for that canvas */
    fl_get_win_mouse(win,&mapping_x_canvas,&mapping_y_canvas,&keymap);

    if(mapping_mouse_button==MOUSELEFT){/*
      if (mapping_state==teleoperator)
	{
	// robot is being moved using the canvas 
	mapping_robot_mouse_motion=1;
	
	// getting difference between old and new coordenates 
	diff_x=(mapping_x_canvas-old_mapping_x_canvas);
	diff_y=(old_mapping_y_canvas-mapping_y_canvas);
	
	sqrt_value=sqrt((diff_x*diff_x)+(diff_y*diff_y));
	if(diff_y>=0) acos_value=acos(diff_x/sqrt_value);
	else acos_value=2*PI-acos(diff_x/sqrt_value);
	diff_w=myencoders[2]-acos_value;
	
	// shortest way to the robot theta
	if(diff_w>0){
	  if(diff_w>=2*PI-diff_w){
	    if(2*PI-diff_w<=PI*0.7) w_teleop=RADTODEG*(diff_w)*(0.3);
	    else w_teleop=2.;
	    
	  }else{
	    if(diff_w<=PI*0.7) w_teleop=RADTODEG*(diff_w)*(-0.3);
	    else w_teleop=-2.;
	  }
	}else if(diff_w<0){
	  // changing signus to diff_w 
	  diff_w=diff_w*(-1);
	  if(diff_w>=2*PI-diff_w){
	    if(2*PI-diff_w<=PI*0.7) w_teleop=RADTODEG*(diff_w)*(-0.3);
	    else w_teleop=-2.;
	  }else{
	    if(diff_w<=2*PI-diff_w) w_teleop=RADTODEG*(diff_w)*(0.3);
	    else w_teleop=2;
	  }	  
	}else w_teleop=0.;
	if(w_teleop<-joystick_maxRotVel) w_teleop=-joystick_maxRotVel;
	else if(w_teleop>joystick_maxRotVel) w_teleop=joystick_maxRotVel;

	// setting new value for v 
	if((diff_w>=PI/2)&&(2*PI-diff_w>=PI/2)) v_teleop=(-1)*sqrt_value;
	else v_teleop=sqrt_value;
	if(v_teleop<-joystick_maxTranVel) v_teleop=-joystick_maxTranVel;
	else if(v_teleop>joystick_maxTranVel) v_teleop=joystick_maxTranVel;

	// updating the joystick at GUI 
	if(v_teleop>=0){
	  fl_set_positioner_yvalue(fd_mappinggui->joystick,powf(v_teleop/joystick_maxTranVel,1/3.));
	}else{
	  fl_set_positioner_yvalue(fd_mappinggui->joystick,powf(v_teleop/-joystick_maxTranVel,1/3.));
	}
	if(w_teleop>=0){
	  fl_set_positioner_xvalue(fd_mappinggui->joystick,(powf(w_teleop/joystick_maxRotVel,1/3.)/2.)+0.5);
	}else {
	  fl_set_positioner_xvalue(fd_mappinggui->joystick,(powf(-w_teleop/joystick_maxRotVel,1/3.)/2.)+0.5);
	}
      }
     */ 
    }else if(mapping_mouse_button==MOUSERIGHT){

      /* getting difference between old and new coordenates */
      diff_x=(old_mapping_x_canvas-mapping_x_canvas);
      diff_y=(mapping_y_canvas-old_mapping_y_canvas);
      old_mapping_x_canvas=mapping_x_canvas;
      old_mapping_y_canvas=mapping_y_canvas;

      /* changing odometric range */
      mapping_odometrico[0]+=mapping_rango*(diff_x)*(0.005);
      mapping_odometrico[1]+=mapping_rango*(diff_y)*(0.005);
      mapping_visual_refresh=TRUE;
    }
  }

  return 0;
}

/* callback function for button released inside the canvas object*/
int mapping_button_released_on_micanvas(FL_OBJECT *ob, Window win, int win_width, int win_height, XEvent *xev, void *user_data)
{
  
  if(mapping_canvas_mouse_button_pressed==1){

    if(mapping_mouse_button==MOUSELEFT){/*
      if (mapping_state==teleoperator){
	// robot is being stopped 
	mapping_robot_mouse_motion=1;

	// stopping robot 
	v_teleop=0.;
	w_teleop=0.;
	fl_set_positioner_xvalue(fd_mappinggui->joystick,0.5);
	fl_set_positioner_yvalue(fd_mappinggui->joystick,0.0);
      }*/
    }

    /* a button has been released */
    mapping_canvas_mouse_button_pressed=0;
  }

  return 0;
}


void mapping_guisuspend_aux(void)
{
  v_teleop=0; w_teleop=0; 
  /* to make a safety stop when the robot is being teleoperated from GUI */
  mydelete_buttonscallback(mapping_guibuttons);
  mydelete_displaycallback(mapping_guidisplay);
  fl_hide_form(fd_mappinggui->mappinggui);
}

void mapping_guiresume_aux(void)
{
  static int k=0;
  XGCValues gc_values;

  if (k==0) /* not initialized */
    {
      k++;

      /* Coord del sistema odometrico respecto del visual */
      mapping_odometrico[0]=0.;
      mapping_odometrico[1]=0.;
      mapping_odometrico[2]=0.;
      mapping_odometrico[3]= cos(0.);
      mapping_odometrico[4]= sin(0.);

      mapping_display_state = mapping_display_state | DISPLAY_LASER;
      mapping_display_state = mapping_display_state | DISPLAY_SONARS;
      mapping_display_state = mapping_display_state | DISPLAY_ROBOT;
      //mapping_display_state = mapping_display_state | DISPLAY_MEMORY;
      //mapping_display_state = mapping_display_state | DISPLAY_SEGMENT;
      //mapping_display_state = mapping_display_state | DISPLAY_SEGMENT_MEMORY;
		
		
      fd_mappinggui = create_form_mappinggui();
      fl_set_form_position(fd_mappinggui->mappinggui,400,50);
      fl_show_form(fd_mappinggui->mappinggui,FL_PLACE_POSITION,FL_FULLBORDER,"mapping");
      mapping_canvas_win= FL_ObjWin(fd_mappinggui->micanvas);
      gc_values.graphics_exposures = False;
      mappinggui_gc = XCreateGC(mydisplay, mapping_canvas_win, GCGraphicsExposures, &gc_values);  

      /* canvas handlers */
      fl_add_canvas_handler(fd_mappinggui->micanvas,ButtonPress,mapping_button_pressed_on_micanvas,NULL);
      fl_add_canvas_handler(fd_mappinggui->micanvas,ButtonRelease,mapping_button_released_on_micanvas,NULL);
      fl_add_canvas_handler(fd_mappinggui->micanvas,MotionNotify,mapping_mouse_motion_on_micanvas,NULL);
    }
  else 
    {
      fl_show_form(fd_mappinggui->mappinggui,FL_PLACE_POSITION,FL_FULLBORDER,"mapping");
      mapping_canvas_win= FL_ObjWin(fd_mappinggui->micanvas);
    }

  /* Empiezo con el canvas en blanco */
  mapping_width = fd_mappinggui->micanvas->w;
  mapping_height = fd_mappinggui->micanvas->h;
  fl_winset(mapping_canvas_win); 
  fl_rectbound(0,0,mapping_width,mapping_height,FL_WHITE);   
  /*  XFlush(display);*/
  
  mapping_trackrobot=TRUE;
  fl_set_button(fd_mappinggui->track_robot,PUSHED);
  
  fl_set_slider_bounds(fd_mappinggui->escala,RANGO_MAX,RANGO_MIN);
  fl_set_slider_filter(fd_mappinggui->escala,mapping_range); /* Para poner el valor del slider en metros en pantalla */
  fl_set_slider_value(fd_mappinggui->escala,RANGO_INICIAL);
  mapping_escala = mapping_width/mapping_rango;

  mapping_odom=estimate;
  fl_set_button(fd_mappinggui->bestimate_odom,PUSHED);
  //mapping_state=teleoperator;
  //fl_set_button(fd_mappinggui->rb_vff,PUSHED);	
  //fl_set_positioner_xvalue(fd_mappinggui->joystick,0.5);
  //fl_set_positioner_yvalue(fd_mappinggui->joystick,0.);
  v_teleop=0.;
  w_teleop=0.;

  fl_set_positioner_xvalue(fd_mappinggui->center,0.5);
  fl_set_positioner_yvalue(fd_mappinggui->center,0.5);
	
  myregister_buttonscallback(mapping_guibuttons);
  myregister_displaycallback(mapping_guidisplay);
}

void mapping_hide(){
   callback fn;
  if (fn==NULL){
    if ((fn=(callback)myimport ("graphics_xforms", "suspend_callback"))!=NULL){
      fn ((gui_function)mapping_guisuspend_aux);
    }
  }
  else{
    fn ((gui_function)mapping_guisuspend_aux);
  }
}

void mapping_show(){
   callback fn;
   if (fn==NULL){
      if ((fn=(callback)myimport ("graphics_xforms", "resume_callback"))!=NULL){
         fn ((gui_function)mapping_guiresume_aux);
      }
   }
   else{
      fn ((gui_function)mapping_guiresume_aux);
   }
}
