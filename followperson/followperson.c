/*
 *  Copyright (C) 2006 José María Cañas Plaza 
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  Authors : José María Cañas Plaza <jmplaza@gsyc.escet.urjc.es>
 *            Roberto Calvo Palomino <rocapal@gsyc.escet.urjc.es>
 *            Jose Antonio Santos Cadena <santoscadenas@gmail.com>
 *
 */

#include <stdlib.h>
#include <unistd.h>
#include <forms.h>
#include <math.h>
#include <jde.h>
#include <pioneer.h>
#include <graphics_xforms.h>
#include <colorspaces.h>
#include <fuzzylib.h>
#include <followpersongui.h>
#include <followperson.h>


/** Image standard number of rows*/
#define SIFNTSC_ROWS 240
/** Image standard number of columns*/
#define SIFNTSC_COLUMNS 320

#define followpersonVER  	"followperson - 2.0.0" 

#define D(x...)                  /*printf(x)*/

#define MIN_PIXELES 400          /* Numero minimo de pixeles filtrados */
#define MIN_LINEAS 40            /* Numero minimo de lineas horizontales filtradas */
#define MAX_LINEAS 120				 /* Numero maximo de lineas horizontales filtradas */
#define MAX_LINEAS_FRENA 75		 /*Numero maximo de lineas horizontales que se filtran antes de comenzar a frenar*/
#define MIN_DISTANCIA 40         /* CTE Minima distancia para mover la PANTILT */

#define CTE_PTU 1.9              /* CTE que nos convierte de distancia a unidades PANTILT */
#define ENCOD_TO_DEG (3.086/60.) /* CTE que nos pasa de unidades PANTILT a grados */
#define DEG_TO_ENCOD (60./3.086) /* CTE que nos pasa de grados a unidades PANTILT */

#define MAXPAN_POS 95
#define MAXPAN_NEG -95

/*Gui variables*/
Display *mydisplay;
int  *myscreen;

/*Gui callbacks*/
registerbuttons myregister_buttonscallback;
registerdisplay myregister_displaycallback;
deletebuttons mydelete_buttonscallback;
deletedisplay mydelete_displaycallback;

int last_movement;               /* Indica el ultimo movimiento que ha realizado la pantilt */
struct dfilter data_filter;

int followperson_id=0;
int followperson_brothers[MAX_SCHEMAS];
arbitration followperson_callforarbitration;
int followperson_cycle=100;   /*ANTES 100*/      /* ms */

FD_followpersongui *fd_followpersongui=NULL;

/* Necesarias para las Xlib */
GC followperson_gc;
Window followperson_win;           /* image window */
XImage *imagenOrig;
XImage *hsifiltrada;
XImage *histograma;

/* Defines if the visualization of histograma is activated */
int isActivatedShowHistogram=1;

/* Defines if the visualization of ImageHSI is activated */
int isActivatedShowImageHSI=0;


#define PI 3.141592654
/* this memory must exist even with the followpersongui turned on */
char imagenOrig_buf[SIFNTSC_COLUMNS*SIFNTSC_ROWS*4];
char hsifiltrada_buf[SIFNTSC_COLUMNS*SIFNTSC_ROWS*4];
#define SMAX SIFNTSC_COLUMNS     /*320*/
char disco_buf[SMAX*SMAX*4];
char histograma_buf[SMAX*SMAX*4];
int masc[SMAX*SMAX];

float *mypan_angle=NULL, *mytilt_angle=NULL;  /* degs */
float *mylongitude=NULL; /* degs, pan angle */
float *mylatitude=NULL; /* degs, tilt angle */
float *mylongitude_speed=NULL;
float *mylatitude_speed=NULL;
float *max_pan=NULL;
float *min_pan=NULL;
float *max_tilt=NULL;
float *min_tilt=NULL;

runFn ptmotorsrun, ptencodersrun;
stopFn ptmotorsstop, ptencodersstop;

char** mycolorA;
runFn colorArun;
stopFn colorAstop;

float *myencoders = NULL;
runFn encodersrun;
stopFn encoderstop;

float *myv = NULL;
float *myw = NULL;
runFn motorsrun;
stopFn motorstop;

int *mylaser=NULL;
runFn laserrun ;
stopFn laserstop;

/*Tipos par el calculo de las fuerzas */

Tvoxel *target=NULL;
int *golocalNav=NULL;
int *myDistObj=NULL;
runFn  localNavRun;
stopFn  localNavStop;
int peligro;

/* Para los botones */
#define PUSHED 1
#define RELEASED 0

/* Variables que guardan las coordenadas del quesito */
#define DEGTORAD     (3.14159264 / 180.0)
#define RADTODEG     (180.0 /3.14159264)
#define centro_x 160
#define centro_y 160
#define radio_hsi_map 160.0

int x_pulsada,y_pulsada,xsoltada,ysoltada;
int x_max=200;
int y_max=200;
int x_min=centro_x;
int y_min=centro_y;

int xtrapecio1;
int xtrapecio2;
int ytrapecio1;
int ytrapecio2;

int xquesito1;
int xquesito2;
int xquesito3;
int xquesito4;

int yquesito1;
int yquesito2;
int yquesito3;
int yquesito4;

int pulsada=0;
/*Fin quesito*/

int toblack=0;

int follow=0;

double s_min, s_max, h_min, h_max, i_min, i_max;
int hsimap_threshold;

/*Parametros para la lógica borrosa*/
int fuzzy_controller;
int lineas;
float v_lineal,w_angular;

/** CONSTANTE PARA EL MOVIMIENTO **/
#define CTEANG 0.5

/* Parametros para el movimiento */
int diff_angulos;
int diff_ant;

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* PARÁMETROS DE VENTANA DE SEGURIDAD */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
#define peligroX 250.00/*ancho para la ventana de peligro*/
#define peligroY 600.00/*alto para la ventana de peligro*/
float distanciasVentanaPeligro[90];

/**********************************************************************************/
/**********************************************************************************/
/**************************COMPORTAMIENTO VENTANA**********************************/
/**********************************************************************************/   
void rellenarDistanciasPeligro () { /* Concretamos las dimensiones de la ventana, según los parámetros peligroX y peligroY */
	int phi = 0; /* inicialmente comenzamos con el láser de ángulo 0 */

	while (tan (phi*DEGTORAD) <= (peligroY/peligroX)) { /* rellenamos la zona que tiene igual huecoX */
		distanciasVentanaPeligro [phi] = peligroX / (cos (phi*DEGTORAD));
		phi ++;
	}

	while (phi < 90) { /* rellenamos la zona que tiene igual coseno */
		distanciasVentanaPeligro [phi] = peligroY / (sin (phi*DEGTORAD));
		phi ++;
	}
}
/*
void rellenarVentanaSeguridad () {
	int i, j;

	for (i = 0;	i < 90; i ++) { /* primer cuadrante relleno **
		laser2xy (i, distanciasVentanaSeguridad [i], &ventanaSeguridad [i],&myencoders[0]);
	}

	for (i = 90, j = 89; i < 180; i ++, j--) { /* segundo cuadrante relleno **
		laser2xy (i, distanciasVentanaSeguridad [j], &ventanaSeguridad [i],&myencoders[0]);
	}
}
*/

int calcularAnguloSemiExtremo () {
	float phi = atan (peligroY / peligroX);

	phi = phi*RADTODEG;
	phi = (int) ceil (phi);

	return (phi);
}

int objetoPeligroso()
{
	int i, j;
	int obstacleFound = FALSE;
	int phiSemi = calcularAnguloSemiExtremo();

	i = phiSemi;
	while ((i < 90) && (obstacleFound==FALSE)) {
		if ((mylaser[i] < distanciasVentanaPeligro[i]))
			obstacleFound = TRUE;
		i ++;
	}

	j = 89;
	if (obstacleFound == FALSE) { /* continuamos buscando en la segunda mitad*/
		while ((i < (180 - phiSemi)) && (obstacleFound==FALSE)) {
			if (mylaser[i] < distanciasVentanaPeligro[j])
				obstacleFound = TRUE;
			i++;
			j--;
		}
	}

	return (obstacleFound);
}

/**********************************************************************************/
/**********************************************************************************/
/**********************************F I N*******************************************/
/**********************************************************************************/

void followperson_imports()
{

	/* importamos las variables para la navegation local*/
  localNavRun=myimport("localNavigation","run");
  localNavStop=myimport("localNavigation","stop");
	target=myimport("localNavigation","target");
	golocalNav=*(int**) myimport("localNavigation","golocalNavigation");
	myDistObj=myimport("localNavigation","distanciaObjetivo");
	
	/* importamos motors de pantilt */
  mylongitude=myimport("ptmotors", "longitude");
  mylatitude=myimport ("ptmotors", "latitude");
  mylongitude_speed=myimport("ptmotors", "longitude_speed");
  mylatitude_speed=myimport("ptmotors","latitude_speed");
  
  max_pan=myimport("ptmotors", "max_longitude");
  max_tilt=myimport("ptmotors", "max_latitude");
  min_pan=myimport("ptmotors", "min_longitude");
  min_tilt=myimport("ptmotors", "min_latitude");

	ptmotorsrun=myimport("ptmotors","run");
	ptmotorsstop=myimport("ptmotors","stop");

  /* importamos encoders de pantilt */
  mypan_angle=myimport("ptencoders", "pan_angle");
  mytilt_angle=myimport("ptencoders", "tilt_angle");

  ptencodersrun=myimport("ptencoders", "run");
  ptencodersstop=myimport("ptencoders", "stop");
  
  /* Importamos colorA */
  mycolorA=myimport ("colorA", "colorA");
  colorArun=myimport("colorA", "run");
  colorAstop=myimport("colorA", "stop");

	/*importamos motores y enconders*/
  myencoders=(float *)myimport("encoders","jde_robot");
  encodersrun=(runFn)myimport("encoders","run");
  encoderstop=(stopFn)myimport("encoders","stop");

  myv=(float *)myimport("motors","v");
  myw=(float *)myimport("motors","w");
  motorsrun=(runFn)myimport("motors","run");
  motorstop=(stopFn)myimport("motors","stop");

	/*importamos el laser*/
  mylaser=(int *)myimport("laser","laser");
  laserrun=(runFn)myimport("laser","run");
  laserstop=(stopFn *)myimport("laser","stop");

}

void hsi2rgb (double H, double S, double I, double *r, double *g, double *b)
{
  /*Pasa de hsi a rgb. Todos entre 0 y 1, execpto H, que esta entre 0 y 2*pi */

  if (S <= 1)
  {
    /* Hay que pasar el punto HS a el espacio RGB */
    if (H <= 2.0 * PI / 3.0)
    {
      *b = (1.0 - S) / 3.0;
      *r = (1.0 + ((S * cos (H)) / (cos (PI / 3.0 - H)))) / 3.0;
      *g = 1.0 - (*b + *r);
    }
    else if (H <= 4.0 * PI / 3.0)
    {
      H = H - 2 * PI / 3.0;
      *r = (1.0 - S) / 3.0;
      *g = (1.0 + ((S * cos (H)) / (cos (PI / 3.0 - H)))) / 3.0;
      *b = 1.0 - (*r + *g);
    }
    else if (H <= 2.0 * PI)
    {
      H = H - 4.0 * PI / 3.0;
      *g = (1.0 - S) / 3.0;
      *b = (1.0 + ((S * cos (H)) / (cos (PI / 3.0 - H)))) / 3.0;
      *r = 1.0 - (*g + *b);
    }
  }
  else
  {                              /* Puntos fuera del circulo negros */
    *r = 0;
    *b = 0;
    *g = 0;
  }
}


void draw_hsimap(char *buffer, int size)
{
  int i,j,ind;
  float x, y, H, S, scale;
  double r,g,b;
  unsigned char R,G,B;

  for(j=0; j < size; j++)
  {
    for(i=0; i < size; i++)
    {
      x = (2.0*i)/size - 1.0;
      y = 1.0 - (2.0*j)/size;
      if ((x >= -1e-7) && (x <= 1e-7))
      {
        if (y > 0)
        {
          H = PI/2.0;
        }
        else
        {
          H = 3.0*PI/2.0;
        }
      }                          /*x != 0 */
      else
      {
        if (x < 0)
        {
          H = atan(y/x) + PI;
        }
        else
        {
          H = atan (y/x);
        }
      }
      H=-H;
      if (H>2*PI)
      {
        H-=2*PI;
      }
      if (H<0)
      {
        H+=2*PI;
      }

      S = sqrt(y*y + x*x);
      ind = (size*j + i)*4;

      hsi2rgb (H,S,0,&r,&g,&b);
      scale = 255.0;
      R = (unsigned char) (scale * r);
      G = (unsigned char) (scale * g);
      B = (unsigned char) (scale * b);
      buffer[ind]   = B;         /* Blue */
      buffer[ind+1] = G;         /* Green */
      buffer[ind+2] = R;         /* Red */
    }
  }
}


/*Funciones para dibujar los quesitos*/
int drawcircle(char *img, int xcentro, int ycentro, int radio, FL_COLOR thiscolor)
{

  int r,g,b;
  int x1,y1;
  float i;

  /* In this image always graf coordinates x=horizontal, y=vertical, starting at the top left corner of the image. They can't reach 240 or 320, their are not valid values for the pixels.  */

  if (thiscolor==FL_BLACK) {r=0;g=0;b=0;}
  else if (thiscolor==FL_RED) {r=255;g=0;b=0;}
  else if (thiscolor==FL_BLUE) {r=0;g=0;b=255;}
  else if (thiscolor==FL_PALEGREEN) {r=113;g=198;b=113;}
  else if (thiscolor==FL_WHEAT) {r=255;g=231;b=155;}
  else if (thiscolor==FL_DEEPPINK) {r=213;g=85;b=178; }
  else if (thiscolor==FL_WHITE) {r=255;g=255;b=255;}
  else {r=0;g=0;b=0;}

  for (i=0.;i<=360;i=i+0.1)
  {
    x1=cos(i*DEGTORAD)*radio+xcentro;
    y1=sin(i*DEGTORAD)*radio+ycentro;
    fflush (NULL);
    img[(y1*SIFNTSC_COLUMNS+x1)*4]=b;
    img[(y1*SIFNTSC_COLUMNS+x1)*4+1]=g;
    img[(y1*SIFNTSC_COLUMNS+x1)*4+2]=r;
  }
  return 0;
}


int lineinimage(char *img, int xa, int ya, int xb, int yb, FL_COLOR thiscolor)
{
  float L;
  int i,imax,r,g,b;
  int lastx,lasty,thisx,thisy,lastcount;
  int threshold=1;
  int Xmax,Xmin,Ymax,Ymin;

  Xmin=0; Xmax=SIFNTSC_COLUMNS-1; Ymin=0; Ymax=SIFNTSC_COLUMNS-1;
  /* In this image always graf coordinates x=horizontal, y=vertical, starting at the top left corner of the image. They can't reach 240 or 320, their are not valid values for the pixels.  */

  if (thiscolor==FL_BLACK) {r=0;g=0;b=0;}
  else if (thiscolor==FL_RED) {r=255;g=0;b=0;}
  else if (thiscolor==FL_BLUE) {r=0;g=0;b=255;}
  else if (thiscolor==FL_PALEGREEN) {r=113;g=198;b=113;}
  else if (thiscolor==FL_WHEAT) {r=255;g=231;b=155;}
  else if (thiscolor==FL_DEEPPINK) {r=213;g=85;b=178;}
  else if (thiscolor==FL_WHITE) {r=255;g=255;b=255;}
  else {r=0;g=0;b=0;}

  /* first, check both points are inside the limits and draw them */
  if ((xa>=Xmin) && (xa<Xmax+1) && (ya>=Ymin) && (ya<Ymax+1) &&
    (xb>=Xmin) && (xb<Xmax+1) && (yb>=Ymin) && (yb<Ymax+1))
  {
    /* draw both points */

    img[(SMAX*ya+xa)*4+0]=b;
    img[(SMAX*ya+xa)*4+1]=g;
    img[(SMAX*ya+xa)*4+2]=r;
    img[(SMAX*yb+xb)*4+0]=b;
    img[(SMAX*yb+xb)*4+1]=g;
    img[(SMAX*yb+xb)*4+2]=r;

    L=(float)sqrt((double)((xb-xa)*(xb-xa)+(yb-ya)*(yb-ya)));
    imax=4*(int)L+1;
    /* if (debug==1) printf("xa=%d ya=%d xb=%d yb=%d L=%.f imax=%d\n",xa,ya,xb,yb,L,imax);  */
    lastx=xa; lasty=xb; lastcount=0;
    for(i=0;i<=imax;i++)
    {
      thisy=(int)((float)ya+(float)i/(float)imax*(float)(yb-ya));
      thisx=(int)((float)xa+(float)i/(float)imax*(float)(xb-xa));
      if ((thisy==lasty)&&(thisx==lastx)) lastcount++;
      else
      {
        if (lastcount>=threshold)
        {                        /* draw that point in the image */
          img[(SMAX*lasty+lastx)*4+0]=b;
          img[(SMAX*lasty+lastx)*4+1]=g;
          img[(SMAX*lasty+lastx)*4+2]=r;
        }
        lasty=thisy;
        lastx=thisx;
        lastcount=0;
      }
    }
    return 0;
  }
  else return -1;
}


int drawarc(char *img, int xcentro, int ycentro, int radio, int x1, int y1, int x2, int y2, FL_COLOR thiscolor)
{

  int r,g,b;
  int x,y;
  float i,imax;

  /* In this image always graf coordinates x=horizontal, y=vertical, starting at the top left corner of the image. They can't reach 240 or 320, their are not valid values for the pixels.  */
  if ((x1==x2)&&(y1==y2))
  {
    drawcircle(img, xcentro, ycentro, radio, thiscolor);
  }
  else
  {
    if (thiscolor==FL_BLACK) {r=0;g=0;b=0;}
    else if (thiscolor==FL_RED) {r=255;g=0;b=0;}
    else if (thiscolor==FL_BLUE) {r=0;g=0;b=255;}
    else if (thiscolor==FL_PALEGREEN) {r=113;g=198;b=113;}
    else if (thiscolor==FL_WHEAT) {r=255;g=231;b=155;}
    else if (thiscolor==FL_DEEPPINK) {r=213;g=85;b=178; }
    else if (thiscolor==FL_WHITE) {r=255;g=255;b=255;}
    else {r=0;g=0;b=0;}
    x1=x1-xcentro;
    y1=ycentro-y1;
    x2=x2-xcentro;
    y2=ycentro-y2;

    if (x1==0)
    {
      if (y1<0)
      {
        i=3*3.1416/2.;
      }
      else
      {
        i=3.1416/2.;
      }
    }
    else
    {
      if (y1==0)
      {
        if (x1<0)
        {
          i=3.1416;
        }
        else
        {
          i=0.;
        }
      }
      else
      {
        if (x1>0)
        {
          i=atan((float)y1/(float)x1);
        }
        else
        {
          i=atan((float)y1/(float)x1)+3.1416;
        }
      }
    }

    i=i*RADTODEG;

    if (x2==0)
    {
      if (y2<0)
      {
        imax=3*3.1416/2.;
      }
      else
      {
        imax=3.1416/2.;
      }
    }
    else
    {
      if (y2==0)
      {
        if (x2<0)
        {
          imax=3.1416;
        }
        else
        {
          imax=0.;
        }
      }
      else
      {
        if (x2>0)
        {
          imax=atan((float)y2/(float)x2);
        }
        else
        {
          imax=atan((float)y2/(float)x2)+3.1416;
        }
      }
    }
    imax=imax*RADTODEG;
    if (imax<i)
    {
      imax=imax+360;
    }
    for (;i<=imax;i=i+0.1)
    {
      x=(cos(i*DEGTORAD)*radio+xcentro);
      y=(ycentro-sin(i*DEGTORAD)*radio);

      img[(y*SMAX+x)*4]=b;
      img[(y*SMAX+x)*4+1]=g;
      img[(y*SMAX+x)*4+2]=r;
    }
  }
  return 0;
}


void drawcheese (char *img,int x_centro,int y_centro, double h_max, double h_min, double s_max, double s_min, FL_COLOR thiscolor)
{
  int x1,y1,x2,y2;
  s_max=s_max*radio_hsi_map;
  s_min=s_min*radio_hsi_map;

  x1=(cos(h_max)*s_min+x_centro);
  y1=(y_centro-sin(h_max)*s_min);
  x2=(cos(h_max)*s_max+x_centro);
  y2=(y_centro-sin(h_max)*s_max);

  lineinimage(img,x1,y1,x2,y2,thiscolor);

  x1=(cos(h_min)*s_min+x_centro);
  y1=(y_centro-sin(h_min)*s_min);
  x2=(cos(h_min)*s_max+x_centro);
  y2=(y_centro-sin(h_min)*s_max);

  lineinimage(img,x1,y1,x2,y2,thiscolor);

  x1=(cos(h_min)*s_max+x_centro);
  y1=(y_centro-sin(h_min)*s_max);
  x2=(cos(h_max)*s_max+x_centro);
  y2=(y_centro-sin(h_max)*s_max);

  drawarc(img,x_centro,y_centro,s_max,x1,y1,x2,y2,thiscolor);

  x1=(cos(h_min)*s_min+x_centro);
  y1=(y_centro-sin(h_min)*s_min);
  x2=(cos(h_max)*s_min+x_centro);
  y2=(y_centro-sin(h_max)*s_min);

  drawarc(img,x_centro,y_centro,s_min,x1,y1,x2,y2,thiscolor);

}


/*Fin funciones quesito*/

int followpersongui_setupDisplay(void)
/* Inicializa las ventanas, la paleta de colores y memoria compartida para visualizacion*/
{
  int vmode;
  XGCValues gc_values;

  gc_values.graphics_exposures = False;
  followperson_gc = XCreateGC(mydisplay,followperson_win, GCGraphicsExposures, &gc_values);

  vmode= fl_get_vclass();

  if ((vmode==TrueColor)&&(fl_state[vmode].depth==16))
  {
    /* Imagen principal */
    imagenOrig = XCreateImage(mydisplay,DefaultVisual(mydisplay,*myscreen),16, ZPixmap,0,imagenOrig_buf,SIFNTSC_COLUMNS,SIFNTSC_ROWS,8,0);

    /*Imagen filtrada */
    hsifiltrada = XCreateImage(mydisplay, DefaultVisual(mydisplay,*myscreen),16, ZPixmap,0,hsifiltrada_buf,SIFNTSC_COLUMNS,SIFNTSC_ROWS,8,0);

    /* Mapa HSI */
    histograma = XCreateImage(mydisplay, DefaultVisual(mydisplay,*myscreen),16, ZPixmap,0,histograma_buf,SMAX,SMAX,8,0);
  }
  else if ((vmode==TrueColor)&&(fl_state[vmode].depth==24))
  {
    /* Imagen principal */
    imagenOrig = XCreateImage(mydisplay,DefaultVisual(mydisplay,*myscreen),24, ZPixmap,0,imagenOrig_buf,SIFNTSC_COLUMNS,SIFNTSC_ROWS,8,0);

    /*Imagen filtrada */
    hsifiltrada = XCreateImage(mydisplay, DefaultVisual(mydisplay,*myscreen),24, ZPixmap,0,hsifiltrada_buf,SIFNTSC_COLUMNS,SIFNTSC_ROWS,8,0);

    /* Mapa HSI */
    histograma = XCreateImage(mydisplay, DefaultVisual(mydisplay,*myscreen),24, ZPixmap,0,histograma_buf,SMAX,SMAX,8,0);
  }
  else if ((vmode==TrueColor)&&(fl_state[vmode].depth==32))
  {
    /* Imagen principal */
    imagenOrig = XCreateImage(mydisplay,DefaultVisual(mydisplay,*myscreen),32, ZPixmap,0,imagenOrig_buf,SIFNTSC_COLUMNS,SIFNTSC_ROWS,8,0);

    /*Imagen filtrada */
    hsifiltrada = XCreateImage(mydisplay, DefaultVisual(mydisplay,*myscreen),32, ZPixmap,0,hsifiltrada_buf,SIFNTSC_COLUMNS,SIFNTSC_ROWS,8,0);

    /* Mapa HSI */
    histograma = XCreateImage(mydisplay, DefaultVisual(mydisplay,*myscreen),32, ZPixmap,0,histograma_buf,SMAX,SMAX,8,0);
  }
  else if ((vmode==PseudoColor)&&(fl_state[vmode].depth==8))
  {
    /* Imagen principal */
    imagenOrig = XCreateImage(mydisplay,DefaultVisual(mydisplay,*myscreen),8, ZPixmap,0,imagenOrig_buf,SIFNTSC_COLUMNS,SIFNTSC_ROWS,8,0);

    /*Imagen filtrada */
    hsifiltrada = XCreateImage(mydisplay, DefaultVisual(mydisplay,*myscreen),8, ZPixmap,0,hsifiltrada_buf,SIFNTSC_COLUMNS,SIFNTSC_ROWS,8,0);

    /* Mapa HSI */
    histograma = XCreateImage(mydisplay, DefaultVisual(mydisplay,*myscreen),8, ZPixmap,0,histograma_buf,SMAX,SMAX,8,0);
  }
  else
  {
    perror("Unsupported color mode in X server");exit(1);
  }
  return 1;
}

void pantilt_iteration()
{

#define VEL_MAX_PAN 2500.0*ENCOD_TO_DEG
#define VEL_MIN_PAN 350.0*ENCOD_TO_DEG
#define POS_MIN_PAN 40.0
#define POS_MAX_PAN 160.0

#define VEL_MAX_TILT 1000.0*ENCOD_TO_DEG
#define VEL_MIN_TILT 300.0*ENCOD_TO_DEG
#define POS_MIN_TILT 40.0
#define POS_MAX_TILT 120.0

  float angulo_x, angulo_y;

  D ("Pantilt Iteration, pixeles=%d(%d) - distancia=%d(%d)\n",data_filter.pixeles,MIN_PIXELES,data_filter.distancia,MIN_DISTANCIA);
  if (data_filter.pixeles > MIN_PIXELES)
  {

                                 /* banda muerta */
    if (data_filter.distancia > MIN_DISTANCIA)
    {

      /* Calculamos la distancia que se tiene que mover la PAN-TILT */

                                 //  CTE_PTU);
      angulo_x = ENCOD_TO_DEG * ((int) abs(data_filter.x) );
                                 //  CTE_PTU);
      angulo_y = ENCOD_TO_DEG * ((int) abs(data_filter.y) );

      *mylongitude_speed = VEL_MAX_PAN-((POS_MAX_PAN-(abs(data_filter.x)))/((POS_MAX_PAN-POS_MIN_PAN)/(VEL_MAX_PAN-VEL_MIN_PAN)));
      *mylatitude_speed = VEL_MAX_TILT-((POS_MAX_TILT-(abs(data_filter.y)))/((POS_MAX_TILT-POS_MIN_TILT)/(VEL_MAX_TILT-VEL_MIN_TILT)));

      //*mylongitude_speed = 1000*ENCOD_TO_DEG;
      //*mylatitude_speed = 300*ENCOD_TO_DEG;

      switch (data_filter.cuadrante)
      {

        /* 

          -----------------------
          |          |          |
          |          |          |
          |    4     |    3     |
          |          |          |
          |--------- |--------- |
          |          |          |
          |          |          |
          |    2     |     1    |
          |          |          |
          -----------------------

        */
        case 1:                  /* Si esta en el 1 -> Mover hacia abajo y derecha */
          D("OBJETIVO: Primer Cuadrante\n");
          *mylongitude = *min_pan;
          *mylatitude =  *min_tilt;
          last_movement=right;
          break;

        case 2:                  /* Si esta en el 2 -> Mover hacia abajo e izquierda */
          D("OBJETIVO: Segundo Cuadrante\n");
          *mylongitude = *max_pan;
          *mylatitude = *min_tilt;
          last_movement=left;
          break;
        case 3:                  /* Si esta en el 3 -> Mover hacia arriba e derecha */
          D("OBJETIVO: Tercer Cuadrante\n");
          *mylongitude = *min_pan;
          *mylatitude = *max_tilt;
          last_movement=right;
          break;

        case 4:                  /*  Si esta en el 4 -> Mover hacia arriba e izquierda */
          D("OBJETIVO: Cuarto Cuadrante\n");
          *mylongitude = *max_pan;
          *mylatitude = *max_tilt;
          last_movement=left;
          break;

        default:
          D("Pantilt Iteration: Cuadrante erroneo\n");
      }

      D("Pantilt Iteration: longitude=%f - latitude=%f\n",*mylongitude,*mylatitude);
    }
    else
    {
      /* Paramos los ejes ya que estamos en zona de la banda muerta */
      D("Pantilt Iteration: Estamos en zona muerta ... parar ejes\n");
      *mylongitude_speed = 0.0;
      *mylatitude_speed = 0.0;
    }
  }
}


void busqueda_iteration()
{
  
  *mylongitude_speed=1200*ENCOD_TO_DEG;
  *mylatitude_speed = 0.0;

  /* Buscamos hacia la derecha */
  if (last_movement==right)
  {
    D("[DERECHA]Busqueda Iteration: pan_angle=%f (MAX=%f) , tilt_angle=%f\n",*mypan_angle, MAX_PAN_ANGLE ,*mytilt_angle);
    if ( *mypan_angle > MAXPAN_NEG )
    {
      *mylongitude = *min_pan;
    }
    else
    {
      last_movement=left;
    }

  }
  /* Buscamos hacia la izquierda */
  else
  {
    D("[IZQUIERDA]Busqueda Iteration: pan_angle=%f (MAX=%f) , tilt_angle=%f\n",*mypan_angle, MAX_PAN_ANGLE ,*mytilt_angle);
    if ( *mypan_angle < MAXPAN_POS )
    {
      *mylongitude = *max_pan;
    }
    else
    {
      last_movement=right;
    }
  }

}


int sqr (int num)
{
  return num*num;
}

int muycerca()
{

  if ( (data_filter.lineas > MAX_LINEAS) /*|| (data_filter.pixeles < MIN_PIXELES)*/ )
    return 1;
  else {
    return 0;
  }

}

int diffangulos()
{
	return (int) ((*mypan_angle)-myencoders[2]);
}

float movimientoAng(int angact)
{
	return CTEANG*angact;
}

void movimiento_iteration()
{
	/*printf("pan_angle = %f\n",*mypan_angle);*/

	/*Diferentes estados para el movimiento cuando no hay peligro*/
	/*Si existe peligro controla los movimientos el VFF*/


	printf("LINEAS = %d\n",data_filter.lineas);
	printf("PELIGRO = %d\n",peligro);
	diff_angulos = diffangulos();
	printf("DIFF ANG = %d\n",diff_angulos);
	if((*myDistObj >= 0) && (*myDistObj <= 100))
	{
		*golocalNav = 0;
		/*Orientamos el robot*/
		if (muycerca())
		{
			printf("ESTAS CERCA\n");
			*myv=0;
			*myw=0;
		}
		else
		{
			if (abs(diff_angulos) > 10)/*Si NO estamos orientados*/
			{
				/*if(diff_angulos < -10)
				{
					printf("GIRAMOS DERECHAR\n");
					/*Girar derecha*/
					/**myw = -15;
				}
				else
				{
					printf("GIRAMOS IZQUIERDA\n");
					/*Girar izquierda*/
					/**myw = 15;
				}	*/
				*myw = movimientoAng(diff_angulos);
			}
			else/*Estamos lejos*/
			{
				printf("ESTAS MUY LEJOS\n");
				*myv = 300;/*Avanzamos lento*/
			}
		}
		 
	}
	diff_ant=diff_angulos;
	printf("V= %f\n",*myv);
	printf("W= %f\n",*myw);
}

void init_fuzzyController () {
	// Obtención de reglas para la actuación según el gradiente que se genere
	fuzzy_controller = fc_open("/home/jmvega/jdeconf/followperson.fzz"); // abrimos nuestro fichero de reglas borrosas

	if (fuzzy_controller!=-1) { // si hemos abierto correctamente el fichero de reglas borrosas
		fc_link(fuzzy_controller,"angulo",&diff_angulos);
		fc_link(fuzzy_controller,"lineas",&lineas);
	}
}

void followperson_iteration()
{
  int i;
  double r,g,b,I,H,S;
  unsigned int X, Y;
  double x,y;
  struct HSV* myHSV;

  int pixeles=0, pasa_filtro=0, num_lineas=0;
  int pixel_x=1, pixel_y=1;

	if (follow == 1)
	{

		/*printf("NUM LINEAS = %d \n",data_filter.lineas);
		printf("NUM PIXELES = %d \n",data_filter.pixeles);*/
	
  	data_filter.x=0;
  	data_filter.y=0;
  	data_filter.distancia=0;
	
  	all[followperson_id].k++;
	
  	/*  printf("followperson iteration %d\n",d++);*/
	
  	/*
  	for(i=0; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS; i++)
  	{                              // Blue Byte 
  	  imagenOrig_buf[i*4]=mycolorA[i*3];
  	                               // Green Byte 
  	  imagenOrig_buf[i*4+1]=mycolorA[i*3+1];
  	                               // Red Byte 
  	  imagenOrig_buf[i*4+2]=mycolorA[i*3+2];
  	  imagenOrig_buf[i*4+3]=0;     // dummy byte 
  	}
  	*/
  	
  	for(i=0; i<SMAX*SMAX; i++)
  	  masc[i]=0;
	
  	for(i=0;i< SIFNTSC_ROWS*SIFNTSC_COLUMNS; i++)
  	{
	
  	  r = (float)(unsigned int)(unsigned char)(*mycolorA)[i*3+2]; //imagenOrig_buf[i*4+2];
  	  g = (float)(unsigned int)(unsigned char)(*mycolorA)[i*3+1]; //imagenOrig_buf[i*4+1];
  	  b = (float)(unsigned int)(unsigned char)(*mycolorA)[i*3];   //imagenOrig_buf[i*4];
	
  	  myHSV = (struct HSV*) RGB2HSV_getHSV( (int)r,(int)g,(int)b);
  	  H=myHSV->H;
  	  S=myHSV->S;
  	  I=myHSV->V;
	
  	  if((I<=i_max)&&(I>=i_min)&&
  	    (S >= s_min) && (S <= s_max) &&
  	    (H >= h_min) && (H <= h_max) )
  	  {
  	    /* pasa el filtro */
  	    if (isActivatedShowImageHSI)
  	    {
  	      hsifiltrada_buf[i*4+0]=(*mycolorA)[i*3]; //imagenOrig_buf[i*4+0];
  	      hsifiltrada_buf[i*4+1]=(*mycolorA)[i*3+1]; //imagenOrig_buf[i*4+1];
  	      hsifiltrada_buf[i*4+2]=(*mycolorA)[i*3+2]; //imagenOrig_buf[i*4+2];
  	      hsifiltrada_buf[i*4+3]=0;
  	    }
	
  	    pixeles++;
  	    data_filter.x += pixel_x;
  	    data_filter.y += pixel_y;
  	    pasa_filtro=1;
	
  	  }
  	  else
  	  {
  	    if (isActivatedShowImageHSI)
  	    {
  	      /*No pasa el filtro*/
  	      if (toblack)
  	      {
  	        /*Pasar a negro*/
  	        hsifiltrada_buf[i*4+0] = (unsigned char) 0;
  	        hsifiltrada_buf[i*4+1] = (unsigned char) 0;
  	        hsifiltrada_buf[i*4+2] = (unsigned char) 0;
  	        hsifiltrada_buf[i*4+3] = 0;
  	      }
  	      else
  	      {
  	        /* En vez de a negro lo pasamos a BW, oscurecido para que se vea mejor (180/255 sobre 1) */
  	        hsifiltrada_buf[i*4+0] = (unsigned char) (180.0*I);
  	        hsifiltrada_buf[i*4+1] = (unsigned char) (180.0*I);
  	        hsifiltrada_buf[i*4+2] = (unsigned char) (180.0*I);
  	        hsifiltrada_buf[i*4+3] = 0;
  	      }
  	    }
  	  }
	
  	  /* Hacemos una conversion para simular que estamos sobre un array dimensional y saber en que pixel (x,y) estamos */
  	 if (pixel_x==320)
  	 {
  	   pixel_x=1;
  	   pixel_y++;
  	   if (pasa_filtro==1)
        num_lineas++;
 		    pasa_filtro=0;
	
 		 	} else
 	   		pixel_x++;

 	   if (isActivatedShowHistogram)
 	   {
 	     /*Apunto los valores en la mscara HSI (histograma negativo) */
 	     x = S*cos(-H-(3.1416/2.)); /* El rojo (H=0)est a la derecha */
 	     y = S*sin(-H-(3.1416/2.));
	
 	     X = (x + 1.0)*SMAX/2.0;
 	     Y = (1.0 - y)*SMAX/2.0;
	
 	     masc[X*SMAX + Y]++;
     }

  	}                              // FOR

  	/* Dibujamos el histograma negativo */

  	if (isActivatedShowHistogram)
  	{
   	 	memcpy(histograma_buf, disco_buf, SMAX*SMAX*4);
    	for (i=0;i<=SMAX*SMAX; i++)
    	{
      	if (masc[i] >= hsimap_threshold)
      	{
      	  histograma_buf[i*4+0] = (char)255;
      	  histograma_buf[i*4+1] = (char)255;
      	  histograma_buf[i*4+2] = (char)255;
      	}
    	}	
  	}	

	  data_filter.pixeles = pixeles;
  	data_filter.lineas = num_lineas;

		peligro = objetoPeligroso();
		if ( peligro == 1 ){
			printf ("Estamos en peligro\n");
			system ("mplayer /usr/share/gnome-games/sounds/die.ogg");

			if ( *golocalNav == 0){
				laser2xy(90,1500,target,&myencoders[0]);		
				printf("Asignado objetivo = [%i, %i]\n", target->x, target->y);
				*golocalNav = 1;
			}

			printf("GOLOCALNAV = %d\n",*golocalNav);
		}

		if (*golocalNav == 0) {
	  	if (data_filter.pixeles>MIN_PIXELES) {
				//system("mplayer /usr/share/gnome-games/sounds/life.ogg");
	    	/* hallamos la media de los puntos */
	    	data_filter.x = data_filter.x / data_filter.pixeles;
	    	data_filter.y = data_filter.y / data_filter.pixeles;

		    /* hallamos las coordenadas reales para el centro (160,120)   */
		    data_filter.x = data_filter.x - 160;
		    data_filter.y = data_filter.y - 120;
		
		    /* hallamos la distancia entre el centro y el punto medio */
		    data_filter.distancia = sqrt((sqr(data_filter.x)+sqr(data_filter.y)));
		
		    /* miramos en que cuadrante se encuentra */
		    if (data_filter.x > 0)
		      if (data_filter.y > 0)
		        data_filter.cuadrante=1;
			    else
			      data_filter.cuadrante=3;
		    else
			    if (data_filter.y > 0)
			      data_filter.cuadrante=2;
			    else
	  		    data_filter.cuadrante=4;

	  	  if ((data_filter.x==0) && (data_filter.y==0))
	  	    data_filter.cuadrante=0;
		
	  	  pantilt_iteration();
				movimiento_iteration();
			}	else { // no encuentra el color
				printf("BUSCANDO OBJETO\n");
				*myv=0;
				*myw=0;
	  		busqueda_iteration();
			}
		} else // golocalNav = 1
			if ((*myDistObj >= 0) && (*myDistObj <= 100)) *golocalNav = 0; // comprobamos si seguimos usando VFF
	}	else { // boton follow no pulsado
		*myv = 0;
		*myw = 0;
		*mylatitude = 0;
		*mylongitude = 0;
		*golocalNav = 0;
	}
}


void followperson_stop()
{
  pthread_mutex_lock(&(all[followperson_id].mymutex));
  put_state(followperson_id,slept);

  *mylongitude_speed = 0.0;
  *mylatitude_speed = 0.0;

  all[followperson_id].children[(*(int *)myimport("ptencoders","id"))]=FALSE;
  all[followperson_id].children[(*(int *)myimport("ptmotors","id"))]=FALSE;
  all[followperson_id].children[(*(int *)myimport("laser","id"))]=FALSE;
  all[followperson_id].children[(*(int *)myimport("localNavigation","id"))]=FALSE;
  ptmotorsstop();
  ptencodersstop();
  colorAstop();
	localNavStop();
	laserstop();
	 

  RGB2HSV_destroyTable();

  printf("followperson: off\n");
  pthread_mutex_unlock(&(all[followperson_id].mymutex));
}


void followperson_run(int father, int *brothers, arbitration fn)
{
  int i;


  /* update the father incorporating this schema as one of its children */
  if (father!=GUIHUMAN && father!=SHELLHUMAN)
  {
    pthread_mutex_lock(&(all[father].mymutex));
    all[father].children[followperson_id]=TRUE;
    pthread_mutex_unlock(&(all[father].mymutex));
  }

  pthread_mutex_lock(&(all[followperson_id].mymutex));
  /* this schema runs its execution with no children at all */
  for(i=0;i<MAX_SCHEMAS;i++) all[followperson_id].children[i]=FALSE;
  all[followperson_id].father=father;
  if (brothers!=NULL)
  {
    for(i=0;i<MAX_SCHEMAS;i++) followperson_brothers[i]=-1;
    i=0;
    while(brothers[i]!=-1) {followperson_brothers[i]=brothers[i];i++;}
  }

	followperson_imports();

	*golocalNav = 0;
  
  
  if (colorArun!=NULL)
	colorArun(followperson_id, NULL, NULL);
  
	
  
  followperson_callforarbitration=fn;
  put_state(followperson_id,notready);
  printf("followperson: on\n");
    

  RGB2HSV_init();
  RGB2HSV_createTable();

  pthread_cond_signal(&(all[followperson_id].condition));
  pthread_mutex_unlock(&(all[followperson_id].mymutex));
}





void *followperson_thread(void *not_used) 
{
  struct timeval a,b;
  long diff, next;

  for(;;)
    {
      pthread_mutex_lock(&(all[followperson_id].mymutex));

      if (all[followperson_id].state==slept) 
	{
	  pthread_cond_wait(&(all[followperson_id].condition),&(all[followperson_id].mymutex));
	  pthread_mutex_unlock(&(all[followperson_id].mymutex));
	}
      else 
	{
	  /* check preconditions. For now, preconditions are always satisfied*/
	  if (all[followperson_id].state==notready) put_state(followperson_id,ready);
	  else if (all[followperson_id].state==ready)	  /* check brothers and arbitrate. For now this is the only winner */
	    { 
	      put_state(followperson_id,winner);
  			*mylongitude_speed= 0.0;
  			*mylatitude_speed= 0.0;
	      /* start the winner state from controlled motor values */ 
	      all[followperson_id].children[(*(int *)myimport("ptencoders","id"))]=TRUE;
	      all[followperson_id].children[(*(int *)myimport("ptmotors","id"))]=TRUE;
	      all[followperson_id].children[(*(int *)myimport("laser","id"))]=TRUE;
	      all[followperson_id].children[(*(int *)myimport("localNavigation","id"))]=TRUE;
				localNavRun(followperson_id,NULL,NULL);			
	      ptencodersrun(followperson_id,NULL,NULL);
	      ptmotorsrun(followperson_id,NULL,NULL);
	     	laserrun(followperson_id,NULL,NULL);

	    }	  
	  else if (all[followperson_id].state==winner);

	  if (all[followperson_id].state==winner)
	    /* I'm the winner and must execute my iteration */
	    {
	      pthread_mutex_unlock(&(all[followperson_id].mymutex));
	      gettimeofday(&a,NULL);
	      followperson_iteration();
	      gettimeofday(&b,NULL);  

	      diff = (b.tv_sec-a.tv_sec)*1000000+b.tv_usec-a.tv_usec;
	      next = followperson_cycle*1000-diff-10000; 
	      if (next>0) 
		/* discounts 10ms taken by calling usleep itself */
		usleep(followperson_cycle*1000-diff);
	      else 
		/* just let this iteration go away. overhead time negligible */
		{/*printf("time interval violated: followperson\n"); */
		usleep(followperson_cycle*1000);
		}
	    }
	  else 
	    {
	      pthread_mutex_unlock(&(all[followperson_id].mymutex));
	      usleep(followperson_cycle*1000);
	    }
	}
    }
}

void followperson_guiinit(){
   if ((mydisplay= (Display *)myimport("graphics_xforms", "display"))==NULL){
      fprintf (stderr, "oplfow: I can't fetch display from graphics_xforms\n");
      jdeshutdown(1);
   }
   if ((myscreen= (int *)myimport("graphics_xforms", "screen"))==NULL){
      fprintf (stderr, "oplfow: I can't fetch screen from graphics_xforms\n");
      jdeshutdown(1);
   }

   if (myregister_buttonscallback==NULL){
      if ((myregister_buttonscallback=(registerbuttons)myimport ("graphics_xforms", "register_buttonscallback"))==NULL){
         printf ("opflow: I can't fetch register_buttonscallback from graphics_xforms\n");
         jdeshutdown(1);
      }
      if ((mydelete_buttonscallback=(deletebuttons)myimport ("graphics_xforms", "delete_buttonscallback"))==NULL){
         printf ("opflow: I can't fetch delete_buttonscallback from graphics_xforms\n");
         jdeshutdown(1);
      }
      if ((myregister_displaycallback=(registerdisplay)myimport ("graphics_xforms", "register_displaycallback"))==NULL){
         printf ("opflow: I can't fetch register_displaycallback from graphics_xforms\n");
         jdeshutdown(1);
      }
      if ((mydelete_displaycallback=(deletedisplay)myimport ("graphics_xforms", "delete_displaycallback"))==NULL){
         jdeshutdown(1);
         printf ("ofplow: I can't fetch delete_displaycallback from graphics_xforms\n");
      }
   }
}

void followperson_terminate()
{
  if (fd_followpersongui!=NULL)
    {
      if (all[followperson_id].guistate==on) 
	fl_hide_form(fd_followpersongui->followpersongui); 
      fl_free_form(fd_followpersongui->followpersongui);
    }
  printf ("followperson terminate\n");
}

void followperson_init(char *configfile)
{
  printf("followperson_init\n"); 
  draw_hsimap(disco_buf, SMAX);
  i_min=120.0; i_max=255.0;
  h_min=277.5; h_max=252.5;
  s_min=0.25; s_max=0.56;


  hsimap_threshold=20;

  pthread_mutex_lock(&(all[followperson_id].mymutex));
  printf("followperson schema started up\n");

  put_state(followperson_id,slept);

	rellenarDistanciasPeligro (); /* ventana que nos avisa del peligro*/

	/*Iniciamos el controlador para la lógica borrosa*/
	init_fuzzyController();
	
  followperson_guiinit();	
  
  pthread_create(&(all[followperson_id].mythread),NULL,followperson_thread,NULL);
  pthread_mutex_unlock(&(all[followperson_id].mymutex));
}


void followperson_guibuttons(void *obj2)
{
  double aux;
  FL_OBJECT *obj=(FL_OBJECT *)obj2;
  
  if (obj == fd_followpersongui-> btActiveHist )
  {
    if ( fl_get_button(fd_followpersongui->btActiveHist)==PUSHED )
      isActivatedShowHistogram=0;
    else
      isActivatedShowHistogram=0;
  }
  else if (obj == fd_followpersongui-> btActiveHSI )
  {
    if ( fl_get_button(fd_followpersongui->btActiveHSI)==PUSHED )
      isActivatedShowImageHSI=1;
    else
      isActivatedShowImageHSI=0;
  }
  else if (obj == fd_followpersongui->Hmax)
  {
    h_max=fl_get_slider_value(fd_followpersongui->Hmax);
    if(h_max>360.0)
    {
      h_max=h_max-360.0;
    }
    if(h_max<0)
    {
      h_max=h_max+360.0;
    }
    if (h_max==h_min)
    {
      fl_set_slider_value(fd_followpersongui->Smin,0.0);
      s_min=0.0;
    }
  }
  else if (obj == fd_followpersongui->Hmin)
  {
    h_min=fl_get_slider_value(fd_followpersongui->Hmin);
    if(h_min>360.0)
    {
      h_min=h_min-360.0;
    }
    if(h_max<0)
    {
      h_min=h_min+360.0;
    }
    if (h_max==h_min)
    {
      fl_set_slider_value(fd_followpersongui->Smin,0.0);
      s_min=0.0;
    }
  }

  else if (obj == fd_followpersongui->Smax)
  {
    s_max=fl_get_slider_value(fd_followpersongui->Smax);
    if (s_max<=s_min)
    {
      aux=s_min;
      s_min=s_max;
      s_max=aux;
      fl_set_slider_value(fd_followpersongui->Smax,s_max);
      fl_set_slider_value(fd_followpersongui->Smin,s_min);
    }
  }

  else if (obj == fd_followpersongui->Smin)
  {
    s_min=fl_get_slider_value(fd_followpersongui->Smin);
    if (s_max<=s_min)
    {
      aux=s_min;
      s_min=s_max;
      s_max=aux;
      fl_set_slider_value(fd_followpersongui->Smax,s_max);
      fl_set_slider_value(fd_followpersongui->Smin,s_min);
    }
  }

  else if (obj == fd_followpersongui->Imax)
  {
    i_max=fl_get_slider_value(fd_followpersongui->Imax);
    if (i_max<=i_min)
    {
      aux=i_min;
      i_min=i_max;
      i_max=aux;
      fl_set_slider_value(fd_followpersongui->Imax,i_max);
      fl_set_slider_value(fd_followpersongui->Imin,i_min);
    }
  }

  else if (obj == fd_followpersongui->Imin)
  {
    i_min=fl_get_slider_value(fd_followpersongui->Imin);
    if (i_max<=i_min)
    {
      aux=i_min;
      i_min=i_max;
      i_max=aux;
      fl_set_slider_value(fd_followpersongui->Imax,i_max);
      fl_set_slider_value(fd_followpersongui->Imin,i_min);
    }
  }

  else if (obj ==fd_followpersongui->w_slider)
  {
    hsimap_threshold=(int)fl_get_slider_value(fd_followpersongui->w_slider);
    /*printf("hsimap_threshold %d\n",hsimap_threshold);*/
  }
  if (obj ==fd_followpersongui->toblack)
  {
    if (fl_get_button(fd_followpersongui->toblack)==RELEASED)
    {
      toblack=0;
    }
    else
      toblack=1;
  }
	if (obj ==fd_followpersongui->followbutton)
  {
    if (fl_get_button(fd_followpersongui->followbutton)==RELEASED)
    {
      follow=0;
    }
    else
      follow=1;
  }
}


void followperson_guidisplay()
{
  int vmode,i;
  
  vmode= fl_get_vclass();
  
  if ( (vmode==TrueColor)&&(fl_state[vmode].depth==24 || fl_state[vmode].depth==32) )
  {
	  for(i=0; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS; i++)
	  {					 
		  imagenOrig_buf[i*4]= (*mycolorA)[3*i];
		  imagenOrig_buf[i*4+1]= (*mycolorA)[3*i+1];
		  imagenOrig_buf[i*4+2]= (*mycolorA)[3*i+2];
		  imagenOrig_buf[i*4+3]=0; 
	  }
		
  }
  drawcheese(histograma_buf,centro_x,centro_y,h_max,h_min,s_max,s_min,FL_PALEGREEN);

  XPutImage(mydisplay,followperson_win,followperson_gc,imagenOrig,0,0,fd_followpersongui->oculo_orig->x, fd_followpersongui->oculo_orig->y,  SIFNTSC_COLUMNS, SIFNTSC_ROWS);

  XPutImage(mydisplay,followperson_win,followperson_gc,hsifiltrada,0,0,fd_followpersongui->oculo_modif->x, fd_followpersongui->oculo_modif->y,  SIFNTSC_COLUMNS, SIFNTSC_ROWS);

  XPutImage(mydisplay,followperson_win,followperson_gc,histograma,0,0,fd_followpersongui->histograma->x, fd_followpersongui->histograma->y, SMAX, SMAX);
}


void followperson_hide_aux(void)
{
  all[followperson_id].guistate=off;
  mydelete_buttonscallback(followperson_guibuttons);
  mydelete_displaycallback(followperson_guidisplay);
  fl_hide_form(fd_followpersongui->followpersongui);
}

void followperson_hide(void){
   static callback fn=NULL;
   if (fn==NULL){
      if ((fn=(callback)myimport ("graphics_xforms", "suspend_callback"))!=NULL){
         fn ((gui_function)followperson_hide_aux);
      }
   }
   else{
      fn ((gui_function)followperson_hide_aux);
   }
}

int myclose_form(FL_FORM *form, void *an_argument)
{
  followperson_hide();
  return FL_IGNORE;
}

void followperson_show_aux(void)
{
  static int k=0;

  all[followperson_id].guistate=on;
  if (k==0)                      /* not initialized */
  {
    k++;
    fd_followpersongui = create_form_followpersongui();
    fl_set_form_position(fd_followpersongui->followpersongui,400,50);
    fl_show_form(fd_followpersongui->followpersongui,FL_PLACE_POSITION,FL_FULLBORDER,followpersonVER);
    fl_set_form_atclose(fd_followpersongui->followpersongui,myclose_form,0);
    followperson_win= FL_ObjWin(fd_followpersongui->oculo_orig);
    followpersongui_setupDisplay();
  }
  else
  {
    fl_show_form(fd_followpersongui->followpersongui,FL_PLACE_POSITION,FL_FULLBORDER,followpersonVER);
    followperson_win= FL_ObjWin(fd_followpersongui->oculo_orig);
  }
  

  myregister_buttonscallback(followperson_guibuttons);
  myregister_displaycallback(followperson_guidisplay);

  /* HSV Values for pink ball */
  i_min=120.0; i_max=255.0;
  h_min=277.5; h_max=252.5;
  s_min=0.25; s_max=0.56;

  /* HSI Values for red ball */
  /*
  i_min=49.0; i_max=150.3;
  h_min=240.0; h_max=280.0;
  s_min=0.43; s_max=0.80;
  */
  fl_set_slider_bounds(fd_followpersongui->w_slider,100,1);
  fl_set_slider_value(fd_followpersongui->w_slider,hsimap_threshold);
  fl_set_slider_bounds(fd_followpersongui->Imin,0,255);
  fl_set_slider_value(fd_followpersongui->Imin,i_min);
  fl_set_slider_bounds(fd_followpersongui->Imax,0,255);
  fl_set_slider_value(fd_followpersongui->Imax,i_max);
  fl_set_slider_bounds(fd_followpersongui->Hmin,360,0);
  fl_set_slider_value(fd_followpersongui->Hmin,h_min);
  fl_set_slider_bounds(fd_followpersongui->Hmax,360,0);
  fl_set_slider_value(fd_followpersongui->Hmax,h_max);
  fl_set_slider_bounds(fd_followpersongui->Smin,1,0);
  fl_set_slider_value(fd_followpersongui->Smin,s_min);
  fl_set_slider_bounds(fd_followpersongui->Smax,1,0);
  fl_set_slider_value(fd_followpersongui->Smax,s_max);
}

void followperson_show(void){
   static callback fn=NULL;
   if (fn==NULL){
      if ((fn=(callback)myimport ("graphics_xforms", "resume_callback"))!=NULL){
         fn ((gui_function)followperson_show_aux);
      }
   }
   else{
      fn ((gui_function)followperson_show_aux);
   }
}

int handle2 (FL_OBJECT *obj, int event, FL_Coord mx, FL_Coord my, int key, void *xev)
{
  int x_matriz,y_matriz;
  unsigned char r,g,b;
  double H,S,I;
  struct HSV* myHSV;

  //if (!isActivatedShowHistogram)
  // 	return 0;

  if (event==FL_PUSH)
  {
    if ((my>=10) && (my<=250) && (mx>=430) && (mx<=750))
    {
      x_matriz=mx-430;
      y_matriz=my-10;
      r=imagenOrig_buf[(SIFNTSC_COLUMNS*y_matriz+x_matriz)*4+2];
      g=imagenOrig_buf[(SIFNTSC_COLUMNS*y_matriz+x_matriz)*4+1];
      b=imagenOrig_buf[(SIFNTSC_COLUMNS*y_matriz+x_matriz)*4];

      //rgb2hsi2(r/255.0,g/255.0,b/255.0,&H,&S,&I);

      myHSV = (struct HSV*) RGB2HSV_getHSV((int)r,(int)g,(int)b);
      H=myHSV->H;
      S=myHSV->S;
      I=myHSV->V;

	  //printf("(%d,%d,%d) = (%.1f,%.1f,%.1f) \n",r,g,b,H,S,I);

      h_max=H+20.0;
      h_min=H-20.0;
      if (h_max>360.0)
      {
        h_max=h_max-360.0;
      }
      if (h_min<0)
      {
        h_min=h_min+360.0;
      }

      s_max=S+.1;
      s_min=S-.1;
      if (s_max>1.)
      {
        s_max=1.;
      }
      if (s_min<0)
      {
        s_min=0;
      }

      i_max=I+50.;
      i_min=I-50.;
      if (i_max>255.)
      {
        i_max=255.;
      }
      if (i_min<0.)
      {
        i_min=0.;
      }
      fl_set_slider_value(fd_followpersongui->Hmax,h_max);
      fl_set_slider_value(fd_followpersongui->Hmin,h_min);
      fl_set_slider_value(fd_followpersongui->Smax,s_max);
      fl_set_slider_value(fd_followpersongui->Smin,s_min);
      fl_set_slider_value(fd_followpersongui->Imax,i_max);
      fl_set_slider_value(fd_followpersongui->Imin,i_min);
    }

  }
  return 0;
}


int handle (FL_OBJECT *obj, int event, FL_Coord mx, FL_Coord my, int key, void *xev)
{

  if (!isActivatedShowHistogram)
    return 0;

  if(event==FL_DBLCLICK)
  {
    
    x_pulsada=mx-10;y_pulsada=my-10;
    x_pulsada=x_pulsada-centro_x;
    y_pulsada=centro_y-y_pulsada;
    if (x_pulsada==0)
    {
      if (y_pulsada<0)
      {
        h_max=3.1416/2.;
      }
      else
      {
        h_max=3.*3.1416/2.;
      }
    }
    else
    {
      if (y_pulsada==0)
      {
        if (x_pulsada<0)
        {
          h_max=3.1416;
        }
        else
        {
          h_max=0.;
        }
      }
      else
      {
        if (x_pulsada>0)
        {
          h_max=atan((float)(y_pulsada)/(float)x_pulsada);
          if(y_pulsada<0)
          {
            h_max=h_max+2*3.1416;
          }
        }
        else
        {
          h_max=atan((float)y_pulsada/(float)x_pulsada)+3.1416;
        }
      }
    }
    h_max=h_max+20.0;
    h_min=h_max-40.0;
    if (h_max>360.0)
    {
      h_max=h_max-360.0;
    }
    if (h_min<0)
    {
      h_min=h_min+360.0;
    }
    s_max=(float)sqrt((x_pulsada*x_pulsada)+(y_pulsada*y_pulsada))/radio_hsi_map;
    s_max=s_max+.1;
    s_min=s_max-.2;
    if (s_max>1.)
    {
      s_max=1.;
    }
    if (s_min<0.)
    {
      s_min=0.;
    }
    /*CAMBIO LOS VALORES DE LOS SLIDERS*/
    fl_set_slider_value(fd_followpersongui->Hmax,h_max);
    fl_set_slider_value(fd_followpersongui->Hmin,h_min);
    fl_set_slider_value(fd_followpersongui->Smax,s_max);
    fl_set_slider_value(fd_followpersongui->Smin,s_min);
  }
  xquesito1=cos(h_max)*s_max*radio_hsi_map+centro_x;
  yquesito1=centro_y-sin(h_max)*s_max*radio_hsi_map;

  xquesito2=cos(h_min)*s_max*radio_hsi_map+centro_x;
  yquesito2=centro_y-sin(h_min)*s_max*radio_hsi_map;

  xquesito3=cos(h_max)*s_min*radio_hsi_map+centro_x;
  yquesito3=centro_y-sin(h_max)*s_min*radio_hsi_map;

  xquesito4=cos(h_min)*s_min*radio_hsi_map+centro_x;
  yquesito4=centro_y-sin(h_min)*s_min*radio_hsi_map;

  if (event==FL_PUSH)
  {
    /*printf("H_max:%f,H_min:%f,S_max:%f,S_min:%f\n",h_max,h_min,s_max,s_min);*/
    if (((mx-10)<=xquesito1+5) && ((mx-10)>=xquesito1-5))
    {
      if (((my-10)<=yquesito1+5) && ((my-10)>=yquesito1-5))
      {
        x_pulsada=mx-10;y_pulsada=my-10;
        pulsada=1;
      }
    }
    if (((mx-10)<=xquesito2+5) && ((mx-10)>=xquesito2-5))
    {
      if (((my-10)<=yquesito2+5) && ((my-10)>=yquesito2-5))
      {
        x_pulsada=mx-10;y_pulsada=my-10;
        pulsada=2;
      }
    }
    if (((mx-10)<=xquesito3+5) && ((mx-10)>=xquesito3-5))
    {
      if (((my-10)<=yquesito3+5) && ((my-10)>=yquesito3-5))
      {
        x_pulsada=mx-10;y_pulsada=my-10;
        pulsada=3;
      }
    }
    if (((mx-10)<=xquesito4+5) && ((mx-10)>=xquesito4-5))
    {
      if (((my-10)<=yquesito4+5) && ((my-10)>=yquesito4-5))
      {
        x_pulsada=mx-10;y_pulsada=my-10;
        pulsada=4;
      }
    }
    if (h_max==h_min)
    {
      x_pulsada=mx-10;
      y_pulsada=my-10;
      x_pulsada=x_pulsada-centro_x;
      y_pulsada=centro_y-y_pulsada;
      if ((float)sqrt((x_pulsada*x_pulsada)+(y_pulsada*y_pulsada))/radio_hsi_map<=1)
      {
        s_max=(float)sqrt((x_pulsada*x_pulsada)+(y_pulsada*y_pulsada))/radio_hsi_map;
        pulsada=0;
        s_min=0.;
        fl_set_slider_value(fd_followpersongui->Smax,s_max);
        fl_set_slider_value(fd_followpersongui->Smin,s_min);
      }
    }

  }
  if (event==FL_MOUSE)
  {
    if (pulsada>0)
    {
      xsoltada=mx-10;ysoltada=my-10;
      if (sqrt((xsoltada-centro_x)*(xsoltada-centro_x)+(ysoltada-centro_y)*(ysoltada-centro_y))<=radio_hsi_map)
      {
        xsoltada=xsoltada-centro_x;
        ysoltada=centro_y-ysoltada;
        switch(pulsada)
        {
          case 1:
            s_max=(float)sqrt((xsoltada*xsoltada)+(ysoltada*ysoltada))/radio_hsi_map;
            if (xsoltada==0)
            {
              if (ysoltada<0)
              {
                h_max=3*3.1416/2.;
              }
              else
              {
                h_max=3.1416/2.;
              }
            }
            else
            {
              if (ysoltada==0)
              {
                if (xsoltada<0)
                {
                  h_max=3.1416;
                }
                else
                {
                  h_max=0.;
                }
              }
              else
              {
                if (xsoltada>0)
                {
                  h_max=atan((float)ysoltada/(float)xsoltada);
                  if(ysoltada<0)
                  {
                    h_max=h_max+2*3.1416;
                  }
                }
                else
                {
                  h_max=atan((float)ysoltada/(float)xsoltada)+3.1416;
                }
              }
            }
            break;
          case 2:
            s_max=(float)sqrt((xsoltada*xsoltada)+(ysoltada*ysoltada))/radio_hsi_map;
            if (xsoltada==0)
            {
              if (ysoltada<0)
              {
                h_min=3*3.1416/2.;
              }
              else
              {
                h_min=3.1416/2.;
              }
            }
            else
            {
              if (ysoltada==0)
              {
                if (xsoltada<0)
                {
                  h_min=3.1416;
                }
                else
                {
                  h_min=0.;
                }
              }
              else
              {
                if (xsoltada>0)
                {
                  h_min=atan((float)ysoltada/(float)xsoltada);
                  if(ysoltada<0)
                  {
                    h_min=h_min+2*3.1416;
                  }
                }
                else
                {
                  h_min=atan((float)ysoltada/(float)xsoltada)+3.1416;
                }
              }
            }
            break;
          case 3:
            s_min=(float)sqrt((xsoltada*xsoltada)+(ysoltada*ysoltada))/radio_hsi_map;
            if (xsoltada==0)
            {
              if (ysoltada<0)
              {
                h_max=3*3.1416/2.;
              }
              else
              {
                h_max=3.1416/2.;
              }
            }
            else
            {
              if (ysoltada==0)
              {
                if (xsoltada<0)
                {
                  h_max=3.1416;
                }
                else
                {
                  h_max=0.;
                }
              }
              else
              {
                if (xsoltada>0)
                {
                  h_max=atan((float)ysoltada/(float)xsoltada);
                  if(ysoltada<0)
                  {
                    h_max=h_max+2*3.1416;
                  }
                }
                else
                {
                  h_max=atan((float)ysoltada/(float)xsoltada)+3.1416;
                }
              }
            }

            break;
          case 4:
            s_min=(float)sqrt((xsoltada*xsoltada)+(ysoltada*ysoltada))/radio_hsi_map;
            if (xsoltada==0)
            {
              if (ysoltada<0)
              {
                h_min=3*3.1416/2.;
              }
              else
              {
                h_min=3.1416/2.;
              }
            }
            else
            {
              if (ysoltada==0)
              {
                if (xsoltada<0)
                {
                  h_min=3.1416;
                }
                else
                {
                  h_min=0.;
                }
              }
              else
              {
                if (xsoltada>0)
                {
                  h_min=atan((float)ysoltada/(float)xsoltada);
                  if(ysoltada<0)
                  {
                    h_min=h_min+2*3.1416;
                  }
                }
                else
                {
                  h_min=atan((float)ysoltada/(float)xsoltada)+3.1416;
                }
              }
            }
            break;
          default:
            break;
        }
        fl_set_slider_value(fd_followpersongui->Hmax,h_max);
        fl_set_slider_value(fd_followpersongui->Hmin,h_min);
        fl_set_slider_value(fd_followpersongui->Smax,s_max);
        fl_set_slider_value(fd_followpersongui->Smin,s_min);
      }
    }
  }
  if (event==FL_RELEASE)
  {
    pulsada=0;
  }
  return 0;
}
