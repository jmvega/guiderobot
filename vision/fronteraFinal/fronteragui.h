/** Header file generated with fdesign on Tue Jan 13 14:51:45 2009.**/

#ifndef FD_fronteragui_h_
#define FD_fronteragui_h_

/** Callbacks, globals and object handlers **/
extern int freeobj_ventanaAA_handle(FL_OBJECT *, int, FL_Coord, FL_Coord,
			int, void *);
extern int freeobj_filteredImage_handle(FL_OBJECT *, int, FL_Coord, FL_Coord,
			int, void *);
extern int freeobj_groundImage_handle(FL_OBJECT *, int, FL_Coord, FL_Coord,
			int, void *);


/**** Forms and Objects ****/
typedef struct {
	FL_FORM *fronteragui;
	void *vdata;
	char *cdata;
	long  ldata;
	FL_OBJECT *hide;
	FL_OBJECT *ventanaA;
	FL_OBJECT *colorA;
	FL_OBJECT *filteredImage;
	FL_OBJECT *groundImage;
	FL_OBJECT *minicanvas;
	FL_OBJECT *canvasButton;
	FL_OBJECT *fronteraButton;
	FL_OBJECT *fronteracanvas;
	FL_OBJECT *go1;
	FL_OBJECT *go50;
	FL_OBJECT *turnRight45;
	FL_OBJECT *turnLeft45;
	FL_OBJECT *turnRight90;
	FL_OBJECT *turnLeft90;
	FL_OBJECT *flashButton;
} FD_fronteragui;

extern FD_fronteragui * create_form_fronteragui(void);

#endif /* FD_fronteragui_h_ */
