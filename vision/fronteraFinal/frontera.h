#include <GL/gl.h>              
#include <GL/glx.h>
#include <GL/glu.h>
#include <GL/freeglut_std.h>
#include <forms.h>

#include "jde.h"
#include <glcanvas.h>
#include "susan.h"
#include "graphics_xforms.h"
#include "fronteragui.h"
#include "pioneer.h"
#include "progeo.h"
#include "colorspaces.h"
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_multifit.h>
#include "gridslib.h"

/* GRAPHIC coordenates to OPTICAL coordenates */
#define GRAPHIC_TO_OPTICAL_X(x,y) (SIFNTSC_ROWS-1-y)
#define GRAPHIC_TO_OPTICAL_Y(x,y) (x)
#define OPTICAL_TO_GRAPHIC_X(x,y) (y)
#define OPTICAL_TO_GRAPHIC_Y(x,y) (SIFNTSC_ROWS-1-x)
#define ROOM_MAX_X 7925.

extern void frontera_startup();
extern void frontera_suspend();
extern void teleoperator_resume(int father, int *brothers, arbitration fn);
extern int frontera_cycle;

// Estructura para definir un plano en el espacio
typedef struct {
	float ax;
	float by;
	float cz;
	float d;
} TPlano;
