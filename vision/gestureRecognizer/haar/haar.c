/*
 *  Copyright (C) 2007 Javier Martín Ramos 
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  Authors : Javier Martín Ramos <j.ramos@pantuflo.es>
 */

#include "haar.h"

int haar_id=0;
int haar_brothers[MAX_SCHEMAS];
arbitration haar_callforarbitration;

enum haar_states {init,t1,r1,t2,r2,t3,r3,t4,end};
static int haar_state;
FD_haargui *fd_haargui;

#define MIN_EXP_WIDTH 20
#define MIN_EXP_HEIGHT 20

static CvMemStorage* storage = 0;
static CvMemStorage* storageTmp = 0;
static CvMemStorage* storageAux = 0;
static CvHaarClassifierCascade* cascade = 0;
const char* cascade_name = "haarcascade_frontalface_alt.xml";
IplImage *imgLocal = NULL;
CvMat img; // Envoltura a mycolorA
int display_active = 0;
CvSeq *facesTmp, *facesAux;
double detectionTimeAux;

/* imported variables*/
int mysoncolorA;
unsigned char *mycolorA=NULL;
runFn mycolorAresume;
stopFn mycolorAsuspend;
int mycolorAheight;
int mycolorAwidth;
int mycolorAnChannels;

/* exported variables */
int haar_cycle=100; /* ms */
double detectionTime;
CvSeq *faces;
pthread_mutex_t haarMutex;
unsigned long int facesClock;

inline void detectaCaras(IplImage *img) {
  static unsigned long int lastClock = 0;
  cvClearMemStorage( storageTmp );

  if( cascade && img )
  {
    detectionTimeAux = (double) cvGetTickCount();
    facesTmp = cvHaarDetectObjects( img, cascade, storageTmp,
                                  1.1, 2, CV_HAAR_DO_CANNY_PRUNING,
                                  cvSize(MIN_EXP_WIDTH, MIN_EXP_HEIGHT) );
    detectionTimeAux = (double) cvGetTickCount() - detectionTimeAux;
    detectionTimeAux =  detectionTimeAux/((double)cvGetTickFrequency()*1000.);

    pthread_mutex_lock(&haarMutex);
    facesClock = lastClock;
    lastClock++;
    pthread_mutex_unlock(&haarMutex);
  }
}

inline void dibujaCaras(IplImage* dst) {
   int scale = 1;
   CvRect* r;
   CvPoint pt1, pt2;
   int i;
   for( i = 0; i < (facesTmp ? facesTmp->total : 0); i++ ) {
      r = (CvRect*)cvGetSeqElem(facesTmp, i);
//       pt1.x = r->x*scale;
//       pt2.x = (r->x+r->width)*scale;
//       pt1.y = r->y*scale;
//       pt2.y = (r->y+r->height)*scale;
//       cvRectangle(dst, pt1, pt2, CV_RGB(0,0,255), 4, 8, 0);
      float scale = 1.3;
      CvPoint center;
      int radius;
      center.x = cvRound((r->x + r->width*0.5));
      center.y = cvRound((r->y + r->height*0.5));
      radius = cvRound((r->width + r->height)*0.25*scale);
      cvCircle( dst, center, radius, CV_RGB(255,0,0), 2, 8, 0 );
   }
}

void haar_iteration(){
   static char d = 0;
   static unsigned char *mycolorATmp = NULL;
   speedcounter(haar_id);

   if (d == 0) {
      cascade = (CvHaarClassifierCascade*)cvLoad( cascade_name, 0, 0, 0 );
      if (!cascade) {
         fprintf(stderr, "Haar: Could not load classifier cascade\n");
         jdeshutdown(1);
      }
      storage = cvCreateMemStorage(0);
      storageTmp = cvCreateMemStorage(0);
   }

   pthread_mutex_lock(&haarMutex);
   if (mycolorA != NULL) {
      if (mycolorA != mycolorATmp) {
         // Source image has been changed
         int type;
//          cvReleaseMat(&img);
         switch (mycolorAnChannels) {
            case 1: type = CV_8UC1;
                    break;
            case 3: type = CV_8UC3;
                    break;
            case 4: type = CV_8UC4;
                    break;
            default: fprintf(stderr, "Haar: Incorrect number of channels for input image\n");
                     jdeshutdown(1);
         }
         img = cvMat(mycolorAheight, mycolorAwidth, type, mycolorA);
         if (imgLocal != NULL)
         cvReleaseImage(&imgLocal);
         imgLocal = cvCreateImage(cvSize(mycolorAwidth, mycolorAheight), IPL_DEPTH_8U, 1);
         mycolorATmp = mycolorA;
      }

      if (imgLocal != NULL) {
         switch (mycolorAnChannels) {
            case 1: cvCopy(&img, imgLocal, NULL);
                    break;
            case 3: cvCvtColor(&img, imgLocal, CV_BGR2GRAY);
                    break;
            case 4: cvCvtColor(&img, imgLocal, CV_BGRA2GRAY);
                    break;
         }
//          printf("#haar detecta caras\n"); fflush(NULL);
         pthread_mutex_unlock(&haarMutex);
         detectaCaras(imgLocal);
         if (display_active) {
            dibujaCaras(imgLocal);
            cvShowImage( "haar", imgLocal);
            cvWaitKey(10);
         }
      }
      else
         pthread_mutex_unlock(&haarMutex);
   }
   pthread_mutex_lock(&haarMutex);
   detectionTime = detectionTimeAux;
   CV_SWAP(faces, facesTmp, facesAux);
   CV_SWAP(storage, storageTmp, storageAux);
   pthread_mutex_unlock(&haarMutex);
   d++;
}


/*Importar símbolos*/
void haar_imports(){
   void *aux = NULL;

   aux = myimport("closeup", "img");
   if (aux)
      mycolorA=*(unsigned char **) aux;
   else
      jdeshutdown(1);
   aux = myimport("closeup","height");
   if (aux)
      mycolorAheight = *(int *)aux;
   else
      jdeshutdown(1);
   aux = myimport("closeup","width");
   if (aux)
      mycolorAwidth =*(int *)aux;
   else
      jdeshutdown(1);
   aux = myimport("closeup", "nChannels");
   if (aux)
      mycolorAnChannels = *(int *)aux;
   else
      mycolorAnChannels = 3;
}

/*Exportar símbolos*/
void haar_exports(){
  myexport("haar","id",&haar_id);
  myexport("haar","cycle",&haar_cycle);
  myexport("haar","run",(void *)haar_run);
  myexport("haar","stop",(void *)haar_stop);
  myexport("haar", "detectionTime", &detectionTime);
  myexport("haar", "faces", &faces);
  myexport("haar", "haarMutex", &haarMutex);
  myexport("haar", "facesClock", &facesClock);
}

/*Las inicializaciones van en esta parte*/
void haar_init2(){
  pthread_mutex_init(&haarMutex,PTHREAD_MUTEX_TIMED_NP);
}

/*Al suspender el esquema*/
void haar_fin(){
//    if (mycolorAsuspend != NULL)
//       mycolorAsuspend();
}


void haar_stop()
{
  /* printf("haar: cojo-suspend\n");*/
  pthread_mutex_lock(&(all[haar_id].mymutex));
  put_state(haar_id,slept);
  printf("haar: off\n");
  pthread_mutex_unlock(&(all[haar_id].mymutex));
  /*  printf("haar: suelto-suspend\n");*/
  haar_fin();
}


void haar_run(int father, int *brothers, arbitration fn)
{
  int i;

  /* update the father incorporating this schema as one of its children */
  if (father!=GUIHUMAN && father!=SHELLHUMAN)
    {
      pthread_mutex_lock(&(all[father].mymutex));
      all[father].children[haar_id]=TRUE;
      pthread_mutex_unlock(&(all[father].mymutex));
    }
  pthread_mutex_lock(&(all[haar_id].mymutex));
  /* this schema resumes its execution with no children at all */
  for(i=0;i<MAX_SCHEMAS;i++) all[haar_id].children[i]=FALSE;
  all[haar_id].father=father;
  if (brothers!=NULL)
    {
      for(i=0;i<MAX_SCHEMAS;i++) haar_brothers[i]=-1;
      i=0;
      while(brothers[i]!=-1) {haar_brothers[i]=brothers[i];i++;}
    }
  haar_callforarbitration=fn;
  put_state(haar_id,notready);
  printf("haar: on\n");
  haar_imports();
//   all[haar_id].children[mysoncolorA]=TRUE;
//   mycolorAresume(haar_id,NULL,NULL);
  pthread_cond_signal(&(all[haar_id].condition));
  pthread_mutex_unlock(&(all[haar_id].mymutex));
}

void *haar_thread(void *not_used)
{
   struct timeval a,b;
   long n=0; /* iteration */
   long next,bb,aa;

   for(;;)
   {
      pthread_mutex_lock(&(all[haar_id].mymutex));

      if (all[haar_id].state==slept)
      {
   haar_state=init;
   pthread_cond_wait(&(all[haar_id].condition),&(all[haar_id].mymutex));
   pthread_mutex_unlock(&(all[haar_id].mymutex));
      }
      else
      {
   /* check preconditions. For now, preconditions are always satisfied*/
   if (all[haar_id].state==notready)
      put_state(haar_id,ready);
   /* check brothers and arbitrate. For now this is the only winner */
   if (all[haar_id].state==ready)
   {put_state(haar_id,winner);
   gettimeofday(&a,NULL);
   aa=a.tv_sec*1000000+a.tv_usec;
   n=0;
   }

   if (all[haar_id].state==winner)
      /* I'm the winner and must execute my iteration */
   {
      pthread_mutex_unlock(&(all[haar_id].mymutex));
      /*      gettimeofday(&a,NULL);*/
      n++;
      haar_iteration();
      gettimeofday(&b,NULL);
      bb=b.tv_sec*1000000+b.tv_usec;
      pthread_mutex_lock(&haarMutex);
      next=aa+(n+1)*(long)haar_cycle*1000-bb;
      pthread_mutex_unlock(&haarMutex);

      if (next>5000)
      {
         usleep(next-5000);
         /* discounts 5ms taken by calling usleep itself, on average */
      }
      else  ;
   }
   else
      /* just let this iteration go away. overhead time negligible */
   {
      pthread_mutex_unlock(&(all[haar_id].mymutex));
      pthread_mutex_lock(&haarMutex);
      usleep(haar_cycle*1000);
      pthread_mutex_unlock(&haarMutex);
   }
      }
   }
}

void haar_init()
{
  pthread_mutex_lock(&(all[haar_id].mymutex));
  printf("haar schema started up\n");
  haar_exports();
  put_state(haar_id,slept);
  pthread_create(&(all[haar_id].mythread),NULL,haar_thread,NULL);
  pthread_mutex_unlock(&(all[haar_id].mymutex));
  haar_init2();
}

void haar_guibuttons(FL_OBJECT *obj){
}

void haar_guidisplay(){
//   char text[80]="";
//   static float k=0;
//   k=k+1.;
//   sprintf(text,"it %.1f",k);
//   fl_set_object_label(fd_haargui->fps,text);
//    pthread_mutex_lock(&haarMutex);
//    dibujaCaras(imgLocal);
//    pthread_mutex_unlock(&haarMutex);
//    cvShowImage( "haar", imgLocal);
//    cvWaitKey(10);
}


void haar_hide(void){
//   delete_buttonscallback(haar_guibuttons);
  delete_displaycallback(haar_guidisplay);
//   fl_hide_form(fd_haargui->haargui);
   cvDestroyWindow("haar");
   display_active = 0;
}

void haar_show(void){
  static int k=0;

  if (k==0) /* not initialized */
    {
      k++;
      fd_haargui = create_form_haargui();
      fl_set_form_position(fd_haargui->haargui,400,50);
    }
//   register_buttonscallback(haar_guibuttons);
  register_displaycallback(haar_guidisplay);
//  fl_show_form(fd_haargui->haargui,FL_PLACE_POSITION,FL_FULLBORDER,"haar");
   cvNamedWindow( "haar", 1 );
   display_active = 1;
}
