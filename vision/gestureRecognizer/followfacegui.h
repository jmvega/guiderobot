/** Header file generated with fdesign on Tue Feb 12 09:21:13 2008.**/

#ifndef FD_followfacegui_h_
#define FD_followfacegui_h_

/** Callbacks, globals and object handlers **/


/**** Forms and Objects ****/
typedef struct {
	FL_FORM *followfacegui;
	void *vdata;
	char *cdata;
	long  ldata;
	FL_OBJECT *_joystick;
	FL_OBJECT *_panSpeed;
	FL_OBJECT *_tiltSpeed;
	FL_OBJECT *_zoomSpeed;
	FL_OBJECT *_zoom;
	FL_OBJECT *_haar;
	FL_OBJECT *_neuralnet;
	FL_OBJECT *_intproy;
	FL_OBJECT *hmin;
	FL_OBJECT *hmax;
	FL_OBJECT *smin;
	FL_OBJECT *smax;
	FL_OBJECT *vmin;
	FL_OBJECT *vmax;
} FD_followfacegui;

extern FD_followfacegui * create_form_followfacegui(void);

#endif /* FD_followfacegui_h_ */
