/* Form definition file generated with fdesign. */

#include "forms.h"
#include <stdlib.h>
#include "followfacegui.h"

FD_followfacegui *create_form_followfacegui(void)
{
  FL_OBJECT *obj;
  FD_followfacegui *fdui = (FD_followfacegui *) fl_calloc(1, sizeof(*fdui));

  fdui->followfacegui = fl_bgn_form(FL_NO_BOX, 708, 206);
  obj = fl_add_box(FL_UP_BOX,0,0,708,206,"");
    fl_set_object_lcolor(obj,FL_BLUE);
  fdui->_joystick = obj = fl_add_positioner(FL_NORMAL_POSITIONER,170,20,170,90,"PT position");
  fdui->_panSpeed = obj = fl_add_counter(FL_NORMAL_COUNTER,20,20,140,30,"Pan Speed");
    fl_set_counter_precision(obj, 2);
    fl_set_counter_bounds(obj, 0.00, 1.00);
    fl_set_counter_step(obj, 0.01, 0.10);
  fdui->_tiltSpeed = obj = fl_add_counter(FL_NORMAL_COUNTER,20,80,140,30,"Tilt Speed");
    fl_set_counter_precision(obj, 2);
    fl_set_counter_bounds(obj, 0.00, 1.00);
    fl_set_counter_step(obj, 0.01, 0.10);
  fdui->_zoomSpeed = obj = fl_add_counter(FL_NORMAL_COUNTER,20,140,140,30,"Zoom Speed");
    fl_set_counter_precision(obj, 2);
    fl_set_counter_bounds(obj, 0.00, 1.00);
    fl_set_counter_step(obj, 0.01, 0.10);
  fdui->_zoom = obj = fl_add_counter(FL_NORMAL_COUNTER,170,140,170,30,"Zoom Position");
    fl_set_counter_precision(obj, 2);
    fl_set_counter_bounds(obj, 0.00, 1.00);
    fl_set_counter_step(obj, 0.01, 0.10);
  fdui->_haar = obj = fl_add_checkbutton(FL_PUSH_BUTTON,355,21,34,30,"Haar");
  fdui->_neuralnet = obj = fl_add_checkbutton(FL_PUSH_BUTTON,355,55,34,30,"NeuralNet");
  fdui->_intproy = obj = fl_add_checkbutton(FL_PUSH_BUTTON,355,89,34,30,"IntProy");
  fdui->hmin = obj = fl_add_counter(FL_NORMAL_COUNTER,457,18,110,20,"H minimo");
    fl_set_counter_precision(obj, 0);
    fl_set_counter_bounds(obj, 0, 180);
    fl_set_counter_step(obj, 1, 10);
  fdui->hmax = obj = fl_add_counter(FL_NORMAL_COUNTER,577,18,110,20,"H maximo");
    fl_set_counter_precision(obj, 0);
    fl_set_counter_bounds(obj, 0, 180);
    fl_set_counter_step(obj, 1, 10);
  fdui->smin = obj = fl_add_counter(FL_NORMAL_COUNTER,457,58,110,20,"S minimo");
    fl_set_counter_precision(obj, 0);
    fl_set_counter_bounds(obj, 0, 256);
    fl_set_counter_step(obj, 1, 10);
  fdui->smax = obj = fl_add_counter(FL_NORMAL_COUNTER,577,58,110,20,"S maximo");
    fl_set_counter_precision(obj, 0);
    fl_set_counter_bounds(obj, 0, 256);
    fl_set_counter_step(obj, 1, 10);
  fdui->vmin = obj = fl_add_counter(FL_NORMAL_COUNTER,457,98,110,20,"V minimo");
    fl_set_counter_precision(obj, 0);
    fl_set_counter_bounds(obj, 0, 256);
    fl_set_counter_step(obj, 1, 10);
  fdui->vmax = obj = fl_add_counter(FL_NORMAL_COUNTER,577,98,110,20,"V maximo");
    fl_set_counter_precision(obj, 0);
    fl_set_counter_bounds(obj, 0, 256);
    fl_set_counter_step(obj, 1, 10);
  fl_end_form();

  fdui->followfacegui->fdui = fdui;

  return fdui;
}
/*---------------------------------------*/

