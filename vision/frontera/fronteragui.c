/* Form definition file generated with fdesign. */

#include "forms.h"
#include <stdlib.h>
#include "fronteragui.h"

FD_fronteragui *create_form_fronteragui(void)
{
  FL_OBJECT *obj;
  FD_fronteragui *fdui = (FD_fronteragui *) fl_calloc(1, sizeof(*fdui));

  fdui->fronteragui = fl_bgn_form(FL_NO_BOX, 1200, 810);
  obj = fl_add_box(FL_FRAME_BOX,0,0,1200,810,"");
    fl_set_object_lcolor(obj,FL_BLUE);
  fdui->hide = obj = fl_add_button(FL_NORMAL_BUTTON,1120,10,50,20,"HIDE");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
    fl_set_object_color(obj,FL_DARKCYAN,FL_COL1);
    fl_set_object_lcolor(obj,FL_CYAN);
    fl_set_object_lsize(obj,FL_NORMAL_SIZE);
  fdui->ventanaA = obj = fl_add_free(FL_NORMAL_FREE,840,230,160,120,"ventanaA",
			freeobj_ventanaAA_handle);
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
  fdui->ventanaB = obj = fl_add_free(FL_NORMAL_FREE,1010,230,160,120,"ventanaB",
			freeobj_ventanaBB_handle);
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
  fdui->ventanaC = obj = fl_add_free(FL_NORMAL_FREE,840,40,160,120,"ventanaC",
			freeobj_ventanaCC_handle);
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
  fdui->ventanaD = obj = fl_add_free(FL_NORMAL_FREE,1010,40,160,120,"ventanaD",
			freeobj_ventanaDD_handle);
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
  fdui->canvas = obj = fl_add_glcanvas(FL_NORMAL_CANVAS,10,10,810,500,"");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
  fdui->colorA = obj = fl_add_button(FL_PUSH_BUTTON,880,360,70,20,"camera A");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
    fl_set_object_color(obj,FL_COL1,FL_INACTIVE);
    fl_set_object_lcolor(obj,FL_DARKTOMATO);
  fdui->colorB = obj = fl_add_button(FL_PUSH_BUTTON,1060,360,70,20,"camera B");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
    fl_set_object_color(obj,FL_COL1,FL_INACTIVE);
    fl_set_object_lcolor(obj,FL_DARKTOMATO);
  fdui->colorC = obj = fl_add_button(FL_PUSH_BUTTON,880,170,70,20,"camera C");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
    fl_set_object_color(obj,FL_COL1,FL_INACTIVE);
    fl_set_object_lcolor(obj,FL_DARKTOMATO);
  fdui->colorD = obj = fl_add_button(FL_PUSH_BUTTON,1060,170,70,20,"camera D");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
    fl_set_object_color(obj,FL_COL1,FL_INACTIVE);
    fl_set_object_lcolor(obj,FL_DARKTOMATO);
  fdui->filteredImage = obj = fl_add_free(FL_NORMAL_FREE,40,550,320,240,"",
			freeobj_filteredImage_handle);
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
  fdui->groundImage = obj = fl_add_free(FL_NORMAL_FREE,370,550,320,240,"",
			freeobj_groundImage_handle);
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
  obj = fl_add_text(FL_NORMAL_TEXT,150,520,100,20,"Filtered image");
    fl_set_object_lcolor(obj,FL_GREEN);
    fl_set_object_lalign(obj,FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
  obj = fl_add_text(FL_NORMAL_TEXT,490,520,100,20,"Ground filter");
    fl_set_object_lcolor(obj,FL_SLATEBLUE);
    fl_set_object_lalign(obj,FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
  fdui->minicanvas = obj = fl_add_glcanvas(FL_NORMAL_CANVAS,840,400,330,390,"");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
  fdui->canvasButton = obj = fl_add_button(FL_PUSH_BUTTON,710,550,110,40,"Change View");
    fl_set_object_lcolor(obj,FL_DODGERBLUE);
    fl_set_object_lsize(obj,FL_NORMAL_SIZE);
    fl_set_object_lstyle(obj,FL_NORMAL_STYLE+FL_EMBOSSED_STYLE);
  fdui->valuator = obj = fl_add_valslider(FL_VERT_BROWSER_SLIDER,750,610,30,180,"");
  fl_end_form();

  fdui->fronteragui->fdui = fdui;

  return fdui;
}
/*---------------------------------------*/

