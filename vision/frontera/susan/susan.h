#ifndef SUSAN_LOADED
#define SUSAN_LOADED


#define MAX_NO_CORNERS 1850
#define MAX_NO_EDGES 2650


extern bt,     /* brightness threshold */
       principle,
       thin_post_proc,
       three_by_three,
       drawing_mode,
       susan_quick;
extern float dt; /* Distance threshold */


void init_susan(int picxsize, int picysize);
void i2smooth(unsigned char *image);
void i2edges(unsigned char *image);
void i2onlyedges(unsigned char *image);
void i2edges_nocopy(unsigned char *image, unsigned char *output);
void i2corners(unsigned char *image);

#endif
