/*
 *  Copyright (C) 2008 José María Cañas Plaza 
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Authors : Julio Vega Pérez (julio [dot] vega [at] urjc [dot] es)
 *  Authors : José María Cañas (jmplaza [at] gsyc [dot] es
 *  Authors : Darío Rodríguez de Diego (drd [dot] sqki [at] gmail [dot] com)
 *  Authors : Víctor Hidalgo 
 *
 *  This schema was programed for Guiderobot Project http://jde.gsyc.es/index.php/guiderobot
 *
 */

#include "jde.h"
#include "pioneer.h"
#include "graphics_gtk.h"
#include "frontera.h"

#include <glade/glade.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gtk/gtkgl.h>
#include <gtkextra/gtkextra.h>

#define MAXNAME 50
#define MAXID 50
#define MINROWS 1
#define MAXCOLS 6
#define MINPERSONS 1
#define MAXPHOTO 200
#define MAXPATH 200
#define MINLEFTATTACH 0
#define MINRIGHTATTACH 1
#define MINTOPATTACH 0
#define MINBOTTOMATTACH 1

int frontera_brothers[MAX_SCHEMAS];
arbitration frontera_callforarbitration;

#define v3f glVertex3f

//DEFINIMOS LOS VALORES QUE TENDRA QUE CUMPLIR EL FILTRO HSI
#define H_SUELO_MAX   224.321*DEGTORAD
#define H_SUELO_MIN   282.025*DEGTORAD
#define S_SUELO_MAX   1.0  
#define S_SUELO_MIN   0.0

// Posibles estados de las celdas de la rejilla
#define OCCUPIED -1
#define EMPTY -2

#define PI 3.141592654

#define MOUSELEFT 1
#define PUSHED 1
#define RELEASED 0

#define MAX_RADIUS_VALUE 10000000000
#define MIN_RADIUS_VALUE 0
#define WHEEL_DELTA 10

#define ANCHO_IMAGEN 320
#define LARGO_IMAGEN 240

// Valores imprescindibles para hacer estimaciones de distancias
#define foco 4.0
#define	focover 37.0	// Angulos de apertura
#define	focohor 48.0

// ARCO QUE REALIZA LA CAMARA AL REALIZAR EL BARRIDO
#define GRADOS_ARCO 180

#define resolucion (int) ((SIFNTSC_COLUMNS/focohor)*GRADOS_ARCO)

#define puntos_clave 5
#define IZQ  1
#define DER  -1

#define MAXWORLD 30000.

int DEBUG = 1;
int flash = FALSE;
int traslacion = 0, rotacion = 0;
#define MAX_TIME_ON_GRID 99999 * 1000000 /* tiempo máximo de permanencia de un punto en la rejilla (dado en microseg.) */

typedef struct map {
	Tvoxel *point;
	int *state;
	unsigned long int *updatingTime;
} Tmap;

Tvoxel centrogrid;
Tgrid *occupancyGrid = NULL; // grid que contiene información recibida por la cámara
int isInitGrid = FALSE;

int pendienteDeReubicacionGrafica = FALSE;
Tmap tempMap;

// Variables para guardar las matrices K, R y T de nuestra cámara:
gsl_matrix *K_1,*R_1;
gsl_vector* x_1;
HPoint2D progtest2D;
HPoint3D progtest3D;

struct HSV* myHSV;
TPinHoleCamera virtualcam0; /* para ver el minimundillo */
TPinHoleCamera virtualcam1; /* para moverme por el mundillo */
TPinHoleCamera virtualcam2; /* para ver el entorno circundante al robot */
TPinHoleCamera roboticLabCam0; // cámaras del techo del laboratorio
TPinHoleCamera roboticLabCam1; //
TPinHoleCamera roboticLabCam2; //
TPinHoleCamera roboticLabCam3; //
TPinHoleCamera ceilLabCam;
TPinHoleCamera robotCamera;
TPinHoleCamera robotCamera2;
TPinHoleCamera *myCamera; // puntero temporal

int actualCameraView; // identificador de la cámara de la escena actual

int flashImage = TRUE;

Display *mydisplay;
int *myscreen;

registerdisplay myregister_displaycallback;
deletedisplay mydelete_displaycallback;

char *configFile;

HPoint2D myActualPoint2D; // variables para el cálculo de los rayos virtuales
HPoint3D myActualPoint3D;
HPoint2D myActualPointImage, miPrimerPunto, miSegundoPunto;
HPoint3D cameraPos3D;
HPoint3D myEndPoint3D;
HPoint3D intersectionPoint;

HPoint3D groundPoints[SIFNTSC_COLUMNS*SIFNTSC_ROWS*4], fronteraPoints[SIFNTSC_COLUMNS], primerPunto3D, segundoPunto3D;
int actualGroundPoint = 0, actualFronteraPoint = 0; // contador para el punto actual del suelo...
int primerPunto = FALSE, segundoPunto = FALSE;
float distanciaPlanoImagen, incremento;

HPoint2D mouse_on_minicanvas, mouse_on_fronteracanvas;

struct image_struct {
	int width;
	int height;
	int bpp;	// bytes per pixel
	char *image;
};

/*GTK variables*/
GladeXML *xml=NULL; /*Fichero xml*/
GtkWidget *win;
GtkWidget *fronteraCanvas;
GtkWidget *miniCanvas;

GtkImage *originalImage; // Widgets de las imágenes que se obtienen del Window1 (GTK)
GtkImage *filteredImage;
GtkImage *borderImage;

struct image_struct *imageA = NULL; // Imágenes en crudo
struct image_struct *imageAfiltered = NULL;
struct image_struct *imageAbordered = NULL;

struct image_struct *originalImageRGB = NULL; // Imágenes preparadas para ser pintadas (cambio de bits)
struct image_struct *filteredImageRGB = NULL;
struct image_struct *borderImageRGB = NULL;

/*Global variables*/
FILE *f;
Tvoxel *target;/*Variable who export a new target*/

char filepath[MAXPATH];

/*Some control variable*/
int loadedgui=0;

float myR, myG, myB;
static int vmode;
static XImage *imagenA, *imagenFiltrada, *imagenSuelo;
static char *imagenA_buf, *imagenFiltrada_buf, *imagenSuelo_buf; /* puntero a memoria para la imagen a visualizar en el servidor X. No compartida con el servidor */
static long int tabla[256]; 
/* tabla con la traduccion de niveles de gris a numero de pixel en Pseudocolor-8bpp. Cada numero de pixel apunta al valor adecuado del ColorMap, con el color adecuado instalado */
static int pixel8bpp_rojo, pixel8bpp_blanco, pixel8bpp_amarillo;

int groundView = FALSE, fronteraView = FALSE;
int foa_mode, cam_mode;
float radius = 0.1;
float t = 0.5; // lambda

static int message = FALSE;

static char *samplesource=NULL;

int frontera_id=0; 
int frontera_brothers[MAX_SCHEMAS];
arbitration frontera_callforarbitration;
int frontera_cycle=1000; /* ms */

char **myActualCamera;

int *mycolorAwidth		= NULL;
int *mycolorAheight		= NULL;
char **mycolorA;
runFn colorArun;
stopFn colorAstop;

unsigned long display_state;

int botonPulsado;

float old_x=0, old_y=0;
float longi_foa = 0.0;
float lati_foa = 0.0;
float foax = 0.0;
float foay = 0.0;
float foaz = 0.0;
float xcam = -1.0;
float ycam = -1.0;
float zcam = -1.0;
float lati = 0.0;
float longi = 0.0;

float old_x2=0, old_y2=0;
float longi_foa2 = 0.0;
float lati_foa2 = 0.0;
float foax2 = 0.0;
float foay2 = 0.0;
float foaz2 = 0.0;
float xcam2 = -1.0;
float ycam2 = -1.0;
float zcam2 = -1.0;
float lati2 = 0.0;
float longi2 = 0.0;

/*CALLBACKS*/
void groundButton_pressed (GtkButton *button, gpointer user_data) {
	GtkButton *myBorderButton;
	myBorderButton = (GtkButton *)glade_xml_get_widget(xml, "borderButton");

	gtk_button_released (myBorderButton); // desactivamos el otro botón (son excluyentes)

  groundView = TRUE;
  fronteraView = FALSE;
}

void groundButton_released (GtkButton *button, gpointer user_data) {
  groundView = FALSE;
}

void borderButton_pressed (GtkButton *button, gpointer user_data) {
	GtkButton *myGroundButton;
	myGroundButton = (GtkButton *)glade_xml_get_widget(xml, "groundButton");

	gtk_button_released (myGroundButton); // desactivamos el otro botón (son excluyentes)

  fronteraView = TRUE;
  groundView = FALSE;
}

void borderButton_released (GtkButton *button, gpointer user_data) {
	fronteraView = FALSE;
}

void go1Button_pressed (GtkButton *button, gpointer user_data) {
	traslacion += 1000;
}

void go1Button_released (GtkButton *button, gpointer user_data) {}

void go50Button_pressed (GtkButton *button, gpointer user_data) {
	traslacion += 500;
}

void go50Button_released (GtkButton *button, gpointer user_data) {}

void turn45RButton_pressed (GtkButton *button, gpointer user_data) {
	rotacion += 45;
}

void turn45RButton_released (GtkButton *button, gpointer user_data) {}

void turn45LButton_pressed (GtkButton *button, gpointer user_data) {
	rotacion -= 45;
}

void turn45LButton_released (GtkButton *button, gpointer user_data) {}

void turn90RButton_pressed (GtkButton *button, gpointer user_data) {
	rotacion += 90;
}

void turn90RButton_released (GtkButton *button, gpointer user_data) {}

void turn90LButton_pressed (GtkButton *button, gpointer user_data) {
	rotacion -= 90;
}

void turn90LButton_released (GtkButton *button, gpointer user_data) {}

void flashButton_pressed (GtkButton *button, gpointer user_data) {
	flash = TRUE;
}

void flashButton_released (GtkButton *button, gpointer user_data) {}

void camera1Button_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera2Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera3Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera4Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "ceilCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);

  actualCameraView = 1;
}

void camera1Button_released (GtkButton *button, gpointer user_data) {
/*	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_toggled (myCameraTempButton);

  actualCameraView = 0;*/
}

void camera2Button_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera1Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera3Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera4Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "ceilCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);

  actualCameraView = 2;
}

void camera2Button_released (GtkButton *button, gpointer user_data) {
/*	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_toggled (myCameraTempButton);

  actualCameraView = 0;*/
}

void camera3Button_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera2Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera1Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera4Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "ceilCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);

  actualCameraView = 3;
}

void camera3Button_released (GtkButton *button, gpointer user_data) {
/*	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_toggled (myCameraTempButton);

  actualCameraView = 0;*/
}

void camera4Button_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera2Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera3Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera1Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "ceilCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);

  actualCameraView = 4;
}

void camera4Button_released (GtkButton *button, gpointer user_data) {
/*	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_toggled (myCameraTempButton);

  actualCameraView = 0;*/
}

void ceilCameraButton_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera2Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera3Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera4Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera1Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);

  actualCameraView = 5;
}

void ceilCameraButton_released (GtkButton *button, gpointer user_data) {
/*	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_toggled (myCameraTempButton);

  actualCameraView = 0;*/
}

void userCameraButton_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera2Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera3Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera4Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera1Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "ceilCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);

  actualCameraView = 0;
}

void userCameraButton_released (GtkButton *button, gpointer user_data) {
/*	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_toggled (myCameraTempButton);

  actualCameraView = 0;*/
}

static gboolean button_press_event (GtkWidget *widget, GdkEventButton *event, gpointer data){
  float boton_x;
  float boton_y;
  int back;

  event = (GdkEventButton*)event;
  old_x=event->x;
  old_y=event->y;

  /* Los dos modos son excluyentes */
  if (2 == event->button){
		botonPulsado = 2;
		flashImage = TRUE;
	  return;
  } else {
  	flashImage = FALSE;
  	if (1 == event->button){
			botonPulsado = 1;
		  cam_mode = 1;
		  foa_mode = 0;  
		}else if (3 == event->button){
			botonPulsado = 3;
		  foa_mode = 1;
		  cam_mode = 0;
		}
	}	
}

static gboolean motion_notify_event (GtkWidget *widget, GdkEventButton *event, gpointer data) {

	printf ("entramos2\n");

   float x=event->x;
   float y=event->y;
  
	event = (GdkEventButton*)event;
	//printf("Entramos en el motion_notify_event \n");
	/* las siguiente ecuaciones sirven mapear el movimiento:
	Eje X :: (0,w) -> (-180,180)
	Eje Y :: (0,h) -> (-90,90)
	donde (h,w) es el larcho,ancho del gl_canvas
	Si el button pulsado es 2 (centro del raton) entonces no actualizamos la posicion
	*/

	if (2 == botonPulsado) {
		flashImage = TRUE;
	  return TRUE;
	}	
	//printf ("No se trata de un boton 2, es: %d \n ", drawing_area->allocation.width);
	mouse_on_fronteracanvas.x = ((event->x*360/fronteraCanvas->allocation.width)-180);
	mouse_on_fronteracanvas.y = ((event->y*-180/fronteraCanvas->allocation.height)+90);

 	if (GDK_BUTTON1_MASK){ /*Si está pulsado el botón 1*/
//		theta -= x - old_x;
//		phi -= y - old_y;
		gtk_widget_queue_draw (widget);
		old_x=x;
		old_y=y;
	}
 return TRUE;
}

/// Controlador de la camara de opengl
void opengl_camara_controller_motion(	GtkWidget	*widget,
					GdkEventMotion	*event,
					gpointer	user_data) {

	printf ("entramos\n");
	float desp = 0.01;
	float x=event->x;
	float y=event->y;
	
	if (event->state & GDK_BUTTON1_MASK) { /*Si está pulsado el botón 1*/
		if ((x - old_x) > 0.0)		longi -= desp;
		else if ((x - old_x) < 0.0)	longi += desp;

		if ((y - old_y) > 0.0)		lati += desp;
		else if ((y - old_y) < 0.0)	lati -= desp;

		xcam=radius*cosf(lati)*cosf(longi) + foax;
		ycam=radius*cosf(lati)*sinf(longi) + foay;
		zcam=radius*sinf(lati) + foaz;
	}

	if (event->state & GDK_BUTTON3_MASK) {
		if ((x - old_x) > 0.0)		longi -= desp;
		else if ((x - old_x) < 0.0)	longi += desp;

		if ((y - old_y) > 0.0)		lati += desp;
		else if ((y - old_y) < 0.0)	lati -= desp;

		foax=radius*cosf(lati_foa)*cosf(lati_foa) + xcam;
		foay=radius*cosf(lati_foa)*sinf(lati_foa) + ycam;
		foaz=radius*sinf(lati_foa) + zcam;
	}

	old_x=x;
	old_y=y;
}

void opengl_camara_controller_zoom(	GtkWidget	*widget,
					GdkEventScroll	*event,
					gpointer	user_data) {
	float vx, vy, vz;

	vx = (foax - xcam)/radius;
	vy = (foay - ycam)/radius;
	vz = (foaz - zcam)/radius;

	if (event->direction == GDK_SCROLL_UP){
		foax = foax + vx;
		foay = foay + vy;
		foaz = foaz + vz;

		xcam = xcam + vx;
		ycam = ycam + vy;
		zcam = zcam + vz;
	}

	if (event->direction == GDK_SCROLL_DOWN){
		foax = foax - vx;
		foay = foay - vy;
		foaz = foaz - vz;

		xcam = xcam - vx;
		ycam = ycam - vy;
		zcam = zcam - vz;
	}
}

void opengl_camara_controller_motion2(	GtkWidget	*widget,
					GdkEventMotion	*event,
					gpointer	user_data) {

	printf ("entramos\n");
	float desp = 0.01;
	float x=event->x;
	float y=event->y;
	
	if (event->state & GDK_BUTTON1_MASK) { /*Si está pulsado el botón 1*/
		if ((x - old_x2) > 0.0)		longi2 -= desp;
		else if ((x - old_x2) < 0.0)	longi2 += desp;

		if ((y - old_y2) > 0.0)		lati2 += desp;
		else if ((y - old_y2) < 0.0)	lati2 -= desp;

		xcam2=radius*cosf(lati2)*cosf(longi2) + foax2;
		ycam2=radius*cosf(lati2)*sinf(longi2) + foay2;
		zcam2=radius*sinf(lati2) + foaz2;
	}

	if (event->state & GDK_BUTTON3_MASK) {
		if ((x - old_x2) > 0.0)		longi2 -= desp;
		else if ((x - old_x2) < 0.0)	longi2 += desp;

		if ((y - old_y2) > 0.0)		lati2 += desp;
		else if ((y - old_y2) < 0.0)	lati2 -= desp;

		foax2=radius*cosf(lati_foa2)*cosf(lati_foa2) + xcam2;
		foay2=radius*cosf(lati_foa2)*sinf(lati_foa2) + ycam2;
		foaz2=radius*sinf(lati_foa2) + zcam2;
	}

	old_x2=x;
	old_y2=y;
}

void opengl_camara_controller_zoom2(	GtkWidget	*widget,
					GdkEventScroll	*event,
					gpointer	user_data) {
	float vx, vy, vz;

	vx = (foax2 - xcam2)/radius;
	vy = (foay2 - ycam2)/radius;
	vz = (foaz2 - zcam2)/radius;

	if (event->direction == GDK_SCROLL_UP){
		foax2 = foax2 + vx;
		foay2 = foay2 + vy;
		foaz2 = foaz2 + vz;

		xcam2 = xcam2 + vx;
		ycam2 = ycam2 + vy;
		zcam2 = zcam2 + vz;
	}

	if (event->direction == GDK_SCROLL_DOWN){
		foax2 = foax2 - vx;
		foay2 = foay2 - vy;
		foaz2 = foaz2 - vz;

		xcam2 = xcam2 - vx;
		ycam2 = ycam2 - vy;
		zcam2 = zcam2 - vz;
	}
}

static gboolean scroll_event (GtkRange *range, GdkEventScroll *event, gpointer data){
   if (event->direction == GDK_SCROLL_DOWN){
	if (radius > MIN_RADIUS_VALUE)
   	radius-=WHEEL_DELTA;
   }
   if (event->direction == GDK_SCROLL_UP){
     if (radius<MAX_RADIUS_VALUE){
      radius+=WHEEL_DELTA;
       }
   }
   if (radius < 0.5){ radius = 0.5;}
   gtk_widget_queue_draw(GTK_WIDGET((GtkWidget *)data));
   
   return TRUE;
}

/*Callback of window closed*/
void on_delete_window (GtkWidget *widget,GdkEvent *event,gpointer user_data)
{
   gdk_threads_leave();
   frontera_hide();
   gdk_threads_enter();
}

struct image_struct *create_image(int width, int height, int bpp) {
	struct image_struct *w;

	w = (struct image_struct*) malloc(sizeof(struct image_struct));
	w->width = width;
	w->height = height;
	w->bpp = bpp;
	w->image = (char*) malloc(width * height * bpp);

	return w;
}

void remove_image(struct image_struct *w) {
	free(w->image);
	free(w);
}

/** prepare2draw ************************************************************************
* Prepare an image to draw it with a GtkImage in BGR format with 3 pixels per byte.	*
*	src: source image								*
*	dest: destiny image. It has to have the same size as the source image and	*
*	      3 bytes per pixel.
*****************************************************************************************/
void prepare2draw (struct image_struct *src, struct image_struct *dest) {
	int i;

	for (i=0; i<src->width*src->height; i++) {
		dest->image[i*dest->bpp+0] = src->image[i*src->bpp+2];
		dest->image[i*dest->bpp+1] = src->image[i*src->bpp+1];
		dest->image[i*dest->bpp+2] = src->image[i*src->bpp+0];
	}
}

unsigned long int dameTiempo() {
	struct timeval t; 

	gettimeofday(&t,NULL);
	return t.tv_sec*1000000 + t.tv_usec;	
}

void updateGridTiming () {
	int j;

	for(j=0;j< ((occupancyGrid->size)*(occupancyGrid->size)); j++) {
		if ((dameTiempo() - occupancyGrid->map[j].ultima_actualizacion) > MAX_TIME_ON_GRID) { // es punto viejete
			occupancyGrid->map[j].estado = EMPTY; // lo marcamos a vacio para no me haga de frontera
			occupancyGrid->map[j].ultima_actualizacion = dameTiempo ();
		}
	}
}

void initCalibration () {

  K_1 = gsl_matrix_alloc(3,3);
  R_1 = gsl_matrix_alloc(3,3);
  x_1 = gsl_vector_alloc(3);

	// Fill K_1 matrix, with information got on Calibrator GUI
	gsl_matrix_set(K_1,0,0,300.0);
  gsl_matrix_set(K_1,0,1,3.57);
  gsl_matrix_set(K_1,0,2,182.98);

	gsl_matrix_set(K_1,1,0,0.);
  gsl_matrix_set(K_1,1,1,290.64);
  gsl_matrix_set(K_1,1,2,104.61);

	gsl_matrix_set(K_1,2,0,0.);
  gsl_matrix_set(K_1,2,1,0.);
  gsl_matrix_set(K_1,2,2,1.);

	// Fill R_1 matrix, with information got on Calibrator GUI
	gsl_matrix_set(R_1,0,0,0.65);
  gsl_matrix_set(R_1,0,1,-0.75);
  gsl_matrix_set(R_1,0,2,-0.13);

	gsl_matrix_set(R_1,1,0,-0.34);
  gsl_matrix_set(R_1,1,1,-0.45);
  gsl_matrix_set(R_1,1,2,0.83);

	gsl_matrix_set(R_1,2,0,0.68);
  gsl_matrix_set(R_1,2,1,0.49);
  gsl_matrix_set(R_1,2,2,0.55);

	// Fill x_1 vector, with information got on Calibrator GUI
  gsl_vector_set(x_1, 0, -213.4);
  gsl_vector_set(x_1, 1, -207.0);
  gsl_vector_set(x_1, 2, -208.9);
}

double rad2deg(double alfa){
  return (alfa*180)/3.14159264;
}


void drawRobotCamera(double ppx, double ppy, int camera){
  // Estos incrementos se usan para obtener el siguiente punto
  float inc_x = (1./SIFNTSC_COLUMNS)*100;
  float inc_y = (1./SIFNTSC_ROWS)*100;
  
  int i,j,offset,red,green,blue;

	// dibujamos el rectangulo donde se proyectará la imagen
  glColor3f( 0.0, 0.0, 0.0 );  
  glScalef(0.2,0.2,0.2);
  
  glBegin(GL_LINES);
  v3f( -50, 50, -100 );   
  v3f( 50, 50, -100 );   
  glEnd();
  
  glBegin(GL_LINES);
  v3f( -50, 50, -100 );   
  v3f( -50, -50, -100 );   
  glEnd();

  glBegin(GL_LINES);
  v3f( 50, 50, -100 );   
  v3f( 50, -50, -100 );   
  glEnd();

  glBegin(GL_LINES);
  v3f( 50, -50, -100 );   
  v3f( -50, -50, -100 );   
  glEnd();

  // pintamos la imagen
  if (mycolorA!=NULL)
    for (j=0;j<SIFNTSC_ROWS;j++)
      for(i=0;i<SIFNTSC_COLUMNS;i++) {
			  // obtenemos el color del pixel de la imagen de entrada
				offset = j*320+i;
				if (mycolorA!=NULL) {
				  red = (*mycolorA)[offset*3+2];
				  green = (*mycolorA)[offset*3+1];
				  blue = (*mycolorA)[offset*3+0];
				}
				
				glColor3f( (float)red/255.0, (float)green/255.0, (float)blue/255.0 );  
				
				// pintamos el punto correspondiente en el escenario pintado con OGL
				glBegin(GL_POINTS);
				v3f(-50.+(inc_x*i),50.-(inc_y*j),-100. );
				glEnd();
			}
  
  // fin del dibujo del rectangulo y su imagen interior. Ahora dibujamos los triángulos.
  glColor3f( 1.0, 0.0, 0.0 );  
  glBegin(GL_LINES);
  v3f( 0.0, 0.0, 0.0 );   
  v3f( -50, 50, -100 );   
  glEnd();

  glColor3f( 0.0, 1.0, 0.0 );  
  glBegin(GL_LINES);
  v3f( 0.0, 0.0, 0.0 );   
  v3f( 50, 50, -100 );   
  glEnd();

  glColor3f( 0.0, 0.0, 1.0 );  
  glBegin(GL_LINES);
  v3f( 0.0, 0.0, 0.0 );   
  v3f( 50, -50, -100 );   
  glEnd();
  
  glColor3f( 1.0, 1.0, 0.0 );  
  glBegin(GL_LINES);
  v3f( 0.0, 0.0, 0.0 );   
  v3f( -50, -50, -100 );   
  glEnd();

  glBegin(GL_LINES);
  glColor3f( 0.0, 1.0, 0.0 );  
  v3f( 0.0, 0.0, 0.0 );   
  v3f( -50+ppx,-50+ppy, -100 );   
  glEnd();

  /* Por alguna razon, el tamaño maximo que se puede utilizar para un punto
     es de un solo pixel que es practicamente invisible para un ser humano.
     Visto esto, se ha decidido usar un cubo de un lado muy pequeÃ±o para 
     dibujar "puntos grandes
  ***/
  glLoadIdentity();
  glColor3f( 1.0, 0.0, 0.0 );
  glTranslatef( 
	 -(float)gsl_vector_get(x_1,0),
	 -(float)gsl_vector_get(x_1,1),
	 -(float)gsl_vector_get(x_1,2));
  
  glutSolidCube(0.01);
	glScalef(1,1,1); // reestablecemos la escala
}

void setRobotCameraPos () {

  HPoint2D p2;
  HPoint3D p3;
  int i,j=0;
  gsl_matrix *RT,*T,*Res;
  
  RT = gsl_matrix_alloc(4,4);
  T = gsl_matrix_alloc(3,4);
  Res = gsl_matrix_alloc(3,4);
  
  gsl_matrix_set(T,0,0,1);
  gsl_matrix_set(T,1,1,1);
  gsl_matrix_set(T,2,2,1);

  gsl_matrix_set(T,0,3,gsl_vector_get(x_1,0));
  gsl_matrix_set(T,1,3,gsl_vector_get(x_1,1));
  gsl_matrix_set(T,2,3,gsl_vector_get(x_1,2));

  gsl_linalg_matmult (R_1,T,Res);  

  for (i=0;i<3;i++)
    for (j=0;j<4;j++)
      gsl_matrix_set(RT,i,j,gsl_matrix_get(Res,i,j));
 
  /** set 0001 in the last row of RT */
  gsl_matrix_set(RT,3,0,0);
  gsl_matrix_set(RT,3,1,0);
  gsl_matrix_set(RT,3,2,0);
  gsl_matrix_set(RT,3,3,1);

  /** Set camera position*/
  robotCamera.position.X = gsl_vector_get(x_1,0);
  robotCamera.position.Y = gsl_vector_get(x_1,1);
  robotCamera.position.Z = gsl_vector_get(x_1,2);

  printf("Camera position %f:%f:%f\n",robotCamera.position.X, robotCamera.position.Y, robotCamera.position.Z);
      
  /** Seting intrensic matrix*/
  robotCamera.k11 = gsl_matrix_get(K_1,0,0);
  robotCamera.k12 = gsl_matrix_get(K_1,0,1);
  robotCamera.k13 = gsl_matrix_get(K_1,0,2);
  robotCamera.k14 = 0;

  robotCamera.k21 = gsl_matrix_get(K_1,1,0);
  robotCamera.k22 = gsl_matrix_get(K_1,1,1);
  robotCamera.k23 = gsl_matrix_get(K_1,1,2);
  robotCamera.k24 = 0;

  robotCamera.k31 = gsl_matrix_get(K_1,2,0);
  robotCamera.k32 = gsl_matrix_get(K_1,2,1);
  robotCamera.k33 = gsl_matrix_get(K_1,2,2);
  robotCamera.k34 = 0;

  /** Seting extrensic*/
  robotCamera.rt11 = gsl_matrix_get(RT,0,0);
  robotCamera.rt12 = gsl_matrix_get(RT,0,1);
  robotCamera.rt13 = gsl_matrix_get(RT,0,2);
  robotCamera.rt14 = gsl_matrix_get(RT,0,3);
  
  robotCamera.rt21 = gsl_matrix_get(RT,1,0);
  robotCamera.rt22 = gsl_matrix_get(RT,1,1);
  robotCamera.rt23 = gsl_matrix_get(RT,1,2);
  robotCamera.rt24 = gsl_matrix_get(RT,1,3);

  robotCamera.rt31 = gsl_matrix_get(RT,2,0);
  robotCamera.rt32 = gsl_matrix_get(RT,2,1);
  robotCamera.rt33 = gsl_matrix_get(RT,2,2);
  robotCamera.rt34 = gsl_matrix_get(RT,2,3);

  robotCamera.rt41 = gsl_matrix_get(RT,3,0);
  robotCamera.rt42 = gsl_matrix_get(RT,3,1);
  robotCamera.rt43 = gsl_matrix_get(RT,3,2);
  robotCamera.rt44 = gsl_matrix_get(RT,3,3);

  p3.X = 100;
  p3.Y = 100;
  p3.Z = 100;
  p3.H = 1;

  progtest2D.x = 0;
  progtest2D.y = 0;
  progtest2D.h = 1;

  project(p3,&p2,robotCamera);
  
  //if (DEBUG)
  printf("El punto project en %f:%f:%f\n",p2.x,p2.y,p2.h);

  if (backproject(&progtest3D, progtest2D, robotCamera)){
    
    progtest3D.X =  progtest3D.X/progtest3D.H;
    progtest3D.Y =  progtest3D.Y/progtest3D.H;
    progtest3D.Z =  progtest3D.Z/progtest3D.H;

  //if (DEBUG)
    printf("el backproject de %.2f-%.2f es %.2f-%.2f-%.2f-%.2f \n\n",
	   progtest2D.x,progtest2D.y,
	   progtest3D.X, progtest3D.Y, progtest3D.Z, progtest3D.H
	   );
  }
}

void drawMyAxes () {
  glLineWidth(2.0f);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

	glColor3f (1.,1.,1.);
  glBegin(GL_LINES);
  v3f(-robotCamera.position.X, -robotCamera.position.Y, 0.000000);
  v3f(segundoPunto3D.X, segundoPunto3D.Y, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(-robotCamera.position.X, -robotCamera.position.Y, 0.000000);
  v3f(primerPunto3D.X, primerPunto3D.Y, 0.000000);
  glEnd();
}

void drawRoboticLabPlanes () {
	/* LABORATORIO DE ROBÓTICA */
  glLineWidth(2.0f);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

	/* SUELO */
  glColor3f(0.93, 0.93, 0.455);
	// Parte central:
  glBegin(GL_QUADS);
	  v3f(0.000000, 0.000000, 0.000000);
	  v3f(0.000000, 4589.000000, 0.000000);
	  v3f(7925.000000, 4589.000000, 0.000000);
	  v3f(7925.000000, 0.000000, 0.000000);
  glEnd();

	// Lateral de la puerta (1ª parte):
  glBegin(GL_QUADS);
	  v3f(1211.000000, -554.000000, 0.000000);
	  v3f(1211.000000, 5.000000, 0.000000);
	  v3f(5476.000000, 5.000000, 0.000000);
	  v3f(5476.000000, -554.000000, 0.000000);
  glEnd();

	// Lateral de la puerta (trocito intermedio de sobrante defectuoso):
  glBegin(GL_QUADS);
	  v3f(5470.000000, -120.000000, 0.000000);
	  v3f(5470.000000, 10.000000, 0.000000);
	  v3f(6115.000000, 10.000000, 0.000000);
	  v3f(6115.000000, -120.000000, 0.000000);
  glEnd();

	// Lateral de la puerta (2ª parte):
  glBegin(GL_QUADS);
	  v3f(6110.000000, -554.000000, 0.000000);
	  v3f(6110.000000, 5.000000, 0.000000);
	  v3f(7925.000000, 5.000000, 0.000000);
	  v3f(7925.000000, -554.000000, 0.000000);
  glEnd();

	// Lateral enfrente de la puerta (1ª parte):
  glBegin(GL_QUADS);
	  v3f(2419.000000, 4580.000000, 0.000000);
	  v3f(2419.000000, 4879.000000, 0.000000);
	  v3f(5498.000000, 4879.000000, 0.000000);
	  v3f(5498.000000, 4580.000000, 0.000000);
  glEnd();

	// Lateral enfrente de la puerta (2ª parte):
  glBegin(GL_QUADS);
	  v3f(6264.000000, 4580.000000, 0.000000);
	  v3f(6264.000000, 4879.000000, 0.000000);
	  v3f(7925.000000, 4879.000000, 0.000000);
	  v3f(7925.000000, 4580.000000, 0.000000);
  glEnd();

	/* CAMPO DE FUTBOL */
  glColor3f(0., 0.59, 0.);
  glBegin(GL_QUADS); // daremos algo de altura para no causar "conflictos graficos" con el suelo
	  v3f(1560.000000, 596.000000, 25.000000);
	  v3f(1560.000000, 4589.000000, 25.000000);
	  v3f(7925.000000, 4589.000000, 25.000000);
	  v3f(7925.000000, 596.000000, 25.000000);
  glEnd();

	/* PAREDES */

	// Lateral izquierdo:
  glColor3f(0.98, 0.98, 0.98);
  glBegin(GL_QUADS);
	  v3f(0.000000, 0.000000, 3000.000000);
	  v3f(0.000000, 4589.000000, 3000.000000);
	  v3f(0.000000, 4589.000000, 0.000000);
	  v3f(0.000000, 0.000000, 0.000000);
	glEnd();

	// Lateral enfrente de la puerta:
  glBegin(GL_QUADS); // 1ª parte
	  v3f(0.000000, 4589.000000, 0.000000);
	  v3f(0.000000, 4589.000000, 3000.000000);
	  v3f(2419.000000, 4589.000000, 3000.000000);
	  v3f(2419.000000, 4589.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 2ª parte
	  v3f(2419.000000, 4589.000000, 0.000000);
	  v3f(2419.000000, 4589.000000, 3000.000000);
	  v3f(2419.000000, 4879.000000, 3000.000000);
	  v3f(2419.000000, 4879.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 3ª parte
	  v3f(2419.000000, 4879.000000, 0.000000);
	  v3f(2419.000000, 4879.000000, 3000.000000);
	  v3f(5476.000000, 4879.000000, 3000.000000);
	  v3f(5476.000000, 4879.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 4ª parte
	  v3f(5476.000000, 4879.000000, 0.000000);
	  v3f(5476.000000, 4879.000000, 3000.000000);
	  v3f(5476.000000, 4589.000000, 3000.000000);
	  v3f(5476.000000, 4589.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 5ª parte
	  v3f(5476.000000, 4589.000000, 0.000000);
	  v3f(5476.000000, 4589.000000, 3000.000000);
	  v3f(6264.000000, 4589.000000, 3000.000000);
	  v3f(6264.000000, 4589.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 6ª parte
	  v3f(6264.000000, 4589.000000, 0.000000);
	  v3f(6264.000000, 4589.000000, 3000.000000);
	  v3f(6264.000000, 4879.000000, 3000.000000);
	  v3f(6264.000000, 4879.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 6ª parte
	  v3f(6264.000000, 4879.000000, 0.000000);
	  v3f(6264.000000, 4879.000000, 3000.000000);
	  v3f(7925.000000, 4879.000000, 3000.000000);
	  v3f(7925.000000, 4879.000000, 0.000000);
	glEnd();

	// Lateral a la derecha de la puerta:
  glBegin(GL_QUADS); // 6ª parte
	  v3f(7925.000000, 4879.000000, 0.000000);
	  v3f(7925.000000, 4879.000000, 3000.000000);
	  v3f(7925.000000, -554.000000, 3000.000000);
	  v3f(7925.000000, -554.000000, 0.000000);
	glEnd();

	// Lateral de la puerta:
  glBegin(GL_QUADS); // 1ª parte
	  v3f(7925.000000, -554.000000, 0.000000);
	  v3f(7925.000000, -554.000000, 3000.000000);
	  v3f(6110.000000, -554.000000, 3000.000000);
	  v3f(6110.000000, -554.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 2ª parte
	  v3f(6110.000000, -554.000000, 0.000000);
	  v3f(6110.000000, -554.000000, 3000.000000);
	  v3f(6110.000000, -119.000000, 3000.000000);
	  v3f(6110.000000, -119.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 3ª parte
	  v3f(6110.000000, -119.000000, 0.000000);
	  v3f(6110.000000, -119.000000, 3000.000000);
	  v3f(5476.000000, -119.000000, 3000.000000);
	  v3f(5476.000000, -119.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 4ª parte
	  v3f(5476.000000, -554.000000, 0.000000);
	  v3f(5476.000000, -554.000000, 3000.000000);
	  v3f(1211.000000, -554.000000, 3000.000000);
	  v3f(1211.000000, -554.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 5ª parte
	  v3f(1211.000000, -554.000000, 0.000000);
	  v3f(1211.000000, -554.000000, 3000.000000);
	  v3f(1211.000000, 0.000000, 3000.000000);
	  v3f(1211.000000, 0.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 6ª parte
	  v3f(1211.000000, 0.000000, 0.000000);
	  v3f(1211.000000, 0.000000, 3000.000000);
	  v3f(0.000000, 0.000000, 3000.000000);
	  v3f(0.000000, 0.000000, 0.000000);
	glEnd();

	/* PUERTA */
  glColor3f(0.67, 0., 0.);
  glBegin(GL_QUADS);
	  v3f(4613.000000, -554.000000, 0.000000);
	  v3f(4613.000000, -554.000000, 2030.000000);
	  v3f(3879.000000, -554.000000, 2030.000000);
	  v3f(3879.000000, -554.000000, 0.000000);
  glEnd();

	/* VENTANA */
  glColor3f(0.5, 0.5, 1.);
  glBegin(GL_QUADS);
	  v3f(2585.000000, 4879.000000, 835.000000);
	  v3f(2585.000000, 4879.000000, 2532.000000);
	  v3f(5335.000000, 4879.000000, 2532.000000);
	  v3f(5335.000000, 4879.000000, 835.000000);
  glEnd();
}

void drawHallLines () {
	/* el pasillo mide (15x1.99)+1.50 cm. de largo y 1.90 cm. de ancho */

	/* SUELO */
  glBegin(GL_LINES);
	  v3f(0.0, -554.0, 0.0);
	  v3f(31350.0, -554.0, 0.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(31350.0, -554.0, 0.0);
	  v3f(31350.0, -2454.0, 0.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(31350.0, -2454.0, 0.0);
	  v3f(0.0, -2454.0, 0.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(0.0, -2454.0, 0.0);
	  v3f(0.0, -554.0, 0.0);
  glEnd();

	/* TECHO DEL PASILLO */
  glBegin(GL_LINES);
	  v3f(0.0, -554.0, 3000.0);
	  v3f(31350.0, -554.0, 3000.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(31350.0, -554.0, 3000.0);
	  v3f(31350.0, -2454.0, 3000.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(31350.0, -2454.0, 3000.0);
	  v3f(0.0, -2454.0, 3000.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(0.0, -2454.0, 3000.0);
	  v3f(0.0, -554.0, 3000.0);
  glEnd();

	/* ESQUINAS */
  glBegin(GL_LINES);
	  v3f(0.0, -554.0, 0.0);
	  v3f(0.0, -554.0, 3000.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(0.0, -2454.0, 0.0);
	  v3f(0.0, -2454.0, 3000.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(31350.0, -554.0, 0.0);
	  v3f(31350.0, -554.0, 3000.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(31350.0, -2454.0, 0.0);
	  v3f(31350.0, -2454.0, 3000.0);
  glEnd();
}

void drawRoboticLabLines () {
  glColor3f(1., 1., 1.);
  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 3000.000000);
  v3f(0.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 3000.000000);
  v3f(1211.000000, 0.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, 0.000000, 3000.000000);
  v3f(1211.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, -554.000000, 3000.000000);
  v3f(5476.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -554.000000, 3000.000000);
  v3f(5476.000000, -119.000000, 3000.000000);
  glEnd();
	
  glBegin(GL_LINES);
  v3f(5476.000000, -119.000000, 3000.000000);
  v3f(6110.000000, -119.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -119.000000, 3000.000000);
  v3f(6110.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -554.000000, 3000.000000);
  v3f(7925.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, -554.000000, 3000.000000);
  v3f(7925.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, 4879.000000, 3000.000000);
  v3f(6264.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4879.000000, 3000.000000);
  v3f(6264.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 3000.000000);
  v3f(5498.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 3000.000000);
  v3f(5498.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4879.000000, 3000.000000);
  v3f(2419.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4879.000000, 3000.000000);
  v3f(2419.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4589.000000, 3000.000000);
  v3f(0.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 30.000000);
  v3f(0.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 30.000000);
  v3f(1211.000000, 0.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, 0.000000, 30.000000);
  v3f(1211.000000, -554.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, -554.000000, 30.000000);
  v3f(5476.000000, -554.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -554.000000, 30.000000);
  v3f(5476.000000, -119.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -119.000000, 30.000000);
  v3f(6110.000000, -119.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -119.000000, 30.000000);
  v3f(6110.000000, -554.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -554.000000, 30.000000);
  v3f(7925.000000, -554.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, -554.000000, 30.000000);
  v3f(7925.000000, 4879.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, 4879.000000, 30.000000);
  v3f(6264.000000, 4879.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4879.000000, 30.000000);
  v3f(6264.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 30.000000);
  v3f(5498.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 30.000000);
  v3f(5498.000000, 4879.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4879.000000, 30.000000);
  v3f(2419.000000, 4879.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4879.000000, 30.000000);
  v3f(2419.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4589.000000, 30.000000);
  v3f(0.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 0.000000);
  v3f(0.000000, 0.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, 0.000000, 0.000000);
  v3f(1211.000000, 0.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, -554.000000, 0.000000);
  v3f(1211.000000, -554.000000, 3000.000000);
  glEnd();

	// LINEAS LIMITROFES PUERTA-------------------
  glColor3f(1., 0.5, 0.);
  glBegin(GL_LINES);
  v3f(3879.000000, -554.000000, 0.000000);
  v3f(3879.000000, -554.000000, 2030.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(3879.000000, -554.000000, 2030.000000);
  v3f(4613.000000, -554.000000, 2030.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4613.000000, -554.000000, 2030.000000);
  v3f(4613.000000, -554.000000, 0.000000);
  glEnd();
	//--------------------------------------------

  glColor3f(1., 1., 1.);
  glBegin(GL_LINES);
  v3f(5476.000000, -554.000000, 0.000000);
  v3f(5476.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -119.000000, 0.000000);
  v3f(5476.000000, -119.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -119.000000, 0.000000);
  v3f(6110.000000, -119.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -554.000000, 0.000000);
  v3f(6110.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, -554.000000, 0.000000);
  v3f(7925.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, 4879.000000, 0.000000);
  v3f(7925.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4879.000000, 0.000000);
  v3f(6264.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 0.000000);
  v3f(6264.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 0.000000);
  v3f(5498.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4879.000000, 0.000000);
  v3f(5498.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4879.000000, 0.000000);
  v3f(2419.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4589.000000, 0.000000);
  v3f(2419.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 4589.000000, 0.000000);
  v3f(0.000000, 4589.000000, 3000.000000);
  glEnd();

	// LINEAS LIMITROFES DE LA VENTANA-----------
  glColor3f(1., 0.5, 0.);
  glBegin(GL_LINES);
  v3f(2585.000000, 4879.000000, 835.000000);
  v3f(5335.000000, 4879.000000, 835.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2585.000000, 4879.000000, 2532.000000);
  v3f(5335.000000, 4879.000000, 2532.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2585.000000, 4879.000000, 835.000000);
  v3f(2585.000000, 4879.000000, 2532.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5335.000000, 4879.000000, 835.000000);
  v3f(5335.000000, 4879.000000, 2532.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(3960.000000, 4879.000000, 835.000000);
  v3f(3960.000000, 4879.000000, 2532.000000);
  glEnd();
	//--------------------------------------------

  glColor3f(1., 1., 1.);
	// LINEAS LIMITROFES DEL CAMPO FUTBOL
  glBegin(GL_LINES);
  v3f(1560.000000, 596.000000, 30.000000);
  v3f(1560.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 30.000000);
  v3f(2419.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1560.000000, 596.000000, 30.000000);
  v3f(7925.000000, 596.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 30.000000);
  v3f(7925.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 796.000000, 30.000000);
  v3f(2160.000000, 4301.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 796.000000, 30.000000);
  v3f(7560.000000, 796.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 4301.000000, 30.000000);
  v3f(7560.000000, 4301.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 796.000000, 30.000000);
  v3f(7560.000000, 4301.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4860.000000, 796.000000, 30.000000);
  v3f(4860.000000, 4301.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 2051.000000, 30.000000);
  v3f(2810.000000, 2051.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2810.000000, 2051.000000, 30.000000);
  v3f(2810.000000, 3351.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2810.000000, 3351.000000, 30.000000);
  v3f(2160.000000, 3351.000000, 30.000000 );
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 2051.000000, 30.000000);
  v3f(6910.000000, 2051.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 3351.000000, 30.000000);
  v3f(6910.000000, 3351.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6910.000000, 2051.000000, 30.000000);
  v3f(6910.000000, 3351.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 2341.000000, 30.000000);
  v3f(4500.000000, 3061.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5220.000000, 2341.000000, 30.000000);
  v3f(5220.000000, 3061.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 2341.000000, 30.000000);
  v3f(5220.000000, 2341.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 3061.000000, 30.000000);
  v3f(5220.000000, 3061.000000, 30.000000);
  glEnd();
}

void drawRoboticLabGround () {
  glLineWidth(2.0f);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

	glColor3f (1.,1.,1.);
  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 0.000000);
  v3f(0.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 0.000000);
  v3f(1211.000000, 0.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, 0.000000, 0.000000);
  v3f(1211.000000, -554.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, -554.000000, 0.000000);
  v3f(5476.000000, -554.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -554.000000, 0.000000);
  v3f(5476.000000, -119.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -119.000000, 0.000000);
  v3f(6110.000000, -119.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -119.000000, 0.000000);
  v3f(6110.000000, -554.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -554.000000, 0.000000);
  v3f(7925.000000, -554.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, -554.000000, 0.000000);
  v3f(7925.000000, 4879.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, 4879.000000, 0.000000);
  v3f(6264.000000, 4879.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4879.000000, 0.000000);
  v3f(6264.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 0.000000);
  v3f(5498.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 0.000000);
  v3f(5498.000000, 4879.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4879.000000, 0.000000);
  v3f(2419.000000, 4879.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4879.000000, 0.000000);
  v3f(2419.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4589.000000, 0.000000);
  v3f(0.000000, 4589.000000, 0.000000);
  glEnd();

  glColor3f(1., 1., 1.);
	// LINEAS LIMITROFES DEL CAMPO FUTBOL
  glBegin(GL_LINES);
  v3f(1560.000000, 596.000000, 0.000000);
  v3f(1560.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 0.000000);
  v3f(2419.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1560.000000, 596.000000, 0.000000);
  v3f(7925.000000, 596.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 0.000000);
  v3f(7925.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 796.000000, 0.000000);
  v3f(2160.000000, 4301.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 796.000000, 0.000000);
  v3f(7560.000000, 796.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 4301.000000, 0.000000);
  v3f(7560.000000, 4301.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 796.000000, 0.000000);
  v3f(7560.000000, 4301.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4860.000000, 796.000000, 0.000000);
  v3f(4860.000000, 4301.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 2051.000000, 0.000000);
  v3f(2810.000000, 2051.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2810.000000, 2051.000000, 0.000000);
  v3f(2810.000000, 3351.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2810.000000, 3351.000000, 0.000000);
  v3f(2160.000000, 3351.000000, 0.000000 );
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 2051.000000, 0.000000);
  v3f(6910.000000, 2051.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 3351.000000, 0.000000);
  v3f(6910.000000, 3351.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6910.000000, 2051.000000, 0.000000);
  v3f(6910.000000, 3351.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 2341.000000, 0.000000);
  v3f(4500.000000, 3061.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5220.000000, 2341.000000, 0.000000);
  v3f(5220.000000, 3061.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 2341.000000, 0.000000);
  v3f(5220.000000, 2341.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 3061.000000, 0.000000);
  v3f(5220.000000, 3061.000000, 0.000000);
  glEnd();
}


/* We want to know the intersection between a line given by two points (A and B) on 3D, and a plan (the ground, for example). Then, we'll get another point; it'll be our solution */
int linePlaneIntersection (HPoint3D A, HPoint3D B, HPoint3D *intersectionPoint) {
	HPoint3D v;	// Line director vector: it the same to take A or B as origin or destination extrem...

	A.X = -A.X;
	A.Y = -A.Y;
	A.Z = -A.Z;

	B.X = -B.X;
	B.Y = -B.Y;
	B.Z = -B.Z;

	v.X = (A.X - B.X);
	v.Y = (A.Y - B.Y);
	v.Z = (A.Z - B.Z);

	// We'll calculate the groun intersection (Z = 0) on our robot system. Parametric equations:
	intersectionPoint->Z = 0; // intersectionPoint->Z = A.Z + t*v.Z => t = (-A.Z / v.Z)
	t = (-A.Z) / (v.Z);

	intersectionPoint->X = A.X + (t*v.X);
	intersectionPoint->Y = A.Y + (t*v.Y);
}

void pointProjection (HPoint2D point, char **myColor, int puntosExtremos) {
	float temp;

	if (myColor == mycolorA)
		myCamera = &robotCamera;

	myActualPoint2D.y = point.x;
	myActualPoint2D.x = point.y;
	myActualPoint2D.h = 1.;

	// Proyectamos tal punto en 3D sobre el Plano Imagen de nuestra cámara virtual
	backproject(&myActualPoint3D, myActualPoint2D, *myCamera);

	// Coordenadas en 3D de la posicion de la cámara
	cameraPos3D.X = myCamera->position.X;
	cameraPos3D.Y = myCamera->position.Y;
	cameraPos3D.Z = myCamera->position.Z;
	cameraPos3D.H = 1;
/*
	// Distancia entre los puntos myActualPoint3D - cameraPos3D
	distanciaPlanoImagen=(float)sqrt((double)((myActualPoint3D.X-cameraPos3D.X)*(myActualPoint3D.X-cameraPos3D.X)+(myActualPoint3D.Y-cameraPos3D.Y)*(myActualPoint3D.Y-cameraPos3D.Y)+(myActualPoint3D.Z-cameraPos3D.Z)*(myActualPoint3D.Z-cameraPos3D.Z)));
	incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"
	
	// Alargamos tal distancia (lanzamiento del rayo óptico)
	myEndPoint3D.X = ((incremento * myActualPoint3D.X + (1. - incremento) * cameraPos3D.X));
	myEndPoint3D.Y = ((incremento * myActualPoint3D.Y + (1. - incremento) * cameraPos3D.Y));
	myEndPoint3D.Z = ((incremento * myActualPoint3D.Z + (1. - incremento) * cameraPos3D.Z));
	myEndPoint3D.H = 1.;
*/

	linePlaneIntersection (myActualPoint3D, cameraPos3D, &intersectionPoint); // luego negaremos los puntos

	if (puntosExtremos == TRUE) {
		if (primerPunto == FALSE) { // no está establecido aun el primer punto
			primerPunto3D.X = intersectionPoint.X;
			primerPunto3D.Y = intersectionPoint.Y;
			primerPunto3D.Z = intersectionPoint.Z;
			primerPunto = TRUE;
		} else if (segundoPunto == FALSE) { // no está establecido aun el segundo punto
			segundoPunto3D.X = intersectionPoint.X;
			segundoPunto3D.Y = intersectionPoint.Y;
			segundoPunto3D.Z = intersectionPoint.Z;
			segundoPunto = TRUE;
		}
	}
	else {
		if ((fronteraView == FALSE) && (groundView == TRUE)) {
			glColor3f (0.5,0.5,0.5);
		  glBegin(GL_LINES);
			  v3f(-cameraPos3D.X, -cameraPos3D.Y, -cameraPos3D.Z);  
			  v3f(-myActualPoint3D.X, -myActualPoint3D.Y, -myActualPoint3D.Z);
		  glEnd();
		} else if (fronteraView == TRUE) {
			if (actualFronteraPoint == ((SIFNTSC_COLUMNS) - 1))
				actualFronteraPoint = 0;

			fronteraPoints[actualFronteraPoint].X = intersectionPoint.X;
			fronteraPoints[actualFronteraPoint].Y = intersectionPoint.Y;
			fronteraPoints[actualFronteraPoint].Z = intersectionPoint.Z;

			actualFronteraPoint ++;
		}

		if ((groundView == TRUE) && (flash == TRUE)) {
			if (actualGroundPoint == ((SIFNTSC_COLUMNS*SIFNTSC_ROWS*4) - 1))
				actualGroundPoint = 0;
/*
			groundPoints[actualGroundPoint].X = intersectionPoint.X;
			groundPoints[actualGroundPoint].Y = intersectionPoint.Y;
			groundPoints[actualGroundPoint].Z = intersectionPoint.Z;*/

			// Transformación de absoluto a relativo al robot, con las respectivas transformaciones
			groundPoints[actualGroundPoint].X = -280.4 + intersectionPoint.X * cos (DEGTORAD*(0)) - intersectionPoint.Y * sin (DEGTORAD*(0));
			groundPoints[actualGroundPoint].Y = -265.3 + intersectionPoint.X * sin (DEGTORAD*(0)) + intersectionPoint.Y * cos (DEGTORAD*(0));
			groundPoints[actualGroundPoint].Z = intersectionPoint.Z;

			// Transformación de relativo al robot a absoluto
			temp = 280.4 + groundPoints[actualGroundPoint].X * cos (DEGTORAD*(360-rotacion)) - groundPoints[actualGroundPoint].Y * sin (DEGTORAD*(360-rotacion));
			groundPoints[actualGroundPoint].Y = 265.3 + groundPoints[actualGroundPoint].X * sin (DEGTORAD*(360-rotacion)) + groundPoints[actualGroundPoint].Y * cos (DEGTORAD*(360-rotacion));
			groundPoints[actualGroundPoint].X = temp;

			actualGroundPoint ++;
		}
	}
}

void drawOpticalRays (TPinHoleCamera *cam) {
  HPoint3D a3A;
  HPoint2D a;

	glColor3f( 0.7, 0.7, 0.7 );

	cameraPos3D.X=0.;
	cameraPos3D.Y=0.;
	cameraPos3D.Z=0.;
	cameraPos3D.H=1.;

	/* optical axis of camera */
	a.x = SIFNTSC_ROWS-1;
	a.y = SIFNTSC_COLUMNS-1;
	a.h = 1.;
	backproject (&a3A, a, *cam);

	distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
	incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

	a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
	a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
	a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
	a3A.H = 1.;

	glBegin(GL_LINES);
		v3f( a3A.X, a3A.Y, a3A.Z );
		v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
	glEnd();

	/**/
	a.x = 0;
	a.y = 0;
	a.h = 1.;
	backproject (&a3A, a, *cam);

	distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
	incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

	a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
	a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
	a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
	a3A.H = 1.;

	glBegin(GL_LINES);
		v3f( a3A.X, a3A.Y, a3A.Z );
		v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
	glEnd();

	/**/
	a.x = SIFNTSC_ROWS-1;
	a.y = 0;
	a.h = 1.;
	backproject (&a3A, a, *cam);

	distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
	incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

	a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
	a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
	a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
	a3A.H = 1.;

	glBegin(GL_LINES);
		v3f( a3A.X, a3A.Y, a3A.Z );
		v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
	glEnd();

	/**/
	a.x = 0;
	a.y = SIFNTSC_COLUMNS-1;
	a.h = 1.;
	backproject (&a3A, a, *cam);

	distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
	incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

	a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
	a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
	a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
	a3A.H = 1.;

	glBegin(GL_LINES);
		v3f( a3A.X, a3A.Y, a3A.Z );
		v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
	glEnd();
}

void drawCam(TPinHoleCamera *cam, int b)
{
  HPoint3D a3A, a3B, a3C, a3D;
  HPoint2D a;

/*	if (b==2){
		glColor3f( 0.7, 0.7, 0.7 );
	} else if (b==3){
		glColor3f( 0.7, 0.7, 0.7 );
	} else if (b==4){
		glColor3f( 0.7, 0.7, 0.7 );
	} else if (b==5){
		glColor3f( 0.7, 0.7, 0.7 );
	}
*/
	glColor3f( 0.7, 0.7, 0.7 );

	cameraPos3D.X=-cam->position.X;
	cameraPos3D.Y=-cam->position.Y;
	cameraPos3D.Z=-cam->position.Z;
	cameraPos3D.H=cam->position.H;

	/* optical axis of camera */
	if ((mycolorA!=NULL) && (cam == &robotCamera)) {
		a.x = SIFNTSC_ROWS-1;
		a.y = SIFNTSC_COLUMNS-1;
		a.h = 1.;
		//a.y = GRAPHIC_TO_OPTICAL_Y(a.y,a.x);
		//a.x = GRAPHIC_TO_OPTICAL_X(a.y,a.x);
		backproject (&a3A, a, *cam);

		a3A.X = -a3A.X;
		a3A.Y = -a3A.Y;
		a3A.Z = -a3A.Z;

		distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
		incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

		a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
		a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
		a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
		a3A.H = 1.;

		glBegin(GL_LINES);
			v3f( a3A.X, a3A.Y, a3A.Z );
			v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glEnd();

		/**/
		a.x = 0;
		a.y = 0;
		a.h = 1.;
		//a.y = GRAPHIC_TO_OPTICAL_Y(a.y,a.x);
		//a.x = GRAPHIC_TO_OPTICAL_X(a.y,a.x);

		backproject (&a3A, a, *cam);

		distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
		incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

		a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
		a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
		a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
		a3A.H = 1.;

		glBegin(GL_LINES);
			v3f( -a3A.X, -a3A.Y, -a3A.Z );
			v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glEnd();

		/**/
		a.x = SIFNTSC_ROWS-1;
		a.y = 0;
		a.h = 1.;
		//a.y = GRAPHIC_TO_OPTICAL_Y(a.y,a.x);
		//a.x = GRAPHIC_TO_OPTICAL_X(a.y,a.x);

		backproject (&a3A, a, *cam);

		a3A.X = -a3A.X;
		a3A.Y = -a3A.Y;
		a3A.Z = -a3A.Z;

		distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
		incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

		a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
		a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
		a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
		a3A.H = 1.;

		glBegin(GL_LINES);
			v3f( a3A.X, a3A.Y, a3A.Z );
			v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glEnd();

		/**/
		a.x = 0;
		a.y = SIFNTSC_COLUMNS-1;
		a.h = 1.;
		//a.y = GRAPHIC_TO_OPTICAL_Y(a.y,a.x);
		//a.x = GRAPHIC_TO_OPTICAL_X(a.y,a.x);

		backproject (&a3A, a, *cam);

		a3A.X = -a3A.X;
		a3A.Y = -a3A.Y;
		a3A.Z = -a3A.Z;

		distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
		incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

		a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
		a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
		a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
		a3A.H = 1.;

		glBegin(GL_LINES);
			v3f( a3A.X, a3A.Y, a3A.Z );
			v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glEnd();
	} // fin if (dibujamos los rayos opticos, si estamos en la cámara oportuna...)

	/* FieldOfView of camera */
	a.x = SIFNTSC_ROWS - 1.;
	a.y = SIFNTSC_COLUMNS - 1.;
	a.h = 1.;
	backproject(&a3A, a, *cam);
	a.x = 0.;
	a.y = SIFNTSC_COLUMNS - 1.;
	a.h = 1.;
	backproject(&a3B, a, *cam);
	a.x = 0.;
	a.y = 0.;
	a.h = 1.;
	backproject(&a3C, a, *cam);
	a.x = SIFNTSC_ROWS - 1.;
	a.y = 0.;
	a.h = 1.;
	backproject(&a3D, a, *cam);

	// first triangle
	glBegin(GL_LINES);
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glVertex3f(-a3A.X, -a3A.Y, -a3A.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(-a3A.X, -a3A.Y, -a3A.Z );
		glVertex3f(-a3B.X, -a3B.Y, -a3B.Z );
	glEnd ();

	glBegin(GL_LINES);
		glVertex3f(-a3B.X, -a3B.Y, -a3B.Z );
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
	glEnd ();

	glColor3f( 0.3, 0.3, 0.3 );
	// second triangle
	glBegin(GL_LINES);
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glVertex3f(-a3B.X, -a3B.Y, -a3B.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(-a3B.X, -a3B.Y, -a3B.Z );
		glVertex3f(-a3C.X, -a3C.Y, -a3C.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(-a3C.X, -a3C.Y, -a3C.Z );
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
	glEnd();

	glColor3f( 0.7, 0.7, 0.7 );
	// third triangle
	glBegin(GL_LINES);
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glVertex3f(-a3C.X, -a3C.Y, -a3C.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(-a3C.X, -a3C.Y, -a3C.Z );
		glVertex3f(-a3D.X, -a3D.Y, -a3D.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(-a3D.X, -a3D.Y, -a3D.Z );
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
	glEnd();

	glColor3f( 0.3, 0.3, 0.3 );
	// last triangle
	glBegin(GL_LINES);
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glVertex3f(-a3D.X, -a3D.Y, -a3D.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(-a3D.X, -a3D.Y, -a3D.Z );
		glVertex3f(-a3A.X, -a3A.Y, -a3A.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(-a3A.X, -a3A.Y, -a3A.Z );
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
	glEnd();

	glColor3f( 0.1, 0.1, 0.1 );
	// square
	glBegin(GL_LINES);
		glVertex3f(-a3A.X, -a3A.Y, -a3A.Z );
		glVertex3f(-a3D.X, -a3D.Y, -a3D.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(-a3D.X, -a3D.Y, -a3D.Z );
		glVertex3f(-a3C.X, -a3C.Y, -a3C.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(-a3C.X, -a3C.Y, -a3C.Z );
		glVertex3f(-a3B.X, -a3B.Y, -a3B.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(-a3B.X, -a3B.Y, -a3B.Z );
		glVertex3f(-a3A.X, -a3A.Y, -a3A.Z );
	glEnd();
}

void initOccupancyGrid () {
	int i;

	centrogrid.x = 275.9;//myencoders[0];
	centrogrid.y = 276.5;//myencoders[1];

	occupancyGrid = inicia_grid (centrogrid, configFile); // memoria

	tempMap.point = (Tvoxel*)malloc((occupancyGrid->size*occupancyGrid->size)*sizeof(Tvoxel));
	tempMap.state = (int*)malloc((occupancyGrid->size*occupancyGrid->size)*sizeof(int));
	tempMap.updatingTime = (unsigned long int*)malloc((occupancyGrid->size*occupancyGrid->size)*sizeof(unsigned long int));

	for(i=0;i< ((occupancyGrid->size)*(occupancyGrid->size)) ;i++) {
		occupancyGrid->map[i].estado = EMPTY;
		occupancyGrid->map[i].ultima_actualizacion = dameTiempo ();

		tempMap.point[i] = occupancyGrid->map[i].centro;
		tempMap.state[i] = EMPTY;
		tempMap.updatingTime[i] = occupancyGrid->map[i].ultima_actualizacion;
	}

	if (occupancyGrid==NULL) { // si no hubo ningún problema en la inicialización
/*		if (DEBUG) printf("Occupancy Grid initialized correctly--> Size %.0f mm, each cell %.0f mm\n",occupancyGrid->size*occupancyGrid->resolucion,occupancyGrid->resolucion);
	} else {*/
		if (DEBUG) printf("Not enough memory for occupancy grid\n");
		exit (-1);
	}
} 

void updateRejillaGPP () {
	int i, j, k, celda;
	Tvoxel tope, posRobot, actual;
	int finRayo;
	int celdilla;

	updateGridTiming ();

	// 1º.- Actualizaremos los puntos de forma que solaparemos los que ya teníamos, en la nueva rejilla
	if (pendienteDeReubicacionGrafica) { // se ha reubicado la rejilla, por lo que puede haber solapamiento
		if (DEBUG) printf("Updating occupancy grid according to previous occupancy grid and actual visual information...\n");
		for(j=0;j< ((occupancyGrid->size)*(occupancyGrid->size)); j++) {
			occupancyGrid->map[j].estado = EMPTY; // inicialización: desconocemos la nueva zona
			occupancyGrid->map[j].ultima_actualizacion = dameTiempo ();
			for(k=0;k< ((occupancyGrid->size)*(occupancyGrid->size)); k++) {
				if ((occupancyGrid->map[j].centro.x == tempMap.point[k].x) && (occupancyGrid->map[j].centro.y == tempMap.point[k].y)) {
					occupancyGrid->map[j].estado = tempMap.state[k]; // solapamos estado: es zona conocida
					occupancyGrid->map[j].ultima_actualizacion = tempMap.updatingTime[k]; // nos quedamos con su tiempo
					break;
				}
			}
		}
		pendienteDeReubicacionGrafica = FALSE;
	}

	posRobot.x = 0; // TODO:myencoders [0];
	posRobot.y = 0; // TODO:myencoders [1];

	// 2º.- Ahora actualizaremos según la información que nos llega de los lásers: zona conocida también y actualizada
	
}

/*Callback of image clicked*/
void on_image_clicked(GtkWidget *widget, gpointer user_data)
{
/*	TList *aux;
	char *widgetname;
	int match;

	if (persons != NULL)
	{
		aux = persons;
		match = 0;
		widgetname = gtk_widget_get_name(widget);
		match = strcmp(widgetname,aux->person.widgetid);
		while ((aux->next != NULL) && (match != 0))
		{
			aux = aux->next;
			match = strcmp(widgetname,aux->person.widgetid);
		}
		printf("X = %f Y = %f\n",aux->person.location.x,aux->person.location.y);
		*target = aux->person.location;
	}*/
}

void frontera_iteration()
{  
	//drawFilteredImage();
}

/*Importar símbolos*/
void frontera_imports()
{
	/* importar colorA */
	mycolorA = (char**) myimport("colorA","colorA");
	colorArun = (runFn) myimport("colorA", "run");
	colorAstop	= (stopFn) myimport("colorA", "stop");
	mycolorAwidth	= (int*) myimport("colorA", "width");
	mycolorAheight = (int*) myimport("colorA", "height");
/*
	mycolorA = (char**) myimport("colorA","colorA");
	colorArun = (runFn) myimport("colorA", "run");
	colorAstop	= (stopFn) myimport("colorA", "stop");
	mycolorAwidth	= (int*) myimport("colorA", "width");
	mycolorAheight = (int*) myimport("colorA", "height");
*/
	if (mycolorA == NULL || colorArun == NULL || colorAstop == NULL){
	  fprintf(stderr, "I can't import variables from colorA\n");
	  jdeshutdown(1);
	}
}

void frontera_stop()
{
	colorAstop();

	RGB2HSV_destroyTable();

	pthread_mutex_lock(&(all[frontera_id].mymutex));
	put_state(frontera_id,slept);
	printf("frontera: off\n");
	pthread_mutex_unlock(&(all[frontera_id].mymutex));

	free(imageA);
	free(imageAbordered);
	free(imageAfiltered);

	remove_image(originalImageRGB);
	remove_image(borderImageRGB);
	remove_image(filteredImageRGB);
}


void frontera_run(int father, int *brothers, arbitration fn)
{
	int i;

	printf ("run1\n");

  pthread_mutex_lock(&(all[frontera_id].mymutex)); // CERROJO -- LOCK
  /* this schema resumes its execution with no children at all */
  for(i=0;i<MAX_SCHEMAS;i++) all[frontera_id].children[i]=FALSE;
 
  all[frontera_id].father=father;
  if (brothers!=NULL)
    {
      for(i=0;i<MAX_SCHEMAS;i++) frontera_brothers[i]=-1;
      i=0;
      while(brothers[i]!=-1) {frontera_brothers[i]=brothers[i];i++;}
    }
  frontera_callforarbitration=fn;
  put_state(frontera_id,notready);
	printf ("run2\n");
  printf("frontera: on\n");
  pthread_cond_signal(&(all[frontera_id].condition));
  pthread_mutex_unlock(&(all[frontera_id].mymutex)); // CERROJO -- UNLOCK

  frontera_imports();
  /* Wake up drivers schemas */	
  colorArun(frontera_id, NULL, NULL);

	myHSV = (struct HSV*)malloc(sizeof(struct HSV));

	printf ("run3\n");
  //imageA = create_image(*mycolorAwidth, *mycolorAheight, 3);
  imageA = create_image(SIFNTSC_COLUMNS, SIFNTSC_ROWS, 3);
  free(imageA->image); // vaciamos el espacio reservado para la imagen
  imageA->image = (char *) *mycolorA; // asignamos directamente lo que viene de mycolorA

  //imageAfiltered = create_image(*mycolorAwidth, *mycolorAheight, 3);
  imageAfiltered = create_image(SIFNTSC_COLUMNS, SIFNTSC_ROWS, 3);
  //free(imageAfiltered->image); // vaciamos el espacio reservado para la imagen
  //drawFilteredImage ();

	printf ("run4\n");

  //imageAbordered = create_image(*mycolorAwidth, *mycolorAheight, 3);
  imageAbordered = create_image(SIFNTSC_COLUMNS, SIFNTSC_ROWS, 3);
  //free(imageAbordered->image); // vaciamos el espacio reservado para la imagen
  //drawGroundImage ();

	printf ("run5\n");

	//originalImageRGB = create_image(*mycolorAwidth, *mycolorAheight, 3);
  originalImageRGB = create_image(SIFNTSC_COLUMNS, SIFNTSC_ROWS, 3);
	//filteredImageRGB = create_image(*mycolorAwidth, *mycolorAheight, 3);
  filteredImageRGB = create_image(SIFNTSC_COLUMNS, SIFNTSC_ROWS, 3);
	//borderImageRGB = create_image(*mycolorAwidth, *mycolorAheight, 3);
  borderImageRGB = create_image(SIFNTSC_COLUMNS, SIFNTSC_ROWS, 3);
}

void *frontera_thread(void *not_used)
{
  struct timeval a,b;
  long diff, next;

  for(;;)
    {
      pthread_mutex_lock(&(all[frontera_id].mymutex));

      if (all[frontera_id].state==slept) 
	{
	  pthread_cond_wait(&(all[frontera_id].condition),&(all[frontera_id].mymutex));
	  pthread_mutex_unlock(&(all[frontera_id].mymutex));
	}
      else 
	{
	  /* check preconditions. For now, preconditions are always satisfied*/
	  if (all[frontera_id].state==notready) put_state(frontera_id,ready);
	  else if (all[frontera_id].state==ready)	  /* check brothers and arbitrate. For now this is the only winner */
	    { 
	      put_state(frontera_id,winner);
	    }	  
	  else if (all[frontera_id].state==winner);

	  if (all[frontera_id].state==winner)
	    /* I'm the winner and must execute my iteration */
	    {
	      pthread_mutex_unlock(&(all[frontera_id].mymutex));
	      gettimeofday(&a,NULL);
	      frontera_iteration();
	      gettimeofday(&b,NULL);  

	      diff = (b.tv_sec-a.tv_sec)*1000000+b.tv_usec-a.tv_usec;
	      next = frontera_cycle*1000-diff-10000; 
	      if (next>0) 
		/* discounts 10ms taken by calling usleep itself */
		usleep(frontera_cycle*1000-diff);
	      else 
		/* just let this iteration go away. overhead time negligible */
		{printf("time interval violated: frontera\n"); 
		usleep(frontera_cycle*1000);
		}
	    }
	  else 
	    {
	      pthread_mutex_unlock(&(all[frontera_id].mymutex));
	      usleep(frontera_cycle*1000);
	    }
	}
    }
}

void frontera_terminate()
{
  pthread_mutex_lock(&(all[frontera_id].mymutex));
  frontera_stop();  
  pthread_mutex_unlock(&(all[frontera_id].mymutex));
  sleep(2);
  //fl_free_form(fd_fronteragui->fronteragui);
//  free(imagenA_buf);
//  free(imagenFiltrada_buf);
//  free(imagenSuelo_buf);
}

void createConfigFile () {
	FILE *salida;
	salida = fopen (configFile,"w");

	if (salida == NULL) {
		fprintf(stderr,"Parser: I can't found file %s\n", configFile); exit(-1);
	}

	printf("Building config file %s...\n", configFile);
	fprintf(salida,"dimension = %d\n", 4000);
	fprintf(salida,"resolucion = 50\n");
	fprintf(salida,"tipo = ec_diferencial\n");
	fprintf(salida,"ec_diferencial_speed = 1\n");
	fprintf(salida,"paso_tiempo = nulo\n");
	fprintf(salida,"cell_angles = 0\n");
	fprintf(salida,"mayoria_saturacion = 30\n");
	fprintf(salida,"mayoria_ruido = 10\n");
	fprintf(salida,"long_historia = 0\n");
	fprintf(salida,"\n");
	fprintf(salida,"sonar_filtra = independientes\n");
	fprintf(salida,"sonar_geometria = cono_denso\n");
	fprintf(salida,"sonar_apertura = 20.\n");
	fprintf(salida,"sonar_noobstacle = 3000.\n");
	fprintf(salida,"sonar_radialerror = 10.\n");
	fprintf(salida,"sonar_fdistancia = lineal\n");
	fprintf(salida,"sonar_residuo = 0.05\n");
	fprintf(salida,"sonar_o = 0.4\n");
	fprintf(salida,"sonar_e = -0.5\n");
	fprintf(salida,"sonar_mind = 700\n");
	fprintf(salida,"sonar_maxd = 1100.\n");
	fprintf(salida,"\n");
	fprintf(salida,"robot_geometria = cilindro\n");
	fprintf(salida,"robot_radio = 248.\n");
	fprintf(salida,"robot_e = -0.8\n");
	fprintf(salida,"\n");
	fprintf(salida,"laser_geometria = cono_denso\n");
	fprintf(salida,"laser_apertura = 0.5\n");
	fprintf(salida,"laser_muestras = 90\n");
	fprintf(salida,"laser_noobstacle = 8000.\n");
	fprintf(salida,"laser_o = 1\n");
	fprintf(salida,"laser_e = -0.7\n");

	fclose(salida);
}

/*Exportar símbolos*/
void frontera_exports()
{
   myexport("frontera","cycle",&frontera_cycle);
   myexport("frontera","resume",(void *)frontera_run);
   myexport("frontera","suspend",(void *)frontera_stop);
}

void frontera_init(char *configfile)
{
  pthread_mutex_lock(&(all[frontera_id].mymutex)); // CERROJO -- LOCK

  printf("frontera schema started up\n");
  frontera_exports();
  put_state(frontera_id,slept);
  pthread_create(&(all[frontera_id].mythread),NULL,frontera_thread,NULL);
  if (myregister_displaycallback==NULL){
		if ((myregister_displaycallback=(registerdisplay)myimport ("graphics_gtk", "register_displaycallback"))==NULL)
		{
		  printf ("I can't fetch register_displaycallback from graphics_gtk\n");
		  jdeshutdown(1);
		}
		if ((mydelete_displaycallback=(deletedisplay)myimport ("graphics_gtk", "delete_displaycallback"))==NULL)
		{
		  printf ("I can't fetch delete_displaycallback from graphics_gtk\n");
		  jdeshutdown(1);
		}
  }

  pthread_mutex_unlock(&(all[frontera_id].mymutex)); // CERROJO -- UNLOCK

	// Valores iniciales para la cámara virtual con la que observo la escena de puntos 3D en el suelo
  virtualcam0.position.X=4000.;
  virtualcam0.position.Y=4000.;
  virtualcam0.position.Z=6000.;
  virtualcam0.position.H=1.;
  virtualcam0.foa.X=0.;
  virtualcam0.foa.Y=0.;
  virtualcam0.foa.Z=0.;
  virtualcam0.position.H=1.;
  virtualcam0.roll=0.;

	// Valores iniciales para la cámara virtual con la que observo la escena
  virtualcam1.position.X=4000;
  virtualcam1.position.Y=4000.;
  virtualcam1.position.Z=6000.;
  virtualcam1.position.H=1.;
  virtualcam1.foa.X=0.;
  virtualcam1.foa.Y=0.;
  virtualcam1.foa.Z=0.;
  virtualcam1.position.H=1.;
  virtualcam1.roll=0.;

	// Valores para la cámara virtual con la que observo el entorno de frontera al robot
  virtualcam2.position.X=4000.;
  virtualcam2.position.Y=4000.;
  virtualcam2.position.Z=6000.;
  virtualcam2.position.H=1.;
  virtualcam2.foa.X=0.;
  virtualcam2.foa.Y=0.;
  virtualcam2.foa.Z=0.;
  virtualcam2.foa.H=1.;
  virtualcam2.roll=0.;

	// Valores para la cámara del techo del laboratorio
  ceilLabCam.position.X=3912.000000;
  ceilLabCam.position.Y=2117.000000;
  ceilLabCam.position.Z=12000.000000;
  ceilLabCam.position.H=1.;
  ceilLabCam.foa.X=3000.000000;
  ceilLabCam.foa.Y=2000.0000000;
  ceilLabCam.foa.Z=0.00000;
  ceilLabCam.foa.H=1.;
  //ceilLabCam.fdist=405.399994;
  ceilLabCam.u0=142.600006;
  ceilLabCam.v0=150.399994;
  ceilLabCam.roll=1.55;

	// Valores para la cámara virtual de la esquina derecha del laboratorio (myColorA)
  roboticLabCam0.position.X=7875.000000;
  roboticLabCam0.position.Y=-514.000000;
  roboticLabCam0.position.Z=3000.000000;
  roboticLabCam0.position.H=1.;
  roboticLabCam0.foa.X=6264.000000;
  roboticLabCam0.foa.Y=785.0000000;
  roboticLabCam0.foa.Z=1950.00000;
  roboticLabCam0.foa.H=1.;
  //roboticLabCam0.fdist=405.399994;
  roboticLabCam0.u0=142.600006;
  roboticLabCam0.v0=150.399994;
  roboticLabCam0.roll=3.107262;

	// Valores para la cámara virtual de la esquina derecha de enfrente del laboratorio (myColorB)
  roboticLabCam1.position.X=7875.000000;
  roboticLabCam1.position.Y=4749.000000;
  roboticLabCam1.position.Z=3000.000000;
  roboticLabCam1.position.H=1.;
  roboticLabCam1.foa.X=6264.000000;
  roboticLabCam1.foa.Y=3700.000000;
  roboticLabCam1.foa.Z=1950.00000;
  roboticLabCam1.foa.H=1.;
  //roboticLabCam1.fdist=405.399994;
  roboticLabCam1.u0=142.600006;
  roboticLabCam1.v0=142.600006;
  roboticLabCam1.roll=3.007028;

	// Valores para la cámara virtual de la esquina izquierda de enfrente del laboratorio (myColorC)
  roboticLabCam2.position.X=50.000000;
  roboticLabCam2.position.Y=4471.000000;
  roboticLabCam2.position.Z=2955.000000;
  roboticLabCam2.position.H=1.;
  roboticLabCam2.foa.X=2432.399902;
  roboticLabCam2.foa.Y=2918.000000;
  roboticLabCam2.foa.Z=1300.000000;
  roboticLabCam2.foa.H=1.;
  //roboticLabCam2.fdist=405.399994;
  roboticLabCam2.u0=142.600006;
  roboticLabCam2.v0=150.399994;
  roboticLabCam2.roll=3.107262;

	// Valores para la cámara virtual de la esquina izquierda del laboratorio (myColorD)
  roboticLabCam3.position.X=50.000000;
  roboticLabCam3.position.Y=100.000000;
  roboticLabCam3.position.Z=2955.000000;
  roboticLabCam3.position.H=1.;
  roboticLabCam3.foa.X=2160.000000;
  roboticLabCam3.foa.Y=1151.000000;
  roboticLabCam3.foa.Z=1550.000000;
  roboticLabCam3.foa.H=1.;
  //roboticLabCam3.fdist=405.399994;
  roboticLabCam3.u0=142.600006;
  roboticLabCam3.v0=150.399994;
  roboticLabCam3.roll=2.956911;

	update_camera_matrix (&roboticLabCam0);
	update_camera_matrix (&roboticLabCam1);
	update_camera_matrix (&roboticLabCam2);
	update_camera_matrix (&roboticLabCam3);
	update_camera_matrix (&ceilLabCam);

  actualCameraView = 0; // empezamos con la cámara del user

	initCalibration (); // calibramos la cámara del robot
	setRobotCameraPos (); // con los parámetros de calibración, posicionamos nuestra robotCamera

  xcam = robotCamera.position.X;
  ycam = robotCamera.position.X;
  zcam = robotCamera.position.X;

	configFile = "./gradientPlanning.conf";
	createConfigFile ();

	RGB2HSV_init();
	RGB2HSV_createTable();
}

static int InitOGL(int w, int h)
 /* Inicializa OpenGL con los parametros que diran como se visualiza. */
{
   glViewport(0,0,(GLint)w,(GLint)h);  
   glDrawBuffer(GL_BACK);
   glClearColor(0.6f, 0.8f, 1.0f, 0.0f);  
   glClearDepth(1.0);
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
   glEnable(GL_TEXTURE_2D);     /* Enable Texture Mapping */
   glEnable (GL_AUTO_NORMAL);
   glEnable (GL_NORMALIZE);  
   glEnable(GL_DEPTH_TEST);     /* Enables Depth Testing */
   glDepthFunc(GL_LESS);  
   glShadeModel(GL_SMOOTH);     /* Enables Smooth Color Shading */
   glEnable(GL_BLEND);
   glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

   return 0;
}

static int InitFronteraOGL(int w, int h)
 /* Inicializa OpenGL con los parametros que diran como se visualiza. */
{
   glViewport(0,0,(GLint)w,(GLint)h);  
   glDrawBuffer(GL_BACK);
   glClearColor(0.f, 0.8f, 0.5f, 0.0f);  
   glClearDepth(1.0);
   /* This Will Clear The Background Color To Light Blue */
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
   glEnable(GL_TEXTURE_2D);     /* Enable Texture Mapping */
   glEnable (GL_AUTO_NORMAL);
   glEnable (GL_NORMALIZE);  
   glEnable(GL_DEPTH_TEST);     /* Enables Depth Testing */
   glDepthFunc(GL_LESS);  
   glShadeModel(GL_SMOOTH);     /* Enables Smooth Color Shading */
   glEnable(GL_BLEND);
   glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

   return 0;
}

static int InitMiniOGL(int w, int h)
 /* Inicializa OpenGL con los parametros que diran como se visualiza. */
{
   glViewport(0,0,(GLint)w,(GLint)h);  
   glDrawBuffer(GL_BACK);
   glClearColor(0.f, 0.8f, 0.5f, 0.0f);  
   glClearDepth(1.0);
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
   glEnable(GL_TEXTURE_2D);     /* Enable Texture Mapping */
   glEnable (GL_AUTO_NORMAL);
   glEnable (GL_NORMALIZE);  
   glEnable(GL_DEPTH_TEST);     /* Enables Depth Testing */
   glDepthFunc(GL_LESS);  
   glShadeModel(GL_SMOOTH);     /* Enables Smooth Color Shading */
   glEnable(GL_BLEND);
   glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

   return 0;
}

void initFronteraCanvasSettings () {
  float r,lati,longi;

	if (!flashImage) {
	  if (foa_mode) {
	    longi=2*PI*mouse_on_fronteracanvas.x/360.;
	    lati=2*PI*mouse_on_fronteracanvas.y/360.;
	    r=500;
	    virtualcam2.foa.X=r*cos(lati)*cos(longi);
	    virtualcam2.foa.Y=r*cos(lati)*sin(longi);
	    virtualcam2.foa.Z=r*sin(lati);
	  }
	  
	  if (cam_mode) {
	    longi=2*PI*mouse_on_fronteracanvas.x/360.;
	    lati=2*PI*mouse_on_fronteracanvas.y/360.;
	    
	    virtualcam2.position.X=radius*cos(lati)*cos(longi);
	    virtualcam2.position.Y=radius*cos(lati)*sin(longi);
	    virtualcam2.position.Z=radius*sin(lati);
	    
	    virtualcam2.foa.X=0;
	    virtualcam2.foa.Y=0;
	    virtualcam2.foa.Z=0;
	  }
	}

  InitFronteraOGL(1064,568);
    
  /* Virtual camera */
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity(); 

  /* perspective projection. intrinsic parameters + frustrum */
  gluPerspective(45.,(GLfloat)1064/(GLfloat)568,1.0,50000.0);
  /* extrinsic parameters */
	if (actualCameraView == 0) // user camera view
		gluLookAt(xcam,ycam,zcam,foax,foay,foaz,0.,0.,1.);
	else if (actualCameraView == 1) // robotics lab camera 1
		gluLookAt(roboticLabCam0.position.X,roboticLabCam0.position.Y,roboticLabCam0.position.Z,roboticLabCam0.foa.X,roboticLabCam0.foa.Y,roboticLabCam0.foa.Z,0.,0.,1.);
	else if (actualCameraView == 2) // robotics lab camera 2
		gluLookAt(roboticLabCam1.position.X,roboticLabCam1.position.Y,roboticLabCam1.position.Z,roboticLabCam1.foa.X,roboticLabCam1.foa.Y,roboticLabCam1.foa.Z,0.,0.,1.);
	else if (actualCameraView == 3) // robotics lab camera 3
		gluLookAt(roboticLabCam2.position.X,roboticLabCam2.position.Y,roboticLabCam2.position.Z,roboticLabCam2.foa.X,roboticLabCam2.foa.Y,roboticLabCam2.foa.Z,0.,0.,1.);
	else if (actualCameraView == 4) // robotics lab camera 4
		gluLookAt(roboticLabCam3.position.X,roboticLabCam3.position.Y,roboticLabCam3.position.Z,roboticLabCam3.foa.X,roboticLabCam3.foa.Y,roboticLabCam3.foa.Z,0.,0.,1.);
	else if (actualCameraView == 5) // robotics lab ceil camera
		gluLookAt(ceilLabCam.position.X,ceilLabCam.position.Y,ceilLabCam.position.Z,ceilLabCam.foa.X,ceilLabCam.foa.Y,ceilLabCam.foa.Z,0.,0.,1.);

}

void initMiniCanvasSettings () {
  float r,lati,longi;

	if (!flashImage) {
	  if (foa_mode) {
	    longi=2*PI*mouse_on_minicanvas.x/360.;
	    lati=2*PI*mouse_on_minicanvas.y/360.;
	    r=500;
	    virtualcam0.foa.X=r*cos(lati)*cos(longi);
	    virtualcam0.foa.Y=r*cos(lati)*sin(longi);
	    virtualcam0.foa.Z=r*sin(lati);
	  }
	  
	  if (cam_mode) {
	    longi=2*PI*mouse_on_minicanvas.x/360.;
	    lati=2*PI*mouse_on_minicanvas.y/360.;
	    
	    virtualcam0.position.X=radius*cos(lati)*cos(longi);
	    virtualcam0.position.Y=radius*cos(lati)*sin(longi);
	    virtualcam0.position.Z=radius*sin(lati);
	    
	    virtualcam0.foa.X=0;
	    virtualcam0.foa.Y=0;
	    virtualcam0.foa.Z=0;
	  }
	}

  InitMiniOGL(600, 520);
    
  /* Virtual camera */
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity(); 

  /* perspective projection. intrinsic parameters + frustrum */
  gluPerspective(45.,(GLfloat)640/(GLfloat)480,1.0,50000.0);
  /* extrinsic parameters */
  gluLookAt(virtualcam0.position.X,virtualcam0.position.Y,virtualcam0.position.Z,virtualcam0.foa.X,virtualcam0.foa.Y,virtualcam0.foa.Z,0.,0.,1.);
}

void drawFilteredImage () {
	int i,c,row,j,k;
  double H, S, V;
  float myR, myG, myB;

  // FilteredImage display
/*	for (i=0, k=0**(SIFNTSC_ROWS*SIFNTSC_COLUMNS)-1**; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS; i++, k++) {
		c=i%(SIFNTSC_COLUMNS);
		row=i/(SIFNTSC_COLUMNS);
		j=row*SIFNTSC_COLUMNS+c;
*/
	for(i=0; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS; i++) {
		c=i%SIFNTSC_COLUMNS;
		row=i/SIFNTSC_COLUMNS;
		j=row*SIFNTSC_COLUMNS+c;

    if (mycolorA!=NULL) {
			imageAfiltered->image[i*3] = (*mycolorA)[j*3];
			imageAfiltered->image[i*3+1] = (*mycolorA)[j*3+1];
			imageAfiltered->image[i*3+2] = (*mycolorA)[j*3+2];
		} else { // no recibimos señal
			imageAfiltered->image[i*3]=0.;
			imageAfiltered->image[i*3+1]=0.;
			imageAfiltered->image[i*3+2]=0.;
    }
		//imageAfiltered->image[k*3+3]=0.; // dummy

		myR = (float)(unsigned int)(unsigned char)imageAfiltered->image[i*3+2];
		myG = (float)(unsigned int)(unsigned char)imageAfiltered->image[i*3+1];
		myB = (float)(unsigned int)(unsigned char)imageAfiltered->image[i*3+0];

		myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

		if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
			(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
			(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no pasamos el filtro de campo fútbol
			imageAfiltered->image[i*3]=0.;
			imageAfiltered->image[i*3+1]=0.;
			imageAfiltered->image[i*3+2]=0.;
		}
	}
}
/*
void drawImageA () {
	int i,c,row,j,k;

  // color imageA display
	if (mycolorA != NULL) {
    for(i=0, k=0; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS; i++, k++){
      c=i%SIFNTSC_COLUMNS;
      row=i/SIFNTSC_COLUMNS;
      j=row*SIFNTSC_COLUMNS+c;
      if (mycolorA!=NULL){
        imagenA_buf[k*4]=(*mycolorA)[j*3];
        imagenA_buf[k*4+1]=(*mycolorA)[j*3+1];
        imagenA_buf[k*4+2]=(*mycolorA)[j*3+2];
      }
      else{
        imagenA_buf[k*4]=0;
        imagenA_buf[k*4+1]=0;
        imagenA_buf[k*4+2]=0;
      }
      imagenA_buf[k*4+3]=UCHAR_MAX;
    }
	}
}
*/

void drawFronteraImage () {
  int i, j, c, row, pos, columnPoint, rowPoint, posPoint;

  double H, S, V;
  float myR, myG, myB;

	int esFrontera;
	int v1, v2, v3, v4, v5, v6, v7, v8;

	/* Ground image */
	for (j=0;j<ANCHO_IMAGEN;j++) { // recorrido en columnas
		i = LARGO_IMAGEN-1;
		esFrontera = FALSE;
		while ((i>=0) && (esFrontera == FALSE)) { // recorrido en filas
			if (mycolorA!=NULL) {
				myActualCamera = mycolorA;
				
			  pos = i*ANCHO_IMAGEN+j; // posicion actual

				myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[pos*3+2];
				myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[pos*3+1];
				myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[pos*3+0];

				myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

				if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
					(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
					(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si soy negro

					esFrontera = TRUE; // lo damos por frontera en un principio, luego veremos

					// Calculo los demás vecinos, para ver de qué color son...
					c = j - 1;
					row = i;
					v1 = row*ANCHO_IMAGEN+c;

					if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
						 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es válido ponemos sus valores a 0
						myR = 0.;
						myG = 0.;
						myB = 0.;
					} else {
						myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v1*3+2];
						myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v1*3+1];
						myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v1*3+0];
					}

					myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

					if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
						(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
						(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

						c = j - 1;
						row = i - 1;
						v2 = row*ANCHO_IMAGEN+c;

						if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
							 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es válido ponemos sus valores a 0
							myR = 0.;
							myG = 0.;
							myB = 0.;
						} else {
							myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v2*3+2];
							myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v2*3+1];
							myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v2*3+0];
						}

						myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

						if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
							(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
							(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

							c = j;
							row = i - 1;
							v3 = row*ANCHO_IMAGEN+c;

							if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
								 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es válido ponemos sus valores a 0
								myR = 0.;
								myG = 0.;
								myB = 0.;
							} else {
								myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v3*3+2];
								myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v3*3+1];
								myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v3*3+0];
							}

							myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

							if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
								(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
								(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

								c = j + 1;
								row = i - 1;
								v4 = row*ANCHO_IMAGEN+c;

								if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
									 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es válido ponemos sus valores a 0
									myR = 0.;
									myG = 0.;
									myB = 0.;
								} else {
									myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v4*3+2];
									myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v4*3+1];
									myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v4*3+0];
								}

								myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

								if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
									(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
									(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

									c = j + 1;
									row = i;
									v5 = row*ANCHO_IMAGEN+c;

									if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
										 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es válido ponemos sus valores a 0
										myR = 0.;
										myG = 0.;
										myB = 0.;
									} else {
										myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v5*3+2];
										myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v5*3+1];
										myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v5*3+0];
									}

									myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

									if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
										(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
										(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

										c = j + 1;
										row = i + 1;
										v6 = row*ANCHO_IMAGEN+c;

										if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
											 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es válido ponemos sus valores a 0
											myR = 0.;
											myG = 0.;
											myB = 0.;
										} else {
											myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v6*3+2];
											myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v6*3+1];
											myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v6*3+0];
										}

										myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

										if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
											(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
											(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

											c = j;
											row = i + 1;
											v7 = row*ANCHO_IMAGEN+c;

											if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
												 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es válido ponemos sus valores a 0
												myR = 0.;
												myG = 0.;
												myB = 0.;
											} else {
												myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v7*3+2];
												myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v7*3+1];
												myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v7*3+0];
											}

											myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

											if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
												(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
												(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

												c = j - 1;
												row = i + 1;
												v8 = row*ANCHO_IMAGEN+c;

												if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
													 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es válido ponemos sus valores a 0
													myR = 0.;
													myG = 0.;
													myB = 0.;
												} else {
													myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v8*3+2];
													myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v8*3+1];
													myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v8*3+0];
												}

												myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

												if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
													(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
													(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, se acabó, no es pto. frontera
													esFrontera = FALSE;
												} // fin if vecinito 8
											} // fin if vecinito 7
										} // fin if vecinito 6
									} // fin if vecinito 5
								} // fin if vecinito 4
							} // fin if vecinito 3
						} // fin if vecinito 2
					} // fin if vecinito 1

					if (esFrontera == TRUE) { // si NO SOY COLOR CAMPO y alguno de los vecinitos ES color campo...
						imageAbordered->image[pos*3]=255;
						imageAbordered->image[pos*3+1]=255;
						imageAbordered->image[pos*3+2]=255;

						myActualPointImage.x = i;
						myActualPointImage.y = j;
							
						pointProjection (myActualPointImage, myActualCamera, 0);
					} else {
						imageAbordered->image[pos*3]=255;
						imageAbordered->image[pos*3+1]=255;
						imageAbordered->image[pos*3+2]=255;
					}

					if (primerPunto == FALSE) {
						miPrimerPunto.x = LARGO_IMAGEN - 1;
						miPrimerPunto.y = 0;
						pointProjection (miPrimerPunto, myActualCamera, 1);
					} else if (segundoPunto == FALSE) {
						miSegundoPunto.x = LARGO_IMAGEN - 1;
						miSegundoPunto.y = ANCHO_IMAGEN - 1;
						pointProjection (miSegundoPunto, myActualCamera, 1);
					}
				} // fin if (soy negro)
			} // fin if myColorX != NULL
			i --;
		} // fin while (filas)
 	} // fin for (columnas)
}

void drawGroundImage () {
  int i, k, c, row, pos, columnPoint, rowPoint, posPoint;

  double H, S, V;
  float myR, myG, myB;

	int esFrontera;
	int v1, v2, v3, v4, v5, v6, v7, v8;

	/* Ground image */
	for (i=0, k=0; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS; i++, k++) {
		if (mycolorA!=NULL) {
			myActualCamera = mycolorA;

			//printf ("drawGroundImage1 %i\n",i);

			columnPoint = i%(SIFNTSC_COLUMNS);
			rowPoint = i/(SIFNTSC_COLUMNS);
			pos = rowPoint*SIFNTSC_COLUMNS+columnPoint;

			myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[pos*3+2];
			myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[pos*3+1];
			myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[pos*3+0];

			myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

			if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
				(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
				(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si soy negro

				esFrontera = TRUE; // lo damos por frontera en un principio, luego veremos

				// Calculo los demás vecinos, para ver de qué color son...
				c = (i-(SIFNTSC_COLUMNS))%(SIFNTSC_COLUMNS);
				row=(i-(SIFNTSC_COLUMNS))/(SIFNTSC_COLUMNS);
				v1=row*SIFNTSC_COLUMNS+c; // vecinito 1

				if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
					 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es válido ponemos sus valores a 0
					myR = 0.;
					myG = 0.;
					myB = 0.;
				} else {
					myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v1*3+2];
					myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v1*3+1];
					myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v1*3+0];
				}

				myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

				if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
					(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
					(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

					c = (i+(SIFNTSC_COLUMNS))%(SIFNTSC_COLUMNS);
					row=(i+(SIFNTSC_COLUMNS))/(SIFNTSC_COLUMNS);
					v2=row*SIFNTSC_COLUMNS+c; // vecinito 2

					if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
						 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es válido ponemos sus valores a 0
						myR = 0.;
						myG = 0.;
						myB = 0.;
					} else {
						myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v2*3+2];
						myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v2*3+1];
						myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v2*3+0];
					}

					myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

					if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
						(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
						(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

						c = (i-1)%(SIFNTSC_COLUMNS);
						row=(i-1)/(SIFNTSC_COLUMNS);
						v3=row*SIFNTSC_COLUMNS+c; // vecinito 3

						if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
							 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es válido ponemos sus valores a 0
							myR = 0.;
							myG = 0.;
							myB = 0.;
						} else {
							myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v3*3+2];
							myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v3*3+1];
							myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v3*3+0];
						}

						myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

						if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
							(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
							(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

							c = ((i-1)-(SIFNTSC_COLUMNS))%(SIFNTSC_COLUMNS);
							row=((i-1)-(SIFNTSC_COLUMNS))/(SIFNTSC_COLUMNS);
							v4=row*SIFNTSC_COLUMNS+c; // vecinito 4

							if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
								 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es válido ponemos sus valores a 0
								myR = 0.;
								myG = 0.;
								myB = 0.;
							} else {
								myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v4*3+2];
								myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v4*3+1];
								myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v4*3+0];
							}

							myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

							if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
								(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
								(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

								c = ((i-1)+(SIFNTSC_COLUMNS))%(SIFNTSC_COLUMNS);
								row=((i-1)+(SIFNTSC_COLUMNS))/(SIFNTSC_COLUMNS);
								v5=row*SIFNTSC_COLUMNS+c; // vecinito 5

								if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
									 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es válido ponemos sus valores a 0
									myR = 0.;
									myG = 0.;
									myB = 0.;
								} else {
									myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v5*3+2];
									myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v5*3+1];
									myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v5*3+0];
								}

								myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

								if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
									(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
									(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

									c = (i+1)%(SIFNTSC_COLUMNS);
									row=(i+1)/(SIFNTSC_COLUMNS);
									v6=row*SIFNTSC_COLUMNS+c; // vecinito 6

									if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
										 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es válido ponemos sus valores a 0
										myR = 0.;
										myG = 0.;
										myB = 0.;
									} else {
										myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v6*3+2];
										myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v6*3+1];
										myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v6*3+0];
									}

									myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

									if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
										(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
										(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

										c = ((i+1)-(SIFNTSC_COLUMNS))%(SIFNTSC_COLUMNS);
										row=((i+1)-(SIFNTSC_COLUMNS))/(SIFNTSC_COLUMNS);
										v7=row*SIFNTSC_COLUMNS+c; // vecinito 7

										if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
											 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es válido ponemos sus valores a 0
											myR = 0.;
											myG = 0.;
											myB = 0.;
										} else {
											myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v7*3+2];
											myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v7*3+1];
											myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v7*3+0];
										}

										myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

										if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
											(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
											(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

											c = ((i+1)+(SIFNTSC_COLUMNS))%(SIFNTSC_COLUMNS);
											row=((i+1)+(SIFNTSC_COLUMNS))/(SIFNTSC_COLUMNS);
											v8=row*SIFNTSC_COLUMNS+c; // vecinito 8

											if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
												 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es válido ponemos sus valores a 0
												myR = 0.;
												myG = 0.;
												myB = 0.;
											} else {
												myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v8*3+2];
												myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v8*3+1];
												myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v8*3+0];
											}

											myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

											if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
												(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
												(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, se acabó, no es pto. frontera
												esFrontera = FALSE;
											} // fin if vecinito 8
										} // fin if vecinito 7
									} // fin if vecinito 6
								} // fin if vecinito 5
							} // fin if vecinito 4
						} // fin if vecinito 3
					} // fin if vecinito 2
				} // fin if vecinito 1

				if (esFrontera == TRUE) { // si NO SOY COLOR CAMPO y alguno de los vecinitos ES color campo...
					imageAbordered->image[k*3]=255;
					imageAbordered->image[k*3+1]=255;
					imageAbordered->image[k*3+2]=255;

					myActualPointImage.x = rowPoint;
					myActualPointImage.y = columnPoint;
						
					pointProjection (myActualPointImage, myActualCamera, 0);
				} else { // si todos los vecinitos son color NO campo
					imageAbordered->image[k*3]=0;
					imageAbordered->image[k*3+1]=0;
					imageAbordered->image[k*3+2]=0;
				}
			} else { // fin if soy negro (else no soy negro)
				imageAbordered->image[k*3]=0;
				imageAbordered->image[k*3+1]=0;
				imageAbordered->image[k*3+2]=0;
			}

			//imageAbordered->image[k*4+3]=0;
		} // fin if myColorX != NULL
	} // fin FOR
}

void drawGroundPoints () {
	int i;

	glColor3f(0.0f, 0.0f, 1.0f);
	glBegin (GL_POINTS);
		for (i = 0; i < SIFNTSC_COLUMNS*SIFNTSC_ROWS*4; i ++) {
			glVertex3f (groundPoints [i].X, groundPoints [i].Y, groundPoints [i].Z);
		}
	glEnd ();
}

void drawFronteraPoints () {
	int i;

	glColor3f(0.0f, 0.0f, 1.0f);
	glBegin (GL_POINTS);
		for (i = 0; i < SIFNTSC_COLUMNS; i ++) {
			glVertex3f (fronteraPoints [i].X, fronteraPoints [i].Y, fronteraPoints [i].Z);
		}
	glEnd ();
}

static gboolean expose_event (GtkWidget *widget, GdkEventExpose *event, gpointer data) {
	GdkGLContext *glcontext;
	GdkGLDrawable *gldrawable;
	static pthread_mutex_t gl_mutex;

	printf ("expose0.9\n");

	pthread_mutex_lock(&gl_mutex);

	printf ("expose1\n");

	glcontext = gtk_widget_get_gl_context (widget);
	gldrawable = gtk_widget_get_gl_drawable (widget);

	printf ("expose2\n");

	if (!gdk_gl_drawable_gl_begin (gldrawable, glcontext)){
	  pthread_mutex_unlock(&gl_mutex);
	  return FALSE;
	}

	initFronteraCanvasSettings ();

	drawMyAxes ();
	//drawRoboticLabGround ();
	drawRoboticLabLines ();
	//drawRoboticLabGround ();
	drawHallLines ();
	glLoadIdentity ();
	drawCam (&roboticLabCam0, 2); // dibujamos la cámara virtual de visualización de imagen
	drawCam (&roboticLabCam1, 3); // dibujamos la cámara virtual de visualización de imagen
	drawCam (&roboticLabCam2, 4); // dibujamos la cámara virtual de visualización de imagen
	drawCam (&roboticLabCam3, 5); // dibujamos la cámara virtual de visualización de imagen

	drawFronteraImage ();	
	drawFronteraPoints (); // sólo vemos los puntos frontera
	//drawGroundImage ();
	//drawGroundPoints (); // vemos todos los puntos bordes

	if (gdk_gl_drawable_is_double_buffered (gldrawable)){
	  gdk_gl_drawable_swap_buffers (gldrawable);
	}
	else{
	  glFlush ();
	}

	gdk_gl_drawable_gl_end (gldrawable);

	pthread_mutex_unlock(&gl_mutex);
	return TRUE;
}

void frontera_guidisplay()
{
	printf ("guidisplay1\n");

	drawFilteredImage();
	//drawFilteredImage();
	prepare2draw(imageAfiltered, filteredImageRGB);
	prepare2draw(imageA, originalImageRGB);
	prepare2draw(imageAbordered, borderImageRGB);

	printf ("guidisplay1.1\n");

	gdk_threads_enter();
		if (fronteraCanvas!=NULL) expose_event(fronteraCanvas, NULL, NULL);
	gdk_threads_leave();

	printf ("guidisplay2\n");

	gdk_threads_enter();
		gtk_widget_queue_draw(GTK_WIDGET(win));
	gdk_threads_leave();

	printf ("guidisplay3\n");

}

void frontera_hide() {
  if (win!=NULL) {
      gdk_threads_enter();
      gtk_widget_hide(win);
      gdk_threads_leave();
	}
  mydelete_displaycallback(frontera_guidisplay);
	loadedgui=0;
	all[frontera_id].guistate=off;
}

void frontera_show()
{
	printf ("show1\n");

	static pthread_mutex_t frontera_gui_mutex;
	GtkButton *groundButton, *borderButton, *go1Button, *go50Button, *turn45RButton, *turn45LButton, *turn90RButton, *turn90LButton, *flashButton;
	GtkToggleButton *camera1Button, *camera2Button, *camera3Button, *camera4Button, *ceilCameraButton, *userCameraButton;
	GtkWidget *widget1;

	pthread_mutex_lock(&frontera_gui_mutex);
	if (!loadedgui){
	printf ("show2\n");

		loadglade ld_fn;
		loadedgui=1;
		pthread_mutex_unlock(&frontera_gui_mutex);

		/*Load the window from the .glade xml file*/
		gdk_threads_enter();  
		if ((ld_fn=(loadglade)myimport("graphics_gtk","load_glade"))==NULL){
		    fprintf (stderr,"I can't fetch 'load_glade' from 'graphics_gtk'.\n");
		    jdeshutdown(1);
		}

	printf ("show3\n");

		xml = ld_fn ("frontera.glade");
		if (xml==NULL){
		    fprintf(stderr, "Error loading graphical frontera on xml\n");
		    jdeshutdown(1);
		}

		// Set OpenGL Parameters
		GdkGLConfig *glconfig;
	printf ("show4\n");
		/* Try double-buffered visual */
		glconfig = gdk_gl_config_new_by_mode ((GdkGLConfigMode)(GDK_GL_MODE_RGB |	GDK_GL_MODE_DEPTH |	GDK_GL_MODE_DOUBLE));
		if (glconfig == NULL)  {
			g_print ("*** Cannot find the double-buffered visual.\n");
			g_print ("*** Trying single-buffered visual.\n");

			/* Try single-buffered visual */
			glconfig = gdk_gl_config_new_by_mode ((GdkGLConfigMode)(GDK_GL_MODE_RGB |	GDK_GL_MODE_DEPTH));
			if (glconfig == NULL) {
				g_print ("*** No appropriate OpenGL-capable visual found.\n");
				jdeshutdown(1);
			}
	printf ("show5\n");

		}

		fronteraCanvas = glade_xml_get_widget(xml, "fronteraCanvas"); // TODO: Hacer también para miniCanvas
		//gtk_widget_set_size_request (drawing_area, 400, 400);

		gtk_widget_unrealize(fronteraCanvas);
	printf ("show6\n");
		if (fronteraCanvas == NULL) printf ("Frontera Canvas is NULL\n");
		// Set OpenGL-capability to the widget
		if (gtk_widget_set_gl_capability (fronteraCanvas,	glconfig,	NULL,	TRUE,	GDK_GL_RGBA_TYPE)==FALSE) {
			printf ("No Gl capability\n");
			jdeshutdown(1);
		}
		gtk_widget_realize(fronteraCanvas);

		gtk_widget_set_child_visible (GTK_WIDGET(fronteraCanvas), TRUE);
		//gtk_widget_set_child_visible (GTK_WIDGET(glade_xml_get_widget(xml, "vbox1")), TRUE);

		gtk_widget_add_events ( fronteraCanvas,
		                        GDK_BUTTON1_MOTION_MASK    |
		                        GDK_BUTTON2_MOTION_MASK    |
		                        GDK_BUTTON3_MOTION_MASK    |
		                        GDK_BUTTON_PRESS_MASK      |
		                        GDK_BUTTON_RELEASE_MASK    |
		                        GDK_VISIBILITY_NOTIFY_MASK);	

		widget1=(GtkWidget *)glade_xml_get_widget(xml, "fronteraCanvas");
		g_signal_connect (G_OBJECT (widget1), "motion_notify_event", G_CALLBACK (opengl_camara_controller_motion), NULL);
		g_signal_connect (G_OBJECT (widget1), "scroll-event", G_CALLBACK (opengl_camara_controller_zoom), NULL);
/*
		miniCanvas = glade_xml_get_widget(xml, "miniCanvas"); // TODO: Hacer también para miniCanvas
		//gtk_widget_set_size_request (drawing_area, 400, 400);

		gtk_widget_unrealize(miniCanvas);
	printf ("show6\n");
		if (miniCanvas == NULL) printf ("Mini Canvas is NULL\n");
		// Set OpenGL-capability to the widget
		if (gtk_widget_set_gl_capability (miniCanvas,	glconfig,	NULL,	TRUE,	GDK_GL_RGBA_TYPE)==FALSE) {
			printf ("No Gl capability\n");
			jdeshutdown(1);
		}
		gtk_widget_realize(miniCanvas);

		gtk_widget_set_child_visible (GTK_WIDGET(miniCanvas), TRUE);
		//gtk_widget_set_child_visible (GTK_WIDGET(glade_xml_get_widget(xml, "vbox1")), TRUE);

		gtk_widget_add_events ( miniCanvas,
		                        GDK_BUTTON1_MOTION_MASK    |
		                        GDK_BUTTON2_MOTION_MASK    |
		                        GDK_BUTTON3_MOTION_MASK    |
		                        GDK_BUTTON_PRESS_MASK      |
		                        GDK_BUTTON_RELEASE_MASK    |
		                        GDK_VISIBILITY_NOTIFY_MASK);

		widget1=(GtkWidget *)glade_xml_get_widget(xml, "miniCanvas");
		g_signal_connect (G_OBJECT (widget1), "motion_notify_event", G_CALLBACK (opengl_camara_controller_motion2), NULL);
		g_signal_connect (G_OBJECT (widget1), "scroll-event", G_CALLBACK (opengl_camara_controller_zoom2), NULL);
*/
		// CONNECT CALLBACKS
		win = glade_xml_get_widget(xml, "window1");
		glade_xml_signal_autoconnect (xml); // Conectar los callbacks

		// OpenGl worlds control buttons:
		groundButton = (GtkButton *)glade_xml_get_widget(xml, "groundButton");
		g_signal_connect (G_OBJECT (groundButton), "pressed", G_CALLBACK (groundButton_pressed), NULL);
		g_signal_connect (G_OBJECT (groundButton), "released", G_CALLBACK (groundButton_released), NULL);

		borderButton = (GtkButton *)glade_xml_get_widget(xml, "borderButton");
		g_signal_connect (G_OBJECT (borderButton), "pressed", G_CALLBACK (borderButton_pressed), NULL);
		g_signal_connect (G_OBJECT (borderButton), "released", G_CALLBACK (borderButton_released), NULL);

		/*g_signal_connect (G_OBJECT (widget1), "button_press_event", G_CALLBACK (button_press_event), NULL);
		g_signal_connect (G_OBJECT (widget1), "motion_notify_event", G_CALLBACK (motion_notify_event), NULL);
		g_signal_connect (G_OBJECT (widget1), "scroll-event", G_CALLBACK (scroll_event), fronteraCanvas);*/

		// Movement control buttons:
		go1Button = (GtkButton *)glade_xml_get_widget(xml, "go1Button");
		g_signal_connect (G_OBJECT (go1Button), "pressed", G_CALLBACK (go1Button_pressed), NULL);
		g_signal_connect (G_OBJECT (go1Button), "released", G_CALLBACK (go1Button_released), NULL);

		go50Button = (GtkButton *)glade_xml_get_widget(xml, "go50Button");
		g_signal_connect (G_OBJECT (go50Button), "pressed", G_CALLBACK (go50Button_pressed), NULL);
		g_signal_connect (G_OBJECT (go50Button), "released", G_CALLBACK (go50Button_released), NULL);

		turn45RButton = (GtkButton *)glade_xml_get_widget(xml, "turn45RButton");
		g_signal_connect (G_OBJECT (turn45RButton), "pressed", G_CALLBACK (turn45RButton_pressed), NULL);
		g_signal_connect (G_OBJECT (turn45RButton), "released", G_CALLBACK (turn45RButton_released), NULL);

		turn45LButton = (GtkButton *)glade_xml_get_widget(xml, "turn45LButton");
		g_signal_connect (G_OBJECT (turn45LButton), "pressed", G_CALLBACK (turn45LButton_pressed), NULL);
		g_signal_connect (G_OBJECT (turn45LButton), "released", G_CALLBACK (turn45LButton_released), NULL);

		turn90RButton = (GtkButton *)glade_xml_get_widget(xml, "turn90RButton");
		g_signal_connect (G_OBJECT (turn90RButton), "pressed", G_CALLBACK (turn90RButton_pressed), NULL);
		g_signal_connect (G_OBJECT (turn90RButton), "released", G_CALLBACK (turn90RButton_released), NULL);

		turn90LButton = (GtkButton *)glade_xml_get_widget(xml, "turn90LButton");
		g_signal_connect (G_OBJECT (turn90LButton), "pressed", G_CALLBACK (turn90LButton_pressed), NULL);
		g_signal_connect (G_OBJECT (turn90LButton), "released", G_CALLBACK (turn90LButton_released), NULL);

		flashButton = (GtkButton *)glade_xml_get_widget(xml, "flashButton");
		g_signal_connect (G_OBJECT (flashButton), "pressed", G_CALLBACK (flashButton_pressed), NULL);
		g_signal_connect (G_OBJECT (flashButton), "released", G_CALLBACK (flashButton_released), NULL);

		camera1Button = (GtkToggleButton *)glade_xml_get_widget(xml, "camera1Button");
		g_signal_connect (G_OBJECT (camera1Button), "toggled", G_CALLBACK (camera1Button_pressed), NULL);
		g_signal_connect (G_OBJECT (camera1Button), "untoggled", G_CALLBACK (camera1Button_released), NULL);

		camera2Button = (GtkToggleButton *)glade_xml_get_widget(xml, "camera2Button");
		g_signal_connect (G_OBJECT (camera2Button), "pressed", G_CALLBACK (camera2Button_pressed), NULL);
		g_signal_connect (G_OBJECT (camera2Button), "released", G_CALLBACK (camera2Button_released), NULL);

		camera3Button = (GtkToggleButton *)glade_xml_get_widget(xml, "camera3Button");
		g_signal_connect (G_OBJECT (camera3Button), "pressed", G_CALLBACK (camera3Button_pressed), NULL);
		g_signal_connect (G_OBJECT (camera3Button), "released", G_CALLBACK (camera3Button_released), NULL);

		camera4Button = (GtkToggleButton *)glade_xml_get_widget(xml, "camera4Button");
		g_signal_connect (G_OBJECT (camera4Button), "pressed", G_CALLBACK (camera4Button_pressed), NULL);
		g_signal_connect (G_OBJECT (camera4Button), "released", G_CALLBACK (camera4Button_released), NULL);

		ceilCameraButton = (GtkToggleButton *)glade_xml_get_widget(xml, "ceilCameraButton");
		g_signal_connect (G_OBJECT (ceilCameraButton), "pressed", G_CALLBACK (ceilCameraButton_pressed), NULL);
		g_signal_connect (G_OBJECT (ceilCameraButton), "released", G_CALLBACK (ceilCameraButton_released), NULL);

		userCameraButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
		g_signal_connect (G_OBJECT (userCameraButton), "pressed", G_CALLBACK (userCameraButton_pressed), NULL);
		g_signal_connect (G_OBJECT (userCameraButton), "released", G_CALLBACK (userCameraButton_released), NULL);

	printf ("show8\n");

		if (win==NULL){
			fprintf(stderr, "Error loading graphic interface\n");
			jdeshutdown(1);
		} else {
			gtk_widget_show(win);
			gtk_widget_queue_draw(GTK_WIDGET(win));
		}
		gdk_threads_leave();
	} else {
		pthread_mutex_unlock(&frontera_gui_mutex);
		gdk_threads_enter();
		gtk_widget_show(win);
		gtk_widget_queue_draw(GTK_WIDGET(win));
		gdk_threads_leave();
	}

	gdk_threads_enter();
	// Initialization of the image buffers
	myregister_displaycallback(frontera_guidisplay);

	GdkPixbuf *originalImageBuf, *filteredImageBuf, *borderImageBuf;

	originalImage = GTK_IMAGE(glade_xml_get_widget(xml, "originalImage"));
	filteredImage = GTK_IMAGE(glade_xml_get_widget(xml, "filteredImage"));
	borderImage = GTK_IMAGE(glade_xml_get_widget(xml, "borderImage"));

	originalImageBuf = gdk_pixbuf_new_from_data(originalImageRGB->image,
					GDK_COLORSPACE_RGB,0,8,
					originalImageRGB->width,originalImageRGB->height,
					originalImageRGB->width*3,NULL,NULL);
	filteredImageBuf = gdk_pixbuf_new_from_data(filteredImageRGB->image,
					GDK_COLORSPACE_RGB,0,8,
					filteredImageRGB->width,filteredImageRGB->height,
					filteredImageRGB->width*3,NULL,NULL);
	borderImageBuf = gdk_pixbuf_new_from_data(borderImageRGB->image,
					GDK_COLORSPACE_RGB,0,8,
					borderImageRGB->width,borderImageRGB->height,
					borderImageRGB->width*3,NULL,NULL);

	gtk_image_set_from_pixbuf(originalImage, originalImageBuf);
	gtk_image_set_from_pixbuf(filteredImage, filteredImageBuf);
	gtk_image_set_from_pixbuf(borderImage, borderImageBuf);

	int a = 0;
	//glutInit(&a, NULL);

	printf ("show9\n");

	gdk_threads_leave();
/*   }
   myregister_displaycallback(frontera_guidisplay);
   all[frontera_id].guistate=on;*/
}
