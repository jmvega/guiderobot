Drstereo.camera1.fdist = (822.31442+838.45829)/4;
    parstereo.camera1.u0 = 240/2;
    parstereo.camera1.v0 = 320/2;
		parstereo.camera1.position.X=x1;
		parstereo.camera1.position.Y=y1;
		parstereo.camera1.position.Z=z1;
		parstereo.camera1.position.H=1.;

		/* Transformación del quaternion en la matriz R */
		parstereo.camera1.rt11= sqr(qw1) + sqr(qx1) - sqr(qy1) - sqr(qz1);
  	parstereo.camera1.rt12= 2 * (qx1*qy1 - qw1*qz1);
  	parstereo.camera1.rt13= 2 * (qw1*qy1 + qx1*qz1);
  	parstereo.camera1.rt21= 2 * (qx1*qy1 + qw1*qz1);
  	parstereo.camera1.rt22= sqr(qw1) - sqr(qx1) + sqr(qy1) - sqr(qz1);
  	parstereo.camera1.rt23= 2 * (qy1*qz1 - qw1*qx1);
  	parstereo.camera1.rt31= 2 * (qx1*qz1 - qw1*qy1);
  	parstereo.camera1.rt32= 2 * (qw1*qx1 + qy1*qz1);
  	parstereo.camera1.rt33= sqr(qw1) - sqr(qx1) - sqr(qy1) + sqr(qz1);

		/* Giro de la matriz R */
		r11 = 0*parstereo.camera1.rt11 + -1*parstereo.camera1.rt21 + 0*parstereo.camera1.rt31;
		r12 = 0*parstereo.camera1.rt12 + -1*parstereo.camera1.rt22 + 0*parstereo.camera1.rt32;
		r13 = 0*parstereo.camera1.rt13 + -1*parstereo.camera1.rt23 + 0*parstereo.camera1.rt33;
		r21 = 1*parstereo.camera1.rt11 + 0*parstereo.camera1.rt21 + 0*parstereo.camera1.rt31;
		r22 = 1*parstereo.camera1.rt12 + 0*parstereo.camera1.rt22 + 0*parstereo.camera1.rt32;
		r23 = 1*parstereo.camera1.rt13 + 0*parstereo.camera1.rt23 + 0*parstereo.camera1.rt33;
		r31 = 0*parstereo.camera1.rt11 + 0*parstereo.camera1.rt21 + 1*parstereo.camera1.rt31;
		r32 = 0*parstereo.camera1.rt12 + 0*parstereo.camera1.rt22 + 1*parstereo.camera1.rt32;
		r33 = 0*parstereo.camera1.rt13 + 0*parstereo.camera1.rt23 + 1*parstereo.camera1.rt33;
		parstereo.camera1.rt11 = r11;
		parstereo.camera1.rt12 = r12;
		parstereo.camera1.rt13 = r13;
		parstereo.camera1.rt21 = r21;
		parstereo.camera1.rt22 = r22;
		parstereo.camera1.rt23 = r23;
		parstereo.camera1.rt31 = r31;
		parstereo.camera1.rt32 = r32;
		parstereo.camera1.rt33 = r33;
		
		/* Añado la columna -RC */
  	parstereo.camera1.rt14=-x1*parstereo.camera1.rt11-y1*parstereo.camera1.rt12-z1*parstereo.camera1.rt13;
  	parstereo.camera1.rt24=-x1*parstereo.camera1.rt21-y1*parstereo.camera1.rt22-z1*parstereo.camera1.rt23;
  	parstereo.camera1.rt34=-x1*parstereo.camera1.rt31-y1*parstereo.camera1.rt32-z1*parstereo.camera1.rt33;

		/* Termino de rellenar la matriz */
  	parstereo.camera1.rt41=0.;
  	parstereo.camera1.rt42=0.;
  	parstereo.camera1.rt43=0.;
  	parstereo.camera1.rt44=1.;

		/* Matriz K */
		parstereo.camera1.k11=822.31442/2;  parstereo.camera1.k12=0.; parstereo.camera1.k13=parstereo.camera1.u0; parstereo.camera1.k14=0.;
  	parstereo.camera1.k21=0.; parstereo.camera1.k22=838.45829/2;  parstereo.camera1.k23=parstereo.camera1.v0; parstereo.camera1.k24=0.;
  	parstereo.camera1.k31=0.; parstereo.camera1.k32=0.; parstereo.camera1.k33=1.; parstereo.camera1.k34=0.;
