#include <GL/gl.h>              
#include <GL/glx.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <GL/freeglut_std.h>
#include <forms.h>
#include <glcanvas.h>

#include <glade/glade.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gtk/gtkgl.h>
#include <gtkextra/gtkextra.h>

#include <jde.h>
#include <pioneer.h>
#include <graphics_gtk.h>
#include <progeo.h>
#include <colorspaces.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_multifit.h>

#include <opencv/cv.h>

#define ROOM_MAX_X 7925.
#define v3f glVertex3f
#define PI 3.141592654

#define MAX_RADIUS_VALUE 10000000000
#define MIN_RADIUS_VALUE 0
#define WHEEL_DELTA 10

#define ANCHO_IMAGEN 320
#define LARGO_IMAGEN 240
#define MAXWORLD 20000.

extern void frontera_init();
extern void frontera_stop();
extern void frontera_run(int father, int *brothers, arbitration fn);
extern int frontera_cycle;

typedef struct SoRtype{
  struct SoRtype *father;
  float posx;
  float posy;
  float posz;
  float foax;
  float foay;
  float foaz;
  float roll;
} SofReference;


struct image_struct {
	int width;
	int height;
	int bpp;	// bytes per pixel
	char *image;
};

