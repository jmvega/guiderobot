#include <forms.h>
#include "gridslib.h"
#include "graphics_xforms.h"
#include "visual_locgui.h"
#include "pioneer.h"
#include "jde.h"
#include "colorspaces.h"
#include <GL/gl.h>              
#include <GL/glx.h>
#include <GL/glu.h>
#include <GL/freeglut_std.h>
#include <forms.h>
#include <glcanvas.h>
#include "progeo.h"
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_multifit.h>
#include <stdio.h>

#define OBS_COLUMNS 4
#define OBS_ROWS 3
#define PARTICULAS_EN_X 28
#define PARTICULAS_EN_Y 15
#define NUM_PARTICLES PARTICULAS_EN_X*PARTICULAS_EN_Y*36
#define RADIO_VISION 7000
#define NUM_MUESTRAS 47
#define ANGULO_VISION 47
#define COTA_MIN -1.0
#define COTA_MAX 8.0
#define SIGMA_RADIUS 30. /* mm? */
#define SIGMA_THETA 0.5 /* degrees? */
#define WHITE_WEIGHT 0.01
#define GREEN_WEIGHT 0.25

extern void visual_loc_startup();
extern void visual_loc_suspend();
extern void visual_loc_resume(int father, int *brothers, arbitration fn);
extern void filtro();

typedef struct
{
  int num_pixel_azul;
  int num_pixel_morado;
  int num_pixel_rojo;
  int num_pixel_verde;
  int color_dominante;
}Tresumen;
  

typedef struct
{
  int ancho;
  int alto;
  int dimension;
  float resolucion;
  float x_robot;
  float y_robot;
  float theta_robot;
}Tmundo;

typedef struct
{
  Tvoxel point;
  float theta;
  float probability;
}particles_type;

typedef struct
{
  Tvoxel point;
  float theta;
  float accumulation;
  float low;
}accumulation_type;

typedef struct
{
  char colour;
  int max,min;
  int white_pixels,other_pixels;
  float weight;
  float total_weight;
}object_type;

typedef struct {
  float X;
  float Y;
  unsigned char blue;
  unsigned char green;
  unsigned char red;
} TPixel;

typedef struct {
  unsigned char B;
  unsigned char G;
  unsigned char R;
} Tcolor;

extern int visual_loc_cycle; /* ms */
extern Tgrid *visual_loc_grid;
extern Tgrid *location_grid;
extern Tgrid **cubo_valido;
extern Tgrid **otro_cubo;

extern float robot_teorico[5];
extern int celda_vision;
extern float dist;
extern char convolution[OBS_COLUMNS];
extern Tvoxel puntos_destino[NUM_MUESTRAS];
extern Tvoxel puntito;
extern int observacion_nueva,robot_moved,robot_moved_last_observation;
extern float previous_robot[3];
extern int completed_particles, moving_particles;
extern particles_type particles[NUM_PARTICLES];
extern int alarm_degeneration;
extern float robot_corregido[5];

/*********************** PARTE DEL GUI ****************************/
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#define DISPLAY_ROBOT 0x01UL
#define DISPLAY_PARTICLES 0x02UL
#define DISPLAY_LASER 0x04UL
#define DISPLAY_ROBOT_CORREGIDO 0x08UL
#define DISPLAY_COLORIMAGEA 0x10UL
#define DISPLAY_FILTRADAA 0x20UL
#define DISPLAY_TEORICA 0x80UL
#define DISPLAY_REAL 0x40UL
#define DISPLAY_CONO 0x60UL

#define NUM_REJILLAS 16
#define GUINUM_PARTICULAS 6000
#define NUM_COLORS 4

typedef struct
{
  XPoint point;
  XPoint theta_draw_point;
}graphic_particles_type;

extern float mouse_x, mouse_y;
extern int mouse_new, view_cono, corte, show_grid_location,open_eyes,throw_part,filter_on;
extern int average,maxime,file_log;
extern float joystick_x, joystick_y;
extern float pt_joystick_x, pt_joystick_y;

extern float valores_discretos_theta[NUM_REJILLAS];
extern float Stol_min[NUM_COLORS], Htol_min[NUM_COLORS],Htol_max[NUM_COLORS],Stol_max[NUM_COLORS];
extern float Itol_min, Itol_max;
extern unsigned long display_state;
extern int guivisualoc_cycle;
extern int pinto_celdilla;
extern graphic_particles_type graphic_particles[GUINUM_PARTICULAS]; 

extern void guivisualoc_shutdown();
extern void guivisualoc_startup();
extern void guivisualoc_resume();
extern void guivisualoc_suspend();
