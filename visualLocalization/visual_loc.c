#include "visual_loc.h"

#define v3f glVertex3f
//CONSTANTES
#define MAX 1250
#define VACIO -2
#define ENCAPILLA -3
#define OZ -4
#define STAGE 1
#define SRIsim 2
#define RESOL_LOC 100
#define ALPHA 0.5
#define MAXRNDMTABLE 12000

// Factor de muestreo a la hora de considerar las im�genes tanto te�ricas como reales
#define FACTOR_MUESTREO 10

// N�mero de columnas y de filas de la imagen resumen
#define NUM_COLUMNAS 4
#define NUM_FILAS 3

HPoint2D myActualPoint2D; // variables para el c�lculo de los rayos virtuales
HPoint3D myActualPoint3D;
HPoint3D intersectionPoint;

#define altoVentana 1450.00 /* mm de longitud lado X */
#define anchoVentana 1930.00 /* mm de longitud lado Y */
float punto1x, punto1y, punto2x, punto2y, punto3x, punto3y, punto4x, punto4y, puntoNortex, puntoNortey; // puntos extremos del visor en techo (320x240)
float myTheta;
char texto[80]="";
#define numPuntosX 25 /* 25 puntos en ventanaGPPX para dibujar gr�ficamente */
#define numPuntosY 25 /* 25 puntos en ventanaGPPY para dibujar gr�ficamente */

#define PI 3.141592654

#define MOUSELEFT 1
#define PUSHED 1
#define RELEASED 0

#define MAX_RADIUS_VALUE 10000000
#define MIN_RADIUS_VALUE 0
#define WHEEL_DELTA 1000

#define ANCHO_IMAGEN 320
#define LARGO_IMAGEN 240

gsl_matrix *K_1,*R_1;
gsl_vector* x_1;
HPoint2D progtest2D;
HPoint3D progtest3D;

struct HSV* myHSV;
struct HSV* myHSV2;
TPinHoleCamera virtualcam0; /* para ver el minimundillo */
TPinHoleCamera robotCamera;
TPinHoleCamera *myCamera; // puntero temporal

int flashImage = TRUE;
int clickOnDraw = FALSE;
int writtenFile = FALSE;
int manualMode = FALSE;

HPoint2D myActualPoint2D; // variables para el c�lculo de los rayos virtuales
HPoint3D myActualPoint3D;
HPoint2D myActualPointImage;
HPoint3D cameraPos3D;
HPoint3D myEndPoint3D;
HPoint3D intersectionPoint;

float distanciaPlanoImagen, incremento;
int inicializadoArray = FALSE;
float anchoCelda = (1./SIFNTSC_ROWS)*6000; // 25
float altoCelda = (1./SIFNTSC_COLUMNS)*6000; // 18.75
int zonasActuales[NUM_COLUMNAS*NUM_FILAS];

HPoint2D mouse_on_canvas;
HPoint2D myPixel;
int foa_mode, cam_mode;
float radius = 500;
float t = 0.5; // lambda

/* Gui callbacks */
registerbuttons myregister_buttonscallback;
registerdisplay myregister_displaycallback;
deletebuttons mydelete_buttonscallback;
deletedisplay mydelete_displaycallback;

char *fich_imagen;
Tgrid *visual_loc_grid=NULL; 
Tgrid *location_grid=NULL;

char **mycolorA;
resumeFn colorAresume;
suspendFn colorAsuspend;

char **mycolorB;
resumeFn colorBresume;
suspendFn colorBsuspend;

int visual_loc_id=0; 
int visual_loc_brothers[MAX_SCHEMAS];
arbitration visual_loc_callforarbitration;

int *mylaser = NULL;
resumeFn laserresume;
suspendFn lasersuspend;

float *myencoders = NULL;
resumeFn encodersresume;
suspendFn encoderssuspend;

float *myv = NULL;
float *myw = NULL;
resumeFn motorsresume;
suspendFn motorssuspend;

float robot_teorico[5];
int celda_vision;
Tgrid **cubo_valido;
Tgrid *cubo_probabilidad_1[NUM_REJILLAS];
Tgrid *cubo_probabilidad_2[NUM_REJILLAS];
Tgrid **otro_cubo;

Tmundo mi_mundo;
float origenpoint_x, origenpoint_y;
float destinopoint_x, destinopoint_y;  
float q_llevo;
float theta_robot;
float x_rob;
float y_rob;
Tvoxel point;
float previous_robot[3];
static float incorporated_odometry[3];
Tvoxel puntito;
Tvoxel puntos_destino[NUM_MUESTRAS];
static float resolucion;

int visual_loc_brothers[MAX_SCHEMAS];
arbitration visual_loc_callforarbitration;
int visual_loc_cycle=200; /* ms */

enum visual_loc_states {init,t1,r1,t2,r2,t3,r3,t4,end};

char convolution[OBS_COLUMNS];
char obs_real_filtrada[SIFNTSC_COLUMNS*SIFNTSC_ROWS*4];
char obs_teorica_filtrada[SIFNTSC_COLUMNS*SIFNTSC_ROWS*4];
char previous_filtered[OBS_COLUMNS*3];
int obs_teorica[NUM_MUESTRAS];
int incorporated=0;
Tresumen colores_encontrados[NUM_COLUMNAS*NUM_FILAS], colores_encontrados_teorica[NUM_COLUMNAS*NUM_FILAS]; // zonas de resumen (4 x 3)
float dist,desplazamiento_x=0.,desplazamiento_y=0.,desplazamiento_theta=0.,grados_girados=0.;
int new_observation,robot_moved,robot_moved_last_observation;
float p_acumulada, p_observada, ratio_acumulado, ratio_observado,ratio;
float pseudorandomize_table[MAXRNDMTABLE];
int rr=0; /* index for the pseudorandomize table */

particles_type particles[NUM_PARTICLES];
accumulation_type accumulation[NUM_PARTICLES];
static float min_num_part, max_num_part_x, max_num_part_y;
static int visual_loc_state;
static int incremento_celdilla_x, incremento_celdilla_y, incremento_theta;
int completed_particles=0, moving_particles=0, alarm_degeneration;
float sigma_r, sigma_t;
float robot_corregido[5];

float Hmin[NUM_COLORS], Hmax[NUM_COLORS], Vmin[NUM_COLORS], Vmax[NUM_COLORS];

float HminRojo   = 6.08;
float HminVerde  = 1.89;
float HminCyan   = 3.15;
float HminMorado = 4.88;

float HmaxRojo   = 0.28;
float HmaxVerde  = 2.55;
float HmaxCyan   = 3.57;
float HmaxMorado = 5.30;

float SminRojo   = 0.30;
float SminVerde  = 0.30;
float SminCyan   = 0.50;
float SminMorado = 0.15;

float SmaxRojo   = 0.80;
float SmaxVerde  = 0.60;
float SmaxCyan   = 0.80;
float SmaxMorado = 0.63;

int unavez=1;
//int umbralColorZona = 150; // de 6400 p�xeles que hay en cada "zona"
int umbralColorZona = 2; // con la "miniimagen" reducimos /10
/*****************************************/
int visual_locgui_cycle=100; /* ms */
#define FORCED_REFRESH 5000 /* ms */ 
/*Every forced_refresh the display is drawn from scratch. If it is too small it will cause flickering with grid display. No merece la pena una hebra de "display_lento" solo para repintar completamente la pantalla. */

#define joystick_maxRotVel 30 /* deg/sec */
#define joystick_maxTranVel 200 /* mm/sec */

float joystick_x, joystick_y;
float pt_joystick_x, pt_joystick_y;
float mouse_x, mouse_y;
int mouse_new=0;
int back=0;

#define PUSHED 1
#define RELEASED 0

int pantiltencoders_cb=0;
int laser_cb=0;
int sonars_cb=0;
int encoders_cb=0;
int imageA_cb=0;
int imageB_cb=0;
int teorica_cb=0;
int real_cb=0;

int show,view_cono;

/* Necesarias para las Xlib */
Display* display;
GC visual_locgui_gc;
int* screen;
Window visual_locgui_win; /* image window */
static int vmode;
static XImage *imagenA,*imagenB,*imagen_teorica,*imagen_real,*imagenRealResumida,*imagenTeoricaResumida;
static char *imagenA_buf, *imagenB_buf, *imagen_teorica_buf, *imagen_real_buf; /* puntero a memoria para la imagen a visualizar en el servidor X. No compartida con el servidor */
long int tabla[256]; 
/* tabla con la traduccion de niveles de gris a numero de pixel en Pseudocolor-8bpp. Cada numero de pixel apunta al valor adecuado del ColorMap, con el color adecuado instalado */
int pixel8bpp_rojo, pixel8bpp_blanco, pixel8bpp_amarillo;

FD_visual_locgui *fd_visual_locgui;
GC      visual_locgui_gc;
Window  visual_loc_canvas_win; /* canvas window */
float   escala, width, height, grados_dial, xpos_click, ypos_click, thetapos_click;

float odometrico[5];

unsigned long display_state;
int visual_refresh=TRUE;
int iteracion_display=0;

#define EGOMAX NUM_SONARS+5
XPoint ego[EGOMAX];
XPoint ego_corregido[EGOMAX];

float last_heading; /* ultima orientacion visualizada del robot */
int numego=0;
int visual_delete_ego=FALSE;
int visual_delete_ego_corregido=FALSE;

XPoint laser_dpy[NUM_LASER];
int visual_delete_laser=FALSE;

XPoint us_dpy[NUM_SONARS*2];
int visual_delete_us=FALSE;

int visual_delete_particles=FALSE;

XRectangle *visual_loc_Graf;
int visual_loc_grid_setup=FALSE;
int visual_delete_visual_loc_=FALSE;
int visual_refresh_visual_loc_=FALSE;
XRectangle celdillaGraf, old_cells[GUINUM_PARTICULAS];
int canvas_click,show_grid_location=0,show_grid=0,corte=0,open_eyes,throw_part,filter_on;
int average,maxime,file_log;
float theta_x, theta_y, index_dial;

XPoint prueba,pintapuntito;

#define RANGO_MAX 50000. /* en mm */
#define RANGO_MIN 500. /* en mm */ 
#define RANGO_INICIAL 50000. /* en mm */
float rango=RANGO_INICIAL; /* Rango de visualizacion en milimetros */

char fpstext[80]="";

char obs_real_plana[SIFNTSC_COLUMNS*SIFNTSC_ROWS*4];
char obs_teorica_plana[SIFNTSC_COLUMNS*SIFNTSC_ROWS*4];

char imagenRealResumida_buf[((SIFNTSC_COLUMNS*SIFNTSC_ROWS)/16)*4];
char imagenTeoricaResumida_buf[((SIFNTSC_COLUMNS*SIFNTSC_ROWS)/16)*4];
int suelo=33,puerta=34,papelera=35,extintor=36, mi_gris=37, mi_rojo=38, rojoTecho = 31, colorRaro = 42, verde=39, pinto_celdilla=1, techo1 = 41, techo2 = 42, techo3 = 43, techo4 = 44, techo5 = 45, techo6 = 46, techo7 = 47, techo8 = 48, techo9 = 49;
float valores_discretos_theta[NUM_REJILLAS]={0.,22.5,45.,67.5,90.,112.5,135.,157.5,180.,202.5,225.,247.5,270.,292.5,315.,337.5},ilum_range_min=100.,ilum_range_max=10.;
float Htol_min[NUM_COLORS],Stol_min[NUM_COLORS],Htol_max[NUM_COLORS],Stol_max[NUM_COLORS];
float Itol_min, Itol_max;
graphic_particles_type graphic_particles[GUINUM_PARTICULAS]; 
XRectangle *celdilla_part;

float randomNumber (float min, float max)
{
  return (float) ((max - min) * rand () / RAND_MAX) + min;
}

float gauss_rndm(float std, float mean)
{
  double x;
  double pi, r1, r2;
  
  pi = 4 * atan (1);
  r1 = -log (1 - randomNumber(0,1));
  r2 = 2 * pi * randomNumber(0,1);
  r1 = sqrt (2 * r1);
  x = r1 * cos (r2);
  return std * x + mean;
}

//Pseudo-random
void rndm()
{
  int i;
  for (i=0;i<MAXRNDMTABLE;i++)
    pseudorandomize_table[i]=((float)rand())/RAND_MAX;
}

int min (int r, int g, int b) {

  if ((r<=g) && (r<=b))
    return r;
  else if ((g<=r)&&(g<=b))
    return g;
  else if ((b<=r)&&(b<=g))
    return b;
}

int xy2celda(int x, int y, int dim)
{
  int celda;
  celda=(y*dim)+x;
  return celda;

}

int xy2celda_vision(Tvoxel point,Tgrid *grid)
{
  int celda,deltax,deltay;

  deltax=(int)((point.x-grid->lo.x-grid->resolucion/2.0)/grid->resolucion);
  deltay=(int)((point.y-grid->lo.y-grid->resolucion/2.0)/grid->resolucion);
  celda=deltay*grid->size+deltax;
  
  return celda;

}

void us2xy_corregido(int numsensor, float d,float phi, Tvoxel *point) 

/*  Calcula la posicion respecto de sistema de referencia inicial (sistema odometrico) del punto detectado en el sistema de coordenadas solidario al sensor. OJO depende de estructura posiciones y de por el sensor, sabiendo que:
   a) el robot se encuentra en robot[0], robot[1] con orientacion robot[2] respecto al sistema de referencia externo,
   b) que el sensor se encuentra en xsen, ysen con orientacion asen respecto del sistema centrado en el robot apuntando hacia su frente, 
   c) el punto esta a distancia d del sensor en el angulo phi 
*/ 
{
  float  Xp_sensor, Yp_sensor, Xp_robot, Yp_robot;

  Xp_sensor = d*cos(DEGTORAD*phi);
  Yp_sensor = d*sin(DEGTORAD*phi);
  /* Coordenadas del punto detectado por el US con respecto al sistema del sensor, eje x+ normal al sensor */
  Xp_robot = us_coord[numsensor][0] + Xp_sensor*us_coord[numsensor][3] - Yp_sensor*us_coord[numsensor][4];
  Yp_robot = us_coord[numsensor][1] + Yp_sensor*us_coord[numsensor][3] + Xp_sensor*us_coord[numsensor][4];
  /* Coordenadas del punto detectado por el US con respecto al robot */
  (*point).x = Xp_robot*robot_corregido[3] - Yp_robot*robot_corregido[4] + robot_corregido[0];
  (*point).y = Yp_robot*robot_corregido[3] + Xp_robot*robot_corregido[4] + robot_corregido[1];
  /* Coordenadas del punto con respecto al origen del SdeR */
}

//This function creates a configuration file
void create_configfile(char *fich_sal, int dimension, float resolucion)
{
  FILE *salida;

  salida = fopen(fich_sal,"w");
  if (salida == NULL) {
		fprintf(stderr,"Parser: I can't found file %s\n", fich_sal); exit(-1);
	}

	printf("Building config file %s, %d\n", fich_sal, dimension);
  
  fprintf(salida,"dimension = %d\n",dimension);  
  fprintf(salida,"resolucion = %f\n",resolucion);
  fprintf(salida,"tipo = ec_diferencial\n");
  fprintf(salida,"ec_diferencial_speed = 1\n");
  fprintf(salida,"paso_tiempo = nulo\n");
  fprintf(salida,"cell_angles = 0\n");
  fprintf(salida,"mayoria_saturacion = 30\n");
  fprintf(salida,"mayoria_ruido = 10\n");
  fprintf(salida,"long_historia = 0\n");
  fprintf(salida,"\n");
  fprintf(salida,"sonar_filtra = independientes\n");
  fprintf(salida,"sonar_geometria = cono_denso\n");
  fprintf(salida,"sonar_apertura = 20.\n");
  fprintf(salida,"sonar_noobstacle = 3000.\n");
  fprintf(salida,"sonar_radialerror = 10.\n");
  fprintf(salida,"sonar_fdistancia = lineal\n");
  fprintf(salida,"sonar_residuo = 0.05\n");
  fprintf(salida,"sonar_o = 0.4\n");
  fprintf(salida,"sonar_e = -0.5\n");
  fprintf(salida,"sonar_mind = 700\n");
  fprintf(salida,"sonar_maxd = 1100.\n");
  fprintf(salida,"\n");
  fprintf(salida,"robot_geometria = cilindro\n");
  fprintf(salida,"robot_radio = 248.\n");
  fprintf(salida,"robot_e = -0.8\n");
  fprintf(salida,"\n");
  fprintf(salida,"laser_geometria = cono_denso\n");
  fprintf(salida,"laser_apertura = 0.5\n");
  fprintf(salida,"laser_muestras = 90\n");
  fprintf(salida,"laser_noobstacle = 8000.\n");
  fprintf(salida,"laser_o = 1\n");
  fprintf(salida,"laser_e = -0.7\n"); 
  
  fclose(salida);
}
  
//Build the occupation grid
void relleno_grid(Tmundo mundo, Tgrid *grid) {
  FILE *imagen;
  FILE *balizas;
  int leidos, i, k, linea, reading;
  char *casillas;
  char *mapa_balizas="./mapa_balizas";
  char my_byte;
  int x_world, y_world,celda;//,fil,col;
    
  imagen = fopen(fich_imagen,"r");

	if (imagen == NULL) {
		fprintf(stderr,"Parser: I can't found file %s\n", fich_imagen); exit(-1);
	}

	printf("Image file %s opened. Building image buffer...\n", fich_imagen);
    //buff_imagen=(char*)malloc((mundo.alto*mundo.ancho)*sizeof(char));
    casillas=(char*)malloc((mundo.alto*mundo.ancho+1)*sizeof (char));
        
    i=0;
    reading=1;
    linea=1;
    while (reading)
      {
	leidos=fread(&my_byte,1,1,imagen);
	reading=(!feof(imagen));
	if ((my_byte=='\n')&&(linea<=4)) linea++;
	if ((linea>4)&&(my_byte!='\n')){
	  casillas[i]=my_byte;
	  i++;
	}
      }
    fclose (imagen);
    
    printf("Image file %s closed.\n",fich_imagen);
        
    printf("Building occupancy grid according to image buffer...\n");
    
    balizas=fopen(mapa_balizas,"w");
    
    if (balizas == NULL) 
      {fprintf(stderr,"Parser: I can't found file %s\n",mapa_balizas); exit(-1);}

    
    for (k=0;k<grid->size*grid->size;k++)
      {
	
	y_world=((k/(*grid).size)*(*grid).resolucion)/mundo.resolucion;
	x_world=((k%(*grid).size)*(*grid).resolucion)/mundo.resolucion;

	if ((x_world<=mundo.ancho)&&(y_world<=mundo.alto))  
	  { 	   
	    celda=xy2celda(x_world,mundo.alto-y_world,mundo.ancho);

			//printf ("%i\n", casillas[celda]);
	    if (celda<mundo.alto*mundo.ancho){
	      if ((int) casillas[celda]==-90)  
		(*grid).map[k].estado=-1; 
	      else
		if ((int) casillas[celda]==110)
		  {
		    (*grid).map[k].estado=-2;
		    fprintf(balizas,"%.2f,%.2f,O\n",(*grid).map[k].centro.x,(*grid).map[k].centro.y);
		  }
		else
		  if ((int) casillas[celda]==89) // 88
		    {
		      (*grid).map[k].estado=-3;
		      fprintf(balizas,"%.2f,%.2f,R\n",(*grid).map[k].centro.x,(*grid).map[k].centro.y);
		    }
		  else
		    if ((int) casillas[celda]==60)
		      {
			(*grid).map[k].estado=-4;
			fprintf(balizas,"%.2f,%.2f,A\n",(*grid).map[k].centro.x,(*grid).map[k].centro.y);
		      }
		    else
		      if ((int) casillas[celda]==120)
			{
			(*grid).map[k].estado=-5;
			fprintf(balizas,"%.2f,%.2f,V\n",(*grid).map[k].centro.x,(*grid).map[k].centro.y);
		      }
		    else
		      if ((int) casillas[celda]==54) // ROJO
			{
			(*grid).map[k].estado=-6;
			fprintf(balizas,"%.2f,%.2f,V\n",(*grid).map[k].centro.x,(*grid).map[k].centro.y);
		      }
		    else
		      if ((int) casillas[celda]==46) // MORADO
			{
			(*grid).map[k].estado=-7;
			fprintf(balizas,"%.2f,%.2f,V\n",(*grid).map[k].centro.x,(*grid).map[k].centro.y);
		      }
		    else
		      if ((int) casillas[celda]==-74) // VERDE
			{
			(*grid).map[k].estado=-8;
			fprintf(balizas,"%.2f,%.2f,V\n",(*grid).map[k].centro.x,(*grid).map[k].centro.y);
		      }
		    else
		      if ((int) casillas[celda]==-55) // CYAN
			{
			(*grid).map[k].estado=-9;
			fprintf(balizas,"%.2f,%.2f,V\n",(*grid).map[k].centro.x,(*grid).map[k].centro.y);
		      }
		    else
		      if ((int) casillas[celda]==-2)
			{
			(*grid).map[k].estado=-10;
			fprintf(balizas,"%.2f,%.2f,V\n",(*grid).map[k].centro.x,(*grid).map[k].centro.y);
		      }
		    else
		      if ((int) casillas[celda]==69)
			{
			(*grid).map[k].estado=-11;
			fprintf(balizas,"%.2f,%.2f,V\n",(*grid).map[k].centro.x,(*grid).map[k].centro.y);
		      }
		    else
		      if ((int) casillas[celda]==-8)
			{
			(*grid).map[k].estado=-12;
			fprintf(balizas,"%.2f,%.2f,V\n",(*grid).map[k].centro.x,(*grid).map[k].centro.y);
		      }
		    else
		      if ((int) casillas[celda]==248)
			{
			(*grid).map[k].estado=-13;
			fprintf(balizas,"%.2f,%.2f,V\n",(*grid).map[k].centro.x,(*grid).map[k].centro.y);
		      }
		    else
		      if ((int) casillas[celda]==209)
			{
			(*grid).map[k].estado=-14;
			fprintf(balizas,"%.2f,%.2f,V\n",(*grid).map[k].centro.x,(*grid).map[k].centro.y);
		      }
		    else
		      if ((int) casillas[celda]==54)
			{
			(*grid).map[k].estado=-15;
			fprintf(balizas,"%.2f,%.2f,V\n",(*grid).map[k].centro.x,(*grid).map[k].centro.y);
		      }
			
		    else
					(*grid).map[k].estado=0;
	    } 
	  }
      }
  fclose(balizas);
}

//Reads the configuration of the world 
void capturarMundo () {
	FILE *world;
	FILE *imagen;
	char *temp;
	char *path="../worlds/";
	char *fich_mundo="../worlds/stageDep2.world";
	int i,j,k,leidos,linea,cont;
	char buf[MAX], aux[MAX], val[MAX], x0[MAX], y0[MAX], x1[MAX], y1[MAX];
	char buf_image[2000*2000];

	world= fopen(fich_mundo,"r");
	if (fich_mundo == NULL) {
		fprintf(stderr,"Parser: I can't found file %s\n", fich_mundo); exit(-1);
	}

	printf("Reading world file %s\n", fich_mundo);

	leidos=1;
	j=0;
	while (leidos!=0)	{
		i=0; 
		buf[0]=' ';
		while ((leidos==1)&&(buf[i]!='\n'))
			leidos=fread(&buf[++i],1,1,world);

		linea=i-1;
		i=0;
		while (buf[i]==' ') i++;

		if (!((buf[i]=='\n')||(i>=linea))) {
			sscanf(&buf[i],"%s %s %s %s %s %s",aux,val,x0,y0,x1,y1);

			if (strcmp(aux,"bitmap")==0) { // hemos encontrado la l�nea de especificaci�n del fichero 
				cont=0;
				temp=(char*)malloc((strlen(val)-2)*sizeof(char));
				temp[0] = "\0";
				fich_imagen=(char*)malloc((strlen(temp)+strlen(path))*sizeof(char));
				fich_imagen[0] = "\0";

				for (k=1;k<strlen(val)-1;k++)
				temp[cont++]=val[k];

				sprintf(fich_imagen,"%s%s",path,temp); // obtenemos la ruta completa del fichero imagen a abrir
				printf("Image file to use: %s\n",fich_imagen);
			}

			if (strcmp(aux,"resolution")==0) { // l�nea de especificaci�n de la resoluci�n
				mi_mundo.resolucion=atof(val)*1000;
				printf("Stage resolution: %f\n",mi_mundo.resolucion);
			} else if (strcmp(aux,"pose")==0) { // l�nea de posici�n del robot en nuestro mundo stage
				mi_mundo.x_robot=6000;//atof(val)*1000;
				mi_mundo.y_robot=-10000;//atof(x0)*1000;
				mi_mundo.theta_robot=-90;//atof(y0);
				printf("Robot pose: [%f, %f] Theta: %f\n", mi_mundo.x_robot, mi_mundo.y_robot, mi_mundo.theta_robot);
			}
		} // fin if (!((buf[i]=='\n')||(i>=linea)))
	} // fin while (leidos!=0)

	fclose(world);
	printf("World file %s closed.\n", fich_mundo);

	imagen = fopen (fich_imagen,"r"); // fichero imagen

	if (imagen == NULL) {
		fprintf(stderr,"Parser: I can't found file %s\n", fich_imagen); exit(-1);
	}

	printf("Reading image file %s\n", fich_imagen);

	cont=0;
	leidos=1;
	j=0;
	while (leidos!=0)	{
		i=0; 
		buf_image[0]=' ';
		while ((leidos==1)&&(buf_image[i]!='\n'))
			leidos=fread(&buf_image[++i],1,1,imagen);

		linea=i-1;
		i=0;
		while (buf_image[i]==' ') i++;

		if (!((buf_image[i]=='\n')||(i>=linea))) {
			if (cont<3) {
				sscanf(&buf_image[i],"%s %s",aux,val);	
				cont++;
				if (cont==3) {
					mi_mundo.alto=(int)atoi(val);
					mi_mundo.ancho=(int)atoi(aux);
					printf("Image has %d rows & %d columns, using resolution %f\n",mi_mundo.alto, mi_mundo.ancho, mi_mundo.resolucion);
				}
			}
		} // fin if (!((buf_image[i]=='\n')||(i>=linea)))
	} // fin while (leidos!=0)
	fclose(imagen);
	printf("File %s closed.\n",fich_imagen);

	if (mi_mundo.alto>=mi_mundo.ancho)
		mi_mundo.dimension=(int)(mi_mundo.alto*mi_mundo.resolucion);
	else
		mi_mundo.dimension=(int)(mi_mundo.ancho*mi_mundo.resolucion);
}

//Calculating destination points
void calculo_destinos(float x,float  y, int theta, float inc)
{
  int ang=-(ANGULO_VISION/2);
  int i;
  for (i=0;i<ANGULO_VISION;i++)
    {
      puntos_destino[i].x=x+(RADIO_VISION)*cos((theta+ang)*DEGTORAD);
      puntos_destino[i].y=y+(RADIO_VISION)*sin((theta+ang)*DEGTORAD);
      ang=ang+inc;
    }
}

void obs_teorica_refresh(float click_x, float click_y, int theta)
{
  /*Ecuacion de la recta P=A+lambda(B-A)
    iremos trazando rectas para ir recorriendo punto a punto toda la zona del campo de vision
    y buscando los colores relevantes. El incremento de lambda ser� (0,1/RADIO)*/
  
  int i,no_encontrado;
  float relativo_x, relativo_y;//click_x, click_y;
  float lambda;
    
  calculo_destinos(click_x,click_y,theta,1);
  
	for (i=0;i<ANGULO_VISION;i++) {
		no_encontrado=1;
		lambda=0.02;
		while ((lambda<=1.)&&(no_encontrado)) {
			relativo_x=click_x+lambda*(puntos_destino[i].x-click_x);
			relativo_y=click_y+lambda*(puntos_destino[i].y-click_y);
			puntito.x=relativo_x;
			puntito.y=relativo_y;
			lambda=lambda+0.02;
			celda_vision=xy2celda_vision(puntito,visual_loc_grid);
			no_encontrado=(((int)visual_loc_grid->map[celda_vision].estado==0));
		}

		/*Almacenamos por cada grado de vision la informacion visual*/
		obs_teorica[i]=((int)visual_loc_grid->map[celda_vision].estado);
	}
}

void create_obs_teorica()
{
  int i,j;
  /*Pintamos desde la ultima lectura para que quede coherente*/
  i=ANGULO_VISION;
  for(j=0;j<OBS_COLUMNS;j++)
    {
      /*Como las muestras recogidas son mucho menores que la anchura de la imagen lo que hacemos
	es clonar tantas veces nos indice OBS_COLUMNS/NUM_MUESTRAS cada muestra recogida*/
      if (j%(OBS_COLUMNS/ANGULO_VISION+2)==0) i--;

      /*Dependiendo de los colores que tengamos en la rejilla asi pintamos en la imagen*/
      switch (obs_teorica[i]){
      case (-1):{	  
	obs_teorica_plana[j*3]=(unsigned char)255;
	obs_teorica_plana[j*3+1]=(unsigned char)255;
	obs_teorica_plana[j*3+2]=(unsigned char)255;
	break;}
      case (-2):{
	obs_teorica_plana[j*3]=(unsigned char)0;
	obs_teorica_plana[j*3+1]=(unsigned char)51;
	obs_teorica_plana[j*3+2]=(unsigned char)153;
	break;}
      case (-3):{
	obs_teorica_plana[j*3]=(unsigned char)0;
	obs_teorica_plana[j*3+1]=(unsigned char)0;
	obs_teorica_plana[j*3+2]=(unsigned char)255;
	break;}
      case (-4):{
	obs_teorica_plana[j*3]=(unsigned char)255;
	obs_teorica_plana[j*3+1]=(unsigned char)0;
	obs_teorica_plana[j*3+2]=(unsigned char)0;
	break;}
      case (-5):{
	obs_teorica_plana[j*3]=(unsigned char)0;
	obs_teorica_plana[j*3+1]=(unsigned char)255;
	obs_teorica_plana[j*3+2]=(unsigned char)0;
	break;}
      default:{
	obs_teorica_plana[j*3]=(unsigned char)255;
	obs_teorica_plana[j*3+1]=(unsigned char)255;
	obs_teorica_plana[j*3+2]=(unsigned char)255;
	break;}
      }
      
    }
}

void crearImagenResumenTeorica()
{
	int i,j;
	int zona = 0;
	int c, row;
	int miZona = 0;
	int pos;

	for (i=0;i<SIFNTSC_COLUMNS/NUM_COLUMNAS;i++)	{
		if ((i!=0) && ((i%(SIFNTSC_COLUMNS/(NUM_COLUMNAS*NUM_COLUMNAS))) == 0)) { // salto en esa fila!
			zona ++; // zona siguiente
			miZona = zona;
		} else {
			zona = miZona;
		}

		for(j=0;j<SIFNTSC_ROWS/NUM_COLUMNAS;j++)	{
			if ((j!=0) && ((j%(SIFNTSC_ROWS/(NUM_COLUMNAS*NUM_FILAS))) == 0)) { // salto en esa columna!
				zona ++; // zona siguiente
			}
	
			pos = (j*(SIFNTSC_COLUMNS/NUM_COLUMNAS)+i)*NUM_COLUMNAS;

			switch (colores_encontrados_teorica[zona].color_dominante){
				case 0:{ // domina el rojo
					obs_teorica_plana[pos+0]=(unsigned char)0;
					obs_teorica_plana[pos+1]=(unsigned char)0;
					obs_teorica_plana[pos+2]=(unsigned char)255;
					break;
				}
				case 1:{ // domina el verde
					obs_teorica_plana[pos+0]=(unsigned char)0;
					obs_teorica_plana[pos+1]=(unsigned char)255;
					obs_teorica_plana[pos+2]=(unsigned char)0;
					break;
				}
				case 2:{ // domina el azul
					obs_teorica_plana[pos+0]=(unsigned char)255;
					obs_teorica_plana[pos+1]=(unsigned char)255;
					obs_teorica_plana[pos+2]=(unsigned char)0;
					break;
				}
				case 3:{ // domina el morado
					obs_teorica_plana[pos+0]=(unsigned char)255;
					obs_teorica_plana[pos+1]=(unsigned char)0;
					obs_teorica_plana[pos+2]=(unsigned char)128;
					break;
				}
				default:{ // nadie domina, as� que a gris
					obs_teorica_plana[pos+0]=(unsigned char)128;
					obs_teorica_plana[pos+1]=(unsigned char)128;
					obs_teorica_plana[pos+2]=(unsigned char)128;
					break;
				}
			}
		}
	}

  for(i=0; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS/(NUM_COLUMNAS*NUM_COLUMNAS); i++){
    c=i%(SIFNTSC_COLUMNS/NUM_COLUMNAS);
    row=i/(SIFNTSC_COLUMNS/NUM_COLUMNAS);
    j=row*(SIFNTSC_COLUMNS/NUM_COLUMNAS)+c;

		imagenTeoricaResumida_buf[i*4+0] = obs_teorica_plana[j*4+0];
    imagenTeoricaResumida_buf[i*4+1] = obs_teorica_plana[j*4+1];
    imagenTeoricaResumida_buf[i*4+2] = obs_teorica_plana[j*4+2];
    imagenTeoricaResumida_buf[i*4+3] = 0;
	}
}

void crearImagenResumenReal()
{
	int i,j;
	int zona = 0;
	int c, row;
	int miZona = 0;
	int pos;

	for (i=0; i<SIFNTSC_COLUMNS/NUM_COLUMNAS; i++)	{
		if ((i!=0) && ((i%(SIFNTSC_COLUMNS/(NUM_COLUMNAS*NUM_COLUMNAS))) == 0)) { // salto en esa fila!
			zona ++; // zona siguiente
			miZona = zona;
		} else {
			zona = miZona;
		}

		for(j=0; j<SIFNTSC_ROWS/NUM_COLUMNAS; j++)	{
			if ((j!=0) && ((j%(SIFNTSC_ROWS/(NUM_COLUMNAS*NUM_FILAS))) == 0)) { // salto en esa columna!
				zona ++; // zona siguiente
			}

			pos = (j*(SIFNTSC_COLUMNS/NUM_COLUMNAS)+i)*NUM_COLUMNAS;
	
			switch (colores_encontrados[zona].color_dominante){
				case 0:{ // domina el rojo
					obs_real_plana[pos+0]=(unsigned char)0;
					obs_real_plana[pos+1]=(unsigned char)0;
					obs_real_plana[pos+2]=(unsigned char)255;
					break;
				}
				case 1:{ // domina el verde
					obs_real_plana[pos+0]=(unsigned char)0;
					obs_real_plana[pos+1]=(unsigned char)255;
					obs_real_plana[pos+2]=(unsigned char)0;
					break;
				}
				case 2:{ // domina el azul
					obs_real_plana[pos+0]=(unsigned char)255;
					obs_real_plana[pos+1]=(unsigned char)255;
					obs_real_plana[pos+2]=(unsigned char)0;
					break;
				}
				case 3:{ // domina el morado
					obs_real_plana[pos+0]=(unsigned char)255;
					obs_real_plana[pos+1]=(unsigned char)0;
					obs_real_plana[pos+2]=(unsigned char)128;
					break;
				}
				default:{ // nadie domina, as� que a gris
					obs_real_plana[pos+0]=(unsigned char)128;
					obs_real_plana[pos+1]=(unsigned char)128;
					obs_real_plana[pos+2]=(unsigned char)128;
					break;
				}
			}
		}
	}

  for(i=0; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS/(NUM_COLUMNAS*NUM_COLUMNAS); i++){
    c=i%(SIFNTSC_COLUMNS/NUM_COLUMNAS);
    row=i/(SIFNTSC_COLUMNS/NUM_COLUMNAS);
    j=row*(SIFNTSC_COLUMNS/NUM_COLUMNAS)+c;

		imagenRealResumida_buf[i*4+0] = obs_real_plana[j*4+0];
    imagenRealResumida_buf[i*4+1] = obs_real_plana[j*4+1];
    imagenRealResumida_buf[i*4+2] = obs_real_plana[j*4+2];
    imagenRealResumida_buf[i*4+3] = 0;
	}
}

void resumirImagenTeorica() {
	int i,j;
	int zona = 0; // zona en la que estamos
	int miZona = 0;
	int pos;

	for (i=0; i<NUM_COLUMNAS*NUM_FILAS; i++) { // inicializaci�n de las zonas
		colores_encontrados_teorica[i].num_pixel_azul=0;
		colores_encontrados_teorica[i].num_pixel_morado=0;
		colores_encontrados_teorica[i].num_pixel_rojo=0;
		colores_encontrados_teorica[i].num_pixel_verde=0;
	}

	for (i=0;i<SIFNTSC_COLUMNS/FACTOR_MUESTREO;i++)	{
		if ((i!=0) && ((i%(SIFNTSC_COLUMNS/(NUM_COLUMNAS*FACTOR_MUESTREO))) == 0)) { // salto en esa fila!
			zona ++; // zona siguiente
			miZona = zona;
		} else {
			zona = miZona;
		}

		for(j=0;j<SIFNTSC_ROWS/FACTOR_MUESTREO;j++)	{
			if ((j!=0) && ((j%(SIFNTSC_ROWS/(NUM_FILAS*FACTOR_MUESTREO))) == 0)) { // salto en esa columna!
				zona ++; // zona siguiente
			}

			pos = (j*(SIFNTSC_COLUMNS/FACTOR_MUESTREO)+i)*4;

			if (((unsigned char)obs_teorica_filtrada[pos+0]==0)
				&&((unsigned char)obs_teorica_filtrada[pos+1]==0)
				&&((unsigned char)obs_teorica_filtrada[pos+2]==255)) { // rojo
					colores_encontrados_teorica[zona].num_pixel_rojo ++;
			} else if (((unsigned char)obs_teorica_filtrada[pos+0]==0)
				&&((unsigned char)obs_teorica_filtrada[pos+1]==255)
				&&((unsigned char)obs_teorica_filtrada[pos+2]==0)) { // verde
					colores_encontrados_teorica[zona].num_pixel_verde ++;
			} else if (((unsigned char)obs_teorica_filtrada[pos+0]==255)
				&&((unsigned char)obs_teorica_filtrada[pos+1]==255)
				&&((unsigned char)obs_teorica_filtrada[pos+2]==0)) { // azul
					colores_encontrados_teorica[zona].num_pixel_azul ++;
			} else if (((unsigned char)obs_teorica_filtrada[pos+0]==255)
				&&((unsigned char)obs_teorica_filtrada[pos+1]==0)
				&&((unsigned char)obs_teorica_filtrada[pos+2]==128)) { // morado
					colores_encontrados_teorica[zona].num_pixel_morado ++;
			}
		}
	}

	for (zona = 0; zona < NUM_COLUMNAS*NUM_FILAS; zona ++) { // establecemos el color dominante
		if ((colores_encontrados_teorica[zona].num_pixel_rojo >= umbralColorZona)
			&& (colores_encontrados_teorica[zona].num_pixel_rojo >= colores_encontrados_teorica[zona].num_pixel_azul)
			&& (colores_encontrados_teorica[zona].num_pixel_rojo >= colores_encontrados_teorica[zona].num_pixel_verde)
			&& (colores_encontrados_teorica[zona].num_pixel_rojo >= colores_encontrados_teorica[zona].num_pixel_morado))
				colores_encontrados_teorica[zona].color_dominante = 0; // domina el Rojo
				//zonasActuales[zona] = 0;
		else if ((colores_encontrados_teorica[zona].num_pixel_verde >= umbralColorZona)
			&& (colores_encontrados_teorica[zona].num_pixel_verde >= colores_encontrados_teorica[zona].num_pixel_azul)
			&& (colores_encontrados_teorica[zona].num_pixel_verde >= colores_encontrados_teorica[zona].num_pixel_rojo)
			&& (colores_encontrados_teorica[zona].num_pixel_verde >= colores_encontrados_teorica[zona].num_pixel_morado))
				colores_encontrados_teorica[zona].color_dominante = 1; // domina el Verde
				//zonasActuales[zona] = 1;
		else if ((colores_encontrados_teorica[zona].num_pixel_azul >= umbralColorZona)
			&& (colores_encontrados_teorica[zona].num_pixel_azul >= colores_encontrados_teorica[zona].num_pixel_rojo)
			&& (colores_encontrados_teorica[zona].num_pixel_azul >= colores_encontrados_teorica[zona].num_pixel_verde)
			&& (colores_encontrados_teorica[zona].num_pixel_azul >= colores_encontrados_teorica[zona].num_pixel_morado))
				colores_encontrados_teorica[zona].color_dominante = 2; // domina el Azul
				//zonasActuales[zona] = 2;
		else if ((colores_encontrados_teorica[zona].num_pixel_morado >= umbralColorZona)
			&& (colores_encontrados_teorica[zona].num_pixel_morado >= colores_encontrados_teorica[zona].num_pixel_azul)
			&& (colores_encontrados_teorica[zona].num_pixel_morado >= colores_encontrados_teorica[zona].num_pixel_verde)
			&& (colores_encontrados_teorica[zona].num_pixel_morado >= colores_encontrados_teorica[zona].num_pixel_rojo))
				colores_encontrados_teorica[zona].color_dominante = 3; // domina el Morado
				//zonasActuales[zona] = 3;

		else colores_encontrados_teorica[zona].color_dominante=-1; // no hay nadie "dominante"
	}

	//crearImagenResumenTeorica ();
}

void resumirImagenReal() {
	int i,j;
	int zona = 0; // zona en la que estamos
	int miZona = 0;
	int pos;

	for (i=0; i<NUM_COLUMNAS*NUM_FILAS; i++) { // inicializaci�n de las zonas
		colores_encontrados[i].num_pixel_azul=0;
		colores_encontrados[i].num_pixel_morado=0;
		colores_encontrados[i].num_pixel_rojo=0;
		colores_encontrados[i].num_pixel_verde=0;
	}

	for (i=0;i<(SIFNTSC_COLUMNS/FACTOR_MUESTREO);i++)	{
		if ((i!=0) && ((i%(SIFNTSC_COLUMNS/40)) == 0)) { // salto en esa fila!
			zona ++; // zona siguiente
			miZona = zona;
		} else {
			zona = miZona;
		}

		for(j=0;j<SIFNTSC_ROWS/FACTOR_MUESTREO;j++)	{
			if ((j!=0) && ((j%(SIFNTSC_ROWS/30)) == 0)) { // salto en esa columna!
				zona ++; // zona siguiente
			}

			pos = (j*(SIFNTSC_COLUMNS/FACTOR_MUESTREO)+i)*4;

			if (((unsigned char)obs_real_filtrada[pos+0]==0)
				&&((unsigned char)obs_real_filtrada[pos+1]==0)
				&&((unsigned char)obs_real_filtrada[pos+2]==255)) { // rojo
					colores_encontrados[zona].num_pixel_rojo ++;
			} else if (((unsigned char)obs_real_filtrada[pos+0]==0)
				&&((unsigned char)obs_real_filtrada[pos+1]==255)
				&&((unsigned char)obs_real_filtrada[pos+2]==0)) { // verde
					colores_encontrados[zona].num_pixel_verde ++;
			} else if (((unsigned char)obs_real_filtrada[pos+0]==255)
				&&((unsigned char)obs_real_filtrada[pos+1]==255)
				&&((unsigned char)obs_real_filtrada[pos+2]==0)) { // azul
					colores_encontrados[zona].num_pixel_azul ++;
			} else if (((unsigned char)obs_real_filtrada[pos+0]==255)
				&&((unsigned char)obs_real_filtrada[pos+1]==0)
				&&((unsigned char)obs_real_filtrada[pos+2]==128)) { // morado
					colores_encontrados[zona].num_pixel_morado ++;
			}
		}
	}

	for (zona = 0; zona < NUM_COLUMNAS*NUM_FILAS; zona ++) { // establecemos el color dominante
		if ((colores_encontrados[zona].num_pixel_rojo >= umbralColorZona)
			&& (colores_encontrados[zona].num_pixel_rojo >= colores_encontrados[zona].num_pixel_azul)
			&& (colores_encontrados[zona].num_pixel_rojo >= colores_encontrados[zona].num_pixel_verde)
			&& (colores_encontrados[zona].num_pixel_rojo >= colores_encontrados[zona].num_pixel_morado))
				colores_encontrados[zona].color_dominante = 0; // domina el Rojo
		else if ((colores_encontrados[zona].num_pixel_verde >= umbralColorZona)
			&& (colores_encontrados[zona].num_pixel_verde >= colores_encontrados[zona].num_pixel_azul)
			&& (colores_encontrados[zona].num_pixel_verde >= colores_encontrados[zona].num_pixel_rojo)
			&& (colores_encontrados[zona].num_pixel_verde >= colores_encontrados[zona].num_pixel_morado))
				colores_encontrados[zona].color_dominante = 1; // domina el Verde
		else if ((colores_encontrados[zona].num_pixel_azul >= umbralColorZona)
			&& (colores_encontrados[zona].num_pixel_azul >= colores_encontrados[zona].num_pixel_rojo)
			&& (colores_encontrados[zona].num_pixel_azul >= colores_encontrados[zona].num_pixel_verde)
			&& (colores_encontrados[zona].num_pixel_azul >= colores_encontrados[zona].num_pixel_morado))
				colores_encontrados[zona].color_dominante = 2; // domina el Azul
		else if ((colores_encontrados[zona].num_pixel_morado >= umbralColorZona)
			&& (colores_encontrados[zona].num_pixel_morado >= colores_encontrados[zona].num_pixel_azul)
			&& (colores_encontrados[zona].num_pixel_morado >= colores_encontrados[zona].num_pixel_verde)
			&& (colores_encontrados[zona].num_pixel_morado >= colores_encontrados[zona].num_pixel_rojo))
				colores_encontrados[zona].color_dominante = 3; // domina el Morado

		else colores_encontrados[zona].color_dominante=-1; // no hay nadie "dominante"
	}

	crearImagenResumenReal ();
}

float distancia()
{
  int num_pixel,i,j,iguales,primera_vez,object_index,cola_min,cola_max,cont=0;
  float distancia;
  object_type real_objects[OBS_COLUMNS/2];

  /*Calculo de la distancia entre observaciones*/
  num_pixel=OBS_COLUMNS;
  object_index=0;
  i=0;
  while(i<OBS_COLUMNS-1)
    {
     
      /*Marron*/
      if ((obs_real_plana[i*3]==(char)0)&&(obs_real_plana[i*3+1]==(char)51)
	  &&(obs_real_plana[i*3+2]==(char)153))
	{	  
	  if (primera_vez){
	    real_objects[object_index].min=i; 
	    primera_vez=0; 
	    real_objects[object_index].colour='O';
	  }
	  if ((obs_real_plana[(i+1)*3]!=(char)0)||(obs_real_plana[(i+1)*3+1]!=(char)51)
	      ||(obs_real_plana[(i+1)*3+2]!=(char)153)||((i+1)==OBS_COLUMNS))
	    {
	      real_objects[object_index].max=i;
	      real_objects[object_index].weight=0.;
	      object_index+=1;
	      primera_vez=1;
	    }
	}
      
      /*Verde*/
      if ((obs_real_plana[i*3]==(char)0)&&(obs_real_plana[i*3+1]==(char)255)
	  &&(obs_real_plana[i*3+2]==(char)0))
	{	  
	  if (primera_vez){
	    real_objects[object_index].min=i; 
	    primera_vez=0; 
	    real_objects[object_index].colour='V';
	  }
	  if ((obs_real_plana[(i+1)*3]!=(char)0)||(obs_real_plana[(i+1)*3+1]!=(char)255)
	      ||(obs_real_plana[(i+1)*3+2]!=(char)0)||((i+1)==OBS_COLUMNS))
	    {
	      real_objects[object_index].max=i;
	      real_objects[object_index].weight=0.;
	      object_index+=1;
	      primera_vez=1;
	    }
	}
      
      /*Azul*/
      if ((obs_real_plana[i*3]==(char)255)&&(obs_real_plana[i*3+1]==(char)0)
	  &&(obs_real_plana[i*3+2]==(char)0))
	{	  
	  if (primera_vez){
	    real_objects[object_index].min=i; 
	    primera_vez=0; 
	    real_objects[object_index].colour='A';
	  }
	  if ((obs_real_plana[(i+1)*3]!=(char)255)||(obs_real_plana[(i+1)*3+1]!=(char)0)
	      ||(obs_real_plana[(i+1)*3+2]!=(char)0)||((i+1)==OBS_COLUMNS))
	    {
	      real_objects[object_index].max=i;
	      real_objects[object_index].weight=0.;
	      object_index+=1;
	      primera_vez=1;
	    }
	  
	}

      /*Blanco*/
      if ((obs_real_plana[i*3]==(char)255)&&(obs_real_plana[i*3+1]==(char)255)
	  &&(obs_real_plana[i*3+2]==(char)255))
	{	  
	  if (primera_vez){
	    real_objects[object_index].min=i; 
	    primera_vez=0; 
	    real_objects[object_index].colour='B';
	  }
	  if ((obs_real_plana[(i+1)*3]!=(char)255)||(obs_real_plana[(i+1)*3+1]!=(char)255)
	      ||(obs_real_plana[(i+1)*3+2]!=(char)255)||((i+1)==OBS_COLUMNS))
	    {
	      real_objects[object_index].max=i;
	      real_objects[object_index].weight=0.;
	      object_index+=1;
	      primera_vez=1;
	    }
	  
	}
      
      i++;
      
    }
  
  if (!primera_vez) { real_objects[object_index].max=OBS_COLUMNS; object_index+=1;}
  
  /*  now i have to compare every object in real observation with the theoretical observation*/
  distancia=0;
  cont=object_index;
  for (i=0;i<object_index;i++)
    {
      if (real_objects[i].min-5>0) cola_min=real_objects[i].min-5; else	cola_min=0;
      if (real_objects[i].max+5<OBS_COLUMNS-1) cola_max=real_objects[i].max+5; else cola_max=OBS_COLUMNS-1;
      
      //printf("Cola min = %d Cola max = %d\n",cola_min,cola_max);
      iguales=0;
      for(j=cola_min;j<cola_max;j++)
	{   
	  switch (real_objects[i].colour){
	  case 'O':{
	    if ((obs_teorica_plana[j*3]==(char)0)&&(obs_teorica_plana[j*3+1]==(char)51)
		&&(obs_teorica_plana[j*3+2]==(char)153)) {
	      if ((real_objects[i].max-real_objects[i].min+1)<25)
		iguales+=2;
	      else
		iguales++;
			}
	    break;
	  }
	  case 'V':{
	    if ((obs_teorica_plana[j*3]==(char)0)&&(obs_teorica_plana[j*3+1]==(char)255)
		&&(obs_teorica_plana[j*3+2]==(char)0))
	      iguales+=2;
	    break;
	  }
	  case 'A':{
	    if ((obs_teorica_plana[j*3]==(char)255)&&(obs_teorica_plana[j*3+1]==(char)0)
		&&(obs_teorica_plana[j*3+2]==(char)0))
	      iguales++;
	    break;
	  }
	  case 'B':{
	    if ((obs_teorica_plana[j*3]==(char)255)&&(obs_teorica_plana[j*3+1]==(char)255)
		&&(obs_teorica_plana[j*3+2]==(char)255))
	      iguales++;
	    break;
	  }
	  }
	  
	}     
      
      if (iguales>(real_objects[i].max-real_objects[i].min + 1))
	iguales=real_objects[i].max-real_objects[i].min + 1;
      
      real_objects[i].weight=(float)((float)iguales/(float)(real_objects[i].max-real_objects[i].min+1));

      if ((real_objects[i].weight==1.)&&(real_objects[i].colour=='B')) cont--;
    }
  /*White pixels are not very informative*/

  for (i=0;i<object_index;i++)
    {
      if (real_objects[i].weight<1.)
	{
	  if ((object_index!=1)||(real_objects[i].colour!='B'))
	    {
	      if ((real_objects[i].colour=='B')
		  &&(real_objects[i].weight==0.))
		real_objects[i].weight=-1.;
	      else
		if ((real_objects[i].colour=='B')
		    &&(real_objects[i].weight>=0.4))

		  real_objects[i].weight-=(1-real_objects[i].weight);
		
	      real_objects[i].total_weight=(float)(1./(float)cont);
	      distancia+=real_objects[i].weight*real_objects[i].total_weight;
	      //printf("Distancia = %f\n",distancia);
	    }
	}
      else
	if (real_objects[i].colour!='B')
	  {
	    real_objects[i].total_weight=(float)(1./(float)cont);
	    distancia+=real_objects[i].weight*real_objects[i].total_weight;
	  }
    }
  
  
  if ((distancia<0.7)&&(distancia>=0.2)) distancia=0.1;
  else
    if (distancia<0.2) distancia=0.;
  //printf("Se parecen en un %.2f\%\n",distancia);    
  return distancia;
}

void location_grid_accumulation(Tgrid *grid, float theta)
{
  int i;
  Tvoxel punto;
  for(i=0;i<(grid->size*grid->size);i++)
    {
      punto.x=(*grid).map[i].centro.x; punto.y=(*grid).map[i].centro.y;
      obs_teorica_refresh(punto.x,punto.y,theta);
      create_obs_teorica();
            
      p_observada=distancia();
      ratio_acumulado=(*grid).map[i].probabilidad;

      if (p_observada<=0.4) p_observada=0.4;
      if (p_observada>=0.8) p_observada=0.9;

      ratio_observado=p_observada/(1-p_observada);

      ratio=ratio_acumulado+log(ratio_observado);
      
      if ((ratio<COTA_MAX) && (ratio>COTA_MIN)){
	(*grid).map[i].probabilidad= ratio;
      }      
      else
	if ((ratio)>0)
	  (*grid).map[i].probabilidad=COTA_MAX;
	else
	  (*grid).map[i].probabilidad=COTA_MIN;
      
      //(*grid).map[i].probabilidad=ALPHA*distancia()+(1-ALPHA)*(*grid).map[i].probabilidad;
    }
}

void location_grid_completion(Tgrid *grid, float theta)
{
  int i,celda;
  Tvoxel punto;
  for(i=0;i<(grid->size*grid->size);i++)
    {
      //i=20;
      punto.x=(*grid).map[i].centro.x; punto.y=(*grid).map[i].centro.y;
      celda=xy2celda_vision(punto,visual_loc_grid);
      obs_teorica_refresh(punto.x,punto.y,theta);

      create_obs_teorica();
      p_observada=distancia();

      ratio=log(p_observada/(1-p_observada));
      
      if ((ratio<COTA_MAX) && (ratio>COTA_MIN)){
	(*grid).map[i].probabilidad= ratio;
      }      
      else
	if ((ratio)>0)
	  (*grid).map[i].probabilidad=COTA_MAX;
	else
	  (*grid).map[i].probabilidad=COTA_MIN;

      (*grid).map[i].actualizada=0;
    }
  (*grid).theta=theta;
}  

void reload_probability_cube(Tgrid *grid)
{
  int i;
  for(i=0;i<(grid->size*grid->size);i++)
    (*grid).map[i].probabilidad=COTA_MIN;
}

void relocate_probability_cloud(int old_index, int index)
{
  int i,j,i2,j2,old_celda,nueva_celda;
  float probability;
    
  for(i=0;i<(cubo_valido[index]->size);i++)
    for(j=0;j<(cubo_valido[index]->size);j++)
      {
	i2=i+incremento_celdilla_x;
	j2=j+incremento_celdilla_y;
	
	if ((i2>=0)&&(i2<cubo_valido[index]->size)
	    &&(j2>=0)&&(j2<cubo_valido[index]->size))
	  {
	    old_celda=j*cubo_valido[old_index]->size+i;
	    nueva_celda=j2*cubo_valido[index]->size+i2;
	    probability=cubo_valido[old_index]->map[old_celda].probabilidad;
	    otro_cubo[index]->map[nueva_celda].probabilidad=probability;
	    
	  }
	else ; /* la nueva posicion cae fuera de cubo */
      }
  
}

double average_ilumination()
{
  int i,R,G,B;
  double ilumination=0.;
  for (i=0; i<(SIFNTSC_COLUMNS*SIFNTSC_ROWS)*3;i=i+3)
    { if (mycolorA != NULL) {
      B=(unsigned char)(*mycolorA)[i];
      G=(unsigned char)(*mycolorA)[i+1];
      R=(unsigned char)(*mycolorA)[i+2];
      }
      ilumination=ilumination+((R+G+B)/3.);
    }
  ilumination=ilumination/(SIFNTSC_COLUMNS*SIFNTSC_ROWS);
  return ilumination;
}


/* Calculamos la coordenada (x, y) correspondiente en un sistema de referencia local solidario al robot, d�ndonos un p�xel de esa imagen (entre 320 y 240 en u y v respectivamente). */
void pixel2coordenadaLocal (int u, int v, int* x, int* y) {
	int variacionPixelX = anchoVentana / SIFNTSC_COLUMNS;
	int variacionPixelY = altoVentana / SIFNTSC_ROWS;

	*x = -(anchoVentana/2) + (u*variacionPixelX);
	*y = (altoVentana/2) - (v*variacionPixelY);
}

/* Pasamos de coordenadas locales al robot (myencoders) a posiciones absolutas en el mundo */
void local2absoluto (float posParticleX, float posParticleY, float thetaParticle, int x, int y, float* X, float *Y) {
/*
	*X = myencoders[0] + x*cos(myencoders[2]) - y*sin(myencoders[2]);
	*Y = myencoders[1] + x*sin(myencoders[2]) + y*cos(myencoders[2]);*/

	*X = posParticleX + x*cos(thetaParticle) - y*sin(thetaParticle);
	*Y = posParticleY + x*sin(thetaParticle) + y*cos(thetaParticle);
}

Tcolor devolverColor (float X, float Y) {
	Tcolor myColor;

	if (mycolorB != NULL) {
		int column = (int) (X/anchoCelda);
		int row = (int) (Y/altoCelda);
		row = (SIFNTSC_ROWS-1) - row;
		column = (SIFNTSC_COLUMNS-1) - column;
		int offset = row * SIFNTSC_COLUMNS + column;

		myColor.R = (unsigned char) (*mycolorB)[offset*3+2];
		myColor.G = (unsigned char) (*mycolorB)[offset*3+1];
		myColor.B = (unsigned char) (*mycolorB)[offset*3+0];
	}

	return (myColor);
} 

void filtroTeorica (float posParticleX, float posParticleY, float thetaParticle) {
	int i,j,c,row,offset,R,G,B;
	float X, Y;
	int x, y;
	Tcolor myColor;
/*
  for (j=0; j<SIFNTSC_ROWS/FACTOR_MUESTREO; j++) {
    for (i=0; i<SIFNTSC_COLUMNS/FACTOR_MUESTREO; i++) {
			offset = (j*SIFNTSC_COLUMNS+i)*FACTOR_MUESTREO;
*/

	for (i=0; i<(SIFNTSC_COLUMNS*SIFNTSC_ROWS)/100;i++) {
    c=i%(SIFNTSC_COLUMNS/FACTOR_MUESTREO);
    row=i/(SIFNTSC_COLUMNS/FACTOR_MUESTREO);
    j=(row*(SIFNTSC_COLUMNS)+c)*FACTOR_MUESTREO;

			pixel2coordenadaLocal (c*FACTOR_MUESTREO, row*FACTOR_MUESTREO, &x, &y); // paso a coordenadas relativas al robot
			local2absoluto (posParticleX, posParticleY, thetaParticle, x, y, &X, &Y); // paso a coordenadas absolutas en el mundo

			// Me quedo siempre con las cuatro esquinas, para dibujar el recuadro
			if ((row == 0) && (c == 0)) {
				punto1x = X;
				punto1y = Y;
			} else if ((row == 0) && (c == ((SIFNTSC_COLUMNS/FACTOR_MUESTREO)-1))) {
				punto2x = X;
				punto2y = Y;
			} else if ((row == ((SIFNTSC_ROWS/FACTOR_MUESTREO)-1)) && (c == 0)) {
				punto3x = X;
				punto3y = Y;
			} else if ((row == ((SIFNTSC_ROWS/FACTOR_MUESTREO)-1)) && (c == ((SIFNTSC_COLUMNS/FACTOR_MUESTREO)-1))) {
				punto4x = X;
				punto4y = Y;
			} else if ((row == 0) && (c == (((SIFNTSC_COLUMNS/FACTOR_MUESTREO)-1)/2))) {
				puntoNortex = X;
				puntoNortey = Y;
			}
			myColor = devolverColor (X, Y);

			//Hacemos conversion a HSV
			myHSV2 = (struct HSV*) RGB2HSV_getHSV ((int)myColor.R,(int)myColor.G,(int)myColor.B); // pasamos de RGB -> HSV

			//Filtro para el rojo
			if ((((myHSV2->H*DEGTORAD >= 0) && (myHSV2->H*DEGTORAD <= HmaxRojo)) || ((myHSV2->H*DEGTORAD >= HminRojo) && (myHSV2->H*DEGTORAD <= 6.28))) && ((myHSV2->S >= SminRojo) && (myHSV2->S <= SmaxRojo))) {
				B = 0; G = 0; R = 255;
				obs_teorica_filtrada[i*4+0]= (unsigned char)B;
				obs_teorica_filtrada[i*4+1]= (unsigned char)G;
				obs_teorica_filtrada[i*4+2]= (unsigned char)R;
			}

			//Filtro para el verde
			else if (((myHSV2->H*DEGTORAD >= HminVerde) && (myHSV2->H*DEGTORAD <= HmaxVerde) &&
				(myHSV2->S >= SminVerde) && (myHSV2->S <= SmaxVerde))) {
				B = 0; G = 255; R = 0;
				obs_teorica_filtrada[i*4+0]= (unsigned char)B;
				obs_teorica_filtrada[i*4+1]= (unsigned char)G;
				obs_teorica_filtrada[i*4+2]= (unsigned char)R;
			}

			//Filtro para el cyan
			else if (((myHSV2->H*DEGTORAD >= HminCyan) && (myHSV2->H*DEGTORAD <= HmaxCyan) &&
				(myHSV2->S >= SminCyan) && (myHSV2->S <= SmaxCyan))) {
				B = 255; G = 255; R = 0;
				obs_teorica_filtrada[i*4+0]= (unsigned char)B;
				obs_teorica_filtrada[i*4+1]= (unsigned char)G;
				obs_teorica_filtrada[i*4+2]= (unsigned char)R;
			}

			//Filtro para el morado
			else if (((myHSV2->H*DEGTORAD >= HminMorado) && (myHSV2->H*DEGTORAD <= HmaxMorado) &&
				(myHSV2->S >= SminMorado) && (myHSV2->S <= SmaxMorado))) {
				B = 255; G = 0; R = 128;
				obs_teorica_filtrada[i*4+0]= (unsigned char)B;
				obs_teorica_filtrada[i*4+1]= (unsigned char)G;
				obs_teorica_filtrada[i*4+2]= (unsigned char)R;
			}

			//Si no se ha filtrado ningun color
			else {
				obs_teorica_filtrada[i*4+0]=(unsigned char)(int)128;
				obs_teorica_filtrada[i*4+1]=(unsigned char)(int)128;
				obs_teorica_filtrada[i*4+2]=(unsigned char)(int)128;
			}
		}
}

void filtro() {
	int i,R,G,B,j,c,row;
	double H,S,I;
	float grey;

/*	for (i=0; i<(SIFNTSC_COLUMNS*SIFNTSC_ROWS);i++) {
		c=i%(SIFNTSC_COLUMNS);
		row=i/(SIFNTSC_COLUMNS);
		j=row*SIFNTSC_COLUMNS+c;*/

	for (i=0; i<(SIFNTSC_COLUMNS*SIFNTSC_ROWS)/100;i++) {
    c=i%(SIFNTSC_COLUMNS/FACTOR_MUESTREO);
    row=i/(SIFNTSC_COLUMNS/FACTOR_MUESTREO);
    j=(row*(SIFNTSC_COLUMNS)+c)*FACTOR_MUESTREO;

		B=(unsigned char)(*mycolorA)[j*3]; 
		G=(unsigned char)(*mycolorA)[j*3+1]; 
		R=(unsigned char)(*mycolorA)[j*3+2]; 

		//Hacemos conversion a HSV
		myHSV = (struct HSV*) RGB2HSV_getHSV ((int)R,(int)G,(int)B); // pasamos de RGB -> HSV

		/* Filtro para el rojo */
		if ((((myHSV->H*DEGTORAD >= 0) && (myHSV->H*DEGTORAD <= HmaxRojo)) || ((myHSV->H*DEGTORAD >= HminRojo) && (myHSV->H*DEGTORAD <= 6.28))) && ((myHSV->S >= SminRojo) && (myHSV->S <= SmaxRojo))) {
		   /*(((myHSV->H*DEGTORAD >= HminRojo) && (myHSV->H*DEGTORAD <= HmaxRojo) &&
			 (myHSV->V >= VminRojo) && (myHSV->V <= VmaxRojo))) {*/
			B = 0; G = 0; R = 255;
			obs_real_filtrada[i*4+0]= (unsigned char)B;
			obs_real_filtrada[i*4+1]= (unsigned char)G;
			obs_real_filtrada[i*4+2]= (unsigned char)R;
		}

		/* Filtro para el verde */ 
		else if (((myHSV->H*DEGTORAD >= HminVerde) && (myHSV->H*DEGTORAD <= HmaxVerde) &&
			(myHSV->S >= SminVerde) && (myHSV->S <= SmaxVerde))) {
			B = 0; G = 255; R = 0;
			obs_real_filtrada[i*4+0]= (unsigned char)B;
			obs_real_filtrada[i*4+1]= (unsigned char)G;
			obs_real_filtrada[i*4+2]= (unsigned char)R;
		}

		/* Filtro para el cyan */ 
		else if (((myHSV->H*DEGTORAD >= HminCyan) && (myHSV->H*DEGTORAD <= HmaxCyan) &&
			(myHSV->S >= SminCyan) && (myHSV->S <= SmaxCyan))) {
			B = 255; G = 255; R = 0;
			obs_real_filtrada[i*4+0]= (unsigned char)B;
			obs_real_filtrada[i*4+1]= (unsigned char)G;
			obs_real_filtrada[i*4+2]= (unsigned char)R;
		}

		/* Filtro para el morado */ 
		else if (((myHSV->H*DEGTORAD >= HminMorado) && (myHSV->H*DEGTORAD <= HmaxMorado) &&
			(myHSV->S >= SminMorado) && (myHSV->S <= SmaxMorado))) {
			B = 255; G = 0; R = 128;
			obs_real_filtrada[i*4+0]= (unsigned char)B;
			obs_real_filtrada[i*4+1]=(unsigned char)G;
			obs_real_filtrada[i*4+2]=(unsigned char)R;
		}

		/*Si no se ha filtrado ningun color*/
		else {
			obs_real_filtrada[i*4+0]=(unsigned char)(int)128;
			obs_real_filtrada[i*4+1]=(unsigned char)(int)128;
			obs_real_filtrada[i*4+2]=(unsigned char)(int)128;
		}
	} // fin for (i<(SIFNTSC_COLUMNS*SIFNTSC_ROWS))
}

int distance_between_images()
{
  int i,distintos=0;
	for(i=0;i<OBS_COLUMNS;i++) {
		if ((obs_real_plana[i*3]!=previous_filtered[i*3])
			&&(obs_real_plana[i*3+1]!=previous_filtered[i*3+1])
			&&(obs_real_plana[i*3+2]!=previous_filtered[i*3+2]))
				distintos++;
	}
  
  if (((float)distintos/(float)OBS_COLUMNS)>=0.1)
    return 1;
  else 
    return 0;
}

void create_particles(float min, float max_x, float max_y)
{
  int i, k;
  float j;
 /*
  for(i=0;i<NUM_PARTICLES;i=i++)
    {
      j=(float)(MAXRNDMTABLE)*rand()/RAND_MAX;
      particles[i].point.x=(float) ((max_x - min) * pseudorandomize_table[(int)j]);
      j=(float)(MAXRNDMTABLE)*rand()/RAND_MAX;
      particles[i].point.y=(float) ((max_y - min) * pseudorandomize_table[(int)j]);
      j=(float)(MAXRNDMTABLE)*rand()/RAND_MAX;
      particles[i].theta = (float) (DEGTORAD * ((360) * pseudorandomize_table[(int)j]));
    }
*/
  // CON DISTINTAS THETAS PARA CADA PART�CULA (360 / 36) = CADA 10 GRADOS
  for(i=0;i<NUM_PARTICLES;i=i+36)
    {
      j=(float)(MAXRNDMTABLE)*rand()/RAND_MAX;
			for (k=0; k < 36; k ++) {
	      particles[i+k].point.x=(float) ((max_x - min) * pseudorandomize_table[(int)j]);
			}
      j=(float)(MAXRNDMTABLE)*rand()/RAND_MAX;
			for (k=0; k < 36; k ++) {
	      particles[i+k].point.y=(float) ((max_y - min) * pseudorandomize_table[(int)j]);
			}
			for (k=0; k < 36; k ++) {
	      particles[i+k].theta=(10*DEGTORAD)*k;
			}
    }

/*  // UNIFORME
	int i, j, offset;

  for (j=0; j<PARTICULAS_EN_Y; j++) {
    for (i=0; i<PARTICULAS_EN_X; i++) {
			offset = j*PARTICULAS_EN_X+i; // leemos la imagen de forma inversa a como la proyectamos...

			particles[offset].point.x = (7750/PARTICULAS_EN_X)*i;//anchoCelda*i*FACTOR_MUESTREO;
			particles[offset].point.y = (4500/PARTICULAS_EN_Y)*j;//altoCelda*j*FACTOR_MUESTREO;
			particles[offset].theta = 0;//DEGTORAD*myTheta;

		}
	}*/
}

void cleaning_particles_observation()
{
  int i, j;

  for (i=0;i<NUM_PARTICLES;i++)
    {
      particles[i].point.x=0.;
      particles[i].point.y=0.;
      particles[i].theta=0.;  
      particles[i].probability=0.0;
    }
} 

float myDistance () {
	int puntuacionDominante;
	int puntuacionSubdominante;
	float puntuacionGlobalZona = 0.;
	int i;

	for (i = 0; i < NUM_COLUMNAS*NUM_FILAS; i ++) {
		puntuacionDominante = 0;
		puntuacionSubdominante = 0;

		if ((colores_encontrados[i].color_dominante != -1) && (colores_encontrados_teorica[i].color_dominante != -1) && (colores_encontrados[i].color_dominante == colores_encontrados_teorica[i].color_dominante)) {
			puntuacionDominante ++;
			switch (colores_encontrados[i].color_dominante) {
				case (0):{ // domina el rojo -> comprobamos los dem�s
					if ((colores_encontrados[i].num_pixel_azul >= umbralColorZona) && (colores_encontrados_teorica[i].num_pixel_azul >= umbralColorZona))
						puntuacionSubdominante ++;
					if ((colores_encontrados[i].num_pixel_morado >= umbralColorZona) && (colores_encontrados_teorica[i].num_pixel_morado >= umbralColorZona))
						puntuacionSubdominante ++;

					if ((colores_encontrados[i].num_pixel_verde >= umbralColorZona) && (colores_encontrados_teorica[i].num_pixel_verde >= umbralColorZona))
						puntuacionSubdominante ++;
					break;}
				case (1):{ // domina el verde -> comprobamos los dem�s
					if ((colores_encontrados[i].num_pixel_azul >= umbralColorZona) && (colores_encontrados_teorica[i].num_pixel_azul >= umbralColorZona))
						puntuacionSubdominante ++;

					if ((colores_encontrados[i].num_pixel_morado >= umbralColorZona) && (colores_encontrados_teorica[i].num_pixel_morado >= umbralColorZona))
						puntuacionSubdominante ++;

					if ((colores_encontrados[i].num_pixel_rojo >= umbralColorZona) && (colores_encontrados_teorica[i].num_pixel_rojo >= umbralColorZona))
						puntuacionSubdominante ++;
					break;}
				case (2):{ // domina el azul -> comprobamos los dem�s
					if ((colores_encontrados[i].num_pixel_verde >= umbralColorZona) && (colores_encontrados_teorica[i].num_pixel_verde >= umbralColorZona))
						puntuacionSubdominante ++;

					if ((colores_encontrados[i].num_pixel_morado >= umbralColorZona) && (colores_encontrados_teorica[i].num_pixel_morado >= umbralColorZona))
						puntuacionSubdominante ++;

					if ((colores_encontrados[i].num_pixel_rojo >= umbralColorZona) && (colores_encontrados_teorica[i].num_pixel_rojo >= umbralColorZona))
						puntuacionSubdominante ++;
					break;}
				case (3):{ // domina el morado -> comprobamos los dem�s
					if ((colores_encontrados[i].num_pixel_verde >= umbralColorZona) && (colores_encontrados_teorica[i].num_pixel_verde >= umbralColorZona))
						puntuacionSubdominante ++;

					if ((colores_encontrados[i].num_pixel_azul >= umbralColorZona) && (colores_encontrados_teorica[i].num_pixel_azul >= umbralColorZona))
						puntuacionSubdominante ++;

					if ((colores_encontrados[i].num_pixel_rojo >= umbralColorZona) && (colores_encontrados_teorica[i].num_pixel_rojo >= umbralColorZona))
						puntuacionSubdominante ++;
					break;}
				default:{ // no hay color dominante
					if ((colores_encontrados[i].num_pixel_verde >= umbralColorZona) && (colores_encontrados_teorica[i].num_pixel_verde >= umbralColorZona))
						puntuacionSubdominante ++;

					if ((colores_encontrados[i].num_pixel_azul >= umbralColorZona) && (colores_encontrados_teorica[i].num_pixel_azul >= umbralColorZona))
						puntuacionSubdominante ++;

					if ((colores_encontrados[i].num_pixel_rojo >= umbralColorZona) && (colores_encontrados_teorica[i].num_pixel_rojo >= umbralColorZona))
						puntuacionSubdominante ++;
					break;}
			} // fin switch
		} // fin if
		puntuacionGlobalZona += ((0.3*puntuacionDominante) + (0.7*(puntuacionSubdominante/3))); // como mucho valdr� 1
	} // fin for

	return ((puntuacionGlobalZona/(NUM_COLUMNAS*NUM_FILAS))*10); // devolvemos en forma de probabilidad (casos favorables/casos posibles)
}

void calculate_particles_observation()
{
  int i;

  for (i=0;i<NUM_PARTICLES;i++) {
		filtroTeorica (particles[i].point.x, particles[i].point.y, particles[i].theta);
		resumirImagenTeorica ();

    particles[i].probability=myDistance();
		sprintf(texto,"   ");
		fl_set_object_label(fd_visual_locgui->progress,texto);
		sprintf(texto,"%i",(int)((i/NUM_PARTICLES)/100));
		fl_set_object_label(fd_visual_locgui->progress,texto);
  }
} 

void incorporate_particles_movement()
{
  float angulo_desplazado1,angulo_desplazado2;
  float xy_increase=0.,new_x,new_y,new_theta;
  int i,robot_moved;


  robot_moved=((fabs(myencoders[1]-incorporated_odometry[0])>=100.)
	       ||(fabs((-myencoders[0])-incorporated_odometry[1])>=100.)
	       ||(fabs(RADTODEG*myencoders[2]-RADTODEG*incorporated_odometry[2])>=1.0));
  
  if (robot_moved){
    desplazamiento_x=myencoders[1]-incorporated_odometry[0];
    desplazamiento_y=(-myencoders[0])-incorporated_odometry[1];
    xy_increase=sqrt((desplazamiento_x*desplazamiento_x) + (desplazamiento_y*desplazamiento_y));
    
    if (RADTODEG*myencoders[2]>RADTODEG*incorporated_odometry[2])
      {
	angulo_desplazado1=RADTODEG*myencoders[2]-RADTODEG*incorporated_odometry[2]-360.;
	angulo_desplazado2=RADTODEG*myencoders[2]-RADTODEG*incorporated_odometry[2];
	if (fabs(angulo_desplazado1)<fabs(angulo_desplazado2)){
	  desplazamiento_theta=angulo_desplazado1;
	  grados_girados=grados_girados+fabs(angulo_desplazado1);}		
	else {
	  desplazamiento_theta=angulo_desplazado2;
	  grados_girados=grados_girados+fabs(angulo_desplazado2);
	}
	
      }		
    else if (RADTODEG*myencoders[2]<RADTODEG*incorporated_odometry[2])
      {
	angulo_desplazado1=RADTODEG*myencoders[2]-RADTODEG*incorporated_odometry[2]+360.;
	angulo_desplazado2=RADTODEG*myencoders[2]-RADTODEG*incorporated_odometry[2];
	if (fabs(angulo_desplazado1)<fabs(angulo_desplazado2)){
	  desplazamiento_theta=angulo_desplazado1;
	  grados_girados=grados_girados+fabs(angulo_desplazado1);}		
	else {
	  desplazamiento_theta=angulo_desplazado2;
	  grados_girados=grados_girados+fabs(angulo_desplazado2);			
	}
      }
    incorporated_odometry[0]=myencoders[1];
    incorporated_odometry[1]=-myencoders[0];
    incorporated_odometry[2]=myencoders[2];
  }
 /*
  for (i=0;i<NUM_PARTICLES;i++)
    {
      sigma_r=gauss_rndm(SIGMA_RADIUS,0.);
      sigma_t=gauss_rndm(SIGMA_THETA,0.);

      new_x=particles[i].point.x+(xy_increase+sigma_r)*cos((particles[i].theta)*DEGTORAD);
      new_y=particles[i].point.y+(xy_increase+sigma_r)*sin((particles[i].theta)*DEGTORAD);
      new_theta=particles[i].theta+desplazamiento_theta+sigma_t;

      if ((new_x>0.)&&(new_x<mi_mundo.dimension))
	particles[i].point.x=new_x;
      if ((new_y>0.)&&(new_y<mi_mundo.dimension))
	particles[i].point.y=new_y;
      /* in case of bad new position, the particle remains at the same place **
      while (new_theta>=360.) 
	new_theta-=360.;
      while (new_theta<0.)
	new_theta+=360.;
      particles[i].theta=new_theta;
    }
  */

  for (i=0;i<NUM_PARTICLES;i++)
    {
      sigma_r=gauss_rndm(SIGMA_RADIUS,0.);
      sigma_t=gauss_rndm(SIGMA_THETA,0.);

      new_x=particles[i].point.x+(xy_increase+sigma_r)*cos(particles[i].theta);
      new_y=particles[i].point.y+(xy_increase+sigma_r)*sin(particles[i].theta);
      new_theta=particles[i].theta+desplazamiento_theta+sigma_t;

      if ((new_x>0.)&&(new_x<mi_mundo.dimension))
	particles[i].point.x=new_x;
      if ((new_y>0.)&&(new_y<mi_mundo.dimension))
	particles[i].point.y=new_y;
      /* in case of bad new position, the particle remains at the same place */
      while (new_theta>=(360*DEGTORAD)) 
	new_theta-=(360*DEGTORAD);
      while (new_theta<0.)
	new_theta+=(360*DEGTORAD);
      particles[i].theta=new_theta;
    }

  desplazamiento_theta=0.;
  grados_girados=0.;

}


/*This function makes a binary search*/
int binary_search(float number)
{
  int li,ls,middle,found;
  
  li=0;
  ls=NUM_PARTICLES;
  middle=(li+ls)/2;
  found=0;
  while (!found)
    {      
      if ((number<=accumulation[middle].accumulation) &&
	  (number>=accumulation[middle].low)) found=1;
      else if (number>accumulation[middle].accumulation) li=middle;
      else ls=middle;
      middle=(ls+li)/2;
      
    }
  return middle;
  
}


/*This function makes the particles sample (it samples the particles)*/
void resample_particles()
{
  int i,id;
  float random_number,max_accumulation=0.;

  for(i=0;i<NUM_PARTICLES;i++)
    { accumulation[i].low=max_accumulation;
      accumulation[i].accumulation=particles[i].probability+max_accumulation;
      accumulation[i].point.x=particles[i].point.x;
      accumulation[i].point.y=particles[i].point.y;
      accumulation[i].theta=particles[i].theta;
      max_accumulation=accumulation[i].accumulation;
    }

  if (max_accumulation<=((float)(NUM_PARTICLES/4000))) 
    /* very small accumulated probability, very bad particles.
     we enforce a uniform roulette */
     {
       alarm_degeneration=1;
       printf("Degeneration\n");
       completed_particles=0;
     } 
  else alarm_degeneration=0;
    
    
    for (i=0;i<NUM_PARTICLES;i++){
      if (particles[i].probability>=0.){
       //  printf("Elitism %d %f\n",i,particles[i].probability); /* elitism */
       /*binary search*/
       
       //random_number=(max_accumulation-0)*pseudorandomize_table[i];
       random_number=(max_accumulation-0)*rand()/RAND_MAX;
       id=binary_search(random_number);
                    
       particles[i].point.x=accumulation[id].point.x;
       particles[i].point.y=accumulation[id].point.y;
       particles[i].theta=accumulation[id].theta;
     }
   }

}
       
/*
void incorporate_grid_movement()
{
  int i, indice=0;
  float angulo_desplazado1,angulo_desplazado2,umbral_theta=22.5;


  desplazamiento_x=desplazamiento_x+myencoders[0]-incorporated_odometry[0];
  desplazamiento_y=desplazamiento_y+myencoders[1]-incorporated_odometry[1];
  
  if (RADTODEG*myencoders[2]>RADTODEG*incorporated_odometry[2])
    {
      angulo_desplazado1=RADTODEG*myencoders[2]-RADTODEG*incorporated_odometry[2]-360;
      angulo_desplazado2=RADTODEG*myencoders[2]-RADTODEG*incorporated_odometry[2];
      if ((fabs(angulo_desplazado1)<=360.0)&&
	  (fabs(angulo_desplazado1)<fabs(angulo_desplazado2))){
	desplazamiento_theta=desplazamiento_theta+angulo_desplazado1;
	grados_girados=grados_girados+fabs(angulo_desplazado1);}		
      else{
	desplazamiento_theta=desplazamiento_theta+angulo_desplazado2;
	grados_girados=grados_girados+fabs(angulo_desplazado2);}		
		
    }
  else if (RADTODEG*myencoders[2]<RADTODEG*incorporated_odometry[2])
    {
      angulo_desplazado1=RADTODEG*myencoders[2]-RADTODEG*incorporated_odometry[2]+360;
      angulo_desplazado2=RADTODEG*myencoders[2]-RADTODEG*incorporated_odometry[2];
      if ((fabs(angulo_desplazado1)<=360.0)&&
	  (fabs(angulo_desplazado1)<fabs(angulo_desplazado2))){
	desplazamiento_theta=desplazamiento_theta+angulo_desplazado1;
	grados_girados=grados_girados+fabs(angulo_desplazado1);}		
      else{
	desplazamiento_theta=desplazamiento_theta+angulo_desplazado2;
	grados_girados=grados_girados+fabs(angulo_desplazado2);}			
    }

  incremento_celdilla_x=0; incremento_celdilla_y=0; incremento_theta=0;
  if (fabs(desplazamiento_x)>=200.){
    incremento_celdilla_x=(int)((float)(desplazamiento_x*(1+((resolucion/2.)/(unsigned)desplazamiento_x)))/(float)resolucion);
    desplazamiento_x=0.;}
	  
  if (fabs(desplazamiento_y)>=200.){
    incremento_celdilla_y=(int)((float)(desplazamiento_y*(1+((resolucion/2.)/(unsigned)desplazamiento_y)))/(float)resolucion);
    desplazamiento_y=0.;}

  if (((grados_girados)/*+umbral_theta/2**)>=umbral_theta){
    printf("SUPERADO EL UMBRAL\n");
    if (desplazamiento_theta<0) incremento_theta=1;
    else if (desplazamiento_theta>0) incremento_theta=-1;
	      
    desplazamiento_theta=0.;
    grados_girados=0.;
    printf("incremento = %d\n",incremento_theta);
  }

  for (i=0;i<NUM_REJILLAS;i++)
    {
      indice=i+incremento_theta;
      if (indice==NUM_REJILLAS) indice=0;
      if (indice==-1) indice=NUM_REJILLAS-1;
      relocate_probability_cloud(i,indice);
    }

  incorporated_odometry[0]=myencoders[0];
  incorporated_odometry[1]=myencoders[1];
  incorporated_odometry[2]=myencoders[2];
}
*/

float calculate_theta_average()
{
  float media,aux_average;
  float pond_index,diff;
  int i;

  media=0.;
  aux_average=0.;
  
  for (i=1;i<NUM_PARTICLES;i++)
    {
      
      pond_index=particles[i].probability/(accumulation[i-1].accumulation+particles[i].probability);

      diff=particles[i-1].theta-particles[i].theta;
      
      if (((diff<(180*DEGTORAD))&&(diff>0.))
	  ||((diff>(-180*DEGTORAD))&&(diff<0.)))
	aux_average=particles[i-1].theta+pond_index*(particles[i].theta-particles[i-1].theta);
	      
      else if ((diff>(180*DEGTORAD))&&(diff>0.))
	{	  
	  aux_average=particles[i-1].theta+pond_index*((360*DEGTORAD)-(diff));
	  aux_average=((int)aux_average%6);
	}
      else if ((diff<(-180*DEGTORAD))&&(diff<0.))
	{
	  aux_average=particles[i].theta-pond_index*((-360*DEGTORAD)-(diff));
	  aux_average=((int)aux_average%6);
	}
      
      media+=aux_average;
      
    }
    
  return media;
}

void visual_loc_iteration() {
	struct timeval time;
	static long time_module;
	int i,index,time_value;
	float max,x_corregida,y_corregida,tht_corregida;
	float x_varianza,y_varianza;
	float desviacion_tipica;
	FILE *f;

	speedcounter(visual_loc_id);

	if (unavez){
		previous_robot[0]=myencoders[1];
		previous_robot[1]=-myencoders[0];
		previous_robot[2]=myencoders[2];

		rndm(); // ponemos semilla pseudo-random en las entradas de la tabla de part�culas
		gettimeofday(&time,NULL);
		time_module=time.tv_sec;
		unavez=0;
	}

	if (mycolorA != NULL) {
		filtro(); // Hacemos filtro de la imagen real en curso 
		resumirImagenReal();
	}
/*
	if (mycolorB != NULL) {
		if (!completed_particles) {
			//First uniform particles sample
			cleaning_particles_observation(); // borramos el contenido de las part�culas previas
			create_particles(min_num_part,max_num_part_x,max_num_part_y); // repartirmos part�culas entre 0 y max (X,Y)
			calculate_particles_observation();
			completed_particles=1;
		}*/

		if (filter_on) {
			calculate_particles_observation(); // Observation

			incorporate_particles_movement(); // Movement

			resample_particles(); // Resample
		}

		max=particles[0].probability;
		for (i=1;i<NUM_PARTICLES;i++)
			if (max<particles[i].probability) {
				max=particles[i].probability;
				index=i;
			}

		x_corregida=0.; y_corregida=0.; tht_corregida=0.;
		x_varianza=0.; y_varianza=0.;
		for (i=1;i<NUM_PARTICLES;i++) {
			x_corregida+=particles[i].point.x;
			y_corregida+=particles[i].point.y;
			x_varianza+=(particles[i].point.x*particles[i].point.x);
			y_varianza+=(particles[i].point.y*particles[i].point.y);
		}

		x_varianza=x_varianza/NUM_PARTICLES;
		y_varianza=y_varianza/NUM_PARTICLES;
		desviacion_tipica=sqrt((x_varianza-robot_corregido[0]*robot_corregido[0]))+sqrt((y_varianza-robot_corregido[1]*robot_corregido[1]));	
		gettimeofday(&time,NULL);
		time_value=time.tv_sec%time_module;
		if (file_log) {
			f=fopen("./varianza.dat","a");
			fprintf(f,"%.2f %d\n",desviacion_tipica,time_value);
			fclose(f);
		}

		if (maxime) { // Estimaci�n m�xima del robot
			robot_corregido[0]=particles[index].point.x;
			robot_corregido[1]=particles[index].point.y;
			robot_corregido[2]=particles[index].theta;	    
			robot_corregido[3]=cos(robot_corregido[2]);
			robot_corregido[4]=sin(robot_corregido[2]);
		}	else if (average) { // Estimaci�n media del robot
			robot_corregido[0]=x_corregida/NUM_PARTICLES;
			robot_corregido[1]=y_corregida/NUM_PARTICLES;
			robot_corregido[2]=(calculate_theta_average()/NUM_PARTICLES)*DEGTORAD;
			robot_corregido[3]=cos(robot_corregido[2]);
			robot_corregido[4]=sin(robot_corregido[2]);
		}
}

void visual_loc_suspend()
{
  pthread_mutex_lock(&(all[visual_loc_id].mymutex));
  put_state(visual_loc_id,slept);
  printf("visual_loc: off\n");
  pthread_mutex_unlock(&(all[visual_loc_id].mymutex)); 
}

void visual_loc_resume(int father, int *brothers, arbitration fn)
{
	int i;

  pthread_mutex_lock(&(all[visual_loc_id].mymutex));
  /* this schema resumes its execution with no children at all */
  for(i=0;i<MAX_SCHEMAS;i++) all[visual_loc_id].children[i]=FALSE;
 
  all[visual_loc_id].father=father;
  if (brothers!=NULL)
    {
      for(i=0;i<MAX_SCHEMAS;i++) visual_loc_brothers[i]=-1;
      i=0;
      while(brothers[i]!=-1) {visual_loc_brothers[i]=brothers[i];i++;}
    }
  visual_loc_callforarbitration=fn;
  put_state(visual_loc_id,notready);
/*
  mylaser=(int *)myimport("laser","laser");
  laserresume=(resumeFn)myimport("laser","resume");
  lasersuspend=(suspendFn *)myimport("laser","suspend");
*/
  myencoders=(float *)myimport("encoders","jde_robot");
  encodersresume=(resumeFn)myimport("encoders","resume");
  encoderssuspend=(suspendFn)myimport("encoders","suspend");

  myv=(float *)myimport("motors","v");
  myw=(float *)myimport("motors","w");
  motorsresume=(resumeFn)myimport("motors","resume");
  motorssuspend=(suspendFn)myimport("motors","suspend");

	mycolorA=myimport ("colorA", "colorA");
	colorAresume=(resumeFn)myimport("colorA", "resume");
	colorAsuspend=(suspendFn)myimport("colorA", "suspend");

	mycolorB=myimport ("colorB", "colorB");
	colorBresume=(resumeFn)myimport("colorB", "resume");
	colorBsuspend=(suspendFn)myimport("colorB", "suspend");

  printf("visual_loc: on\n");
  pthread_cond_signal(&(all[visual_loc_id].condition));
  pthread_mutex_unlock(&(all[visual_loc_id].mymutex));
}

void visual_loc_close()
{
  pthread_mutex_lock(&(all[visual_loc_id].mymutex));
  visual_loc_suspend();  
  pthread_mutex_unlock(&(all[visual_loc_id].mymutex));
  sleep(2);
  free(imagenA_buf);
  free(imagenB_buf);
}

void *visual_loc_thread(void *not_used) 
{
  struct timeval a,b;
  long diff, next;

  for(;;)
    {
      pthread_mutex_lock(&(all[visual_loc_id].mymutex));

      if (all[visual_loc_id].state==slept) 
	{
	  pthread_cond_wait(&(all[visual_loc_id].condition),&(all[visual_loc_id].mymutex));
	  pthread_mutex_unlock(&(all[visual_loc_id].mymutex));
	}
      else 
	{
	  /* check preconditions. For now, preconditions are always satisfied*/
	  if (all[visual_loc_id].state==notready) put_state(visual_loc_id,ready);
	  else if (all[visual_loc_id].state==ready)	  /* check brothers and arbitrate. For now this is the only winner */
	    { 
	      put_state(visual_loc_id,winner);
	      *myv=0; *myw=0; 
	      /* start the winner state from controlled motor values */ 
	      //all[visual_loc_id].children[(*(int *)myimport("laser","id"))]=TRUE;
	      all[visual_loc_id].children[(*(int *)myimport("motors","id"))]=TRUE;
	      all[visual_loc_id].children[(*(int *)myimport("encoders","id"))]=TRUE;
	      //laserresume(visual_loc_id,NULL,NULL);
	      encodersresume(visual_loc_id,NULL,NULL);
	      motorsresume(visual_loc_id,NULL,NULL);
	    }	  
	  else if (all[visual_loc_id].state==winner);

	  if (all[visual_loc_id].state==winner)
	    /* I'm the winner and must execute my iteration */
	    {
	      pthread_mutex_unlock(&(all[visual_loc_id].mymutex));
	      gettimeofday(&a,NULL);
	      visual_loc_iteration();
	      gettimeofday(&b,NULL);  

	      diff = (b.tv_sec-a.tv_sec)*1000000+b.tv_usec-a.tv_usec;
	      next = visual_loc_cycle*1000-diff-10000; 
	      if (next>0) 
		/* discounts 10ms taken by calling usleep itself */
		usleep(visual_loc_cycle*1000-diff);
	      else 
		/* just let this iteration go away. overhead time negligible */
		{printf("time interval violated: visual_loc\n"); 
		usleep(visual_loc_cycle*1000);
		}
	    }
	  else 
	    {
	      pthread_mutex_unlock(&(all[visual_loc_id].mymutex));
	      usleep(visual_loc_cycle*1000);
	    }
	}
    }
}

static int InitOGL(FL_OBJECT *ob, Window win,int w,int h, XEvent *xev, void *ud)
 /* Inicializa OpenGL con los parametros que diran como se visualiza. */
{
   glViewport(0,0,(GLint)w,(GLint)h);  
   glDrawBuffer(GL_BACK);
   glClearColor(0.6f, 0.8f, 1.0f, 0.0f);  
   glClearDepth(1.0);
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
   glEnable(GL_TEXTURE_2D);     /* Enable Texture Mapping */
   glEnable (GL_AUTO_NORMAL);
   glEnable (GL_NORMALIZE);  
   glEnable(GL_DEPTH_TEST);     /* Enables Depth Testing */
   glDepthFunc(GL_LESS);  
   glShadeModel(GL_SMOOTH);     /* Enables Smooth Color Shading */
   glEnable(GL_BLEND);
   glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

   return 0;
}

void initCalibration () {
  K_1 = gsl_matrix_alloc(3,3);
  R_1 = gsl_matrix_alloc(3,3);
  x_1 = gsl_vector_alloc(3);

	// Fill K_1 matrix, with information got on Calibrator GUI
	gsl_matrix_set(K_1,0,0,468.0);
  gsl_matrix_set(K_1,0,1,-1.16);
  gsl_matrix_set(K_1,0,2,179.89);

	gsl_matrix_set(K_1,1,0,0);
  gsl_matrix_set(K_1,1,1,473.43);
  gsl_matrix_set(K_1,1,2,152.59);

	gsl_matrix_set(K_1,2,0,0);
  gsl_matrix_set(K_1,2,1,0);
  gsl_matrix_set(K_1,2,2,1);

	// Fill R_1 matrix, with information got on Calibrator GUI
	gsl_matrix_set(R_1,0,0,0.64);
  gsl_matrix_set(R_1,0,1,-0.76);
  gsl_matrix_set(R_1,0,2,-0.09);

	gsl_matrix_set(R_1,1,0,-0.39);
  gsl_matrix_set(R_1,1,1,-0.43);
  gsl_matrix_set(R_1,1,2,0.82);

	gsl_matrix_set(R_1,2,0,0.66);
  gsl_matrix_set(R_1,2,1,0.49);
  gsl_matrix_set(R_1,2,2,0.57);

	// Fill x_1 vector, with information got on Calibrator GUI
  gsl_vector_set(x_1, 0, -478.2);
  gsl_vector_set(x_1, 1, -444.1);
  gsl_vector_set(x_1, 2, -465.4);
}

void setRobotCameraPos () {
  HPoint2D p2;
  HPoint3D p3;
  int i,j=0;
  gsl_matrix *RT,*T,*Res;
  
  RT = gsl_matrix_alloc(4,4);
  T = gsl_matrix_alloc(3,4);
  Res = gsl_matrix_alloc(3,4);
  
  gsl_matrix_set(T,0,0,1);
  gsl_matrix_set(T,1,1,1);
  gsl_matrix_set(T,2,2,1);

  gsl_matrix_set(T,0,3,gsl_vector_get(x_1,0));
  gsl_matrix_set(T,1,3,gsl_vector_get(x_1,1));
  gsl_matrix_set(T,2,3,gsl_vector_get(x_1,2));

  gsl_linalg_matmult (R_1,T,Res);  

  for (i=0;i<3;i++)
    for (j=0;j<4;j++)
      gsl_matrix_set(RT,i,j,gsl_matrix_get(Res,i,j));
  
  
  /** set 0001 in the last row of RT */
  gsl_matrix_set(RT,3,0,0);
  gsl_matrix_set(RT,3,1,0);
  gsl_matrix_set(RT,3,2,0);
  gsl_matrix_set(RT,3,3,1);

  /** Set camera position*/

  robotCamera.position.X = gsl_vector_get(x_1,0);
  robotCamera.position.Y = gsl_vector_get(x_1,1);
  robotCamera.position.Z = gsl_vector_get(x_1,2);

  printf("Camera position %f:%f:%f\n",robotCamera.position.X, robotCamera.position.Y, robotCamera.position.Z);
      
  /** Seting intrensic matrix*/
  robotCamera.k11 = gsl_matrix_get(K_1,0,0);
  robotCamera.k12 = gsl_matrix_get(K_1,0,1);
  robotCamera.k13 = gsl_matrix_get(K_1,0,2);
  robotCamera.k14 = 0;

  robotCamera.k21 = gsl_matrix_get(K_1,1,0);
  robotCamera.k22 = gsl_matrix_get(K_1,1,1);
  robotCamera.k23 = gsl_matrix_get(K_1,1,2);
  robotCamera.k24 = 0;

  robotCamera.k31 = gsl_matrix_get(K_1,2,0);
  robotCamera.k32 = gsl_matrix_get(K_1,2,1);
  robotCamera.k33 = gsl_matrix_get(K_1,2,2);
  robotCamera.k34 = 0;

  /** Seting extrensic*/
  robotCamera.rt11 = gsl_matrix_get(RT,0,0);
  robotCamera.rt12 = gsl_matrix_get(RT,0,1);
  robotCamera.rt13 = gsl_matrix_get(RT,0,2);
  robotCamera.rt14 = gsl_matrix_get(RT,0,3);
  
  robotCamera.rt21 = gsl_matrix_get(RT,1,0);
  robotCamera.rt22 = gsl_matrix_get(RT,1,1);
  robotCamera.rt23 = gsl_matrix_get(RT,1,2);
  robotCamera.rt24 = gsl_matrix_get(RT,1,3);

  robotCamera.rt31 = gsl_matrix_get(RT,2,0);
  robotCamera.rt32 = gsl_matrix_get(RT,2,1);
  robotCamera.rt33 = gsl_matrix_get(RT,2,2);
  robotCamera.rt34 = gsl_matrix_get(RT,2,3);

  robotCamera.rt41 = gsl_matrix_get(RT,3,0);
  robotCamera.rt42 = gsl_matrix_get(RT,3,1);
  robotCamera.rt43 = gsl_matrix_get(RT,3,2);
  robotCamera.rt44 = gsl_matrix_get(RT,3,3);

  p3.X = 100;
  p3.Y = 100;
  p3.Z = 100;
  p3.H = 1;

  progtest2D.x = 0;
  progtest2D.y = 0;
  progtest2D.h = 1;

  project(p3,&p2,robotCamera);
  
  if (backproject(&progtest3D, progtest2D, robotCamera)){
    progtest3D.X =  progtest3D.X/progtest3D.H;
    progtest3D.Y =  progtest3D.Y/progtest3D.H;
    progtest3D.Z =  progtest3D.Z/progtest3D.H;
  }
}

void visual_loc_startup() {
  pthread_mutex_lock(&(all[visual_loc_id].mymutex));
  myexport("visual_loc","id",&visual_loc_id);
  myexport("visual_loc","resume",(void *) &visual_loc_resume);
  myexport("visual_loc","suspend",(void *) &visual_loc_suspend);
  printf("visual_loc schema started up\n");
  put_state(visual_loc_id,slept);
  pthread_create(&(all[visual_loc_id].mythread),NULL,visual_loc_thread,NULL);
	if (myregister_buttonscallback==NULL){
    if ((myregister_buttonscallback=
	 (registerbuttons)myimport ("graphics_xforms", "register_buttonscallback"))==NULL){
      printf ("I can't fetch register_buttonscallback from graphics_xforms\n");
      jdeshutdown(1);
    }
    if ((mydelete_buttonscallback=(deletebuttons)myimport ("graphics_xforms", "delete_buttonscallback"))==NULL){
      printf ("I can't fetch delete_buttonscallback from graphics_xforms\n");
      jdeshutdown(1);
    }
    if ((myregister_displaycallback=
	 (registerdisplay)myimport ("graphics_xforms", "register_displaycallback"))==NULL){
      printf ("I can't fetch register_displaycallback from graphics_xforms\n");
      jdeshutdown(1);
    }
    if ((mydelete_displaycallback=(deletedisplay)myimport ("graphics_xforms", "delete_displaycallback"))==NULL){
      jdeshutdown(1);
      printf ("I can't fetch delete_displaycallback from graphics_xforms\n");
    }
  }

  if ((screen=(int *)myimport("graphics_xforms", "screen"))==NULL){
    fprintf (stderr, "visual_loc: I can't fetch screen from graphics_xforms\n");
    jdeshutdown(1);
  }
  if ((display=(Display *)myimport("graphics_xforms", "display"))==NULL){
    fprintf (stderr, "visual_loc: I can't fetch display from graphics_xforms\n");
    jdeshutdown(1);
  }
  pthread_mutex_unlock(&(all[visual_loc_id].mymutex));

	/****************/

  Tvoxel centrogrid,centrogrid_loc;
  char *configfile="./visual_loc.conf"; 
  char *location_file="./location.conf";
  FILE *f;
  int i;

	if (STAGE) {
		printf("Building stage world...\n");
		capturarMundo ();
		create_configfile(configfile,mi_mundo.dimension,69.5);

		centrogrid.x=mi_mundo.dimension/2.;
		centrogrid.y=mi_mundo.dimension/2.;

		theta_robot=mi_mundo.theta_robot;
		x_rob=mi_mundo.x_robot;
		y_rob=mi_mundo.y_robot;
	}

	printf("Visual Localization configuration written on file %s\n",configfile);

	visual_loc_grid=inicia_grid(centrogrid,configfile);
	relleno_grid(mi_mundo, visual_loc_grid);  

	create_configfile(location_file,5000,100.);
	centrogrid_loc.x=x_rob;
	centrogrid_loc.y=y_rob;

	for(i=0;i<NUM_REJILLAS;i++){
		cubo_probabilidad_1[i]=inicia_grid(centrogrid_loc,location_file);
		cubo_probabilidad_2[i]=inicia_grid(centrogrid_loc,location_file);
	}

	cubo_valido=cubo_probabilidad_1;
	otro_cubo=cubo_probabilidad_2;

	location_grid=inicia_grid(centrogrid_loc,location_file);
	resolucion=location_grid->resolucion;

	for(i=0;i<(location_grid->size*location_grid->size);i++)
		location_grid->map[i].estado=-1;

	robot_teorico[0]=0.;
	robot_teorico[1]=0.;
	robot_teorico[2]=0.;
	robot_teorico[3]=cos(robot_teorico[2]);
	robot_teorico[4]=sin(robot_teorico[2]);

	min_num_part=0.;
	max_num_part_y=4500;
	max_num_part_x=7750;

	f=fopen("./varianza.dat","w");
	fprintf(f,"");
	fclose(f);

	// Valores iniciales para la c�mara virtual con la que observo la escena
  virtualcam0.position.X=4000.;
  virtualcam0.position.Y=4000.;
  virtualcam0.position.Z=6000.;
  virtualcam0.position.H=1.;
  virtualcam0.foa.X=0.;
  virtualcam0.foa.Y=0.;
  virtualcam0.foa.Z=0.;
  virtualcam0.position.H=1.;
  virtualcam0.roll=0.;

	initCalibration (); // calibramos la c�mara del robot
	setRobotCameraPos (); // con los par�metros de calibraci�n, posicionamos nuestra robotCamera

  int argc=1;
  char **argv;
  char *aa;
  char a[]="myjde";
 
  aa=a;
  argv=&aa;

	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

	RGB2HSV_init(); // reserva de la tabla de filtrado
	RGB2HSV_createTable();
}

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/*******************P A R T E   G R A F I C A**********************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/

void drawRecuadro () {
	// Pintar situaci�n del robot:
	glColor3f (1., 0., 0.);
	glPointSize (20.0);

	glColor3f (0., 0., 1.);
	glPointSize (12.0);
	glLineWidth (12.0);
	glBegin(GL_LINE_LOOP);
		v3f(punto1x, punto1y, 3100);
		v3f(punto2x, punto2y, 3100);
		v3f(punto4x, punto4y, 3100);
		v3f(punto3x, punto3y, 3100);
	glEnd();

	glColor3f (0., 1., 0.);

  glBegin(GL_LINES);
		if (manualMode)
		  v3f(intersectionPoint.X, intersectionPoint.Y, 3100);
		else
		  v3f(myencoders[1],-myencoders[0], 3100);

	  v3f(puntoNortex, puntoNortey, 3100);
  glEnd();
}

int celdaRejilla2xy (float celdaX, float celdaY, int *i, int *j) {
	if ((celdaX >= (anchoCelda*SIFNTSC_COLUMNS)) || (celdaY >= (altoCelda*SIFNTSC_ROWS)) || (celdaX < 0) || (celdaY < 0)) // celda no perteneciente
		return (-1);
	else {
		*i = (int) (celdaX/(anchoCelda*SIFNTSC_COLUMNS));
		*j = (int) (celdaY/(altoCelda*SIFNTSC_ROWS));
		return (0); // celda OK
	}
}

int xy2celdaRejilla(int X, int Y, int* celdaX, int* celdaY) {
  int celda=-1;
  int i,j;
  
  int resolucion=6000;
  
 	i=(int)X/anchoCelda;
  j=(int)Y/altoCelda;

	if ((i>=0) && (i<SIFNTSC_COLUMNS) && (j>=0) && (j<SIFNTSC_ROWS)) { // punto perteneciente a rejilla
	  if ((i>=0) && (j>=0)) {
			*celdaX = i;
			*celdaY = j;
		  return (0);
		} else
			return (-1);
	}
}

void drawTecho () {
	int i,j,i2,j2,offset;
	unsigned char red,green,blue;

	if (mycolorB != NULL) {
	  for (j=0, j2=SIFNTSC_ROWS-1; j<SIFNTSC_ROWS, j2>=0; j++, j2--) {
	    for (i=0, i2=SIFNTSC_COLUMNS-1; i<SIFNTSC_COLUMNS, i2>=0; i++, i2--) {
				offset = j2*320+i2; // leemos la imagen de forma inversa a como la proyectamos...

			  blue = (unsigned char)(*mycolorB)[offset*3+0];
			  green = (unsigned char)(*mycolorB)[offset*3+1];
			  red = (unsigned char)(*mycolorB)[offset*3+2];
				
				glColor3f((float)red/255.0, (float)green/255.0, (float)blue/255.0);

				// pintamos el punto correspondiente en el escenario pintado con OGL
				glPointSize (6.0);
				glBegin(GL_POINTS);
					v3f(anchoCelda*i, altoCelda*j, 3000);
				glEnd();
			}
		}

		inicializadoArray = TRUE;

		glPointSize (1.0);
	}
}

void drawParticles () {
	int i;
	float prob, x, y, theta;

	glPointSize (7.0);

  for (i = 0 ; i < NUM_PARTICLES; i++) {
		prob = particles[i].probability;
		x = particles[i].point.x;
		y = particles[i].point.y;
		theta = particles[i].theta; // TODO: de momento la estamos despreciando

		if(prob==0) glColor3f(1,1,1);
/*		else if(prob<0.066) glColor3f(0.966666666666667,0.966666666666667,0.966666666666667);
		else if(prob<0.11) glColor3f(0.933333333333333,0.933333333333333,0.933333333333333);
		else if(prob<0.133) glColor3f(0.9,0.9,0.9);*/
		else if(prob<=0.25) glColor3f(1.,1.,0.); //glColor3f(0.866666666666667,0.866666666666667,0.866666666666667);
/*		else if(prob<0.20) glColor3f(0.833333333333333,0.833333333333333,0.833333333333333);
		else if(prob<0.23) glColor3f(0.8,0.8,0.8);
		else if(prob<0.26) glColor3f(0.766666666666667,0.766666666666667,0.766666666666667);
		else if(prob<0.30) glColor3f(0.733333333333333,0.733333333333333,0.733333333333333);*/
		else if(prob<=0.50) glColor3f(0.,1.,0.);//glColor3f(0.7,0.7,0.7);
/*		else if(prob<0.36) glColor3f(0.666666666666667,0.666666666666667,0.666666666666667);
		else if(prob<0.40) glColor3f(0.633333333333333,0.633333333333333,0.633333333333333);
		else if(prob<0.43) glColor3f(0.6,0.6,0.6);
		else if(prob<0.46) glColor3f(0.566666666666667,0.566666666666667,0.566666666666667);
		else if(prob<0.50) glColor3f(0.533333333333333,0.533333333333333,0.533333333333333);
		else if(prob<0.53) glColor3f(0.5,0.5,0.5);
		else if(prob<0.56) glColor3f(0.466666666666667,0.466666666666667,0.466666666666667);
		else if(prob<0.60) glColor3f(0.433333333333333,0.433333333333333,0.433333333333333);
		else if(prob<0.63) glColor3f(0.4,0.4,0.4);*/
		else if(prob<=0.75) glColor3f(0.,1.,1.);//glColor3f(0.366666666666667,0.366666666666667,0.366666666666667);
/*		else if(prob<0.70) glColor3f(0.333333333333333,0.333333333333333,0.333333333333333);
		else if(prob<0.73) glColor3f(0.3,0.3,0.3);
		else if(prob<0.76) glColor3f(0.266666666666667,0.266666666666667,0.266666666666667);
		else if(prob<0.80) glColor3f(0.233333333333333,0.233333333333333,0.233333333333333);
		else if(prob<0.83) glColor3f(0.2,0.2,0.2);
		else if(prob<0.86) glColor3f(0.166666666666667,0.166666666666667,0.166666666666667);
		else if(prob<0.90) glColor3f(0.133333333333333,0.133333333333333,0.133333333333333);
		else if(prob<0.93) glColor3f(0.1,0.1,0.1);
		else if(prob<0.96) glColor3f(0.0666666666666667,0.0666666666666667,0.0666666666666667);
		else if(prob<1.00) glColor3f(0.0333333333333333,0.0333333333333333,0.0333333333333333);*/
		else glColor3f(0.,0.,1.);//glColor3f(0,0,0);

		glBegin(GL_POINTS);
			v3f(x, y, 200);
		glEnd();
	}
}

void escribirFichero () {
	int i, k;
	float miX, miY, miTheta, maxProb;

	FILE *f;
	f = fopen("ficheroGrafico.txt", "w");

	if (f == NULL) {
	   perror ("Error to open graphic file ficheroGrafico.txt\n");
	   return -1;
	}

/*
  for (i = 0 ; i < NUM_PARTICLES; i++) {
		fprintf (f, "%f\t%f\t%f\t%f\n", (particles[i].point.x), (particles[i].point.y), (particles[i].theta), (particles[i].probability));
	}*/

/*	// IMPRIMIR LAS MEJORES THETAS DE CADA PART�CULA
  for(i=0;i<NUM_PARTICLES;i=i+36)
    {
		maxProb = 0;
		for (k=0; k < 36; k ++) {
			if (particles[i+k].probability >= maxProb) {
				miX = particles[i+k].point.x;
				miY = particles[i+k].point.y;
				miTheta = particles[i+k].theta;
				maxProb = particles[i+k].probability;
			}
    }
		fprintf (f, "%f\t%f\t%f\t%f\n", (miX), (miY), (miTheta), (maxProb));
	}
	*/

	printf ("Written graphic information on file\n");

	if(!fclose(f) )
		printf("File closed correctly\n" );
	else {
		printf( "Error to close graphic file\n" );
		return 1;
	}
}

int xy2canvas(Tvoxel point, XPoint* grafico)
/* return -1 if point falls outside the canvas */
{
  float xi, yi;
     
  xi = (point.x * odometrico[3] + point.y * odometrico[4] - odometrico[0])*escala;
  yi = (-point.x * odometrico[4] + point.y * odometrico[3] - odometrico[1])*escala;
      
  /* Con esto cambiamos al sistema de referencia de visualizacion, centrado en algun punto xy y con alguna orientacion definidos por odometrico. Ahora cambiamos de ese sistema al del display, donde siempre hay un desplazamiento a la esquina sup. izda. y que las y se cuentan para abajo. */
  
  grafico->x = xi + width/2;
  grafico->y = -yi + height/2;

  
  if ((grafico->x <0)||(grafico->x>width)) return -1; 
  if ((grafico->y <0)||(grafico->y>height)) return -1; 
  return 0;
}

int grid2canvas(Tvoxel point, XRectangle* rectangulo, Tgrid *grid)
{
  unsigned int lado;
  XPoint esquinaGraf;
  Tvoxel esquina;

  esquina.x=point.x-grid->resolucion/2.;
  esquina.y=point.y+grid->resolucion/2.;
    
  if (xy2canvas(esquina,&esquinaGraf)>=0)
    {
      lado=(unsigned int) (grid->resolucion*escala);
      if (lado==0) lado=1;
     
      rectangulo->x=esquinaGraf.x;
      rectangulo->y=esquinaGraf.y;
      rectangulo->width=lado;
      rectangulo->height=lado;
      return 0;
    }
  else return -1; /* point falls outside the canvas */
}

void vision(int color)
{/*
  Tvoxel cono,cono_destino1, cono_destino2;
  XPoint pinto,pinto_destino1,pinto_destino2,arc_limit;
   
  cono.x=robot_teorico[0];
  cono.y=robot_teorico[1];
  cono_destino1.x=cono.x+RADIO_VISION*cos((robot_teorico[2]+(ANGULO_VISION/2))*DEGTORAD);
  cono_destino1.y=cono.y+RADIO_VISION*sin((robot_teorico[2]+(ANGULO_VISION/2))*DEGTORAD);
   
  xy2canvas(cono,&pinto);
  xy2canvas(cono_destino1,&pinto_destino1);
  if (color) fl_set_foreground(visual_locgui_gc,FL_BLUE); else fl_set_foreground(visual_locgui_gc,suelo);
    
  XDrawLine(display,visual_loc_canvas_win,visual_locgui_gc,pinto.x,pinto.y,pinto_destino1.x,pinto_destino1.y);
  
  cono_destino2.x=cono.x+RADIO_VISION*cos((robot_teorico[2]-(ANGULO_VISION/2))*DEGTORAD);
  cono_destino2.y=cono.y+RADIO_VISION*sin((robot_teorico[2]-(ANGULO_VISION/2))*DEGTORAD);
  
  xy2canvas(cono_destino2,&pinto_destino2);
  XDrawLine(display,visual_loc_canvas_win,visual_locgui_gc,pinto_destino2.x,pinto_destino2.y,pinto.x,pinto.y);
  arc_limit.x=(pinto_destino2.x-pinto.x);
  arc_limit.y=(pinto_destino2.y-pinto.y);
 
  //XDrawArc(display,canvas_win,visual_locgui_gc,pinto.x-(arc_limit.x/2),pinto.y-(arc_limit.y/2),arc_limit.x,arc_limit.y,(robot_teorico[2]-40)*64,80*64);
  fl_set_foreground(visual_locgui_gc,suelo);*/
}
  
static int image_displaysetup() 
/* Inicializa las ventanas, la paleta de colores y memoria compartida para visualizacion*/ 
{
  XGCValues gc_values;
  XWindowAttributes win_attributes;
  XColor nuevocolor;
  int pixelNum, numCols;
  int allocated_colors=0, non_allocated_colors=0;
    
  visual_locgui_win= FL_ObjWin(fd_visual_locgui->ventana0);
  XGetWindowAttributes(display, visual_locgui_win, &win_attributes);  
  //screen = DefaultScreen(display);
  XMapWindow(display, visual_locgui_win);
    
  gc_values.graphics_exposures = False;
  visual_locgui_gc = XCreateGC(display, visual_locgui_win, GCGraphicsExposures, &gc_values);

	myHSV = (struct HSV*)malloc(sizeof(struct HSV));
 	myHSV2 = (struct HSV*)malloc(sizeof(struct HSV));
  /* Utilizan el Visual (=estructura de color) y el colormap con que este operando el programa principal con su Xforms. No crea un nuevo colormap, sino que modifica el que se estaba usando a traves de funciones de Xforms*/
  vmode= fl_get_vclass();
  if ((vmode==TrueColor))//&&(fl_state[vmode].depth==32)) 
    { printf("display: truecolor bpp\n");
	
      /*teorica observada*/
      imagen_teorica_buf = (char *) malloc(OBS_COLUMNS*OBS_ROWS*4); 
      imagen_teorica = XCreateImage(display,DefaultVisual(display,*screen),fl_state[vmode].depth,ZPixmap,0,imagen_teorica_buf,OBS_COLUMNS, OBS_ROWS,8,0);

      imagen_real_buf = (char *) malloc(OBS_COLUMNS*OBS_ROWS*4); 
      imagen_real = XCreateImage(display,DefaultVisual(display,*screen),fl_state[vmode].depth, ZPixmap,0,imagen_real_buf,OBS_COLUMNS, OBS_ROWS,8,0);

      imagenA_buf = (char *) malloc(SIFNTSC_COLUMNS*SIFNTSC_ROWS*4); 
      imagenA = XCreateImage(display,DefaultVisual(display,*screen),fl_state[vmode].depth, ZPixmap,0,imagenA_buf,SIFNTSC_COLUMNS, SIFNTSC_ROWS,8,0);
      imagenB_buf = (char *) malloc(SIFNTSC_COLUMNS*SIFNTSC_ROWS*4); 
      imagenB = XCreateImage(display,DefaultVisual(display,*screen),fl_state[vmode].depth, ZPixmap,0,imagenB_buf,SIFNTSC_COLUMNS, SIFNTSC_ROWS,8,0);

      imagenRealResumida = XCreateImage(display,DefaultVisual(display,*screen),fl_state[vmode].depth, ZPixmap,0,imagenRealResumida_buf,SIFNTSC_COLUMNS/4, SIFNTSC_ROWS/4,8,0);

      imagenTeoricaResumida = XCreateImage(display,DefaultVisual(display,*screen),fl_state[vmode].depth, ZPixmap,0,imagenTeoricaResumida_buf,SIFNTSC_COLUMNS/4, SIFNTSC_ROWS/4,8,0);

      return win_attributes.depth;
    }
  else 
    {
      perror("Unsupported color mode in X server");exit(1);
    }
  return win_attributes.depth;
}


int visual_loc_displaysetup()
{
  int i;

  visual_loc_Graf=malloc(sizeof(XRectangle)*visual_loc_grid->size*visual_loc_grid->size);
  if (visual_loc_Graf==NULL) {printf("gui: not enough memory for visual_loc_ display\n"); exit(-1);}
  
  for(i=0;i< ((visual_loc_grid->size)*(visual_loc_grid->size));i++) 
    grid2canvas(visual_loc_grid->map[i].centro,&visual_loc_Graf[i],visual_loc_grid);
  
  return 1;
}

void motion_event(FL_OBJECT *ob, Window win, int win_width, 
		  int win_height, XKeyEvent *xev, void *user_data){
  float longi, lati,r ;

	xev = (XKeyPressedEvent*)xev;

	/* las siguiente ecuaciones sirven mapear el movimiento:

	Eje X :: (0,w) -> (-180,180)
	Eje Y :: (0,h) -> (-90,90)

	donde (h,w) es el larcho,ancho del gl_canvas
	Si el button pulsado es 2 (centro del raton) entonces no actualizamos la posicion

	*/

	if (2 == xev->keycode) {
		flashImage = TRUE;
	  return;
	}

	mouse_on_canvas.x = ((xev->x*360/fd_visual_locgui->micanvas->w)-180);
	mouse_on_canvas.y = ((xev->y*-180/fd_visual_locgui->micanvas->h)+90);
}

void button_press_event (FL_OBJECT *ob, Window win, int win_width, 
			 int win_height, XKeyEvent *xev, void *user_data){
  
  xev = (XKeyPressedEvent*)xev;
  float longi, lati,r;

	/* Los dos modos son excluyentes */
	if (2 == xev->keycode) {
		flashImage = TRUE;
	  return;
	}else {
		flashImage = FALSE;
		if (1 == xev->keycode){
		  cam_mode = 1;
		  foa_mode = 0;  
		}else if (3 == xev->keycode){
			myPixel.y = xev->x;
			myPixel.x = 480-xev->y;
		  foa_mode = 1;
		  cam_mode = 0;
		}else if (5 == xev->keycode){
		  if (radius < MAX_RADIUS_VALUE)
		    radius+=WHEEL_DELTA;
		}else if (4 == xev->keycode){
		  if (radius>MIN_RADIUS_VALUE)
		    radius-=WHEEL_DELTA;
		}
	}
}

void initVirtualCam () {
  float r,lati,longi;

	if (!flashImage) {
/*	  if (foa_mode){
	    longi=2*PI*mouse_on_canvas.x/360.;
	    lati=2*PI*mouse_on_canvas.y/360.;
	    r=500;
	    virtualcam0.foa.X=r*cos(lati)*cos(longi);
	    virtualcam0.foa.Y=r*cos(lati)*sin(longi);
	    virtualcam0.foa.Z=r*sin(lati);
	  }*/
	  
	  if (cam_mode) {
	    longi=2*PI*mouse_on_canvas.x/360.;
	    lati=2*PI*mouse_on_canvas.y/360.;
	    
	    virtualcam0.position.X=radius*cos(lati)*cos(longi);
	    virtualcam0.position.Y=radius*cos(lati)*sin(longi);
	    virtualcam0.position.Z=radius*sin(lati);
	    
	    virtualcam0.foa.X=0;
	    virtualcam0.foa.Y=0;
	    virtualcam0.foa.Z=0;
	  }
	}
}

void initConfiguration () {
  fl_activate_glcanvas(fd_visual_locgui->micanvas);
  /* Set the OpenGL state machine to the right context for this display */
  /* reset of the depth and color buffers */
  InitOGL(fd_visual_locgui->micanvas, FL_ObjWin(fd_visual_locgui->micanvas),fd_visual_locgui->micanvas->w,fd_visual_locgui->micanvas->h,NULL,NULL);
    
  /* Virtual camera */
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity(); 

  /* perspective projection. intrinsic parameters + frustrum */
  gluPerspective(45.,(GLfloat)640/(GLfloat)480,1.0,50000.0);
  /* extrinsic parameters */
  gluLookAt(virtualcam0.position.X,virtualcam0.position.Y,virtualcam0.position.Z,virtualcam0.foa.X,virtualcam0.foa.Y,virtualcam0.foa.Z,0.,0.,1.);

	virtualcam0.fdist = (float) 480/(2*tan(DEGTORAD*(45/2)));
	virtualcam0.u0 = (float) 480/2;
	virtualcam0.v0 = (float) 640/2;

	update_camera_matrix (&virtualcam0);
	glEnable (GL_CULL_FACE); // solo veremos una cara de los planos, para el "efecto transparencia"
	glCullFace (GL_FRONT);
}

void drawRoboticLabPlanes () {
	/* LABORATORIO DE ROB�TICA */
  glLineWidth(2.0f);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

	/* SUELO */
  glColor3f(0.93, 0.93, 0.455);
	// Parte central:
  glBegin(GL_QUADS);
	  v3f(0.000000, 0.000000, 0.000000);
	  v3f(0.000000, 4589.000000, 0.000000);
	  v3f(7925.000000, 4589.000000, 0.000000);
	  v3f(7925.000000, 0.000000, 0.000000);
  glEnd();

	// Lateral de la puerta (1� parte):
  glBegin(GL_QUADS);
	  v3f(1211.000000, -554.000000, 0.000000);
	  v3f(1211.000000, 5.000000, 0.000000);
	  v3f(5476.000000, 5.000000, 0.000000);
	  v3f(5476.000000, -554.000000, 0.000000);
  glEnd();

	// Lateral de la puerta (trocito intermedio de sobrante defectuoso):
  glBegin(GL_QUADS);
	  v3f(5470.000000, -120.000000, 0.000000);
	  v3f(5470.000000, 10.000000, 0.000000);
	  v3f(6115.000000, 10.000000, 0.000000);
	  v3f(6115.000000, -120.000000, 0.000000);
  glEnd();

	// Lateral de la puerta (2� parte):
  glBegin(GL_QUADS);
	  v3f(6110.000000, -554.000000, 0.000000);
	  v3f(6110.000000, 5.000000, 0.000000);
	  v3f(7925.000000, 5.000000, 0.000000);
	  v3f(7925.000000, -554.000000, 0.000000);
  glEnd();

	// Lateral enfrente de la puerta (1� parte):
  glBegin(GL_QUADS);
	  v3f(2419.000000, 4580.000000, 0.000000);
	  v3f(2419.000000, 4879.000000, 0.000000);
	  v3f(5498.000000, 4879.000000, 0.000000);
	  v3f(5498.000000, 4580.000000, 0.000000);
  glEnd();

	// Lateral enfrente de la puerta (2� parte):
  glBegin(GL_QUADS);
	  v3f(6264.000000, 4580.000000, 0.000000);
	  v3f(6264.000000, 4879.000000, 0.000000);
	  v3f(7925.000000, 4879.000000, 0.000000);
	  v3f(7925.000000, 4580.000000, 0.000000);
  glEnd();

	/* CAMPO DE FUTBOL */
  glColor3f(0., 0.59, 0.);
  glBegin(GL_QUADS); // daremos algo de altura para no causar "conflictos graficos" con el suelo
	  v3f(1560.000000, 596.000000, 25.000000);
	  v3f(1560.000000, 4589.000000, 25.000000);
	  v3f(7925.000000, 4589.000000, 25.000000);
	  v3f(7925.000000, 596.000000, 25.000000);
  glEnd();

	/* PAREDES */

	// Lateral izquierdo:
  glColor3f(0.98, 0.98, 0.98);
  glBegin(GL_QUADS);
	  v3f(0.000000, 0.000000, 3000.000000);
	  v3f(0.000000, 4589.000000, 3000.000000);
	  v3f(0.000000, 4589.000000, 0.000000);
	  v3f(0.000000, 0.000000, 0.000000);
	glEnd();

	// Lateral enfrente de la puerta:
  glBegin(GL_QUADS); // 1� parte
	  v3f(0.000000, 4589.000000, 0.000000);
	  v3f(0.000000, 4589.000000, 3000.000000);
	  v3f(2419.000000, 4589.000000, 3000.000000);
	  v3f(2419.000000, 4589.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 2� parte
	  v3f(2419.000000, 4589.000000, 0.000000);
	  v3f(2419.000000, 4589.000000, 3000.000000);
	  v3f(2419.000000, 4879.000000, 3000.000000);
	  v3f(2419.000000, 4879.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 3� parte
	  v3f(2419.000000, 4879.000000, 0.000000);
	  v3f(2419.000000, 4879.000000, 3000.000000);
	  v3f(5476.000000, 4879.000000, 3000.000000);
	  v3f(5476.000000, 4879.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 4� parte
	  v3f(5476.000000, 4879.000000, 0.000000);
	  v3f(5476.000000, 4879.000000, 3000.000000);
	  v3f(5476.000000, 4589.000000, 3000.000000);
	  v3f(5476.000000, 4589.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 5� parte
	  v3f(5476.000000, 4589.000000, 0.000000);
	  v3f(5476.000000, 4589.000000, 3000.000000);
	  v3f(6264.000000, 4589.000000, 3000.000000);
	  v3f(6264.000000, 4589.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 6� parte
	  v3f(6264.000000, 4589.000000, 0.000000);
	  v3f(6264.000000, 4589.000000, 3000.000000);
	  v3f(6264.000000, 4879.000000, 3000.000000);
	  v3f(6264.000000, 4879.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 6� parte
	  v3f(6264.000000, 4879.000000, 0.000000);
	  v3f(6264.000000, 4879.000000, 3000.000000);
	  v3f(7925.000000, 4879.000000, 3000.000000);
	  v3f(7925.000000, 4879.000000, 0.000000);
	glEnd();

	// Lateral a la derecha de la puerta:
  glBegin(GL_QUADS); // 6� parte
	  v3f(7925.000000, 4879.000000, 0.000000);
	  v3f(7925.000000, 4879.000000, 3000.000000);
	  v3f(7925.000000, -554.000000, 3000.000000);
	  v3f(7925.000000, -554.000000, 0.000000);
	glEnd();

	// Lateral de la puerta:
  glBegin(GL_QUADS); // 1� parte
	  v3f(7925.000000, -554.000000, 0.000000);
	  v3f(7925.000000, -554.000000, 3000.000000);
	  v3f(6110.000000, -554.000000, 3000.000000);
	  v3f(6110.000000, -554.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 2� parte
	  v3f(6110.000000, -554.000000, 0.000000);
	  v3f(6110.000000, -554.000000, 3000.000000);
	  v3f(6110.000000, -119.000000, 3000.000000);
	  v3f(6110.000000, -119.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 3� parte
	  v3f(6110.000000, -119.000000, 0.000000);
	  v3f(6110.000000, -119.000000, 3000.000000);
	  v3f(5476.000000, -119.000000, 3000.000000);
	  v3f(5476.000000, -119.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 4� parte
	  v3f(5476.000000, -554.000000, 0.000000);
	  v3f(5476.000000, -554.000000, 3000.000000);
	  v3f(1211.000000, -554.000000, 3000.000000);
	  v3f(1211.000000, -554.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 5� parte
	  v3f(1211.000000, -554.000000, 0.000000);
	  v3f(1211.000000, -554.000000, 3000.000000);
	  v3f(1211.000000, 0.000000, 3000.000000);
	  v3f(1211.000000, 0.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 6� parte
	  v3f(1211.000000, 0.000000, 0.000000);
	  v3f(1211.000000, 0.000000, 3000.000000);
	  v3f(0.000000, 0.000000, 3000.000000);
	  v3f(0.000000, 0.000000, 0.000000);
	glEnd();

	/* PUERTA */
  glColor3f(0.67, 0., 0.);
  glBegin(GL_QUADS);
	  v3f(4613.000000, -554.000000, 0.000000);
	  v3f(4613.000000, -554.000000, 2030.000000);
	  v3f(3879.000000, -554.000000, 2030.000000);
	  v3f(3879.000000, -554.000000, 0.000000);
  glEnd();

	/* VENTANA */
  glColor3f(0.5, 0.5, 1.);
  glBegin(GL_QUADS);
	  v3f(2585.000000, 4879.000000, 835.000000);
	  v3f(2585.000000, 4879.000000, 2532.000000);
	  v3f(5335.000000, 4879.000000, 2532.000000);
	  v3f(5335.000000, 4879.000000, 835.000000);
  glEnd();
}

void drawHallLines () {
	/* el pasillo mide (15x1.99)+1.50 cm. de largo y 1.90 cm. de ancho */

	/* SUELO */
  glBegin(GL_LINES);
	  v3f(0.0, -554.0, 0.0);
	  v3f(31350.0, -554.0, 0.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(31350.0, -554.0, 0.0);
	  v3f(31350.0, -2454.0, 0.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(31350.0, -2454.0, 0.0);
	  v3f(0.0, -2454.0, 0.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(0.0, -2454.0, 0.0);
	  v3f(0.0, -554.0, 0.0);
  glEnd();

	/* TECHO DEL PASILLO */
  glBegin(GL_LINES);
	  v3f(0.0, -554.0, 3000.0);
	  v3f(31350.0, -554.0, 3000.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(31350.0, -554.0, 3000.0);
	  v3f(31350.0, -2454.0, 3000.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(31350.0, -2454.0, 3000.0);
	  v3f(0.0, -2454.0, 3000.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(0.0, -2454.0, 3000.0);
	  v3f(0.0, -554.0, 3000.0);
  glEnd();

	/* ESQUINAS */
  glBegin(GL_LINES);
	  v3f(0.0, -554.0, 0.0);
	  v3f(0.0, -554.0, 3000.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(0.0, -2454.0, 0.0);
	  v3f(0.0, -2454.0, 3000.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(31350.0, -554.0, 0.0);
	  v3f(31350.0, -554.0, 3000.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(31350.0, -2454.0, 0.0);
	  v3f(31350.0, -2454.0, 3000.0);
  glEnd();
}

void drawRoboticLabLines () {
  glColor3f(1., 1., 1.);
  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 3000.000000);
  v3f(0.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 3000.000000);
  v3f(1211.000000, 0.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, 0.000000, 3000.000000);
  v3f(1211.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, -554.000000, 3000.000000);
  v3f(5476.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -554.000000, 3000.000000);
  v3f(5476.000000, -119.000000, 3000.000000);
  glEnd();
	
  glBegin(GL_LINES);
  v3f(5476.000000, -119.000000, 3000.000000);
  v3f(6110.000000, -119.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -119.000000, 3000.000000);
  v3f(6110.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -554.000000, 3000.000000);
  v3f(7925.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, -554.000000, 3000.000000);
  v3f(7925.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, 4879.000000, 3000.000000);
  v3f(6264.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4879.000000, 3000.000000);
  v3f(6264.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 3000.000000);
  v3f(5498.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 3000.000000);
  v3f(5498.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4879.000000, 3000.000000);
  v3f(2419.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4879.000000, 3000.000000);
  v3f(2419.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4589.000000, 3000.000000);
  v3f(0.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 30.000000);
  v3f(0.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 30.000000);
  v3f(1211.000000, 0.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, 0.000000, 30.000000);
  v3f(1211.000000, -554.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, -554.000000, 30.000000);
  v3f(5476.000000, -554.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -554.000000, 30.000000);
  v3f(5476.000000, -119.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -119.000000, 30.000000);
  v3f(6110.000000, -119.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -119.000000, 30.000000);
  v3f(6110.000000, -554.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -554.000000, 30.000000);
  v3f(7925.000000, -554.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, -554.000000, 30.000000);
  v3f(7925.000000, 4879.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, 4879.000000, 30.000000);
  v3f(6264.000000, 4879.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4879.000000, 30.000000);
  v3f(6264.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 30.000000);
  v3f(5498.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 30.000000);
  v3f(5498.000000, 4879.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4879.000000, 30.000000);
  v3f(2419.000000, 4879.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4879.000000, 30.000000);
  v3f(2419.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4589.000000, 30.000000);
  v3f(0.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 0.000000);
  v3f(0.000000, 0.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, 0.000000, 0.000000);
  v3f(1211.000000, 0.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, -554.000000, 0.000000);
  v3f(1211.000000, -554.000000, 3000.000000);
  glEnd();

	// LINEAS LIMITROFES PUERTA-------------------
  glColor3f(1., 0.5, 0.);
  glBegin(GL_LINES);
  v3f(3879.000000, -554.000000, 0.000000);
  v3f(3879.000000, -554.000000, 2030.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(3879.000000, -554.000000, 2030.000000);
  v3f(4613.000000, -554.000000, 2030.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4613.000000, -554.000000, 2030.000000);
  v3f(4613.000000, -554.000000, 0.000000);
  glEnd();
	//--------------------------------------------

  glColor3f(1., 1., 1.);
  glBegin(GL_LINES);
  v3f(5476.000000, -554.000000, 0.000000);
  v3f(5476.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -119.000000, 0.000000);
  v3f(5476.000000, -119.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -119.000000, 0.000000);
  v3f(6110.000000, -119.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -554.000000, 0.000000);
  v3f(6110.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, -554.000000, 0.000000);
  v3f(7925.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, 4879.000000, 0.000000);
  v3f(7925.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4879.000000, 0.000000);
  v3f(6264.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 0.000000);
  v3f(6264.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 0.000000);
  v3f(5498.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4879.000000, 0.000000);
  v3f(5498.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4879.000000, 0.000000);
  v3f(2419.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4589.000000, 0.000000);
  v3f(2419.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 4589.000000, 0.000000);
  v3f(0.000000, 4589.000000, 3000.000000);
  glEnd();

	// LINEAS LIMITROFES DE LA VENTANA-----------
  glColor3f(1., 0.5, 0.);
  glBegin(GL_LINES);
  v3f(2585.000000, 4879.000000, 835.000000);
  v3f(5335.000000, 4879.000000, 835.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2585.000000, 4879.000000, 2532.000000);
  v3f(5335.000000, 4879.000000, 2532.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2585.000000, 4879.000000, 835.000000);
  v3f(2585.000000, 4879.000000, 2532.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5335.000000, 4879.000000, 835.000000);
  v3f(5335.000000, 4879.000000, 2532.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(3960.000000, 4879.000000, 835.000000);
  v3f(3960.000000, 4879.000000, 2532.000000);
  glEnd();
	//--------------------------------------------

  glColor3f(1., 1., 1.);
	// LINEAS LIMITROFES DEL CAMPO FUTBOL
  glBegin(GL_LINES);
  v3f(1560.000000, 596.000000, 30.000000);
  v3f(1560.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 30.000000);
  v3f(2419.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1560.000000, 596.000000, 30.000000);
  v3f(7925.000000, 596.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 30.000000);
  v3f(7925.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 796.000000, 30.000000);
  v3f(2160.000000, 4301.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 796.000000, 30.000000);
  v3f(7560.000000, 796.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 4301.000000, 30.000000);
  v3f(7560.000000, 4301.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 796.000000, 30.000000);
  v3f(7560.000000, 4301.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4860.000000, 796.000000, 30.000000);
  v3f(4860.000000, 4301.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 2051.000000, 30.000000);
  v3f(2810.000000, 2051.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2810.000000, 2051.000000, 30.000000);
  v3f(2810.000000, 3351.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2810.000000, 3351.000000, 30.000000);
  v3f(2160.000000, 3351.000000, 30.000000 );
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 2051.000000, 30.000000);
  v3f(6910.000000, 2051.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 3351.000000, 30.000000);
  v3f(6910.000000, 3351.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6910.000000, 2051.000000, 30.000000);
  v3f(6910.000000, 3351.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 2341.000000, 30.000000);
  v3f(4500.000000, 3061.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5220.000000, 2341.000000, 30.000000);
  v3f(5220.000000, 3061.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 2341.000000, 30.000000);
  v3f(5220.000000, 2341.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 3061.000000, 30.000000);
  v3f(5220.000000, 3061.000000, 30.000000);
  glEnd();
}

int linePlaneIntersection (HPoint3D A, HPoint3D B, HPoint3D *intersectionPoint) {
	HPoint3D v;	// Line director vector: it the same to take A or B as origin or destination extrem...

	v.X = (A.X - B.X);
	v.Y = (A.Y - B.Y);
	v.Z = (A.Z - B.Z);

	// We'll calculate the groun intersection (Z = 0) on our robot system. Parametric equations:
	intersectionPoint->Z = 0; // intersectionPoint->Z = A.Z + t*v.Z => t = (-A.Z / v.Z)
	t = (-A.Z) / (v.Z);

	intersectionPoint->X = A.X + (t*v.X);
	intersectionPoint->Y = A.Y + (t*v.Y);
}

void pointProjection (HPoint2D point) {
	point.h = 1.;

	// Proyectamos tal punto en 3D sobre el Plano Imagen de nuestra c�mara virtual
	backproject(&myActualPoint3D, point, virtualcam0);

	linePlaneIntersection (myActualPoint3D, virtualcam0.position, &intersectionPoint); // luego negaremos los puntos
}

void visual_loc_guidisplay() {
	int i, j, c, row;

	initVirtualCam ();
  initConfiguration ();
	//drawRoboticLabPlanes ();
	glLineWidth (1.0);
	drawRoboticLabLines ();
	drawHallLines ();
	drawTecho ();

	if (!filter_on)
		drawRecuadro ();

	if (clickOnDraw) {
		drawParticles ();

		if (!writtenFile) {
			escribirFichero ();
			writtenFile = TRUE;
		}
	}

	if (foa_mode) {
		update_camera_matrix(&virtualcam0);
		pointProjection (myPixel);
	}

	glPointSize (7.0);	
	glColor3f (1.0, 0., 0.);
	glBegin(GL_POINTS);
		v3f(intersectionPoint.X, intersectionPoint.Y,	intersectionPoint.Z);
	glEnd();

	if (manualMode) { // C�lculo de posici�n MANUAL de observaci�n
		filtroTeorica (intersectionPoint.X, intersectionPoint.Y, DEGTORAD*((float) fl_get_slider_value(fd_visual_locgui->theta)));
	} else { // Por defecto, la posici�n de observaci�n ser� la posici�n del robot
		filtroTeorica (myencoders[1], -myencoders[0], myencoders[2]);
	}

	// Sea cual fuere el punto de observaci�n elegido, hacemos el resumen y creamos la imagen resumida de tal vista
	resumirImagenTeorica ();
	crearImagenResumenTeorica();


  // intercambio de buffers: ha de hacerse lo �ltimo para tomar siempre todos los objetos de OpenGL
  glXSwapBuffers(fl_display, fl_get_canvas_id(fd_visual_locgui->micanvas));

/*
  int i,j,indice_max,index_grid,R,G,B;
  Tvoxel kaka,aux_point;
  float prob,nivel,mayor, diff;

  fl_winset(visual_loc_canvas_win); 
  
  if ((track_robot==TRUE)&&
      ((fabs(myencoders[0]-odometrico[0])>(rango/4.))||
       (fabs(myencoders[1]-odometrico[1])>(rango/4.))))
    {odometrico[0]=myencoders[0];
      odometrico[1]=myencoders[1];
      visual_refresh = TRUE;
      if (debug) printf("gui: robot tracking, display movement\n"); 
    }

  if (iteracion_display*visual_locgui_cycle>FORCED_REFRESH) 
    {iteracion_display=0;
      visual_refresh=TRUE;
    }
  else iteracion_display++;


  if (visual_refresh==TRUE)
    {
      if (debug) printf(" TOTAL ");
      fl_rectbound(0,0,width,height,suelo);   
      XFlush(display);
      //"I think I am" grid refresh
      if ((corte)||(show_grid_location))
	for(i=0;i<((location_grid->size)*(location_grid->size));i++)
	  {
	    if (grid2canvas(location_grid->map[i].centro,&celdillaGraf,location_grid)>=0)
	      {
		prob=(location_grid->map[i].probabilidad);
		nivel=((prob-COTA_MIN)/(COTA_MAX-COTA_MIN))*255;
		fl_mapcolor(mi_gris,(int)nivel, (int)nivel, (int)nivel);
		fl_set_foreground(visual_locgui_gc,mi_gris);
		XDrawRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&celdillaGraf,1);
		XFillRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&celdillaGraf,1);
	      }
	  }
    }
  
  if (visual_loc_grid_setup==FALSE)
    {
      visual_loc_displaysetup();
      visual_loc_grid_setup=TRUE;
    }
  */
  average=fl_get_button(fd_visual_locgui->visrobot_corregido_media)==PUSHED;
  maxime=fl_get_button(fd_visual_locgui->visrobot_corregido_max)==PUSHED;
  if (average)
    fl_set_button(fd_visual_locgui->visrobot_corregido_max,RELEASED);
  if (maxime)
    fl_set_button(fd_visual_locgui->visrobot_corregido_media,RELEASED);
	/*
  
  file_log=(fl_get_button(fd_visual_locgui->log)==PUSHED);
  open_eyes=(fl_get_button(fd_visual_locgui->open_eyes)==PUSHED);*/
  filter_on=(fl_get_button(fd_visual_locgui->filter_on)==PUSHED);
	manualMode=(fl_get_button(fd_visual_locgui->manualVision)==PUSHED);
/*  throw_part=(fl_get_button(fd_visual_locgui->throw_part)==PUSHED);

  if (fl_get_button(fd_visual_locgui->redistribution)==PUSHED)
    {
      throw_part=0;
      fl_set_button(fd_visual_locgui->filter_on,RELEASED);
      completed_particles=0;
      
    }
*/
  if (alarm_degeneration)
    fl_set_button(fd_visual_locgui->degeneration,PUSHED);
  else
    fl_set_button(fd_visual_locgui->degeneration,RELEASED);
/*
  //Visualization of the particles*

  if (throw_part)
    {
      /*Erase last particles*
      if ((((display_state&DISPLAY_PARTICLES)!=0) &&(visual_refresh==FALSE))
	  || (visual_delete_particles==TRUE))
	for(i=0;i<GUINUM_PARTICULAS;i++)
	  {
	    fl_set_foreground(visual_locgui_gc,suelo);
	    if (grid2canvas(particles[i].point,&celdillaGraf,visual_loc_grid)>=0){
	      XDrawRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&old_cells[i],1);
	      XFillRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&old_cells[i],1);
	    }
	    XDrawLine(display,visual_loc_canvas_win,visual_locgui_gc,graphic_particles[i].point.x,graphic_particles[i].point.y,graphic_particles[i].theta_draw_point.x,graphic_particles[i].theta_draw_point.y);
	    
	  }
      
      if ((display_state&DISPLAY_PARTICLES)!=0)
	for(i=0;i<GUINUM_PARTICULAS;i++)
	  {
	    
	    if (grid2canvas(particles[i].point,&celdillaGraf,visual_loc_grid)>=0)
	      {		           
		//Drawing new particle
		R=(int)255;
		G=(int)((1.-particles[i].probability)*255);
		B=(int)((1.-particles[i].probability)*255);
		fl_mapcolor(mi_rojo,R,G,B);
		fl_set_foreground(visual_locgui_gc,mi_rojo);
		
		XDrawRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&celdillaGraf,1);
		XFillRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&celdillaGraf,1);
		old_cells[i]=celdillaGraf;
		aux_point.x=particles[i].point.x+200*cos(particles[i].theta*DEGTORAD);
		aux_point.y=particles[i].point.y+200*sin(particles[i].theta*DEGTORAD);
		xy2canvas(particles[i].point,&graphic_particles[i].point);
		xy2canvas(aux_point,&graphic_particles[i].theta_draw_point);
		XDrawLine(display,visual_loc_canvas_win,visual_locgui_gc,graphic_particles[i].point.x,graphic_particles[i].point.y,graphic_particles[i].theta_draw_point.x,graphic_particles[i].theta_draw_point.y);
		
	      }
	  }
    }
  else
    {
      //Cleaning the particles
      for(i=0;i<GUINUM_PARTICULAS;i++)
	{
	  fl_set_foreground(visual_locgui_gc,suelo);
	  if (grid2canvas(particles[i].point,&celdillaGraf,visual_loc_grid)>=0){
	    XDrawRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&old_cells[i],1);
	    XFillRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&old_cells[i],1);
	  }
	  XDrawLine(display,visual_loc_canvas_win,visual_locgui_gc,graphic_particles[i].point.x,graphic_particles[i].point.y,graphic_particles[i].theta_draw_point.x,graphic_particles[i].theta_draw_point.y);
	  
	}
    }
  
  //Visualization of one of the grids
  corte=(fl_get_button(fd_visual_locgui->corte)==PUSHED);
  
  if (corte)
    {
      view_cono=0;
      robot_teorico[2]=360*fl_get_dial_value(fd_visual_locgui->input_dial);
      diff=robot_teorico[2]-valores_discretos_theta[0];
      i=0;
      while((i<NUM_REJILLAS)&&(diff>0))
	{
	  
	  if (fabs(robot_teorico[2]-valores_discretos_theta[i])<=diff)
	    {	
	      diff=robot_teorico[2]-valores_discretos_theta[i];
	      index_dial=valores_discretos_theta[i];
	      index_grid=i;
	    }
	  i++;
	}
      i=0;
      sprintf(fpstext,"%.1f�",robot_teorico[2]);
      fl_set_object_label(fd_visual_locgui->input_theta,fpstext);
      sprintf(fpstext,"%.1f�",index_dial);
      fl_set_object_label(fd_visual_locgui->output_theta,fpstext);
      
      fl_set_dial_value(fd_visual_locgui->output_dial,index_dial/360.);
      
      for(i=0;i<((location_grid->size)*(location_grid->size));i++)
	if (grid2canvas(cubo_valido[index_grid]->map[i].centro,&celdillaGraf,cubo_valido[index_grid])>=0)
	  {
	    prob=(cubo_valido[index_grid]->map[i].probabilidad);
	    nivel=((prob-COTA_MIN)/(COTA_MAX-COTA_MIN))*255;
	    //nivel=prob*255;
	    if (nivel!=location_grid->map[i].estado)
	    // then I should display in the GUI the new value for the cell
	      {
		location_grid->map[i].probabilidad=prob;
		//location_grid->map[i].estado=(int)nivel;
		fl_mapcolor(mi_gris,(int)nivel, (int)nivel, (int)nivel);
		fl_set_foreground(visual_locgui_gc,mi_gris);
		XDrawRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&celdillaGraf,1);
		XFillRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&celdillaGraf,1);
		
	      }
	    
	  }
    }
  else 
    {
      fl_set_foreground(visual_locgui_gc,suelo);
      XDrawRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&celdillaGraf,1);
      XFillRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&celdillaGraf,1);
    }
    
  //Visualization of location grid
  show_grid_location=(fl_get_button(fd_visual_locgui->grid_loc)==PUSHED);
  
  if (show_grid_location)
    {
      view_cono=0;
      for(i=0;i<((location_grid->size)*(location_grid->size));i++)
	{
	  mayor=cubo_valido[0]->map[i].probabilidad;
	  indice_max=0;
	  for(j=1;j<NUM_REJILLAS;j++)
	    {
	      if ((cubo_valido[j]->map[i].probabilidad)>mayor)
		{
		  mayor=cubo_valido[j]->map[i].probabilidad;
		  indice_max=j;
		} 
		} 
	  location_grid->map[i].probabilidad=cubo_valido[indice_max]->map[i].probabilidad;
	  if (grid2canvas(location_grid->map[i].centro,&celdillaGraf,location_grid)>=0)
	    {
	      prob=(location_grid->map[i].probabilidad);
	      nivel=((prob-COTA_MIN)/(COTA_MAX-COTA_MIN))*255;
	      if (nivel!=location_grid->map[i].estado)
		// then I should display in the GUI the new value for the cell
		{
		  location_grid->map[i].estado=(int)nivel;
		  fl_mapcolor(mi_gris,(int)nivel, (int)nivel, (int)nivel);
		  fl_set_foreground(visual_locgui_gc,mi_gris);
		  XDrawRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&celdillaGraf,1);
		  XFillRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&celdillaGraf,1);
		}
	    }
	}
    }
  
  
  else 
    {
      fl_set_foreground(visual_locgui_gc,suelo);
      XDrawRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&celdillaGraf,1);
      XFillRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&celdillaGraf,1);
    }
  
  //Visualization of world grid

  show_grid=(fl_get_button(fd_visual_locgui->grid)==PUSHED);

  if (show_grid) 
    { 
      show=1;
      for(i=0;i<((visual_loc_grid->size)*(visual_loc_grid->size));i++)  
	{ 
	  if (grid2canvas(visual_loc_grid->map[i].centro,&celdillaGraf,visual_loc_grid)>=0)
	    // checks whether the cell falls outside the canvas or not 
	    {
	      
	      switch ((int)(visual_loc_grid->map[i].estado))
		{
		case (-1):{
		  fl_set_foreground(visual_locgui_gc,FL_WHITE);
		  XDrawRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&celdillaGraf,1);
		  XFillRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&celdillaGraf,1);
		  break;
		}
		case (-2):{
		  fl_set_foreground(visual_locgui_gc,puerta);
		  XDrawRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&celdillaGraf,1);
		  XFillRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&celdillaGraf,1);
		  break;
		}
		case (-3):{
		  fl_set_foreground(visual_locgui_gc,extintor);
		  XDrawRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&celdillaGraf,1);
		  XFillRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&celdillaGraf,1);
		  break;
		}
		case (-4):{
		  fl_set_foreground(visual_locgui_gc,papelera);
		  XDrawRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&celdillaGraf,1);
		  XFillRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&celdillaGraf,1);
		  break;
		}
		case (-5):{
		  fl_set_foreground(visual_locgui_gc,verde);
		  XDrawRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&celdillaGraf,1);
		  XFillRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&celdillaGraf,1);
		  break;
		}
		case (-6):{
		  fl_set_foreground(visual_locgui_gc,rojoTecho);
		  XDrawRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&celdillaGraf,1);
		  XFillRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&celdillaGraf,1);
		  break;
		}
		case (-7):{
		  fl_set_foreground(visual_locgui_gc,techo1);
		  XDrawRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&celdillaGraf,1);
		  XFillRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&celdillaGraf,1);
		  break;
		}
		case (-8):{
		  fl_set_foreground(visual_locgui_gc,techo2);
		  XDrawRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&celdillaGraf,1);
		  XFillRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&celdillaGraf,1);
		  break;
		}
		case (-9):{
		  fl_set_foreground(visual_locgui_gc,techo3);
		  XDrawRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&celdillaGraf,1);
		  XFillRectangles(display,visual_loc_canvas_win,visual_locgui_gc,&celdillaGraf,1);
		  break;
		}
		
		}
	      
	      visual_loc_grid->map[i].nueva=FALSE;
	      visual_loc_Graf[i].x=celdillaGraf.x;
	      visual_loc_Graf[i].y=celdillaGraf.y;
	      visual_loc_Graf[i].width=celdillaGraf.width;
	      visual_loc_Graf[i].height=celdillaGraf.height;
	      fl_set_foreground(visual_locgui_gc,suelo);
	    } 
	}
    }
  
  // Borrado del grid **
  else
    {
      fl_set_foreground(visual_locgui_gc,suelo); 
      XDrawRectangles(display,visual_loc_canvas_win,visual_locgui_gc,visual_loc_Graf,visual_loc_grid->size*visual_loc_grid->size);
      XFillRectangles(display,visual_loc_canvas_win,visual_locgui_gc,visual_loc_Graf,visual_loc_grid->size*visual_loc_grid->size);
    }
  show=0;
  
  //Vision del cono de vision**
  //printf("VIEW %d\n",view_cono);
  if (fl_get_button(fd_visual_locgui->cono_vision)==RELEASED) view_cono=0;

  if (view_cono==1) 
    {
      //O->FL_BLACK 1->FL_BLUE**
      
      vision(0);
      robot_teorico[0]=mouse_x;
      robot_teorico[1]=mouse_y;
      robot_teorico[2]=360*fl_get_dial_value(fd_visual_locgui->input_dial);
      robot_teorico[3]=cos(robot_teorico[2]*DEGTORAD);
      robot_teorico[4]=sin(robot_teorico[2]*DEGTORAD);
      vision(1);
      visual_refresh=TRUE;
    }
  else
    vision(0);
  
  // VISUALIZACION de una instantanea laser
  if ((((display_state&DISPLAY_LASER)!=0)&&(visual_refresh==FALSE))
      || (visual_delete_laser==TRUE))
    {  
      fl_set_foreground(visual_locgui_gc,suelo); 
      XDrawPoints(display,visual_loc_canvas_win,visual_locgui_gc,laser_dpy,NUM_LASER,CoordModeOrigin);
    }

  if ((display_state&DISPLAY_LASER)!=0){
    if (debug) printf(" laser ");
    for(i=0;i<NUM_LASER;i++)
      {
	laser2xy(i,mylaser[i],&kaka,&myencoders[0]);
	xy2canvas(kaka,&laser_dpy[i]);
      }
    fl_set_foreground(visual_locgui_gc,FL_BLUE);
    XDrawPoints(display,visual_loc_canvas_win,visual_locgui_gc,laser_dpy,NUM_LASER,CoordModeOrigin);
  }


  // VISUALIZACION: pintar o borrar de el PROPIO ROBOT. Siempre hay un repintado total. Esta es la ultima estructura que se se pinta, para que ninguna otra se solape encima

  if ((((display_state&DISPLAY_ROBOT)!=0) &&(visual_refresh==FALSE))
      || (visual_delete_ego==TRUE))
    {  
      fl_set_foreground(visual_locgui_gc,suelo); 
      // clean last robot, but only if there wasn't a total refresh. In case of total refresh the white rectangle already cleaned all
      for(i=0;i<numego;i++) XDrawLine(display,visual_loc_canvas_win,visual_locgui_gc,ego[i].x,ego[i].y,ego[i+1].x,ego[i+1].y);

    }
    

  if ((display_state&DISPLAY_ROBOT)!=0){
    if (debug) printf(" ego ");
    fl_set_foreground(visual_locgui_gc,FL_MAGENTA);
    // relleno los nuevos
    us2xy(15,0.,0.,&kaka,&myencoders[0]);
    xy2canvas(kaka,&ego[0]);
    us2xy(3,0.,0.,&kaka,&myencoders[0]);
    xy2canvas(kaka,&ego[1]);
    us2xy(4,0.,0.,&kaka,&myencoders[0]);
    xy2canvas(kaka,&ego[2]);
    us2xy(8,0.,0.,&kaka,&myencoders[0]);
    xy2canvas(kaka,&ego[3]);
    us2xy(15,0.,0.,&kaka,&myencoders[0]);
    xy2canvas(kaka,&ego[EGOMAX-1]);
    for(i=0;i<NUM_SONARS;i++)
      {
	us2xy((15+i)%NUM_SONARS,0.,0.,&kaka,&myencoders[0]); // Da en el Tvoxel kaka las coordenadas del sensor, pues es distancia 0
	xy2canvas(kaka,&ego[i+4]);       
      }
 
    // pinto los nuevos
    numego=EGOMAX-1;
    for(i=0;i<numego;i++) XDrawLine(display,visual_loc_canvas_win,visual_locgui_gc,ego[i].x,ego[i].y,ego[i+1].x,ego[i+1].y);
  }

// VISUALIZACION: pintar o borrar de el PROPIO ROBOT.  Siempre hay un repintado total. Esta es la ultima estructura que se se pinta, para que ninguna otra se solape encima

  if ((((display_state&DISPLAY_ROBOT_CORREGIDO)!=0) &&(visual_refresh==FALSE))
      || (visual_delete_ego_corregido==TRUE))
    {  
      fl_set_foreground(visual_locgui_gc,suelo); 
      // clean last robot, but only if there wasn't a total refresh. In case of total refresh the white rectangle already cleaned all
      for(i=0;i<numego;i++) XDrawLine(display,visual_loc_canvas_win,visual_locgui_gc,ego_corregido[i].x,ego_corregido[i].y,ego_corregido[i+1].x,ego_corregido[i+1].y);

    }
    

  if ((display_state&DISPLAY_ROBOT_CORREGIDO)!=0){
    if (debug) printf(" ego_corregido ");
    fl_set_foreground(visual_locgui_gc,FL_GREEN);
    // relleno los nuevos
    us2xy_corregido(15,0.,0.,&kaka);
    xy2canvas(kaka,&ego_corregido[0]);
    us2xy_corregido(3,0.,0.,&kaka);
    xy2canvas(kaka,&ego_corregido[1]);
    us2xy_corregido(4,0.,0.,&kaka);
    xy2canvas(kaka,&ego_corregido[2]);
    us2xy_corregido(8,0.,0.,&kaka);
    xy2canvas(kaka,&ego_corregido[3]);
    us2xy_corregido(15,0.,0.,&kaka);
    xy2canvas(kaka,&ego_corregido[EGOMAX-1]);
    for(i=0;i<NUM_SONARS;i++)
      {
	us2xy_corregido((15+i)%NUM_SONARS,0.,0.,&kaka); // Da en el Tvoxel kaka las coordenadas del sensor, pues es distancia 0 
	xy2canvas(kaka,&ego_corregido[i+4]);       
      }
 
    // pinto los nuevos 
    numego=EGOMAX-1;
    for(i=0;i<numego;i++) XDrawLine(display,visual_loc_canvas_win,visual_locgui_gc,ego_corregido[i].x,ego_corregido[i].y,ego_corregido[i+1].x,ego_corregido[i+1].y);
  }

  if (debug) printf("\n");*/

/*
  // grey imageA display *
  if ((display_state&DISPLAY_COLORIMAGEA)==0)
    {
      // Pasa de la imagen capturada (greyA) a la imagen para visualizar (imagenA_buf), "cambiando" de formato adecuadamente
      if ((vmode==PseudoColor)&&(fl_state[vmode].depth==8))
	{for(i=0; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS; i++) 
	    {
      imagenA_buf[i]= (unsigned char)tabla[(unsigned char)((*mycolorA)[i])];}}
      else if ((vmode==TrueColor)&&(fl_state[vmode].depth==16)) {
	  for(i=0; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS; i++)
	    { imagenA_buf[i*2+1]=(0xf8&((unsigned char)(*mycolorA)[i]))+((0xe0&((unsigned char)(*mycolorA)[i]))>>5);
	      imagenA_buf[i*2]=((0xf8&((unsigned char)(*mycolorA)[i]))>>3)+((0x1c&((unsigned char)(*mycolorA)[i]))<<3);
	    }
	}
      else if ((vmode==TrueColor)&&(fl_state[vmode].depth==24)) 
	{for(i=0; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS; i++) 
	    {	if (mycolorA != NULL) {
			  imagenA_buf[i*4]=(*mycolorA)[i]; // Blue Byte
	      imagenA_buf[i*4+1]=(*mycolorA)[i]; // Green Byte *
	      imagenA_buf[i*4+2]=(*mycolorA)[i]; // Red Byte *
	      imagenA_buf[i*4+3]=0; // dummy byte
		  }}
	}
    }
*/
  // color imageA display
//  if ((display_state&DISPLAY_COLORIMAGEA)!=0) {
		for(i=0; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS; i++) { 
			imagenA_buf[i*4]=(*mycolorA)[i*3];
      imagenA_buf[i*4+1]=(*mycolorA)[i*3+1];
      imagenA_buf[i*4+2]=(*mycolorA)[i*3+2];
      imagenA_buf[i*4+3]=0;  
		}
//  }

	// color imageB display
  for(i=0; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS; i ++) {
		if (mycolorB != NULL) {
			imagenB_buf[i*4]=(*mycolorB)[i*3];
			imagenB_buf[i*4+1]=(*mycolorB)[i*3+1];
			imagenB_buf[i*4+2]=(*mycolorB)[i*3+2];
			imagenB_buf[i*4+3]=0;  
		}
	}

  if ((display_state&DISPLAY_FILTRADAA)!=0) {      
		for(i=0; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS; i++) { 
			imagenA_buf[i*4+0]=obs_real_filtrada[i*4+0];
			imagenA_buf[i*4+1]=obs_real_filtrada[i*4+1];
			imagenA_buf[i*4+2]=obs_real_filtrada[i*4+2];
			imagenA_buf[i*4+3]=0;
		}
	}

//  if (((display_state&DISPLAY_FILTRADAA)!=0)||((display_state&DISPLAY_COLORIMAGEA)!=0))
//    {
      XPutImage(display,visual_locgui_win,visual_locgui_gc,imagenA,0,0,fd_visual_locgui->ventana0->x+1, fd_visual_locgui->ventana0->y+1, SIFNTSC_COLUMNS, SIFNTSC_ROWS);

      XPutImage(display,visual_locgui_win,visual_locgui_gc,imagenRealResumida,0,0,fd_visual_locgui->resumen0->x+1, fd_visual_locgui->resumen0->y+1, SIFNTSC_COLUMNS/4, SIFNTSC_ROWS/4);

      XPutImage(display,visual_locgui_win,visual_locgui_gc,imagenTeoricaResumida,0,0,fd_visual_locgui->resumen1->x+1, fd_visual_locgui->resumen1->y+1, SIFNTSC_COLUMNS/4, SIFNTSC_ROWS/4);

//    }


	XPutImage(display,visual_locgui_win,visual_locgui_gc,imagenB,0,0,fd_visual_locgui->ventana1->x+1, fd_visual_locgui->ventana1->y+1, SIFNTSC_COLUMNS, SIFNTSC_ROWS);

/*
  if ((display_state&DISPLAY_TEORICA)!=0)
    {
      //rellenamos el buffer*
      //dividiremos el ancho del buffer entre el numero de observaciones y por cada division pintaremos lo que hayamos recogido en la observaci�n teorica*
      j=0;
	for(i=0; i<OBS_ROWS*OBS_COLUMNS; i++) {   
		if (i%OBS_COLUMNS==0) j=0; 
		imagen_teorica_buf[i*4]= obs_teorica_plana[j*3];
		imagen_teorica_buf[i*4+1]=obs_teorica_plana[j*3+1];
		imagen_teorica_buf[i*4+2]=obs_teorica_plana[j*3+2];
		imagen_teorica_buf[i*4+3]= 0;
		j++;
	}

      // TODO: XPutImage(display,image_win,image_gc,imagen_teorica,0,0,fd_visual_locgui->obs_teorica->x+1, fd_visual_locgui->obs_teorica->y+1,OBS_COLUMNS, OBS_ROWS);
    }

if ((display_state&DISPLAY_REAL)!=0)
    {
            
      //rellenamos el buffer*
      //dividiremos el ancho del buffer entre el numero de observaciones y por cada division pintaremos lo que hayamos recogido en la observaci�n teorica*
            
	for(i=0; i<OBS_ROWS*OBS_COLUMNS; i++) {   
		imagen_real_buf[i*4]= obs_real_plana[i*3];
		imagen_real_buf[i*4+1]=obs_real_plana[i*3+1];
		imagen_real_buf[i*4+2]=obs_real_plana[i*3+2];
		imagen_real_buf[i*4+3]= 0;
	}
		
      // TODO: XPutImage(display,image_win,image_gc,imagen_real,0,0,fd_visual_locgui->obs_real->x+1, fd_visual_locgui->obs_real->y+1,OBS_COLUMNS, OBS_ROWS);
    }

 if ((display_state&DISPLAY_REAL)&&(display_state&DISPLAY_TEORICA)!=0)
   {
     //printf("Las imagenes son el %f%\n",dist);
     sprintf(fpstext,"%.1f %%",dist*100);
     fl_set_object_label(fd_visual_locgui->dist_frame,fpstext);
   }
 */
 // clear all flags. If they were set at the beginning, they have been already used in this iteration *
	visual_refresh=FALSE;
	visual_delete_us=FALSE; 
	visual_delete_laser=FALSE; 
	visual_delete_ego=FALSE;
	visual_delete_ego_corregido=FALSE;
	visual_delete_particles=FALSE;
}

void visual_loc_guibuttons (void *obj1) 
{
  /* Puesto que el control no se cede al form, sino que se hace polling de sus botones pulsados, debemos proveer el enlace para los botones que no tengan callback asociada, en esta rutina de polling. OJO aquellos que tengan callback asociada jamas se veran con fl_check_forms, la libreria llamara automaticamente a su callback sin que fl_check_forms o fl_do_forms se enteren en absoluto.*/
  FL_OBJECT *obj=(FL_OBJECT *)obj1;

  if ((obj == fd_visual_locgui->visrobot_corregido_media)
	   ||(obj == fd_visual_locgui->visrobot_corregido_max))
    {
      if ((fl_get_button(fd_visual_locgui->visrobot_corregido_media)==RELEASED)
	  &&(fl_get_button(fd_visual_locgui->visrobot_corregido_max)==RELEASED))
	{display_state = display_state & ~DISPLAY_ROBOT_CORREGIDO;
	  visual_delete_ego_corregido=TRUE;
	}
      else 
	display_state=display_state | DISPLAY_ROBOT_CORREGIDO;
    }

  else if (obj == fd_visual_locgui->viscolorimageA)
    {
      if (fl_get_button(fd_visual_locgui->viscolorimageA)==RELEASED)
	display_state = display_state & ~DISPLAY_COLORIMAGEA;
      else 
	display_state=display_state | DISPLAY_COLORIMAGEA;
    }
  else if (obj == fd_visual_locgui->colorimageA)
    { 
      if (fl_get_button(fd_visual_locgui->colorimageA)==PUSHED) {
         mycolorA=myimport ("colorA", "colorA");
         colorAresume=(resumeFn)myimport("colorA", "resume");
         colorAsuspend=(suspendFn)myimport("colorA", "suspend");
         if (colorAresume!=NULL){
            display_state = display_state | DISPLAY_COLORIMAGEA;
            colorAresume(visual_loc_id, NULL, NULL);
         }
         else{
            display_state = display_state & ~DISPLAY_COLORIMAGEA;
            fl_set_button(obj, RELEASED);
         }
      }
      else {
         display_state = display_state & ~DISPLAY_COLORIMAGEA;
         if (colorAsuspend!=NULL)
            colorAsuspend();
      }
  } else if(obj == fd_visual_locgui->visfiltradaA)
    {
      if (fl_get_button(fd_visual_locgui->visfiltradaA)==RELEASED)
	display_state = display_state & ~DISPLAY_FILTRADAA;
      else 
	display_state=display_state | DISPLAY_FILTRADAA;
    } 
  else if (obj == fd_visual_locgui->thetaButton)
    {
			if (!completed_particles) {
				myTheta = (float) fl_get_slider_value(fd_visual_locgui->theta);
				cleaning_particles_observation(); // borramos el contenido de las part�culas previas
				create_particles(min_num_part,max_num_part_x,max_num_part_y); // repartirmos part�culas entre 0 y max (X,Y)
				calculate_particles_observation();
				clickOnDraw = TRUE;
				completed_particles = 1;
			}
		}
}

int visual_loc_button_released_on_micanvas(FL_OBJECT *ob, Window win, int win_width, int win_height, XEvent *xev, void *user_data)
{
  FL_Coord y,x;
  unsigned int whichbutton;
  float ygraf, xgraf;
  Tvoxel nuevocentro;
  int i,j;

  fl_get_win_mouse(win,&x,&y,&whichbutton);
  /* win will be always the canvas window, because this callback has been defined only for that canvas */
  
  /* from graphical coordinates to spatial ones */
  xgraf=((float) (x-width/2))/escala;
  ygraf=((float) (height/2-y))/escala;
  mouse_x=(xgraf*odometrico[3]-ygraf*odometrico[4]+odometrico[0]);
  mouse_y=(xgraf*odometrico[4]+ygraf*odometrico[3]+odometrico[1]);
  mouse_new=1;
  
  printf("(%d,%d) CLICK on mouse_x=%.2f mouse_y=%.2f \nrobot_x=%.2f robot_y=%.2f  mira a=%.2f grados\n\n",x,y,mouse_x,mouse_y,myencoders[0],myencoders[1],myencoders[2]);
  
  return 0;
}

void visual_loc_guiresume_aux(void)
{
  static int k=0;
  if (k==0) { // not initialized
    k++;

    fd_visual_locgui = create_form_visual_locgui();
    fl_set_form_position(fd_visual_locgui->visual_locgui,200,50);
		fl_add_canvas_handler(fd_visual_locgui->micanvas,Expose,InitOGL,0);
		fl_add_canvas_handler(fd_visual_locgui->micanvas,MotionNotify,motion_event,0);
		fl_add_canvas_handler(fd_visual_locgui->micanvas,ButtonPress,button_press_event,0);
    //fl_add_canvas_handler(fd_visual_locgui->micanvas,ButtonRelease,visual_loc_button_released_on_micanvas,NULL);
	  
/*	  visual_locgui_gc = fl_state[fl_get_vclass()].gc[0];
	  visual_loc_canvas_win = FL_ObjWin(fd_visual_locgui->micanvas);
	  width = fd_visual_locgui->micanvas->w;
	  height = fd_visual_locgui->micanvas->h;
	  /* Empiezo con el canvas en negro *
	  fl_winset(visual_loc_canvas_win); 
	  fl_rectbound(0,0,width,height,suelo);*

	  /* Coord del sistema odometrico respecto del visual *
	  odometrico[0]=20000.;
	  odometrico[1]=22500.;
	  odometrico[2]=0.;
	  odometrico[3]=cos(0.);
	  odometrico[4]=sin(0.);

	  /*Definicion de los colores relevantes en el mundo*
	  fl_mapcolor(suelo,250,223,151);
	  fl_mapcolor(puerta,163,63,3);
	  fl_mapcolor(papelera,40,40,200);
	  fl_mapcolor(extintor,255,38,19);
	  fl_mapcolor(verde,0,255,0);
	  fl_mapcolor(rojoTecho,255,0,0);
	  fl_mapcolor(colorRaro,0,0,0);
	  fl_mapcolor(techo1,128,0,255);
	  fl_mapcolor(techo2,0,255,0);
	  fl_mapcolor(techo3,0,255,255);
	  fl_mapcolor(techo4,0,0,0);
	  fl_mapcolor(techo5,0,0,0);
	  fl_mapcolor(techo6,0,0,0);
	  fl_mapcolor(techo7,0,0,0);
	  fl_mapcolor(techo8,0,0,0);
	  fl_mapcolor(techo9,0,0,0);*/

	  fl_set_button(fd_visual_locgui->colorimageA,RELEASED);
		fl_set_slider_bounds(fd_visual_locgui->theta,(float)0.0,(float)359.0);
  	fl_set_slider_value(fd_visual_locgui->theta,(float)0.0);
	}

  myregister_buttonscallback(visual_loc_guibuttons);
  myregister_displaycallback(visual_loc_guidisplay);
	fl_show_form(fd_visual_locgui->visual_locgui,FL_PLACE_POSITION,FL_FULLBORDER,"GUIA MUSEO. LOCALIZACION VISUAL");
	visual_locgui_win = FL_ObjWin(fd_visual_locgui->ventana0);
	image_displaysetup();
}

void visual_loc_guiresume(){
   static callback fn=NULL;
   if (fn==NULL){
      if ((fn=(callback)myimport ("graphics_xforms", "resume_callback"))!=NULL){
         fn ((gui_function)visual_loc_guiresume_aux);
      }
   }
   else{
      fn ((gui_function)visual_loc_guiresume_aux);
   }
}

void visual_loc_guisuspend_aux(void)
{
  /* to make a safety stop when the robot is being teleoperated from GUI */
  mydelete_buttonscallback(visual_loc_guibuttons);
  mydelete_displaycallback(visual_loc_guidisplay);
  fl_hide_form(fd_visual_locgui->visual_locgui);
}

void visual_loc_guisuspend(){
  static callback fn=NULL;
  if (fn==NULL){
    if ((fn=(callback)myimport ("graphics_xforms", "suspend_callback"))!=NULL){
      fn ((gui_function)visual_loc_guisuspend_aux);
    }
  }
  else{
    fn ((gui_function)visual_loc_guisuspend_aux);
  }
}

