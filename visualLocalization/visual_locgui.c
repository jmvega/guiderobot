/* Form definition file generated with fdesign. */

#include "forms.h"
#include <stdlib.h>
#include "visual_locgui.h"

FD_visual_locgui *create_form_visual_locgui(void)
{
  FL_OBJECT *obj;
  FD_visual_locgui *fdui = (FD_visual_locgui *) fl_calloc(1, sizeof(*fdui));

  fdui->visual_locgui = fl_bgn_form(FL_NO_BOX, 1020, 630);
  obj = fl_add_box(FL_UP_BOX,0,0,1020,630,"");
    fl_set_object_lcolor(obj,FL_BLUE);
  fdui->ventana0 = obj = fl_add_frame(FL_ENGRAVED_FRAME,680,45,323,243,"Image0");
    fl_set_object_color(obj,FL_COL1,FL_COL1);
  fdui->frame_rateA = obj = fl_add_text(FL_NORMAL_TEXT,770,295,50,20,"");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
    fl_set_object_lalign(obj,FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
  fdui->colorimageA = obj = fl_add_lightbutton(FL_PUSH_BUTTON,700,295,70,20,"colorA");
    fl_set_object_boxtype(obj,FL_NO_BOX);
    fl_set_object_color(obj,FL_COL1,FL_GREEN);
    fl_set_object_lcolor(obj,FL_GREEN);
    fl_set_object_lsize(obj,FL_NORMAL_SIZE);
    fl_set_object_lstyle(obj,FL_TIMESBOLD_STYLE);
  fdui->viscolorimageA = obj = fl_add_button(FL_PUSH_BUTTON,680,295,20,20,"");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
    fl_set_object_color(obj,FL_COL1,FL_YELLOW);
    fl_set_object_lcolor(obj,FL_DARKTOMATO);
  fdui->frame_filtradaA = obj = fl_add_text(FL_NORMAL_TEXT,950,295,50,20,"");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
    fl_set_object_lalign(obj,FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
  fdui->filtradaA = obj = fl_add_lightbutton(FL_PUSH_BUTTON,860,295,70,20,"filteredA");
    fl_set_object_boxtype(obj,FL_NO_BOX);
    fl_set_object_color(obj,FL_COL1,FL_GREEN);
    fl_set_object_lcolor(obj,FL_GREEN);
    fl_set_object_lsize(obj,FL_NORMAL_SIZE);
    fl_set_object_lstyle(obj,FL_TIMESBOLD_STYLE);
  fdui->visfiltradaA = obj = fl_add_button(FL_PUSH_BUTTON,840,295,20,20,"");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
    fl_set_object_color(obj,FL_COL1,FL_YELLOW);
    fl_set_object_lcolor(obj,FL_DARKTOMATO);
  obj = fl_add_labelframe(FL_ENGRAVED_FRAME,15,510,340,100,"Montecarlo method");
    fl_set_object_lcolor(obj,FL_GREEN);
    fl_set_object_lsize(obj,FL_NORMAL_SIZE);
    fl_set_object_lstyle(obj,FL_FIXEDBOLD_STYLE+FL_SHADOW_STYLE);
  fdui->filter_on = obj = fl_add_button(FL_PUSH_BUTTON,125,550,100,20,"filter-on");
    fl_set_button_shortcut(obj,"^M",1);
    fl_set_object_boxtype(obj,FL_ROUNDED3D_UPBOX);
    fl_set_object_color(obj,FL_DARKER_COL1,FL_TOMATO);
    fl_set_object_lcolor(obj,FL_YELLOW);
  fdui->degeneration = obj = fl_add_button(FL_NORMAL_BUTTON,20,525,90,70,"Degeneration");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
    fl_set_object_color(obj,FL_DARKER_COL1,FL_TOMATO);
    fl_set_object_lcolor(obj,FL_YELLOW);
  fdui->visrobot_corregido_media = obj = fl_add_button(FL_PUSH_BUTTON,230,560,50,30,"Average");
    fl_set_object_boxtype(obj,FL_ROUNDED3D_UPBOX);
    fl_set_object_color(obj,FL_DARKER_COL1,FL_WHITE);
    fl_set_object_lcolor(obj,FL_BLUE);
    fl_set_object_lsize(obj,FL_TINY_SIZE);
  fdui->visrobot_corregido_max = obj = fl_add_button(FL_PUSH_BUTTON,230,530,50,30,"Maxime");
    fl_set_object_boxtype(obj,FL_ROUNDED3D_UPBOX);
    fl_set_object_color(obj,FL_DARKER_COL1,FL_WHITE);
    fl_set_object_lcolor(obj,FL_BLUE);
    fl_set_object_lsize(obj,FL_TINY_SIZE);
  fdui->log = obj = fl_add_button(FL_PUSH_BUTTON,135,570,80,30,"log");
    fl_set_object_boxtype(obj,FL_ROUNDED3D_UPBOX);
    fl_set_object_color(obj,FL_DARKER_COL1,FL_SLATEBLUE);
    fl_set_object_lcolor(obj,FL_RED);
  fdui->ventana1 = obj = fl_add_frame(FL_ENGRAVED_FRAME,680,370,320,240,"Image0");
    fl_set_object_color(obj,FL_COL1,FL_COL1);
  fdui->resumen0 = obj = fl_add_frame(FL_ENGRAVED_FRAME,440,550,80,60,"Image0");
    fl_set_object_color(obj,FL_COL1,FL_COL1);
  fdui->resumen1 = obj = fl_add_frame(FL_ENGRAVED_FRAME,565,550,80,60,"Image0");
    fl_set_object_color(obj,FL_COL1,FL_COL1);
  obj = fl_add_text(FL_NORMAL_TEXT,780,10,140,30,"Imagen real");
    fl_set_object_lcolor(obj,FL_DODGERBLUE);
    fl_set_object_lsize(obj,FL_LARGE_SIZE);
    fl_set_object_lalign(obj,FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
    fl_set_object_lstyle(obj,FL_NORMAL_STYLE+FL_EMBOSSED_STYLE);
  obj = fl_add_text(FL_NORMAL_TEXT,770,330,160,30,"Imagen techo");
    fl_set_object_lcolor(obj,FL_RED);
    fl_set_object_lsize(obj,FL_LARGE_SIZE);
    fl_set_object_lalign(obj,FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
    fl_set_object_lstyle(obj,FL_NORMAL_STYLE+FL_EMBOSSED_STYLE);
  obj = fl_add_text(FL_NORMAL_TEXT,420,525,120,20,"I. Real Resumida");
    fl_set_object_lcolor(obj,FL_DODGERBLUE);
    fl_set_object_lalign(obj,FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
  obj = fl_add_text(FL_NORMAL_TEXT,535,525,140,20,"I. Te�rica Resumida");
    fl_set_object_lcolor(obj,FL_RED);
    fl_set_object_lalign(obj,FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
  fdui->micanvas = obj = fl_add_glcanvas(FL_NORMAL_CANVAS,15,15,640,480,"");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
  fdui->theta = obj = fl_add_valslider(FL_VERT_BROWSER_SLIDER,370,500,50,100,"Theta");
  fdui->thetaButton = obj = fl_add_button(FL_NORMAL_BUTTON,145,520,60,30,"GO!");
    fl_set_object_color(obj,FL_RED,FL_COL1);
    fl_set_object_lcolor(obj,FL_YELLOW);
  obj = fl_add_text(FL_NORMAL_TEXT,280,535,70,20,"Progress");
    fl_set_object_lcolor(obj,FL_SPRINGGREEN);
    fl_set_object_lalign(obj,FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
  obj = fl_add_text(FL_NORMAL_TEXT,325,560,20,20,"%");
    fl_set_object_color(obj,FL_DARKCYAN,FL_MCOL);
    fl_set_object_lcolor(obj,FL_YELLOW);
    fl_set_object_lalign(obj,FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
  fdui->progress = obj = fl_add_frame(FL_ENGRAVED_FRAME,285,560,40,20,"");
    fl_set_object_color(obj,FL_DARKCYAN,FL_COL1);
    fl_set_object_lcolor(obj,FL_YELLOW);
  fdui->manualVision = obj = fl_add_button(FL_PUSH_BUTTON,485,500,120,20,"User observation");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
    fl_set_object_color(obj,FL_DARKER_COL1,FL_TOMATO);
    fl_set_object_lcolor(obj,FL_DODGERBLUE);
  fl_end_form();

  fdui->visual_locgui->fdui = fdui;

  return fdui;
}
/*---------------------------------------*/

