set dgrid3d 60,60
set hidden3d
set xlabel "Eje X Laboratorio"
set ylabel "Eje Y Laboratorio"
set title "Probabilidad particulas"
set zrange [0:1]
set terminal png
set output "grafica.png"
splot "ficheroGrafico.txt" u 1:2:4 with lines
