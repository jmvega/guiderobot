/** Header file generated with fdesign on Tue Oct 14 13:46:19 2008.**/

#ifndef FD_visual_locgui_h_
#define FD_visual_locgui_h_

/** Callbacks, globals and object handlers **/


/**** Forms and Objects ****/
typedef struct {
	FL_FORM *visual_locgui;
	void *vdata;
	char *cdata;
	long  ldata;
	FL_OBJECT *ventana0;
	FL_OBJECT *frame_rateA;
	FL_OBJECT *colorimageA;
	FL_OBJECT *viscolorimageA;
	FL_OBJECT *frame_filtradaA;
	FL_OBJECT *filtradaA;
	FL_OBJECT *visfiltradaA;
	FL_OBJECT *filter_on;
	FL_OBJECT *degeneration;
	FL_OBJECT *visrobot_corregido_media;
	FL_OBJECT *visrobot_corregido_max;
	FL_OBJECT *log;
	FL_OBJECT *ventana1;
	FL_OBJECT *resumen0;
	FL_OBJECT *resumen1;
	FL_OBJECT *micanvas;
	FL_OBJECT *theta;
	FL_OBJECT *thetaButton;
	FL_OBJECT *progress;
	FL_OBJECT *manualVision;
} FD_visual_locgui;

extern FD_visual_locgui * create_form_visual_locgui(void);

#endif /* FD_visual_locgui_h_ */
